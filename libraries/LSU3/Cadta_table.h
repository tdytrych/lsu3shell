#ifndef LSU3_CADTATABLE_H
#define LSU3_CADTATABLE_H
#include <UNU3SU3/UNU3SU3Basics.h>
#include <LSU3/types.h>

namespace lsu3
{

class Cadta_table
{
	public:
	Cadta_table(const std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC>& proton_adta, const std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC>& neutron_adta);
	inline LmMuS2 getTensor(const uint32_t index) const {return data_[index];}
	inline SU3xSU2::LABELS operator[] (const uint32_t index) const {return SU3xSU2::LABELS(1, get_lm(data_[index]), get_mu(data_[index]), get_S2(data_[index]));}

	uint32_t getId(const SU3xSU2::LABELS& tensor) const
	{
		std::vector<LmMuS2>::const_iterator it = std::lower_bound(data_.begin(), data_.end(), cpp0x::tie(tensor.lm, tensor.mu, tensor.S2));
		assert(it != data_.end());
		return std::distance(data_.begin(), it);
	}
	private:
//	LmMuS2 data type defined in LSU3/types.h	
	std::vector<LmMuS2> data_;
};
}
#endif
