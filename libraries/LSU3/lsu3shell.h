#pragma once

#include <cstdint>
#include <string>

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/VBC_Matrix.h>
#include <SU3NCSMUtils/CRunParameters.h>

namespace lsu3
{
   void MapRankToColRow_MFDn_Compatible(int ndiag, int my_rank, int& row, int& col);

   // forward declarations:
   class CncsmSU3xSU2Basis;
   class VBC_Matrix;

   uintmax_t CalculateME(
         const std::string& hilbert_space_definition_file_name,
         const std::string& hamiltonian_file_name,
         int ndiag, int idiag, int jdiag,
         VBC_Matrix& vbc, long int& nstates_sofar, uint64_t& dim,
         CncsmSU3xSU2Basis& bra, CncsmSU3xSU2Basis& ket,
         CRunParameters& run_params,
         bool simulate);
}
