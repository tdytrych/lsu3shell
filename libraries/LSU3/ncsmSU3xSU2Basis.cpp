#include <LSU3/ncsmSU3xSU2Basis.h>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#define ALTERNATIVE_RESHUFFLE
#define ALTERNATIVE_GEN_FIN_IRREPS
//#define DEVEL_LOGS
//#define DEV_TIME_LOGS

#ifdef ALTERNATIVE_RESHUFFLE
#include <omp.h>

#include <algorithm>
#include <chrono>
#include <type_traits>
#endif

#ifdef DEV_TIME_LOGS
#include <omp.h>

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");
#endif

std::string MakeBasisName(const std::string& basis_file_header, int idiag, int ndiag) {
   std::string file_name(basis_file_header);
   file_name += ".basis_n";

#if (__GNUC__ == 4 && __GNUC_MINOR__ == 4)
   file_name += std::to_string((long long int)ndiag);
#else
   file_name += std::to_string(ndiag);
#endif

   file_name += "i";

#if (__GNUC__ == 4 && __GNUC_MINOR__ == 4)
   file_name += std::to_string((long long int)idiag);
#else
   file_name += std::to_string(idiag);
#endif

   return file_name;
}


bool IsProcessDiagonal(int my_rank, int ndiag) { return my_rank < ndiag; };

namespace lsu3 {

/******************************************************************
                        *** Functions facilitating debugging ****
 ******************************************************************/

void Set(CTuple<int, 4>& lmS, const UN::SU3xSU2& gamma) {
   lmS[0] = gamma.lm;
   lmS[1] = gamma.mu;
   lmS[2] = gamma.S2;
   lmS[3] = gamma.mult;
}
void Set(CTuple<int, 4>& lmS, const SU3xSU2::LABELS& omega) {
   lmS[0] = omega.lm;
   lmS[1] = omega.mu;
   lmS[2] = omega.S2;
   lmS[3] = omega.rho;
}

template <class T>
void Output(const T& gamma, const SU3xSU2_SMALL_VEC_BASE& omega, std::vector<CTuple<int, 4>>& Labels) {
   CTuple<int, 4> lmS;

   Set(lmS, gamma[0]);
   Labels.push_back(lmS);

   if (omega.empty()) {
      return;
   }

   Set(lmS, gamma[1]);
   Labels.push_back(lmS);

   for (size_t iomega = 0; iomega < omega.size() - 1; ++iomega) {
      Set(lmS, omega[iomega]);
      Labels.push_back(lmS);
      Set(lmS, gamma[iomega + 2]);
      Labels.push_back(lmS);
   }
   Set(lmS, (omega.back()));
   Labels.push_back(lmS);
}

void Print(const CTuple<int, 4>& lmS) {
   cout << lmS[3] << "(" << lmS[0] << " " << lmS[1] << ")" << lmS[2];
}

void PrintState(const std::vector<CTuple<int, 4>>& Labels) {
   cout << "[";
   size_t index;
   size_t nOmegas = (Labels.size() - 1) / 2;
   for (size_t i = 0; i < nOmegas; ++i) {
      cout << "{";
   }
   Print(Labels[0]);
   if (nOmegas > 0) {
      cout << " x ";
      Print(Labels[1]);
      cout << "}";

      index = 2;
      for (size_t i = 0; i < nOmegas - 1; ++i) {
         Print(Labels[index]);
         cout << " x ";
         Print(Labels[index + 1]);
         cout << "}";
         index += 2;
      }
      Print(Labels[index]);
   }

   cout << "]";
}

void Print(const SingleDistributionSmallVectorBase& distr) {
   cout << "[";
   for (size_t i = 0; i < distr.size() - 1; ++i) {
      cout << (int)distr[i] << " ";
   }
   cout << (int)distr.back() << "]";
}

void Print(UN::SU3xSU2_VEC& gamma, SU3xSU2_SMALL_VEC_BASE& omega) {
   std::vector<CTuple<int, 4>> Labels;

   Output(gamma, omega, Labels);

   PrintState(Labels);
}

void CncsmSU3xSU2Basis::ShowProtonConf(uint32_t proton_irrep_index) const {
   SingleDistributionSmallVector distr_p;
   UN::SU3xSU2_VEC gamma_p;
   SU3xSU2_SMALL_VEC omega_p;

   getDistr_p(proton_irrep_index, distr_p);
   getGamma_p(proton_irrep_index, gamma_p);
   getOmega_p(proton_irrep_index, omega_p);

   Print(distr_p);
   Print(gamma_p, omega_p);
}

void CncsmSU3xSU2Basis::ShowNeutronConf(uint32_t neutron_irrep_index) const {
   SingleDistributionSmallVector distr_n;
   UN::SU3xSU2_VEC gamma_n;
   SU3xSU2_SMALL_VEC omega_n;

   getDistr_n(neutron_irrep_index, distr_n);
   getGamma_n(neutron_irrep_index, gamma_n);
   getOmega_n(neutron_irrep_index, omega_n);

   Print(distr_n);
   Print(gamma_n, omega_n);
}

/********************************************************************************
                        *** Constructors and their helper functions   ****
 ********************************************************************************/
struct Compare_UNSU3xSU2_VEC {
   struct Compare_UNSU3xSU2 {
      inline bool operator()(const UN::SU3xSU2& a, const UN::SU3xSU2& b) const {
         return (a.lm < b.lm ||
                 (((a.lm == b.lm) && a.mu < b.mu) ||
                  ((a.lm == b.lm && a.mu == b.mu && a.S2 < b.S2) ||
                   (a.lm == b.lm && a.mu == b.mu && a.S2 == b.S2 && a.mult < b.mult))));
      }
   };
   inline bool operator()(const UN::SU3xSU2_VEC& a, const UN::SU3xSU2_VEC& b) const {
      return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(),
                                          Compare_UNSU3xSU2());
   }
};

struct Compare_SU3_VEC {
   //	SU3::LABELS::operator< does not compare rho maximal multiplicity!
   //	and hence I have to define my own operator< which takes into account
   //	also multiplicities: Compare_SU3_With_Rho
   struct Compare_SU3_With_Rho {
      inline bool operator()(const SU3::LABELS& lhs, const SU3::LABELS& rhs) const {
         return (lhs.lm < rhs.lm ||
                 (((lhs.lm == rhs.lm) && lhs.mu < rhs.mu) ||
                  ((lhs.lm == rhs.lm && lhs.mu == rhs.mu && lhs.rho < rhs.rho))));
      }
   };
   inline bool operator()(const SU3_VEC& a, const SU3_VEC& b) const {
      return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(),
                                          Compare_SU3_With_Rho());
   }
};

void CncsmSU3xSU2Basis::GenerateDistrGammaOmega_SortedVectors(
    const CncsmSU3Basis& basis, std::vector<SingleDistributionSmallVector>& distribs_p,
    std::vector<SingleDistributionSmallVector>& distribs_n, std::vector<UN::SU3xSU2_VEC>& gammas,
    std::vector<SU3_VEC>& omegas_su3, std::vector<SU2_VEC>& omegas_spin)

{
   std::set<SingleDistributionSmallVector> distribs_p_set, distribs_n_set;
   std::set<UN::SU3xSU2_VEC, Compare_UNSU3xSU2_VEC> gammas_set;
   std::set<SU3_VEC, Compare_SU3_VEC> omegas_su3_set;
   std::set<SU2_VEC> omegas_spin_set;

   SingleDistributionSmallVector distr_p, distr_n;
   UN::SU3xSU2_VEC gamma_p, gamma_n;
   SU3xSU2_SMALL_VEC omega_p, omega_n;

   for (IdentFermConfIterator protonConfIter = basis.firstProtonConf(); protonConfIter.hasDistr();
        protonConfIter.nextDistr()) {
      distr_p.resize(0);
      protonConfIter.getDistr(distr_p);
      distribs_p_set.insert(distr_p);

      for (protonConfIter.rewindGamma(); protonConfIter.hasGamma(); protonConfIter.nextGamma()) {
         gamma_p.resize(0);
         protonConfIter.getGamma(gamma_p);
         gammas_set.insert(gamma_p);

         for (protonConfIter.rewindOmega(); protonConfIter.hasOmega(); protonConfIter.nextOmega()) {
            omega_p.resize(0);
            protonConfIter.getOmega(omega_p);
            omegas_su3_set.insert(SU3_VEC(omega_p.begin(), omega_p.end()));
            omegas_spin_set.insert(SU2_VEC(omega_p.begin(), omega_p.end()));
         }
      }
   }

   for (IdentFermConfIterator neutronConfIter = basis.firstNeutronConf();
        neutronConfIter.hasDistr(); neutronConfIter.nextDistr()) {
      distr_n.resize(0);
      neutronConfIter.getDistr(distr_n);
      distribs_n_set.insert(distr_n);

      for (neutronConfIter.rewindGamma(); neutronConfIter.hasGamma(); neutronConfIter.nextGamma()) {
         gamma_n.resize(0);
         neutronConfIter.getGamma(gamma_n);
         gammas_set.insert(gamma_n);

         for (neutronConfIter.rewindOmega(); neutronConfIter.hasOmega();
              neutronConfIter.nextOmega()) {
            omega_n.resize(0);
            neutronConfIter.getOmega(omega_n);
            omegas_su3_set.insert(SU3_VEC(omega_n.begin(), omega_n.end()));
            omegas_spin_set.insert(SU2_VEC(omega_n.begin(), omega_n.end()));
         }
      }
   }

   distribs_p.reserve(distribs_p_set.size());
   distribs_n.reserve(distribs_n_set.size());

   distribs_p.insert(distribs_p.end(), distribs_p_set.begin(), distribs_p_set.end());
   distribs_n.insert(distribs_n.end(), distribs_n_set.begin(), distribs_n_set.end());
   gammas.insert(gammas.end(), gammas_set.begin(), gammas_set.end());
   omegas_su3.insert(omegas_su3.end(), omegas_su3_set.begin(), omegas_su3_set.end());
   omegas_spin.insert(omegas_spin.end(), omegas_spin_set.begin(), omegas_spin_set.end());
}

#ifndef ALTERNATIVE_RESHUFFLE
// the last two arguments (std::function, MPI_Comm) are redundant in the old
// version of Reshuffle as dims_ vectors are computed for each section.
void CncsmSU3xSU2Basis::Reshuffle(const proton_neutron::ModelSpace& ncsmModelSpace,
                                  const uint16_t idiag, const uint16_t ndiag, std::function<bool (int, int)>, MPI_Comm) {
#ifdef DEV_TIME_LOGS
   double start = omp_get_wtime();
#endif

   uint32_t number_of_states(0);

   idiag_ = idiag;
   ndiag_ = ndiag;

   dims_.clear();
   dims_.insert(dims_.begin(), ndiag, 0);

   max_states_in_block_ = 0;
   pnbasis_ipin_.resize(0);
   first_state_in_block_.resize(0);
   block_end_.resize(0);
   wpn_.resize(0);
   //	iterate over all possible (ip in) pairs and generate
   //	wpn_
   //	wpn_irreps_container_
   //	pnbasis_
   SU3xSU2_VEC wpn_su3xsu2;
   wpn_su3xsu2.reserve(1024);

   SU3_VEC wpn_su3;
   wpn_su3.reserve(1024);

   SU2_VEC allowed_spins;
   allowed_spins.reserve(50);

   std::vector<uint32_t> wpn_container_indices(wpn_su3xsu2.size());
   wpn_container_indices.reserve(1024);

   proton_neutron::ModelSpace::const_iterator nhwSubspace;
   int ipin_pair(0);

   for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
      int Nphw = nhw_p(index_p);
      int ap_max = getMult_p(index_p);
      SU3xSU2::LABELS wp(getProtonSU3xSU2(index_p));
      for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
         int Nhw = Nphw + nhw_n(index_n);
         if (Nhw > Nmax()) {
            break;  // fermionic irreps are stored in an increasing order according to the number of
                    // oscillator quanta
         }
         nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
         if (nhwSubspace ==
             ncsmModelSpace
                 .end())  // => given Nhw subspace does not belong to ncsmModelSpace model space
         {
            continue;
         }
         int an_max = getMult_n(index_n);
         SU3xSU2::LABELS wn(getNeutronSU3xSU2(index_n));

         int idiag_current =
             (ipin_pair %
              ndiag_);  // current (ip, in) pair belongs to idiag_current section of basis
         allowed_spins.resize(0);
         std::vector<SU3_VEC> allowed_su3;
         //	for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
         //	return vector of allowed SU3 labels for each allowed spin:
         //	{{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
         nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(wp.S2, wn.S2), allowed_spins,
                                            allowed_su3);
         uint32_t nwpn_spin = allowed_spins.size();
         if (nwpn_spin)  // ==> (Sp Sn) are allowed
         {
            wpn_su3.resize(0);
            wpn_su3xsu2.resize(0);
            SU3::Couple(wp, wn, wpn_su3);
            SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();
            //	iterate over allowed spins and create vector of allowed SU3xSU2 irreps
            for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin) {
               if (!allowed_su3[ispin].empty())  //	==> I need to select only allowed (lm mu)
               {
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3->lm &&
                          current_allowed_su3->mu == current_su3->mu)) {
                        wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
                     }
                  }
               } else  //	if array of allowed SU(3) irreps is empty ==> include ALL SU(3)
                       // irreps listed in wpn_su3
               {
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
                  }
               }
            }

            wpn_container_indices.resize(0);  // store indices pointing to wpn_irreps_container_ so
                                              // we don't have to perform search
            for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
               uint32_t index = wpn_irreps_container_.getIndexInTable(wpn_su3xsu2[i]);
               dims_[idiag_current] +=
                   ap_max * an_max * wpn_su3xsu2[i].rho * wpn_irreps_container_[index].dim();
               wpn_container_indices.push_back(index);
            }

            if (idiag_ == idiag_current)  // the wpn irreps stored in wpn_su3xsu2 will be stored
            {
               first_state_in_block_.push_back(number_of_states);

               for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
                  uint16_t dim = wpn_irreps_container_[wpn_container_indices[i]].dim();
                  if (dim > 0) {
                     wpn_.push_back(wpn_container_indices[i]);
                     number_of_states += ap_max * an_max * wpn_su3xsu2[i].rho * dim;
                  }
               }
               uint16_t nstates_in_block = number_of_states - first_state_in_block_.back();
               if (nstates_in_block > max_states_in_block_) {
                  max_states_in_block_ = nstates_in_block;
               }
               pnbasis_ipin_.push_back(index_p);
               pnbasis_ipin_.push_back(index_n);
               block_end_.push_back(wpn_.size());
            }
            ipin_pair++;
         }
      }
   }

#ifdef DEV_TIME_LOGS
   std::cout << "CncsmSU3xSU2Basis::Reshuffle runtime: " << cyan << omp_get_wtime() - start << reset
             << " [s]" << std::endl;
#endif

#ifdef DEVEL_LOGS
   std::cout << red << "idiag_: " << reset << idiag_ << std::endl;
   std::cout << red << "ndiag_: " << reset << ndiag_ << std::endl;
   std::cout << red << "max_states_in_block_: " << reset << max_states_in_block_ << std::endl;

   std::cout << red << "dims_[" << dims_.size() << "]: " << reset;
   for (long i = 0; i < dims_.size(); i++) std::cout << dims_[i] << ", ";
   std::cout << std::endl;

   std::cout << red << "pnbasis_ipin_[" << pnbasis_ipin_.size() << "]: " << reset;
   for (long i = 0; i < pnbasis_ipin_.size() / 2; i += 2)
      if ((i < 20) || (i > pnbasis_ipin_.size() / 2 - 20))
         std::cout << "(" << pnbasis_ipin_[2 * i] << " " << pnbasis_ipin_[2 * i + 1] << "), ";
      else if (i == 20)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "wpn_[" << wpn_.size() << "]: " << reset;
   for (long i = 0; i < wpn_.size(); i++)
      if ((i < 10) || (i > wpn_.size() - 10))
         std::cout << wpn_[i] << ", ";
      else if (i == 10)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "first_state_in_block_[" << first_state_in_block_.size() << "]: " << reset;
   for (long i = 0; i < first_state_in_block_.size(); i++)
      if ((i < 10) || (i > first_state_in_block_.size() - 10))
         std::cout << first_state_in_block_[i] << ", ";
      else if (i == 10)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "block_end_[" << block_end_.size() << "]: " << reset;
   for (long i = 0; i < block_end_.size(); i++)
      if ((i < 10) || (i > block_end_.size() - 10))
         std::cout << block_end_[i] << ", ";
      else if (i == 10)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "wpn_irreps_container_[]: " << reset;
   std::cout << "size = " << wpn_irreps_container_.getSize() << std::endl;
// std::cout << red << "wpn_irreps_container_[0] first element: " << reset;
// wpn_irreps_container_[0].rewind();
// std::cout
//    << "rho = " << wpn_irreps_container_[0].rho
//    << ", lm = " << wpn_irreps_container_[0].lm
//    << ", mu = " << wpn_irreps_container_[0].mu
//    << ", L = " << wpn_irreps_container_[0].L()
//    << ", kmax = " << wpn_irreps_container_[0].kmax() << std::endl;
#endif
}

#else  // ALTERNATIVE_RESHUFFLE

// allowed spins and su3 thread-loacal cache helpers:
using S2_t = decltype(SU3xSU2::LABELS::S2);
using wpwn_S2_t = std::pair<S2_t, S2_t>;
using allowed_spins_su3_t = std::pair<SU2_VEC, std::vector<SU3_VEC>>;
using allowed_spins_su3_map_t = std::map<wpwn_S2_t, allowed_spins_su3_t>;

allowed_spins_su3_map_t::iterator allowed_spins_su3_map_get(
    allowed_spins_su3_map_t& map, const S2_t wp_S2, const S2_t wn_S2,
    const proton_neutron::ModelSpace::const_iterator& nhwSubspace, SU2_VEC& allowed_spins,
    std::vector<SU3_VEC>& allowed_su3) {
   auto iter = map.find(std::make_pair(wp_S2, wn_S2));
   if (iter == map.end()) {
      allowed_spins.clear();
      allowed_su3.clear();
      nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(wp_S2, wn_S2), allowed_spins, allowed_su3);
      iter = (map.insert(std::make_pair(
                  std::make_pair(wp_S2, wn_S2),                                     // key
                  std::make_pair(std::move(allowed_spins), std::move(allowed_su3))  // value
                  )))
                 .first;
   }
   return iter;
}

// couple thread-local cache helpers:
using wpwn_t = std::pair<SU3xSU2::LABELS, SU3xSU2::LABELS>;
using couple_cache_t = std::map<wpwn_t, SU3_VEC>;
couple_cache_t::iterator couple_cache_get(couple_cache_t& map, const SU3xSU2::LABELS& wp,
                                          const SU3xSU2::LABELS& wn, SU3_VEC& wpn_su3) {
   auto iter = map.find(std::make_pair(wp, wn));
   if (iter == map.end()) {
      wpn_su3.clear();
      SU3::Couple(wp, wn, wpn_su3);
      iter = (map.insert(std::make_pair(std::make_pair(wp, wn), std::move(wpn_su3)))).first;
   }
   return iter;
}

// shifted parallel prefix sum; need to be called inside a parallel region
template <typename U>
void parallel_prefix_sum(std::vector<U>& input, std::vector<U>& output) {
   const long n = input.size();
   const int t = omp_get_thread_num();
   const int T = omp_get_num_threads();

   static std::vector<long> pp_sum_temp;  // shared by vectors

// resets output vector
#pragma omp single
   {
      output.clear();
      output.resize(n, 0);
   }

#pragma omp for schedule(static)
   for (long i = 1; i < n; i++) output[i] = input[i - 1];

#pragma omp single
   {
      pp_sum_temp.clear();
      pp_sum_temp.resize(T + 1, 0);
   }

   long sum = 0;
#pragma omp for schedule(static)
   for (long i = 0; i < n; i++) {
      sum += output[i];
      output[i] = sum;
   }
   pp_sum_temp[t + 1] = sum;

#pragma omp barrier

   long offset = 0;
   for (int i = 0; i < (t + 1); i++) offset += pp_sum_temp[i];

#pragma omp for schedule(static)
   for (long i = 0; i < n; i++) output[i] += offset;

// additionally releases memory of input vector
#pragma omp single
   {
      input.clear();
      input.shrink_to_fit();
   }
}

// Diagonal: function takes two parameters: int my_rank, int ndiag, and returns bool.
// if false ==> my_rank receives dims_ data during MPI_Allreduce
// if true ==> my_rank sends dims_ data during MPI_Allreduce
void CncsmSU3xSU2Basis::Reshuffle(const proton_neutron::ModelSpace& ncsmModelSpace,
                                  const uint16_t idiag, const uint16_t ndiag,
                                  std::function<bool(int, int)> Diagonal, MPI_Comm comm) {
#ifdef DEV_TIME_LOGS
   double start = omp_get_wtime();
#endif

   // SETUP PHASE: BEGIN

   idiag_ = idiag;
   ndiag_ = ndiag;

   // reset class variables and data structures
   max_states_in_block_ = 0;

   dims_.clear();
   dims_.resize(ndiag, 0);

   pnbasis_ipin_.clear();
   first_state_in_block_.clear();
   block_end_.clear();
   wpn_.clear();

   // prepare nhw subspaces for possible Nhw <= Nmax
   std::vector<proton_neutron::ModelSpace::const_iterator> nhwSubspaces(Nmax() + 1);
   for (int Nhw = 0; Nhw <= Nmax(); Nhw++)
      nhwSubspaces[Nhw] = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);

   // Nhw caches
   std::vector<uint8_t> nhw_p_cache(pconfs_.size());
#pragma omp parallel for
   for (int index_p = 0; index_p < pconfs_.size(); ++index_p) nhw_p_cache[index_p] = nhw_p(index_p);

   std::vector<uint8_t> nhw_n_cache(nconfs_.size());
#pragma omp parallel for
   for (int index_n = 0; index_n < nconfs_.size(); ++index_n) nhw_n_cache[index_n] = nhw_n(index_n);

   // wp and wn caches
   std::vector<SU3xSU2::LABELS> wp_cache(pconfs_.size());
#pragma omp parallel for
   for (int index_p = 0; index_p < pconfs_.size(); ++index_p)
      wp_cache[index_p] = getProtonSU3xSU2(index_p);

   std::vector<SU3xSU2::LABELS> wn_cache(nconfs_.size());
#pragma omp parallel for
   for (int index_n = 0; index_n < nconfs_.size(); ++index_n)
      wn_cache[index_n] = getNeutronSU3xSU2(index_n);

   // temporaries
   std::vector<int> ins_for_ip(pconfs_.size());
   std::vector<int> start_pos_for_ip;

   std::vector<int> local_ins_for_ip(pconfs_.size());
   std::vector<int> local_start_pos_for_ip;

   std::vector<long> wpns_for_ip(pconfs_.size(), 0);  // must be zeroed out
   std::vector<long> wpns_start;

   std::vector<int> n_states_in_block_for_ip(pconfs_.size(), 0);  // must be zeroed out
   std::vector<int> n_states_in_block_start(pconfs_.size());

   long local_ipins = 0;
   long local_wpns = 0;

   long max_states_in_block = 0;

// SETUP PHASE: END

#pragma omp parallel
   {
      // thread-local allowed spins and su3 thread-loacal cache:
      std::vector<allowed_spins_su3_map_t> allowed_spins_su3_cache(Nmax() + 1);

      // thread-local temporaries: declared here because of memory allocation efficiency
      SU2_VEC allowed_spins_temp;
      std::vector<SU3_VEC> allowed_su3_temp;

// FIRST ITERATION PHASE: BEGIN
#ifdef DEV_TIME_LOGS
      double start_first_iter = omp_get_wtime();
#endif

#pragma omp for schedule(dynamic, 16)
      for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
         const int Nphw = nhw_p_cache[index_p];
         const auto& wp = wp_cache[index_p];

         int ins_for_ip_temp = 0;

         for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
            int Nhw = Nphw + nhw_n_cache[index_n];
            if (Nhw > Nmax()) break;

            auto nhwSubspace = nhwSubspaces[Nhw];
            if (nhwSubspace == ncsmModelSpace.end()) continue;

            const auto& wn = wn_cache[index_n];

            auto iter =
                allowed_spins_su3_map_get(allowed_spins_su3_cache[Nhw], wp.S2, wn.S2, nhwSubspace,
                                          allowed_spins_temp, allowed_su3_temp);

            const SU2_VEC& allowed_spins = iter->second.first;
            if (allowed_spins.size() == 0) continue;

            ins_for_ip_temp++;
         }
         ins_for_ip[index_p] = ins_for_ip_temp;
      }

      parallel_prefix_sum(ins_for_ip, start_pos_for_ip);

#ifdef DEV_TIME_LOGS
#pragma omp barrier
#pragma omp single
      std::cout << "CncsmSU3xSU2Basis::Reshuffle first phase runtime: " << cyan
                << omp_get_wtime() - start_first_iter << reset << " [s]" << std::endl;
#endif
// FIRST ITERATION PHASE: END

// SECOND ITERATION PHASE: BEGIN
#ifdef DEV_TIME_LOGS
      double start_second_iter = omp_get_wtime();
#endif

      // couple cache
      couple_cache_t couple_cache;

      // thread-private for memory-efficiency:
      SU3_VEC wpn_su3_temp;

      SU3xSU2_VEC wpn_su3xsu2;
      wpn_su3xsu2.reserve(1024);

      std::vector<uint32_t> wpn_container_indices;
      wpn_container_indices.reserve(1024);

      std::vector<uint32_t> dims(ndiag, 0);

// find out number of valid PROCESS-LOCAL (ip,in) pairs for each ip:
#pragma omp for schedule(dynamic, 16) reduction(max : max_states_in_block) \
                             reduction(+ : local_ipins, local_wpns)
      for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
         const int Nphw = nhw_p_cache[index_p];
         const auto& wp = wp_cache[index_p];
         int ap_max = getMult_p(index_p);

         int ipin_pair = start_pos_for_ip[index_p];  // pair number in global ordering
         int local_ins_for_ip_temp = 0;

         for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
            int Nhw = Nphw + nhw_n_cache[index_n];
            if (Nhw > Nmax()) break;

            auto nhwSubspace = nhwSubspaces[Nhw];
            if (nhwSubspace == ncsmModelSpace.end()) continue;

            const auto& wn = wn_cache[index_n];
            int an_max = getMult_n(index_n);

            auto iter =
                allowed_spins_su3_map_get(allowed_spins_su3_cache[Nhw], wp.S2, wn.S2, nhwSubspace,
                                          allowed_spins_temp, allowed_su3_temp);
            const SU2_VEC& allowed_spins = iter->second.first;
            const std::vector<SU3_VEC>& allowed_su3 = iter->second.second;

            uint32_t nwpn_spin = allowed_spins.size();
            if (nwpn_spin == 0) continue;

            int idiag_current =
                (ipin_pair %
                 ndiag_);  // current (ip, in) pair belongs to idiag_current section of basis
            ipin_pair++;

            if (idiag_ != idiag_current) continue;
            // current (ip, in) pair belongs to current process from now on:
            local_ins_for_ip_temp++;

            auto iter2 = couple_cache_get(couple_cache, wp, wn, wpn_su3_temp);
            const auto& wpn_su3 = iter2->second;

            SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();

            // wpn_su3xsu2 computation:
            wpn_su3xsu2.clear();
            //	iterate over allowed spins and create vector of allowed SU3xSU2 irreps
            for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin) {
               if (!allowed_su3[ispin].empty()) {  //	==> I need to select only allowed (lm mu)
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3->lm &&
                          current_allowed_su3->mu == current_su3->mu))
                        wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
                  }
               } else {  //	if array of allowed SU(3) irreps is empty ==> include ALL SU(3)
                         //irreps listed in wpn_su3
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3)
                     wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
               }
            }

            // wpn_container_indiceis computation:
            wpn_container_indices.clear();
            for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
               uint32_t index = wpn_irreps_container_.getIndexInTable(
                   wpn_su3xsu2[i]);  // OPENMP: should be read-only
               dims[idiag_current] +=
                   ap_max * an_max * wpn_su3xsu2[i].rho *
                   wpn_irreps_container_[index].dim();  // OPENMP: should be read-only
               wpn_container_indices.push_back(index);
            }

            // data structures updates:
            uint32_t n_states_in_block = 0;

            for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
               uint16_t dim = wpn_irreps_container_[wpn_container_indices[i]]
                                  .dim();  // OPENMP: should be read-only
               if (dim > 0) {
                  wpns_for_ip[index_p]++;
                  n_states_in_block += ap_max * an_max * wpn_su3xsu2[i].rho * dim;
               }
            }

            if (n_states_in_block > max_states_in_block) max_states_in_block = n_states_in_block;
            n_states_in_block_for_ip[index_p] += n_states_in_block;
         }
         local_ins_for_ip[index_p] = local_ins_for_ip_temp;

         local_ipins += local_ins_for_ip_temp;
         local_wpns += wpns_for_ip[index_p];
      }

#pragma omp single
      max_states_in_block_ = max_states_in_block;

#pragma omp atomic update
      dims_[idiag_] += dims[idiag_];  // NEED TO BE REDUCED ACROSS MPI PROCESSES !!!

      parallel_prefix_sum(local_ins_for_ip, local_start_pos_for_ip);
      parallel_prefix_sum(wpns_for_ip, wpns_start);
      parallel_prefix_sum(n_states_in_block_for_ip, n_states_in_block_start);

#ifdef DEV_TIME_LOGS
#pragma omp barrier
#pragma omp single
      std::cout << "CncsmSU3xSU2Basis::Reshuffle second phase runtime: " << cyan
                << omp_get_wtime() - start_second_iter << reset << " [s]" << std::endl;
#endif
// SECOND ITERATION PHASE: END

// THIRD ITERATION PHASE: BEGIN
#ifdef DEV_TIME_LOGS
      double start_third_iter = omp_get_wtime();
#endif

#pragma omp single
      {
         pnbasis_ipin_.resize(2 * local_ipins);
         first_state_in_block_.resize(local_ipins);
         block_end_.resize(local_ipins);
         wpn_.resize(local_wpns);
      }

#pragma omp for schedule(dynamic, 16)
      for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
         const int Nphw = nhw_p_cache[index_p];
         const auto& wp = wp_cache[index_p];
         int ap_max = getMult_p(index_p);

         int ipin_pair = start_pos_for_ip[index_p];              // pair number in global ordering
         int local_ipin_pair = local_start_pos_for_ip[index_p];  // pair number in local ordering

         long wpn_i = wpns_start[index_p];
         int number_of_states = n_states_in_block_start[index_p];

         for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
            int Nhw = Nphw + nhw_n_cache[index_n];
            if (Nhw > Nmax()) break;

            auto nhwSubspace = nhwSubspaces[Nhw];
            if (nhwSubspace == ncsmModelSpace.end()) continue;

            const auto& wn = wn_cache[index_n];
            int an_max = getMult_n(index_n);

            auto iter =
                allowed_spins_su3_map_get(allowed_spins_su3_cache[Nhw], wp.S2, wn.S2, nhwSubspace,
                                          allowed_spins_temp, allowed_su3_temp);
            const SU2_VEC& allowed_spins = iter->second.first;
            const std::vector<SU3_VEC>& allowed_su3 = iter->second.second;

            uint32_t nwpn_spin = allowed_spins.size();
            if (nwpn_spin == 0) continue;

            int idiag_current =
                (ipin_pair %
                 ndiag_);  // current (ip, in) pair belongs to idiag_current section of basis
            ipin_pair++;

            if (idiag_ != idiag_current) continue;
            // current (ip, in) pair belongs to current process from now on:

            pnbasis_ipin_[2 * local_ipin_pair] = index_p;
            pnbasis_ipin_[2 * local_ipin_pair + 1] = index_n;
            // local_ipin_pair++; // NOT HERE YET

            auto iter2 = couple_cache_get(couple_cache, wp, wn, wpn_su3_temp);
            const auto& wpn_su3 = iter2->second;

            SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();

            // wpn_su3xsu2 computation:
            wpn_su3xsu2.clear();
            //	iterate over allowed spins and create vector of allowed SU3xSU2 irreps
            for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin) {
               if (!allowed_su3[ispin].empty()) {  //	==> I need to select only allowed (lm mu)
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3->lm &&
                          current_allowed_su3->mu == current_su3->mu))
                        wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
                  }
               } else {  //	if array of allowed SU(3) irreps is empty ==> include ALL SU(3)
                         //irreps listed in wpn_su3
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3)
                     wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
               }
            }

            // wpn_container_indiceis computation:
            wpn_container_indices.clear();
            for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
               uint32_t index = wpn_irreps_container_.getIndexInTable(
                   wpn_su3xsu2[i]);  // OPENMP: should be read-only
               wpn_container_indices.push_back(index);
            }

            first_state_in_block_[local_ipin_pair] = number_of_states;

            for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
               uint16_t dim = wpn_irreps_container_[wpn_container_indices[i]]
                                  .dim();  // OPENMP: should be read-only
               if (dim > 0) {
                  wpn_[wpn_i++] = wpn_container_indices[i];
                  number_of_states += ap_max * an_max * wpn_su3xsu2[i].rho * dim;
               }
            }

            block_end_[local_ipin_pair] = wpn_i;

            local_ipin_pair++;
         }
      }

#ifdef DEV_TIME_LOGS
#pragma omp barrier
#pragma omp single
      std::cout << "CncsmSU3xSU2Basis::Reshuffle third phase runtime: " << cyan
                << omp_get_wtime() - start_third_iter << reset << " [s]" << std::endl;
#endif
      // THIRD ITERATION PHASE: END

   }  // end omp parallel

// FOURTH ITERATION PHASE: BEGIN
#ifdef DEV_TIME_LOGS
   double start_fourth_iter = omp_get_wtime();
#endif

   if (ndiag_ > 1) {
      int nprocs, my_rank;
      MPI_Comm_size(comm , &nprocs);
      MPI_Comm_rank(comm , &my_rank);

      if (nprocs < ndiag_)
         std::cerr << "WARNING: program is probably run in the simulation mode; if not, basis constructions won't be globally complete (nprocs < NDIAG)\n";

      if (nprocs > 1) {
         // assert(nprocs >= ndiag_);
         // If given process my_rank is not "diagonal".
         if (!Diagonal(my_rank, ndiag_))
            std::fill(dims_.begin(), dims_.end(), 0);

         static_assert(std::is_same<decltype(dims_)::value_type, uint32_t>::value,
               "Dims values type probably changed!");
         MPI_Allreduce(MPI_IN_PLACE, dims_.data(), ndiag_, MPI_UINT32_T, MPI_SUM, comm);
      }
   }

#ifdef DEV_TIME_LOGS
   std::cout << "CncsmSU3xSU2Basis::Reshuffle fourth phase runtime: " << cyan
             << omp_get_wtime() - start_fourth_iter << reset << " [s]" << std::endl;
#endif
// FOURTH ITERATION PHASE: END

#ifdef DEV_TIME_LOGS
   std::cout << "CncsmSU3xSU2Basis::Reshuffle runtime: " << cyan << omp_get_wtime() - start << reset
             << " [s]" << std::endl;
#endif

#ifdef DEVEL_LOGS
   std::cout << red << "idiag_: " << reset << idiag_ << std::endl;
   std::cout << red << "ndiag_: " << reset << ndiag_ << std::endl;
   std::cout << red << "max_states_in_block_: " << reset << max_states_in_block_ << std::endl;

   std::cout << red << "dims_[" << dims_.size() << "]: " << reset;
   for (long i = 0; i < dims_.size(); i++) std::cout << dims_[i] << ", ";
   std::cout << std::endl;

   std::cout << red << "pnbasis_ipin_[" << pnbasis_ipin_.size() << "]: " << reset;
   for (long i = 0; i < pnbasis_ipin_.size() / 2; i += 2)
      if ((i < 20) || (i > pnbasis_ipin_.size() / 2 - 20))
         std::cout << "(" << pnbasis_ipin_[2 * i] << " " << pnbasis_ipin_[2 * i + 1] << "), ";
      else if (i == 20)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "wpn_[" << wpn_.size() << "]: " << reset;
   for (long i = 0; i < wpn_.size(); i++)
      if ((i < 10) || (i > wpn_.size() - 10))
         std::cout << wpn_[i] << ", ";
      else if (i == 10)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "first_state_in_block_[" << first_state_in_block_.size() << "]: " << reset;
   for (long i = 0; i < first_state_in_block_.size(); i++)
      if ((i < 10) || (i > first_state_in_block_.size() - 10))
         std::cout << first_state_in_block_[i] << ", ";
      else if (i == 10)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "block_end_[" << block_end_.size() << "]: " << reset;
   for (long i = 0; i < block_end_.size(); i++)
      if ((i < 10) || (i > block_end_.size() - 10))
         std::cout << block_end_[i] << ", ";
      else if (i == 10)
         std::cout << " ... ";
   std::cout << std::endl;

   std::cout << red << "wpn_irreps_container_[]: " << reset;
   std::cout << "size = " << wpn_irreps_container_.getSize() << std::endl;
// std::cout << red << "wpn_irreps_container_[0] first element: " << reset;
// wpn_irreps_container_[0].rewind();
// std::cout
//    << "rho = " << wpn_irreps_container_[0].rho
//    << ", lm = " << wpn_irreps_container_[0].lm
//    << ", mu = " << wpn_irreps_container_[0].mu
//    << ", L = " << wpn_irreps_container_[0].L()
//    << ", kmax = " << wpn_irreps_container_[0].kmax() << std::endl;
#endif
}

#endif  // ALTERNATIVE_RESHUFFLE

// Load basis from a binary boost::archive::binary_iarchive input file
void CncsmSU3xSU2Basis::LoadBasis(const std::string& file_name) {
   //	open input file as a binary file
   std::ifstream basis_file(file_name, std::ios::binary);
   if (!basis_file) {
      std::cerr << "File '" << file_name << "' does not exist or could not be opened!" << std::endl;
      exit(EXIT_FAILURE);
   }
   //	read file with basis into a binary archive
   boost::archive::binary_iarchive input(basis_file);
   //	call CncsmSU3xSU2Basis::load to initialize
   //	data members from the binary archive
   input >> *this;
}

void CncsmSU3xSU2Basis::ConstructBasis(const proton_neutron::ModelSpace& ncsmModelSpace,
                                       const uint16_t idiag, const uint16_t ndiag, MPI_Comm comm, std::function<bool(int, int)> Diagonal) {
                                       
   idiag_ = idiag;
   ndiag_ = ndiag;
   dims_.clear();
   dims_.resize(ndiag, 0);
   min_ho_quanta_p_ = 255;
   min_ho_quanta_n_ = 255;
   JJ_ = ncsmModelSpace.JJ();
   nprotons_ = ncsmModelSpace.Z();
   nneutrons_ = ncsmModelSpace.N();

   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   CncsmSU3Basis basis(ncsmModelSpace);
   nmax_ = ncsmModelSpace.back().N();

   //	sorted vector of gammas; common for protons & neutrons
   std::vector<UN::SU3xSU2_VEC> gammas;
   //	sorted vector of inter-shell su3 coupling; common for protons & neutrons
   std::vector<SU3_VEC> omegas_su3;
   //	sorted vector of inter-shell spin coupling; common for protons & neutrons
   std::vector<SU2_VEC> omegas_spin;

   start = std::chrono::system_clock::now();

   GenerateDistrGammaOmega_SortedVectors(basis, distribs_p_, distribs_n_, gammas, omegas_su3,
                                         omegas_spin);

   duration = std::chrono::system_clock::now() - start;
   //	cout << "GenerateDistrGammaOmega_SortedVectors: ... " << duration.count() << endl;

   start = std::chrono::system_clock::now();

   std::vector<uint32_t> gamma_positions(gammas.size(), 0),
       omega_spin_positions(omegas_spin.size(), 0);
   std::vector<uint32_t> omega_su3_positions(omegas_su3.size(), 0);

   //	Suppose that vector of gamma vectors, vector of omega_su3 vectors, and
   //	vector of omega_spin vectors are stored in continuous arrays.

   //	Here we calculate position of a first element of each gamma vector
   for (int i = 1; i < gammas.size(); ++i) {
      gamma_positions[i] = gamma_positions[i - 1] + gammas[i - 1].size();
      assert(gamma_positions[i] >=
             gamma_positions[i - 1]);  // FALSE ==> uint16_t needs to be replaced by uint32_t
   }
   //	Here we calculate position of a first element of each omegas_su3 vector
   for (int i = 1; i < omegas_su3.size(); ++i) {
      omega_su3_positions[i] = omega_su3_positions[i - 1] + omegas_su3[i - 1].size();
   }
   //	Here we calculate position of a first element of each omegas_spin vector
   for (int i = 1; i < omegas_spin.size(); ++i) {
      omega_spin_positions[i] = omega_spin_positions[i - 1] + omegas_spin[i - 1].size();
      assert(omega_spin_positions[i] >=
             omega_spin_positions[i - 1]);  // FALSE ==> uint16_t needs to be replaced by uint32_t
   }

   occupied_shells_distr_p_.resize(distribs_p_.size());
   // loop over proton distributions and count the number of occupied shells for each distribution
   // and also calculate minimal number of HO quanta for proton distributions
   for (int i = 0; i < distribs_p_.size(); ++i) {
      uint8_t num_quanta(0);
      for (int j = 0; j < distribs_p_[i].size(); ++j) {
         num_quanta += distribs_p_[i][j] * j;
      }
      if (num_quanta < min_ho_quanta_p_) {
         std::swap(num_quanta, min_ho_quanta_p_);
      }
      occupied_shells_distr_p_[i] =
          std::count_if(distribs_p_[i].begin(), distribs_p_[i].end(),
                        std::bind2nd(std::greater<uint8_t>(), (uint8_t)0));
   }

   occupied_shells_distr_n_.resize(distribs_n_.size());
   // loop over proton distributions and count the number of occupied shells for each distribution
   // and also calculate minimal number of HO quanta for neutron distributions
   for (int i = 0; i < distribs_n_.size(); ++i) {
      uint8_t num_quanta(0);
      for (int j = 0; j < distribs_n_[i].size(); ++j) {
         num_quanta += distribs_n_[i][j] * j;
      }
      if (num_quanta < min_ho_quanta_n_) {
         std::swap(num_quanta, min_ho_quanta_n_);
      }
      occupied_shells_distr_n_[i] =
          std::count_if(distribs_n_[i].begin(), distribs_n_[i].end(),
                        std::bind2nd(std::greater<uint8_t>(), (uint8_t)0));
   }

   //	variables needed for construction of pconfs_ and nconfs_ vectors
   SingleDistributionSmallVector distr;
   UN::SU3xSU2_VEC gamma;
   SU3xSU2_SMALL_VEC omega;
   uint32_t iw_su3, iw_spin, igamma;
   NUCLEON_BASIS_INDICES new_index_p, new_index_n;
   //	Constructs elements of pconfs_ vector
   //	Elements are pointers to
   //	[0] = dp
   //	[1] = gamma
   //	[2] = omega_su3
   //	[3] = omega_spin
   for (IdentFermConfIterator protonConfIter = basis.firstProtonConf(); protonConfIter.hasDistr();
        protonConfIter.nextDistr()) {
      distr.resize(0);
      protonConfIter.getDistr(distr);
      cpp0x::get<kDistr>(new_index_p) = std::distance(
          distribs_p_.begin(), std::lower_bound(distribs_p_.begin(), distribs_p_.end(), distr));

      for (protonConfIter.rewindGamma(); protonConfIter.hasGamma(); protonConfIter.nextGamma()) {
         gamma.resize(0);
         protonConfIter.getGamma(gamma);
         igamma = std::distance(gammas.begin(), std::lower_bound(gammas.begin(), gammas.end(),
                                                                 gamma, Compare_UNSU3xSU2_VEC()));

         cpp0x::get<kGamma>(new_index_p) = gamma_positions[igamma];

         for (protonConfIter.rewindOmega(); protonConfIter.hasOmega(); protonConfIter.nextOmega()) {
            omega.resize(0);
            protonConfIter.getOmega(omega);

            iw_su3 = std::distance(
                omegas_su3.begin(),
                std::lower_bound(omegas_su3.begin(), omegas_su3.end(),
                                 SU3_VEC(omega.begin(), omega.end()), Compare_SU3_VEC()));
            cpp0x::get<kOmegaSu3>(new_index_p) = omega_su3_positions[iw_su3];

            iw_spin = std::distance(omegas_spin.begin(),
                                    std::lower_bound(omegas_spin.begin(), omegas_spin.end(),
                                                     SU2_VEC(omega.begin(), omega.end())));
            cpp0x::get<kOmegaSpin>(new_index_p) = omega_spin_positions[iw_spin];

            pconfs_.push_back(new_index_p);
         }
      }
   }

   //	This loop fills nconfs_ vector with pointers to
   //	[0] = dp
   //	[1] = gamma
   //	[2] = omega_su3
   //	[3] = omega_spin
   for (IdentFermConfIterator neutronConfIter = basis.firstNeutronConf();
        neutronConfIter.hasDistr(); neutronConfIter.nextDistr()) {
      distr.resize(0);
      neutronConfIter.getDistr(distr);
      cpp0x::get<kDistr>(new_index_n) = std::distance(
          distribs_n_.begin(), std::lower_bound(distribs_n_.begin(), distribs_n_.end(), distr));

      for (neutronConfIter.rewindGamma(); neutronConfIter.hasGamma(); neutronConfIter.nextGamma()) {
         gamma.resize(0);
         neutronConfIter.getGamma(gamma);
         igamma = std::distance(gammas.begin(), std::lower_bound(gammas.begin(), gammas.end(),
                                                                 gamma, Compare_UNSU3xSU2_VEC()));

         cpp0x::get<kGamma>(new_index_n) = gamma_positions[igamma];

         for (neutronConfIter.rewindOmega(); neutronConfIter.hasOmega();
              neutronConfIter.nextOmega()) {
            omega.resize(0);
            neutronConfIter.getOmega(omega);

            iw_su3 = std::distance(
                omegas_su3.begin(),
                std::lower_bound(omegas_su3.begin(), omegas_su3.end(),
                                 SU3_VEC(omega.begin(), omega.end()), Compare_SU3_VEC()));
            cpp0x::get<kOmegaSu3>(new_index_n) = omega_su3_positions[iw_su3];

            iw_spin = std::distance(omegas_spin.begin(),
                                    std::lower_bound(omegas_spin.begin(), omegas_spin.end(),
                                                     SU2_VEC(omega.begin(), omega.end())));
            cpp0x::get<kOmegaSpin>(new_index_n) = omega_spin_positions[iw_spin];

            nconfs_.push_back(new_index_n);
         }
      }
   }

   gammas_.reserve(gamma_positions.back() + gammas.back().size());
   for (int i = 0; i < gammas.size(); ++i) {
      gammas_.insert(gammas_.end(), gammas[i].begin(), gammas[i].end());
   }

   omegas_su3_.reserve(omega_su3_positions.back() + omegas_su3.back().size());
   for (int i = 0; i < omegas_su3.size(); ++i) {
      omegas_su3_.insert(omegas_su3_.end(), omegas_su3[i].begin(), omegas_su3[i].end());
   }

   omegas_spin_.reserve(omega_spin_positions.back() + omegas_spin.back().size());
   for (int i = 0; i < omegas_spin.size(); ++i) {
      omegas_spin_.insert(omegas_spin_.end(), omegas_spin[i].begin(), omegas_spin[i].end());
   }

   duration = std::chrono::system_clock::now() - start;
   //	cout << "Rest of stuff: " << duration.count() << endl;

   //	now when both pconfs_ and nconfs_ have been generated we could generate list of final
   // SU3xSU2 irreps
   start = std::chrono::system_clock::now();
   GenerateFinal_SU3xSU2Irreps_Container(ncsmModelSpace);
   duration = std::chrono::system_clock::now() - start;
   //	cout << "GenerateFinal_SU3xSU2Irreps_Container: " << duration.count() << endl;

   start = std::chrono::system_clock::now();
   Reshuffle(ncsmModelSpace, idiag, ndiag, Diagonal, comm);
   duration = std::chrono::system_clock::now() - start;
   //	cout << "Reshuffle: " << duration.count << endl;
}

void CncsmSU3xSU2Basis::ConstructBasis(const proton_neutron::ModelSpace& ncsmModelSpace,
                                       const CncsmSU3xSU2Basis& basis, const uint16_t idiag,
                                       const uint16_t ndiag, 
                                       MPI_Comm comm,
                                       std::function<bool(int, int)> Diagonal) {
   wpn_irreps_container_.CreateContainer(basis.wpn_irreps_container_);
   JJ_ = ncsmModelSpace.JJ();
   nprotons_ = ncsmModelSpace.Z();
   nneutrons_ = ncsmModelSpace.N();

   min_ho_quanta_p_ = basis.min_ho_quanta_p_;
   min_ho_quanta_n_ = basis.min_ho_quanta_n_;
   nmax_ = basis.nmax_;

   // occupied_shells_distr_p_[i] = the number of occupied shells for ith proton distribution
   occupied_shells_distr_p_ = basis.occupied_shells_distr_p_;
   occupied_shells_distr_n_ = basis.occupied_shells_distr_n_;

   //	arrays storing quantum labels required for a complete definition of
   //	proton/neutron configurations in a given model space
   distribs_p_ = basis.distribs_p_;
   distribs_n_ = basis.distribs_n_;
   gammas_ = basis.gammas_;
   omegas_su3_ = basis.omegas_su3_;
   omegas_spin_ = basis.omegas_spin_;
   pconfs_ = basis.pconfs_;
   nconfs_ = basis.nconfs_;

   Reshuffle(ncsmModelSpace, idiag, ndiag, Diagonal, comm);  // takes care of idiag_ ndiag_ and dims_
}

//	Assign a set of proton configurations to an MPI process based on Nmax and number of
// processes.
//	This defines a set of proton configurations that are utilized to
//	generate all possible proton-neutron configurations.
//
//	Input:
//	Nmax:   many-body basis cutoff
//	my_rank: MPI process id
//	nprocs: number of processes
//	numberStates_Nphw: array with Nmax+1 elements each storing number of proton configurations
// with Nhw HO excitation quanta
//
//	Output:
//	index_p_first: index identifying the first proton configuration assigned.
//	index_p_end:   boundary of proton configurations assigned (index_p < index_p_end)
//
//	Strategies:
//	1) if (number of processors < Nmax + 1) ==> processes split workload evenly based on number
// of proton configurations
//	This yields quite poor load balancing as states with different value of may differ by
// several order of magnitude for different Nhw
//	value
//
//	2) if (number of processors >= Nmax + 1) ==> for each Nhw subspace, workload is split
// between (almost, i.e. differ by at most one) equal number of processes.
// 	The 0hw subspace is an exception for being exclusively assigned to the process with
// my_rank=0.
void GetWorkload(const int Nmax, const int my_rank, const int nprocs, const int* numberStates_Nphw,
                 int& index_p_first, int& index_p_end) {
   if (nprocs < Nmax + 1) {
      if (my_rank == 0) {
         std::cout << "Warning: the number of processors (" << nprocs
                   << ") one uses for a basis construction should be at least equal to (Nmax+1):"
                   << Nmax + 1 << std::endl;
         std::cout << "Construction may not be well load balanced!" << std::endl;
      }
      //	Compute total number of proton configurations
      uint32_t nstates = std::accumulate(numberStates_Nphw, numberStates_Nphw + Nmax + 1, 0);
      //	number of proton configurations per process
      int pprocs = nstates / nprocs;
      //	end this is the remainder
      int mod = nstates % nprocs;

      //	First 'mod' process take care of (pprocs + 1) proton configurations
      int npindices = pprocs + (my_rank < mod);
      //	process with my_rank is responsible for proton configurations starting from
      // index_p_first up to index_p_end
      index_p_first =
          (my_rank < mod) ? my_rank * (pprocs + 1) : mod * (pprocs + 1) + (my_rank - mod) * pprocs;
      index_p_end = index_p_first + npindices;
   } else {
      int remaining_processes = (nprocs - 1) % Nmax;
      //	Nphw = 0hw is done by a single process
      //	Process with id:0 is responsible for 0hw space
      int nprocs_perNphw = 1;
      int firstRank = 0;
      int nhw;
      for (nhw = 0; nhw <= Nmax; ++nhw) {
         //			the rank of the first process responsible for the next [i.e. nhw+1]
         // subspace
         int next_firstRank = firstRank + nprocs_perNphw;

         if (my_rank <
             next_firstRank)  // ==> this process is responsible for the current nhw subspace
         {
            break;
         }
         //	rank of the first process  responsible for nhw subspace
         firstRank = next_firstRank;
         //	number of processes that are responsible for nhw subspace
         nprocs_perNphw = (nprocs - 1) / Nmax + (remaining_processes > 0);
         remaining_processes--;
      }
      //		at this moment we have process my_rank responsible for nhw subspace. There
      // are nprocs_perNphw processes
      //		responsible for this subspace, starting with process my_rank=firstRank

      uint32_t numberOfStates = numberStates_Nphw[nhw];
      //	rank the process in the group assigned to nhw subspace
      int32_t my_group_rank = my_rank - firstRank;

      //	number of proton configurations per process
      int pprocs = numberOfStates / nprocs_perNphw;
      //	end this is the remainder
      int mod = numberOfStates % nprocs_perNphw;

      //	First 'mod' processes in the nhw process group take care of (pprocs + 1) proton
      // configurations
      //	all the remaining processes are assigned  pprocs configurations.
      int npindices = pprocs + (my_group_rank < mod);
      //	Sum the number of proton configurations spanning 0hw, 1hw, 2hw, ..., (n-1)hw
      // subspaces
      //	to compute the position of the first proton configuration with nhw HO excitation
      // energy
      index_p_first = std::accumulate(numberStates_Nphw, numberStates_Nphw + nhw, 0);
      //	if my rank is nhw group is less than the number of remaining states
      if (my_group_rank < mod) {
         // 			all states up to me were assigned one additional state
         index_p_first += my_group_rank * (pprocs + 1);
      } else {
         // 			compute position of the first proton state for a
         // 			process with group rank beyond the number of unassigned
         // 			states
         index_p_first += mod * (pprocs + 1) + (my_group_rank - mod) * pprocs;
      }
      index_p_end = index_p_first + npindices;
   }
}

//	Use a basis of an arbitrary segment of a model space and employ MPI processes that make up
// MPI communicator
//	mpi_comm to construct basis as a single segment, i.e. ndiag_=1 and idiag_=0 set of
// parameters.
void CncsmSU3xSU2Basis::ConstructFullBasis(const proton_neutron::ModelSpace& ncsmModelSpace,
                                           const CncsmSU3xSU2Basis& basis, MPI_Comm mpi_comm) {
   //	S T E P # 1
   //	Copy common data from provided basis segment
   //	Rationale:
   //	All segments of basis stored in CncsmSU3xSU2Basis store the same proton and
   //	neutron configurations data.
   //
   //	Copy data for proton and neutron configurations from CncsmSU3xSU2Basis& basis
   wpn_irreps_container_.CreateContainer(basis.wpn_irreps_container_);
   JJ_ = ncsmModelSpace.JJ();
   nprotons_ = ncsmModelSpace.Z();
   nneutrons_ = ncsmModelSpace.N();
   //	Set a single segment parameters
   idiag_ = 0;
   ndiag_ = 1;
   dims_.resize(1);  // there is only one segment!
   dims_[0] = 0;

   min_ho_quanta_p_ = basis.min_ho_quanta_p_;
   min_ho_quanta_n_ = basis.min_ho_quanta_n_;
   nmax_ = basis.nmax_;

   // occupied_shells_distr_p_[i] = the number of occupied shells for ith proton distribution
   occupied_shells_distr_p_ = basis.occupied_shells_distr_p_;
   occupied_shells_distr_n_ = basis.occupied_shells_distr_n_;

   //	arrays storing quantum labels required for a complete definition of
   //	proton/neutron configurations in a given model space
   distribs_p_ = basis.distribs_p_;
   distribs_n_ = basis.distribs_n_;
   gammas_ = basis.gammas_;
   omegas_su3_ = basis.omegas_su3_;
   omegas_spin_ = basis.omegas_spin_;
   pconfs_ = basis.pconfs_;
   nconfs_ = basis.nconfs_;
   //	S T E P # 2
   //	Reset clear structures for storing proton-neutron basis data
   pnbasis_ipin_.resize(0);
   wpn_.resize(0);
   first_state_in_block_.resize(0);
   block_end_.resize(0);
   //	S T E P # 3:
   //	Each process constructs proton-neutron data. Workload between
   //	processes is set by a certain number of proton configurations.
   int my_rank, nprocs;

   MPI_Comm_rank(mpi_comm, &my_rank);
   MPI_Comm_size(mpi_comm, &nprocs);

   //	Compute number of proton configurations at each nhw subspace
   std::vector<int> numberStates_Nphw(Nmax() + 1, 0);
   for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
      numberStates_Nphw[nhw_p(index_p)]++;
   }

   int index_p_first(0);
   int index_p_end(0);
   //	Compute workload
   GetWorkload(Nmax(), my_rank, nprocs, &numberStates_Nphw[0], index_p_first, index_p_end);

   uint32_t number_of_states_local(0);

   //	iterate over all possible (ip in) pairs and generate
   //	local arrays and data
   uint16_t max_states_in_block_local(0);
   std::vector<uint32_t> pnbasis_ipin_local;
   std::vector<uint32_t> first_state_in_block_local;
   std::vector<uint32_t> block_end_local;
   std::vector<uint16_t> wpn_local;

   //	data buffers used for a temporary storage
   SU3xSU2_VEC wpn_su3xsu2;
   wpn_su3xsu2.reserve(1024);

   SU3_VEC wpn_su3;
   wpn_su3.reserve(1024);

   SU2_VEC allowed_spins;
   allowed_spins.reserve(50);

   proton_neutron::ModelSpace::const_iterator nhwSubspace;

   std::chrono::system_clock::time_point start;
   if (my_rank == 0) {
      std::cout << "Constructiong basis from a segment using " << nprocs << " MPI processes."
                << std::endl;
      start = std::chrono::system_clock::now();
   }

   //	cout << "my_rank:" << my_rank << "\t first index_p:" << index_p_first << endl;
   for (int index_p = index_p_first; index_p < index_p_end; ++index_p)
   //	for (int index_p = 0; index_p < pconfs_.size(); ++index_p)
   {
      int Nphw = nhw_p(index_p);
      int ap_max = getMult_p(index_p);
      SU3xSU2::LABELS wp(getProtonSU3xSU2(index_p));
      for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
         int Nhw = Nphw + nhw_n(index_n);
         if (Nhw > Nmax()) {
            break;  // fermionic irreps are stored in an increasing order according to the number of
                    // oscillator quanta
         }
         nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
         if (nhwSubspace ==
             ncsmModelSpace
                 .end())  // => given Nhw subspace does not belong to ncsmModelSpace model space
         {
            continue;
         }
         int an_max = getMult_n(index_n);
         SU3xSU2::LABELS wn(getNeutronSU3xSU2(index_n));

         allowed_spins.resize(0);
         std::vector<SU3_VEC> allowed_su3;
         //	for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
         //	return vector of allowed SU3 labels for each allowed spin:
         //	{{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
         nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(wp.S2, wn.S2), allowed_spins,
                                            allowed_su3);
         uint32_t nwpn_spin = allowed_spins.size();
         if (nwpn_spin)  // ==> (Sp Sn) are allowed
         {
            wpn_su3.resize(0);
            wpn_su3xsu2.resize(0);
            SU3::Couple(wp, wn, wpn_su3);
            SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();
            //	iterate over allowed spins and create vector of allowed SU3xSU2 irreps
            for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin) {
               if (!allowed_su3[ispin].empty())  //	==> I need to select only allowed (lm mu)
               {
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3->lm &&
                          current_allowed_su3->mu == current_su3->mu)) {
                        wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
                     }
                  }
               } else  //	if array of allowed SU(3) irreps is empty ==> include ALL SU(3)
                       // irreps listed in wpn_su3
               {
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
                  }
               }
            }

            first_state_in_block_local.push_back(number_of_states_local);
            for (int i = 0; i < wpn_su3xsu2.size(); ++i) {
               uint32_t index = wpn_irreps_container_.getIndexInTable(wpn_su3xsu2[i]);
               uint32_t dim = wpn_irreps_container_[index].dim();
               if (dim > 0) {
                  wpn_local.push_back(index);
                  number_of_states_local += ap_max * an_max * wpn_su3xsu2[i].rho * dim;
               }
            }

            uint16_t nstates_in_block = number_of_states_local - first_state_in_block_local.back();
            if (nstates_in_block > max_states_in_block_local) {
               max_states_in_block_local = nstates_in_block;
            }
            pnbasis_ipin_local.push_back(index_p);
            pnbasis_ipin_local.push_back(index_n);
            // Note that block_end_local stores only values in local
            block_end_local.push_back(wpn_local.size());
         }
      }
   }

   //	std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
   //	cout << "my_rank:" << my_rank << " Resulting time: " << duration.count() << endl;

   MPI_Barrier(mpi_comm);

   if (my_rank == 0) {
      std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
      cout << "Construction took " << duration.count() << " seconds." << endl;
      std::cout << "Gathering, creating and coppying data ... ";
      start = std::chrono::system_clock::now();
   }
   ///////////////////////////////////////////////////////////
   //	uint16_t max_states_in_block_local(0) & dims_[0]
   ///////////////////////////////////////////////////////////
   MPI_Allreduce((void*)&max_states_in_block_local, (void*)&max_states_in_block_, 1,
                 MPI_UNSIGNED_SHORT, MPI_MAX, mpi_comm);
   MPI_Allreduce((void*)&number_of_states_local, (void*)&dims_[0], 1, MPI_UNSIGNED, MPI_SUM,
                 mpi_comm);
   ///////////////////////////////////////////////////////////
   //	std::vector<decltype(pnbasis_ipin_)::value_type> pnbasis_ipin_local;
   ///////////////////////////////////////////////////////////
   uint32_t size_local = pnbasis_ipin_local.size();
   int32_t recvcnts[nprocs];
   int32_t displ[nprocs];

   MPI_Allgather((void*)&size_local, 1, MPI_UNSIGNED, recvcnts, 1, MPI_UNSIGNED, mpi_comm);

   uint32_t size_total = 0;
   for (int i = 0; i < nprocs; ++i) {
      displ[i] = size_total;
      size_total += recvcnts[i];
   }

   pnbasis_ipin_.resize(size_total);
   MPI_Allgatherv((void*)&pnbasis_ipin_local[0], size_local, MPI_UNSIGNED, (void*)&pnbasis_ipin_[0],
                  recvcnts, displ, MPI_UNSIGNED, mpi_comm);
   ///////////////////////////////////////////////////////////
   //	std::vector<decltype(wpn_)::value_type> wpn_local;
   ///////////////////////////////////////////////////////////
   size_local = wpn_local.size();
   MPI_Allgather((void*)&size_local, 1, MPI_UNSIGNED, recvcnts, 1, MPI_UNSIGNED, mpi_comm);
   size_total = 0;
   //	cout << "my_rank: " << my_rank << " wpn_local.size():" << size_local << endl;
   for (int i = 0; i < nprocs; ++i) {
      displ[i] = size_total;
      size_total += recvcnts[i];
   }

   wpn_.resize(size_total);
   MPI_Allgatherv((void*)&wpn_local[0], size_local, MPI_UNSIGNED_SHORT, (void*)&wpn_[0], recvcnts,
                  displ, MPI_UNSIGNED_SHORT, mpi_comm);
   ///////////////////////////////////////////////////////////
   //	std::vector<decltype(block_end_)::value_type> block_end_local;
   ///////////////////////////////////////////////////////////
   //	block_end_local contains indices pointing to wpn_local
   //	We need to transform values stored in block_end_local into global values
   //	recvcnts[i] contains size of wpn_local for process with my_rank=i
   //	the last value stored in block_end_local for process with my_rank=i is equal to size of
   // wpn_local
   int32_t* nwpn_local = recvcnts;
   //	Transform values stored in block_end_local so that they point into global array wpn_
   //	Compute number of states stored in wpn_ by processes with rank 0 ... my_rank - 1
   uint32_t offset = 0;
   for (int i = 0; i < my_rank; ++i) {
      offset += nwpn_local[i];
   }
   //	States in wpn_local will be stored in wpn_ right after contributions due to processes 0 ...
   // my_rank-1
   //	Their position in wpn_ table will be equal to their local position plus	number of states
   // in wpn_ due to processes 0 ... my_rank-1
   for (int i = 0; i < block_end_local.size(); ++i) {
      block_end_local[i] += offset;
   }
   //	Gather block_end_local data into block_end_
   size_local = block_end_local.size();
   MPI_Allgather((void*)&size_local, 1, MPI_UNSIGNED, recvcnts, 1, MPI_UNSIGNED, mpi_comm);
   size_total = 0;
   for (int i = 0; i < nprocs; ++i) {
      displ[i] = size_total;
      size_total += recvcnts[i];
   }
   block_end_.resize(size_total);
   MPI_Allgatherv((void*)&block_end_local[0], size_local, MPI_UNSIGNED, (void*)&block_end_[0],
                  recvcnts, displ, MPI_UNSIGNED, mpi_comm);
   ///////////////////////////////////////////////////////////
   //	std::vector<decltype(first_state_in_block_)::value_type> first_state_in_block_local;
   ///////////////////////////////////////////////////////////
   //	We need to transform positions of blocks in local segment of basis processed by my_rank
   // process and stored in
   //	first_state_in_block_local into proper global values
   uint32_t nstates_per_rank[nprocs];
   MPI_Allgather((void*)&number_of_states_local, 1, MPI_UNSIGNED, (int*)nstates_per_rank, 1,
                 MPI_UNSIGNED, mpi_comm);
   //	Compute number of states done by processes 0 ... my_rank-1
   offset = 0;
   for (int i = 0; i < my_rank; ++i) {
      offset += nstates_per_rank[i];
   }
   //	Transform values in first_state_in_block_local into proper global values
   for (int i = 0; i < first_state_in_block_local.size(); ++i) {
      first_state_in_block_local[i] += offset;
   }
   //	Distribute content of first_state_in_block_local between processes
   //	so that at the end each process has identical copy of first_state_in_block_
   size_local = first_state_in_block_local.size();
   MPI_Allgather((void*)&size_local, 1, MPI_UNSIGNED, recvcnts, 1, MPI_UNSIGNED, mpi_comm);
   size_total = 0;
   for (int i = 0; i < nprocs; ++i) {
      displ[i] = size_total;
      size_total += recvcnts[i];
   }
   first_state_in_block_.resize(size_total);
   MPI_Allgatherv((void*)&first_state_in_block_local[0], size_local, MPI_UNSIGNED,
                  (void*)&first_state_in_block_[0], recvcnts, displ, MPI_UNSIGNED, mpi_comm);

   if (my_rank == 0) {
      std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
      std::cout << "Done in " << duration.count() << " seconds." << endl;
   }
}

CncsmSU3xSU2Basis& CncsmSU3xSU2Basis::operator=(const CncsmSU3xSU2Basis& rhs) {
   ndiag_ = rhs.ndiag_;
   idiag_ = rhs.idiag_;
   dims_ = rhs.dims_;
   JJ_ = rhs.JJ_;
   nprotons_ = rhs.nprotons_;
   nneutrons_ = rhs.nneutrons_;

   min_ho_quanta_p_ = rhs.min_ho_quanta_p_;
   min_ho_quanta_n_ = rhs.min_ho_quanta_n_;
   nmax_ = rhs.nmax_;
   // occupied_shells_distr_p_[i] = the number of occupied shells for ith proton distribution
   occupied_shells_distr_p_ = rhs.occupied_shells_distr_p_;
   occupied_shells_distr_n_ = rhs.occupied_shells_distr_n_;
   //	arrays storing quantum labels required for a complete definition of
   //	proton/neutron configurations in a given model space
   distribs_p_ = rhs.distribs_p_;
   distribs_n_ = rhs.distribs_n_;
   gammas_ = rhs.gammas_;
   omegas_su3_ = rhs.omegas_su3_;
   omegas_spin_ = rhs.omegas_spin_;
   pconfs_ = rhs.pconfs_;
   nconfs_ = rhs.nconfs_;

   pnbasis_ipin_ = rhs.pnbasis_ipin_;
   wpn_ = rhs.wpn_;
   first_state_in_block_ = rhs.first_state_in_block_;
   block_end_ = rhs.block_end_;
   max_states_in_block_ = rhs.max_states_in_block_;

   wpn_irreps_container_.CreateContainer(rhs.wpn_irreps_container_);
   return *this;
}

SU3xSU2::LABELS CncsmSU3xSU2Basis::getOmega_pn(const uint32_t proton_irrep_index,
                                               const uint32_t neutron_irrep_index,
                                               const uint32_t pn_irrep_index) const {
   SU3xSU2::LABELS wpn(wpn_irreps_container_[wpn_[pn_irrep_index]]);
   wpn.rho =
       SU3::mult(getProtonSU3xSU2(proton_irrep_index), getNeutronSU3xSU2(neutron_irrep_index), wpn);
   return wpn;
}

#ifndef ALTERNATIVE_GEN_FIN_IRREPS

void CncsmSU3xSU2Basis::GenerateFinal_SU3xSU2Irreps_Container(
    const proton_neutron::ModelSpace& ncsmModelSpace) {
   proton_neutron::ModelSpace::const_iterator nhwSubspace;

   SU3_VEC wpn_su3;
   wpn_su3.reserve(1024);

   SU2_VEC allowed_spins;
   allowed_spins.reserve(50);

   std::set<SU3xSU2::LABELS> finalSU3xSU2Irreps;
   for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
      int Nphw = nhw_p(index_p);
      SU3xSU2::LABELS wp(getProtonSU3xSU2(index_p));
      for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
         int Nhw = Nphw + nhw_n(index_n);
         if (Nhw > Nmax()) {
            break;  // fermionic irreps are stored in an increasing order according to the number of
                    // oscillator quanta
         }
         nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
         if (nhwSubspace == ncsmModelSpace.end()) {
            continue;
         }
         SU3xSU2::LABELS wn(getNeutronSU3xSU2(index_n));

         allowed_spins.resize(0);
         std::vector<SU3_VEC> allowed_su3;
         //   for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
         //   return vector of allowed SU3 labels for each allowed spin:
         //   {{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
         nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(wp.S2, wn.S2), allowed_spins,
                                            allowed_su3);
         uint32_t nwpn_spin = allowed_spins.size();
         if (nwpn_spin) {
            wpn_su3.resize(0);
            SU3::Couple(wp, wn, wpn_su3);
            SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();
            //   iterate over allowed spins ...
            for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin) {
               if (!allowed_su3[ispin].empty())  // ==> I need to select only allowed (lm mu)
               {
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3->lm &&
                          current_allowed_su3->mu == current_su3->mu)) {
                        finalSU3xSU2Irreps.insert(SU3xSU2::LABELS(
                            1, current_su3->lm, current_su3->mu, allowed_spins[ispin]));
                     }
                  }
               } else  // if array of allowed SU(3) irreps is empty ==> include ALL SU(3) irreps
                       // listed in wpn_su3
               {
                  for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin();
                       current_su3 < last_wpn_su3; ++current_su3) {
                     finalSU3xSU2Irreps.insert(SU3xSU2::LABELS(1, current_su3->lm, current_su3->mu,
                                                               allowed_spins[ispin]));
                  }
               }
            }
         }
      }
   }
   SU3xSU2_VEC irrep_list(finalSU3xSU2Irreps.size());
   std::copy(finalSU3xSU2Irreps.begin(), finalSU3xSU2Irreps.end(), irrep_list.begin());
   wpn_irreps_container_.CreateContainer(irrep_list, ncsmModelSpace.JJ());
}

#else  // ALTERNATIVE_GEN_FIN_IRREPS

//	Creates a table of allowed SU3xSU2 irreps with their basis for all possible basis states
void CncsmSU3xSU2Basis::GenerateFinal_SU3xSU2Irreps_Container(
    const proton_neutron::ModelSpace& ncsmModelSpace) {

#ifdef DEV_TIME_LOGS
   double start = omp_get_wtime();
#endif

   // Np lmp mup 2Sp
   std::set<std::array<int, 4>> proton_irreps;
   // Nn lmn mun 2Sn
   std::set<std::array<int, 4>> neutron_irreps;

   for (int ip = 0; ip < pconf_size(); ++ip) {
      int Nphw = nhw_p(ip);
      SU3xSU2::LABELS wp(getProtonSU3xSU2(ip));
      proton_irreps.insert({Nphw, wp.lm, wp.mu, wp.S2});
   }

   for (int in = 0; in < nconf_size(); ++in) {
      int Nnhw = nhw_n(in);
      SU3xSU2::LABELS wn(getNeutronSU3xSU2(in));
      neutron_irreps.insert({Nnhw, wn.lm, wn.mu, wn.S2});
   }

   SU3_VEC wpn_su3;
   std::vector<SU3_VEC> allowed_su3;
   SU2_VEC allowed_spins;

   std::set<SU3xSU2::LABELS> finalSU3xSU2Irreps;

   for (const auto& proton_irrep : proton_irreps) {
      int Nphw = proton_irrep[0];
      int lmp = proton_irrep[1];
      int mup = proton_irrep[2];
      int SSp = proton_irrep[3];

      for (const auto& neutron_irrep : neutron_irreps) {
         int Nnhw = neutron_irrep[0];
         int lmn = neutron_irrep[1];
         int mun = neutron_irrep[2];
         int SSn = neutron_irrep[3];

         int Nhw = Nphw + Nnhw;
         if (Nhw > Nmax()) {
            break;  // fermionic irreps are stored in an increasing order according to the number of
                    // oscillator quanta
         }

         auto nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
         if (nhwSubspace == ncsmModelSpace.end()) {
            continue;
         }

         allowed_spins.clear();
         allowed_su3.clear();
         nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(SSp, SSn), allowed_spins, allowed_su3);

         uint32_t number_spins = allowed_spins.size();
         if (number_spins) {
            wpn_su3.clear();
            SU3::Couple(SU3::LABELS(1, lmp, mup), SU3::LABELS(1, lmn, mun), wpn_su3);

            for (uint32_t ispin = 0; ispin < number_spins; ++ispin) {
               int SS = allowed_spins[ispin];

               if (!allowed_su3[ispin].empty()) {  //   ==> I need to select only allowed (lm mu)
                  for (const auto& current_su3 : wpn_su3) {
                     auto current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3.lm &&
                          current_allowed_su3->mu == current_su3.mu)) {
                        finalSU3xSU2Irreps.insert(
                            SU3xSU2::LABELS(1, current_su3.lm, current_su3.mu, SS));
                     }
                  }
               } else {
                  for (const auto& current_su3 : wpn_su3) {
                     finalSU3xSU2Irreps.insert(
                         SU3xSU2::LABELS(1, current_su3.lm, current_su3.mu, SS));
                  }
               }
            }
         }
      }
   }

   SU3xSU2_VEC irrep_list(finalSU3xSU2Irreps.size());
   std::copy(finalSU3xSU2Irreps.begin(), finalSU3xSU2Irreps.end(), irrep_list.begin());
   wpn_irreps_container_.CreateContainer(irrep_list, ncsmModelSpace.JJ());

#ifdef DEV_TIME_LOGS
   std::cout << "CncsmSU3xSU2Basis::GenerateFinal_SU3xSU2Irreps_Container runtime: " << cyan
             << omp_get_wtime() - start << reset << " [s]" << std::endl;
#endif
}

#endif  // ALTERNATIVE_GEN_FIN_IRREPS

void CncsmSU3xSU2Basis::ShowMemoryRequirements() const {
   cout << "max states in block: " << max_states_in_block_ << endl;
   cout << "#distribs_p: " << distribs_p_.size() << endl;
   cout << "#distribs_n: " << distribs_n_.size() << endl;

   cout << "#(U(N)>SU(3)xSU(2)) irreps stored in gamma_: ";
   cout << gammas_.size()
        << "  memory: " << sizeof(UN::SU3xSU2_VEC::value_type) * gammas_.size() / (1024.0 * 1024.0)
        << " mb" << endl;

   cout << "# rho_max(l m) irreps in omegas_su3_: " << omegas_su3_.size()
        << "  memory: " << sizeof(SU3_VEC::value_type) * omegas_su3_.size() / (1024.0 * 1024.0)
        << " mb" << endl;

   cout << "# 2S spins in omegas_spin_: " << omegas_spin_.size()
        << "  memory: " << sizeof(SU2_VEC::value_type) * omegas_spin_.size() / (1024.0 * 1024.0)
        << " mb" << endl;

   cout << "#elements in pconfs: " << pconfs_.size() << "  memory: "
        << sizeof(std::vector<NUCLEON_BASIS_INDICES>::value_type) * pconfs_.size() /
               (1024.0 * 1024.0)
        << " mb" << endl;
   cout << "#elements in nconfs: " << nconfs_.size() << "  memory: "
        << sizeof(std::vector<NUCLEON_BASIS_INDICES>::value_type) * nconfs_.size() /
               (1024.0 * 1024.0)
        << " mb" << endl;

   cout << "#element in pnbasis_ipin_: " << pnbasis_ipin_.size();
   cout << " memory: "
        << sizeof(std::vector<uint32_t>::value_type) * pnbasis_ipin_.size() / (1024.0 * 1024)
        << " mb" << endl;
   cout << "#elements in wpn: " << wpn_.size()
        << " memory: " << sizeof(std::vector<uint16_t>::value_type) * wpn_.size() / (1024.0 * 1024)
        << " mb" << endl;
   cout << "#elements in first_state_in_block_: " << first_state_in_block_.size() << " memory: "
        << sizeof(std::vector<uint32_t>::value_type) * first_state_in_block_.size() /
               (1024.0 * 1024.0)
        << " mb." << endl;
   cout << "last element in first_state_in_block_ has value of " << first_state_in_block_.back()
        << endl;
   cout << "#elements in block_end_: " << block_end_.size() << " memory: "
        << sizeof(std::vector<uint32_t>::value_type) * block_end_.size() / (1024.0 * 1024.0)
        << " mb." << endl;
   cout << "last element in block_end_ has value of " << block_end_.back() << endl;
}

/**************************************************************
                                *** PROTON INTERFACE ***
 **************************************************************/
uint8_t CncsmSU3xSU2Basis::HOquanta_p(const uint32_t proton_irrep_index) const {
   uint8_t num_quanta(0);
   uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);

   for (int i = 0; i < distribs_p_[idistr].size(); ++i) {
      num_quanta += distribs_p_[idistr][i] * i;
   }
   return num_quanta;
}

void CncsmSU3xSU2Basis::getOmega_p(uint32_t proton_irrep_index, SU3xSU2_SMALL_VEC_BASE& omega_p) const {
   assert(omega_p.empty());

   const SU3::LABELS* omega_su3 = &omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index])];
   const SU2::LABEL* omega_spin =
       &omegas_spin_[cpp0x::get<kOmegaSpin>(pconfs_[proton_irrep_index])];
   uint32_t number_omegas =
       occupied_shells_distr_p_[cpp0x::get<kDistr>(pconfs_[proton_irrep_index])] - 1;

   omega_p.resize(number_omegas);
   for (int i = 0; i < number_omegas; ++i) {
      omega_p[i].rho = omega_su3[i].rho;
      omega_p[i].lm = omega_su3[i].lm;
      omega_p[i].mu = omega_su3[i].mu;
      omega_p[i].S2 = omega_spin[i];
   }
}

void CncsmSU3xSU2Basis::getGamma_p(uint32_t proton_irrep_index, UN::SU3xSU2_VEC& gamma) const {
   assert(gamma.empty());
   uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);
   uint32_t igamma = cpp0x::get<kGamma>(pconfs_[proton_irrep_index]);
   //	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
   gamma.insert(gamma.end(), &gammas_[igamma], &gammas_[igamma] + occupied_shells_distr_p_[idistr]);
   assert(gamma.size() == occupied_shells_distr_p_[idistr]);
   assert(gamma.size());
}

void CncsmSU3xSU2Basis::getDistr_p(uint32_t proton_irrep_index, SingleDistributionSmallVectorBase& distr) const {
   assert(distr.empty());
   uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);
   distr.insert(distr.begin(), distribs_p_[idistr].begin(), distribs_p_[idistr].end());
}

SU3xSU2::LABELS CncsmSU3xSU2Basis::getProtonSU3xSU2(uint32_t proton_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return SU3xSU2::LABELS(
          omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]) + ilast_irrep],
          omegas_spin_[cpp0x::get<kOmegaSpin>(pconfs_[proton_irrep_index]) + ilast_irrep]);
   } else {
      assert(number_shells == 1);
      const UN::SU3xSU2* gamma = &gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])];
      return SU3xSU2::LABELS(1, gamma->lm, gamma->mu, gamma->S2);
   }
}

uint16_t CncsmSU3xSU2Basis::getMaximalMultiplicity_p() const
{
   uint16_t max = 1;
   for (int index_p = 0; index_p < pconfs_.size(); ++index_p) {
      int ap_max = getMult_p(index_p);
      if (ap_max > max)
      {
         max = ap_max;
      }
   }
   return max;
}
uint16_t CncsmSU3xSU2Basis::getMult_p(const uint32_t proton_irrep_index) const {
   uint16_t max_mult = 1;
   uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);
   uint32_t igamma = cpp0x::get<kGamma>(pconfs_[proton_irrep_index]);
   //	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
   for (int i = 0; i < occupied_shells_distr_p_[idistr]; ++i) {
      max_mult *= gammas_[igamma + i].mult;
   }

   uint32_t iomega_su3 = cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]);
   for (int i = 0; i < occupied_shells_distr_p_[idistr] - 1; ++i) {
      max_mult *= omegas_su3_[iomega_su3 + i].rho;
   }
   return max_mult;
}

int32_t CncsmSU3xSU2Basis::getLambda_p(const uint32_t proton_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]) + ilast_irrep].lm;
   } else {
      assert(number_shells == 1);
      return gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])].lm;
   }
}

int32_t CncsmSU3xSU2Basis::getMu_p(const uint32_t proton_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]) + ilast_irrep].mu;
   } else {
      assert(number_shells == 1);
      return gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])].mu;
   }
}

int32_t CncsmSU3xSU2Basis::getSS_p(const uint32_t proton_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return omegas_spin_[cpp0x::get<kOmegaSpin>(pconfs_[proton_irrep_index]) + ilast_irrep];
   } else {
      assert(number_shells == 1);
      return gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])].S2;
   }
}

void CncsmSU3xSU2Basis::ShowProtonIrreps() {
   for (int i = 0; i < pconfs_.size(); ++i) {
      SingleDistributionSmallVector distr;
      int nhw = nhw_p(i);

      getDistr_p(i, distr);
      cout << "ip: " << i << "\tnhw:" << nhw << "\t[";
      for (int j = 0; j < distr.size() - 1; ++j) {
         cout << (int)distr[j] << " ";
      }
      cout << (int)distr.back() << "]\t";
      UN::SU3xSU2_VEC gamma;
      getGamma_p(i, gamma);
      cout << "gamma:";
      for (int j = 0; j < gamma.size() - 1; ++j) {
         cout << gamma[j] << " ";
      }
      cout << gamma.back();
      cout << "\tomega:";

      SU3xSU2_SMALL_VEC omega;
      getOmega_p(i, omega);
      if (omega.size()) {
         for (int j = 0; j < omega.size() - 1; ++j) {
            cout << (int)omega[j].rho << "(" << (int)omega[j].lm << " " << (int)omega[j].mu << ")"
                 << (int)omega[j].S2 << " ";
         }
         cout << (int)omega.back().rho << "(" << (int)omega.back().lm << " " << (int)omega.back().mu
              << ")" << (int)omega.back().S2 << " ";
      } else {
         cout << gamma.back();
      }
      cout << "\tmax_mult_p: " << getMult_p(i);
      cout << endl;
   }
}

/**************************************************************
                                *** NEUTRON INTERFACE ***
 **************************************************************/
void CncsmSU3xSU2Basis::getOmega_n(uint32_t neutron_irrep_index, SU3xSU2_SMALL_VEC_BASE& omega_n) const {
   assert(omega_n.empty());

   const SU3::LABELS* omega_su3 = &omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index])];
   const SU2::LABEL* omega_spin =
       &omegas_spin_[cpp0x::get<kOmegaSpin>(nconfs_[neutron_irrep_index])];
   uint32_t number_omegas =
       occupied_shells_distr_n_[cpp0x::get<kDistr>(nconfs_[neutron_irrep_index])] - 1;

   omega_n.resize(number_omegas);
   for (int i = 0; i < number_omegas; ++i) {
      omega_n[i].rho = omega_su3[i].rho;
      omega_n[i].lm = omega_su3[i].lm;
      omega_n[i].mu = omega_su3[i].mu;
      omega_n[i].S2 = omega_spin[i];
   }
}

uint8_t CncsmSU3xSU2Basis::HOquanta_n(const uint32_t neutron_irrep_index) const {
   uint8_t num_quanta(0);
   uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);

   for (int i = 0; i < distribs_n_[idistr].size(); ++i) {
      num_quanta += distribs_n_[idistr][i] * i;
   }
   return num_quanta;
}

void CncsmSU3xSU2Basis::getDistr_n(uint32_t neutron_irrep_index, SingleDistributionSmallVectorBase& distr) const {
   assert(distr.empty());
   uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);
   distr.insert(distr.begin(), distribs_n_[idistr].begin(), distribs_n_[idistr].end());
}

void CncsmSU3xSU2Basis::getGamma_n(uint32_t neutron_irrep_index, UN::SU3xSU2_VEC& gamma) const {
   assert(gamma.empty());
   uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);
   uint32_t igamma = cpp0x::get<kGamma>(nconfs_[neutron_irrep_index]);
   //	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
   gamma.insert(gamma.end(), &gammas_[igamma], &gammas_[igamma] + occupied_shells_distr_n_[idistr]);
   assert(gamma.size() == occupied_shells_distr_n_[idistr]);
   assert(gamma.size());
}

SU3xSU2::LABELS CncsmSU3xSU2Basis::getNeutronSU3xSU2(uint32_t neutron_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return SU3xSU2::LABELS(
          omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]) + ilast_irrep],
          omegas_spin_[cpp0x::get<kOmegaSpin>(nconfs_[neutron_irrep_index]) + ilast_irrep]);
   } else {
      assert(number_shells == 1);
      const UN::SU3xSU2* gamma = &gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])];
      return SU3xSU2::LABELS(1, gamma->lm, gamma->mu, gamma->S2);
   }
}

uint16_t CncsmSU3xSU2Basis::getMaximalMultiplicity_n() const
{
   uint16_t max = 1;
   for (int index_n = 0; index_n < nconfs_.size(); ++index_n) {
      int an_max = getMult_n(index_n);
      if (an_max > max)
      {
         max = an_max;
      }
   }
   return max;
}

uint16_t CncsmSU3xSU2Basis::getMult_n(const uint32_t neutron_irrep_index) const {
   uint16_t max_mult = 1;
   uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);
   uint32_t igamma = cpp0x::get<kGamma>(nconfs_[neutron_irrep_index]);
   //	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
   for (int i = 0; i < occupied_shells_distr_n_[idistr]; ++i) {
      max_mult *= gammas_[igamma + i].mult;
   }

   uint32_t iomega_su3 = cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]);
   for (int i = 0; i < occupied_shells_distr_n_[idistr] - 1; ++i) {
      max_mult *= omegas_su3_[iomega_su3 + i].rho;
   }
   return max_mult;
}

int32_t CncsmSU3xSU2Basis::getLambda_n(const uint32_t neutron_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]) + ilast_irrep].lm;
   } else {
      assert(number_shells == 1);
      return gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])].lm;
   }
}

int32_t CncsmSU3xSU2Basis::getMu_n(const uint32_t neutron_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]) + ilast_irrep].mu;
   } else {
      assert(number_shells == 1);
      return gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])].mu;
   }
}

int32_t CncsmSU3xSU2Basis::getSS_n(const uint32_t neutron_irrep_index) const {
   uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
   if (number_shells > 1) {
      uint32_t ilast_irrep = number_shells - 2;
      return omegas_spin_[cpp0x::get<kOmegaSpin>(nconfs_[neutron_irrep_index]) + ilast_irrep];
   } else {
      assert(number_shells == 1);
      return gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])].S2;
   }
}

void CncsmSU3xSU2Basis::ShowNeutronIrreps() {
   for (int i = 0; i < nconfs_.size(); ++i) {
      SingleDistributionSmallVector distr;
      int nhw = nhw_n(i);

      getDistr_n(i, distr);
      cout << "in: " << i << "\tnhw:" << nhw << "\t[";
      for (int j = 0; j < distr.size() - 1; ++j) {
         cout << (int)distr[j] << " ";
      }
      cout << (int)distr.back() << "]\t";
      UN::SU3xSU2_VEC gamma;
      getGamma_n(i, gamma);
      cout << "gamma:";
      for (int j = 0; j < gamma.size() - 1; ++j) {
         cout << gamma[j] << " ";
      }
      cout << gamma.back();
      cout << "\tomega:";

      SU3xSU2_SMALL_VEC omega;
      getOmega_n(i, omega);
      if (omega.size()) {
         for (int j = 0; j < omega.size() - 1; ++j) {
            cout << (int)omega[j].rho << "(" << (int)omega[j].lm << " " << (int)omega[j].mu << ")"
                 << (int)omega[j].S2 << " ";
         }
         cout << (int)omega.back().rho << "(" << (int)omega.back().lm << " " << (int)omega.back().mu
              << ")" << (int)omega.back().S2 << " ";
      } else {
         cout << gamma.back();
      }
      cout << "\tmax_mult_n: " << getMult_n(i);
      cout << endl;
   }
}
}
