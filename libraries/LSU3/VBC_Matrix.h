#ifndef VBC_MATRIX_H
#define VBC_MATRIX_H

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <functional>
#include <iomanip>    
#include <iostream>  
#include <string>
#include <vector>

#include <omp.h>

namespace lsu3 
{

    struct VBC_Matrix 
    {
        std::vector<int> rowind;
        std::vector<int> colind;
        std::vector<int> rownnz;
        std::vector<int> colnnz;

        std::vector<float> vals;

        size_t irow;
        size_t icol;
        size_t nrows;
        size_t ncols;
        size_t nnz;

        VBC_Matrix() : nnz(0) { }
		size_t memory_usage() const
		{
			return (rowind.size() + colind.size() + rownnz.size() + colnnz.size())*sizeof(int) + vals.size()*sizeof(float);
		}

        void save_matrix_market(const std::string& filename)
        {
            std::ofstream ofs(filename.c_str());

			ofs << std::fixed;
			ofs << std::setprecision(8);

            ofs << "%%MatrixMarket matrix coordinate real symmetric" << std::endl;

            uintmax_t nnz_ = 0;
            uintmax_t n_vals = vals.size();
            for (uintmax_t k = 0; k < n_vals; ++k)
                if (vals[k] != 0.0)
                    nnz_++;
            ofs << nrows << " " << ncols << " " << nnz_ << std::endl;

            uintmax_t l = 0;
            uintmax_t n_blocks = rowind.size();
            for (uintmax_t k = 0; k < n_blocks; ++k) {
                for (uintmax_t i = 0; i < rownnz[k]; ++i) {
                    for (uintmax_t j = 0; j < colnnz[k]; ++j) {
                        if (vals[l] != 0.0) {
                            uintmax_t row = rowind[k] + i;
                            uintmax_t col = colind[k] + j;
                            ofs << (row+1) << " " << (col+1) << " " << vals[l] << std::endl;
                        }
                        
                        l++;
                    }
                }
            }
        }

        void release()
        {
           rowind.clear();
           rowind.shrink_to_fit();
           colind.clear();
           colind.shrink_to_fit();
           rownnz.clear();
           rownnz.shrink_to_fit();
           colnnz.clear();
           colnnz.shrink_to_fit();
           vals.clear();
           vals.shrink_to_fit();
        }
    };

#define RESTRICT __restrict__

    template <typename T>
    class VBC_single_vector_matvec
    {
       public:
          VBC_single_vector_matvec(const VBC_Matrix& vbc, int I, int J, uint64_t nrows, uint64_t ncols)
             : vbc_(vbc), I_(I), J_(J), diag_(I == J), nrows_(nrows), ncols_(ncols),
               nblocks_(vbc_.rowind.size()), block_idx_(nblocks_)
          {
             // first indexes for blocks
             block_idx_[0] = 0;
             for (uint64_t block = 1; block < nblocks_; block++)
                block_idx_[block] = block_idx_[block - 1] + vbc_.rownnz[block - 1] * vbc_.colnnz[block - 1];

             // setup thread-local vectors
             #pragma omp parallel
             {
                #pragma omp single
                local_y_.resize(omp_get_num_threads());

                local_y_[ omp_get_thread_num() ].resize(nrows_);

                if (!diag_)
                {
                   #pragma omp single
                   local_yt_.resize(omp_get_num_threads());

                   local_yt_[ omp_get_thread_num() ].resize(ncols_);
                }
             }
          }

       // void operator()(const T* RESTRICT x, const T* RESTRICT xt, T* RESTRICT y, T* RESTRICT yt)
          template <typename U>
          void operator()(U&& x, U&& y, U&& xt, U&& yt)
          {
//           std::fill(y.data(), y.data() + nrows_, (T)0.0);
//           if (!diag_)
//              std::fill(yt.data(), yt.data() + ncols_, (T)0.0);

             #pragma omp parallel
             {
                int ithread = omp_get_thread_num();
                auto & yy = local_y_[ithread];

                std::fill(yy.begin(), yy.end(), (T)0.0);

                if (!diag_)
                {
                   auto & yyt = local_yt_[ithread];
                   std::fill(yyt.begin(), yyt.end(), (T)0.0);

                   #pragma omp for schedule(runtime)
                   for (uint64_t block = 0; block < nblocks_; block++)
                   {
                      uint64_t first_row = vbc_.rowind[block];
                      uint64_t end_row = vbc_.rowind[block] + vbc_.rownnz[block];
                      uint64_t first_col = vbc_.colind[block];
                      uint64_t end_col = vbc_.colind[block] + vbc_.colnnz[block];

                      uint64_t idx = block_idx_[block];

                      for (uint64_t row = first_row; row < end_row; row++)
                         for (uint64_t col = first_col; col < end_col; col++)
                         {
                            yy[row] += vbc_.vals[idx] * x[col];
                            yyt[col] += vbc_.vals[idx] * xt[row];
                            idx++;
                         }
                   }

                   #pragma omp critical
                   {
                      std::transform(yy.begin(), yy.end(), y.begin(), y.begin(), std::plus<T>{});
                      std::transform(yyt.begin(), yyt.end(), yt.begin(), yt.begin(), std::plus<T>{});
                   }
                }
                else // diagonal processors:
                {
                   #pragma omp for schedule(runtime)
                   for (uint64_t block = 0; block < nblocks_; block++)
                   {
                      uint64_t first_row = vbc_.rowind[block];
                      uint64_t end_row = vbc_.rowind[block] + vbc_.rownnz[block];
                      uint64_t first_col = vbc_.colind[block];
                      uint64_t end_col = vbc_.colind[block] + vbc_.colnnz[block];

                      uint64_t idx = block_idx_[block];

                      for (uint64_t row = first_row; row < end_row; row++)
                         for (uint64_t col = first_col; col < end_col; col++)
                         {
                            yy[row] += vbc_.vals[idx] * x[col];
                            if (first_row != first_col)
                               yy[col] += vbc_.vals[idx] * x[row];
                            idx++;
                         }
                   }

                   #pragma omp critical
                   {
                      std::transform(yy.begin(), yy.end(), y.begin(), y.begin(), std::plus<T>{});
                   }
                }
             }
          }

       private:
          const VBC_Matrix& vbc_;

          int I_, J_;
          bool diag_;
          uint64_t nrows_, ncols_;

          uint64_t nblocks_;
          std::vector<uint64_t> block_idx_;

          std::vector< std::vector<T> > local_y_;
          std::vector< std::vector<T> > local_yt_;
    };

    template <typename T>
    class VBC_block_vector_matvec
    {
       public:
          VBC_block_vector_matvec(const VBC_Matrix& vbc, int I, int J, uint64_t nrows, uint64_t ncols)
             : vbc_(vbc), I_(I), J_(J), diag_(I == J), nrows_(nrows), ncols_(ncols),
               nblocks_(vbc_.rowind.size()), block_idx_(nblocks_)
          {
             // first indexes for blocks
             block_idx_[0] = 0;
             for (uint64_t block = 1; block < nblocks_; block++)
                block_idx_[block] = block_idx_[block - 1] + vbc_.rownnz[block - 1] * vbc_.colnnz[block - 1];

             // setup thread-local vectors
             #pragma omp parallel
             {
                #pragma omp single
                local_y_.resize(omp_get_num_threads());

                if (!diag_)
                {
                   #pragma omp single
                   local_yt_.resize(omp_get_num_threads());
                }
             }
          }

          void operator()(const T* RESTRICT x, const T* RESTRICT xt, T* RESTRICT y, T* RESTRICT yt, int s)
          {
             std::fill(y, y + s * nrows_, (T)0.0);
             if (!diag_)
                std::fill(yt, yt + s * ncols_, (T)0.0);

             #pragma omp parallel
             {
                int ithread = omp_get_thread_num();

                auto & yy = local_y_[ithread];
                yy.resize(s * nrows_);
                std::fill(yy.begin(), yy.end(), (T)0.0);

                if (!diag_)
                {
                   auto & yyt = local_yt_[ithread];
                   yyt.resize(s * ncols_);
                   std::fill(yyt.begin(), yyt.end(), (T)0.0);

                   #pragma omp for schedule(runtime)
                   for (uint64_t block = 0; block < nblocks_; block++)
                   {
                      uint64_t first_row = vbc_.rowind[block];
                      uint64_t end_row = vbc_.rowind[block] + vbc_.rownnz[block];
                      uint64_t first_col = vbc_.colind[block];
                      uint64_t end_col = vbc_.colind[block] + vbc_.colnnz[block];

                      uint64_t idx = block_idx_[block];

                      for (uint64_t row = first_row; row < end_row; row++)
                         for (uint64_t col = first_col; col < end_col; col++)
                         {
                            for (int k = 0; k < s; k++)
                            {
                               yy[s * row + k] += vbc_.vals[idx] * x[s * col + k];
                               yyt[s * col + k] += vbc_.vals[idx] * xt[s * row + k];
                            }
                            idx++;
                         }
                   }

                   #pragma omp critical
                   {
                      std::transform(yy.begin(), yy.end(), y, y, std::plus<T>{});
                      std::transform(yyt.begin(), yyt.end(), yt, yt, std::plus<T>{});
                   }
                }
                else // diagonal processors:
                {
                   #pragma omp for schedule(runtime)
                   for (uint64_t block = 0; block < nblocks_; block++)
                   {
                      uint64_t first_row = vbc_.rowind[block];
                      uint64_t end_row = vbc_.rowind[block] + vbc_.rownnz[block];
                      uint64_t first_col = vbc_.colind[block];
                      uint64_t end_col = vbc_.colind[block] + vbc_.colnnz[block];

                      uint64_t idx = block_idx_[block];

                      for (uint64_t row = first_row; row < end_row; row++)
                         for (uint64_t col = first_col; col < end_col; col++)
                         {
                            for (int k = 0; k < s; k++)
                            {
                               yy[s * row + k] += vbc_.vals[idx] * x[s * col + k];
                               if (first_row != first_col)
                                  yy[s * col + k] += vbc_.vals[idx] * x[s * row + k];
                            }
                            idx++;
                         }
                   }

                   #pragma omp critical
                   {
                      std::transform(yy.begin(), yy.end(), y, y, std::plus<T>{});
                   }
                }
             }
          }

       private:
          const VBC_Matrix& vbc_;

          int I_, J_;
          bool diag_;
          uint64_t nrows_, ncols_;

          uint64_t nblocks_;
          std::vector<uint64_t> block_idx_;

          std::vector< std::vector<T> > local_y_;
          std::vector< std::vector<T> > local_yt_;
    };
}

#endif
