#ifndef BROADCAST_DATA_CONTAINER_H
#define BROADCAST_DATA_CONTAINER_H
#include <mpi.h>

#include <chrono>

#include <boost/mpi.hpp>
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 

template<class T>
void BroadcastDataContainer(T& data_container, const std::string& container_name)
{
#ifdef DEBUG_OUTPUT
	std::chrono::system_clock::time_point start;
#endif

	boost::mpi::communicator mpi_comm_world;
	int my_rank(mpi_comm_world.rank());

	std::string buffer;
	int buffer_length;

	if (my_rank == 0)
	{
#ifdef DEBUG_OUTPUT		
		cout << "BroadcastDataContainer:\t\tBeggining broadcasting container '" << container_name << "'. ";
		start = std::chrono::system_clock::now();
#endif
		std::ostringstream ss;
		boost::archive::binary_oarchive oa(ss);
		oa << data_container;
		buffer = ss.str();
		buffer_length = buffer.size();

      if (buffer_length != buffer.size())
      {
         std::cerr << "Error while broadcasting container '" <<  container_name << "'.";
         std::cerr << " Size of data in bytes:" << buffer.size() << " is larger than max(int):2147483647!" << std::endl;
         exit(0);
      }

#ifdef DEBUG_OUTPUT		
		cout << "Size of serialized class: " << sizeof(char)*buffer_length/(1024.0*1024.0) << " MB." << endl;
#endif		
	}

	MPI_Bcast((void*)&buffer_length, 1, MPI_INT, 0, mpi_comm_world);

	if (my_rank != 0)
	{
		buffer.resize(buffer_length);
	}

	MPI_Bcast((void*)buffer.data(), buffer_length, MPI_CHAR, 0, mpi_comm_world);

	if (my_rank != 0)
	{
		std::istringstream ss(buffer);
		boost::archive::binary_iarchive oa(ss);
		oa >> data_container;
	}
#ifdef DEBUG_OUTPUT	
	else
	{
		cout << "BroadcastDataContainer:\t\tProcess 0 is done! It took: " << std::chrono::duration<double>(std::chrono::system_clock::now() - start).count() << endl;
	}
#endif	
}

template<class T>
void BroadcastDataContainer(T& data_container, const MPI_Comm& communicator, const std::string& container_name)
{
#ifdef DEBUG_OUTPUT
	std::chrono::system_clock::time_point start;
#endif

	int my_rank;
	MPI_Comm_rank(communicator, &my_rank);

	std::string buffer;
	int buffer_length;

	if (my_rank == 0)
	{
#ifdef DEBUG_OUTPUT		
		cout << "BroadcastDataContainer:\t\tBeggining broadcasting container '" << container_name << "'. ";
		start = std::chrono::system_clock::now();
#endif
		std::ostringstream ss;
		boost::archive::binary_oarchive oa(ss);
		oa << data_container;
		buffer = ss.str();
		buffer_length = buffer.size();

      if (buffer_length != buffer.size())
      {
         std::cerr << "Error while broadcasting container '" <<  container_name << "'.";
         std::cerr << " Size of data in bytes:" << buffer.size() << " is larger than max(int):2147483647!" << std::endl;
         exit(0);
      }

#ifdef DEBUG_OUTPUT		
		cout << "Size of serialized class: " << sizeof(char)*buffer_length/(1024.0*1024.0) << " MB." << endl;
#endif		
	}

	MPI_Bcast((void*)&buffer_length, 1, MPI_INT, 0, communicator);

	if (my_rank != 0)
	{
		buffer.resize(buffer_length);
	}

	MPI_Bcast((void*)buffer.data(), buffer_length, MPI_CHAR, 0, communicator);

	if (my_rank != 0)
	{
		std::istringstream ss(buffer);
		boost::archive::binary_iarchive oa(ss);
		oa >> data_container;
	}
#ifdef DEBUG_OUTPUT	
	else
	{
		cout << "BroadcastDataContainer:\t\tProcess 0 is done! It took: " << std::chrono::duration<double>(std::chrono::system_clock::now() - start).count() << endl;
	}
#endif	
}
#endif
