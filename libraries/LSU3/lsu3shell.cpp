#include <algorithm>
#include <chrono>
#include <cmath>
#include <stack>
#include <stdexcept>
#include <vector>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/mpi.hpp>

#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <LookUpContainers/lock.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/VBC_Matrix.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <su3.h>

#include "lsu3shell.h"

void lsu3::MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col) {
   assert(ndiag * (ndiag + 1) / 2 >= my_rank);

   int executing_process_id(0);
   for (size_t i = 0; i < ndiag; ++i) {
      row = 0;
      for (col = i; col < ndiag; ++col, ++row, ++executing_process_id) 
         if (my_rank == executing_process_id) 
            return;
   }
}

uintmax_t lsu3::CalculateME(
      const std::string& hilbert_space_definition_file_name,
      const std::string& hamiltonian_file_name,
      int ndiag, int idiag, int jdiag,
      lsu3::VBC_Matrix& vbc, long int& nstates_sofar, uint64_t& dim,
      lsu3::CncsmSU3xSU2Basis& bra, lsu3::CncsmSU3xSU2Basis& ket,
      CRunParameters& run_params,
      bool simulate)
{
   boost::mpi::communicator mpi_comm;
   int my_rank = mpi_comm.rank();
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   //  test if the first parameter is model space file name
   bool modelSpaceProvided =
       hilbert_space_definition_file_name.find(".model_space") !=
       std::string::npos;  // true if the first argument contains ".model_space"
                           //	time how long it will take to create bra and ket basis
   start = std::chrono::system_clock::now();
// lsu3::CncsmSU3xSU2Basis ket, bra;
   if (simulate) {
      if (!modelSpaceProvided)  // ==> we need to load basis states from files
      {
         std::cout << "Reading basis from '"
              << MakeBasisName(hilbert_space_definition_file_name, idiag, ndiag) << "'."
              << std::endl;
         bra.LoadBasis(hilbert_space_definition_file_name, idiag, ndiag);

         std::cout << "Reading basis from '"
              << MakeBasisName(hilbert_space_definition_file_name, jdiag, ndiag) << "'."
              << std::endl;
         ket.LoadBasis(hilbert_space_definition_file_name, jdiag, ndiag);
      } else {
         proton_neutron::ModelSpace ncsmModelSpace;
         ncsmModelSpace.Load(hilbert_space_definition_file_name);

         ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
         bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);
      }
   } else {
      if (!modelSpaceProvided)  // ==> we need to load basis states from files
      {
         if (my_rank == 0) {
            std::cout << "Reading basis from " << ndiag << " input files, starting with '"
                 << MakeBasisName(hilbert_space_definition_file_name, 0, ndiag) << "'."
                 << std::endl;
         }

         MPI_Comm ROW_COMM, COL_COMM;
         //	All processes with the same value of idiag are grouped and within
         //	each group ordered based on jdiag value
         MPI_Comm_split(mpi_comm, idiag, my_rank, &ROW_COMM);
         //	All processes with the same value of jdiag are grouped and within
         //	each group ordered based on idiag value
         MPI_Comm_split(mpi_comm, jdiag, my_rank, &COL_COMM);

         std::string bra_buffer, ket_buffer;
         int bra_buffer_length, ket_buffer_length;

         if (idiag == jdiag)  // I am diagonal process
         {
            assert(idiag == my_rank && my_rank < mpi_comm.size());

            //	load basis states from file that contains an idiag segment of a
            //	basis split into ndiag segments
            ket.LoadBasis(hilbert_space_definition_file_name, jdiag, ndiag);
            //	create a binary archive from basis
            std::ostringstream ss;
            boost::archive::binary_oarchive oa(ss);
            oa << ket;
            //	copy memory content of a binary archive into a buffer
            ket_buffer = ss.str();
            ket_buffer_length = ket_buffer.size();
            //	diagonal processes have bra and ket basis equal to each other
            bra = ket;
            bra_buffer = ket_buffer;
            bra_buffer_length = ket_buffer_length;

            //				cout << "Size of serialized class: " <<
            // sizeof(char)*bra_buffer_length/(1024.0*1024.0) << " MB." << endl;
         }
         // broadcast length of buffers ...
         // process with my_rank = idiag is a root for each group of row processes
         // process with my_rank = jdiag is a root for each group of column processes
         MPI_Bcast((void*)&bra_buffer_length, 1, MPI_INT, 0, ROW_COMM);
         MPI_Bcast((void*)&ket_buffer_length, 1, MPI_INT, 0, COL_COMM);

         if (idiag != jdiag)  // I am not a diagonal process ==> need to allocate buffer for bra and
                              // ket basis
         {
            bra_buffer.resize(bra_buffer_length);
            ket_buffer.resize(ket_buffer_length);
         }

         MPI_Bcast((void*)bra_buffer.data(), bra_buffer_length, MPI_CHAR, 0, ROW_COMM);
         MPI_Bcast((void*)ket_buffer.data(), ket_buffer_length, MPI_CHAR, 0, COL_COMM);

         if (idiag !=
             jdiag)  // If not diagonal process => create bra and ket basis from buffers received
         {
            std::istringstream bra_ss(bra_buffer);
            boost::archive::binary_iarchive bra_oa(bra_ss);
            bra_oa >> bra;

            std::istringstream ket_ss(ket_buffer);
            boost::archive::binary_iarchive ket_oa(ket_ss);
            ket_oa >> ket;
         }
      } else {
         proton_neutron::ModelSpace ncsmModelSpace;
         std::string model_space_buffer;
         int model_space_buffer_length;
         if (my_rank == 0) {
            ncsmModelSpace.Load(hilbert_space_definition_file_name);

            std::ostringstream ss;
            boost::archive::binary_oarchive oa(ss);
            oa << ncsmModelSpace;
            //	copy memory content of a binary archive into a buffer
            model_space_buffer = ss.str();
            model_space_buffer_length = model_space_buffer.size();
         }

         MPI_Bcast((void*)&model_space_buffer_length, 1, MPI_INT, 0, mpi_comm);
         if (my_rank != 0) {
            model_space_buffer.resize(model_space_buffer_length);
         }
         MPI_Bcast((void*)model_space_buffer.data(), model_space_buffer_length, MPI_CHAR, 0,
                   mpi_comm);
         if (my_rank != 0) {
            std::istringstream model_space_ss(model_space_buffer);
            boost::archive::binary_iarchive model_space_oa(model_space_ss);
            model_space_oa >> ncsmModelSpace;
         }

         ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
         bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);
      }
   }

   dim = ket.getModelSpaceDim();

   if (my_rank == 0) {
      duration = std::chrono::system_clock::now() - start;
      std::cout << "Time to construct basis: ... " << duration.count() << std::endl;
   }

   unsigned long firstStateId_bra = bra.getFirstStateId();
   unsigned long firstStateId_ket = ket.getFirstStateId();

   //	stringstream interaction_log_file_name;
   //	interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
   //	ofstream interaction_log_file(interaction_log_file_name.str().c_str());
   std::ofstream interaction_log_file("/dev/null");

   CBaseSU3Irreps baseSU3Irreps(bra.NProtons(), bra.NNeutrons(), bra.Nmax());

   //	since root process will read rme files, it is save to let this
   //	process to create missing rme files (for PPNN interaction) if they do not exist
   //	=> true
   bool log_is_on = false;
   CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);

   //	PN interaction is read after PPNN, and hence all rmes should be already in memory.
   //	if rme file does not exist, then false
   bool generate_missing_rme = false;
   CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on,
                                interaction_log_file);

   su3::init();
   try {
      start = std::chrono::system_clock::now();
      run_params.LoadHamiltonian(my_rank, hamiltonian_file_name, interactionPPNN, interactionPN,
                                 bra.NProtons() + bra.NNeutrons());
   } catch (const std::logic_error& e) {
      std::cerr << e.what() << std::endl;
      mpi_comm.abort(-1);
   }
   su3::finalize();

   if (my_rank == 0) {
      duration = std::chrono::system_clock::now() - start;
      std::cout << "Process 0: LoadInteractionTerms took " << duration.count() << std::endl;
      if (simulate) {
         //       interactionPPNN.ShowRMETables();
         std::cout << "Size of memory: "
              << interactionPPNN.GetRmeLookUpTableMemoryRequirements() / (1024.0 * 1024.0) << " MB."
              << std::endl;
      }

      // Check minimal and maximal rme values. If they are too large then
      // do not proceed.
      float min, max;
      interactionPPNN.min_max_values(min, max);
      std::cout << "minimal rme value read from tables: " << min << std::endl;
      std::cout << "maximal rme value read from tables: " << max << std::endl;
      if (fabs(min) > 100 || fabs(max) > 100) {
         std::cerr << "Error: magnitude of minimal/maximal rme values are suspitious!" << std::endl;
         std::cerr << "Data stored in rme tables may be corrupted." << std::endl;
         mpi_comm.abort(-1);
      }
   }

   // matrix data
   uintmax_t local_nnz(0);

   try {
      vbc.irow = firstStateId_bra;
      vbc.icol = firstStateId_ket;
      vbc.nrows = bra.dim();
      vbc.ncols = ket.dim();

      //		The order of coefficients is given as follows:
      //  	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
      //		TransformTensorStrengthsIntoPP_NN_structure turns that into:
      //  	index = type*k0max*rho0max + k0*rho0max + rho0
      interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

      std::cout << "Process " << my_rank << " starts calculation of me" << std::endl;
      std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

#ifdef TIME_LOCK
      for (int i = 0; i < 1024; ++i)
         openmp_locking::locktime[i] = std::chrono::duration<double>::zero();
#endif
#pragma omp parallel reduction(+ : local_nnz)
      {
         su3::init_thread();
         local_nnz = CalculateME_Scalar_UpperTriang_OpenMP(interactionPPNN, interactionPN, bra, ket,
                                                           idiag, jdiag, vbc);
#pragma omp barrier         
         su3::finalize_thread();
      }
#ifdef TIME_LOCK
      std::cout << "reporting locking time" << std::endl;
      for (int i = 0; i < 1024; ++i)
         std::cout << "Thread " << i << " : " << openmp_locking::locktime[i] << std::endl;
#endif

      std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
      std::cout << "Resulting time: " << duration.count() << std::endl;
   } catch (const std::exception& e) {
      std::cerr << e.what() << std::endl;
      mpi_comm.abort(-1);
   }
   mpi_comm.barrier();

   // nstates_sofar is needed by MFDn eigensolver
   if (my_rank < ndiag)  // ==> diagonal process ... i.e. bra = ket
   {
      nstates_sofar = bra.getFirstStateId(my_rank);
   }

   return local_nnz;
}
