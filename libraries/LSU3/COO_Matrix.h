#ifndef COO_MATRIX_H
#define COO_MATRIX_H

#include <cstdint>
#include <vector>

#include "VBC_Matrix.h"

namespace lsu3 
{
   struct COO_Matrix
   {
      int64_t nrows, ncols, nnz;
      int64_t irow, icol; // global first row and column index

      std::vector<int32_t> rows;
      std::vector<int32_t> cols;
      std::vector<float> vals;

      void construct_from_vbc(const VBC_Matrix& vbc)
      {
         rows.clear();
         cols.clear();
         vals.clear();

         nrows = vbc.nrows;
         ncols = vbc.ncols;
         
         irow = vbc.irow;
         icol = vbc.icol;

         nnz = 0;

         long l = 0;
         for (long k = 0; k < vbc.rowind.size(); k++) // iterate over block
            for (long i = 0; i < vbc.rownnz[k]; i++) // iterate over block rows
               for (long j = 0; j < vbc.colnnz[k]; j++) { // iterate over block columns
                  int32_t row = vbc.rowind[k] + i;
                  int32_t col = vbc.colind[k] + j;
                  float val = vbc.vals[l];

                  if ((icol + col >= irow + row) && (val != 0.0f)) { // nonzeros from UT
                     rows.push_back(row);
                     cols.push_back(col);
                     vals.push_back(val);
                  }

                  l++;
               }

         nnz = rows.size();
      }

      void release()
      {
         nrows = ncols = nnz = 0;
         irow = icol = 0;

         rows.clear();
         rows.shrink_to_fit();
         cols.clear();
         cols.shrink_to_fit();
         vals.clear();
         vals.shrink_to_fit();
      }
   };
}

#endif
