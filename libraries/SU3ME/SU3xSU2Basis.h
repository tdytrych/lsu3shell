#ifndef SU3xSU2_PHYSICAL_JCOUPLED_BASIS_H
#define SU3xSU2_PHYSICAL_JCOUPLED_BASIS_H

#include <UNU3SU3/UNU3SU3Basics.h>
#include <functional>
namespace SU3xSU2 
{
/** SU3xSU2 physical basis with fixed J */	
	class BasisJfixed: public SU3xSU2::LABELS
	{
/** JJ used to be a static variable. This is no longer possible, since when we calculate transition operators we may need to use, e.g. J=0 basis 
 * for bra and J=2 basis for ket. 
 */
		SU2::LABEL JJ_;
/** list of {L, kappa^max} labes, where L couples with S to JJ_/2 */
		std::vector<std::pair<SO3::LABEL, unsigned char> > allowed_LLkappa_;
/** pointer to allowed_LLkappa_ array. Used for iteration over this vector. */
		unsigned short icurrent_Lk_;
		unsigned short dim_;
/** \f$\sum_{i = 0}^{\text{\texttt{icurrent_Lk_-1}}}\kappa^{\max}_{i} */
		unsigned short first_state_id_;
		public:
		BasisJfixed(const BasisJfixed& b): SU3xSU2::LABELS(b), allowed_LLkappa_(b.allowed_LLkappa_), JJ_(b.JJ_), icurrent_Lk_(b.icurrent_Lk_), dim_(b.dim_), first_state_id_(b.first_state_id_) { }
		BasisJfixed(const SU3xSU2::LABELS& ir, SU2::LABEL JJ);

		inline void rewind() {icurrent_Lk_ = first_state_id_ = 0;}
		inline void rewind(const BasisJfixed& basis) {assert(JJ_ == basis.JJ_ && *this == basis); icurrent_Lk_ = basis.icurrent_Lk_; first_state_id_ = basis.first_state_id_;}

		inline void nextL()  {first_state_id_ += allowed_LLkappa_[icurrent_Lk_++].second;}
		inline bool IsDone() const {return icurrent_Lk_ == allowed_LLkappa_.size();}
		inline int getId(const int k) const {return first_state_id_ + k;}

		inline int L() const {return allowed_LLkappa_[icurrent_Lk_].first;}
		inline int kmax() const {return allowed_LLkappa_[icurrent_Lk_].second;}

		inline int Jmax() const {return JJ_;}
		inline int Jmin() const {return JJ_;}
		inline int JJ() const {return JJ_;}

		inline int  dim() const {return dim_;}
		inline int nJ() const {return 1;}

/** 
   The two following functions makes this class compatible with functions of 
   Calculate_proton_neuton_ncsmSU3BasisJcut.cpp/h, instead of SU3xSU2 physical 
   basis with a Jcut value, for which the latter set of functions was designed.
 */
		inline void delete_kmax() {}
		inline int getId(const int k, const SU2::LABEL JJ) const {assert(JJ == JJ_);return first_state_id_ + k;}

        // THREAD SAFE INTERFACE BEGIN
       struct iterator {
           unsigned short icurrent_Lk_;
           unsigned short first_state_id_;
       };
		inline iterator rewind() const { return iterator{0, 0}; }
		inline void nextL(iterator& iter) const {
            iter.first_state_id_ += allowed_LLkappa_[iter.icurrent_Lk_++].second;
        }

		inline bool IsDone(const iterator& iter) const {
            return iter.icurrent_Lk_ == allowed_LLkappa_.size();
        }

		inline int getId(const iterator& iter, const int k) const {
            return iter.first_state_id_ + k;
        }
		inline int getId(const iterator& iter, const int k, const SU2::LABEL JJ) const {
            assert(JJ == JJ_);
            return iter.first_state_id_ + k;
        }

		inline int L(const iterator& iter) const {
            return allowed_LLkappa_[iter.icurrent_Lk_].first;
        }

		inline int kmax(const iterator& iter) const {
            return allowed_LLkappa_[iter.icurrent_Lk_].second;
        }
        // THREAD SAFE INTERFACE END
	};

	//	size of this basis is 32 bytes. It stores array of kmax(L) values of L = 0 ... Lmax = lm + mu and
	//	calculates number of |k L J>  ... Basis.dim()
	//	allows to iterate over L
	struct BasisJcut: public SU3xSU2::LABELS
	{
		private:
/** JJ used to be a static variable. This is no longer possible, since when we calculate transition operators we may need to use, e.g. J=0 basis 
   for bra and J=2 basis for ket. 
 */
		 SU2::LABEL Jcut_;
//	I use inline function Lmax() insteading saving Lmax_ between variables of Basis in order to save memory size of Basis data structure
		inline unsigned short Lmax() {return lm + mu;}
		public:
//		function getDim calculates number of basis states |(lm mu)S k L J>,
//		where J \in [Jmin .. Jcut_] in irrep (lm mu)S [ir] without construction
//		of the iterators over them
//		as a static function it can be exectuted as unsigned int SU3xSU2::Basis::getDim(ir)
		inline int GetJcut() {return Jcut_;}
		inline void SetJcut(int Jcut) {Jcut_ = Jcut;}
		public:
		BasisJcut(const BasisJcut& b): SU3xSU2::LABELS(1, b.lm, b.mu, b.S2), Jcut_(b.Jcut_), L_(b.L_), kmax_(b.kmax_), dim_(b.dim_), id_(b.id_) {}
		BasisJcut(const SU3xSU2::LABELS& ir, const SU2::LABEL JJcut);
		void delete_kmax() { delete []kmax_;}
		~BasisJcut() {delete_kmax();}

	
		inline int L() const {return 2*L_;} // WARNING: L returns 
		inline int kmax() const {return kmax_[L_];}
//	Jmax == 2L + S_ > Jcut ==> we can not go beyong Jcut and hence we have to set Jmax = Jcut
//	if Jcut > 2L + S_ ===> Jmax = 2L + S_ 
		inline int Jmax() const {return std::min(L_ + L_ + S2, (int)Jcut_);}
		inline int Jmin() const {return abs(L_ + L_ - S2);}
		inline bool IsDone() const {return id_ == dim_;} 
		inline int  dim() const {return dim_;}
		inline int getId() const {return id_;}
		//	This method defines the order of states |k L J> in the basis of
		//	SU(3)xSU(2) representation (lm mu) S
		//	Example: (2 2)S=1
		//
		//	Order (I)
		//	k = 0 L = 0 	J = 1	id = 0
		//
		//	k = 0 L = 2		J = 1   id = 1
		//	k = 0 L = 2		J = 2   id = 2
		//	k = 0 L = 2		J = 3   id = 3
		//	k = 1 L = 2		J = 1   id = 4
		//	k = 1 L = 2		J = 2   id = 5
		//	k = 1 L = 2		J = 3   id = 6
		//	.
		//	Order (II)
		//	k = 0 L = 0 	J = 1	id = 0
		//
		//	k = 0 L = 2		J = 1   id = 1
		//	k = 1 L = 2		J = 1   id = 2
		//	k = 0 L = 2		J = 2   id = 3
		//	k = 1 L = 2		J = 2   id = 4
		//	k = 0 L = 2		J = 3   id = 5
		//	k = 1 L = 2		J = 3   id = 6
		//	.
		//	.
		//	.
		inline int getId(const int k, const int J) const 
		{
			assert(J <= Jmax());
			assert(k < kmax_[L_]);
//			order type (I)			
//			return id_ + k*nJ() + (J - Jmin())/2;
//			order type (II)			
			return id_ + kmax_[L_]*(J - Jmin())/2 + k;
		}
		inline int nJ() const {return (Jmax() - Jmin() + 2)/2;}

		inline void nextL()  {
			id_ += kmax_[L_]*nJ();
			L_ = std::find_if(kmax_+ L_ + 1, kmax_ + Lmax(), std::bind2nd(std::greater<int>(), 0)) - kmax_;
		}
		inline void rewind() 
		{
// find first L_ such that kmax_[L_] > 0 ==> first L in a given representation
// OLD way:	L_ = std::find_if(kmax_, kmax_ + Lmax(), std::bind2nd(std::greater<int>(), 0)) - kmax_;
// faster approach:
// Lmin = 0 if lm is even and mu is even
// Lmin = 1 otherwise 
			L_ = (lm%2 + mu%2) ? 1 : 0;
			id_ = 0;
		}
		inline void rewind(const BasisJcut& basis) 
		{
			L_ = basis.L_;
			id_ = basis.id_;
		}
		public:
		unsigned char *kmax_;
		unsigned short L_; // since L_ in [0 ... lm + mu] which in the case of (255 255) equals to L_ [0 ... 510]
//	this is almost overkill ==> 4 bytes are way more then needed to iterate over dim_ and id_ can adress a way larger su(3) irreps with (lm > 255 mu > 255) and high values of S		
//	in such a cade kmax array starts to get big ... 
		unsigned int dim_, id_;
	};


/** Same as BasisJcut but with static Jcut_. This class is here due to the identical fermion code, and which Kristina uses.  */
	struct BasisIdenticalFermsCompatible: public SU3xSU2::LABELS
	{
		public:
		
		private:
		static int Jcut_;
		inline unsigned short Lmax() {return lm + mu;}
		public:
		static unsigned int getDim(const SU3xSU2::LABELS& ir)
		{
			int Lmax = ir.lm + ir.mu;
			int S = ir.S2;
			unsigned int dim(0);
			for (int L = 0; L <= Lmax; ++L)
			{
				if (abs(2*L - S) <= Jcut_)
				{
					int kmax = SU3::kmax(ir, L);
					if (kmax)
					{
						dim += kmax*((std::min(2*L + S, Jcut_) - abs(2*L - S) + 2)/2);// kmax*( )/2 does not work properly if Jcut is odd ==> kmax*( ( )/2 ) is OK
					}
				}
			}
			return dim;
		}

		static inline int GetJcut() {return Jcut_;}
		static inline void SetJcut(int Jcut) {Jcut_ = Jcut;}
		public:
		BasisIdenticalFermsCompatible(const BasisIdenticalFermsCompatible& b): SU3xSU2::LABELS(1, b.lm, b.mu, b.S2), L_(b.L_), kmax_(b.kmax_), dim_(b.dim_), id_(b.id_) { }
		BasisIdenticalFermsCompatible(const SU3xSU2::LABELS& ir): SU3xSU2::LABELS(1, ir.lm, ir.mu, ir.S2), dim_(0)
		{
			kmax_ = new unsigned char[Lmax() + 1];
			memset(kmax_, 0, Lmax()*sizeof(unsigned char));
			for (L_ = 0; L_ <= Lmax(); ++L_)
			{
				if (Jmin() <= Jcut_)
				{
					kmax_[L_] = SU3::kmax(ir, L_);
					if (kmax_[L_])
					{
						dim_ += kmax_[L_]*((Jmax() - Jmin() + 2)/2);
					}
				}
			}
			rewind();
		}
		void delete_kmax() { delete []kmax_;}
		~BasisIdenticalFermsCompatible() {}
	
		inline int L() const {return 2*L_;}
		inline int kmax() const {return kmax_[L_];}
		inline int Jmax() const {return std::min(L_ + L_ + S2, Jcut_);}
		inline int Jmin() const {return abs(L_ + L_ - S2);}
		inline bool IsDone() const {return id_ == dim_;} 
		inline int  dim() const {return dim_;}
		inline int getId() const {return id_;}
		inline int getId(const int k, const int J) const 
		{
			assert(J <= Jmax());
			assert(k < kmax_[L_]);
			return id_ + kmax_[L_]*(J - Jmin())/2 + k;
		}
		inline int nJ() const {return (Jmax() - Jmin() + 2)/2;}

		inline void nextL()  {
			id_ += kmax_[L_]*nJ();
			L_ = std::find_if(kmax_+ L_ + 1, kmax_ + Lmax(), std::bind2nd(std::greater<int>(), 0)) - kmax_;
		}
		inline void rewind() 
		{
			L_ = (lm%2 + mu%2) ? 1 : 0;
			id_ = 0;
			assert(kmax_[L_] != 0 || dim_ == 0);
		}
		inline void rewind(const BasisIdenticalFermsCompatible& basis) 
		{
			L_ = basis.L_;
			id_ = basis.id_;
		}

		public:
		unsigned char *kmax_;
		unsigned short L_; // since L_ in [0 ... lm + mu] which in the case of (255 255) equals to L_ [0 ... 510]
		unsigned int dim_, id_;
	};

/*
	template <class Basis>
	class IrrepsContainer
	{
		std::vector<Basis> final_SU3xSU2Irreps_;
		public:
		IrrepsContainer(const SU3xSU2_VEC& irrep_list, const SU2::LABEL& JJ)
		{
			final_SU3xSU2Irreps_.reserve(irrep_list.size()); 
			for(SU3xSU2_VEC::const_iterator irrep_su3xsu2 = irrep_list.begin(); irrep_su3xsu2 < irrep_list.end(); ++irrep_su3xsu2) 
			{
				final_SU3xSU2Irreps_.push_back(Basis(*irrep_su3xsu2, JJ));
			}
		}
		size_t rhomax_x_dim(const SU3xSU2::LABELS& irrep_su3xsu2) const
		{
			typename std::vector<Basis>::const_iterator it = std::lower_bound(final_SU3xSU2Irreps_.begin(), final_SU3xSU2Irreps_.end(), irrep_su3xsu2);
			assert(it < final_SU3xSU2Irreps_.end());
			return  irrep_su3xsu2.rho*it->dim();
		}
		const Basis& getSU3xSU2PhysicalBasis(const SU3xSU2::LABELS& irrep_su3xsu2) const
		{
			typename std::vector<Basis>::const_iterator it = std::lower_bound(final_SU3xSU2Irreps_.begin(), final_SU3xSU2Irreps_.end(), irrep_su3xsu2);
			assert(it < final_SU3xSU2Irreps_.end());
			return *it;
		}
		~IrrepsContainer() 
		{
			for (typename std::vector<Basis>::iterator it = final_SU3xSU2Irreps_.begin(); it < final_SU3xSU2Irreps_.end(); ++it) 
			{
				it->delete_kmax();
			}
		}
	};
*/

	template <class Basis>
	class IrrepsContainer
	{
	  Basis* final_SU3xSU2Irreps_;
	  Basis* end_;

	  template <int size>
	  int pad()
	  {
		 if (size % 8 != 0)
			return size+8-(size%8);
		 else
			return size;
	  }

		public:
		void CreateContainer(const SU3xSU2_VEC& irrep_list, const SU2::LABEL& JJ)
		{
		  final_SU3xSU2Irreps_ = (Basis*)new char[pad<sizeof(Basis)>()*irrep_list.size()]; //plus alignment
		  end_ = final_SU3xSU2Irreps_;
			for(SU3xSU2_VEC::const_iterator irrep_su3xsu2 = irrep_list.begin(); irrep_su3xsu2 < irrep_list.end(); ++irrep_su3xsu2) 
			{
			  new(end_) Basis(*irrep_su3xsu2, JJ);
			  end_++;
			}
		}
		IrrepsContainer():final_SU3xSU2Irreps_(0), end_(0) {}
		IrrepsContainer(const IrrepsContainer& container)
		{
			CreateContainer(container);
		}

		void CreateContainer(const IrrepsContainer& rhs)
		{
		  final_SU3xSU2Irreps_ = (Basis*)new char[pad<sizeof(Basis)>()*rhs.getSize()]; //plus alignment
		  end_ = final_SU3xSU2Irreps_;
			for (Basis* t = rhs.final_SU3xSU2Irreps_ ; t != rhs.end_; t++) 
			{
			  new(end_) Basis(*t, t->JJ());
			  end_++;
			}
		}

		IrrepsContainer(const SU3xSU2_VEC& irrep_list, const SU2::LABEL& JJ)
		{
			CreateContainer(irrep_list, JJ);
		}
		inline unsigned int getIndexInTable(const SU3xSU2::LABELS& irrep_su3xsu2) const
		{
			return std::distance(final_SU3xSU2Irreps_, std::lower_bound(final_SU3xSU2Irreps_, end_, irrep_su3xsu2));
		}

		const Basis& operator[](unsigned int index) const {return final_SU3xSU2Irreps_[index];}

		size_t rhomax_x_dim(const SU3xSU2::LABELS& irrep_su3xsu2) const
		{
			Basis* it = std::lower_bound(final_SU3xSU2Irreps_, end_, irrep_su3xsu2);
			assert(it < end_);
			assert(it >= final_SU3xSU2Irreps_);
		  
			return  irrep_su3xsu2.rho*it->dim();
		}
		const Basis& getSU3xSU2PhysicalBasis(const SU3xSU2::LABELS& irrep_su3xsu2) const
		{
		  Basis* it = std::lower_bound(final_SU3xSU2Irreps_, end_, irrep_su3xsu2);
		  assert(it < end_);
		  assert(it >= final_SU3xSU2Irreps_);
		  return *it;
		}
		~IrrepsContainer() 
		{
			for (Basis* t = final_SU3xSU2Irreps_ ; t != end_; t++) 
			{
				t->delete_kmax();
				t->~Basis();
			}
			delete[] (char*)final_SU3xSU2Irreps_;
		}
		unsigned int getSize() const {return std::distance(final_SU3xSU2Irreps_, end_);}
		Basis* begin() const {return final_SU3xSU2Irreps_;}
		Basis* end() const {return end_;}
	};
}
typedef SU3xSU2::BasisJfixed IRREPBASIS;
// typedef SU3xSU2::BasisJcut IRREPBASIS;
#endif
