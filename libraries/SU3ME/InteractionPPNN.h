#ifndef INTERACTION_PPNN_H
#define INTERACTION_PPNN_H
#include <SU3ME/CRMECalculator.h>
#include <SU3ME/CTensorStructure.h>
#include <SU3ME/memory_pool.h>
#include <LookUpContainers/CSSTensorRMELookUpTablesContainer.h>

#include <LSU3/std.h>

class CTensorGroup 
{
	public:		
	typedef TENSOR_STRENGTH COEFF_DOUBLE;
//	This structure allows me to free allocated memory as std::for_each(Tensors.begin(), Tensors.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
//	where Tensors data type == std::vector<std::pair<CRMECalculator*, COEFF_DOUBLE*> >
	struct DeleteCRMECalculatorPtrs: public std::unary_function<const std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*>&, void>
	{
		void operator()(const std::pair<CRMECalculator*, COEFF_DOUBLE*>& Item) const
		{
			Item.first->~CRMECalculator();
            memory_pool::crmecalculator.free(Item.first);
		}
	};
//	Note that the following three vectors can be easily obtained from m_structure
	std::vector<unsigned char> m_nAnnihilators;
	std::vector<char> m_dA;
	std::vector<unsigned char> m_ShellsT;	//	HO shells with active single-shell tensors.
	private:
	std::vector<char> n_adta_InSST_; 	//	needed for a proper calculation of the overall phase in SU(3)xSU(2) reduction formula
										//	this phase depends besides distribution of bra and ket fermions over HO shells also
										//	number of a+/ta operators in a shell with SST.
	public:
	const std::vector<char>& n_adta_InSST() {return n_adta_InSST_;}
	const std::vector<unsigned char>& ShellsT() {return m_ShellsT;}
	//	number of coefficients can be obtained by calling CTensorStructure::GetNCoefficients
	std::vector<std::pair<CTensorStructure*, COEFF_DOUBLE*> > m_Tensors;

	CTensorGroup(	const std::vector<char>& structure, 
					const std::vector<unsigned char>& Shells, 
					const std::vector<char>& dA, 
					const std::vector<unsigned char> nAnnihilators);
	CTensorGroup(const CTensorGroup& TG) { assert(0);}
	~CTensorGroup();

	inline const std::vector<unsigned char>& TensorShells() const {return m_ShellsT;}
	void AddTensor(	const std::vector<CrmeTable*>& SingleShellTensorRmeTables, const std::vector<SU3xSU2::LABELS*> vOmega, 
					WigEckSU3SO3CGTable* pSU3SO3CGTable, const std::vector<COEFF_DOUBLE> dCoeffs);
//	Does ket_confs x TensorGroup --> bra_confs ???
	bool Couple(const boost::container::small_vector_base<unsigned char>& Bra_confs, const boost::container::small_vector_base<unsigned char>& Ket_confs, const std::vector<unsigned char>& hoShells);
//	Does ket_confs x TensorGroup --> bra_confs if TensorGroup describes a+_i a+_i a_i a_i and/or a+_i a_i ???
	bool CoupleSingleShellTensor(const boost::container::small_vector_base<unsigned char>& Ket_confs, const std::vector<unsigned char>& hoShells);

	size_t SelectTensorsByGamma(const	UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const boost::container::small_vector_base<unsigned char>& Ket_confs,
								const std::vector<unsigned char>& BraKetShells, const int phase, std::vector<std::pair<CRMECalculator*, COEFF_DOUBLE*> >& Tensors);

	size_t SelectTensorsByGamma(const nucleon::Type type, const	UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const boost::container::small_vector_base<unsigned char>& Ket_confs,
								const std::vector<unsigned char>& BraKetShells, const int phase, std::vector<std::pair<CRMECalculator*, COEFF_DOUBLE*> >& Tensors);

	size_t SelectTensorsByGamma(const UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const std::vector<unsigned char>& Ket_confs,
											const std::vector<unsigned char>& BraKetShells, std::vector<std::pair<SU3xSU2_VEC, SU3xSU2_SMALL_VEC> >& Tensors);

	size_t SelectTensorsByGamma(const UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const boost::container::small_vector_base<unsigned char>& Ket_confs,
											const std::vector<unsigned char>& BraKetShells, const int phase, std::vector<std::pair<CRMECalculator*, const CTensorStructure*> >& Tensors);

	void Show(const CBaseSU3Irreps& BaseSU3Irreps) const;
	void ShowTensorStructure() const;
	void ShowTensors() const;
};


class CInteractionPPNN
{
//	two data types utilized for loading PPNN interactions	
	typedef std::vector<std::pair<SU3xSU2_VEC, std::vector<TENSOR_STRENGTH> > > TENSORS_WITH_STRENGTHS;
	typedef std::map<std::vector<char>, TENSORS_WITH_STRENGTHS> PPNNInteractionTensors;

//	two data types utilized for loading one-body interactions	
	typedef std::vector<std::pair<cpp0x::array<uint8_t, 4>, std::vector<TENSOR_STRENGTH> > > ONEBODYTENSORS_WITH_STRENGTHS; // {lm mu 2S 2L0}
	typedef std::map<CTuple<int8_t, 2>, ONEBODYTENSORS_WITH_STRENGTHS> OneBodyInteractionTensors;

	const CBaseSU3Irreps& BaseSU3Irreps_;
	CSSTensorRMELookUpTablesContainer rmeLookUpTableContainer_;
	CWigEckSU3SO3CGTablesLookUpContainer wigEckSU3SO3CGTablesLookUpContainer_;
	bool log_is_on_;
	std::ostream& log_file_;
	public:
//	enum {eSingleShell = 0, eTwoShells = 1, eTwoShellsdAZero = 2, eThreeShells = 3, eFourShells = 4};
	private:
	std::vector<CTensorGroup*> m_TensorGroups[9]; // index ranges from 0 up to 8, where vectors located at index = 0, 3 are empty
	//	Groups are divided according to number of shells (n) and the numbre of changes in distribution it induces (Delta)
	//	Delta      n       index in array m_TensorGroups
	//	0    +     1    =    1
	//	0    +     2    =    2
	//	2    +     2    =    4
	//	2    +     3    =    5
	//	4    +     2    =    6
	//	4    +     3    =    7
	//	4    +     4    =    8
	// Vectors located at index = 0, 3 are empty.

//	std::vector<std::vector<CTensorGroup*> > m_TensorGroups;
	//	m_TensorGroups[0] == tensors that act on a single shell
	//	m_TensorGroups[1] == tensors that act on two-shells  and do not have dA = {0 0}
	//	m_TensorGroups[2] == tensors that act on two-shells and have dA = {0, 0} ==> {a+ta}x{a+ta}
	//	m_TensorGroups[3] == tensors that act on three shells
	//	m_TensorGroups[3] == tensors that act on four shells
	public:
	inline bool log_is_on() const {return log_is_on_;}

	//	constructor reads interaction file sFileName, creates CrmeTables that
	//	are needed for a given model space and stores in m_TensorGroups tensors
	//	with their strengths.
/*!
  Load proton-proton and neutron-neutron two-body interaction terms 
  \param[in] BaseSU3Irreps set of unique seed irreps dest 
  \param[in] log_is_on if true, detailed information will be logged into an output stream
  \param[in] log_file output stream in case log_is_on is true.
  */
	CInteractionPPNN(const CBaseSU3Irreps& BaseSU3Irreps, bool log_is_on = true, std::ostream& log_file = std::cout):
														  BaseSU3Irreps_(BaseSU3Irreps),
														  rmeLookUpTableContainer_(BaseSU3Irreps),
														  wigEckSU3SO3CGTablesLookUpContainer_(), log_is_on_(log_is_on), log_file_(log_file) {}

	~CInteractionPPNN();

	size_t GetRelevantTensorGroups(	const boost::container::small_vector_base<unsigned char>& Bra_confs, 		
									const boost::container::small_vector_base<unsigned char>& Ket_confs, 
									const std::vector<unsigned char>& hoShells, 
									std::vector<CTensorGroup*>& vTensorGroups, 
									std::vector<int>& vphases) const;

//	The order of coefficients is given as follows:
//  index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//  This order is used by identicalFermionsMECalculator/main.cpp code
//
//	TransformTensorStrengthsIntoPP_NN_structure turns that into:
//  index = type*k0max*rho0max + k0*rho0max + rho0
//  which is order being used by the proton-neutron m.e. calculator code
	void TransformTensorStrengthsIntoPP_NN_structure();
// TODO: add JJ0 and MM0 generalization of the two-body operator 
	void LoadTwoBodyOperators(const int my_rank, const std::vector<std::pair<std::string, TENSOR_STRENGTH> >& fileNameStrength);
	void LoadTwoBodyOperator(const std::string& fileName);
	void AddOneBodyOperators(const int my_rank, const std::vector<std::pair<std::string, TENSOR_STRENGTH> >& fileNameStrength, SU2::LABEL& JJ0, SU2::LABEL& MM0);
	private:
	void BroadcastInteractionData(std::vector<int8_t>& structures_tot, std::vector<uint16_t>& number_tensors_tot, std::vector<uint8_t>& rholmmu2S_tot, std::vector<TENSOR_STRENGTH>& coeffs_tot, bool bcastRmeTable);
	bool BelongToModelSpace(const std::vector<char>& structure, const std::vector<unsigned char>& ShellsT, const std::vector<char>& dA, const std::vector<unsigned char>& nAnnihilators) const;
	bool adtaBelongToModelSpace(const CTuple<int8_t, 2>& n1n2) const; 
	void LoadTwoBodyOperatorsIntoDataStructures(const std::vector<std::pair<std::string, TENSOR_STRENGTH> >& fileNameStrength, PPNNInteractionTensors& unique_tensors);
	void LoadOneBodyOperatorsIntoDataStructures(const std::vector<std::pair<std::string, TENSOR_STRENGTH> >& fileNameStrength, OneBodyInteractionTensors& unique_Tensors, SU2::LABEL& JJ0, SU2::LABEL& MM0);
	public:
	void GetTensors(std::vector<CTensorStructure*>& tensors) const;

	void Show() const;
	void ShowTensorGroups() const;
	void ShowTensors() const;
	size_t GetRmeLookUpTableMemoryRequirements() const { return rmeLookUpTableContainer_.GetMemorySize();} 
	void ShowRMETables() const {rmeLookUpTableContainer_.ShowRMETables();}
	void min_max_values(SU3xSU2::RME::DOUBLE& min, SU3xSU2::RME::DOUBLE& max) const { rmeLookUpTableContainer_.min_max_rme(min, max);}
};
#endif
