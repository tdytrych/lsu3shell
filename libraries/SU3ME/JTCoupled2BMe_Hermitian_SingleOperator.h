#ifndef	JTCOUPLED2BMe_HERMITIAN
#define JTCOUPLED2BMe_HERMITIAN
#include <SU3ME/OperatorDataStructure.h>
#include <SU3ME/global_definitions.h>

#include <cassert>
#include <bitset>
#include <limits>

template <typename V>
class JTCoupled2BMe_Hermitian_SingleOperator: public  OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, CTuple<V, 3> >
{
	public:
// these two methods implement JPV compatible mapping between nlj and integer
	inline void Get_nlj(const int index, int& n, int& l, int& j) const
	{
		size_t lookup_index = 3*(index - 1);
		n = nlj_look_up_table_[lookup_index];
		l = nlj_look_up_table_[++lookup_index];
		j = nlj_look_up_table_[++lookup_index];
	}
	int Get_Index(const int n, const int l, const int j) const;
	
	typedef V DOUBLE;
	typedef CTuple<V, 3> Value;

	JTCoupled2BMe_Hermitian_SingleOperator(const std::string& sFileName):OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>(sFileName) 
	{
		OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::Sort();

		Create_nlj_look_up_table();
	};

	bool MeJT(const jt_coupled_bra_ket::TwoBodyLabels& I1I2I3I4JT, Value& jt_couple_matrix_element) const;
	bool MeJ(const j_coupled_bra_ket::TwoBodyLabels& I1I2I3I4J, V& mePP, V& meNN, V& mePN) const;
	public:
//	This method multiplies matrix elements by phase = (-)^{1/2 sum (ni - li)} 
//	transforming matrix elements given in HO basis positive at origin into
//	positive at infinity and the other way around.
	void SwitchHOConvention();
	private:
	void Calculate_nlj(const int index, int& n, int& l, int& j) const; // Warning: procedure returns angular momentum l, and not 2*l, while for total momentum j = 2*j

	void Create_nlj_look_up_table();


	std::vector<int> nlj_look_up_table_;
};


//	Return <I1 I2 JT || V || I3 I4 JT>. 
//
//	Note that the JT-coupled basis of the two-nucleon system is spanned by 
//	the states with I1 <= I2, since |I1 I2 J T> = (-)^phase |I2 I1 JT>.
//	
//	Also, we store only the upper diagonal matrix:
//         1 1    1 2     1 3     1 4     2 2     2 3    2 4     3 3     3 4     4 4
//	1 1    x      x       x       x       x       x      x       x       x       x
//	1 2           x       x       x       x       x      x       x       x       x
//	1 3                   x       x       x       x      x       x       x       x
//	1 4                           x       x       x      x       x       x       x
//	2 2                                   x       x      x       x       x       x
//	2 3                                           x      x       x       x       x
//	2 4                                                  x       x       x       x
//	3 3                                                          x       x       x
//	3 4                                                                  x       x
//	4 4                                                                          x
//	
//	and hence besides I1 <= I2 and I3 <= I4, we store only those matrix elements that satisfy: 
//
//	((I1 < I3) || ((I1 == I3) && I2 <= I4)))
//	
//	This can be seen, e.g., from the structure of the third row which is being stored:
//	<1 3 || || 1 3>, <1 3|| ||1 4>  ... I1 == I3 && I2 <= I4
//	<1 3 || || 2 2>, <1 3|| ||2 4>, <1 3|| ||3 3>, <1 3|| ||3 4>, <1 3|| ||4 4> .... I1 < I3
template<typename V>
bool JTCoupled2BMe_Hermitian_SingleOperator<V>::MeJT(const jt_coupled_bra_ket::TwoBodyLabels& I1I2I3I4JT, Value& jt_coupled_matrix_element) const
{
	SU2::LABEL T(I1I2I3I4JT[jt_coupled_bra_ket::T]);	// for two nucleon wave function, T = 0 or 1
	SU2::LABEL J(I1I2I3I4JT[jt_coupled_bra_ket::J]);	// for two fermions wave function, J is always an integer ==> we do not use 2*J values 
	int phase = 0;
	int n, l, ja, jb, jc, jd;
	bool bResult;
	jt_coupled_bra_ket::TwoBodyLabels labels2find(I1I2I3I4JT);
	const jt_coupled_bra_ket::TwoBodyLabels::Data_Type& i1 = labels2find[jt_coupled_bra_ket::I1];
	const jt_coupled_bra_ket::TwoBodyLabels::Data_Type& i2 = labels2find[jt_coupled_bra_ket::I2];
	const jt_coupled_bra_ket::TwoBodyLabels::Data_Type& i3 = labels2find[jt_coupled_bra_ket::I3];
	const jt_coupled_bra_ket::TwoBodyLabels::Data_Type& i4 = labels2find[jt_coupled_bra_ket::I4];

	if (i1 > i2) //	Use symmetry (A.5)		
	{
		Get_nlj(i1, n, l, ja);
		Get_nlj(i2, n, l, jb);
		std::swap(labels2find[jt_coupled_bra_ket::I1], labels2find[jt_coupled_bra_ket::I2]);
		phase += (ja+jb)/2+J+T;
	}
	if (i3 > i4) //	Use symmetry (A.6)		
	{
		Get_nlj(i3, n, l, jc);
		Get_nlj(i4, n, l, jd);
		std::swap(labels2find[jt_coupled_bra_ket::I3], labels2find[jt_coupled_bra_ket::I4]);
		phase += (jc+jd)/2+J+T;
	}
	if	(!((i1 < i3) || ((i1 == i3)  && (i2 <= i4))))	// since the operator is Hermition => use relation (A.9)
	{
		std::swap(labels2find[jt_coupled_bra_ket::I1], labels2find[jt_coupled_bra_ket::I3]);
		std::swap(labels2find[jt_coupled_bra_ket::I2], labels2find[jt_coupled_bra_ket::I4]);
	}
	bResult = OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::FindEntry(labels2find, jt_coupled_matrix_element);
	if (bResult)
	{
		if (T == 0)
		{
			jt_coupled_matrix_element[0] *= MINUSto(phase);
			assert(jt_coupled_matrix_element[1] == 0 && jt_coupled_matrix_element[2] == 0);
		}
		else if (T == 1)
		{
			jt_coupled_matrix_element[0] *= MINUSto(phase);
			jt_coupled_matrix_element[1] *= MINUSto(phase);
			jt_coupled_matrix_element[2] *= MINUSto(phase);
		}
	}
	return bResult;
}

//	Procedure MeJ_Hermitian_SingleOperator uses relations (A.19), (A.20), and (A.21) to calculate:
//	<I1_{p} I2_{p} J || V || I3_{p} I4_{p} J>	if	OptType == PP 
//	<I1_{n} I2_{n} J || V || I3_{n} I4_{n} J>	if	OptType == NN
//	
//	<I1_{p} I2_{n} J || V || I3_{p} I4_{n} J> =	<I1_{n} I2_{p} J || V || I3_{n} I4_{p} J>	if OptType == PN
//
//	NOTE: this function can not return matrix elements of type:
//	<I1_{n} I2_{p} J || V || I3_{p} I4_{n} J> =	<I1_{p} I2_{n} J || V || I3_{n} I4_{p} J> as 
//	this would require to accomodate relation (A.22). 
//	
//	However, one can calculate e.g. <I2_{p} I1_{n} J || V || I3_{p} I4_{n} J> 
//	and then apply relation (A.11) to get <I1_{n} I2_{p} J|| V || I3_{p} I4_{n} J>
template<typename V>
bool JTCoupled2BMe_Hermitian_SingleOperator<V>::MeJ(const j_coupled_bra_ket::TwoBodyLabels& I1I2I3I4J, V& mePP, V& meNN, V& mePN) const
{
	bool bResult;
	jt_coupled_bra_ket::TwoBodyLabels jt_labels;

	jt_labels[jt_coupled_bra_ket::I1] = I1I2I3I4J[j_coupled_bra_ket::I1];
	jt_labels[jt_coupled_bra_ket::I2] = I1I2I3I4J[j_coupled_bra_ket::I2];
	jt_labels[jt_coupled_bra_ket::I3] = I1I2I3I4J[j_coupled_bra_ket::I3];
	jt_labels[jt_coupled_bra_ket::I4] = I1I2I3I4J[j_coupled_bra_ket::I4];
	jt_labels[jt_coupled_bra_ket::J]  = I1I2I3I4J[j_coupled_bra_ket::J];

	Value matrix_element_T0, matrix_element_T1;
	bool bT1, bT0;
	double dT1Coeff(0.0), dT0Coeff(0.0);
	int minusToJDelta_ab = MINUSto(jt_labels[jt_coupled_bra_ket::J])*(jt_labels[jt_coupled_bra_ket::I1] == jt_labels[jt_coupled_bra_ket::I2]);
	int minusToJDelta_cd = MINUSto(jt_labels[jt_coupled_bra_ket::J])*(jt_labels[jt_coupled_bra_ket::I3] == jt_labels[jt_coupled_bra_ket::I4]);

	jt_labels[jt_coupled_bra_ket::T] = 1;
	bT1 = MeJT(jt_labels, matrix_element_T1);
	jt_labels[jt_coupled_bra_ket::T] = 0;
	bT0 = MeJT(jt_labels, matrix_element_T0);

	if (bT1) 
	{
		dT1Coeff = std::sqrt((1 + minusToJDelta_ab)*(1 + minusToJDelta_cd));
	}
	if (bT0) 
	{
		dT0Coeff = std::sqrt((1 - minusToJDelta_ab)*(1 - minusToJDelta_cd));
	}

	if (bT0 || bT1)
	{
//	formula (A.21)
//	PN part of the interaction is not being multipled by 1/4 !!!! ==> the next line mut be commented ...
//		mePN = 0.5*(dT1Coeff*matrix_element_T1[0] + dT0Coeff*matrix_element_T0[0])/4.0;	

		mePN = 0.5*(dT1Coeff*matrix_element_T1[0] + dT0Coeff*matrix_element_T0[0]);
		mePP = matrix_element_T1[1]/4.0;
		meNN = matrix_element_T1[2]/4.0;
	}
	return (bT0 || bT1);
}


//	This function multiplies matrix elements by phase = (-)^{1/2 sum (ni - li)}
//	transforming matrix elements given in HO basis positive at origin into
//	positive at infinity 
template<typename V>
void JTCoupled2BMe_Hermitian_SingleOperator<V>::SwitchHOConvention()
{
	int iphase;
	double dPhase;
	int n, l, j;
	size_t nTerms = OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::size();
	for (size_t iTerm = 0; iTerm < nTerms; ++iTerm)
	{
		iphase = 0;
		jt_coupled_bra_ket::TwoBodyLabels jt_labels(OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::GetLabels(iTerm));

		Get_nlj(jt_labels[jt_coupled_bra_ket::I1], n, l, j);
		iphase += (n - l);
		Get_nlj(jt_labels[jt_coupled_bra_ket::I2], n, l, j);
		iphase += (n - l);
		Get_nlj(jt_labels[jt_coupled_bra_ket::I3], n, l, j);
		iphase += (n - l);
		Get_nlj(jt_labels[jt_coupled_bra_ket::I4], n, l, j);
		iphase += (n - l);
		dPhase = MINUSto(iphase/2);

		OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::m_Terms[iTerm].second[0] *= dPhase;
		OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::m_Terms[iTerm].second[1] *= dPhase;
		OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::m_Terms[iTerm].second[2] *= dPhase;
	}
}

template<typename V>
void JTCoupled2BMe_Hermitian_SingleOperator<V>::Create_nlj_look_up_table()
{
	int max_index(0);
	size_t nTerms = OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::size();
	for (size_t iTerm = 0; iTerm < nTerms; ++iTerm)
	{
		jt_coupled_bra_ket::TwoBodyLabels jt_labels(OperatorDataStructure<jt_coupled_bra_ket::TwoBodyLabels, Value>::GetLabels(iTerm));
		if (jt_labels[jt_coupled_bra_ket::I1] > max_index)
		{
			max_index = jt_labels[jt_coupled_bra_ket::I1];
		}
		if (jt_labels[jt_coupled_bra_ket::I2] > max_index)
		{
			max_index = jt_labels[jt_coupled_bra_ket::I2];
		}
		if (jt_labels[jt_coupled_bra_ket::I3] > max_index)
		{
			max_index = jt_labels[jt_coupled_bra_ket::I3];
		}
		if (jt_labels[jt_coupled_bra_ket::I4] > max_index)
		{
			max_index = jt_labels[jt_coupled_bra_ket::I4];
		}
	}

	for (int index = 1; index <= max_index; ++index)
	{
		int n, l, j;
		Calculate_nlj(index, n, l, j);
		nlj_look_up_table_.push_back(n); nlj_look_up_table_.push_back(l); nlj_look_up_table_.push_back(j);
	}
}


template<typename V>
void JTCoupled2BMe_Hermitian_SingleOperator<V>::Calculate_nlj(const int index, int& n, int& l, int& j) const
{
	n = (-1 + sqrt(1.0 + 8.0*(index-1)))/2;
	int i = (index-1) - n*(n+1)/2;
	if (n%2)
	{
		l = 1 + 2*(i/2);
		j = (i % 2) ? (2*l + 1) : (2*l - 1);
	}
	else
	{
		l = 2*((i+1)/2);
		j = (i % 2) ? 2*l - 1: 2*l + 1;
	}
}

template<typename V>
int JTCoupled2BMe_Hermitian_SingleOperator<V>::Get_Index(const int n, const int l, const int j) const
{
	int index = n*(n + 1)/2 + 1;
	if (n%2)
	{
		index += 2*(l/2);
	}
	else
	{
		if (l == 0) 
		{
			return index;
		} else {
			index += (1 + 2*((l/2) - 1));
		}
	}
	return ((j == (2*l - 1)) ? index : index + 1);
}
#endif
