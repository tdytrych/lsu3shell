#include <SU3ME/ScalarOperators2SU3Tensors.h>

bool CalculatebdbMscheme::operator()(const int np, const int lp, const int jp, const int mp, const int n, const int l, const int j, const int m, MatrixElementValue& matrix_element) const
{
	bool bNonVanishing = (jp == j) & (mp == m) & (np == n); // delta_{j'j} delta_{m'm} delta_{n'n}
	matrix_element = 0;

	if (bNonVanishing == false) {
		return false;
	}

	if (np == 0)
	{
		return true;
	}

	int n1 = np - 1;
	for (int l1 = 2*n1; l1 >= 0; l1 -= 4)
	{
		for (int j1 = abs(l1 - 1); j1 <= l1 + 1; j1 += 2)
		{
			matrix_element += MINUSto((-j + j1)/2)*bd(np, lp, jp, n1, l1, j1)*b(n1, l1, j1, n, l, j);
		}
	}
	matrix_element /= (double)(j + 1);
	return true;
}

bool Calculate_r2_Mscheme::operator()(const int nf, const int llf, const int jjf, const int mmf, const int ni, const int lli, const int jji, const int mmi, MatrixElementValue& matrix_element) const
{
	static double dneg_sqrt3o2 = -1.0*sqrt(3.0)/2.0;
	bool bNonVanishing = (jjf == jji) & (mmf == mmi); //& (llf == lli);
	matrix_element = 0;

	if (bNonVanishing == false) {
		return false;
	}

	double dcoeff = dneg_sqrt3o2*MINUSto((1 + lli + jji)/2)*std::sqrt(jji + 1)*wigner6j(lli, 1, jji, jjf, 0, llf);

	int nt;
	double dsum = 0;

	nt  = nf - 1;	// ==> <nf lf || b+ || nt lt> is non vanishing
	if (nt == (ni + 1) || nt == (ni - 1))
	{
		double dA, dB, dC, dD, dWig6jT;
		for (int llt = 2*nt; llt >=0; llt -= 4)
		{
			dWig6jT = wigner6j(2, 2, 0, lli, llf, llt);

			dA = bd(nf, llf, nt, llt);
			dB =  b(nf, llf, nt, llt);

			dC = bd(nt, llt, ni, lli);
			dD =  b(nt, llt, ni, lli);
					
			dsum += dWig6jT*(dA+dB)*(dC+dD);
		}
	}

	nt = nf + 1;	// ==> <nf lf || b || nt lt> is non vanishing
	if (nt == (ni + 1) || nt == (ni - 1))
	{
		double dA, dB, dC, dD, dWig6jT;
		for (int llt = 2*nt; llt >= 0; llt -= 4)
		{
			dWig6jT = wigner6j(2, 2, 0, lli, llf, llt);

			dA = bd(nf, llf, nt, llt);
			dB = b (nf, llf, nt, llt);

			dC = bd(nt, llt, ni, lli);
			dD = b (nt, llt, ni, lli);
				
			dsum += dWig6jT*(dA+dB)*(dC+dD);
		}
	}

	matrix_element = dcoeff*dsum;
	return true;
}

double CalculateA20(int L, int n, int ll, int jj, int n1, int ll1, int jj1)
{
	const double oneOversqrt2 = 1.0/M_SQRT2;
	if (n != n1 + 2)
	{
		return 0;
	}

	double result = 0;
	int phase = (3 + L + jj1 + ll1)/2;
	double konstanta = oneOversqrt2*MINUSto(phase)*std::sqrt((2*L + 1)*(jj1 + 1))*wigner6j(ll1, 1, jj1, jj, 2*L, ll);

	int nt = n - 1;
	for (int llt = 2*nt; llt >= 0; llt -= 4)
	{
		double dwig6j = wigner6j(2, 2, 2*L, ll1, ll, llt);
		result += dwig6j*bd(n, ll, nt, llt)*bd(nt, llt, n1, ll1);
	}
	return konstanta*(std::sqrt(jj+1))*result;
}

double CalculateB02(int L, int n, int ll, int jj, int n1, int ll1, int jj1)
{
	const double oneOversqrt2 = 1.0/M_SQRT2;
	if (n != n1 - 2)
	{
		return 0;
	}

	double result = 0;
	int phase = (3 + L + jj1 + ll1)/2;
	double konstanta = oneOversqrt2*MINUSto(phase)*std::sqrt((2*L + 1)*(jj1 + 1))*wigner6j(ll1, 1, jj1, jj, 2*L, ll);

	int nt = n + 1;
	for (int llt = 2*nt; llt >= 0; llt -= 4)
	{
		double dwig6j = wigner6j(2, 2, 2*L, ll1, ll, llt);
		result += dwig6j*b(n, ll, nt, llt)*b(nt, llt, n1, ll1);
	}
	return konstanta*(std::sqrt(jj+1))*result;
}


bool Calculate_AB00_Mscheme::operator()(const int np, const int llp, const int jjp, const int mmp, const int n, const int ll, const int jj, const int mm, MatrixElementValue& matrix_element) const
{
	const double konstanta = -1.0/(std::sqrt(6*(jj + 1)));
	bool bNonVanishing = (np == n) & (llp == ll) & (jjp == jj) & (mmp == mm) & (n >= 2); // delta_{n'n;l'l;j'j;m'm} & n >= 2
	matrix_element = 0;

	if (bNonVanishing == false) {
		return false;
	}

	int n1 = n - 2;	// only one possible value of n1
	for (int L = 0; L <= 2; L += 2)
	{
		double AB = 0.0;
		for (int ll1 = 2*n1; ll1 >= 0; ll1 -= 4) // iterate over all allowed 2*l1 
		{ 
			for (int jj1 = abs(ll1 - 1); jj1 <= ll1+1; jj1 += 2) // iterate over 2j1 
			{
				double dwig6 = wigner6j(2*L, 2*L, 0, jj, jj, jj1);
				double A20 = CalculateA20(L, n, ll, jj, n1, ll1, jj1);
				double B02 = CalculateB02(L, n1, ll1, jj1, n, ll, jj);
				AB += dwig6*A20*B02;
			}
		}
		matrix_element += std::sqrt(2*L + 1)*AB;
	}
	matrix_element *= konstanta;
	return true;
}

double Calculate_AB00_TwoBody_Mscheme::operator()(	
						const int n1p, const int ll1p, const int jj1p, const int mm1p, const int t1p,
						const int n2p, const int ll2p, const int jj2p, const int mm2p, const int t2p,
						const int n1, const int ll1, const int jj1, const int mm1, const int t1, 
						const int n2, const int ll2, const int jj2, const int mm2, const int t2) const
{
	bool bNonvanishing = (t1p == t1) & (t2p == t2);
	if (bNonvanishing == false)
	{
		return 0.0;
	}

	const double konstanta = 1.0/(std::sqrt(6*(jj1p + 1)*(jj2p + 1)));
	double result(0.0);
	for (int L = 0; L <= 2; L += 2)
	{
		double sum(0);
		for (int M = -L; M <= L; M++)
		{
			if (SU2::mult(jj1, 2*L, jj1p) && SU2::mult(jj2, 2*L, jj2p))
			{
				sum += MINUSto(-M)*clebschGordan(jj1, mm1, 2*L,  2*M, jj1p, mm1p)*clebschGordan(jj2, mm2, 2*L,  -2*M, jj2p, mm2p);
			}
		}

		double ABme = CalculateA20(L, n1p, ll1p, jj1p, n1, ll1, jj1)*CalculateB02(L, n2p, ll2p, jj2p, n2, ll2, jj2);
		double BAme = CalculateB02(L, n1p, ll1p, jj1p, n1, ll1, jj1)*CalculateA20(L, n2p, ll2p, jj2p, n2, ll2, jj2);

		result += sum*(ABme + BAme);
	}
	return konstanta*result;
}

struct CNoSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return true;
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return true;
	}
} noSelectionRules;

struct CLaLbSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return (n1 == n3 && n2 == n4) || (n1 == n4 && n2 == n3);
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return (su3ir.lm == 2 && su3ir.mu == 2) || (su3ir.lm == 0 && su3ir.mu == 0);
	}
} LaLbSelectionRules;



struct CrirjSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return  (su3ir.lm == 2 && su3ir.mu == 0) || (su3ir.lm == 0 && su3ir.mu == 0) || (su3ir.lm == 0 && su3ir.mu == 2);
	}
} rirjSelectionRules;

struct CAB00SelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return  (su3ir.lm == 0 && su3ir.mu == 0);
	}
} AB00SelectionRules;


struct CLSSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return (n1 == n3 && n2 == n4) || (n1 == n4 && n2 == n3);
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 2;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return (su3ir.lm == 1 && su3ir.mu == 1);
	}
} LSSelectionRules;

struct CBdBSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return true; // B+B operator tensors are composed up to four different shells and I do not know the rules
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return (su3ir.lm == 0 && su3ir.mu == 0);
	}
} BdBSelectionRules;


void Generate_2LaLb_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName)
{
	int nmax = Nmax + valence_shell;
	MatrixElementsTwoBody<Calculate_2LaLb_TwoBody_Mscheme> _2LaLbme;
	CScalarTwoBodyOperators2SU3Tensors<MatrixElementsTwoBody<Calculate_2LaLb_TwoBody_Mscheme> > Decomposer(_2LaLbme, Nmax, valence_shell, nmax);
	size_t nTensorComponents = Decomposer.PerformSU3Decomposition(LaLbSelectionRules, fileName);
}
void Generate_l1s2_p_s1l2_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName)
{
	int nmax = Nmax + valence_shell;
	MatrixElementsTwoBody<Calculate_LS_TwoBody_Mscheme> LS2Bme;
	CScalarTwoBodyOperators2SU3Tensors<MatrixElementsTwoBody<Calculate_LS_TwoBody_Mscheme> > Decomposer(LS2Bme, Nmax, valence_shell, nmax);
	size_t nTensorComponents = Decomposer.PerformSU3Decomposition(LSSelectionRules, fileName);
}
void Generate_bd1b2_p_b1bd2_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName)
{
	int nmax = Nmax + valence_shell;
	MatrixElementsTwoBody<Calculate_bd1b2_p_b1bd2_TwoBody_Mscheme> bdb2Bme;
	CScalarTwoBodyOperators2SU3Tensors<MatrixElementsTwoBody<Calculate_bd1b2_p_b1bd2_TwoBody_Mscheme> > Decomposer(bdb2Bme, Nmax, valence_shell, nmax);
	size_t nTensorComponents = Decomposer.PerformSU3Decomposition(BdBSelectionRules, fileName);
}

void Generate_rirj_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName)
{
	int nmax = Nmax + valence_shell;
	MatrixElementsTwoBody<Calculate_rirj_TwoBody_Mscheme> rirj2Bme;
	CScalarTwoBodyOperators2SU3Tensors<MatrixElementsTwoBody<Calculate_rirj_TwoBody_Mscheme> > Decomposer(rirj2Bme, Nmax, valence_shell, nmax);
	size_t nTensorComponents = Decomposer.PerformSU3Decomposition(rirjSelectionRules, fileName);
}

void Generate_AB00_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName)
{
	int nmax = Nmax + valence_shell;
	MatrixElementsTwoBody<Calculate_AB00_TwoBody_Mscheme> AB002Bme;
	CScalarTwoBodyOperators2SU3Tensors<MatrixElementsTwoBody<Calculate_AB00_TwoBody_Mscheme> > Decomposer(AB002Bme, Nmax, valence_shell, nmax);
	size_t nTensorComponents = Decomposer.PerformSU3Decomposition(AB00SelectionRules, fileName);
}


void L2OneBodyOperator2SU3(int maxShell, const std::string& fileName)
{
	MatrixElementOneBody<CalculateL2Mscheme> L21Bme;
	COneBodyOperators2SU3Tensors<MatrixElementOneBody<CalculateL2Mscheme> > Decomposer(L21Bme, maxShell);
	Decomposer.PerformSU3Decomposition(fileName);
}

void LSOneBodyOperator2SU3(int maxShell, const std::string& fileName)
{
	MatrixElementOneBody<CalculateLSMscheme> LS1Bme;
	COneBodyOperators2SU3Tensors<MatrixElementOneBody<CalculateLSMscheme> > Decomposer(LS1Bme, maxShell);
	Decomposer.PerformSU3Decomposition(fileName);
}

void bdbOneBodyOperator2SU3(int maxShell, const std::string& fileName)
{
	MatrixElementOneBody<CalculatebdbMscheme> bdb1Bme;
	COneBodyOperators2SU3Tensors<MatrixElementOneBody<CalculatebdbMscheme> > Decomposer(bdb1Bme, maxShell);
	Decomposer.PerformSU3Decomposition(fileName);
}

void r2OneBodyOperator2SU3(int maxShell, const std::string& fileName)
{
	MatrixElementOneBody<Calculate_r2_Mscheme> r2me;
	COneBodyOperators2SU3Tensors<MatrixElementOneBody<Calculate_r2_Mscheme> > Decomposer(r2me, maxShell);
	Decomposer.PerformSU3Decomposition(fileName);
}

void AB00OneBodyOperator2SU3(int maxShell, const std::string& fileName)
{
	MatrixElementOneBody<Calculate_AB00_Mscheme> AB00me;
	COneBodyOperators2SU3Tensors<MatrixElementOneBody<Calculate_AB00_Mscheme> > Decomposer(AB00me, maxShell);
	Decomposer.PerformSU3Decomposition(fileName);
}
