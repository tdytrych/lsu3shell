#include <SU3ME/MeEvaluationHelpers.h>

unsigned char getDifferences(const boost::container::small_vector_base<unsigned char>& distr_bra, const boost::container::small_vector_base<unsigned char>& distr_ket)
{
	assert(distr_bra.size() == distr_ket.size());
	unsigned char dA = 0;
	for (unsigned char i = 0; i < distr_bra.size(); ++i)
	{
		dA += abs((int)distr_bra[i] - (int)distr_ket[i]);
		if (dA > 4)
		{
			return dA;
		}
	}
	return dA;
}

//	transform distr_bra and distr_ket and augment gamma_bra and omega_bra according 
unsigned char TransformDistributions_SelectByDistribution(
										const CInteractionPPNN& interaction, 
										const CInteractionPN& interaction_pn, 
										SingleDistributionSmallVectorBase& distr_bra, 
										UN::SU3xSU2_VEC& gamma_bra,
										SU3xSU2_SMALL_VEC& omega_bra,	//	
										SingleDistributionSmallVectorBase& distr_ket, 
										std::vector<unsigned char>& hoShells, unsigned char& num_vacuums_ket_distr,
										std::vector<int>& phase, std::vector<CTensorGroup*>& tensorGroups,
										std::vector<int>& phase_pn, std::vector<CTensorGroup_ada*>& tensorGroups_pn)
{
	unsigned char delta; 
	unsigned char num_vacuums_bra_distr;
	SingleDistributionSmallVector transformed_distribution_bra, transformed_distribution_ket;

	Transform(distr_bra, distr_ket, hoShells, transformed_distribution_bra, transformed_distribution_ket);
	distr_bra.swap(transformed_distribution_bra);
	distr_ket.swap(transformed_distribution_ket);

	num_vacuums_bra_distr = std::count(distr_bra.begin(), distr_bra.end(), 0);
	num_vacuums_ket_distr = std::count(distr_ket.begin(), distr_ket.end(), 0);
	delta = getDifferences(distr_bra, distr_ket);

	phase.resize(0); tensorGroups.resize(0);
	if (delta <= 4)
	{
		interaction.GetRelevantTensorGroups(distr_bra, distr_ket, hoShells, tensorGroups, phase);
	}

	phase_pn.resize(0); tensorGroups_pn.resize(0);
	if (delta <= 2) // a+a can make distr_bra and distr_ket to differ by at most 2 nucleons over all ho shells 
	{
		interaction_pn.GetRelevantTensorGroups(distr_bra, distr_ket, hoShells, tensorGroups_pn, phase_pn);
	}

	if (!tensorGroups.empty() || !tensorGroups_pn.empty())
	{
		UN::SU3xSU2_VEC gamma_bra_vacuum_augmented;
		//	augment gamma_bra and omega_bra
		if (num_vacuums_bra_distr > 0)	// ==>	we must to add a vacuum shells into bra
		{
			AugmentGammaByVacuumShells(distr_bra, gamma_bra, gamma_bra_vacuum_augmented);
			AugmentOmegaByVacuumShells(distr_bra, gamma_bra_vacuum_augmented, omega_bra);	// input: omega_bra output: omega_bra !!!
			gamma_bra.swap(gamma_bra_vacuum_augmented);
		}
	}
	return delta;
}

void TransformGammaKet_SelectByGammas(
//	input
									const std::vector<unsigned char>& hoShells,
									const SingleDistributionSmallVectorBase& distr_ket, 
									const unsigned char num_vacuums_ket_distr, 
									const nucleon::Type type,
									const std::vector<int>& phase, 
									const std::vector<CTensorGroup*>& tensorGroups, 
									const std::vector<int>& phase_pn, 
									const std::vector<CTensorGroup_ada*>& tensorGroups_pn, 
									const UN::SU3xSU2_VEC& gamma_bra_vacuum_augmented, 
//	output:									
									UN::SU3xSU2_VEC& gamma_ket, 
									std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> >& selected_tensors,
									std::vector<std::pair<CRMECalculator*, unsigned int> >& selected_tensors_pn)
{
	assert(selected_tensors.empty());

	UN::SU3xSU2_VEC gamma_ket_vacuum_augmented;
	if (num_vacuums_ket_distr > 0)
	{
		AugmentGammaByVacuumShells(distr_ket, gamma_ket, gamma_ket_vacuum_augmented);
		gamma_ket.swap(gamma_ket_vacuum_augmented);
	}
//	else if (num_vacuums_ket_distr == 0) ==> no shell with vacuum has to be included ==> gamma_ket does not need to be augmented 	

	for (size_t i = 0; i < tensorGroups.size(); ++i)
	{
		tensorGroups[i]->SelectTensorsByGamma(type, gamma_bra_vacuum_augmented, gamma_ket, distr_ket, hoShells, phase[i], selected_tensors);// each RMECalculator::phase_ == vphase[i]
	}

	for (size_t i = 0; i < tensorGroups_pn.size(); ++i)
	{
		tensorGroups_pn[i]->SelectTensorsByGamma(gamma_bra_vacuum_augmented, gamma_ket, distr_ket, hoShells, phase_pn[i], selected_tensors_pn);
	}
}

void TransformOmegaKet_CalculateRME(
//	input:
										const SingleDistributionSmallVectorBase& distr_ket,										
										const UN::SU3xSU2_VEC& gamma_bra,
										const SU3xSU2_SMALL_VEC_BASE& omega_bra,
										const UN::SU3xSU2_VEC& gamma_ket,
										const unsigned char num_vacuum_shells_bra,
										const std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> >& tensors,
										const std::vector<std::pair<CRMECalculator*, unsigned int> >& tensors_pn,
//	output:										
										SU3xSU2_SMALL_VEC& omega_ket, 
										std::vector<RmeCoeffsSU3SO3CGTablePointers>& rmeCoeffs,
										std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_pn)
{
	if (num_vacuum_shells_bra > 0)
	{
		AugmentOmegaByVacuumShells(distr_ket, gamma_ket, omega_ket);
	}

	for (size_t i = 0; i < tensors.size(); ++i)
	{
		//	return pointer to dynamicallty allocated array of rmes	==> it has to be deleted after it has been used
		//	rme is deleted in Reset_rmeCoeffs(vector<RmeCoeffsSU3SO3CGTablePointers> ) function
		SU3xSU2::RME* pRME = tensors[i].first->CalculateRME(gamma_bra, omega_bra, gamma_ket, omega_ket);
		if (pRME)
		{
			rmeCoeffs.push_back(RmeCoeffsSU3SO3CGTablePointers(pRME, tensors[i].second, tensors[i].first->m_pSU3SO3CGTable));
		}
	}

	for (size_t i = 0; i < tensors_pn.size(); ++i)
	{
		SU3xSU2::RME* pRME_pn = tensors_pn[i].first->CalculateRME(gamma_bra, omega_bra, gamma_ket, omega_ket);
		if (pRME_pn)
		{
			rme_index_pn.push_back(std::make_pair(pRME_pn, tensors_pn[i].second));
		}
	}
}

void Reset_rmeIndex(std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_pointer_index)
{
	if (!rme_pointer_index.empty())
	{
		for (size_t i = 0; i < rme_pointer_index.size(); ++i)
		{
			delete []rme_pointer_index[i].first->m_rme;	//	allocated in CRMECalculator::CalculateRME
			delete rme_pointer_index[i].first;
		}
		rme_pointer_index.resize(0);
	}
}


void Reset_rmeCoeffs(std::vector<RmeCoeffsSU3SO3CGTablePointers>& rmeCoeffs)
{
	if (!rmeCoeffs.empty())
	{
		for (size_t i = 0; i < rmeCoeffs.size(); ++i)
		{
			delete []rmeCoeffs[i].rmes_->m_rme;	//	allocated in CRMECalculator::CalculateRME
			delete rmeCoeffs[i].rmes_;
		}
		rmeCoeffs.resize(0);
	}
}

void Reset_rmeCoeffs(std::vector<MECalculatorData>& rmeCoeffs)
{
	if (!rmeCoeffs.empty())
	{
		for (size_t i = 0; i < rmeCoeffs.size(); ++i)
		{
			delete []rmeCoeffs[i].rmes_->m_rme;	//	allocated in CRMECalculator::CalculateRME
			delete rmeCoeffs[i].rmes_;
		}
		rmeCoeffs.resize(0);
	}
}


void InitializeIdenticalOperatorRME(SU3xSU2::RME& identityOperatorRME, const size_t ntotal_max /*default: 1024 */)
{
	identityOperatorRME.m_rme = new SU3xSU2::RME::DOUBLE[ntotal_max]; 
	identityOperatorRME.m_tensor_max = identityOperatorRME.m_rhot_max = identityOperatorRME.m_n2 = 1;
	identityOperatorRME.Tensor = SU3xSU2::LABELS(1, 0, 0, 0);
}

void CreateIdentityOperatorRME(const SU3xSU2::LABELS& bra, const SU3xSU2::LABELS& ket, size_t amax, SU3xSU2::RME& identityOperatorRME)
{
	identityOperatorRME.m_ntotal = amax*amax;
	identityOperatorRME.m_bra_max = identityOperatorRME.m_ket_max = identityOperatorRME.m_n3 = amax;
	
	identityOperatorRME.Bra = bra;
	identityOperatorRME.Ket = ket;

	memset(identityOperatorRME.m_rme, 0, identityOperatorRME.m_ntotal*sizeof(SU3xSU2::RME::DOUBLE));	// set all elements of m_rme = 0.0
	for (size_t i = 0, index = 0; i < amax; ++i, index += (amax+1))
	{
		identityOperatorRME.m_rme[index] = 1.0;
	}
}

void Calculate_Proton_x_Identity_MeData(	const SU3xSU2::LABELS& bra, 
											const SU3xSU2::LABELS& ket,
											const std::vector<RmeCoeffsSU3SO3CGTablePointers>& protonMeData, 
											const SU3xSU2::RME& neutronOperatorRME, 
											std::vector<MECalculatorData>& proton_neutronMeData)
{
//	iterate over structure with pairs of {< ap (lmp mup)Sp ||| T^a0 (lm0 mu0)S0 ||| ap' (lmp' mup')Sp'>_rho, {c^rho0 k0, .... , c^rho0max k0max} }
	for (size_t i = 0; i < protonMeData.size(); ++i)
	{
//	check whether omega_pn_ket == (lmi mui)Si x Tensor == (lm0 mu0)S0 ---> Bra == (lmf muf)Sf
		if (SU2::mult(ket.S2, protonMeData[i].rmes_->Tensor.S2, bra.S2) && SU3::mult(ket, protonMeData[i].rmes_->Tensor, bra))
		{
//	calculate r.m.e. < af (lmf muf)Sf ||| T^(lm0 mu0)S0_{pp} ||| ai (lmi mui)Si> 
//	where af(ai) is an index of a total multiplicity generated by all
//	possible combinations of proton, neutron multiplicities and multiplicity
//	rho due to the coupling final proton and neutron SU(3)xSU(2),
//	ap, an, and rhof in {ap (lmp mup)Sp x an(lmn mun)Sn} rhof (lmf muf)Sf
//	(ap' an' rhoi in {ap' (lmp' mup')Sp'  x an'(lmn mun)Sn} rhof (lmf muf)Sf
			SU3xSU2::RME* prme = new SU3xSU2::RME(bra, protonMeData[i].rmes_->Tensor, ket, protonMeData[i].rmes_, &neutronOperatorRME, NULL);
			WigEckSU3SO3CG* pWigEckSU3SO3CG = protonMeData[i].pointer_SU3SO3CGTable_->GetWigEckSU3SO3CG(bra, ket);
			proton_neutronMeData.push_back(MECalculatorData(prme, pWigEckSU3SO3CG, protonMeData[i].coeffs_, protonMeData[i].pointer_SU3SO3CGTable_->m_LL0));
		}
	}
}

void Calculate_Identity_x_Neutron_MeData(	const SU3xSU2::LABELS& bra, 
											const SU3xSU2::LABELS& ket,
											const SU3xSU2::RME& protonOperatorRME, 
											const std::vector<RmeCoeffsSU3SO3CGTablePointers>& neutronMeData, 
											std::vector<MECalculatorData>& proton_neutronMeData)
{
//	iterate over structure with pairs of {< ap (lmp mup)Sp ||| T^a0 (lm0 mu0)S0 ||| ap' (lmp' mup')Sp'>_rho, {c^rho0 k0, .... , c^rho0max k0max} }
	for (size_t i = 0; i < neutronMeData.size(); ++i)
	{
//	check whether omega_pn_ket == (lmi mui)Si x Tensor == (lm0 mu0)S0 ---> Bra == (lmf muf)Sf
		if (SU2::mult(ket.S2, neutronMeData[i].rmes_->Tensor.S2, bra.S2) && SU3::mult(ket, neutronMeData[i].rmes_->Tensor, bra)) 
		{
//	calculate r.m.e. < af (lmf muf)Sf ||| T^(lm0 mu0)S0_{pp} ||| ai (lmi mui)Si> 
//	where af(ai) is an index of a total multiplicity generated by all
//	possible combinations of proton, neutron multiplicities and multiplicity
//	rho due to the coupling final proton and neutron SU(3)xSU(2),
//	ap, an, and rhof in {ap (lmp mup)Sp x an(lmn mun)Sn} rhof (lmf muf)Sf
//	(ap' an' rhoi in {ap' (lmp' mup')Sp'  x an'(lmn mun)Sn} rhof (lmf muf)Sf
			SU3xSU2::RME* prme = new SU3xSU2::RME(bra, neutronMeData[i].rmes_->Tensor, ket, &protonOperatorRME, neutronMeData[i].rmes_, NULL);
			WigEckSU3SO3CG* pWigEckSU3SO3CG = neutronMeData[i].pointer_SU3SO3CGTable_->GetWigEckSU3SO3CG(bra, ket);
			proton_neutronMeData.push_back(MECalculatorData(prme, pWigEckSU3SO3CG, neutronMeData[i].coeffs_, neutronMeData[i].pointer_SU3SO3CGTable_->m_LL0));
		}
	}
}

void CalculatePNInteractionMeData(const CInteractionPN& interactionPN,
									const SU3xSU2::LABELS& bra, 
									const SU3xSU2::LABELS& ket,
									const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_p,
									const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_n, 
									std::vector<MECalculatorData>& rmeCoeffsPNPN)
{
   // This function assumes
   // 1) L0 == S0 
   // 2) coefficients depend on rho0 and k0
   assert(k_dependent_tensor_strenghts);
	size_t nTensors;
	SU3xSU2::LABELS* tensor_labels;
	TENSOR_STRENGTH* coeffs;
	WigEckSU3SO3CGTable** su3so3cgTables;

	for (size_t iproton = 0; iproton < rme_index_p.size(); ++iproton)
	{
		for (size_t ineutron = 0; ineutron < rme_index_n.size(); ++ineutron)
		{
			nTensors = interactionPN.GetTensorsCoefficientsLabels(rme_index_p[iproton].second, rme_index_n[ineutron].second, tensor_labels, su3so3cgTables, coeffs);
			for (size_t irrep = 0; irrep < nTensors; ++irrep)
			{
				if (SU2::mult(ket.S2, tensor_labels[irrep].S2, bra.S2) && SU3::mult(ket, tensor_labels[irrep], bra)) 
				{
					SU3xSU2::RME* prme = new SU3xSU2::RME(bra, tensor_labels[irrep], ket, rme_index_p[iproton].first, rme_index_n[ineutron].first, NULL);
					WigEckSU3SO3CG* pWigEckSU3SO3CG = su3so3cgTables[irrep]->GetWigEckSU3SO3CG(bra, ket);
					rmeCoeffsPNPN.push_back(MECalculatorData(prme, pWigEckSU3SO3CG, coeffs, su3so3cgTables[irrep]->m_LL0));
				}
				coeffs += tensor_labels[irrep].rho * SU3::kmax(tensor_labels[irrep], tensor_labels[irrep].S2/2);
			}
		}
	}
}

// Same functionality as CalculatePNInteractionMeData but for tensors that have only SU(3)xSU(2) quantum labels. 
// That is, k0 L0 J0 M0 are not specified and strenght coefficients of tensors depend only on rho0
void CalculatePNOperatorRMECoeffs(const CInteractionPN& interactionPN,
									const SU3xSU2::LABELS& bra, 
									const SU3xSU2::LABELS& ket,
									const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_p,
									const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_n, 
									std::vector<MECalculatorData>& rmeCoeffsPNPN)
{
   // As tensor does not depend on L0, it is a redundant parameter of MECalculator constructor.
   const int dummyL0 = -1;
	size_t nTensors;
	SU3xSU2::LABELS* tensor_labels;
	TENSOR_STRENGTH* coeffs;
	WigEckSU3SO3CGTable** su3so3cgTables = nullptr;
	WigEckSU3SO3CG* pWigEckSU3SO3CG = nullptr;

	for (size_t iproton = 0; iproton < rme_index_p.size(); ++iproton)
	{
		for (size_t ineutron = 0; ineutron < rme_index_n.size(); ++ineutron)
		{
			nTensors = interactionPN.GetTensorsCoefficientsLabels(rme_index_p[iproton].second, rme_index_n[ineutron].second, tensor_labels, su3so3cgTables, coeffs);
			for (size_t irrep = 0; irrep < nTensors; ++irrep)
			{
				if (SU2::mult(ket.S2, tensor_labels[irrep].S2, bra.S2) && SU3::mult(ket, tensor_labels[irrep], bra)) 
				{
					SU3xSU2::RME* prme = new SU3xSU2::RME(bra, tensor_labels[irrep], ket, rme_index_p[iproton].first, rme_index_n[ineutron].first, NULL);
					rmeCoeffsPNPN.push_back(MECalculatorData(prme, pWigEckSU3SO3CG, coeffs, dummyL0));
				}
//				Coefficients depend only on rho0
				coeffs += tensor_labels[irrep].rho;
			}
		}
	}
}

void ShowMeCalcData(const std::vector<MECalculatorData>& rmeCoeffsPNPN)
{
	if (rmeCoeffsPNPN.empty())
	{
		return;
	}
	std::cout << "MECalculatorData:" << std::endl;
	for (size_t i = 0; i < rmeCoeffsPNPN.size(); ++i)
	{
		size_t nrmes = rmeCoeffsPNPN[i].rmes_->m_ntotal;
		std::cout << "rmes: ";
		for (size_t j = 0; j < nrmes; ++j)
		{
			std::cout << rmeCoeffsPNPN[i].rmes_->m_rme[j] << " ";
		}
		size_t ncoeffs = 1;
		std::cout << "\tcoeffs: ";
		for (size_t j = 0; j < ncoeffs; ++j)
		{
			std::cout << rmeCoeffsPNPN[i].coeffs_[j] << " ";
		}
		std::cout << std::endl;
	}
}


