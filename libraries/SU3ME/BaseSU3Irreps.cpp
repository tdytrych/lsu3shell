#include <SU3ME/BaseSU3Irreps.h>
/*
CBaseSU3IrrepsFast::CBaseSU3IrrepsFast (const size_t Z, const size_t N, const size_t Nmax)
{
	CUNMaster UNMaster;
//	we need to include only base SU(3) irreps for
//	the number of fermions = max(#protons, #neutron);
	size_t A = std::max(Z, N);
	std::vector<unsigned> vMax;

	std::cout << std::endl << "HO shell\tNumber of Fermions" << std::endl;

	CShellConfigurations Confs(Nmax, A);
	Confs.GetMaxFermionsShells(vMax);
	m_LastShell = Confs.GetMaxShellNumber();
	m_Data.reserve(vMax.size());
	for (size_t n = 0; n <= m_LastShell; ++n)
	{
		unsigned maxA;
		maxA = vMax[n];
		SINGLE_SHELL_IRREPS SingleShellIrreps;
		SingleShellIrreps.reserve(maxA);
		std::cout << n << "\t\t"; std::cout.flush();
		for (int A = 1; A <= vMax[n]; ++A)
		{
			std::cout << A; std::cout.flush();
			IRREPS_FOR_A_FERMIONS SpinIrreps;
			if (A == 1) // single fermion ===> 1(n 0)1/2
			{
				SpinIrreps.push_back(std::make_pair(SU2::LABEL(1), UN::SU3_VEC(1, UN::SU3(1, n, 0))));
			}
			else if (A == (n+1)*(n+2)) // single fermion ===> 1(n 0)1/2
			{
				SpinIrreps.push_back(std::make_pair(SU2::LABEL(0), UN::SU3_VEC(1, UN::SU3(1, 0, 0))));
			}
			else
			{
				UNMaster.GetAllowedSU3xSU2Irreps(n, A, SpinIrreps);
			}
			for (size_t iS = 0; iS < SpinIrreps.size(); ++iS)
			{
				if (SpinIrreps[iS].second.size() > 1) // we don't need to sort vector with one element
				{
					UN::SU3_VEC& Irreps = SpinIrreps[iS].second; 
					std::sort(Irreps.begin(), Irreps.end(), OrderSU3ByC2());
				}
			}
			SingleShellIrreps.push_back(SpinIrreps);
			std::cout << "  ";
		}
		m_Data.push_back(SingleShellIrreps);
		std::cout << std::endl;
	}
}
*/



CBaseSU3Irreps::CBaseSU3Irreps (const size_t Z, const size_t N, const size_t Nmax):Nmax_(Nmax)
{
	CUNMaster UNMaster;
//	we need to include only base SU(3) irreps for
//	the number of fermions = max(#protons, #neutron);
	size_t A = std::max(Z, N);
	std::vector<unsigned> vMax;

//	std::cout << std::endl << "HO shell\tNumber of Fermions" << std::endl;

	CShellConfigurations Confs(Nmax, A);

	const std::vector<tSHELLSCONFIG>& valence_confs = Confs.GetConfigurations(0);
	assert(valence_confs.size() == 1);

	int number_nucleons;
	for (int ho_shell = 0; ho_shell < valence_confs[0].size(); ++ho_shell)
	{ 
		number_nucleons = valence_confs[0][ho_shell];
		if (number_nucleons > 0)
		{
			valence_shell_ = ho_shell;
		}
	}

	Confs.GetMaxFermionsShells(vMax);
	m_LastShell = Confs.GetMaxShellNumber();
	m_Data.reserve(vMax.size());
	for (size_t n = 0; n <= m_LastShell; ++n)
	{
		unsigned maxA;
		maxA = vMax[n];
		SINGLE_SHELL_IRREPS SingleShellIrreps;
		SingleShellIrreps.reserve(maxA);
//		std::cout << n << "\t\t"; std::cout.flush();
		for (int A = 1; A <= vMax[n]; ++A)
		{
//			std::cout << A; std::cout.flush();
			IRREPS_FOR_A_FERMIONS SpinIrreps;
			if (A == 1) // single fermion ===> 1(n 0)1/2
			{
				SpinIrreps.push_back(std::make_pair(SU2::LABEL(1), UN::SU3_VEC(1, UN::SU3(1, n, 0))));
			}
			else if (A == (n+1)*(n+2)) // single fermion ===> 1(n 0)1/2
			{
				SpinIrreps.push_back(std::make_pair(SU2::LABEL(0), UN::SU3_VEC(1, UN::SU3(1, 0, 0))));
			}
			else
			{
				UNMaster.GetAllowedSU3xSU2Irreps(n, A, SpinIrreps);
			}
			for (size_t iS = 0; iS < SpinIrreps.size(); ++iS)
			{
				if (SpinIrreps[iS].second.size() > 1) // we don't need to sort vector with one element
				{
					UN::SU3_VEC& Irreps = SpinIrreps[iS].second; 
					std::sort(Irreps.begin(), Irreps.end(), OrderUNSU3ByC2());
				}
			}
			SingleShellIrreps.push_back(SpinIrreps);
//			std::cout << "  ";
		}
		m_Data.push_back(SingleShellIrreps);
//		std::cout << std::endl;
	}
}

unsigned long CBaseSU3Irreps::GetNBaseIrreps() // MISLEADING NAME!!! number of {mult (lm mu) S} in the memory
{
	unsigned long nBaseIrreps = 0;
	size_t n, iA, iS, j;

	for (n = 0; n <= m_LastShell; ++n)
	{
		const SINGLE_SHELL_IRREPS SingleShellIrreps = m_Data[n];
		for (iA = 0; iA < SingleShellIrreps.size(); ++iA)
		{
			const IRREPS_FOR_A_FERMIONS& SpinIrreps = SingleShellIrreps[iA];
			for (iS = 0; iS < SpinIrreps.size(); ++iS)
			{
				const UN::SU3_VEC& SU3Irreps = SpinIrreps[iS].second;
				nBaseIrreps = nBaseIrreps + for_each(SU3Irreps.begin(), SU3Irreps.end(), CountBaseIrreps());
			}
		}
	}
	return nBaseIrreps;
}

void CBaseSU3Irreps::ShowShellOccupation()
{
	size_t n;
	size_t Amax;

	std::cout << std::endl << "HO shell\tNumber of Fermions" << std::endl;
	for (n = 0; n <= m_LastShell; ++n)
	{
		std::cout << n << "\t\t";
		Amax = GetAmax(n);
		for (int A = 1; A <= Amax; ++A)
		{
			std::cout << A << " ";
		}
		std::cout << std::endl;
	}
}


size_t CBaseSU3Irreps::GetMultiplicity(const size_t n, const size_t A, const SU2::LABEL S, int lm, int mu) const
{
	const UN::SU3_VEC& Irreps = GetIrreps(n, A, S);
	UN::SU3_VEC::const_iterator cit = std::find_if(Irreps.begin(), Irreps.end(), SU3LabelsEqualTo(lm, mu));
	if (cit == Irreps.end())
	{
		return 0;  // Irrep (lm mu) is not present ==> return 0
	}
	else
	{
		return cit->mult;
	}
}


void CBaseSU3Irreps::ShowInternalDataStructure()
{
	size_t nBytesNeeded = 0;
	size_t nIrrepsTotal = 0;
	size_t nRecordsTotal = 0;
	unsigned long maxMult = 0;
	std::cout << std::endl;
	//	iterate over HO shell number n
	for (size_t n = 0; n <= m_LastShell; ++n)
	{
		size_t nIrrepsPerShell = 0;
		size_t nRecordsPerShell = 0;
		std::cout << "n = " << n << std::endl;
//	m_Data[n] = vector whose indices i maps to the number of fermions A
//	as i + 1 = A and elements contains vector of pairs where .first=S2
//	and .second=vector of UN::SU3, each element of the latter
//	corresponding to the given number of fermions A in the HO shell n.
		const SINGLE_SHELL_IRREPS SingleShellIrreps = m_Data[n];
//		iterate over A = i1 + 1 ... the number fermions we need to consider SingleShellIrreps		
		for (int iA = 0; iA < SingleShellIrreps.size(); ++iA)
		{
			size_t nIrrepsPerA = 0;
			size_t nRecordsPerA = 0;
			std::cout << "\t\t A = " << iA + 1 << std::endl;
			const IRREPS_FOR_A_FERMIONS& SpinIrreps = SingleShellIrreps[iA];
			for (size_t iS = 0; iS < SpinIrreps.size(); ++iS)
			{
				nBytesNeeded += sizeof(SPIN_IRREPS);
				std::cout << "\t\t\t S = " << (int)SpinIrreps[iS].first << std::endl;
				const UN::SU3_VEC& SU3Irreps = SpinIrreps[iS].second;
				nBytesNeeded += sizeof(UN::SU3)*SU3Irreps.size();
				nRecordsPerA 		+= SU3Irreps.size();
				for (size_t j = 0; j < SU3Irreps.size(); ++j)
				{
					nIrrepsPerA += SU3Irreps[j].mult;
					if (SU3Irreps[j].mult > maxMult) 
					{
						maxMult = SU3Irreps[j].mult;
					}
					std::cout << "\t\t\t\t" << SU3Irreps[j].mult << "(" << (int)SU3Irreps[j].lm << " " << (int)SU3Irreps[j].mu << ")\t\t" << SU3Irreps[j].C2() << std::endl;
				}
			}
			std::cout << "\t\t\t\t--" << std::endl;
			std::cout << "\t\t\t\t" << nIrrepsPerA << " unique SU(3)xSU(2) base irreps. Due to multiplicity it can be represented by ";
			std::cout << nRecordsPerA << " {mult (lm mu) S}." << std::endl;
			nIrrepsPerShell  += nIrrepsPerA;
			nRecordsPerShell += nRecordsPerA;
		}
		std::cout << "Totally: n = " << n << "has " << nIrrepsPerShell << " unique SU(3)xSU(2) base irreps." << std::endl;
		std::cout << "This can be represented by " << nRecordsPerShell << " {mult (lm mu) S}.\t ==> sizeof(vector<UN::SU3xSU2MULT>) = ";
		std::cout << sizeof(std::vector<UN::SU3xSU2>) + sizeof(UN::SU3xSU2)*nRecordsPerShell << " bytes. ";
		std::cout << "Compare to " << nBytesNeeded << " bytes in case of the current data structure." << std::endl;
		nBytesNeeded = 0;
		nIrrepsTotal += nIrrepsPerShell;
		nRecordsTotal += nRecordsPerShell;
	}
	std::cout << std::endl << "Totally:\t" << nIrrepsTotal << " base SU(3) irreps." << std::endl;
	std::cout << std::endl << "Number of { mult (lm mu) S}:\t" << nRecordsTotal << std::endl;
	std::cout << "maximal multiplicity ... " << maxMult << std::endl;
}
