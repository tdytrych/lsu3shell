#ifndef SU3ME_MEMORY_POOL_H
#define SU3ME_MEMORY_POOL_H

#include <boost/pool/pool.hpp>

namespace memory_pool
{
    // Memory pool for instances of SU3xSU2::RME class.
    extern thread_local boost::pool<> rme;

    // Memory pool for instances of CRMECalculator class.
    extern thread_local boost::pool<> crmecalculator;
}

#endif
