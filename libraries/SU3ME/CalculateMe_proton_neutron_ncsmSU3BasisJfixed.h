#ifndef CALCULATE_ME_PROTON_NEUTRON_FIXEDJ_H
#define CALCULATE_ME_PROTON_NEUTRON_FIXEDJ_H
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h> // for definition MECalculatorData

/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_nonDiagonal_Scalar(	const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);
/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_nonDiagonal_Scalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);

/** This method is utilized by code evaluating physical observables; non-scalar means that (J0 >=0) allowed */
void CalculateME_nonDiagonal_nonScalar(	const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind); 
void CalculateME_nonDiagonal_nonScalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);
/** \todo implement! */
void CalculateME_Diagonal_UpperTriang_nonScalar(const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);
void CalculateME_Diagonal_UpperTriang_nonScalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);
/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_Diagonal_UpperTriang_Scalar(	const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);
/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_Diagonal_UpperTriang_Scalar(const uint32_t ifirst_row,	const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);
#endif
