#ifndef BASESU3IRREPS_H
#define BASESU3IRREPS_H
#include <UNU3SU3/CUNMaster.h>
#include <SU3ME/CShellConfigurations.h>
#include <cassert>
/*
class CBaseSU3IrrepsFast 
{
	public:
	typedef std::vector<UN::SU3xSU2MULT_LIST> SINGLE_SHELL_IRREPS; 
	std::vector<SINGLE_SHELL_IRREPS> m_Data; 	

	public:
	CBaseSU3IrrepsFast(const size_t Z, const size_t N, const size_t Nmax);
};
*/


//	This class stores Base SU(3) irreps which are essential for on-the-fly
//	construction of the many-body SU(3) basis. 
//
//	m_Data[n] stores a vector whose elements are the allowed SU(3)xSU(2) irreps in
//	HO shell n, for each number of nucleons A (m_Data[n][i=A-1]) needed for
//	Nmax model space calculation of nuclei with Z protons and N neutrons.
//	The allowed SU3xSU2 irreps are stored as vector of pairs with 
//	.first = S2 and .second being a vector of UN::SU3, i.e.{mult0 (lm0 mu0), mult1 (lm1 mu1), ... , mult_{max}(lm_max mu_max)}
//
//	Rationale: number of BaseSU3Irreps can become large. Allowed SU3xSU2 irreps
//	simply as a vector of UN::SU3xSU2MULT (i.e. 4 bytes for each elements)
class CBaseSU3Irreps 
{
	char Nmax_;
	char valence_shell_; // this is max(proton valence shell, neutron valence shell)
	public:
	typedef std::pair<SU2::LABEL, UN::SU3_VEC> SPIN_IRREPS; 	//	SPIN_IRREPS.first 	= S2
																//	SPIN_IRREPS.second 	= {mult0 (lm0 mu0), mult1 (lm1 mu1), ... , mult_{max}(lm_max mu_max)}
	typedef std::vector<SPIN_IRREPS> IRREPS_FOR_A_FERMIONS;		//	for A fermions we have a vector of SPIN_IRREPS 

	typedef std::vector<IRREPS_FOR_A_FERMIONS> SINGLE_SHELL_IRREPS; // SINGLE_SHELL_IRREPS[i] ... irreps for A = (i + 1) fermions

	private:									//				m_Data[1]
												//			(corresponds to n = 1)
												//	{{<S=1, {1(1 0)}>}, {<S=0, {1(2 0)}>, <S=2, {1(0 1)}>}, ..... }
												//	    m_Data[1][0]		m_Data[1][1]
												//	 {<S=1, {1(1 0)}>}   {<S=0, {1(2 0)}>, <S=2, {1(0 1)}>}
												//	(corresponds to A=1)   (corresponds to A=2)
												//		{<S=1, {1(0 0)}>}	{<S=0, {1(2 0)}>, <S=2, {1(0 1)}>}
												//		.first	.second		.first	.second
	std::vector<SINGLE_SHELL_IRREPS> m_Data; 	// { {{<S=1, {1(0 0)}>}, {<S=0, {1(0, 0)}>}}, {{<S=1, {1(1 0)}>}, {<S=0, {1(2 0)}>, <S=2, {1(0 1)}>}, }
	size_t m_LastShell; // Maximal HO shell that we include: Note: m_LastShell + 1 == m_Data.size();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct OrderUNSU3ByC2
	{	
		inline bool operator() (const UN::SU3& L, const UN::SU3& R) const { return L.C2() > R.C2();}
	};
	struct SpinEqualTo
	{
		private:
		SU2::LABEL m_S2;
		public:
		SpinEqualTo(const SU2::LABEL& S2): m_S2(S2) {}
		bool operator()(const SPIN_IRREPS& SpinIrreps) { return (SpinIrreps.first == m_S2); }
	};
	struct SU3LabelsEqualTo
	{
		private:
		int lm, mu;
		public:
		SU3LabelsEqualTo(int lm_, int mu_): lm(lm_), mu(mu_) {}
		inline bool operator()(const UN::SU3& IR) { return (IR.lm == lm && IR.mu == mu); }
	};

	struct CountBaseIrreps
	{
		private:
		unsigned long m_nIrreps;
		public:
		CountBaseIrreps(): m_nIrreps(0){}
		inline void operator()(const UN::SU3& su3mult) { m_nIrreps += su3mult.mult; }
		operator unsigned long() { return m_nIrreps; }
	};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public:
	CBaseSU3Irreps (const size_t Z, const size_t N, const size_t Nmax);
	CBaseSU3Irreps (const CBaseSU3Irreps& baseSU3Irreps):m_Data(baseSU3Irreps.m_Data), m_LastShell(baseSU3Irreps.m_LastShell) {}
	void ShowInternalDataStructure();
	void ShowShellOccupation();
	unsigned long GetNBaseIrreps();

	inline char Nmax() const  {return Nmax_;}
	inline char valence_shell() const {return valence_shell_;}

	size_t GetMultiplicity(const size_t n, const size_t A, const SU2::LABEL S, int lm, int mu) const;

	size_t GetLastShell() const {return m_LastShell;}
	inline const SINGLE_SHELL_IRREPS& GetIrreps(const size_t n) const 
	{
		assert(n <= m_LastShell); 
		return m_Data[n];
	}

	inline const IRREPS_FOR_A_FERMIONS& GetIrreps(const size_t n, const size_t A) const 
	{
		assert(n <= m_LastShell); 
		assert(m_Data[n].size() >= A);
		return (m_Data[n])[A-1];
	}

	inline bool GetIrreps(const size_t n, const size_t A, const SU2::LABEL S, UN::SU3_VEC& SU3MultList) const 
	{
		assert(n <= m_LastShell); 
		assert(m_Data[n].size() >= A);

		const IRREPS_FOR_A_FERMIONS& SpinIrreps = GetIrreps(n, A);
		IRREPS_FOR_A_FERMIONS::const_iterator cit = std::find_if(SpinIrreps.begin(), SpinIrreps.end(), SpinEqualTo(S));
		if (cit != SpinIrreps.end())
		{
			SU3MultList = cit->second;
			return true;
		} 
		else
		{
			return false;
		}
	}


	inline const UN::SU3_VEC& GetIrreps(const size_t n, const size_t A, const SU2::LABEL S) const 
	{
		assert(n <= m_LastShell); 
		assert(m_Data[n].size() >= A);

		const IRREPS_FOR_A_FERMIONS& SpinIrreps = GetIrreps(n, A);
		IRREPS_FOR_A_FERMIONS::const_iterator cit = std::find_if(SpinIrreps.begin(), SpinIrreps.end(), SpinEqualTo(S));
		assert(cit != SpinIrreps.end());
		return cit->second;
	}

	inline size_t GetAmax(const size_t n) const 
	{ 
		assert(n <= m_LastShell); 
		return m_Data[n].size();
	}

	inline void GetSpins(const size_t n, const size_t A, std::vector<SU2::LABEL>& Spins) const
	{
		assert(n <= m_LastShell); 
		assert(m_Data[n].size() >= A);
		const IRREPS_FOR_A_FERMIONS& SpinIrreps = GetIrreps(n, A);
		for (size_t i = 0; i < SpinIrreps.size(); ++i)
		{
			Spins.push_back(SpinIrreps[i].first);
		}
	}
	void GetIrreps(const size_t n, const size_t A, UN::SU3xSU2_VEC& UNSU3xSU2_Vec) const
	{
		const IRREPS_FOR_A_FERMIONS& SpinIrreps = GetIrreps(n, A);
		for (size_t i = 0; i < SpinIrreps.size(); ++i)
		{
			SU2::LABEL S2 = SpinIrreps[i].first;
			for (size_t j = 0; j < SpinIrreps[i].second.size(); ++j)
			{
				UNSU3xSU2_Vec.push_back(UN::SU3xSU2(SpinIrreps[i].second[j], S2));
			}
		}
	}
};
#endif
