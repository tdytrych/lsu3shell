#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <LookUpContainers/HashFixed.h>

class dividedpertwo
{
	public:
	inline std::size_t operator()(int const& i) const{return i/2;}
};
class dontdoanything
{
	public:
	inline std::size_t operator()(int const& i) const{return i;}
};

// Stores 9j coefficients needed when evaluating matrix elements of a scalar,
// i.e. J0 = 0, tensors. In that case we have L0 = S0 & J0 = 0 & Jf = Ji
class CWigEck9jScalarTensorLookUpTableFixedJ 
{
	public:
	typedef float DOUBLE;
	/** WIGNER_9J_TABLE[iS0] contains map of integer keys constructed by
	 *  INTEGER_KEY(\f$L_{i}, S_{i}, L_{f},S_{f}\f$) and associated
	 *  9j symbol for fixed J basis (i.e. Jf = Ji)
	 */
  	typedef HashFixed<int, DOUBLE, dontdoanything> CACHE;
	typedef CACHE* WIGNER_9J_TABLE; 
	public:
  CWigEck9jScalarTensorLookUpTableFixedJ(): wig9jTable_((WIGNER_9J_TABLE)new char[sizeof(CACHE)*5]) // S0 == 0, 2, 4 ==> we need only 3 maps //but let's not pay the division. Creating a map and not using it is free
	{ 
	  new(wig9jTable_) CACHE(4096);
	  new(wig9jTable_+2) CACHE(4096);
	  new(wig9jTable_+4) CACHE(4096);
	}

	DOUBLE GetWigner9j(SO3::LABEL LLi, SU2::LABEL SSi, SO3::LABEL LLf, SU2::LABEL SSf, SU2::LABEL SS0, SU2::LABEL JJ)
	{
		//	assert two-body operator
		assert(SS0 == 0 || SS0 == 2 || SS0 == 4);
		CACHE& wig9jS0 = wig9jTable_[SS0];	// SS0/2
		CACHE::iterator LiSiLfSfwig9j = wig9jS0.find(INTEGER_KEY(LLi, SSi, LLf, SSf));

		if (LiSiLfSfwig9j == wig9jS0.end())
		{
			DOUBLE wig9j = wigner9j(LLi, SSi, JJ, SS0, SS0, 0, LLf, SSf, JJ);
			wig9jS0.insert(INTEGER_KEY(LLi, SSi, LLf, SSf), wig9j);
			return wig9j;
		}
		else
		{
		  return *LiSiLfSfwig9j;
		}
	}

  ~CWigEck9jScalarTensorLookUpTableFixedJ()
  {
	 wig9jTable_->~CACHE();
	 (wig9jTable_+2)->~CACHE();
	 (wig9jTable_+4)->~CACHE();
	 delete[] (char*)(wig9jTable_);
  }
	private:
/** Construct integer key from the values of \f$L_{i}, S_{i}, L_{f}, S_{f}\f$. */
	inline int INTEGER_KEY(SO3::LABEL LLket, SU2::LABEL S2ket, SO3::LABEL LLbra, SU2::LABEL S2bra) const { return (LLket << 24) | (S2ket << 16) | (LLbra << 8) | S2bra; }
	WIGNER_9J_TABLE wig9jTable_;
};

/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_nonDiagonal_Scalar(	const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	static CWigEck9jScalarTensorLookUpTableFixedJ wig9jTable;

	static SU2::LABEL JJ(ket.JJ());
	static float fPiJiJ0 = std::sqrt(JJ + 1);

	assert(MECalculatorData::JJ0() == 0 && bra.JJ() == ket.JJ());


	float dPiLfSf;
	SO3::LABEL LL0;
	int S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}


// aimax*afmax*6*6 ... just an estimate.
// it should be aimax*afmax*kmaxf*kmaxi
	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each pair Jf Ji we must calculate nme matrix elements
         MeJfJi.assign(nme, 0.0);		// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         Jf          ai     ki        Ji
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

	      tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				LL0 = S0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), LL0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, (LL0 >> 1));
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, JJ);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}
			}

			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJfId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJiId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJfId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJiId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}

void CalculateME_nonDiagonal_Scalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	static CWigEck9jScalarTensorLookUpTableFixedJ wig9jTable;

	static SU2::LABEL JJ(ket.JJ());
	static float fPiJiJ0 = std::sqrt(JJ + 1);

	assert(MECalculatorData::JJ0() == 0 && bra.JJ() == ket.JJ());


	float dPiLfSf;
	SO3::LABEL LL0;
	int S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each pair Jf Ji we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0);		// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         Jf          ai     ki        Ji
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				LL0 = S0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), LL0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, (LL0 >> 1));
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, JJ);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}
			}

			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJfId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJiId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJfId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = ifirst_row + braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJiId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_Diagonal_UpperTriang_Scalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	assert(firstBraStateId == firstKetStateId);
	static CWigEck9jScalarTensorLookUpTableFixedJ wig9jTable;

	static SU2::LABEL JJ(ket.JJ());
	static float fPiJiJ0 = std::sqrt(JJ + 1);

	assert(MECalculatorData::JJ0() == 0 && bra.JJ() == ket.JJ());

	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each value of J we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalat operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, JJ);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}

			}

			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = ifirst_row + braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]) && (braStateId <= ketStateId))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}


/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_Diagonal_UpperTriang_Scalar(const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	assert(firstBraStateId == firstKetStateId);
	static CWigEck9jScalarTensorLookUpTableFixedJ wig9jTable;

	static SU2::LABEL JJ(ket.JJ());
	static float fPiJiJ0 = std::sqrt(JJ + 1);

	assert(MECalculatorData::JJ0() == 0 && bra.JJ() == ket.JJ());

	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each value of J we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

         tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalat operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, JJ);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}

			}

			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]) && (braStateId <= ketStateId))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}

/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_Diagonal_UpperTriang_nonScalar(const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)

{
	assert(firstBraStateId == firstKetStateId);
	static CWig9jLookUpTable<> wig9jTable;

	static SU2::LABEL JJf(bra.JJ());
	static SU2::LABEL JJi(ket.JJ());
	static float fPiJiJ0 = std::sqrt((JJi + 1)*(MECalculatorData::JJ0() + 1));

	assert(SU2::mult(JJi, MECalculatorData::JJ0(), JJf));

	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
/*         
         // This condition makes sense only for one- and two-body scalar interactions!
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}
*/
			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each value of J we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

         tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = MeCalcData[iTensor].rmes_->Tensor.S2;
				if (!SU2::mult(ket.L(), MeCalcData[iTensor].LL0(), bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, MeCalcData[iTensor].LL0()/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

				assert(k0_max);
				assert(rhot_max);
				assert(a0_max);

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, JJi, MeCalcData[iTensor].LL0(), S0, MECalculatorData::JJ0(), bra.L(), bra.S2, JJf);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}

			}

			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]) && (braStateId <= ketStateId))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}

/** Codes facilitating evaluation of a scalar operator scalar ==> J0=0 only */
void CalculateME_Diagonal_UpperTriang_nonScalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)

{
	assert(firstBraStateId == firstKetStateId);
	static CWig9jLookUpTable<> wig9jTable;

	static SU2::LABEL JJf(bra.JJ());
	static SU2::LABEL JJi(ket.JJ());
	static float fPiJiJ0 = std::sqrt((JJi + 1)*(MECalculatorData::JJ0() + 1));

	assert(SU2::mult(JJi, MECalculatorData::JJ0(), JJf));

	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
/*         
         // This condition makes sense only for one- and two-body scalar interactions!
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}
*/
			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each value of J we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

         tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = MeCalcData[iTensor].rmes_->Tensor.S2;
				if (!SU2::mult(ket.L(), MeCalcData[iTensor].LL0(), bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, MeCalcData[iTensor].LL0()/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

				assert(k0_max);
				assert(rhot_max);
				assert(a0_max);

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, JJi, MeCalcData[iTensor].LL0(), S0, MECalculatorData::JJ0(), bra.L(), bra.S2, JJf);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}

			}

			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = ifirst_row + braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]) && (braStateId <= ketStateId))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}



/** This method is utilized by the code evaluating physical observables; non-scalar means that (J0 >=0) allowed.
 	It was tested for Hamiltonian though and hence needs testing for non-scalar operator. 
*/
void CalculateME_nonDiagonal_nonScalar(	const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	static CWig9jLookUpTable<> wig9jTable;

	static SU2::LABEL JJf(bra.JJ());
	static SU2::LABEL JJi(ket.JJ());
	static float fPiJiJ0 = std::sqrt((JJi + 1)*(MECalculatorData::JJ0() + 1));

	assert(SU2::mult(JJi, MECalculatorData::JJ0(), JJf));
	assert(bra.JJ() == JJf && ket.JJ() == JJi);

	float dPiLfSf;
	int S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
/*         
         // This condition makes sense only for one- and two-body scalar interactions!
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}
*/
			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each pair Jf Ji we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0);		// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         Jf          ai     ki        Ji
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

         tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), MeCalcData[iTensor].LL0(), bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, (MeCalcData[iTensor].LL0() >> 1));
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, JJi, MeCalcData[iTensor].LL0(), S0, MECalculatorData::JJ0(), bra.L(), bra.S2, JJf);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}
			}

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJfId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJiId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJfId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJiId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}

/** This method is utilized by the code evaluating physical observables; non-scalar means that (J0 >=0) allowed.
 	It was tested for Hamiltonian though and hence needs testing for non-scalar operator. 
*/
void CalculateME_nonDiagonal_nonScalar(const uint32_t ifirst_row, const size_t afmax, SU3xSU2::BasisJfixed& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJfixed& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	static CWig9jLookUpTable<> wig9jTable;

	static SU2::LABEL JJf(bra.JJ());
	static SU2::LABEL JJi(ket.JJ());
	static float fPiJiJ0 = std::sqrt((JJi + 1)*(MECalculatorData::JJ0() + 1));

	assert(SU2::mult(JJi, MECalculatorData::JJ0(), JJf));
	assert(bra.JJ() == JJf && ket.JJ() == JJi);

	float dPiLfSf;
	int S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

	std::vector<float> MeJfJi(aimax*afmax*32);
	std::vector<float> tensorMe(aimax*afmax*32);

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
/*         
         // This condition makes sense only for one- and two-body scalar interactions!
			if (abs((bra.L() - ket.L())) > 4)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}
*/
			nme = bra.kmax()*ket.kmax()*afmax*aimax;	// for each pair Jf Ji we must calculate nme matrix elements
			MeJfJi.assign(nme, 0.0);		// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
														//			  |      |          |          |      |  	     |
														//			  |      |          |          |      |  	     |
														//			  V      V          V          V      V  	     V
														//			  af     kf         Jf          ai     ki        Ji
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

         tensorMe.resize(nme);
			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), MeCalcData[iTensor].LL0(), bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, (MeCalcData[iTensor].LL0() >> 1));
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);

				float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, JJi, MeCalcData[iTensor].LL0(), S0, MECalculatorData::JJ0(), bra.L(), bra.S2, JJf);
				if (Negligible8(wigner9j))
				{
					continue;
				}
				for (size_t i = 0; i < nme; ++i)
				{
					MeJfJi[i] += wigner9j*tensorMe[i];
				}
			}

//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			dCoeff = fPiJiJ0*dPiLfSf;
			for (size_t i = 0; i < nme; MeJfJi[i++] *= dCoeff);

			index_me = 0;
			for (int kf = 0; kf < bra.kmax(); ++kf)
			{
				size_t kfLfJfId = bra.getId(kf);
				for (int ki = 0; ki < ket.kmax(); ++ki)
				{
					size_t kiLiJiId = ket.getId(ki);
					for (int af = 0; af < afmax; ++af)
					{
						IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJfId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
						size_t irow_relative = ifirst_row + braStateId - firstBraStateId;
						for (int ai = 0; ai < aimax; ++ai, ++index_me)
						{
							IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJiId*aimax + ai);
							if (!Negligible6(MeJfJi[index_me]))
							{
								vals[irow_relative].push_back(MeJfJi[index_me]);
								col_ind[irow_relative].push_back(ketStateId);
							}
						}
					}
				}
			}
		}
	}
}
