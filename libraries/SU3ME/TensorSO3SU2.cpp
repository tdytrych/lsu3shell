#include <SU3ME/TensorSO3SU2.h>

#include <functional>
#include <algorithm>
#include <numeric>
#include <iostream>

typedef std::pair<K1L1K2L2K3L3, std::vector<double> > SU3CGdataItem;

struct less_K3L3 
{
	bool operator()(const SU3CGdataItem& lhs, const SU3CGdataItem& rhs)
	{
		return lhs.first.k3() < rhs.first.k3() || (lhs.first.k3() == rhs.first.k3() && lhs.first.l3() < rhs.first.l3());
	}
	bool operator()( const SU3CGdataItem& lhs, const K1L1K2L2K3L3& rhs ) const 
	{ 
		return lhs.first.k3() < rhs.k3() || (lhs.first.k3() == rhs.k3() && lhs.first.l3() < rhs.l3()); 
	};
   	bool operator()( const K1L1K2L2K3L3& lhs, const SU3CGdataItem& rhs ) const 
	{
		return lhs.k3() < rhs.first.k3() || (lhs.k3() == rhs.first.k3() && lhs.l3() < rhs.first.l3()); 
	};
};

struct Resizer
{
	private:
	size_t nElems;
	public:
	Resizer(size_t n): nElems(n) {};
	void operator() (std::vector<double>& v)
	{
		v.resize(nElems);
		std::vector<double>(v).swap(v);
	}
};

namespace SU3xSU2 {
	namespace SO3SU2 {

SPS_INDEX StartIndex[OSCILATOR_MAX]={1,3,9,21,41,71,113,169,241,331,441,573,729,911,1121,1361,1633,1939,2281,2661,3081,3543,4049,4601,5201,5851,6553,7309,8121,8991,9921,10913,11969};


void GetStateLabels(unsigned short Index, int& N, int& l, int& j, int& mj)
{
	for (N = 0;(N+1) < OSCILATOR_MAX && (Index - StartIndex[N+1]) >=0; N++);
	Index -= StartIndex[N];
	l = (N%2) ? 1 : 0;
	while ((Index - (4*l+2)) >= 0) {
		Index -= (4*l+2);
		l +=2;
	}
	j = abs(2*l-1); // if l = 0 => j = 1
	if ((Index - (j+1)) >= 0) {
		Index -= j+1;
		j = 2*l+1;
	}
	mj = 2*Index - j;
}

SPS_INDEX GetIndex(const int n, const int l, const int jj, const int mm)
{
	SPS_INDEX index = StartIndex[n];
	for (int tmpl = (n%2); tmpl < l; tmpl += 2)
	{
		index += 4*tmpl+2;
	}
	if (jj == 2*l+1) 
	{
		index += (jj-1);
	}
	return ((index + (jj + mm)/2));
}


// This constructore creates Tensors representing single creation/annihilation operator
Tensor::Tensor(const SU3xSU2::LABELS& RhoLmMuS2, OperatorType Type): SU3xSU2::LABELS(RhoLmMuS2)
{
	m_nBasisOperators = 1; 
	m_max_rho = 1;
	m_OperatorType = Type;
	m_Structure.push_back((Type == CREATION) ? (RhoLmMuS2.lm + 1) : -1*(RhoLmMuS2.mu + 1));

	size_t n = abs(m_Structure[0])-1;
	size_t nsps = nSPS(n);
	if (nsps > nspsmax) 
	{
		std::cerr << "Construction of tensors for harmonic ocillator shell n=" << (int)abs(m_Structure[0])-1 << " has not been implemented yet." << std::endl;
		exit(EXIT_FAILURE);
	}
	m_annihilStart = (m_Structure[0] > 0); 	//	if m_Structure[0] > 0 ==> Tensor == a+ and m_annihilStart = 1. As a consequence (m_Structure.size()==1)-m_annihilStart = 0
											//	and hence method GetNumberOfAnnihilators() returns 0 in case a+ tensor
//	alternatively, one can also use std::find_if, but that is too cumbersome in this trivial case											
//	m_annihilStart = std::find_if(m_Structure.begin(), m_Structure.end(), std::bind2nd(std::less<char>(), 0)) - m_Structure.begin();
}

Tensor::Tensor(	const Tensor& X, const size_t rhoX, 
				const Tensor& Y, const size_t rhoY, 
				const SU3xSU2::LABELS& ResultXY, bool fDiagnostic): SU3xSU2::LABELS(ResultXY)
{
	if (!SU2::mult(X.S2, Y.S2, S2))
	{
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  intrinsic spin does not couple (" << (int)X.S2 << "/2 x " << (int)Y.S2 << "/2 -> " << (int)S2 << "/2)" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_max_rho = SU3::mult(X, Y, *this);
	if (!m_max_rho) {
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  (" << (int)X.lm << " " << (int)X.mu;
		std::cerr << ") and (" << (int)Y.lm << " " << (int)Y.mu << ") does not couple to ";
		std::cerr << "(" << (int)ResultXY.lm << " " << (int)ResultXY.mu << ")" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_nBasisOperators = X.GetNBasisOperators() + Y.GetNBasisOperators();
	m_OperatorType = GENERAL;
	m_Structure.insert(m_Structure.begin(), X.m_Structure.begin(), X.m_Structure.end());
	m_Structure.insert(m_Structure.end(), Y.m_Structure.begin(), Y.m_Structure.end());

	assert(m_nBasisOperators == m_Structure.size());

//	find the first annihilation operator in m_Structure, that is, the first
//	negative element
	m_annihilStart = std::find_if(m_Structure.begin(), m_Structure.end(), std::bind2nd(std::less<char>(), 0)) - m_Structure.begin();

//	22.05.2010: I do not understand the logic of the following comment and setting rho = -1 ==> I decided to comment rho = -1 line.
//
// 	30.05.2010: This constructor generates all tensor components for all multiplicities rho
// 	and hence we set rho = -1
//	rho = -1; 
//	However, in some logic rho also corresponds to rho_max ... and hence it should
//	be in this case equal to m_max_rho.
//	At least in some part of code I was using Tensor.rho to get rho0_max and it failed since Tensor.rho = -1.	
//	It is probably safer to set:
	rho = m_max_rho;
	std::vector<SU3xSU2::PHYSICALJ> vXYlabels;
//	Generate physical J-coupled basis
	SU3xSU2::GetPhysicalJBasis(ResultXY, vXYlabels);
	GenerateComponents(X, rhoX, Y, rhoY, vXYlabels, fDiagnostic);
}

Tensor::Tensor( const Tensor& X,	const size_t rhoX, 
				const Tensor& Y,	const size_t rhoY, 
				const SU3xSU2::LABELS& ResultXY,
				const std::vector<SU3xSU2::PHYSICALJ>& Components2Costruct, bool fDiagnostic): SU3xSU2::LABELS(ResultXY)
{
	if (!SU2::mult(X.S2, Y.S2, S2))
	{
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  intrinsic spin does not couple." << std::endl;
		exit(EXIT_FAILURE);
	}
	m_max_rho = SU3::mult(X, Y, *this);
	if (!m_max_rho) {
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  (" << X.lm << " " << X.mu;
		std::cerr << ") and (" << Y.lm << " " << Y.mu << ") does not couple to ";
		std::cerr << "(" << ResultXY.lm << " " << ResultXY.mu << ")" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_nBasisOperators = X.GetNBasisOperators() + Y.GetNBasisOperators();
	m_OperatorType = GENERAL;
	m_Structure.insert(m_Structure.begin(), X.m_Structure.begin(), X.m_Structure.end());
	m_Structure.insert(m_Structure.end(), Y.m_Structure.begin(), Y.m_Structure.end());

//	find the first annihilation operator in m_Structure, that is, the first
//	negative element
	m_annihilStart = std::find_if(m_Structure.begin(), m_Structure.end(), std::bind2nd(std::less<char>(), 0)) - m_Structure.begin();

//	22.05.2010: I do not understand the logic of the following comment and setting rho = -1 ==> I decided to comment rho = -1 line.
//
// 	We generate tensor components provided in the list Components2Costruct for
// 	all multiplicities rho and hence we set rho = -1 since it is redundant
//	rho = -1; 
//	However, in some logic rho also corresponds to rho_max ... and hence it should
//	be in this case equal to m_max_rho.
//	At least in some part of code I was using Tensor.rho to get rho0_max and it failed since Tensor.rho = -1.	
//	It is probably safer to set:
	rho = m_max_rho;
	GenerateComponents(X, rhoX, Y, rhoY, Components2Costruct, fDiagnostic);
}



void Tensor::GenerateComponents(const Tensor& X, const size_t rhoX, 	
								const Tensor& Y, const size_t rhoY, 
								const std::vector<SU3xSU2::PHYSICALJ>& Components2Costruct, bool fDiagnostic)
{
	if (!m_TensorComponents.empty())
	{
		m_TensorComponents.clear();
	}

	std::vector<SU3CGdataItem> vSU3CGs;
	std::pair<std::vector<SU3CGdataItem>::const_iterator, std::vector<SU3CGdataItem>::const_iterator> SU3CGRange;
	std::vector<SU3CGdataItem>::const_iterator SU3CGdataIter;
	K1L1K2L2K3L3 k3L3toFind;
	double dSpinCG, dCG;
	std::vector<double> Coeff(m_max_rho, 0.0);
	size_t iSpin, nSpinCG, irho, j, nTensorComponents;
	SU3xSU2::PHYSICALJ Xlabels, Ylabels;
	CSU3CGMaster SU3CGFactory;

//	Calculate SU(3) CG's
	SU3CGFactory.GetSO3(X, Y, *this, vSU3CGs);
//	sort SU(3) CG's according to {k3, L3}
	std::sort(vSU3CGs.begin(), vSU3CGs.end(), less_K3L3());

	nTensorComponents = Components2Costruct.size();
//	iterate over Components2Costruct {k0 l0 M_l0 Sigma0}
	for (size_t i = 0; i < nTensorComponents; ++i)
	{
		TENSOR_COMPONENT_ALL_RHO CurrentOperator;
//		make sure enough memory is allocated for m_max_rho	vectors
		CurrentOperator.first.resize(m_max_rho);

		k3L3toFind.k3(Components2Costruct[i].k);
		k3L3toFind.l3(Components2Costruct[i].ll);

		SU3CGRange = std::equal_range(vSU3CGs.begin(), vSU3CGs.end(), k3L3toFind, less_K3L3());

		if (fDiagnostic) std::cout << "Tensor: {" << Components2Costruct[i] << "}:" << std::endl;

		double PiSL = std::sqrt((S2 + 1)*(Components2Costruct[i].ll + 1));
//	iterate over k1 l1 k2 l2
		for (SU3CGdataIter = SU3CGRange.first; SU3CGdataIter != SU3CGRange.second; ++SU3CGdataIter)
		{
//	SU3CG contains a vector of SU(3) CG's with m_max_rho elements
			const std::vector<double>& SU3CG = (*SU3CGdataIter).second; 
//	Generally, SU(3) CG's are not guaranteed to be non-zero (for instance
//	< (0 2) -1  1; (0 2) -1  1 || (1 2) -2  2  > == 0) and hence we need
//	to check whether a given SU(3) CG's are not all equal to zero
			if (std::count_if(SU3CG.begin(), SU3CG.end(), Negligible) == m_max_rho) {
				continue;
			}
			Xlabels.k = (*SU3CGdataIter).first.k1(); 
			Xlabels.ll  = (*SU3CGdataIter).first.l1();
			Ylabels.k = (*SU3CGdataIter).first.k2();
			Ylabels.ll  = (*SU3CGdataIter).first.l2();

			for (Xlabels.jj = abs(Xlabels.ll - X.S2); Xlabels.jj <= Xlabels.ll + X.S2; Xlabels.jj += 2)
			{
				for (Ylabels.jj = abs(Ylabels.ll - Y.S2); Ylabels.jj <= Ylabels.ll + Y.S2; Ylabels.jj += 2)
				{
					if (!SU2::mult(Xlabels.jj, Ylabels.jj, Components2Costruct[i].jj))
					{
						continue;
					}
					double Pij1j2 = std::sqrt((Xlabels.jj + 1)*(Ylabels.jj + 1));
					for (Xlabels.mm = -Xlabels.jj; Xlabels.mm <= Xlabels.jj; Xlabels.mm += 2)
					{
						Ylabels.mm = Components2Costruct[i].mm - Xlabels.mm;
						if (abs(Ylabels.mm) > Ylabels.jj)
						{
							continue;
						}

						double dCG = clebschGordan(Xlabels.jj, Xlabels.mm, Ylabels.jj, Ylabels.mm, Components2Costruct[i].jj, Components2Costruct[i].mm);
						if (Negligible(dCG)) 
						{
							continue;
						}
						double dWig = wigner9j(Xlabels.ll, Ylabels.ll, Components2Costruct[i].ll, X.S2, Y.S2, S2, Xlabels.jj, Ylabels.jj, Components2Costruct[i].jj);
						if (Negligible(dWig))
						{
							continue;
						}

////////////////////////////////////////////////////////////////////////////////////////////
//	now I know components of X and Y needed
//						iterate over multiplicity rho (SU3::ResultXY) <--- (SU3::X x SU3::Y)
						for (irho = 0; irho < m_max_rho; ++irho)
						{
							Coeff[irho] = PiSL*SU3CG[irho]*dCG*Pij1j2*dWig;
if (fDiagnostic) 
{
	std::cout << Coeff[irho] << " * {";
	if (X.m_OperatorType < GENERAL) 
	{
		std::cout << ((X.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(X.lm, Xlabels): AnnihilationTensorToSpsIndex(X.mu, Xlabels));
	} else {
		std::cout << Xlabels;
	}
	if (Y.m_OperatorType < GENERAL)
	{
		std::cout << ", " << ((Y.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Y.n(), Ylabels) : AnnihilationTensorToSpsIndex(Y.n(), Ylabels)) << "}\t\t";
	}
	else 
	{
		std::cout <<  ", " << Ylabels << "}\t\t";
	}
}
						}	// irho

						Add(X, rhoX, Xlabels, Y, rhoY, Ylabels, Coeff, CurrentOperator);
////////////////////////////////////////////////////////////////////////////////////////////
if (fDiagnostic) 
{										
						std::cout << std::endl;
}
					}	// Xlabels.mm
				}	// Ylabels.jj
			}	//	Xlabels.jj
		}	//	SU3CGdataIter
		if ((m_TensorComponents.insert(std::make_pair(Components2Costruct[i], CurrentOperator))).second == false)
		{
			std::cerr << "Inserting tensor operator into m_TensorComponents data structure has failed!" << std::endl;
			exit(EXIT_FAILURE);
		}
if (fDiagnostic)
{	
			std::cout << std::endl;
}			
	}	// tensor components
}


void Tensor::RenormalizeTensorComponents()
{
	size_t irho;
	double dNorm;
	std::map<SU3xSU2::PHYSICALJ, TENSOR_COMPONENT_ALL_RHO>::iterator it = m_TensorComponents.begin();
	for (; it != m_TensorComponents.end(); ++it)
	{
		std::vector<std::vector<double> >& rCoeffs = (it->second).first;
		for (irho = 0; irho < m_max_rho; ++irho)
		{
			dNorm = sqrt(std::inner_product(rCoeffs[irho].begin(), rCoeffs[irho].end(), rCoeffs[irho].begin(), 0.0));
			std::cout << "Norm = " << dNorm << std::endl;
			std::transform(rCoeffs[irho].begin(), rCoeffs[irho].end(), rCoeffs[irho].begin(), std::bind2nd(std::divides<double>(), dNorm));
		}
	}
}

void Tensor::ShowTensorComponents() const
{
	std::cout << "Operator structure: ";
	for (size_t i = 0; i < m_nBasisOperators; ++i)
	{
		std::cout << ((m_Structure[i] > 0) ? "ad_{" : "at_{");
		std::cout << ((m_Structure[i] > 0) ? (m_Structure[i]-1) : -1*(m_Structure[i]+1));
		std::cout << "}";
	}
	std::cout << std::endl;

	std::map<SU3xSU2::PHYSICALJ, TENSOR_COMPONENT_ALL_RHO>::const_iterator cit = m_TensorComponents.begin();
	for (; cit != m_TensorComponents.end(); ++cit)
	{
		SU3xSU2::PHYSICALJ TensorLabels(cit->first);
		std::cout << "Tensor: {" << TensorLabels << "}:" << std::endl;
		const std::vector<std::vector<double> >& rCoeffs = (cit->second).first;

		const std::vector<SPS_INDEX>& Indices = (cit->second).second;
		size_t nCoeffs = rCoeffs[0].size();
		size_t isps = 0;
		for (size_t j = 0; j < nCoeffs; ++j)
		{
			for (size_t irho = 0; irho < m_max_rho; ++irho)
			{
				std::cout << rCoeffs[irho][j] << "\t";
			}
			std::cout << "{";
			for (size_t k = 0; k < m_nBasisOperators - 1; ++k)
			{
				std::cout << Indices[isps++] << ", ";
			}
			std::cout << Indices[isps++] << "}" << std::endl;
		}
		std::cout << std::endl;
	}
}
void Tensor::ShowTensorComponent(const SU3xSU2::PHYSICALJ& TensorComponent) const 
{
	std::cout << "Operator structure: ";
	for (size_t i = 0; i < m_nBasisOperators; ++i)
	{
		std::cout << ((m_Structure[i] > 0) ? "ad_{" : "at_{");
		std::cout << ((m_Structure[i] > 0) ? (m_Structure[i]-1) : -1*(m_Structure[i]+1));
		std::cout << "}";
	}
	std::cout << std::endl;

	std::map<SU3xSU2::PHYSICALJ, TENSOR_COMPONENT_ALL_RHO>::const_iterator cit = m_TensorComponents.find(TensorComponent);
	if (cit != m_TensorComponents.end())
	{
		std::cout << "Tensor: {" << TensorComponent << "}:" << std::endl;
		const std::vector<std::vector<double> >& rCoeffs = (cit->second).first;

		const std::vector<SPS_INDEX>& Indices = (cit->second).second;
		size_t nCoeffs = rCoeffs[0].size();
		size_t isps = 0;
		for (size_t j = 0; j < nCoeffs; ++j)
		{
			for (size_t irho = 0; irho < m_max_rho; ++irho)
			{
				std::cout << rCoeffs[irho][j] << "\t";
			}
			std::cout << "{";
			for (size_t k = 0; k < m_nBasisOperators - 1; ++k)
			{
				std::cout << Indices[isps++] << ", ";
			}
			std::cout << Indices[isps++] << "}" << std::endl;
		}
		std::cout << std::endl;
	}
	else 
	{
		std::cerr << "Unable to find component " << TensorComponent << " in the tensor data structure." << std::endl;
	}
}


Tensor::TENSOR_COMPONENT_SINGLE_RHO Tensor::Component(const size_t irho, const SU3xSU2::PHYSICALJ& labels) const
{
	assert(m_OperatorType == GENERAL);
	
	std::map<SU3xSU2::PHYSICALJ, TENSOR_COMPONENT_ALL_RHO>::const_iterator cit = m_TensorComponents.find(labels);
	if (cit == m_TensorComponents.end()) 
	{
		std::cerr << "tensor component " << labels << " was not found" << std::endl;
		exit(EXIT_FAILURE);
	}
	return std::make_pair(&(((cit->second).first)[irho]), &((cit->second).second));
}

void Tensor::Add(
				const Tensor& X, const size_t rhoX, const SU3xSU2::PHYSICALJ& Xlabels, 
				const Tensor& Y, const size_t rhoY, const SU3xSU2::PHYSICALJ& Ylabels, 
				const std::vector<double>& Coeff, TENSOR_COMPONENT_ALL_RHO& CurrentOperator) const 
{
	if (X.m_OperatorType == GENERAL && Y.m_OperatorType == GENERAL)
	{
		TENSOR_COMPONENT_SINGLE_RHO Xtensor = X.Component(rhoX, Xlabels);
		TENSOR_COMPONENT_SINGLE_RHO Ytensor = Y.Component(rhoY, Ylabels);

		const std::vector<SPS_INDEX>*  Xindices = Xtensor.second;
		const std::vector<SPS_INDEX>*  Yindices = Ytensor.second;
		const std::vector<double>*  Xcoeff = Xtensor.first;
		const std::vector<double>*  Ycoeff = Ytensor.first;

		double dxy;

		size_t nx = Xcoeff->size();
		size_t ny = Ycoeff->size();
		size_t indexY, iy, irho;

		std::vector<SPS_INDEX>::const_iterator XindicesStartPos = Xindices->begin();
		std::vector<SPS_INDEX>::const_iterator XindicesEndPos = XindicesStartPos + X.m_nBasisOperators;
		for (size_t ix = 0; ix < nx; ++ix)
		{
			std::vector<SPS_INDEX>::const_iterator YindicesStartPos = Yindices->begin();
			std::vector<SPS_INDEX>::const_iterator YindicesEndPos = YindicesStartPos + Y.m_nBasisOperators;
			for (iy = 0; iy < ny; ++iy)
			{
				dxy = Xcoeff->operator[](ix)*Ycoeff->operator[](iy);
				for (irho = 0; irho < m_max_rho; ++irho)
				{
					(CurrentOperator.first)[irho].push_back(Coeff[irho]*dxy);
				}
				(CurrentOperator.second).insert((CurrentOperator.second).end(), XindicesStartPos, XindicesEndPos);
				(CurrentOperator.second).insert((CurrentOperator.second).end(), YindicesStartPos, YindicesEndPos);
				YindicesStartPos = YindicesEndPos;
				YindicesEndPos  += Y.m_nBasisOperators;
			}
			XindicesStartPos = XindicesEndPos;
			XindicesEndPos  += X.m_nBasisOperators;
		}
	}
	else if (X.m_OperatorType < GENERAL && Y.m_OperatorType < GENERAL) // (ad ad)  or (at at) or (ad at)
	{
		size_t indexX = (X.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(X.n(), Xlabels) : AnnihilationTensorToSpsIndex(X.n(), Xlabels);
		size_t indexY = (Y.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Y.n(), Ylabels) : AnnihilationTensorToSpsIndex(Y.n(), Ylabels);
		char phase = ((X.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(X.mu, Xlabels) : 1)*((Y.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(Y.mu, Ylabels) : 1);

		for (size_t irho = 0; irho < m_max_rho; ++irho)
		{
			(CurrentOperator.first)[irho].push_back(phase*Coeff[irho]);
		}
		(CurrentOperator.second).push_back(indexX);
		(CurrentOperator.second).push_back(indexY);
	}
	else if (X.m_OperatorType == GENERAL)
	{
		TENSOR_COMPONENT_SINGLE_RHO Xtensor = X.Component(rhoX, Xlabels);
		const std::vector<SPS_INDEX>*  Xindices = Xtensor.second;
		const std::vector<double>*  Xcoeff = Xtensor.first;
		size_t nx = Xcoeff->size();

		size_t indexY = (Y.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Y.n(), Ylabels) : AnnihilationTensorToSpsIndex(Y.n(), Ylabels);
		char phase = (Y.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(Y.mu, Ylabels) : 1;
		size_t irho;
		double dx;
	
		std::vector<SPS_INDEX>::const_iterator XindicesStartPos = Xindices->begin();
		std::vector<SPS_INDEX>::const_iterator XindicesEndPos = XindicesStartPos + X.m_nBasisOperators;
		for (size_t ix = 0; ix < nx; ++ix)
		{
			dx = phase*Xcoeff->operator[](ix);
			for (irho = 0; irho < m_max_rho; ++irho)
			{
				(CurrentOperator.first)[irho].push_back(Coeff[irho]*dx);
			}
			(CurrentOperator.second).insert((CurrentOperator.second).end(), XindicesStartPos, XindicesEndPos);
			(CurrentOperator.second).push_back(indexY);
			XindicesStartPos = XindicesEndPos;
			XindicesEndPos  += X.m_nBasisOperators;
		}
	}
	else if (Y.m_OperatorType == GENERAL)
	{
		TENSOR_COMPONENT_SINGLE_RHO Ytensor = Y.Component(rhoY, Ylabels);
		const std::vector<SPS_INDEX>*  Yindices = Ytensor.second;
		const std::vector<double>*  Ycoeff = Ytensor.first;
		size_t ny = Ycoeff->size();

		size_t indexX = (X.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(X.n(), Xlabels) : AnnihilationTensorToSpsIndex(X.n(), Xlabels);
		char phase = ((X.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(X.mu, Xlabels) : 1);
		size_t irho;
		double dy;

		std::vector<SPS_INDEX>::const_iterator YindicesStartPos = Yindices->begin();
		std::vector<SPS_INDEX>::const_iterator YindicesEndPos = YindicesStartPos + Y.m_nBasisOperators;
		for (size_t iy = 0; iy < ny; ++iy)
		{
			dy = phase*Ycoeff->operator[](iy);
			for (irho = 0; irho < m_max_rho; ++irho)
			{
				(CurrentOperator.first)[irho].push_back(Coeff[irho]*dy);
			}
			(CurrentOperator.second).push_back(indexX);
			(CurrentOperator.second).insert((CurrentOperator.second).end(), YindicesStartPos, YindicesEndPos);
			YindicesStartPos = YindicesEndPos;
			YindicesEndPos += Y.m_nBasisOperators;
		}
	}
}

Creation::Creation(size_t n): Tensor(SU3xSU2::LABELS(n, 0, 1), CREATION) 
{
	std::vector<SU3xSU2::PHYSICALJ> HOShellComponents;
	std::vector<std::vector<double> > vvCoeffs(1, std::vector<double>(1, 1.0));
	std::vector<SPS_INDEX> SpsIndex(1, 0);

	size_t nComponents = SU3xSU2::GetPhysicalJBasis(*this, HOShellComponents);
	for (size_t i = 0; i < nComponents; ++i)
	{
		SpsIndex[0] = CreationTensorToSpsIndex(n, HOShellComponents[i]);
		m_TensorComponents.insert(std::make_pair(HOShellComponents[i], std::make_pair(vvCoeffs, SpsIndex)));
	}
}

Annihilation::Annihilation(size_t n): Tensor(SU3xSU2::LABELS(0, n, 1), ANNIHILATION)
{
	std::vector<SU3xSU2::PHYSICALJ> HOShellComponents;
	std::vector<std::vector<double> > vvCoeffs(1, std::vector<double>(1, 1.0));
	std::vector<SPS_INDEX> SpsIndex(1, 0);

	size_t nComponents = SU3xSU2::GetPhysicalJBasis(*this, HOShellComponents); // false = sorting turned off
	for (size_t i = 0; i < nComponents; ++i)
	{
		SpsIndex[0] 	= AnnihilationTensorToSpsIndex(n, HOShellComponents[i]);
		vvCoeffs[0][0] 	= AnnihilationPhase(n, HOShellComponents[i]);
		m_TensorComponents.insert(std::make_pair(HOShellComponents[i], std::make_pair(vvCoeffs, SpsIndex)));
	}
}

void Tensor::ShowTermIndices(const SU3xSU2::PHYSICALJ& Component, size_t itensor_term)
{
	TENSOR_STORAGE::iterator TensorComponent = m_TensorComponents.find(Component);
	if (TensorComponent == m_TensorComponents.end())
	{
		std::cerr << "Error in Tensor::ShowTerm ... tensorial component " << Component << " was not found." << std::endl;
		exit(EXIT_FAILURE);
	}
	const std::vector<SPS_INDEX>& TensorIndices = (TensorComponent->second).second;
	size_t isps = itensor_term*m_nBasisOperators;
	std::cout << "{";
	for (size_t k = 0; k < m_nBasisOperators - 1; ++k, ++isps)
	{
		std::cout << TensorIndices[isps] << " ";
	}
	std::cout << TensorIndices[isps] << "}" << std::endl;
}

size_t Tensor::GetdA() const
{
	size_t nBasisOperators = GetNBasisOperators();
	int dA = 0;
	for(size_t i = 0; i < nBasisOperators; ++i)
	{
		dA += (m_Structure[i] > 0) ? 1 : -1;
	}
	return dA;
}

}
}
