#include <SU3ME/CalculateMe_IdenticalFermionsNCSMBasis.h>

double Coeff_x_SU3SO3CG_x_RME(size_t k0_max, size_t a0_max,  size_t rhot_max,
								SU3::WIGNER* pSU3SO3CGs, 
								SU3xSU2::RME::DOUBLE* pRME, 
								TENSOR_STRENGTH* coeffs, const int fermionType)
{
	assert(fermionType == PP || fermionType == NN);
	int a0, k0, rhot;
	double dtmp, dResult = 0;

	SU3xSU2::RME::DOUBLE* RME;
	SU3::WIGNER* SU3SO3CG = pSU3SO3CGs;	//	SU3SO3CG[0] ---> < wi ki Li; w0 k0min L0 || wf kf Lf>_{rhotmin}
	TENSOR_STRENGTH* COEFFS = coeffs;
	
	for (k0 = 0; k0 < k0_max; ++k0, SU3SO3CG += rhot_max)
	{
		for (a0 = 0, RME = pRME; a0 < a0_max; ++a0, COEFFS += 2, RME += rhot_max)
		{
			for (rhot = 0, dtmp = 0; rhot < rhot_max; ++rhot)
			{
				dtmp += SU3SO3CG[rhot]*RME[rhot];
			}

			dResult += COEFFS[fermionType]*dtmp;
		}
	}
	return dResult;
}

void CalculateME_debbug(const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
						const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
						const std::vector<MECalculatorData>& MeCalcData, const int fermionType)
{
	assert(fermionType == PP || fermionType == NN);

	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	double dWigner9j, dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1)*(bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<double> MeJ(nJ*nme, 0.0); 	// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalat operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<double> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_, fermionType);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
				for (int J = Jmin; J <= Jmax; J += 2)
				{
					dWigner9j = wigner9j(ket.L(), ket.S2, J, L0, S0, 0, bra.L(), bra.S2, J); // the most time consuming step ...
					if (Negligible(dWigner9j))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]))
								{
									std::cout << "<af=" << af+1 << " (" << bra.lm << " " << bra.mu << ") S=" << bra.S2 << " kf=" << kf + 1 << " Lf=" << bra.L() << " J=" << J << "|| H ||";
									std::cout << "<ai=" << ai+1 << " (" << ket.lm << " " << ket.mu << ") S=" << ket.S2 << " ki=" << ki + 1 << " Li=" << ket.L() << " J=" << J << "> == ";
									std::cout << "<" << braStateId+1 << "|| H ||" << ketStateId+1 << ">" << "\t=\t"; 
									std::cout << MeJ[index_me] << std::endl;
								}
							}
						}
					}
				}
			}
		}
	}
}


void CalculateME_nonDiagonal(	const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
								const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
								const std::vector<MECalculatorData>& MeCalcData, const int fermionType, std::vector<MatrixRow>& resultMe)
{
//	assert(firstBraStateId != firstKetStateId);
	assert(fermionType == PP || fermionType == NN);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	double dWigner9j, dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<double> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalat operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<double> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_, fermionType);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin; J <= Jmax; J += 2)
				{
					dWigner9j = wigner9j(ket.L(), ket.S2, J, L0, S0, 0, bra.L(), bra.S2, J); // the most time consuming step ...
					if (Negligible(dWigner9j))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]))
								{
									resultMe[irow_relative].push_back(std::make_pair(ketStateId, MeJ[index_me]));
								}
							}
						}
					}
				}
			}
		}
	}
}


void CalculateME_Diagonal_LowerTriang(	const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, const int fermionType, std::vector<MatrixRow>& resultMe)
{
	assert(firstBraStateId == firstKetStateId);
	assert(fermionType == PP || fermionType == NN);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	double dWigner9j, dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); ket.L() <= bra.L() && !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<double> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalat operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<double> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_, fermionType);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin; J <= Jmax; J += 2)
				{
					dWigner9j = wigner9j(ket.L(), ket.S2, J, L0, S0, 0, bra.L(), bra.S2, J); // the most time consuming step ...
					if (Negligible(dWigner9j))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]) && (braStateId >= ketStateId))
								{
									resultMe[irow_relative].push_back(std::make_pair(ketStateId, MeJ[index_me]));
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalculateME_Diagonal_UpperTriang(	const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, const int fermionType, std::vector<MatrixRow>& resultMe)
{
	assert(firstBraStateId == firstKetStateId);
	assert(fermionType == PP || fermionType == NN);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	double dWigner9j, dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<double> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalat operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<double> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_, fermionType);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin; J <= Jmax; J += 2)
				{
					dWigner9j = wigner9j(ket.L(), ket.S2, J, L0, S0, 0, bra.L(), bra.S2, J); // the most time consuming step ...
					if (Negligible(dWigner9j))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]) && (braStateId <= ketStateId))
								{
									resultMe[irow_relative].push_back(std::make_pair(ketStateId, MeJ[index_me]));
								}
							}
						}
					}
				}
			}
		}
	}
}
