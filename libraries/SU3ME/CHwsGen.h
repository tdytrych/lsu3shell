/****************************************************************
  CHwsGen.h

  3/5/11 (mac): Include global_definitions.h for directory names.

****************************************************************/

#ifndef CHWSGEN_H
#define CHWSGEN_H
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/CNullSpaceSolver.h>
#include <UNU3SU3/CUNMaster.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <SU3NCSMUtils/combination.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

// If the system of linear equations has more than maxColumns, it is impossible
// to solve it with the current numerical methods (see class CNullSpaceSolver).
// We thus save the matrix and later can use some parallel algorithm for find a
// null space.
// I guess this is still doable on prague
const size_t maxColumns = 200000;

// This is the structure of data that precedes coefficients and basis states of
// the HWS if the latter is stored in form of a binary file.
struct HWSHEADER {
   size_t maxMult;  // maxMult = U(N)>U(3) multiplicity, a.k.a. \alpha
   unsigned lm;
   unsigned mu;
   size_t nBasisStates;
};

enum HWSOutputType { HWSOutputBin, HWSOutputText };

template <typename UN_BASIS_STATE>
class CHwsGen {
   typedef CNullSpaceSolver::DOUBLE DOUBLE;

  private:
   HWSOutputType m_OutputType;
   //	Class CNullSpaceSolver uses numerical libraries GSL and Eigen to find a
   //	null space of a system of linear equations that define HWS.
   CNullSpaceSolver m_NullSpaceSolver;
   //  HO sps in a speedometer order.
   std::vector<U3::SPS> m_HoShellsSps;
   std::vector<std::vector<DOUBLE> > m_CzxCoeffs;
   std::vector<std::vector<DOUBLE> > m_CzyCoeffs;
   std::vector<std::vector<DOUBLE> > m_CxyCoeffs;

  private:
   //	transform U(N) basis states with a given weights into vector<pair<bitset, bitset> >
   void Weight2Bitstrings(const UN::BASIS_STATE_WEIGHT_VECTOR& weight, const unsigned nup,
                          std::vector<UN_BASIS_STATE>& bitstrings);

  private:
   void SaveHWSText(std::ofstream& ofs, const U3::LABELS& U3Labels,
                    const CNullSpaceSolver::HWS& HWSCoeffs,
                    const std::vector<UN_BASIS_STATE>& Basis);
   void SaveHWSBin(std::ofstream& ofs, const U3::LABELS& U3Labels,
                   const CNullSpaceSolver::HWS& HWSCoeffs,
                   const std::vector<UN_BASIS_STATE>& Basis);

  public:
   CHwsGen(unsigned nmax, CNullSpaceSolver::SolverType Solver, HWSOutputType OutputType);

   static std::string GetHWSFileName(const unsigned n, const unsigned A, const unsigned S2,
                                     HWSOutputType OutputType);

   //	Key method of CHwsGen class. It generates all SU(3)xSU(2) HWS and stores them in an output
   //file.
   void GenerateHWS(const unsigned n, const unsigned A, const unsigned S2);

   //	The following methods apply Czx, Czy, Czy, and Sp operators on a given U(N) basis state
   //	const unsigned n ... HO shell number
   void Czx(const UN_BASIS_STATE& BasisState, const unsigned n,
            std::vector<UN_BASIS_STATE>& vResultingBitstrings, std::vector<DOUBLE>& vCoeffs);
   void Czy(const UN_BASIS_STATE& BasisState, const unsigned n,
            std::vector<UN_BASIS_STATE>& vResultingBitstrings, std::vector<DOUBLE>& vCoeffs);
   void Cxy(const UN_BASIS_STATE& BasisState, const unsigned n,
            std::vector<UN_BASIS_STATE>& vResultingBitstrings, std::vector<DOUBLE>& vCoeffs);
   void Sp(const UN_BASIS_STATE& BasisState, const unsigned n,
           std::vector<UN_BASIS_STATE>& vResultingBitstrings, std::vector<DOUBLE>& vCoeffs);
};

template <typename UN_BASIS_STATE>
std::string CHwsGen<UN_BASIS_STATE>::GetHWSFileName(const unsigned n,   // HO shell
                                                    const unsigned A,   // number of fermions
                                                    const unsigned S2,  // 2*spi
                                                    HWSOutputType OutputType) {
   ///////////////////////////////////////////////////////////////////////////////////////////////////////
   //	Generate name of the file where hws will be stored. It has this
   //	structure: "n#A#S#.hws". Note that number following S corresponds to the
   //	value of spin multiplied by two, i.e. 2*S
   std::ostringstream sn;
   std::ostringstream sA;
   std::ostringstream sS2;

   std::string sFileName(su3shell_data_directory);
   sFileName += "/hws/";

   sn << n;
   sA << A;
   sS2 << S2;
   sFileName += 'n' + sn.str() + 'A' + sA.str() + 'S' + sS2.str() +
                ((OutputType == HWSOutputBin) ? "_bin.hws" : "_text.hws");
   return sFileName;
}

/*
 * Arrange single-particle U(3) states of n-th oscillator shell (nz, nx, ny)
 * in speedometer order and calculate coefficients which are used in action of
 * operators Czx, Cxy, and Czy.
 */
template <typename UN_BASIS_STATE>
CHwsGen<UN_BASIS_STATE>::CHwsGen(const unsigned nmax, const CNullSpaceSolver::SolverType Solver,
                                 const HWSOutputType OutputType)
    : m_NullSpaceSolver(Solver), m_OutputType(OutputType) {
   U3::LABELS U3Labels;  // [0] -> nz ; [1] -> nx ; [2] -> ny

   for (unsigned n = 0; n <= nmax; n++) {
      U3::SPS ShellSPS;  // set of sps (Nz, Nx, Ny) for harmonic oscillator shell n

      size_t N = (n + 1) * (n + 2) / 2;
      std::vector<DOUBLE> CzxCoeff(N);
      std::vector<DOUBLE> CzyCoeff(N);
      std::vector<DOUBLE> CxyCoeff(N);
      size_t i = 0;
      for (long k = 0; k <= n; k++) {
         U3Labels[U3::NZ] = n - k;
         for (long nx = k; nx >= 0; nx--) {
            U3Labels[U3::NX] = nx;
            U3Labels[U3::NY] = (n - U3Labels[U3::NZ] - nx);
            ShellSPS.push_back(U3Labels);

            CzxCoeff[i] = sqrt((U3Labels[U3::NZ] + 1.0) * nx);
            CzyCoeff[i] = sqrt((U3Labels[U3::NZ] + 1.0) * U3Labels[U3::NY]);
            CxyCoeff[i] = sqrt((nx + 1.0) * U3Labels[U3::NY]);
            i++;
         }
      }
      m_HoShellsSps.push_back(ShellSPS);
      m_CzxCoeffs.push_back(CzxCoeff);
      m_CzyCoeffs.push_back(CzyCoeff);
      m_CxyCoeffs.push_back(CxyCoeff);
   }
}

// Generate all U(N) basis states that carry a given U(N) weight.
// nup ... total number of fermions with spin up
template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::Weight2Bitstrings(const UN::BASIS_STATE_WEIGHT_VECTOR& weight,
                                                const unsigned nup,
                                                std::vector<UN_BASIS_STATE>& bitstrings) {
   typename UN_BASIS_STATE::first_type spin_up;
   typename UN_BASIS_STATE::second_type spin_down;

   std::vector<size_t>
       sps_available;  // set of sps that are available for iup fermions with spin up

   size_t N = weight.size();
   int iup = nup;  // iup == #fermions that are left for distribution

   for (size_t isps = 0; isps < N; isps++) {
      if (weight[isps] == 2)  // => a given sps is occupied by spin up and spin down fermions
      {
         iup--;
         spin_up.set(isps);
         spin_down.set(isps);
      } else if (weight[isps] == 1) {
         spin_down.set(isps);
         sps_available.push_back(isps);
      }
   }

   if (iup == 0) {  // no particles are left => job is done. This happens when weight was of type,
                    // e.g. (2, 0, 0, 2, 2, 0, 0).
      bitstrings.push_back(std::make_pair(spin_up, spin_down));
      return;
   }

   // create starting combination of sps with spin up. This is equal to the
   // first iup elements of vector sps_available
   std::vector<size_t> SpinUp(sps_available.begin(), sps_available.begin() + iup);
   do {
      typename UN_BASIS_STATE::first_type up(spin_up);
      typename UN_BASIS_STATE::second_type down(spin_down);
      for (size_t i = 0; i < iup; ++i) {
         up.set(SpinUp[i]);
         down.flip(SpinUp[i]);
      }
      bitstrings.push_back(std::make_pair(up, down));
   } while (stdcomb::next_combination(
       sps_available.begin(), sps_available.end(), SpinUp.begin(),
       SpinUp.end()));  // next combination of sps which are occupied by spin up fermions
}

// TASK: apply operator Czx on BasisState bitset.
//
template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::Czx(const UN_BASIS_STATE& BasisState, const unsigned n,
                                  std::vector<UN_BASIS_STATE>& vResultingBitstrings,
                                  std::vector<DOUBLE>& vCoeffs) {
   const U3::SPS& ShellSPS = m_HoShellsSps[n];
   const std::vector<DOUBLE>& Coeffs = m_CzxCoeffs[n];

   unsigned nsps = ShellSPS.size() - 1;  // we can skip the last sps = [0 0 n], that is, nx = 0
   size_t i, icreate;

   const typename UN_BASIS_STATE::first_type& stateUp = BasisState.first;
   const typename UN_BASIS_STATE::second_type& stateDown = BasisState.second;
   unsigned nFermionsBetween;
   size_t cnt;

   for (size_t i = 1; i < nsps; ++i)  // we can also skip the first sps = [n 0 0], that is, nx = 0
   {
      if (ShellSPS[i][U3::NX]) {
         if (stateUp.test(i)) {
            icreate = i - n + ShellSPS[i][U3::NZ];  // Position of sps to be created. I found it by
                                                    // writing down sps in speedometer order
            if (!stateUp.test(icreate)) {           // if sps[icreate] is not occupied
               typename UN_BASIS_STATE::first_type result(stateUp);
               result.set(i, 0);        // 1 -> 0
               result.set(icreate, 1);  // 0 -> 1
               for (nFermionsBetween = 0, cnt = icreate + 1; cnt < i;
                    nFermionsBetween += stateUp.test(cnt++)) {
               }  // nFermionsBetween = #fermions between i and icreate
               vResultingBitstrings.push_back(std::make_pair(result, stateDown));
               vCoeffs.push_back(Coeffs[i] * ((nFermionsBetween % 2) ? -1.0 : 1.0));
            }
         }
         if (stateDown.test(i)) {
            icreate = i - n + ShellSPS[i][U3::NZ];  // Position of sps to be created. I found it by
                                                    // writing down sps in speedometer order
            if (!stateDown.test(icreate)) {         // if sps[icreate] is not occupied
               typename UN_BASIS_STATE::second_type result(stateDown);
               result.set(i, 0);        // 1 -> 0
               result.set(icreate, 1);  // 0 -> 1
               for (nFermionsBetween = 0, cnt = icreate + 1; cnt < i;
                    nFermionsBetween += stateDown.test(cnt++)) {
               }  // nFermionsBetween = #fermions between i and icreate
               vResultingBitstrings.push_back(std::make_pair(stateUp, result));
               vCoeffs.push_back(Coeffs[i] * ((nFermionsBetween % 2) ? -1.0 : 1.0));
            }
         }
      }
   }
}

template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::Czy(const UN_BASIS_STATE& BasisState, const unsigned n,
                                  std::vector<UN_BASIS_STATE>& vResultingBitstrings,
                                  std::vector<DOUBLE>& vCoeffs) {
   const U3::SPS& ShellSPS = m_HoShellsSps[n];
   const std::vector<DOUBLE>& Coeffs = m_CzyCoeffs[n];

   size_t nsps = ShellSPS.size();  // we CANNOT skip the last sps = [nz=0 nx=0 ny=n]
   size_t i, icreate;

   const typename UN_BASIS_STATE::first_type& stateUp = BasisState.first;
   const typename UN_BASIS_STATE::second_type& stateDown = BasisState.second;

   unsigned nFermionsBetween;
   size_t cnt;

   for (i = 2; i < nsps;
        ++i)  // but we can skip the first and the second sps's which are of type  [nz=n nx=0 ny=0]
              // and [nz=n-1 nx=1 ny=0], that is, in both cases ny = 0
   {
      if (ShellSPS[i][U3::NY]) {
         if (stateUp.test(i)) {
            icreate = i - (n - ShellSPS[i][U3::NZ] + 1);
            if (!stateUp.test(icreate)) {  // if sps[icreate] is not occupied
               typename UN_BASIS_STATE::first_type result(stateUp);
               result.set(i, 0);        // 1 -> 0
               result.set(icreate, 1);  // 0 -> 1
               for (nFermionsBetween = 0, cnt = icreate + 1; cnt < i;
                    nFermionsBetween += stateUp.test(cnt++)) {
               }  // nFermionsBetween = #fermions between i and icreate
               vResultingBitstrings.push_back(std::make_pair(result, stateDown));
               vCoeffs.push_back(Coeffs[i] * ((nFermionsBetween % 2) ? -1.0 : 1.0));
            }
         }
         if (stateDown.test(i)) {
            icreate =
                i - (n - ShellSPS[i][U3::NZ] + 1);  // Position of sps to be created. I found it by
                                                    // writing down sps in speedometer order
            if (!stateDown.test(icreate)) {         // if sps[icreate] is not occupied
               typename UN_BASIS_STATE::second_type result(stateDown);
               result.set(i, 0);        // 1 -> 0
               result.set(icreate, 1);  // 0 -> 1
               for (nFermionsBetween = 0, cnt = icreate + 1; cnt < i;
                    nFermionsBetween += stateDown.test(cnt++)) {
               }  // nFermionsBetween = #fermions between i and icreate
               vResultingBitstrings.push_back(std::make_pair(stateUp, result));
               vCoeffs.push_back(Coeffs[i] * ((nFermionsBetween % 2) ? -1.0 : 1.0));
            }
         }
      }
   }
}

// TASK: apply operator Cxy on BasisState bitset.
// NOTE: due to the structure of sps, there is no phase change involved when
// Cxy is applied to the bitstring.  A fermion is merely shifted to the right
// side by one bit and hence does not "jump" over any other fermions.
template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::Cxy(const UN_BASIS_STATE& BasisState, const unsigned n,
                                  std::vector<UN_BASIS_STATE>& vResultingBitstrings,
                                  std::vector<DOUBLE>& vCoeffs) {
   const U3::SPS& ShellSPS = m_HoShellsSps[n];
   const std::vector<DOUBLE>& Coeffs = m_CxyCoeffs[n];

   size_t nsps = ShellSPS.size();  // we CANNOT skip the last sps = [0 0 n], that is, ny = n
   size_t i;

   const typename UN_BASIS_STATE::first_type& stateUp = BasisState.first;
   const typename UN_BASIS_STATE::second_type& stateDown = BasisState.second;

   for (i = 2; i < nsps; ++i)  // but we can skip the first and the second sps's which are of type
                               // [n 0 0] and [n-1 1 0], that is, in both cases ny = 0
   {
      if (ShellSPS[i][U3::NY]) {
         if (stateUp.test(i) && !stateUp.test(i - 1)) {
            typename UN_BASIS_STATE::first_type result(stateUp);
            result.set(i, 0);      // 1 -> 0
            result.set(i - 1, 1);  // 0 -> 1
            vResultingBitstrings.push_back(std::make_pair(result, stateDown));
            vCoeffs.push_back(Coeffs[i]);
         }
         if (stateDown.test(i) && !stateDown.test(i - 1)) {
            typename UN_BASIS_STATE::second_type result(stateDown);
            result.set(i, 0);      // 1 -> 0
            result.set(i - 1, 1);  // 0 -> 1
            vResultingBitstrings.push_back(std::make_pair(stateUp, result));
            vCoeffs.push_back(Coeffs[i]);
         }
      }
   }
}
// Apply spin raising operator S_{\plus} on U(N) basis state
template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::Sp(const UN_BASIS_STATE& BasisState, const unsigned n,
                                 std::vector<UN_BASIS_STATE>& vResultingBitstrings,
                                 std::vector<DOUBLE>& vCoeffs) {
   const U3::SPS& ShellSPS = m_HoShellsSps[n];
   size_t sps = ShellSPS.size();
   size_t i;

   const typename UN_BASIS_STATE::first_type& stateUp = BasisState.first;
   const typename UN_BASIS_STATE::second_type& stateDown = BasisState.second;

   unsigned nFermionsBetween = stateUp.count();
   bool fUp, fDown;

   for (i = 0; i < sps; ++i) {
      fUp = stateUp.test(i);
      fDown = stateDown.test(i);
      if ((fUp == 0) && (fDown == 1))  // if sps i with spin up is available and at the same time
                                       // sps with spin down is occupied
      {
         typename UN_BASIS_STATE::first_type resultUp(stateUp);
         typename UN_BASIS_STATE::second_type resultDown(stateDown);

         resultUp.set(i, 1);
         resultDown.set(i, 0);

         vResultingBitstrings.push_back(std::make_pair(resultUp, resultDown));
         vCoeffs.push_back(((nFermionsBetween % 2) ? -1.0 : 1.0));
      }
      nFermionsBetween -= fUp;
      nFermionsBetween += fDown;
   }
}

/*
 * TASK: for a given HO shell n, number of fermions A, and spin S generate
 * allowed U(3) representations, their multiplicities \alpha in [f] \alpha
 * (N_{z}, N_{z}, N_{y}), and basis states in bitsets that constitute SU(3)
 * highest-weight-state.
 */
template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::GenerateHWS(const unsigned n,   // HO shell
                                          const unsigned A,   // number of fermions
                                          const unsigned S2)  // 2*spin
{
   // 	number of sps for n-th HO shell
   const unsigned N = (n + 1) * (n + 2) / 2;
   if (2 * N < A) {
      std::cout << "Error: only " << 2 * N << " states available for " << A << " fermions!"
                << std::endl;
      exit(EXIT_FAILURE);
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////
   //	Generate name of the file where hws will be stored. It has this
   //	structure: "n#A#S#.hws". Note that number following S corresponds to the
   //	value of spin multiplied by two, i.e. 2*S
   std::string sFileName(GetHWSFileName(n, A, S2, m_OutputType));
   std::ofstream ofs;
   if (m_OutputType == HWSOutputBin) {
      ofs.open(sFileName.c_str(), std::ios::binary);
   } else {
      ofs.open(sFileName.c_str());
      ofs.precision(std::numeric_limits<DOUBLE>::digits10);
      ofs.setf(std::ios::scientific);
   }
   if (!ofs) {
      std::cerr << "Not able to open file " << sFileName << "!" << std::endl;
      exit(EXIT_FAILURE);
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////

   //	generate U(2) Young pattern for spin S
   UN::LABELS U2Labels(2);
   U2Labels[0] =
       (A + S2) / 2;  // Note that this is also equal to the number of fermions with spin up
   U2Labels[1] = (A - S2) / 2;

   if (U2Labels[0] > N) return;
   unsigned Mult;  // this is the multiplicity of a SU(3) irrep in U(N), a.k.a \alpha

   //	generate U(N) Young pattern which "complements" a given spin S2
   UN::LABELS UNLabels(N, 0);
   for (size_t i = 0; i < U2Labels[0]; i++) {
      UNLabels[i] = (i < U2Labels[1]) ? 2 : 1;
   }

   //	mU3_bitsets: Map of all U(3) labels [Nz, Nx, Ny] and their associated U(N) basis states.
   //
   //	Note that we store all U(3) labels and their basis states, even though
   //	most of U(3) labels do not represent an irreducible representation.
   //	Nevetheless, it facilitates construction of a system of linear
   //	equations.  For instance, the action of Czx on states spanning [N_{z}
   //	N_{x} N_{y}]  U(3) irrep will result in a linear combination of states
   //	that carry [N_{z}+1 N_{x}-1 N_{y}] U(3) labels. And since we store all
   //	states with the latter labels, we know right away how many linear
   //	equations (rows) the action of Czx generates.
   std::map<U3::LABELS, std::vector<UN_BASIS_STATE> > mU3_bitsets;

   //	UNMaster implements algorithm for U(N)>U(3) redution
   CUNMaster UNMaster;
   //  mU3_mult is a map of U(3) labels associated with the number of U(N)
   //  weights.  It is being used to calculate multiplicity of U(N)>U(3)
   //  reduction.
   UN::U3MULT_LIST mU3_mult;
   //	mUNweights: map of U(3) labels associated with a set of U(N) basis states weights.
   std::map<U3::LABELS, std::set<UN::BASIS_STATE_WEIGHT_VECTOR> > mUNweights;

   std::cout << "Generating U(" << N << ") basis states and their weights ...";
   std::cout.flush();
   UNMaster.GenerateU3LabelsUNWeights(UNLabels, m_HoShellsSps[n], mU3_mult, mUNweights);
   std::cout << "\t Done" << std::endl;
   // 	Structure of mUNweights elements:
   // 	[Nz, Nx, Ny] ---> {UN::BASIS_STATE_WEIGHT_VECTOR, UN::BASIS_STATE_WEIGHT_VECTOR, ... ,
   // UN::BASIS_STATE_WEIGHT_VECTOR} 	In the first step we transform mUNweights into:
   //	[Nz, Nx, Ny] ---> {UN::BASIS_STATE_BITS, UN::BASIS_STATE_BITS, ... ,
   //UN::BASIS_STATE_WEIGHT_VECTOR}
   std::cout << "Transforming weights to bitsets ...";
   std::cout.flush();
   //	Note that mU3_mult and mUNweight have identical tree structure
   std::map<U3::LABELS, std::set<UN::BASIS_STATE_WEIGHT_VECTOR> >::const_iterator citWeightSet =
       mUNweights.begin();
   std::map<U3::LABELS, std::set<UN::BASIS_STATE_WEIGHT_VECTOR> >::const_iterator LAST_WEIGHT_SET =
       mUNweights.end();
   std::vector<UN_BASIS_STATE> UNBasisStatesBITS;
   //	iterate over all weight set of U(N) irrep
   for (; citWeightSet != LAST_WEIGHT_SET; ++citWeightSet) {
      ////////////////////////////////////////////////////////////////
      //		const U3::LABELS& u3labels = citWeightSet->first;
      //		std::cout << (int)u3labels[U3::NZ] << " " << (int)u3labels[U3::NX] << " " <<
      //(int)u3labels[U3::NY] << std::endl;
      ////////////////////////////////////////////////////////////////

      //	reference to a set of U(N) weights of U(3) irrep
      const std::set<UN::BASIS_STATE_WEIGHT_VECTOR>& WeightsSet = citWeightSet->second;

      std::set<UN::BASIS_STATE_WEIGHT_VECTOR>::const_iterator weight = WeightsSet.begin();
      std::set<UN::BASIS_STATE_WEIGHT_VECTOR>::const_iterator LAST_WEIGHT = WeightsSet.end();
      //	iterate over U(N) weights and transform each weight to bitset and
      for (; weight != LAST_WEIGHT; ++weight) {
         //	I use the fact that U2Labels[0] is equal to the number of fermions with spin up
         Weight2Bitstrings(*weight, U2Labels[0], UNBasisStatesBITS);
      }
      //	KEY STEP: sort basis states so that we can later use the binary search to
      //	find their location. This searching is used during constucton of matrix
      //	defined by a system of linear equations.
      std::sort(UNBasisStatesBITS.begin(), UNBasisStatesBITS.end(),
                UN::CMP_BASIS_STATES<UN_BASIS_STATE>());
      //	finally store U(N) basis states (bitsests)
      mU3_bitsets.insert(std::make_pair(citWeightSet->first, UNBasisStatesBITS));
      //	Set size of UNBasisStatesBITS to 0.
      UNBasisStatesBITS.resize(0);
      //	TRICK: vector<>::resize does not change the storage capacity!
      //	Unfortunately, size of UNBasisStatesBITS is not decreasing and
      //	hence some additional time is spent on memory allocations
      //	=> TODO: find a way to estimate maximal number of basis states
   }
   std::cout << "\t Done" << std::endl;

   //	Iterate over U(3) labels and calculate multiplicity Mult (corresponds to
   //	alpha in [f] alpha (lm mu)S).  if Mult > 0 => apply operators Czx, Cxy, and
   //	Sp to determine a system of underdetermined linear equations whose
   //	solution(s) constitute a given SU(3)xSU(2) highest-weight-state(s).
   UN::U3MULT_LIST::const_iterator citU3 = mU3_mult.begin();
   UN::U3MULT_LIST::const_iterator LAST_U3 = mU3_mult.end();
   typename std::map<U3::LABELS, std::vector<UN_BASIS_STATE> >::const_iterator citBitsetVector =
       mU3_bitsets.begin();

   //	iterate over all [N_{z}, N_{x}, N_{y}] labels spanning U(N) irrep
   for (; citU3 != LAST_U3; ++citU3, ++citBitsetVector) {
      U3::LABELS U3Labels(citU3->first);
      if (U3Labels[U3::NZ] >= U3Labels[U3::NX] &&
          U3Labels[U3::NX] >= U3Labels[U3::NY])  // this could be U(3) irrep ... let's check it
      {
         //	Calculate multiplicity alpha in irrep of U(N)
         Mult = UNMaster.GetMultiplicity(U3Labels, mU3_mult);
         if (Mult)  // => this is U(3) irrep => calculate HWS
         {
            //				std::cout << Mult << "[" << (int)U3Labels[U3::NZ] << " " <<
            //(int)U3Labels[U3::NX] << " " <<(int)U3Labels[U3::NY] << "]\t" << S2 << std::endl;
            std::cout << Mult << "(" << SU3::LM(U3Labels) << " " << SU3::MU(U3Labels) << ")" << S2
                      << std::endl;
            //	Using fact that mU3_mult and mU3_bitsets have the same tree structure
            const std::vector<UN_BASIS_STATE>& BasisStates = citBitsetVector->second;
            //	Number of uknowns in a system of linear equations - corresponds to number of columns
            //(n) in matrix
            size_t ncolumns = BasisStates.size();
            //	Number of linear equations - we do not know this number in advance
            size_t nrows(0);
            //	Number of linear equations generated by Czx*HWS = 0, Cxy*HWS=0, and Sp*HWS=0 (i.e.
            //SU(3)xSU(2) HWS properties)
            size_t nrowsCzx(0), nrowsCxy(0), nrowsSp(0);

            size_t irow, icol, icnt, nbitsets;
            //	vCoeffs is an output parameter for CHwsGen<>::Czx(...), CHwsGen<>::Cxy(...), and
            //CHwsGen<>::Sp(...) methods
            std::vector<DOUBLE> vCoeffs;
            //	vResultingBitstrings is an output parameter for CHwsGen<>::Czx(...),
            //CHwsGen<>::Cxy(...), and CHwsGen<>::Sp(...) methods
            std::vector<UN_BASIS_STATE> vResultingBitstrings;

            //	U(3) labels of states generated by the action of Czx on [Nz, Nx, Ny] configuration
            //=> [Nz+1 Nx-1, Ny]
            U3::LABELS CZX_U3(U3Labels);
            CZX_U3[U3::NZ]++;
            CZX_U3[U3::NX]--;
            std::cout << "Applying Czx ...";
            std::cout.flush();
            //	Obtain reference to U(N) states carrying [Nz+1, Nx -1, Ny] U(3) labels
            typename std::map<U3::LABELS, std::vector<UN_BASIS_STATE> >::const_iterator
                citCzx_bitsets = mU3_bitsets.find(CZX_U3);
            if ((citCzx_bitsets != mU3_bitsets.end()))  // if they exist
            {
               typename std::vector<UN_BASIS_STATE>::const_iterator bitsetsBEGIN =
                   citCzx_bitsets->second.begin();
               typename std::vector<UN_BASIS_STATE>::const_iterator bitsetsEND =
                   citCzx_bitsets->second.end();
               //	Each bitset in Czx_bitsets correspond to one linear equation => nrowsCzx =
               //number of linear equations generated by Czx*HWS = 0
               nrowsCzx = citCzx_bitsets->second.size();
               //	Since we know the maximal number of resulting bitstrings we allocate amount
               //of memory needed
               vResultingBitstrings.reserve(nrowsCzx);
               vCoeffs.reserve(nrowsCzx);
               //	Iterate over are unknown coefficiens, i.e. columns of system of linear
               //equations
               for (icol = 0; icol < ncolumns; ++icol) {
                  //	Apply Czx operator on BasisStates[icol] basis state
                  Czx(BasisStates[icol], n, vResultingBitstrings, vCoeffs);
                  nbitsets = vResultingBitstrings.size();
                  //	Iterate over states that created by action of Czx
                  for (icnt = 0; icnt < nbitsets; ++icnt) {
                     //	Obtain position of a coefficient vCoeffs[icnt] in a system of linear
                     //equation Czx* HWS = 0
                     irow = (std::lower_bound(bitsetsBEGIN, bitsetsEND, vResultingBitstrings[icnt],
                                              UN::CMP_BASIS_STATES<UN_BASIS_STATE>()) -
                             bitsetsBEGIN);
                     //	matrix(irow, icol) = vCoeffs[icnt]
                     m_NullSpaceSolver.SetMatrix(irow, icol, vCoeffs[icnt]);
                  }
                  //	Reduce size of vector containers without deallocation
                  vCoeffs.resize(0);
                  vResultingBitstrings.resize(0);
               }
            }
            std::cout << "\t Done" << std::endl;
            //	The logic of seting Cxy*HWS = 0 equations is the same as Czx*HWS = 0 => IMHO no
            //comments are needed in this section
            std::cout << "Applying Cxy ...";
            std::cout.flush();
            U3::LABELS CXY_U3(U3Labels);
            CXY_U3[U3::NX]++;
            CXY_U3[U3::NY]--;
            typename std::map<U3::LABELS, std::vector<UN_BASIS_STATE> >::const_iterator
                citCxy_bitsets = mU3_bitsets.find(CXY_U3);
            if ((citCxy_bitsets != mU3_bitsets.end())) {
               typename std::vector<UN_BASIS_STATE>::const_iterator bitsetsBEGIN =
                   citCxy_bitsets->second.begin();
               typename std::vector<UN_BASIS_STATE>::const_iterator bitsetsEND =
                   citCxy_bitsets->second.end();
               nrowsCxy = citCxy_bitsets->second.size();

               vResultingBitstrings.reserve(nrowsCzx);
               vCoeffs.reserve(nrowsCzx);
               for (icol = 0; icol < ncolumns; ++icol) {
                  Cxy(BasisStates[icol], n, vResultingBitstrings, vCoeffs);
                  nbitsets = vResultingBitstrings.size();
                  for (icnt = 0; icnt < nbitsets; ++icnt) {
                     irow = (std::lower_bound(bitsetsBEGIN, bitsetsEND, vResultingBitstrings[icnt],
                                              UN::CMP_BASIS_STATES<UN_BASIS_STATE>()) -
                             bitsetsBEGIN);
                     m_NullSpaceSolver.SetMatrix(nrowsCzx + irow, icol, vCoeffs[icnt]);
                  }
                  vCoeffs.resize(0);
                  vResultingBitstrings.resize(0);
               }
            }
            std::cout << "\t Done" << std::endl;

            std::cout << "Applying Sp  ...";
            std::cout.flush();
            //	nrows will now point to the first equation of Sp*HWS = 0 part of a system
            //	of linear equations
            nrows = nrowsCzx + nrowsCxy;
            //	if S2 == A => Sp annihilates any basis state
            if (S2 < A) {
               int j;
               //	In case of operator Sp we do not know in advance how many states we will
               //	obtain as a result its action on BasisStates
               std::vector<int> nElems(ncolumns);
               for (icol = 0; icol < ncolumns; ++icol) {
                  Sp(BasisStates[icol], n, vResultingBitstrings, vCoeffs);
                  //	nElems[icol] = number of non-zero coefficients in column icol
                  nElems[icol] = vResultingBitstrings.size() - 1;
               }
               //	At this point vCoeffs contains all coefficients and vResultingBitstrings
               //	contains all basis states resulting from the action of Sp - some of them
               //	occur multiple times.

               //	Copy vResultingBitstrings into vSp_BITS
               std::vector<UN_BASIS_STATE> vSp_BITS(vResultingBitstrings);
               //	Sort vSp_BITS
               std::sort(vSp_BITS.begin(), vSp_BITS.end(), UN::CMP_BASIS_STATES<UN_BASIS_STATE>());
               //	Remove all non-unique basis states
               typename std::vector<UN_BASIS_STATE>::const_iterator bitsetsEND = std::unique(
                   vSp_BITS.begin(), vSp_BITS.end(), UN::BASIS_STATES_EQUAL<UN_BASIS_STATE>());
               //	At this point vSp_BITS contains ordered set of unique states that result
               //	from the action of Sp on basis states of U(3) irrep

               typename std::vector<UN_BASIS_STATE>::const_iterator bitsetsBEGIN = vSp_BITS.begin();
               size_t nCoefficients = vCoeffs.size();
               for (j = 0, icol = 0; j < nCoefficients; ++j) {
                  //	Calculate row
                  irow = (std::lower_bound(bitsetsBEGIN, bitsetsEND, vResultingBitstrings[j],
                                           UN::CMP_BASIS_STATES<UN_BASIS_STATE>()) -
                          bitsetsBEGIN);
                  //	Calculate columns
                  for (; j > nElems[icol]; icol++) {
                  }
                  //	matrix(irow, icol) = vCoeffs[j]
                  m_NullSpaceSolver.SetMatrix(nrows + irow, icol, vCoeffs[j]);
               }
               nrowsSp = bitsetsEND - bitsetsBEGIN;
            }
            std::cout << "\t Done" << std::endl;
            nrows += nrowsSp;

            std::cout << "Matrix dimensions: " << nrows << " x " << ncolumns << std::endl;
            std::cout << "# non zero elements = " << m_NullSpaceSolver.MatrixSize() << std::endl;
            //	If the size of a system of linear equations is not too big for a given computer
            if (ncolumns <= maxColumns) {
               std::cout << "Solving Ax = 0" << std::endl;
               //
               if (nrows) {
                  m_NullSpaceSolver.CalculateNullSpace(nrows, ncolumns, Mult);
                  const CNullSpaceSolver::HWS& HWSVectors = m_NullSpaceSolver.GetNullSpace();
                  std::cout << std::endl << std::endl;
                  if (m_OutputType == HWSOutputText) {
                     SaveHWSText(ofs, U3Labels, HWSVectors, BasisStates);
                  } else {
                     SaveHWSBin(ofs, U3Labels, HWSVectors, BasisStates);
                  }
               } else {  // nrows = 0 => all basis states are annihilated by Czx Czy and Sp
                         // ==> each basis state is a HWS and hence multiplicity must be equal to
                         // the number of basis states
                  assert(ncolumns == Mult);
                  CNullSpaceSolver::HWS HWSVectors(Mult, std::vector<DOUBLE>(ncolumns, 0.0));
                  // 	Set HWS = {{1.0, 0.0, ...}, {0.0, 1.0, 0.0, ... , ... {0.0, ... 1.0}}
                  for (size_t iMult = 0; iMult < Mult; ++iMult) {
                     HWSVectors[iMult][iMult] = 1.0;
                  }
                  if (m_OutputType == HWSOutputText) {
                     SaveHWSText(ofs, U3Labels, HWSVectors, BasisStates);
                  } else {
                     SaveHWSBin(ofs, U3Labels, HWSVectors, BasisStates);
                  }
               }
            } else  // the system of linear equations is too big
            {
               //	Save it! Later we can find null space of such a big system using
               //supercomputers.
               m_NullSpaceSolver.SaveMatrix(n, A, S2, Mult, SU3::LM(U3Labels), SU3::MU(U3Labels));
            }
            //	Clear matrix and resulting null space vectors (a.k.a HWS) in m_NullSpaceSolver
            m_NullSpaceSolver.Reset();
         }
      }
   }
}

template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::SaveHWSBin(std::ofstream& ofs, const U3::LABELS& U3Labels,
                                         const CNullSpaceSolver::HWS& HWSCoeffs,
                                         const std::vector<UN_BASIS_STATE>& BasisStates) {
   //	Save header
   HWSHEADER header;
   header.maxMult = HWSCoeffs.size();
   header.lm = SU3::LM(U3Labels);
   header.mu = SU3::MU(U3Labels);
   header.nBasisStates = BasisStates.size();
   ofs.write((char*)&header, sizeof(HWSHEADER));
   //	Save coefficients and corresponding basis states
   //	Structure of i-th record:
   //	Coeff_{i 1}, Coeff_{i 2}, ... Coeff_{i maxMult}, |basis state>_{i}
   std::pair<unsigned long, unsigned long> UpDown;
   DOUBLE* Row = new DOUBLE[header.maxMult];
   for (size_t i = 0; i < header.nBasisStates; i++) {
      //	Save coefficients
      for (size_t iMult = 0; iMult < header.maxMult; ++iMult) {
         Row[iMult] = HWSCoeffs[iMult][i];
      }
      ofs.write((char*)Row, sizeof(DOUBLE) * header.maxMult);
      //	Save basis states
      UpDown.first = BasisStates[i].first.to_ulong();
      UpDown.second = BasisStates[i].second.to_ulong();
      ofs.write((char*)&UpDown, sizeof(std::pair<unsigned long, unsigned long>));
   }
   delete[] Row;
}

//	Structure of text HWS file is the same as the structure of binary file.
//	IMHO no further comments are needed.
template <typename UN_BASIS_STATE>
void CHwsGen<UN_BASIS_STATE>::SaveHWSText(std::ofstream& ofs, const U3::LABELS& U3Labels,
                                          const CNullSpaceSolver::HWS& HWSCoeffs,
                                          const std::vector<UN_BASIS_STATE>& BasisStates) {
   /*
    * THIS IS THE CORRECT VERSION OF THIS METHOD
    * I NEED TO TEST SOMETHIN' AND HENCE I COMMENTED IT
    */

   /*
          size_t maxMult = HWSCoeffs.size();
          size_t nBasisStates = BasisStates.size();

          ofs << maxMult << ' ';
          ofs << SU3::LM(U3Labels) << ' ';
          ofs << SU3::MU(U3Labels) << std::endl;
          ofs << nBasisStates << std::endl;

          for (size_t i = 0; i < nBasisStates; i++)
          {
                  for (size_t iMult = 0; iMult < maxMult; ++iMult)
                  {
                          ofs << ' ' << HWSCoeffs[iMult][i];
                  }
                  ofs << '\t' << BasisStates[i].first.to_ulong() << ' ';
                  ofs << BasisStates[i].second.to_ulong() << std::endl;
          }
  */

   //////////////////////////////////////////////////////////////////////////////
   // This code is good for debugging
   size_t maxMult = HWSCoeffs.size();
   size_t nBasisStates = BasisStates.size();
   int N = BasisStates[0].first.size();

   ofs << maxMult << ' ';
   ofs << SU3::LM(U3Labels) << ' ';
   ofs << SU3::MU(U3Labels) << std::endl;
   ofs << nBasisStates << std::endl;
   for (size_t i = 0; i < nBasisStates; i++) {
      for (size_t iMult = 0; iMult < maxMult; ++iMult) {
         ofs << ' ' << HWSCoeffs[iMult][i];
      }
      ofs << "\t";
      for (int k = 1; k <= N; ++k) {
         if (BasisStates[i].first.test(N - k)) {
            ofs << (N - k + 1) << " ";
         }
      }
      for (int k = 1; k <= N; ++k) {
         if (BasisStates[i].second.test(N - k)) {
            ofs << -1 * (N - k + 1) << " ";
         }
      }
      ofs << std::endl;
   }
   //////////////////////////////////////////////////////////////////////////////
}
#endif
