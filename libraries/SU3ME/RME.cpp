#include <SU3ME/RME.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

int rme_uncoupling_phase(const boost::container::small_vector_base<unsigned char>& bra, const std::vector<unsigned char>& tensor, const boost::container::small_vector_base<unsigned char>& ket)
{
	assert(bra.size() == ket.size());
	assert(bra.size() == tensor.size());

	if (bra.size() == 1)
	{
		return 1;
	}

	int a1(bra[0]), 	a2(bra[1]);
	int G1(tensor[0]),	G2(tensor[1]);
	int a1p(ket[0]), 	a2p(ket[1]);
#ifdef FRONT_CREATE	
	int phase = MINUSto(a1p*G2 + a2*G1 + a2*a1p + a1*a2);
#elif defined(REAR_CREATE)
	int phase = MINUSto(a1p*G2 + a2*G1 + a2*a1p + a1p*a2p);
#endif	
	//	for (size_t i = 2; i < bra.size(); ++i)
	size_t end = bra.size();
	for (size_t i = 2; i < end; ++i)
	{
		a1 	+= a2;
		G1 	+= G2;
		a1p += a2p;

		a2  = bra[i];
		G2  = tensor[i];
		a2p = ket[i];
#ifdef FRONT_CREATE	
		phase *= MINUSto(a1p*G2 + a2*G1 + a2*a1p + a1*a2);
#elif defined(REAR_CREATE)
		phase *= MINUSto(a1p*G2 + a2*G1 + a2*a1p + a1p*a2p);
#endif	
	}
	return phase;
}

//	Input:
//	su39lm: 
//	array containing SU(3) 9lm coefficients for a given fixed set of
//	rhot, rho0, rhoi, rhof. It is a matrix with irho1_max columns and
//	irho2_max rows. 
//	
//	rme1:
//	vector of rho1_max reduced matrix elements
//	rme2:
//	vector of rho2_max reduced matrix elements
//
//	ALGORITHM:
//	STEP 1: Multiply matrix su39lm by the column vector rme1. 
//
//  a_{1 1} 		a_{1 2} 		... a_{1 irho1_max}                    < || || >_{rho1 = 1}   
//	a_{2 1} 		a_{2 2} 		... a_{2 irho1_max}                    < || || >_{rho1 = 2}
//	.                                                                x      .
//	.                                                                       .
//	.                                                                       . 
//	a_{irho2_max 1} a_{irho2_max 2}	...	a_{irho2_max irho1_max}            < || || >_{rho1 = irho1_max}
//
//	resultig column vector has irho2_max elements:
//	dsu3_x_rme1_{rho2 = 1}
//	dsu3_x_rme1_{rho2 = 2}
//	.
//	.
//	.
//	dsu3_x_rme1_{irho2_max}
//
//	STEP 2: Multiply dsu3_x_rme1 by the row vector rme2
SU3xSU2::RME::DOUBLE SU3xSU2::RME::rme2_x_su39lm_x_rme1(int irho1_max, int irho2_max, SU3xSU2::RME::DOUBLE* su39lm, SU3xSU2::RME::DOUBLE* rme1, SU3xSU2::RME::DOUBLE* rme2)
{
	SU3xSU2::RME::DOUBLE drme(0.0);
	SU3xSU2::RME::DOUBLE dsu3_x_rme1;
	size_t irho1;
	for (size_t irho2(0), isu39lm(0); irho2 < irho2_max; irho2++ )
	{
	  dsu3_x_rme1 = 0.0;
	  for (irho1 = 0; irho1 < irho1_max; ++irho1, ++isu39lm )
	  {
		 dsu3_x_rme1 += su39lm[isu39lm]*rme1[irho1];
	  }
	  drme += rme2[irho2]*dsu3_x_rme1;
	}
	return drme;
}

double SpinCoeff(int SSf, int SS0, int SSi, int SS1, int SS2, int SS1p, int SS2p, int SSg1, int SSg2)
{
	static CWig9jLookUpTable<SU3xSU2::RME::DOUBLE> wig9jTable;
	double dSu2 = std::sqrt((SSi + 1) * (SS0 + 1) * (SS1 + 1) * (SS2 + 1));
   dSu2 *= wig9jTable.GetWigner9j(SS1p, SS2p, SSi, SSg1, SSg2, SS0, SS1, SS2, SSf);
   return dSu2;
}
//	Constructor implements formula (5.2) which evaluates reduced matrix elements of the two-component system.
//	Input:
//
//	Bra_ 	== (lmf muf)Sf
//	Ket_ 	== (lmi mui)Si
//	Tensor_ == (lm0 mu0)S0
//
//	NOTE: Bra_.rho ( == rhof), Ket_.rho (==rhoi), and Tensor_.rho (==rho0) is
//	calculated does not need to be supplied as it is calculated from
//	(rme1->Bra_ x rme2->Bra_) --> rhof rme.Bra_ This coupling is performed
//	inside of SU39lmRME constructo.
//
//	rme1 ... < alpha_{1} (lm1 mu1) S1 ||| T^{alpha_{gamma1} (lm_{gamma1} mu_{gamma1})S_{gamma1} ||| alpha_{1}' (lm1' mu1')S1'>_{rho1}
//	rme2 ... < alpha_{2} (lm2 mu2) S2 ||| T^{alpha_{gamma2} (lm_{gamma2} mu_{gamma2})S_{gamma2} ||| alpha_{2}' (lm2' mu1')S2'>_{rho2}
//	spinless = false (implicit value) ==> multiply by dSU2
//	spinless = true ==> multiply by dSU2=1.0
SU3xSU2::RME::RME(	const SU3xSU2::LABELS& Bra_, 
					const SU3xSU2::LABELS& Tensor_, 
					const SU3xSU2::LABELS& Ket_, 
					const SU3xSU2::RME* rme1, const SU3xSU2::RME* rme2, 
					SU3xSU2::RME::DOUBLE* buffer, bool spinless)
				: m_operatorType(MultipleShell), Bra(Bra_), Ket(Ket_),  Tensor(Tensor_), m_rhot_max(SU3::mult(Ket_, Tensor_, Bra_))
				{
//	Structure of SU(3) 9-lm coefficients  needed for formula (5.2)
//	
//	rme1.Ket   			rme1.Tensor   		rme1.Bra  			rme1.rhot_max == rho1_max
//	rme2.Ket   			rme2.Tensor   		rme2.Bra  			rme2.rhot_max == rho2_max
//	rme.Ket    			rme.Tensor    		rme.Bra   			rme.rhot_max
//	su39lm.rhoi_max		su39lm.rho0_max		su39lm.rhof_max
//
//	SU3 package stores SU3 9lm in the array with the following structure:
//
//	i = rhot*(rho1_max*rho2_max*rhof_max*rhoi_max*rho0_max) + rho0*(rho1_max*rho2_max*rhof_max*rhoi_max) + rhoi*(rho1_max*rho2_max*rhof_max) + rhof*(rho1_max*rho2_max)
//		+ rho2*rho1_max + rho1
//
	CWig9lmLookUpTable<RME::DOUBLE> su39lm;
	static CWig9jLookUpTable<DOUBLE> wig9jTable;

	DOUBLE* dsu39lm;
	SU3xSU2::RME::DOUBLE* drme1;
	SU3xSU2::RME::DOUBLE* drme2;

//	AccTime<TIMER_RME_CSTR> c;

	{
//	  AccTime<TIMER_RME_CSTR_9lm> c;
	  if (rme1->Bra.lm || rme1->Bra.mu || rme1->Tensor.lm || rme1->Tensor.mu || rme1->Ket.lm || rme1->Ket.mu)
		 {
			su39lm.GetWigner9lm(rme1->Bra, rme1->Tensor, rme1->Ket, rme2->Bra, rme2->Tensor, rme2->Ket, Bra, Tensor, Ket);
		 }
	  else
		 {
			su39lm.GetWigner9lm(rme2->Bra, rme2->Tensor, rme2->Ket, Bra, Tensor, Ket);
		 }
	}

	int rho1_max;
	int rho2_max;
	int rhoi_max;
	int rhof_max;
	int rho0_max;

	int n2bra;
	int n2ket;
	int n2tensor;


	
	{
	  //	  AccTime<TIMER_RME_CSTR_VAR_INIT> c;

	  rho1_max 	= rme1->m_rhot_max;
	  rho2_max 	= rme2->m_rhot_max;
	  rhoi_max 	= su39lm.rhoi_max();
	  rhof_max	= su39lm.rhof_max();
	  rho0_max	= su39lm.rho0_max();
	  
	  n2bra = rme2->m_bra_max * rhof_max;
	  n2ket = rme2->m_ket_max * rhoi_max;
	  n2tensor = rme2->m_tensor_max*rho0_max;


	  Bra.rho = rhof_max;
	  Ket.rho = rhoi_max;
	  Tensor.rho = rho0_max;
	  m_bra_max = rme1->m_bra_max * rme2->m_bra_max * Bra.rho;
	  m_ket_max = rme1->m_ket_max * rme2->m_ket_max * Ket.rho;
	  m_tensor_max = rme1->m_tensor_max * rme2->m_tensor_max * Tensor.rho;
	  m_n3 = m_ket_max*m_tensor_max*m_rhot_max;
	  m_n2 = m_tensor_max*m_rhot_max;
	  
	  m_ntotal = m_bra_max * m_n3;
	}

   m_rme = (buffer == NULL) ? new SU3xSU2::RME::DOUBLE[m_ntotal]: buffer; 
	memset(m_rme, 0, m_ntotal*sizeof(SU3xSU2::RME::DOUBLE));	// set all elements of m_rme = 0.0

	DOUBLE dSu2 = 1.0;
   if (!spinless)
	{
	  dSu2 = std::sqrt((Ket.S2+1)*(Tensor.S2+1)*(rme1->Bra.S2+1)*(rme2->Bra.S2+1));
     dSu2 *= wig9jTable.GetWigner9j(rme1->Ket.S2, rme2->Ket.S2, Ket.S2, rme1->Tensor.S2,
                                         rme2->Tensor.S2, Tensor.S2, rme1->Bra.S2, rme2->Bra.S2,
                                         Bra.S2);
   }

	{
	  int rhof, rhoi, rho0, rhot, ibra1, iket1, itensor1, ibra2, iket2, itensor2, ibra, iket, itensor0;
	  for (rhof = 0; rhof < rhof_max; ++rhof)
		 {
			for (rhoi = 0; rhoi < rhoi_max; ++rhoi)
			  {
				 for (rho0 = 0; rho0 < rho0_max; ++rho0)
					{
					  for (rhot = 0; rhot < m_rhot_max; ++rhot)
						 {
							dsu39lm = su39lm.GetMatrix(rhot, rho0, rhoi, rhof);
							for (ibra1 = 0; ibra1 < rme1->m_bra_max; ++ibra1)
							  {
								 for (iket1 = 0; iket1 < rme1->m_ket_max; ++iket1)
									{
									  for (itensor1 = 0; itensor1 < rme1->m_tensor_max; ++itensor1)
										 {
											drme1 = rme1->GetVector(ibra1, iket1, itensor1); 
											if (drme1 == NULL) {
											  continue;
											}
											for (ibra2 = 0; ibra2 < rme2->m_bra_max; ++ibra2)
											  {
												 for (iket2 = 0; iket2 < rme2->m_ket_max; ++iket2)
													{
													  for (itensor2 = 0; itensor2 < rme2->m_tensor_max; ++itensor2)
														 {
															drme2 = rme2->GetVector(ibra2, iket2, itensor2);
															if (drme2 == NULL) {
															  continue;
															}
															SU3xSU2::RME::DOUBLE drme = dSu2*rme2_x_su39lm_x_rme1(rho1_max, rho2_max, dsu39lm, drme1, drme2); 
															ibra = ibra1 * n2bra + ibra2	* rhof_max + rhof; 	// alpha_{f} in (5.3)
															iket = iket1 * n2ket + iket2 * rhoi_max + rhoi;	// alpha_{i} in (5.5)
															itensor0 = itensor1 * n2tensor + itensor2 * rho0_max + rho0; // aplha_{0} in (5.4)
															Set(ibra, iket, itensor0, rhot, drme);
														 }
													}
											  }
										 }
									}
							  }
						 }
					}
			  }
		 }
	}
}

//	This function calculates total number of rme elements for a given SU(3) rme table
size_t MultTotal(	const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_VEC& omega_bra, 
					const std::vector<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_VEC& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_VEC& omega_ket)
{
	int ntot = 1;
	size_t nOmegas = omega_bra.size();
	for (size_t i = 0; i < nOmegas; ++i)
	{
//		ntot *= gamma_bra[i].mult*gamma_ket[i].mult*omega_ket[i].rho*omega_bra[i].rho*Omega_t[i].rho*Gamma_t[i]->m_tensor_max*Gamma_t[i]->Tensor.rho;
		ntot *= gamma_bra[i].mult*gamma_ket[i].mult*omega_ket[i].rho*omega_bra[i].rho*abs(Omega_t[i].rho)*Gamma_t[i]->m_tensor_max*Gamma_t[i]->Tensor.rho;
	}	
	ntot *= gamma_bra[nOmegas].mult*gamma_ket[nOmegas].mult*Gamma_t[nOmegas]->m_tensor_max*Gamma_t[nOmegas]->Tensor.rho;
	return ntot*SU3::mult(omega_ket[nOmegas-1], Omega_t[nOmegas-1], omega_bra[nOmegas-1]); //	ntot*rhot_max
}

size_t GetMaximalBufferSize(	const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_SMALL_VEC_BASE& omega_bra, 
								const boost::container::small_vector_base<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_SMALL_VEC_BASE& Omega_t, 
								const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_SMALL_VEC_BASE& omega_ket)
{
	assert(!omega_bra.empty());

	int ntot_max = 1;
	for (size_t nOmegas = 1;  nOmegas <= omega_bra.size(); nOmegas++) // omega_bra has at least one element
	{
		int ntot = 1;
		for (size_t i = 0; i < nOmegas; ++i)
		{
			ntot *= gamma_bra[i].mult*gamma_ket[i].mult*omega_ket[i].rho*omega_bra[i].rho*abs(Omega_t[i].rho)*Gamma_t[i]->m_tensor_max*Gamma_t[i]->Tensor.rho;
		}	
		ntot *= gamma_bra[nOmegas].mult*gamma_ket[nOmegas].mult*Gamma_t[nOmegas]->m_tensor_max*Gamma_t[nOmegas]->Tensor.rho;
		ntot *= SU3::mult(omega_ket[nOmegas-1], Omega_t[nOmegas-1], omega_bra[nOmegas-1]); //	ntot*rhot_max
		if (ntot_max < ntot)
		{
			ntot_max = ntot;
		}
	}
	return ntot_max;
}


//	The function CalculateRME_1 calculates rme. The resulting rmes are being stored in 
//	an empty SU3xSU2::RME structure, resultingRME. 
void CalculateRME_1(const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_VEC& omega_bra, 
					const std::vector<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_VEC& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_VEC& omega_ket, 
					SU3xSU2::RME& resultingRME)
{
	size_t nOmegas = Omega_t.size();
	assert(gamma_bra.size()  == nOmegas + 1);
	assert(gamma_ket.size()  == nOmegas + 1);
	assert(Gamma_t.size()  	 == nOmegas + 1);
	assert(omega_bra.size()  == nOmegas);
	assert(omega_ket.size()  == nOmegas);

	assert(resultingRME.m_ntotal == 0 && resultingRME.m_rme == NULL);

//	calculate <omega_bra[0] ||| {T^{Omega_t[0]} ||| omega_ket[0] > using 
	SU3xSU2::RME rme(omega_bra[0], Omega_t[0], omega_ket[0], Gamma_t[0], Gamma_t[1], NULL); 
	for (size_t i = 1; i < nOmegas; ++i)
	{
		SU3xSU2::RME rme_new(omega_bra[i], Omega_t[i], omega_ket[i], &rme, Gamma_t[i + 1], NULL);
		delete []rme.m_rme; // rme.m_rme is not needed ==> deallocate
		rme = rme_new;		// copy new set of rme into rme 
	}
	resultingRME = rme;
}

//	The function CalculateRME_2 implements reduction rule for calculationg rme. 
//	The resulting rmes are being stored in an empty SU3xSU2::RME structure, resultingRME. 
//
//	Note that this function requires at least two subspaces, i.e. omega_bra and
//	omega_ket must have at least one element.
void CalculateRME_2(const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_SMALL_VEC_BASE& omega_bra, 
					const boost::container::small_vector_base<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_SMALL_VEC_BASE& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_SMALL_VEC_BASE& omega_ket, 
					SU3xSU2::RME& resultingRME, bool spinless)
{
	size_t nOmegas = Omega_t.size();

	assert(nOmegas >= 1 && Gamma_t.size() >= 2);	//	==> it is guaranteed that Gamma_t[0] and Gamma_t[1] exist as well as Omega_t[0]

	assert(gamma_bra.size()  == nOmegas + 1);
	assert(gamma_ket.size()  == nOmegas + 1);
	assert(Gamma_t.size()  	 == nOmegas + 1);

	assert(omega_bra.size()  == nOmegas);
	assert(omega_ket.size()  == nOmegas);
	assert(Omega_t.size()  	 == nOmegas);

	assert(resultingRME.m_ntotal == 0 && resultingRME.m_rme == NULL);
//	18/03/2011: profiling has revealed that 2% of computing time is spend in GetMaximalBufferSize(...).
//				Therefore, I decided to comment it and instead use a constant that is large enough.
	size_t ntot = GetMaximalBufferSize(gamma_bra, omega_bra, Gamma_t, Omega_t, gamma_ket, omega_ket);
//	size_t ntot = 1024;

	SU3xSU2::RME::DOUBLE* buffer1 = new SU3xSU2::RME::DOUBLE[ntot];
	SU3xSU2::RME::DOUBLE* buffer2 = new SU3xSU2::RME::DOUBLE[ntot];
	SU3xSU2::RME::DOUBLE* tmp = buffer2;

	//	This line explains why we first check assert(nOmegas >= 1 && Gamma_t.size() >= 2)
	SU3xSU2::RME rme(omega_bra[0], Omega_t[0], omega_ket[0], Gamma_t[0], Gamma_t[1], buffer1, spinless); 
	for (size_t i = 1; i < nOmegas; ++i)
	{
		SU3xSU2::RME rme_new(omega_bra[i], Omega_t[i], omega_ket[i], &rme, Gamma_t[i + 1], tmp, spinless); // NULL => array will be allocated ...
		tmp = rme.m_rme;
		rme = rme_new; // 
	}
	resultingRME = rme;
	// We must erase that memory buffer which is not assigned to resultingRME
	if (resultingRME.m_rme != buffer1) // ==> buffer1 is not needed => delete
	{
		delete []buffer1;
	}
	else 
	{
		delete []buffer2;
	}
	// Note that user is responsible for erasing resultingRME.m_rme
}


void ShowTable(	const UN::SU3xSU2_VEC& gamma_bra, const SU3xSU2_VEC& omega_bra, 
				const std::vector<SU3xSU2::RME*>& Gamma_t, const SU3xSU2_VEC& Omega_t, 
				const UN::SU3xSU2_VEC& gamma_ket, const SU3xSU2_VEC& omega_ket)
{
	assert(gamma_bra.size()  == Gamma_t.size());
	assert(gamma_ket.size()  == Gamma_t.size());
	assert(omega_bra.size()  == Omega_t.size());
	assert(omega_ket.size()  == Omega_t.size());

	for (size_t i = 0; i < gamma_ket.size(); ++i)
	{
		std::cout << gamma_ket[i] << "\t\t\t";
	}
	std::cout << std::endl;

	std::cout << "\t\t\t\t";
	for (size_t i = 0; i < omega_ket.size(); ++i)
	{
		std::cout << omega_ket[i] << "\t\t\t";
	}
	std::cout << std::endl;

	for (size_t i = 0; i < Gamma_t.size(); ++i)
	{
		if (Gamma_t[i])
		{
			std::cout << Gamma_t[i]->m_tensor_max << "(" << (int)Gamma_t[i]->Tensor.lm << " " << (int)Gamma_t[i]->Tensor.mu << ")" << (int)Gamma_t[i]->Tensor.S2 << "\t\t\t";
		} 
		else
		{
			std::cout << "I(0 0)0" << "\t\t\t";
		}
	}
	std::cout << std::endl;

	std::cout << "\t\t\t\t";
	for (size_t i = 0; i < Omega_t.size(); ++i)
	{
		std::cout << Omega_t[i] << "\t\t\t";
	}
	std::cout << std::endl;

	for (size_t i = 0; i < gamma_bra.size(); ++i)
	{
		std::cout << gamma_bra[i] << "\t\t\t";
	}
	std::cout << std::endl;

	std::cout << "\t\t\t\t";
	for (size_t i = 0; i < omega_bra.size(); ++i)
	{
		std::cout << omega_bra[i] << "\t\t\t";
	}
	std::cout << std::endl;
}

void SU3xSU2::RME::ShowVector(size_t ibra, size_t iket, size_t itensor)
{
	SU3xSU2::RME::DOUBLE *vec = GetVector(ibra, iket, itensor);
	size_t irhot = 0;
	std::cout << "{";
	for (; irhot < m_rhot_max-1; ++irhot)
	{
		std::cout << vec[irhot] << ",";
	}
	std::cout << vec[irhot] << "}" << std::endl;
}

void SU3xSU2::RME::ShowStructure()
{
	switch (m_operatorType)
	{
		case SingleShell: std::cout << "Operator type ... single-shell operator" << std::endl; break;
		case MultipleShell: std::cout << "Operator type ... multiple-shell operator" << std::endl; break;
		case Identity: std::cout << "Operator type ... identity operator" << std::endl; break;
	}
	std::cout << "<bra_max = " << m_bra_max << " " << (int)Bra.rho << "(" << (int)Bra.lm << " " << (int)Bra.mu << ")" << (int)Bra.S2 << "|||";
	if (m_operatorType != Identity)
	{
		std::cout << "T^{" << m_tensor_max << " " << (int)Tensor.rho << "(" << (int)Tensor.lm << " " << (int)Tensor.mu << ")}_{" << (int)Tensor.S2 << "}";
	}
	else
	{
		std::cout << "I^{" << m_tensor_max << "(" << (int)Tensor.lm << " " << (int)Tensor.mu << ")}_{" << (int)Tensor.S2 << "}";
	}
	std::cout << "|||ket_max = " << m_ket_max << " " << (int)Ket.rho << "(" << (int)Ket.lm << " " << (int)Ket.mu << ")" << (int)Ket.S2 << ">";
	std::cout << "rhot_max = " << m_rhot_max << std::endl;
	std::cout << "ntotal = " << m_ntotal << std::endl;
}

void SU3xSU2::RME::Show()
{
	for (size_t ibra = 0; ibra < m_bra_max; ++ibra)
	{
		for (size_t iket = 0; iket < m_ket_max; ++iket)
		{
			for (size_t itensor = 0; itensor < m_tensor_max; ++itensor)
			{
				SU3xSU2::RME::DOUBLE *data = GetVector(ibra, iket, itensor);
				for (size_t i = 0; i < m_rhot_max; ++i)
				{
					std::cout << data[i] << " ";
				}
				std::cout << "\t" << "ibra = " << ibra << " " << "iket = " << iket << " itensor = " << itensor << std::endl;
			}
		}
	}
}

double Calculate_SpinCoeff(const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_SMALL_VEC_BASE& omega_bra, 
					const boost::container::small_vector_base<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_SMALL_VEC_BASE& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_SMALL_VEC_BASE& omega_ket) 
{
	size_t nOmegas = Omega_t.size();

	assert(nOmegas >= 1 && Gamma_t.size() >= 2);	//	==> it is guaranteed that Gamma_t[0] and Gamma_t[1] exist as well as Omega_t[0]

	assert(gamma_bra.size()  == nOmegas + 1);
	assert(gamma_ket.size()  == nOmegas + 1);
	assert(Gamma_t.size()  	 == nOmegas + 1);

	assert(omega_bra.size()  == nOmegas);
	assert(omega_ket.size()  == nOmegas);
	assert(Omega_t.size()  	 == nOmegas);

   int SSf, SSi, SS0, SS1, SS2, SS1p, SS2p, SSg1, SSg2;

   SSf = omega_bra[0].S2;
   SS0 = Omega_t[0].S2;
   SSi = omega_ket[0].S2;

   SS1 = Gamma_t[0]->Bra.S2;
   SSg1 = Gamma_t[0]->Tensor.S2;
   SS1p = Gamma_t[0]->Ket.S2;

   SS2 = Gamma_t[1]->Bra.S2;
   SSg2 = Gamma_t[1]->Tensor.S2;
   SS2p = Gamma_t[1]->Ket.S2;
	//	This line explains why we first check assert(nOmegas >= 1 && Gamma_t.size() >= 2)
	double dSu2 = SpinCoeff(SSf, SS0, SSi, SS1, SS2, SS1p, SS2p, SSg1, SSg2);
	for (size_t i = 1; i < nOmegas; ++i)
	{
      SS1 = SSf;
      SSg1 = SS0;
      SS1p = SSi;

      SSf = omega_bra[i].S2;
      SS0 = Omega_t[i].S2;
      SSi = omega_ket[i].S2;

      SS2 = Gamma_t[i + 1]->Bra.S2;
      SSg2 = Gamma_t[i + 1]->Tensor.S2;
      SS2p = Gamma_t[i + 1]->Ket.S2;

      dSu2 *= SpinCoeff(SSf, SS0, SSi, SS1, SS2, SS1p, SS2p, SSg1, SSg2);
	}
   return dSu2;
}

