#ifndef INTTERACTION2SU3TENSORS_H
#define INTTERACTION2SU3TENSORS_H
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <iostream>
#include <set>

////////////////////////////////////////////////////////////////////////////////////////////
//	This program transforms two-body proton-proton/neutron-neutron J-coupled interaction
//	into SU(3) tensorial form according to equation (4.2) on page 11 at SU3NCSM.pdf
////////////////////////////////////////////////////////////////////////////////////////////
template <class CTwoBodyMatrixElements>	//	class storing two body matrix elements
class CScalarTwoBodyOperators2SU3Tensors
{
	public:
	CScalarTwoBodyOperators2SU3Tensors(const CTwoBodyMatrixElements& twoBodyME, const int maxShell): twoBodyME_(twoBodyME) 
	{
		GenerateAllCombinationsHOShells(maxShell);
	} 
	CScalarTwoBodyOperators2SU3Tensors(const CTwoBodyMatrixElements& twoBodyME, const int Nmax, const int valence_shell, const int maxShell): twoBodyME_(twoBodyME) 
	{
		GenerateAllCombinationsHOShells(Nmax, valence_shell, maxShell);
	} 

	template<typename SelectionRules>
	size_t PerformSU3Decomposition(const SelectionRules& IsIncluded, const std::string& sOutputFileName);
	private:
//	This is really clumsy: TODO implement storing in one-dimension vector	
	void GetCoefficientsScalar(	const SU3::LABELS ir[], const SU3::LABELS& IRf, const int Sf, const SU3::LABELS& IRi, const int Si, const SU3::LABELS& Irrep0, const int S0, const int k0, 
								const std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > >& vCGs_abf, 
								const std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > >& vCGs_dci,
								const std::vector<std::pair<K1L1K2L2K3, std::vector<double> > >& vCGs_fi0,
								std::vector<double>& Coeffs);

/** Calculate Wigner 9j coefficients needed for the decomposition of a two-body interaction:
 *  \f{displaymath}{
 *  \left\{
 *  \begin{array}{ccc}
 *    a &   b & c \\
 *  1/2 & 1/2 & f \\
 *    g &   h & j 
 *  \end{array}
 *  \right\}}\f}
 */ 
	double GetWigner9j(int a2, int b2, int c2, int f2, int g2, int h2, int j2);
/** Calculate Wigner 6j coefficients 
 *  \f{displaymath}{
 *  \left\{
 *  \begin{array}{ccc}
 *    a & b & c \\
 *    d & e & f 
 *  \end{array}
 *  \right\}}\f}
 */ 
	double GetWigner6j(int a2, int b2, int c2, int d2, int e2, int f2);
//
//	Can be uncommented if the struct/class provided by the template parameter CTwoBodyMatrixElements implements .size() and .GetLabels(), 
//	void GenerateUniqueHOShells();
	void GenerateAllCombinationsHOShells(const int maxShell);
	void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell);

	enum {A, B, C, D};
	enum {NA, NB, NC, ND};
	std::set<CTuple<int, 4> > nanbncndCombinations_;
	const CTwoBodyMatrixElements& twoBodyME_;
};


template <class CTwoBodyMatrixElements>	
double CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::GetWigner9j(int a2, int b2, int c2, int f2, int g2, int h2, int j2)
{
	static std::map<CTuple<int, 2>, double> wig9js;
	static CTuple<int, 2> key;

//	set key
	key[0] = (a2 << 24) | (b2 << 16) | (c2 << 8) | f2;
	key[1] =              (g2 << 16) | (h2 << 8) | j2;

	std::map<CTuple<int, 2>, double>::const_iterator wig9j_to_be_found = wig9js.find(key);
	if (wig9j_to_be_found == wig9js.end())
	{
		double d = wigner9j(a2, b2, c2, 1, 1, f2, g2, h2, j2);
		wig9js.insert(std::make_pair(key, d));
		return d;
	}
	else
	{
		return wig9j_to_be_found->second;
	}
}

template <class CTwoBodyMatrixElements>	
double CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::GetWigner6j(int a2, int b2, int c2, int d2, int e2, int f2)
{
	static std::map<CTuple<int, 2>, double> wig6js;
	static CTuple<int, 2> key;

//	set key
	key[0] = (a2 << 24) | (b2 << 16) | (c2 << 8) | d2;
	key[1] =                           (e2 << 8) | f2;

	std::map<CTuple<int, 2>, double>::const_iterator wig6j_to_be_found = wig6js.find(key);
	if (wig6j_to_be_found == wig6js.end())
	{
		double d = wigner6j(a2, b2, c2, d2, e2, f2);
		wig6js.insert(std::make_pair(key, d));
		return d;
	}
	else
	{
		return wig6j_to_be_found->second;
	}
}

//	Iterate over two-body matrix elements and create a list of all 
//	{na, nb, nc, nd} combinations in < na la ja; nb lb jb J || V || nc lc jc; nd ld jd J>
template <class CTwoBodyMatrixElements>	
void CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::GenerateAllCombinationsHOShells(const int maxShell)
{
	CTuple<int, 4> na_nb_nc_nd;
	for (int na = 0; na <= maxShell; ++na)
	{
		na_nb_nc_nd[NA] = na;
		for (int nb = 0; nb <= maxShell; ++nb)
		{
			na_nb_nc_nd[NB] = nb;
			for (int nc = 0; nc <= maxShell; ++nc)
			{
				na_nb_nc_nd[NC] = nc;
				for (int nd = 0; nd <= maxShell; ++nd)
				{
					na_nb_nc_nd[ND] = nd;
					nanbncndCombinations_.insert(na_nb_nc_nd);	//	in such case we do not need 
				}
			}
		}
	}
}

template <class CTwoBodyMatrixElements>	
void CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell)
{
	CTuple<int, 4> na_nb_nc_nd;
	for (int na = 0; na <= maxShell; ++na)
	{
		for (int nb = 0; nb <= maxShell; ++nb)
		{
			int adxad_Nhw = 0;
			if (na > valence_shell)
			{
				adxad_Nhw += (na - valence_shell);
			}
			if (nb > valence_shell)
			{
				adxad_Nhw += (nb - valence_shell);
			}
			if (adxad_Nhw > Nmax)
			{
				continue;
			}
			na_nb_nc_nd[NA] = na;
			na_nb_nc_nd[NB] = nb;

			for (int nc = 0; nc <= maxShell; ++nc)
			{
				for (int nd = 0; nd <= maxShell; ++nd)
				{
					int taxta_Nhw = 0;
					if (nc > valence_shell)
					{
						taxta_Nhw += (nc - valence_shell);
					}
					if (nd > valence_shell)
					{
						taxta_Nhw += (nd - valence_shell);
					}
					if (taxta_Nhw > Nmax)
					{
						continue;
					}
					na_nb_nc_nd[NC] = nc;
					na_nb_nc_nd[ND] = nd;
					nanbncndCombinations_.insert(na_nb_nc_nd);	//	in such case we do not need 
				}
			}
		}
	}
}


/*
 * I am too lazy to implement .size() and .GetLabels() for CTwoBodyMatrixElements stuct which
 * calculates L2me on-the-fly, which is a fast process. For the interaction given in the input 
 * file, as represented by JTCoupled2BMe_Hermitian::size() and JTCoupled2BMe_Hermitian::GetLabels()
 * one can uncomment and use for following method.
 *
//	Iterate over two-body matrix elements and create a list of all 
//	{na, nb, nc, nd} combinations in < na la ja; nb lb jb J || V || nc lc jc; nd ld jd J> or < na la ja; nb lb jb J T || V || nc lc jc; nd ld jd J T>
template <class CTwoBodyMatrixElements>	
void CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::GenerateUniqueHOShells()
{
	CTuple<int, 4> na_nb_nc_nd;

	size_t nTerms = twoBodyME_.size();;
	for (size_t iTerm = 0; iTerm < nTerms; ++iTerm)
	{
		int l, j;
		const typename CTwoBodyMatrixElements::Labels& labels = twoBodyME_.GetLabels(iTerm);

		twoBodyME_.Get_nlj(labels[jt_coupled_bra_ket::I1], na_nb_nc_nd[NA], l, j);
		twoBodyME_.Get_nlj(labels[jt_coupled_bra_ket::I2], na_nb_nc_nd[NB], l, j);
		twoBodyME_.Get_nlj(labels[jt_coupled_bra_ket::I3], na_nb_nc_nd[NC], l, j);
		twoBodyME_.Get_nlj(labels[jt_coupled_bra_ket::I4], na_nb_nc_nd[ND], l, j);

		nanbncndCombinations_.insert(na_nb_nc_nd);
	}
}
*/


template <class CTwoBodyMatrixElements>	
template <typename SelectionRules>
size_t CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::PerformSU3Decomposition(const SelectionRules& IsIncluded, const std::string& sOutputFileName)
{
	const int SiSfS0[6][3] = {{0, 0, 0}, {0, 2, 2}, {2, 0, 2}, {2, 2, 0}, {2, 2, 2}, {2, 2, 4}};
	std::ofstream outfile(sOutputFileName.c_str());
	if (!outfile)
	{
		std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
		exit(EXIT_FAILURE);
	}
	outfile.precision(10);
	size_t nTensors = 0;
	CSU3CGMaster SU3CGFactory;
	size_t iS, rho0, rho0max, index;
	int k0, L0, kmax;
	int n1, n2, n3, n4;
	double dPhase;
//	ir[NA] = (na 0), ir[NB] = (nb 0), ir[NC] = (0 nc), ir[ND] = (0 nd)	
	SU3::LABELS ir[4] = {SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0)};

	SU3_VEC::const_iterator LAST_NDNC_IRREP;
	SU3_VEC::const_iterator Irrep0, LAST_FI_IRREP;

	std::set<CTuple<int, 4> >::const_iterator na_nb_nc_nd = nanbncndCombinations_.begin();
	std::set<CTuple<int, 4> >::const_iterator LAST_NA_NB_NC_ND = nanbncndCombinations_.end();

	for (; na_nb_nc_nd != LAST_NA_NB_NC_ND; ++na_nb_nc_nd)
	{
		SU3_VEC FinalIrreps;	//	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
		SU3_VEC InitIrreps;		//	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)

		ir[A].lm = (*na_nb_nc_nd)[NA];	//	ir[A] = (na 0)
		ir[B].lm = (*na_nb_nc_nd)[NB];	//	ir[B] = (nb 0)	
		ir[D].mu = (*na_nb_nc_nd)[ND];	//	ir[D] = (0 nd)
		ir[C].mu = (*na_nb_nc_nd)[NC];	//	ir[C] = (0 nc)

//		na  nb  nd  nc
//		|   |   |   |
//		|   |   |   |
//		V   V   V   V
//		n1  n2  n3  n4
		n1 = ir[A].lm;
		n2 = ir[B].lm;
		n3 = ir[D].mu;
		n4 = ir[C].mu;

		if (!IsIncluded(n1, n2, n3, n4)) // operator does not have this combination of n1 n2 n3 n4
		{
			continue;
		}

		std::cout << ir[A] << " " << ir[B] << " " << ir[C] << " " << ir[D] << "\n";

		SU3::Couple(ir[A], ir[B], FinalIrreps);	//	(na 0) x (nb 0) -> rho=1{(lmf muf)}
		SU3::Couple(ir[D], ir[C], InitIrreps);	//	(0 nd) x (0 nc) -> rho=1{(lmi mui)}	

		SU3_VEC::const_iterator LAST_NANB_IRREP = FinalIrreps.end();
//	iterate over (lmf muf) irreps		
		for (SU3_VEC::const_iterator IRf = FinalIrreps.begin(); IRf != LAST_NANB_IRREP; ++IRf)
		{
//	{<{k1, l1, k2, l2, k3, l3}, {<(na 0) k1 l1; (nb 0) k2 l2 || (lmf muf) k3 L3>_{0}}>}			
			std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > > vCGs_abf;
//	calculate < (na 0) ..., (nb 0) ... || (lmf muf) ...>_{rhomax=1}	Wigner SU(3) coefficients
//	this data structure will be used in GetCoefficients( ... )
			SU3CGFactory.GetSO3(ir[NA], ir[NB], *IRf, vCGs_abf);

			SU3_VEC::const_iterator LAST_NDNC_IRREP = InitIrreps.end();
//	iterate over (lmi mui) irreps		
			for (SU3_VEC::const_iterator IRi = InitIrreps.begin(); IRi != LAST_NDNC_IRREP; ++IRi)
			{
//	{<{k1, l1, k2, l2, k3, l3}, {<(0 nd) k1 l1; (0 nc) k2 l2 || (lmi mui) k3 L3>_{0}}>}			
				std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > > vCGs_dci;
//	calculate < (0 nd) ..., (0 nc) ... || (lmi mui) ...>_{rhomax=1}	Wigner SU(3) coefficients
//	this data structure will be used in GetCoefficients( ... )
				SU3CGFactory.GetSO3(ir[ND], ir[NC], *IRi, vCGs_dci);

				SU3_VEC Irreps0;

				SU3::Couple(*IRf, *IRi, Irreps0); // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
				Irrep0 = Irreps0.begin();
				LAST_FI_IRREP = Irreps0.end();
//	iterate over rho(lmu0 mu0)
				for (; Irrep0 != LAST_FI_IRREP; ++Irrep0)
				{
					if (!IsIncluded(*Irrep0)) // Irrep0 does not have tensorial character of the operator
					{
						continue;
					}
					rho0max = Irrep0->rho;
//	iterate over S_{f} S_{i} S					
					for (iS = 0; iS < 6; ++iS)
					{						
						int Sf = SiSfS0[iS][0];
						int Si = SiSfS0[iS][1];
						int S0 = SiSfS0[iS][2];
						int L0 = S0;
						if (!IsIncluded(S0))
						{
							continue;
						}
						kmax = SU3::kmax(*Irrep0, L0/2);   // HERE WAS AN ERROR: one needs to provide L0/2 instead of L0 !!!
						if (!kmax) {	// L0 is not between allowed L's of irrep (lm0 mu0)
							continue;
						}
//	{<{k1, l1, k2, l2, k3}, {<(lmf muf) k1 l1; (lmi mui) k2 l2 || (lm0 mu0) k3 L0>_{0 ... rho0max}>}			
						std::vector<std::pair<K1L1K2L2K3, std::vector<double> > > vCGs_fi0;
//	calculate <(lmf muf) ...; (lmi mui) ... || (lm0 mu0) ... L0>_{...}
						SU3CGFactory.GetSO3(*IRf, *IRi, *Irrep0, L0, vCGs_fi0);

						std::vector<double> ResultingCoeffs(3*rho0max*kmax, 0.0);
//	iterate over k0
						std::vector<double> k0TensorCoeffs(3*rho0max); 
						for (k0 = 0; k0 < kmax; ++k0)
						{
//	Coeffs.first[rho][i] .... coefficient in front PP/NN tensor [(lmf muf)Sf x (lmi mui)Si]^{rho(lm0 mu0)}_{k0 (L0 S0) 0 0} for ith operator and proton-proton & neutron-neutron fermion operators
//	Coeffs.second[rho][i] .... coefficient in front PN  tensor [(lmf muf)Sf x (lmi mui)Si]^{rho(lm0 mu0)}_{k0 (L0 S0) 0 0} for ith operator and proton-neutron fermion tensor
							GetCoefficientsScalar(ir, *IRf, Sf, *IRi, Si, *Irrep0, S0, k0, vCGs_abf, vCGs_dci, vCGs_fi0, k0TensorCoeffs);

//	phase = (-)^{nd + nc + Sf + L0} * sqrt[(2Si+1)*(2Sf+1)*(2S0+1)]							
							dPhase  = MINUSto(ir[NC].mu + ir[ND].mu + Sf/2 + L0/2)*std::sqrt((Si + 1)*(Sf + 1)*(S0 + 1));
							std::transform(k0TensorCoeffs.begin(), k0TensorCoeffs.end(), k0TensorCoeffs.begin(), std::bind2nd(std::multiplies<double>(), dPhase));

							if (std::count_if(k0TensorCoeffs.begin(), k0TensorCoeffs.end(), Negligible) == k0TensorCoeffs.size())
							{
								continue;
							}
							ResultingCoeffs.insert(ResultingCoeffs.begin() + k0*rho0max*3, k0TensorCoeffs.begin(), k0TensorCoeffs.end());
						}

//	Check whether all coefficients in front of a given tensor are equal to zero	
						if (std::count_if(ResultingCoeffs.begin(), ResultingCoeffs.end(), Negligible) == ResultingCoeffs.size()) {
							continue;
						}
						std::cout << "\t\t" << *Irrep0 << "\n";
//	Store given tensor in outfile with the following structure:
//	n1 n2 n3 n4 IRf Sf IRi Si IR0 S0 
//	app ann apn			rho0 = 0, k0 = 0 
//	app ann apn			rho0 = 1, k0 = 0
//	.
//	.
//	app ann apn			rho0=max, k0 = 0
//	app ann apn			rho0=0, k0 = 1
//	.
//	app ann apn			rho0=rho0max, k0 = k0max
						outfile << n1 << " " << n2 << " " << n3 << " " << n4;
						outfile << "\t\t" << *IRf << " " << Sf << "\t" << *IRi << " " << Si << "\t" << *Irrep0;
						outfile << " " << S0 << "\n";
						for (k0 = 0, index = 0; k0 < kmax; ++k0)
						{
							for (rho0 = 0; rho0 < rho0max; ++rho0, index += 3)
							{
								if (Negligible(ResultingCoeffs[index + PP]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << ResultingCoeffs[index + PP] << " ";
								}

								if (Negligible(ResultingCoeffs[index + NN]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << ResultingCoeffs[index + NN] << " ";
								}

								if (Negligible(ResultingCoeffs[index + PN]))
								{
									outfile << 0 << "\n";
								}
								else
								{
									outfile << ResultingCoeffs[index + PN] << "\n";
								}
							}
						}
						nTensors++;
					}
				}
			}
		}
	}
	std::cout << "number of tensors: " << nTensors << std::endl;
	return nTensors;
}

/////////////////////////////////////////////////
//	ir[NA] = (na 0)
//	ir[NB] = (nb 0)
//	ir[NC] = (0 nc)
//	ir[ND] = (0 nd)
//	IRf = (lmf muf)
//	Sf
//	IRi = (lmi mui)
//	Si
//	Irrep0 = rho0 (lm0 mu0)
//	S0
//	k0
//	vCGs_abf	{<{k1, l1, k2, l2, k3, l3}, {<(na 0) k1 l1; (nb 0) k2 l2 || (lmf muf) k3 l3>_{0}}>}
//	vCGs_dci	{<{k1, l1, k2, l2, k3, l3}, {<(0 nd) k1 l1; (0 nc) k2 l2 || (lmi mui) k3 l3>_{0}}>}			
//	vCGs_fi0	{<{k1, l1, k2, l2, k3}, {<(lmf muf) k1 l1; (lmi mui) k2 l2 || (lm0 mu0) k3 L0>_{0}}>}
//	twoBodyME_  {<{I1, I2, I3, I4, J}, {< || V_{1} || > ... < || V_{nOPERATORS} > }>}
//	Coeffs ... resulting coefficients
//	Coeffs[rho][i] .... coefficient in front tensor [(lmf muf)Sf x (lmi mui)Si]^{rho(lm0 mu0)}_{k0 (L0 S0) 0 0} for ith operator
//
//	Coeffs are calculated according to formula docs/SU3Decomposition.pdf
//
//
//	New version: uses  Negligible8 to determine whether <  || > are equal to 0 
//	but most importantly, adds all positive partial coefficients and negative coefficients  to two separate values 
//	which are at the end added to yield the resulting coefficients. 
//  This should be useful if we have to add a lot of almost equal numbers with different signs ...
//  I feel I could not rule this possibillity and hence implemented as a precaution.
//	This may be an overkill though ...
template <class CTwoBodyMatrixElements>	
void CScalarTwoBodyOperators2SU3Tensors<CTwoBodyMatrixElements>::GetCoefficientsScalar(	const SU3::LABELS ir[], 
																								const SU3::LABELS& IRf, 
																								const int Sf,
																								const SU3::LABELS& IRi, 
																								const int Si,
																								const SU3::LABELS& Irrep0, 
																								const int S0, 
																								const int k0, 
																								const std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > >& vCGs_abf,
																								const std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > >& vCGs_dci,
																								const std::vector<std::pair<K1L1K2L2K3, std::vector<double> > >& vCGs_fi0,
																								std::vector<double>& Coeffs)
{
	std::vector<double> PositiveCoeffs(Coeffs.size(), 0);
	std::vector<double> NegativeCoeffs(Coeffs.size(), 0);
	double dPP, dNN, dPN;

	size_t nfi0 = vCGs_fi0.size();	//	total number of SU(3) CG's of type < (lmf muf) ..., (lmi mui) ... || (lm0 mu0) ... L0>_{...}
	size_t nabf = vCGs_abf.size();
	size_t ndci = vCGs_dci.size();
	size_t rho0max = Irrep0.rho;
	int L0 = S0;
	int kf, Lf, ki, Li;
	double dsu3[rho0max]; // (-)^{Li} x SQRT[Li] x SQRT[Lf] x <fi | 0>_{rho0} x <ab|f> x <dc|i>
	double dPhaseLi;
	size_t iab, idc;
	int la, lb, lc, ld;
	int ja, jb, jc, jd, J;
	j_coupled_bra_ket::TwoBodyLabels jcoupled_labels;

	Coeffs.assign(Coeffs.size(), 0);

//	iterate over {k1 l1 k2 l2 k3} i.e. over {kf Lf Ki Li k0} 	
	for (size_t ifi0 = 0; ifi0 < nfi0; ++ifi0)
	{
//	k3 must be equal to k0 	
		if (k0 != vCGs_fi0[ifi0].first.k3()) 
		{
			continue;
		}
//	if all <(lmf muf) k1 l1; (lmi mui) k2 l2 || (lm0 mu0) k0 L0>_{rho} SU(3) CGs are vanishing => skip the loop
		if (count_if((vCGs_fi0[ifi0].second).begin(), (vCGs_fi0[ifi0].second).end(), Negligible8) == rho0max)
		{
			continue;
		}

		kf = vCGs_fi0[ifi0].first.k1();	
		Lf = vCGs_fi0[ifi0].first.l1();

		ki = vCGs_fi0[ifi0].first.k2();
		Li = vCGs_fi0[ifi0].first.l2();

		dPhaseLi = MINUSto(Li/2)*std::sqrt((Li + 1)*(Lf + 1)); // (-)^{Li} \Pi_{LiLf}
//	iterate over {0, l1, 0, l2, k3 = kf, l3 = Lf} 
		for (size_t iabf = 0; iabf < nabf; ++iabf)
		{
//	select those for which k3 == kf & lf == Lf			
			if (vCGs_abf[iabf].first.k3() != kf || vCGs_abf[iabf].first.l3() != Lf)
			{
				continue;
			}
//	if <(na 0) 1 l1; (nb 0) 1 l2 || (lmf muf) kf Lf> vanishes => skip the loop
			if (Negligible8((vCGs_abf[iabf].second)[0]))
			{
				continue;
			}
			
			la = vCGs_abf[iabf].first.l1();
			lb = vCGs_abf[iabf].first.l2();
			for (size_t idci = 0; idci < ndci; ++idci)
			{
//	select those for which k3 == ki & l3 == Li			
				if (vCGs_dci[idci].first.k3() != ki || vCGs_dci[idci].first.l3() != Li)
				{
					continue;
				}
//	if <(0 nd) 1 l1; (0 nc) 1 l2 || (lmi mui) ki Li> vanishes => skip the loop
				if (Negligible8(vCGs_dci[idci].second[0])) 
				{
					continue;
				}

				ld = vCGs_dci[idci].first.l1();
				lc = vCGs_dci[idci].first.l2();

				for (size_t rho0 = 0; rho0 < rho0max; ++rho0)
				{
					dsu3[rho0] = dPhaseLi*vCGs_fi0[ifi0].second[rho0]*vCGs_abf[iabf].second[0]*vCGs_dci[idci].second[0];// <f i || 0>*< a b ||f>*< c d ||i>
				}
//	iterate over {ja, jb, jd, jc}				
//	and start preparing [I1, I2, I3, I4, J, Type] indices that are being used to find <I1 I2 J || V || I3 I4 J>
				for (ja = abs(la - 1); ja <= la + 1; ja += 2)
				{
					jcoupled_labels[j_coupled_bra_ket::I1] = twoBodyME_.Get_Index(ir[NA].lm, la/2, ja);
					for (jb = abs(lb - 1); jb <= lb + 1; jb += 2)
					{
						jcoupled_labels[j_coupled_bra_ket::I2] = twoBodyME_.Get_Index(ir[NB].lm, lb/2, jb);
						for (jc = abs(lc - 1); jc <= lc + 1; jc += 2)
						{
							jcoupled_labels[j_coupled_bra_ket::I3] = twoBodyME_.Get_Index(ir[NC].mu, lc/2, jc);
							for (jd = abs(ld - 1); jd <= ld + 1; jd += 2)
							{
								jcoupled_labels[j_coupled_bra_ket::I4] = twoBodyME_.Get_Index(ir[ND].mu, ld/2, jd);
								for (J = abs(ja - jb); J <= ja + jb; J += 2)
								{
									if (!SU2::mult(jc, jd, J)) // don't even try ... wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J) = 0
									{
										continue;
									}
									if (!SU2::mult(Lf, Sf, J)) // wigner9j(la, lb, Lf, 1, 1, Sf, ja, jb, J) = 0
									{
										continue;
									}
									if (!SU2::mult(Li, Si, J)) // wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J) = 0
									{
										continue;
									}
									jcoupled_labels[j_coupled_bra_ket::J] = (J/2); // !!!
//	All operators for both proton-proton/neutron-neutron and proton-neutron 
									typename CTwoBodyMatrixElements::DOUBLE mePP(0), meNN(0), mePN(0);
									bool bPPNNPNMe = twoBodyME_.MeJ(jcoupled_labels, mePP, meNN, mePN); 	// 	false ==> get matrix elements for two identical fermions (i.e. PP/NN)
									if (bPPNNPNMe)
									{
										double dsu2PPNN, dsu2PN;

										//	delta_{na nb}{la lb}{ja jb}{ta tb}
										iab = (ir[NA].lm == ir[NB].lm) && (la == lb) && (ja == jb);
										//	delta_{nc nd}{lc ld}{jc jd}{tc td}
										idc = (ir[ND].mu == ir[NC].mu) && (ld == lc) && (jd == jc);

										dsu2PN  = MINUSto((jd+jc)/2);
//	Calculating wigner 9j and 6j symbols on-the-fly is too expensive										
//										dsu2PN *= wigner9j(la, lb, Lf, 1, 1, Sf, ja, jb, J);
//										dsu2PN *= wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J);
//										dsu2PN *= wigner6j(Lf, Li, L0, Si, Sf, J);
										
										dsu2PN *= GetWigner9j(la, lb, Lf, Sf, ja, jb, J);
										dsu2PN *= GetWigner9j(ld, lc, Li, Si, jd, jc, J);
										dsu2PN *= GetWigner6j(Lf, Li, L0, Si, Sf, J);

										dsu2PN *= (J + 1)*std::sqrt((ja + 1)*(jb + 1)*(jc + 1)*(jd + 1));

										dsu2PPNN = dsu2PN;
										dsu2PPNN *= std::sqrt((iab + 1)*(idc + 1));

										for (size_t rho0 = 0, index = 0; rho0 < rho0max; ++rho0, index += 3)
										{
											dPP = dsu3[rho0]*mePP*dsu2PPNN; 	// proton-proton coefficient
//											dNN = 0; // for Vcoul only
											dNN = dsu3[rho0]*meNN*dsu2PPNN; 	// neutron-neutron coefficient
//											dPN = 0; // for Vcoul only
											dPN = dsu3[rho0]*mePN*dsu2PN; 		// proton-neutron coefficient

											if (dPP > 0)
											{
												PositiveCoeffs[index + PP] += dPP;
											}
											else
											{
												NegativeCoeffs[index + PP] += dPP;
											}

											if (dNN > 0)
											{
												PositiveCoeffs[index + NN] += dNN;
											}
											else
											{
												NegativeCoeffs[index + NN] += dNN;
											}

											if (dPN > 0)
											{
												PositiveCoeffs[index + PN] += dPN;
											}
											else
											{
												NegativeCoeffs[index + PN] += dPN;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	for (int i = 0; i < Coeffs.size(); ++i)
	{
		Coeffs[i] = PositiveCoeffs[i] + NegativeCoeffs[i];
	}
}
#endif
