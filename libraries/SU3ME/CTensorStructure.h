#ifndef CTENSORSTRUCTURE_H
#define CTENSORSTRUCTURE_H
#include<SU3ME/CRMECalculator.h>

#include <boost/container/small_vector.hpp>

class CTensorStructure
{
	public:
	unsigned char m_nShells;			//	number of active shells, and hence it equals to the size of arrays m_SingleShellTensors and m_Omega == n - 1
	CrmeTable** m_SingleShellTensors;	// { < ||| T^Gamma_0 ||| >, < ||| T^Gamma_1 || >, ...,  < ||| T^Gamma_n || >}
	WigEckSU3SO3CGTable* m_pSU3SO3CGTable; 
	SU3xSU2::LABELS* m_Omega;	//	array of m_nShells - 1 inter-shell coupling {Omega0, Omega1, ... Omega_{m_nShells - 1}}
	CTensorStructure(const std::vector<CrmeTable*>& SingleShellTensorRmeTables, const std::vector<SU3xSU2::LABELS*> vOmega, WigEckSU3SO3CGTable* pSU3SO3CGTable);
	CTensorStructure(const CTensorStructure& TS) { assert(0); }	// 	This constructor should never be called. 
	~CTensorStructure();
//	Does for each shell n in BraKetShells, do:
//	if n in TensorShells => 
//	gamma_ket[i] x Gamma_i --> gamma_bra[i], where 
//	Gamma_i = m_SingleShellTensors[i]->m_TensorLabels[m_nLabels-1] (see CrmeTable::operator SU3xSU2())
//	if BraKetShells[i] not in TensorShells ==>
//	check whether gamma_{ket}
	bool Couple(const UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const std::vector<unsigned char>& BraKetShells,  
				const std::vector<unsigned char>& TensorShells);

	CRMECalculator* GetRMECalculator( 	const UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, 
										const boost::container::small_vector_base<unsigned char>& Ket_confs,
										const std::vector<unsigned char>& BraKetShells,  
										const std::vector<unsigned char>& TensorShells, const int phase) const;

	void GetGammaOmega( const std::vector<unsigned char>& BraKetShells,  const std::vector<unsigned char>& TensorShells, SU3xSU2_VEC& Gamma_t, SU3xSU2_SMALL_VEC& Omega_t);

	size_t GetNCoefficients() const;

	void ShowLabels() const;
};

void StructureToSingleShellTensors(	const std::vector<char>& structure, std::vector<unsigned char>& Shells, 
									SU3xSU2_VEC& TensorLabels, 
									std::vector<std::vector<char> >& vSingleShell_structures,
									std::vector<std::vector<SU3xSU2::LABELS*> >& vSingleShellTensorLabels,
									std::vector<SU3xSU2::LABELS*>& vOmega);
void StructureToSingleShellTensors(const CTuple<char, 7>& structure, std::vector<unsigned char>& Shells, std::vector<std::vector<char> >& vSingleShell_structures);
#endif
