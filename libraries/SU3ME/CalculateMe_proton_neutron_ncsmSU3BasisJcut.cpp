#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <boost/unordered_map.hpp>

SU2::LABEL MECalculatorData::JJ0_ = 0; 	//	set initially to 0. It may be overwritten in COperatorLoader::Load by JJ0 and MM0 specified in input file containing SU(3) decomposition of one-body operator.
										//	In the future, even two-body operators will include this information .... TODO: implement that 
SU2::LABEL MECalculatorData::MM0_ = 0; // set initially to 0. Can be rewritten by COperatorLoader::


/** \brief Lookup table for 9j recoupling coefficients needed for Wigner-Eckhart
 *
 * 	This class stores Wigner 9j coefficients needed for the Wigner-Eckhart
 * 	theorem for operators with \f$L_{0}=S_{0}, J_{0}=0\f$, that is
 *  \f{displaymath}{
 *  \left\{
 *  \begin{array}{ccc}
 *  L_{i} & S_{i} & J \\
 *  S_{0} & S_{0} & 0 \\
 *  L_{f} & S_{f} & J 
 *  \end{array}
 *  \right\}}\f}
 *
 *  \todo Current version is valid only for two-body operator, that is for \f$S_{0}\f$=0, 1, or 2.
 */
//class CWigEck9jScalarTensorLookUpTableJcut
//{
//	public:
//	typedef float DOUBLE;
//	/** WIGNER_9J_TABLE[iS0] contains map of integer keys constructed by
//	 *  INTEGER_KEY(\f$L_{i}, S_{i}, L_{f},S_{f}\f$) and associated
//	 *  array of all 9j symbols for \f$J = J_{\min}\dots J_{\max}\f$.
//	 */
//	typedef std::vector<std::map<int, DOUBLE*> > WIGNER_9J_TABLE; 
//	public:
//	CWigEck9jScalarTensorLookUpTableJcut(): wig9jTable_(3) {}; // S0 == 0, 2, 4 ==> we need only 3 maps 
//	DOUBLE* GetWigner9j(SO3::LABEL LLi, SU2::LABEL SSi, SO3::LABEL LLf, SU2::LABEL SSf, SU2::LABEL SS0, SU2::LABEL JJbegin)
//	{
//		//	assert two-body operator
//		assert(SS0 == 0 || SS0 == 2 || SS0 == 4);
//		std::map<int, DOUBLE*>& wig9jS0 = wig9jTable_[SS0/2];
//		std::map<int, DOUBLE*>::iterator LiSiLfSfwig9j = wig9jS0.find(INTEGER_KEY(LLi, SSi, LLf, SSf));
//
//		if (LiSiLfSfwig9j == wig9jS0.end())
//		{
//			int JJmin = std::max(abs(LLi - SSi), abs(LLf - SSf));
//			int JJmax = std::min(LLi + SSi, LLf + SSf);
//			int nJ = ((JJmax - JJmin) >> 1) + 1;	// (JJmax - Jmin)/2 + 1
//			assert(nJ >= 1);
//			DOUBLE* dWigner9j_store = new DOUBLE[nJ];
//			DOUBLE* dWigner9j_return(0);
//			for (int i = 0, JJ = JJmin; JJ <= JJmax; ++i, JJ += 2)
//			{
//				dWigner9j_store[i] = wigner9j(LLi, SSi, JJ, SS0, SS0, 0, LLf, SSf, JJ);
//				if (JJ == JJbegin)
//				{
//					dWigner9j_return = &dWigner9j_store[i];
//				}
//			}
//			wig9jS0.insert(std::make_pair(INTEGER_KEY(LLi, SSi, LLf, SSf), dWigner9j_store));
//			return dWigner9j_return;
//		}
//		else
//		{
//			int JJmin = std::max(abs(LLi - SSi), abs(LLf - SSf));
//			int nJ = (JJbegin - JJmin) >> 1;	// (JJmax - Jmin)/2
//			assert(nJ >= 0);
//			return &LiSiLfSfwig9j->second[nJ];
//		}
//	}
//	~CWigEck9jScalarTensorLookUpTableJcut()
//	{
//		for (int iS = 0; iS < 3; ++iS)
//		{
//			std::map<int, DOUBLE*>& wig9jS0 = wig9jTable_[iS];
//			std::map<int, DOUBLE*>::iterator it = wig9jS0.begin();
//			for (; it != wig9jS0.end(); ++it)
//			{
//				delete []it->second;
//			}
//		}
//	}
//	private:
/** Construct integer key from the values of \f$L_{i}, S_{i}, L_{f}, S_{f}\f$. */
//	inline int INTEGER_KEY(SO3::LABEL LLket, SU2::LABEL S2ket, SO3::LABEL LLbra, SU2::LABEL S2bra) const { return (LLket << 24) | (S2ket << 16) | (LLbra << 8) | S2bra; }
//
//	WIGNER_9J_TABLE wig9jTable_;
//};

class dividedpertwo
{
public:
  inline std::size_t operator()(int const& i) const{return i/2;}
};
class dontdoanything
{
public:
  inline std::size_t operator()(int const& i) const{return i;}
};

class CWigEck9jScalarTensorLookUpTableJcut 
{
#ifdef CWigEck9jScalarTensorLookUpTable_PROFILE
  timestamp adding;
  timestamp fetching;
#endif
	public:
	typedef float DOUBLE;
	/** WIGNER_9J_TABLE[iS0] contains map of integer keys constructed by
	 *  INTEGER_KEY(\f$L_{i}, S_{i}, L_{f},S_{f}\f$) and associated
	 *  array of all 9j symbols for \f$J = J_{\min}\dots J_{\max}\f$.
	 */
  //typedef std::map<int, DOUBLE*> CACHE;
  //    typedef boost::unordered_map<int, DOUBLE*, dividedpertwo> CACHE;
    typedef boost::unordered_map<int, DOUBLE*, dontdoanything> CACHE;
	typedef std::vector<CACHE > WIGNER_9J_TABLE; 
	public:
  CWigEck9jScalarTensorLookUpTableJcut(): 
	 wig9jTable_(5) // S0 == 0, 2, 4 ==> we need only 3 maps //but let's not pay the division. Creating a map and not using it is free
   {
#ifdef CWigEck9jScalarTensorLookUpTable_PROFILE
	  nbfetch = 0;
	  iter = 0;
	  size = 0;
#endif
	  wig9jTable_[0].rehash(4096);
	  wig9jTable_[2].rehash(4096);
	  wig9jTable_[4].rehash(4096);

	}

	DOUBLE* GetWigner9j(SO3::LABEL LLi, SU2::LABEL SSi, SO3::LABEL LLf, SU2::LABEL SSf, SU2::LABEL SS0, SU2::LABEL JJbegin)
	{
		//	assert two-body operator
		assert(SS0 == 0 || SS0 == 2 || SS0 == 4);
		CACHE& wig9jS0 = wig9jTable_[SS0];
		//CACHE& wig9jS0 = wig9jTable_[SS0/2];
		int key = INTEGER_KEY(LLi, SSi, LLf, SSf);
		CACHE::iterator LiSiLfSfwig9j = wig9jS0.find(key);

		if (LiSiLfSfwig9j == wig9jS0.end())//not found add it to the cache
		{
#ifdef  CWigEck9jScalarTensorLookUpTable_PROFILE
		  if (LLi%2 == 1 || SSi %2 == 1 || LLf %2 == 1 || SSf%2 ==1)
			 std::cerr<<"not even : "<<LLi<<" "<<SSi<<" "<<LLf<<" "<<SSf<<std::endl;
		  std::cerr<<"key: "<<key<<std::endl;
		  timestamp begin;
#endif
			int JJmin = std::max(abs(LLi - SSi), abs(LLf - SSf));
			int JJmax = std::min(LLi + SSi, LLf + SSf);
			int nJ = JJmax - JJmin + 1;
			assert(nJ >= 1);
			DOUBLE* dWigner9j_store = new DOUBLE[nJ];
			DOUBLE* dWigner9j_return(0);
			for (int i = 0, JJ = JJmin; JJ <= JJmax; ++i, JJ += 2)
			{
				dWigner9j_store[i] = wigner9j(LLi, SSi, JJ, SS0, SS0, 0, LLf, SSf, JJ);
				//ERIK says: we can do this at the end. that should be faster. I do not see much of a difference however.
				//				if (JJ == JJbegin)
				//				{
				//					dWigner9j_return = &dWigner9j_store[i];
				//				}
			}
			assert (JJbegin-JJmin >= 0);
			assert ((JJbegin-JJmin & 1) == 0);
			dWigner9j_return = &dWigner9j_store[(JJbegin-JJmin)/2];

			wig9jS0.insert(std::make_pair(key, dWigner9j_store));

		
#ifdef CWigEck9jScalarTensorLookUpTable_PROFILE
			iter ++;
			size += nJ;
			distribution[nJ] ++;
#endif

			return dWigner9j_return;
		}
		else
		{
			int JJmin = std::max(abs(LLi - SSi), abs(LLf - SSf));
			int nJ = JJbegin - JJmin;
			assert(nJ >= 0);
			DOUBLE* ret = &LiSiLfSfwig9j->second[nJ];
#ifdef  CWigEck9jScalarTensorLookUpTable_PROFILE
			nbfetch++;
#endif
			return ret;
		}
	}
	~CWigEck9jScalarTensorLookUpTableJcut()
	{
#ifndef CWigEck9jScalarTensorLookUpTable_PREALLOC
	  for (int iS = 0; iS < 5; ++iS)
		 {
  			CACHE& wig9jS0 = wig9jTable_[iS];
  			CACHE::iterator it = wig9jS0.begin();
  			for (; it != wig9jS0.end(); ++it)
			  {
				 delete []it->second;
			  }
		 }
#endif

#ifdef CWigEck9jScalarTensorLookUpTable_PROFILE
		std::cerr<<"Cleaning up CWigEck9jScalarTensorLookUpTable. There is "<<iter<<" elements inside of total size "<<size*(sizeof(DOUBLE))<<" bytes"<<std::endl;
		for (distributionmap::iterator it = distribution.begin(); it != distribution.end(); it++)
		  std::cerr<<(*it).first<<'\t'<<(*it).second<<std::endl;
		std::cerr<<"number of fetching operations: "<<nbfetch<<std::endl;
#endif

	}
	private:
/** Construct integer key from the values of \f$L_{i}, S_{i}, L_{f}, S_{f}\f$. */
  inline int INTEGER_KEY(SO3::LABEL LLket, SU2::LABEL S2ket, SO3::LABEL LLbra, SU2::LABEL S2bra) const { return ((LLket << 21) | (S2ket << 14) | (LLbra << 7) | S2bra); } //all the parameter are even so shifting only 7 is safe
//  inline int INTEGER_KEY(SO3::LABEL LLket, SU2::LABEL S2ket, SO3::LABEL LLbra, SU2::LABEL S2bra) const { return ((LLket << 20) | (S2ket << 13) | (LLbra << 6) | S2bra>>1); } //not as fast

	WIGNER_9J_TABLE wig9jTable_;

#ifdef CWigEck9jScalarTensorLookUpTable_PROFILE
  int iter;
  int size;
  typedef std::map<int, int> distributionmap;
  distributionmap distribution;
  int nbfetch;
#endif
};




//	TODO: new version !!!
double Coeff_x_SU3SO3CG_x_RME(size_t k0_max, size_t a0_max,  size_t rhot_max, SU3::WIGNER* pSU3SO3CGs, SU3xSU2::RME::DOUBLE* pRME, TENSOR_STRENGTH* coeffs)
{
	int a0, k0, rhot;
	double dtmp, dResult = 0;

	SU3xSU2::RME::DOUBLE* RME;
	SU3::WIGNER* SU3SO3CG = pSU3SO3CGs;	//	SU3SO3CG[0] ---> < wi ki Li; w0 k0min L0 || wf kf Lf>_{rhotmin}
	TENSOR_STRENGTH* COEFFS = coeffs;
	
	for (k0 = 0; k0 < k0_max; ++k0, SU3SO3CG += rhot_max)
	{
		for (a0 = 0, RME = pRME; a0 < a0_max; ++a0, ++COEFFS, RME += rhot_max)
		{
			for (rhot = 0, dtmp = 0; rhot < rhot_max; ++rhot)
			{
				dtmp += SU3SO3CG[rhot]*RME[rhot];
			}
			dResult += (*COEFFS)*dtmp;
		}
	}
	return dResult;
}

/* Works only for scalar */
void CalculateME_Diagonal_UpperTriang_Scalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<MatrixRow>& resultMe)
{
	assert(firstBraStateId == firstKetStateId);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	static CWigEck9jScalarTensorLookUpTableJcut wig9jTable;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<float> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for a scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<float> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
				CWigEck9jScalarTensorLookUpTableJcut::DOUBLE* dWigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, Jmin);
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin, iJ = 0; J <= Jmax; J += 2, ++iJ)
				{
					if (Negligible(dWigner9j[iJ]))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j[iJ]*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]) && (braStateId <= ketStateId))
								{
									resultMe[irow_relative].push_back(std::make_pair(ketStateId, MeJ[index_me]));
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalculateME_Diagonal_UpperTriang_Scalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind)
{
	assert(firstBraStateId == firstKetStateId);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	static CWigEck9jScalarTensorLookUpTableJcut wig9jTable;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over Lf = Lmin ... Lmax
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		Li = Lf  ... Lmax
		for (ket.rewind(bra); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<float> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for a scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<float> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
				CWigEck9jScalarTensorLookUpTableJcut::DOUBLE* dWigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, Jmin);
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin, iJ = 0; J <= Jmax; J += 2, ++iJ)
				{
					if (Negligible(dWigner9j[iJ]))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j[iJ]*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]) && (braStateId <= ketStateId))
								{
									vals[irow_relative].push_back(MeJ[index_me]);
									col_ind[irow_relative].push_back(ketStateId);
								}
							}
						}
					}
				}
			}
		}
	}
}


/** This functions assumes that operator is a scalar */
void CalculateME_nonDiagonal_Scalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
								const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
								const std::vector<MECalculatorData>& MeCalcData, std::vector<MatrixRow>& resultMe)
{
//	assert(firstBraStateId != firstKetStateId);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	static CWigEck9jScalarTensorLookUpTableJcut wig9jTable;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}


			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<float> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<float> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;

				CWigEck9jScalarTensorLookUpTableJcut::DOUBLE* dWigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, Jmin);
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin, iJ = 0; J <= Jmax; J += 2, ++iJ)
				{
					if (Negligible(dWigner9j[iJ]))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j[iJ]*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]))
								{
									resultMe[irow_relative].push_back(std::make_pair(ketStateId, MeJ[index_me]));
								}
							}
						}
					}
				}
			}
		}
	}
}

/** This functions assumes that operator is a scalar */
void CalculateME_nonDiagonal_Scalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
								const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
								const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind) 
{
//	assert(firstBraStateId != firstKetStateId);
	float dPiLfSf;
	int L0, S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	static CWigEck9jScalarTensorLookUpTableJcut wig9jTable;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}

			int Jmin = std::max(bra.Jmin(), ket.Jmin());
			int Jmax = std::min(bra.Jmax(), ket.Jmax());
			int nJ   = (Jmax - Jmin)/2 + 1;	//	iJ == 0 ... J = Jmin; iJ = 1 ... J = Jmin + 2
			if (nJ <= 0)
			{
				continue;
			}


			nme = bra.kmax()*ket.kmax()*afmax*aimax;		// for each value of J we must calculate nme matrix elements
			std::vector<float> MeJ(nJ*nme, 0.0); 			// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
															//			  |      |          |          |      |  	     |
															//			  |      |          |          |      |  	     |
															//			  V      V          V          V      V  	     V
															//			  af     kf         J          ai     ki         J
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = L0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), L0, bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<float> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, L0/2);
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;

				CWigEck9jScalarTensorLookUpTableJcut::DOUBLE* dWigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, bra.L(), bra.S2, S0, Jmin);
//	for all J in <Jmin, Jmax> calculate	MeJ[J, kf, ki, af, ai] = 9j(Tensor, J) * TensorMe[kf, ki, af, ai]
				for (int J = Jmin, iJ = 0; J <= Jmax; J += 2, ++iJ)
				{
					if (Negligible(dWigner9j[iJ]))
					{
						index_me += nme;
						continue;
					}
					for (size_t i = 0; i < nme; ++i, ++index_me)
					{
						MeJ[index_me] += dWigner9j[iJ]*tensorMe[i];
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (int J = Jmin; J <= Jmax; J += 2)
			{
				dCoeff = std::sqrt(J + 1)*dPiLfSf;
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					size_t kfLfJId = bra.getId(kf, J);
					for (int ki = 0; ki < ket.kmax(); ++ki)
					{
						size_t kiLiJId = ket.getId(ki, J);
						for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
						{
							IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
							size_t irow_relative = braStateId - firstBraStateId;
							for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
							{
								MeJ[index_me] *= dCoeff;
								IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJId*aimax + ai);
								if (!Negligible6(MeJ[index_me]))
								{
									vals[irow_relative].push_back(MeJ[index_me]);
									col_ind[irow_relative].push_back(ketStateId);
								}
							}
						}
					}
				}
			}
		}
	}
}

/** 
 	This function calculates matrix elements for any operator J0 >= 0.
	It was tested for Hamiltonian, which has J0 = 0. It gaves the same results as 
	procedure which assumes explicitly that the operator is a scalar tensor.
	
	It is not computationally tuned whatsoever and hence it is inneficient. 
	My reason for not trying to speed it up is that big runs will be performed 
	in a basis fixed Jf and Ji values. In such a case one can implement
	much faster code.
*/
void CalculateME_nonDiagonal_nonScalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind) 
{
	float dPiLfSf;
	int S0;
	size_t k0_max, a0_max, rhot_max;
	size_t iTensor, index_su3so3cg, index_rme, index_me, nme;
	SU3::WIGNER* pSU3SO3CGs;
	SU3xSU2::RME::DOUBLE* pRME;
	static CWig9jLookUpTable<> wig9jTable;
	float dCoeff; //	dCoeff = Pi_{J Lf Sf}

//	iterate over kf Lf
	for (bra.rewind(); !bra.IsDone(); bra.nextL())
	{
		dPiLfSf = std::sqrt((bra.L() + 1) * (bra.S2 + 1));
//		iterate over ki Li		
		for (ket.rewind(); !ket.IsDone(); ket.nextL())
		{
			if (abs((bra.L() - ket.L()))/2 > 2)	// this condition is valid for two-body interaction only (i.e. L0 = 0, 1, 2)
			{
				continue;
			}
//	The folowing block is very ineficient ...
			int nJfJipairs = 0;
			for (SU2::LABEL JJf = bra.Jmin(); JJf <= bra.Jmax(); JJf += 2)
			{
				for (SU2::LABEL JJi = std::max(abs(JJf - MECalculatorData::JJ0()), ket.Jmin()); JJi <= std::min(JJf + MECalculatorData::JJ0(), ket.Jmax()); JJi += 2, ++nJfJipairs);
			}

			if (nJfJipairs <= 0)
			{
				continue;
			}

			nme = bra.kmax()*ket.kmax()*afmax*aimax;				// for each pair Jf Ji we must calculate nme matrix elements
			std::vector<float> MeJfJi(nJfJipairs*nme, 0.0);		// store me <(*) wf (*) (Lf Sf) * || O || (*) wi (*) (Li Si) *>
																	//			  |      |          |          |      |  	     |
																	//			  |      |          |          |      |  	     |
																	//			  V      V          V          V      V  	     V
																	//			  af     kf         Jf          ai     ki        Ji
//			<af wf kf Lf Sf J || O || ai wi ki Li Si J> --> resultingMe[index], where
//			index = iJ * (kf_max*ki_max*afmax*aimax) + kf * (ki_max*afmax*aimax) + ki * (afmax*aimax) + af * aimax + ai
//			where iJ == 0 ==> J == Jmin; if iJ == 1 ==> J == Jmin + 2

			//	iterate over tensors
			for (iTensor = 0; iTensor < MeCalcData.size(); ++iTensor)
			{
				S0 = MeCalcData[iTensor].rmes_->Tensor.S2;	//	valid only for scalar operator (i.e. J0 = 0)
				if (!SU2::mult(ket.L(), MeCalcData[iTensor].LL0(), bra.L()))
				{ 
					continue;
				}
			
				assert(afmax == MeCalcData[iTensor].rmes_->m_bra_max); 
				assert(aimax == MeCalcData[iTensor].rmes_->m_ket_max); 
// 				tensorMe[index] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi * Li; w0 k0 L0 || wf * Lf>_{rhot} <* wf ||| T^{a0 w0} ||| * wi>_{rhot}
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                |                    |             |                      |	
//                                                                V                    V             V                      V	
//                                                                ki                   kf            af                     ai
				std::vector<float> tensorMe(nme, 0.0);
				k0_max = SU3::kmax(MeCalcData[iTensor].rmes_->Tensor, (MeCalcData[iTensor].LL0() >> 1));
				rhot_max = MeCalcData[iTensor].rmes_->m_rhot_max;
				a0_max = MeCalcData[iTensor].rmes_->m_tensor_max;

//				find a pointer to <wi ki_min Li; w0 k0_min L0 || wf kf_min Lf>_{rhot_min} for a given Lf Li. 
				pSU3SO3CGs = MeCalcData[iTensor].SU3SO3CGs_->SU3SO3CGs(bra.L(), ket.L());
				index_su3so3cg = 0;
				index_me = 0;
//	for all kf, ki, af, ai, calculate TensorMe[kf, ki, af, ai] = sum_{k0 a0} c_{k0 a0} sum_{rhot} <wi ki(*) Li; w0 k0 L0 || wf kf(*) Lf>_{rhot} <af(*) wf ||| T^{a0 w0} ||| ai(*) wi>_{rhot}
//	index_me = kf*(kimax*afmax*aimax) + ki*(afmax*aimax) + af*aimax + ai
				for (int kf = 0; kf < bra.kmax(); ++kf)
				{
					for (int ki = 0; ki < ket.kmax(); ++ki, index_su3so3cg += k0_max*rhot_max)
					{
						index_rme = 0;
						for (int af = 0; af < afmax; ++af)
						{
							for (int ai = 0; ai < aimax; ++ai, index_rme += a0_max * rhot_max, ++index_me)
							{
								pRME = (MeCalcData[iTensor].rmes_->m_rme + index_rme);
								tensorMe[index_me] = Coeff_x_SU3SO3CG_x_RME(k0_max, a0_max, rhot_max, (pSU3SO3CGs+index_su3so3cg), pRME, MeCalcData[iTensor].coeffs_);
							}
						}
					}
				}
				assert(index_me == nme);
				
				index_me = 0;
				for (SU2::LABEL JJf = bra.Jmin(); JJf <= bra.Jmax(); JJf += 2)
				{
					for (SU2::LABEL JJi = std::max(abs(JJf - MECalculatorData::JJ0()), ket.Jmin()); JJi <= std::min(JJf + MECalculatorData::JJ0(), ket.Jmax()); JJi += 2)
					{
						float wigner9j = wig9jTable.GetWigner9j(ket.L(), ket.S2, JJi, MeCalcData[iTensor].LL0(), S0, MECalculatorData::JJ0(), bra.L(), bra.S2, JJf);
						if (Negligible(wigner9j))
						{
							index_me += nme;
							continue;
						}
						for (size_t i = 0; i < nme; ++i, ++index_me)
						{
							MeJfJi[index_me] += wigner9j*tensorMe[i];
						}
					}
				}
			}
//	WARNING: Any change in loops order implies that MeJ[index_me] will not be correct!!!
			index_me = 0;
			for (SU2::LABEL JJf = bra.Jmin(); JJf <= bra.Jmax(); JJf += 2)
			{
				for (SU2::LABEL JJi = std::max(abs(JJf - MECalculatorData::JJ0()), ket.Jmin()); JJi <= std::min(JJf + MECalculatorData::JJ0(), ket.Jmax()); JJi += 2)
				{
					dCoeff = std::sqrt((JJi + 1)*(MECalculatorData::JJ0() + 1))*dPiLfSf;
					for (int kf = 0; kf < bra.kmax(); ++kf)
					{
						size_t kfLfJfId = bra.getId(kf, JJf);
						for (int ki = 0; ki < ket.kmax(); ++ki)
						{
							size_t kiLiJiId = ket.getId(ki, JJi);
							for (int af = 0, afdim = 0; af < afmax; ++af, afdim += bra.dim())
							{
								IdenticalFermionsNCSMBasis::IdType braStateId(firstBraStateId + kfLfJfId*afmax + af);
// irow_relative equals to a relative position of |af (lm mu)f Sf kf Lf Jf> state
// with respect to the first element in the basis |af=0 (lm mu)f Sf 0 Lmin/ Jmin> == firstBraStateId;
								size_t irow_relative = braStateId - firstBraStateId;
								for (int ai = 0, aidim = 0; ai < aimax; ++ai, aidim += ket.dim(), ++index_me)
								{
									MeJfJi[index_me] *= dCoeff;
									IdenticalFermionsNCSMBasis::IdType ketStateId(firstKetStateId + kiLiJiId*aimax + ai);
									if (!Negligible6(MeJfJi[index_me]))
									{
										vals[irow_relative].push_back(MeJfJi[index_me]);
										col_ind[irow_relative].push_back(ketStateId);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
