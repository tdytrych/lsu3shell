#ifndef SCALAR_OPERATORS_2_SU3_TENSORS_H
#define SCALAR_OPERATORS_2_SU3_TENSORS_H
#include <SU3ME/bdbme.h>
#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/OneBodyOperator2SU3Tensors.h>
#include <SU3NCSMUtils/clebschGordan.h>

#include <iostream>
#include <cmath>

struct SPS2Index
{
	static inline void Get_nlj(const int index, int& n, int& l, int& j)
	{
		n = (-1 + std::sqrt(1 + 8*(index-1)))/2;
		int i = (index-1) - n*(n+1)/2;
		if (n%2)
		{
			l = 1 + 2*(i/2);
			j = (i % 2) ? (2*l + 1) : (2*l - 1);
		}
		else
		{
			l = 2*((i+1)/2);
			j = (i % 2) ? 2*l - 1: 2*l + 1;
		}
	}
	static inline int Get_Index(const int n, const int l, const int j) // ASSUMPTION: l == l ... usually l == 2*l, but this method expects l
	{
		int index = n*(n + 1)/2 + 1;
		if (n%2)
		{
			index += 2*(l/2);
		}
		else
		{
			if (l == 0) 
			{
				return index;
			} else {
				index += (1 + 2*((l/2) - 1));
			}
		}
		return ((j == (2*l - 1)) ? index : index + 1);
	}
};

/////////////////////////////////////////////////////////////////
/////	Structures calculating TWO-body matrix elements of	/////	
/////	L^2 L.S b+.b and ri.rj operators.		   			/////
/////////////////////////////////////////////////////////////////
struct Calculate_2LaLb_TwoBody_Mscheme: public SPS2Index
{
//	calculate two-body matrix elements of 2*(LaLb) using relation (11) at docs/me.pdf
	inline double operator()(	const int n1p, const int l1p, const int j1p, const int m1p, const int t1p,
								const int n2p, const int l2p, const int j2p, const int m2p, const int t2p,
								const int n1, const int l1, const int j1, const int m1, const int t1, 
								const int n2, const int l2, const int j2, const int m2, const int t2) const
	{
		double dResult(0.0);
		bool bNonvanishing = ((n1p == n1) & (n2p == n2) & (t1p == t1) & (t2p == t2) & (l1p == l1) & (l2p == l2));
		if (bNonvanishing) 
		{
		//	sum over mu = +1, -1, 0 reduces to just one term with  mu == m1p - m1
			int mu = m1p - m1;
			if (mu == (m2 - m2p) && abs(mu) <= 2)
			{
				dResult += MINUSto(-(mu/2))*clebschGordan(j1, m1, 2, +mu, j1p, m1p)*clebschGordan(j2, m2, 2, -mu, j2p, m2p);
				double phase = 2.0*MINUSto(1 + (j1 + l1 + j2 + l2)/2);
				phase *= std::sqrt((j1 + 1)*(j2 + 1)*(l1/2)*(l1/2 + 1)*(l1 + 1)*(l2/2)*(l2/2 + 1)*(l2 + 1));
				dResult *= phase*wigner6j(l1, 1, j1, j1p, 2, l1) * wigner6j(l2, 1, j2, j2p, 2, l2);
			}
		}
		return dResult;
	}
};


struct Calculate_LS_TwoBody_Mscheme: public SPS2Index
{
//	calculate two-body matrix elements of l_{1}.s_{2} + s_{1}.l_{2} using relation (33) at docs/me.pdf
	inline double operator()(	const int n1p, const int l1p, const int j1p, const int m1p, const int t1p,
						const int n2p, const int l2p, const int j2p, const int m2p, const int t2p,
						const int n1, const int l1, const int j1, const int m1, const int t1, 
						const int n2, const int l2, const int j2, const int m2, const int t2) const
	{
		double dResult(0.0);
		bool bNonvanishing = ((n1p == n1) & (n2p == n2) & (t1p == t1) & (t2p == t2) & (l1p == l1) & (l2p == l2));
		if (bNonvanishing) 
		{
			//	sum over mu = +1, -1, 0 reduces to just one term with  mu == m1p - m1
			int mu = m1p - m1;
			if (mu == (m2 - m2p) && abs(mu) <= 2)
			{
				double dPhase = MINUSto(1 + (l1 + l2)/2)*std::sqrt((j1 + 1)*(j2 + 1))*std::sqrt(1.5)*MINUSto(-(mu/2))*clebschGordan(j1, m1, 2, +mu, j1p, m1p)*clebschGordan(j2, m2, 2, -mu, j2p, m2p);
				double dTerm1 = MINUSto((j1 + j2p)/2)*std::sqrt((l1/2)*(l1/2 + 1)*(l1 + 1))*wigner6j(l1, 1, j1, j1p, 2, l1)*wigner6j(1, l2, j2, j2p, 2, 1);
				double dTerm2 = MINUSto((j2 + j1p)/2)*std::sqrt((l2/2)*(l2/2 + 1)*(l2 + 1))*wigner6j(l2, 1, j2, j2p, 2, l2)*wigner6j(1, l1, j1, j1p, 2, 1);
				dResult = dPhase*(dTerm1 + dTerm2);
			}
		}
		return dResult;
	}
};

struct Calculate_bd1b2_p_b1bd2_TwoBody_Mscheme: public SPS2Index
{
//	calculate two-body matrix elements of b+_{1}.b_{2} + b_{1}.b+_{2} using relation (23) at docs/me.pdf
	inline double operator()(	const int n1p, const int l1p, const int j1p, const int m1p, const int t1p,
								const int n2p, const int l2p, const int j2p, const int m2p, const int t2p,
								const int n1, const int l1, const int j1, const int m1, const int t1, 
								const int n2, const int l2, const int j2, const int m2, const int t2) const
	{
		double dResult(0.0);
		bool bNonvanishing = (t1p == t1) & (t2p == t2);
		if (bNonvanishing) 
		{
			//	sum over mu = +1, -1, 0 reduces to just one term with  mu == m1p - m1
			int mu = m1p - m1;
			if (mu == (m2 - m2p) && abs(mu) <= 2)
			{
				double dPhase = (MINUSto(-(mu/2))*clebschGordan(j1, m1, 2, +mu, j1p, m1p)*clebschGordan(j2, m2, 2, -mu, j2p, m2p))/std::sqrt((j1p+1)*(j2p+1));
				dResult = dPhase*(bd(n1p, l1p, j1p, n1, l1, j1)* b(n2p, l2p, j2p, n2, l2, j2) + b(n1p, l1p, j1p, n1, l1, j1)*bd(n2p, l2p, j2p, n2, l2, j2));
			}
		}
		return dResult;
	}
};

struct Calculate_rirj_TwoBody_Mscheme: public SPS2Index
{
//	calculate two-body matrix elements of r_{1}.r_{2} 
	inline double operator()(	const int n1p, const int ll1p, const int jj1p, const int mm1p, const int t1p,
								const int n2p, const int ll2p, const int jj2p, const int mm2p, const int t2p,
								const int n1, const int ll1, const int jj1, const int mm1, const int t1, 
								const int n2, const int ll2, const int jj2, const int mm2, const int t2) const
	{
		if ((t1p == t1) && (t2p == t2))
		{
			double dA(0), dB(0), dC(0), dD(0);

			if (n1p == (n1 + 1)) 
			{
				dA = bd(n1p, ll1p, jj1p, n1, ll1, jj1);
			}
			if (n1p == (n1 - 1))
			{
				dB =  b(n1p, ll1p, jj1p, n1, ll1, jj1);
			}
			if (n2p == (n2 + 1))
			{
				dC = bd(n2p, ll2p, jj2p, n2, ll2, jj2);
			}
			if (n2p == (n2 - 1))
			{
				dD = b(n2p, ll2p, jj2p, n2, ll2, jj2);
			}

			double drme = (dA + dB)*(dC + dD);

			if (!Negligible8(drme))
			{
				double dCoeff = 1.0/(2.0*std::sqrt((jj1p + 1)*(jj2p + 1)));
				double dResult(0.0);

				dResult -= drme*clebschGordan(jj1, mm1, 2,  2, jj1p, mm1p)*clebschGordan(jj2, mm2, 2, -2, jj2p, mm2p);
				dResult -= drme*clebschGordan(jj1, mm1, 2, -2, jj1p, mm1p)*clebschGordan(jj2, mm2, 2,  2, jj2p, mm2p);
				dResult += drme*clebschGordan(jj1, mm1, 2,  0, jj1p, mm1p)*clebschGordan(jj2, mm2, 2,  0, jj2p, mm2p);

				return dCoeff*dResult;
			}
		}
		return 0.0;
	}
};


struct Calculate_AB00_TwoBody_Mscheme: public SPS2Index
{
//	calculate two-body matrix elements of [A^(2 0) x B^(0 2)]^(0 0)_{00} operator
	double operator()(	const int n1p, const int ll1p, const int jj1p, const int mm1p, const int t1p,
						const int n2p, const int ll2p, const int jj2p, const int mm2p, const int t2p,
						const int n1, const int ll1, const int jj1, const int mm1, const int t1, 
						const int n2, const int ll2, const int jj2, const int mm2, const int t2) const;
};



/////////////////////////////////////////////////////////////////
/////	Structures calculating one-body matrix elements of	/////	
/////	L^2 L.S and b+.b operators.							/////
/////////////////////////////////////////////////////////////////
struct CalculateL2Mscheme
{
	typedef double MatrixElementValue;
//	calculate one-body matrix elements of L^2 according to relation (12)	
	inline bool operator()(const int np, const int lp, const int jp, const int mp, const int n, const int l, const int j, const int m, MatrixElementValue& matrix_element) const
	{
		if (np != n || lp != l || jp != j || mp != m)
		{
			return false;
		}	
		matrix_element = (l/2)*((l/2)+1);
		return true;
	}
};

struct CalculateLSMscheme
{
	typedef double MatrixElementValue;
//	calculate one-body matrix elements of L.S according to relation (34)	
	inline bool operator()(const int np, const int lp, const int jp, const int mp, const int n, const int l, const int j, const int m, MatrixElementValue& matrix_element) const
	{
		if (np != n || lp != l || jp != j || mp != m)
		{
			return false;
		}	
//		matrix_element = -(3.0/SQRT(2))*SQRT(jb+1)*SQRT(lb+1)*SQRT((lb/2)*((lb/2) + 1))*wigner9j(lb, 2, lb, 1, 2, 1, jb, 0, jb);
		matrix_element = 0.5*(((double)jp/2.0)*((double)jp/2.0 + 1.0) - ((double)lp/2.0)*((double)lp/2.0 + 1.0) - 3.0/4.0);
		return true;
	}
};

struct CalculatebdbMscheme
{
	typedef double MatrixElementValue;
//	calculate one-body matrix elements of b+.b according to relation (24)
	bool operator()(const int na, const int la, const int ja, const int ma, const int nb, const int lb, const int jb, const int mb, MatrixElementValue& matrix_element) const;
};

/////////////////////////////////////////////////////////////////////////////////////////
/////	Structures facilitationg decomposition of one-body and two-body operators	/////
/////////////////////////////////////////////////////////////////////////////////////////
template<typename TwoBodyMscheme>
struct MatrixElementsTwoBody
{
	TwoBodyMscheme CalculateTwoBodyMscheme_ME;

	inline size_t GetNumberOfOperators() const {return 1;}
	void Get_nlj(const int index, int& n, int& l, int& j) const // Warning: procedure returns angular momentum l, and not 2*l, while for total momentum j = 2*j
	{
		TwoBodyMscheme::Get_nlj(index, n, l, j);
	}
	inline int Get_Index(const int n, const int l, const int j) const	// ASSUMPTION: l == l ... usually l == 2*l, but this method expects l
	{
		return TwoBodyMscheme::Get_Index(n, l, j);
	}

	typedef double DOUBLE;
	typedef CTuple<DOUBLE, 1> Value;

	inline bool MeJ(const j_coupled_bra_ket::TwoBodyLabels& I1I2I3I4J, DOUBLE& mePP, DOUBLE& meNN, DOUBLE& mePN) const
	{
		int a(I1I2I3I4J[jt_coupled_bra_ket::I1]);
		int b(I1I2I3I4J[jt_coupled_bra_ket::I2]);
		int c(I1I2I3I4J[jt_coupled_bra_ket::I3]);
		int d(I1I2I3I4J[jt_coupled_bra_ket::I4]);
		int J(2*I1I2I3I4J[jt_coupled_bra_ket::J]);

		mePN = CalculateJcoupledNAS_ME(a, 0, b, 1, c, 0, d, 1, J)/*/4.0*/;	//	L^2 operator does not work when mePN/4.0 ==> removed
		mePP = meNN = CalculateJcoupledNAS_ME(a, 0, b, 0, c, 0, d, 0, J)/4.0;
		return (!Negligible8(mePN) || !Negligible8(mePP));
	}

	double CalculateJcoupledNAS_ME(const int a, const int ta, const int b, const int tb, const int c, const int tc, const int d, const int td, const int J) const
	{
		int na, la, ja;
		int nb, lb, jb;
		int nc, lc, jc;
		int nd, ld, jd; 

		Get_nlj(a, na, la, ja);	//	one must be careful here ... Get_nlj returns the value of l, but Calculate2LaLb_mscheme requires l == 2*l
		la *= 2;	//	==> need to multiply by two
		Get_nlj(b, nb, lb, jb);
		lb *= 2;
		Get_nlj(c, nc, lc, jc);
		lc *= 2;
		Get_nlj(d, nd, ld, jd);
		ld *= 2;

		return 	CalculateJcoupledNAS_ME(na, la, ja, ta, nb, lb, jb, tb, nc, lc, jc, tc, nd, ld, jd, td, J);	//	MJ == J
	}

//	This method calculates two-body J-coupled NAS matrix elements 
//	<na la ja ta; nb lb jb tb; J || O ||nc lc jc tc; nd ld jd td; J>_{NA} = \sum ..... <na la ja ma ta; nb lb jb mb tb|| O ||nc lc jc mc tc; nd ld jd md td>_{NAS}
//	by implementing relation (1) from docs/me.pdf.
	double CalculateJcoupledNAS_ME(	const int na, const int la, const int ja, const int ta,
									const int nb, const int lb, const int jb, const int tb,
									const int nc, const int lc, const int jc, const int tc, 
									const int nd, const int ld, const int jd, const int td, const int J) const
	{
		int MJ(J);
		double dResult(0.0);	
		for (int ma = -ja; ma <= ja; ma += 2)
		{
			int mb = MJ - ma;	//	==> ma + mb == MJ
			if (abs(mb) > jb) {	//	==> mb is non-physical
				continue;
			}
			for (int mc = -jc; mc <= jc; mc += 2)
			{
				int md = MJ - mc;	//	==> mc + md == MJ
				if (abs(md) > jd) {	//	==> md is non-physical
					continue;
				}
				double dCG1 = clebschGordan(ja, ma, jb, mb, J, MJ); 
				double dCG2 = clebschGordan(jc, mc, jd, md, J, MJ); 
				if (!Negligible8(dCG1) && !Negligible8(dCG2))
				{
					dResult += dCG1*dCG2*CalculateTwoBodyMschemeNAS_ME(na, la, ja, ma, ta, nb, lb, jb, mb, tb, nc, lc, jc, mc, tc, nd, ld, jd, md, td);
				}
			}
		}
		double Nab = 1.0/(std::sqrt(1 + ( (na == nb) & (la == lb) & (ja == jb) & (ta == tb))));
		double Ncd = 1.0/(std::sqrt(1 + ( (nc == nd) & (lc == ld) & (jc == jd) & (tc == td))));
		return Nab*Ncd*dResult;
//		factor SQRT(2*J + 1) is ommited since we define
//		< J MJ| O | J MJ'> = C * < J|| O || J>
//		double PiJ = SQRT(J + 1);
//		return PiJ*Nab*Ncd*dResult;
	}
	
//	This method calculates two-body NAS matrix elements in m-scheme basis <na
//	la ja ma ta; nb lb jb mb tb|| O ||nc lc jc mc tc; nd ld jd md td>_{NAS} by
//	implementing relation (3) from docs/me.pdf.
	double CalculateTwoBodyMschemeNAS_ME(	const int n1p, const int l1p, const int j1p, const int m1p, const int t1p,
											const int n2p, const int l2p, const int j2p, const int m2p, const int t2p,
											const int n1, const int l1, const int j1, const int m1, const int t1, 
											const int n2, const int l2, const int j2, const int m2, const int t2) const
	{
		return 
		CalculateTwoBodyMscheme_ME(n1p, l1p, j1p, m1p, t1p, n2p, l2p, j2p, m2p, t2p, n1, l1, j1, m1, t1, n2, l2, j2, m2, t2) 
		- 
		CalculateTwoBodyMscheme_ME(n1p, l1p, j1p, m1p, t1p, n2p, l2p, j2p, m2p, t2p, n2, l2, j2, m2, t2, n1, l1, j1, m1, t1);
	}
};



template<typename OneBodyMscheme>
struct MatrixElementOneBody
{
	OneBodyMscheme CalculateOneBodyMschemeMe;
	typedef typename OneBodyMscheme::MatrixElementValue MatrixElementValue;
	public:
	inline size_t GetNumberOfOperators() const {return 1;}

	//	type == PP or NN ... since <p || b+b || p > == <n || b+b || n> ==> 
	inline bool Me(const int na, const int la, const int ja, const int ma, const int nb, const int lb, const int jb, const int mb, const int type, MatrixElementValue& matrix_element) const
	{
		assert(type == PP || type == NN); 
		return CalculateOneBodyMschemeMe(na, la, ja, ma, nb, lb, jb, mb, matrix_element);
	}
};

struct Calculate_r2_Mscheme
{
	typedef double MatrixElementValue;
	bool operator()(const int np, const int lp, const int jp, const int mp, const int n, const int l, const int j, const int m, MatrixElementValue& matrix_element) const;
};

struct Calculate_AB00_Mscheme
{
	typedef double MatrixElementValue;
	bool operator()(const int np, const int lp, const int jp, const int mp, const int n, const int l, const int j, const int m, MatrixElementValue& matrix_element) const;
};


//	Interface functions used for decomposition of TWO-BODY operators
void Generate_2LaLb_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName);
void Generate_l1s2_p_s1l2_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName);
void Generate_bd1b2_p_b1bd2_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName);
void Generate_rirj_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName);
void Generate_AB00_TwoBodyMe(int Nmax, int valence_shell, const std::string& fileName);

//	Interface functions used for decomposition of ONE-BODY operators
void bdbOneBodyOperator2SU3(int maxShell, const std::string& fileName);
void LSOneBodyOperator2SU3(int maxShell, const std::string& fileName);
void L2OneBodyOperator2SU3(int maxShell, const std::string& fileName);
void r2OneBodyOperator2SU3(int maxShell, const std::string& fileName);
void AB00OneBodyOperator2SU3(int maxShell, const std::string& fileName);
#endif
