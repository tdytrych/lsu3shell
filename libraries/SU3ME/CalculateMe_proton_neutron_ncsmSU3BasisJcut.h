#ifndef CALCULATE_ME_PROTON_NEUTRON_H
#define CALCULATE_ME_PROTON_NEUTRON_H

#include <SU3ME/global_definitions.h>
#include <LookUpContainers/WigEckSU3SO3CGTable.h>
#include <SU3ME/ncsmSU3Basis.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/RME.h>

typedef std::vector<std::pair<IdenticalFermionsNCSMBasis::IdType, float> > MatrixRow;

class COperatorLoader;

class MECalculatorData
{
	friend class COperatorLoader;
	friend class CRunParameters;
	public:
	static SU2::LABEL JJ0_; // set initially to 0. Can be rewritten by COperatorLoader::Load
	static SU2::LABEL MM0_; // set initially to 0. Can be rewritten by COperatorLoader::Load
	SO3::LABEL LL0_;	//	set in functions of MeEvaluationHelpers.h
	SU3xSU2::RME* rmes_;
	WigEckSU3SO3CG* SU3SO3CGs_;
	TENSOR_STRENGTH* coeffs_;
	MECalculatorData(SU3xSU2::RME* rmes, WigEckSU3SO3CG* SU3SO3CGs, TENSOR_STRENGTH* coeffs, const SO3::LABEL LL0 = 0): LL0_(LL0), rmes_(rmes), SU3SO3CGs_(SU3SO3CGs), coeffs_(coeffs) {}
	MECalculatorData(const MECalculatorData& MeCalcData): LL0_(MeCalcData.LL0()), rmes_(MeCalcData.rmes_), SU3SO3CGs_(MeCalcData.SU3SO3CGs_), coeffs_(MeCalcData.coeffs_) {}
	MECalculatorData():LL0_(0), rmes_(NULL), SU3SO3CGs_(NULL), coeffs_(NULL) {}
	static inline SU2::LABEL JJ0() {return JJ0_;}
	static inline SU2::LABEL MM0() {return MM0_;}
	inline SO3::LABEL LL0() const {return LL0_;}
};
double Coeff_x_SU3SO3CG_x_RME(size_t k0_max, size_t a0_max,  size_t rhot_max, SU3::WIGNER* pSU3SO3CGs, SU3xSU2::RME::DOUBLE* pRME, TENSOR_STRENGTH* coeffs);

/* calculate non diagonal matrix elements for a scalar operator */
void CalculateME_nonDiagonal_Scalar(const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
								const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
								const std::vector<MECalculatorData>& MeCalcData, std::vector<MatrixRow>& Me);
/* same ... differs by the CSR matrix storage structures */
void CalculateME_nonDiagonal_Scalar(const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
									const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
									const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);


void CalculateME_Diagonal_UpperTriang_Scalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<MatrixRow>& resultMe);
void CalculateME_Diagonal_UpperTriang_Scalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId,
												const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId,
												const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind);


/** 
 	This function calculates matrix elements for any J0 >= 0 tensor operator.
	It was tested for Hamiltonian, which has J0 = 0. It gaves the same results as 
	procedure which assumes explicitly that the operator is a scalar tensor.
	
	It is not computationally tuned whatsoever and hence it is inneficient.  My
	reason for not trying to speed it up is that big runs will be performed in
	a basis fixed Jf and Ji values, i.e. SU3xSU2::BasisJJfixed using functions
	in CalculateME_proton_neutron_ncsmSU3BasisFixedJ.cpp/h */
void CalculateME_nonDiagonal_nonScalar(	const size_t afmax, SU3xSU2::BasisJcut& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
										const size_t aimax, SU3xSU2::BasisJcut& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
										const std::vector<MECalculatorData>& MeCalcData, std::vector<std::vector<float> >& vals, std::vector<std::vector<size_t> >& col_ind); 
#endif
