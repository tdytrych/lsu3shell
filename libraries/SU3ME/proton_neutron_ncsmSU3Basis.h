#ifndef PROTON_NEUTRON_NCSMSU3BASIS_H
#define PROTON_NEUTRON_NCSMSU3BASIS_H
#include <SU3ME/global_definitions.h>
#include <SU3ME/SU3xSU2Basis.h>
#include <SU3ME/ModelSpaceExclusionRules.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <SU3ME/distr2gamma2omega.h>
#include <numeric>

#include <boost/container/small_vector.hpp>

using std::cout;
using std::endl;

typedef std::vector<unsigned char> Distributions;
typedef boost::container::small_vector<unsigned char, storage_limits::initial_small_vector_size> DistributionsSmallVector;

typedef std::vector<unsigned char> SingleDistribution;
typedef boost::container::small_vector<unsigned char, storage_limits::initial_small_vector_size> SingleDistributionSmallVector;
typedef boost::container::small_vector_base<unsigned char> SingleDistributionSmallVectorBase;

typedef std::vector<unsigned char>::const_iterator DistributionsIter;
typedef boost::container::small_vector<unsigned char, storage_limits::initial_small_vector_size>::const_iterator DistributionsSmallVectorIter;

typedef std::vector<unsigned char>::const_iterator SingleDistributionIter;

struct GammasWithinDistribution
{
	GammasWithinDistribution():firstGamma(0), nGammas(0) {}
	unsigned int firstGamma;	//	index with the first gamma ... gammas_?_[GammasWithinDistribution::firstGamma]
	unsigned short nGammas;		//	number of gamma vectors within a given distribution
};

struct OmegasWithinGamma
{
	OmegasWithinGamma():firstOmega_su3(0), firstOmega_spin(0), nomegas_su3(0), nomegas_spin(0) {}
	OmegasWithinGamma(size_t isu3, size_t nsu3, size_t isu2, size_t nsu2):firstOmega_su3(isu3), firstOmega_spin(isu3), nomegas_su3(nsu3), nomegas_spin(nsu2) {}
	size_t firstOmega_su3; 	
	size_t nomegas_su3;		// size_t may be overkill ... I suspect that unsigned short would be sufficient ==> TODO: verify
	size_t firstOmega_spin;
	size_t nomegas_spin;
};


namespace nucleon
{
	enum {kProton = 0, kNeutron = 1};
};

namespace proton_neutron
{
	class ModelSpace
	{
		private:
		friend class boost::serialization::access;
		template<class Archive>
	    void serialize(Archive & ar, const unsigned int version)
    	{
        	ar & Z_;
			ar & N_;
			ar & JJcut_;
	        ar & modelSpace_;
    	}
		public:
		typedef std::vector<CNhwSubspace>::const_iterator const_iterator;
		private:
		unsigned char Z_, N_;
		unsigned JJcut_;	// Jcut_ :  maximal 2J, or the only 2J allowed
		std::vector<CNhwSubspace> modelSpace_;
		public:
// Create model space that will be used to generate all Nhw SU(3) states with all 
// possible values of J. It is used by programs/tools/Sp3RBasisLister.cpp
		ModelSpace(int nprotons, int nneutrons, int nhw)
		{
			Z_ = nprotons;
			N_ = nneutrons;
			JJcut_ = 0xFF; // include all states
			modelSpace_.push_back(CNhwSubspace(nhw, CNhwSubspace::Model_Space_Definition()));
		}
//	Create model space needed for generating Sp(3, R) basis states
		ModelSpace(int nprotons, int nneutrons, int nhw, int ssp, int ssn, int ss, int lm, int mu, int JJ = -1)
		{
			SU3::LABELS ir(lm, mu);
			Z_ = nprotons;
			N_ = nneutrons;
			if (JJ == -1)
			{
//	Set JJcut_ to a maximal 2J value for (lm mu)S irrep. This will guarantee
//	that it will be unique for each |{...Sp x ...Sn }(lm mu)S> irrep.  Note
//	Sp(3,R) state is independent of \kappa L and J values.
				JJcut_ = 2*(lm + mu) + ss;
			}
			else
			{
				JJcut_ = JJ;
			}

			//	here I assume that Lmax at irrep (lm mu) basis has Lmax = lm +
			//	mu I bet one can prove that rigorously
			assert(SU3::kmax(ir, lm+mu) > 0);
			// JJcut_: stretched coupling L x S ==> dim[S(lm mu) k L J] = 1
	 		SpSn sspssn(ssp, ssn);
			Allowed_Spins_and_SU3s allowed_spins_su3s(SU2_VEC(1, ss), std::vector<SU3_VEC>(1, SU3_VEC(1, ir)));
			CNhwSubspace::Model_Space_Definition model_space_definition(1, std::make_pair(sspssn, allowed_spins_su3s));

			modelSpace_.push_back(CNhwSubspace(nhw, model_space_definition));
		}
/** Try to read file with the model space definition. If it could not be open,
 * then store its name and create such file in ~ModelSpace().
 */ 
		ModelSpace(const std::string& model_space_definition_file_name)
		{
			Load(model_space_definition_file_name);
		}

		ModelSpace() {};
		ModelSpace& operator=(const ModelSpace& rhs)
		{
			Z_ = rhs.Z_;
			N_ = rhs.N_;
			JJcut_ = rhs.JJcut_;
			modelSpace_ = rhs.modelSpace_;
			return *this;
		}
/** In case input file does not exist, create it from the definition of the model space */		
		void Save(const std::string& model_space_definition_file_name);
		void Load(const std::string& model_space_definition_file_name, bool without_output = false);
		inline void push_back(const CNhwSubspace& nhw_subspace) {modelSpace_.push_back(nhw_subspace);}
		inline const CNhwSubspace& back() const {return modelSpace_.back();}
		inline const_iterator end() const {return modelSpace_.end();}
		inline const_iterator begin() const {return modelSpace_.begin();}
		inline unsigned char JJcut() const {return JJcut_;}
		inline unsigned char JJ() const {return JJcut_;}
		inline unsigned char number_of_protons() const {return Z_;}
		inline unsigned char number_of_neutrons() const {return N_;}
		inline unsigned char Z() const {return Z_;}
		inline unsigned char N() const {return N_;}
		inline size_t size() const {return modelSpace_.size();}
	};
}

//	forward declarations
class CncsmSU3Basis;

class IdentFermConfIterator 
{
	//	CncsmSU3Basis and PNConfIterator have a full access to methods and
	//	members of IdentFermConfIterator
	public:
	void Set(const IdentFermConfIterator& Iter);
	const DistributionsSmallVector& distributions_; 
	const UN::SU3xSU2_VEC& gammas_;
	const std::vector<GammasWithinDistribution>& gammasStructure_; 
	const SU3_VEC& omegasSU3_;
	const SU2_VEC& omegasSpin_; 
	const std::vector<OmegasWithinGamma>& omegasStructure_;
	unsigned char sizeDistr_;
//	iterators over data structures that define basis
	DistributionsSmallVectorIter currentDistr_;
	UN::SU3xSU2_VEC::const_iterator currentGamma_;
	SU3_VEC::const_iterator currentOmegaSU3_;
	SU2_VEC::const_iterator currentOmegaSpin_;
	std::vector<GammasWithinDistribution>::const_iterator gammasStructureIter_;
	std::vector<OmegasWithinGamma>::const_iterator omegasStructureIter_;
//	temporary vector that is changing during iteration ...		
	SU3xSU2_VEC omegas_;
//	iterator over temporary vector omegas_
	SU3xSU2_VEC::const_iterator currentOmega_;
//	variables needed for the iteration over distributions and gammas
//	igamma_ ... counts how many gammas in a given distribuiton we iterated
//				through ==> it ranges from 0 up to nOccupiedShells_ - 1
	size_t igamma_, ngammas_iterated_, nOccupiedShells_;
	public:
	IdentFermConfIterator(	const DistributionsSmallVector& distributions, 
							const unsigned char& sizeDistr,
							const UN::SU3xSU2_VEC& gammas, const std::vector<GammasWithinDistribution>& gammasStructure, 
							const SU3_VEC& omegasSU3, const SU2_VEC& omegasSpin, const std::vector<OmegasWithinGamma>& omegasStructure):
								distributions_(distributions), 
								gammas_(gammas), 
								gammasStructure_(gammasStructure), 
								omegasSU3_(omegasSU3), 
								omegasSpin_(omegasSpin), 
								omegasStructure_(omegasStructure), 
								sizeDistr_(sizeDistr)
	{
		rewindDistr();
	}

	IdentFermConfIterator(const IdentFermConfIterator& Iter): 
								distributions_(Iter.distributions_), 
								gammas_(Iter.gammas_), 
								gammasStructure_(Iter.gammasStructure_), 
								omegasSU3_(Iter.omegasSU3_), 
								omegasSpin_(Iter.omegasSpin_), 
								omegasStructure_(Iter.omegasStructure_), 
								sizeDistr_(Iter.sizeDistr_),
								currentDistr_(Iter.currentDistr_),
								currentGamma_(Iter.currentGamma_),
								currentOmegaSU3_(Iter.currentOmegaSU3_),
								currentOmegaSpin_(Iter.currentOmegaSpin_),
								gammasStructureIter_(Iter.gammasStructureIter_),
								omegasStructureIter_(Iter.omegasStructureIter_),
								igamma_(Iter.igamma_), 
								ngammas_iterated_(Iter.ngammas_iterated_), 
								nOccupiedShells_(Iter.nOccupiedShells_),
							//	temporary vector that is changing during iteration ...		
								omegas_(Iter.omegas_)
								{
									currentOmega_ = omegas_.begin() + (Iter.currentOmega_ - Iter.omegas_.begin()); 
								}
	void rewindDistr();
	inline bool hasDistr() const {return currentDistr_ < distributions_.end();}
	void nextDistr(); 

	void rewindGamma();
	inline bool hasGamma() const { return igamma_ < gammasStructureIter_->nGammas;}
	void nextGamma();

	inline bool hasOmega() const {return currentOmega_ < omegas_.end();}
	inline void nextOmega() {currentOmega_ += nOccupiedShells_ - (nOccupiedShells_ != 1);}
	void rewindOmega();

	inline void getOmega(SU3xSU2_SMALL_VEC_BASE& omega) const {assert(omega.empty()); omega.insert(omega.begin(), currentOmega_, currentOmega_ + nOccupiedShells_ - 1);}
	inline void getGamma(UN::SU3xSU2_VEC& gamma) const {assert(gamma.empty()); gamma.insert(gamma.begin(), currentGamma_, currentGamma_ + nOccupiedShells_);}
	inline void getDistr(tSHELLSCONFIG& distr) const {assert(distr.empty()); distr.insert(distr.begin(), currentDistr_, currentDistr_ + sizeDistr_);}
	inline void getDistr(SingleDistributionSmallVectorBase& distr) const {assert(distr.empty()); distr.insert(distr.begin(), currentDistr_, currentDistr_ + sizeDistr_);}

	inline SU3xSU2::LABELS getCurrentSU3xSU2() const;
	int getMult() const;
	unsigned char nOccupiedShells() const {
		assert(currentDistr_ + sizeDistr_ <= distributions_.end());
		return std::count_if(currentDistr_, currentDistr_ + sizeDistr_,  std::bind2nd(std::greater<unsigned char>(), (unsigned char)0));
	}
	void ShowModelSpace();
	unsigned char HOquanta() const;
	public:
	void ShowConf() const;
	inline bool operator== (const IdentFermConfIterator& iter) const
	{
		if (omegas_.size() != iter.omegas_.size())
		{
			return false;
		}
		return (omegasStructureIter_ - omegasStructure_.begin()) == (iter.omegasStructureIter_ - iter.omegasStructure_.begin()) 
				&& 
				(currentOmega_ - omegas_.begin()) == (iter.currentOmega_ - iter.omegas_.begin());
	}
};


class PNConfIterator
{
	friend class CncsmSU3Basis;
	private:
	unsigned char status_;

	const CncsmSU3Basis* ncsmSU3PNBasisContainer_;
	proton_neutron::ModelSpace::const_iterator nhwSubspace_;
	IdentFermConfIterator protonConfIter_;
	IdentFermConfIterator neutronConfIter_;
	unsigned int idiag_, ndiag_;
	size_t num_PNomegas_iterated_;
	SU3xSU2_VEC pnOmegas_;
	SU3xSU2_VEC::const_iterator	pnOmegas_end_;
	SU3xSU2_VEC::const_iterator currentPNOmega_;

//	in order to speed up wp x wn 0 --> wpn_su3 & wpn_spin operation I reserve
//	enough of memmory 
	SU3_VEC wpn_su3_temporary_;
	SU2_VEC wpn_spin_temporary_;
	private:
	PNConfIterator(const CncsmSU3Basis* ncsmSU3PNBasisContainer, unsigned int idiag, unsigned int ndiag);

	inline void rewindDistr_p() {protonConfIter_.rewindDistr();}
	inline bool hasDistr_p() {return protonConfIter_.hasDistr();}
	inline void nextDistr_p() {protonConfIter_.nextDistr();}
	void rewindDistr_n();
	bool hasDistr_n(); 
	void nextDistr_n(); 

	inline void rewindGamma_p() {protonConfIter_.rewindGamma();}
	inline bool hasGamma_p() {return protonConfIter_.hasGamma();}
	inline void nextGamma_p() {protonConfIter_.nextGamma();}
	inline void rewindGamma_n() {neutronConfIter_.rewindGamma();}
	inline bool hasGamma_n() {return neutronConfIter_.hasGamma();}
	inline void nextGamma_n() {neutronConfIter_.nextGamma();}

	inline void rewindOmega_p() {protonConfIter_.rewindOmega();}
	inline bool hasOmega_p() {return protonConfIter_.hasOmega();}
	inline void nextOmega_p() {protonConfIter_.nextOmega();}
	inline void rewindOmega_n() {neutronConfIter_.rewindOmega();}
	inline bool hasOmega_n() {return neutronConfIter_.hasOmega();}
	inline void nextOmega_n();

	void rewindPNOmega();
	inline void nextPNOmega() {++currentPNOmega_;}
	public:
	enum NextPNConfStatus {kNewPNOmega = 0, kNewOmega_n = 1, kNewOmega_p = 2, kNewGamma_n = 3, kNewGamma_p = 4, kNewDistr_n = 5, kNewDistr_p = 6};
	PNConfIterator(const PNConfIterator& Iter);
	inline unsigned char status() {return status_;}

	const IdentFermConfIterator& protonConf() { return protonConfIter_;}
	const IdentFermConfIterator& neutronConf() { return neutronConfIter_;}

	void rewind();
	void rewind(const PNConfIterator& Iter);
//	inline bool hasPNOmega() const {return currentPNOmega_ < pnOmegas_.end();}
	inline bool hasPNOmega() const {return currentPNOmega_ < pnOmegas_end_;}
//	size_t dimPNOmega() const; 

//	g++ compiler doesn't get over this ... strange ...
//	inline size_t getMult() const { return protonConfIter_.getMult()*neutronConfIter_.getMult();}
	size_t getMult() const;
	size_t getMult_p() const;
	size_t getMult_n() const;

	inline void getDistr_p(tSHELLSCONFIG& distr) const {protonConfIter_.getDistr(distr);}
	inline void getDistr_n(tSHELLSCONFIG& distr) const {neutronConfIter_.getDistr(distr);}
	inline void getDistr_p(SingleDistributionSmallVectorBase& distr) const {protonConfIter_.getDistr(distr);}
	inline void getDistr_n(SingleDistributionSmallVectorBase& distr) const {neutronConfIter_.getDistr(distr);}

	inline void getGamma_p(UN::SU3xSU2_VEC& gamma_p) const {protonConfIter_.getGamma(gamma_p);}
	inline void getGamma_n(UN::SU3xSU2_VEC& gamma_n) const {neutronConfIter_.getGamma(gamma_n);}

	inline void getOmega_p(SU3xSU2_SMALL_VEC_BASE& omega_p) const {protonConfIter_.getOmega(omega_p);}
	inline void getOmega_n(SU3xSU2_SMALL_VEC_BASE& omega_n) const {neutronConfIter_.getOmega(omega_n);}

	inline SU3xSU2::LABELS getCurrentSU3xSU2() const {return *currentPNOmega_;}
	SU3xSU2::LABELS getProtonSU3xSU2() const;
	SU3xSU2::LABELS getNeutronSU3xSU2() const;
	unsigned char nhw_p() const;
	unsigned char nhw_n() const;
	unsigned char nhw() const;
	void nextPNConf();
	bool hasPNConf() {return hasDistr_p();}
	unsigned int num_PNomegas_iterated() {return num_PNomegas_iterated_;}
};

class CncsmSU3Basis: public CBaseSU3Irreps
{
	friend class IdentFermConfIterator;
	friend class PNConfIterator;
	public:
	CncsmSU3Basis(	const unsigned char Z, const unsigned char N, const proton_neutron::ModelSpace& nucleusModelSpace);
/** Construct ncsm SU(3) basis based on the definition of the model space 
 * \param[in] nucleusModelSpace model space definition
 */
	CncsmSU3Basis(const proton_neutron::ModelSpace& nucleusModelSpace);
//	This is a design issue: CncsmSU3Basis should be independent on
//	whether one uses Jcut or Jfixed basis, CncsmSU3Basis thus does 
//	not provide dim() method.
//	inline unsigned long dim() const {return dim_;}

	IdentFermConfIterator firstProtonConf() const
	{
		return IdentFermConfIterator(distributions_p_, sizeDistr_p_, gammas_p_, gammasStructure_p_, omegasSU3_p_, omegasSpin_p_, omegasStructure_p_); 
	}
	IdentFermConfIterator firstNeutronConf() const
	{
		return IdentFermConfIterator(distributions_n_, sizeDistr_n_, gammas_n_, gammasStructure_n_, omegasSU3_n_, omegasSpin_n_, omegasStructure_n_); 
	}

	size_t rho_times_SU3xSU2dim(const SU3xSU2::LABELS ir) const;

	PNConfIterator firstPNConf(unsigned int idiag, unsigned int ndiag) const
	{
		assert(idiag < ndiag);
		return PNConfIterator(this, idiag, ndiag);
	}

	inline unsigned char Nmax() const {return nucleusModelSpace_.back().N();}
	void ShowMemoryRequirements();
	void ShowProtonSU3xSU2Irreps();
	void ShowNeutronSU3xSU2Irreps();
	inline unsigned char Z() const {return Z_;}
	inline unsigned char N() const {return N_;}
	SU3xSU2_VEC GenerateFinal_SU3xSU2Irreps();
	private:

	void GenerateDistributionSpace(	const tSHELLSCONFIG& distribution, const nucleon::Type type,  GammasWithinDistribution& gammasInfo);
	unsigned char Z_, N_;
//	dims_.size() == number of diagonal processes		
//	dims_[idiag] == number of basis states that a idiag process holds
	proton_neutron::ModelSpace nucleusModelSpace_;
	unsigned char Npmin_, Nnmin_;
//	sizeDistr_?_ ... number of active shells => since HO shells start with s
//	(i.e. n = 0) shell the maximal HO shell nmax == sizeDistr_?_ - 1
	unsigned char sizeDistr_p_, sizeDistr_n_;
//	basis is split into ndiag_ sections by w_pn
//	data structures that define many-body proton-neutron basis
	DistributionsSmallVector distributions_p_; 
	DistributionsSmallVector distributions_n_;
	UN::SU3xSU2_VEC gammas_p_;
	UN::SU3xSU2_VEC gammas_n_;
	std::vector<GammasWithinDistribution> gammasStructure_p_; 
	std::vector<GammasWithinDistribution> gammasStructure_n_;
	SU3_VEC omegasSU3_p_;
	SU3_VEC omegasSU3_n_;
	SU2_VEC omegasSpin_p_; 
	SU2_VEC omegasSpin_n_;
	std::vector<OmegasWithinGamma> omegasStructure_p_; 
	std::vector<OmegasWithinGamma> omegasStructure_n_;
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////       G l o b a l   f u n c t i o n s        /////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//	The whole basis is split into ndiag sections. The dimensions of each
//	section is stored in dims array.  For instance, the size of the i-th section is
//	stored in dims_[i], where i \in [0 ... ndiag-1]. 
//
//	CalculateDimAllSections ... calculates dimension of each section of the	basis and stores result in dims_ array
//	AccumulateSectionDims: returns dims[0] + dims[1] + dims[2] + .... + dims[ndiag-1] == total dimensiton of the basis
//	inline unsigned long AccumulateSectionDims() const {return std::accumulate(dims.begin(), dims.end(), (unsigned long)0);}
//	inline unsigned long firstStateId() const {return std::accumulate(dims_.begin(), dims_.begin() + idiag_, (unsigned long)0);}
//	inline unsigned long firstStateId(unsigned int idiag) const {assert(idiag < ndiag_); return std::accumulate(dims_.begin(), dims_.begin() + idiag, (unsigned long)0);}
//	set all elements of the array dims_[0 ... ndiags-1] to the number of basis
//	states that idiag-th section of the full basis maintains.
template<class SU3xSU2_JCOUPLED_BASIS>
void CalculateDimAllSections(const CncsmSU3Basis& Basis, std::vector<unsigned long>& dims, unsigned int ndiag, const SU3xSU2::IrrepsContainer<SU3xSU2_JCOUPLED_BASIS>& irreps)
{
/*	
	assert(dims.size() == ndiag);
//	make sure that dims_ contains all zeros	
	PNConfIterator pnConfIterator = Basis.firstPNConf(0, 1);
	dims.assign(dims.size(), 0);
	size_t iomega(0);
	for (pnConfIterator.rewindDistr_p(); pnConfIterator.hasDistr_p(); pnConfIterator.nextDistr_p())
	{
		for (pnConfIterator.rewindDistr_n(); pnConfIterator.hasDistr_n(); pnConfIterator.nextDistr_n())
		{
			for (pnConfIterator.rewindGamma_p(); pnConfIterator.hasGamma_p(); pnConfIterator.nextGamma_p())
			{
				for (pnConfIterator.rewindGamma_n(); pnConfIterator.hasGamma_n(); pnConfIterator.nextGamma_n())
				{
					for (pnConfIterator.rewindOmega_p(); pnConfIterator.hasOmega_p(); pnConfIterator.nextOmega_p())
					{
						size_t apmax = pnConfIterator.protonConfIter_.getMult();
						for (pnConfIterator.rewindOmega_n(); pnConfIterator.hasOmega_n(); pnConfIterator.nextOmega_n())
						{
							size_t anmax = pnConfIterator.neutronConfIter_.getMult();
							for (pnConfIterator.rewindPNOmega(); pnConfIterator.hasPNOmega(); pnConfIterator.nextPNOmega(), iomega++)
							{
								dims[iomega % ndiag] += anmax*apmax*irreps.rho_x_dim(pnConfIterator.getCurrentSU3xSU2());
							}
						}
					}
				}
			}
		}
	}
*/
	assert(dims.size() == ndiag);
	size_t iomega(0);
//	make sure that dims_ contains all zeros	
	PNConfIterator pnConfIterator = Basis.firstPNConf(0, 1);
	dims.assign(dims.size(), 0);
	for (pnConfIterator.rewind(); pnConfIterator.hasPNConf(); pnConfIterator.nextPNConf(), iomega++)
	{
		dims[iomega % ndiag] += pnConfIterator.getMult_p() * pnConfIterator.getMult_n()*irreps.rhomax_x_dim(pnConfIterator.getCurrentSU3xSU2());
	}
}

template<class SU3xSU2_JCOUPLED_BASIS>
unsigned long CalculateBasisDim(const PNConfIterator& pnConfIterator, const SU3xSU2::IrrepsContainer<SU3xSU2_JCOUPLED_BASIS>& irreps)
{
	PNConfIterator current_wpn(pnConfIterator);
	unsigned long dim(0);
	for (current_wpn.rewind(); current_wpn.hasPNConf(); current_wpn.nextPNConf())
	{
		dim += current_wpn.getMult_p() * current_wpn.getMult_n()*irreps.rhomax_x_dim(current_wpn.getCurrentSU3xSU2());
	}
	return dim;
}
#endif
