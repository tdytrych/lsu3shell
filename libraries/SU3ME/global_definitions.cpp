/****************************************************************
  global_definitions.cpp

  Last modified 3/5/11.
 
****************************************************************/

#include <SU3ME/global_definitions.h>

#include <cstdio>
#include <cstdlib>

const char *su3shell_data_directory = getenv("SU3SHELL_DATA");

const unsigned nspsmax = 256;//sizeof(unsigned long)*8;
const unsigned nmax = 35;

// true : tensor strengths do depend on kmax (besides multiplicity rho0)
// false: tensor strengths do not depend on kmax
bool k_dependent_tensor_strenghts = true;

namespace storage_limits
{
#ifdef SU3_9LM_HASHFIXED 
	size_t number_9lm_coeffs  = 35000000;
#else
	size_t number_9lm_coeffs  = 165001;
#endif
	size_t number_freq_9lm_coeffs = 5000;
	size_t number_u6lm_coeffs = 11000001;
	size_t number_z6lm_coeffs =  7500001;

//	number_su3su3_coeffs:
//	Maximal number of SU(3) irreps (wf, wi) that can be connected by a tensor
//	w0 L0. Its value depend on model space and interaction used.
//	LSU3shell_analytics can be used to estimate its value.
	size_t number_su3su3_coeffs = 3003;
}
