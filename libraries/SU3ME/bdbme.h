#ifndef BDBME_H
#define BDBME_H
#include <SU3ME/global_definitions.h> 	//	MINUSto(...)
#include <SU3NCSMUtils/clebschGordan.h> 		//	wigner6j(....)

#define POSITIVE_INFTY			

///////////////////////////////////////////////////////////////////////////////////////
///////////////   SO(3) and SU(2) reduced matrix elements of b+ and b   ///////////////
///////////////////////////////////////////////////////////////////////////////////////
// <nf lf || b+ || ni li> ... see equation (5) at docs/meHocs
// ASSUMPTION: it is assumed that lf == 2*lf and li = 2*li !
inline double bd(const int nf, const int lf, const int ni, const int li)
{
	if (nf == ni + 1)
	{
		if (lf == li - 2)
		{
#ifdef POSITIVE_INFTY			
			return -std::sqrt((li/2)*(ni - (li/2) + 2));
#else			
			return  std::sqrt((li/2)*(ni - (li/2) + 2));
#endif			
		}
		if (lf == li + 2)
		{
#ifdef POSITIVE_INFTY			
			return std::sqrt((li/2 + 1)*(ni + (li/2) + 3)); 
#else			
			return std::sqrt((li/2 + 1)*(ni + (li/2) + 3));
#endif			
		}
	}
	return 0;
}
// <nf lf jf || b+ || ni li ji > ... see equation (7) at docs/meHocs
// ASSUMPTION: it is assumed that lf == 2*lf, jf = 2*jf, li = 2*li, and ji = 2*ji !
inline double bd(const int nf, const int lf, const int jf, const int ni, const int li, const int ji)
{
	return MINUSto((ji + lf + 3)/2)*std::sqrt((ji + 1)*(jf + 1))*wigner6j(li, 1, ji, jf, 2, lf)*bd(nf, lf, ni, li);
}
// <nf lf || b || ni li> ... see equation (6) at docs/meHocs
// ASSUMPTION: it is assumed that lf == 2*lf and li = 2*li !
inline double b(const int nf, const int lf, const int ni, const int li)
{
	if (nf == (ni - 1))
	{
		if (lf == (li - 2))
		{
#ifdef POSITIVE_INFTY			
			return -std::sqrt((li/2)*(ni + (li/2) + 1));
#else			
			return -std::sqrt((li/2)*(ni + (li/2) + 1));
#endif			
		}
		if (lf == (li + 2))
		{
#ifdef POSITIVE_INFTY			
			return  std::sqrt(((li/2) + 1)*(ni - (li/2)));
#else			
			return -std::sqrt(((li/2) + 1)*(ni - (li/2)));
#endif			
		}
	}
	return 0;
}
// <nf lf jf || b || ni li ji > ... see equation (8) at docs/meHocs
// ASSUMPTION: it is assumed that lf == 2*lf, jf = 2*jf, li = 2*li, and ji = 2*ji !
inline double b(const int nf, const int lf, const int jf, const int ni, const int li, const int ji)
{
	return MINUSto((ji + lf + 3)/2)*std::sqrt((ji + 1)*(jf + 1))*wigner6j(li, 1, ji, jf, 2, lf)*b(nf, lf, ni, li);
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
#endif
