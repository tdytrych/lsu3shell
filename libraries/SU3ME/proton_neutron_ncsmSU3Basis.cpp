#include <SU3ME/proton_neutron_ncsmSU3Basis.h>

#include <set>
#include <sstream>
#include <stdexcept>

namespace proton_neutron
{
	void ModelSpace::Save(const std::string& model_space_definition_file_name)
	{
		std::ofstream output_file(model_space_definition_file_name.c_str());
		output_file << (int)Z_ << " " << (int)N_ << " " << JJcut_ << std::endl;
		for (size_t i = 0; i < modelSpace_.size(); ++i)
		{
			int number_SpSn = modelSpace_[i].size();
			output_file << (int)modelSpace_[i].N() << "\t";
			output_file << (int)((number_SpSn == 0) ? -1 : number_SpSn) << std::endl;
			for (size_t iSpSn = 0; iSpSn < number_SpSn; ++iSpSn)
			{
				output_file << "\t\t" << (int)modelSpace_[i][iSpSn].first.first << " "; // Sp
				output_file << (int)modelSpace_[i][iSpSn].first.second << " ";// Sn
				int number_S = modelSpace_[i][iSpSn].second.first.size();
				output_file << (int)((number_S == 0) ? -1 : number_S) << std::endl;
				for (size_t iS = 0; iS < number_S; ++iS)
				{
					output_file << "\t\t\t\t" << (int)modelSpace_[i][iSpSn].second.first[iS] << " ";
					int number_su3 = modelSpace_[i][iSpSn].second.second[iS].size();
					output_file << (int)((number_su3 == 0) ? -1 : number_su3) << std::endl << "\t\t\t\t\t";
					for (size_t isu3 = 0; isu3 < number_su3; ++isu3)
					{
						output_file << (int)modelSpace_[i][iSpSn].second.second[iS][isu3].lm << " ";
						output_file << (int)modelSpace_[i][iSpSn].second.second[iS][isu3].mu << " ";
						output_file << "\t";
					}
					output_file << endl;
				}
			}
		}
	}

	void ModelSpace::Load(const std::string& model_space_definition_file_name, bool without_output)
	{
		std::ifstream input_file(model_space_definition_file_name.c_str());
		if (!input_file)
		{
			std::ostringstream error_message;
			error_message << "Could not open model space definition file '" << model_space_definition_file_name << "'.";
			throw std::logic_error(error_message.str());
		}

		if (without_output == false) std::cout << "Loading definition of the model space from '" << model_space_definition_file_name << "'" << std::endl;

		int tmp;
		input_file >> tmp;
		Z_ = tmp;
		input_file >> tmp;
		N_ = tmp;
		input_file >> tmp;
		JJcut_ = tmp;
		
		if (without_output == false) std::cout << "Number of protons: " << (int)number_of_protons() << "\t Number of neutrons:" << (int)number_of_neutrons() << "\tJcut=" << (int)JJcut() << std::endl;

		int N, S, Sp, Sn, number_SpSn, number_S, number_SU3, lm, mu;
		while (true)
		{
			input_file >> N;
			if (!input_file) // this must be the end of file
			{
				break;
			}
			CNhwSubspace::Model_Space_Definition model_space_definition;
			if (without_output == false) std::cout << "Nhw = " << N << " ";
			input_file >> number_SpSn;
			if (!input_file) // this must be the end of file
			{
				break;
			}

			assert(number_SpSn == -1 || number_SpSn > 0);

			if (number_SpSn >= +1)	// include all Sp Sn correlations ==> in order to do that use an empty model_space_definition
			{

				if (without_output == false) std::cout << number_SpSn << std::endl;
				for (size_t iSpSn = 0; iSpSn < number_SpSn; ++iSpSn)
				{
					input_file >> Sp;
					input_file >> Sn;
					input_file >> number_S;
					assert(number_S == -1 || number_S > 0);
					if (without_output == false) std::cout << "Sp=" << Sp << " Sn=" << Sn << " #(allowed Sp x Sn-->S)=" << number_S << endl;
					if (number_S == -1)
					{
						model_space_definition.push_back(std::make_pair(std::make_pair(Sp, Sn), std::make_pair(SU2_VEC(), std::vector<SU3_VEC>()))); // empty allowed total Spin ==> all spins and SU(3) are included
						continue;
					}

					SU2_VEC allowedSpins(number_S);
					std::vector<SU3_VEC> allowedSU3(number_S);
					for (size_t iS = 0; iS < number_S; ++iS)
					{
						input_file >> S;
						input_file >> number_SU3;
						allowedSpins[iS] = S;
						assert(number_SU3 == -1 || number_SU3 > 0);
	

						if (without_output == false) cout << "S=" << (int)allowedSpins[iS] << " #(allowed SU3 irreps)=" << number_SU3 << endl;
						if (number_SU3 >= +1)
						{
							for (size_t isu3 = 0; isu3 < number_SU3; ++isu3)
							{
								input_file >> lm;
								input_file >> mu;
								if (without_output == false) std::cout << "(" << lm << " " << mu << ")\t";
								allowedSU3[iS].push_back(SU3::LABELS(1, lm, mu));
							}
						}
						if (without_output == false) cout << std::endl;
					}
					model_space_definition.push_back(std::make_pair(std::make_pair(Sp, Sn), std::make_pair(allowedSpins, allowedSU3)));
				}
			}
			else
			{
				assert(number_SpSn == -1);
				if (without_output == false) std::cout << "full subspace" << std::endl;
				modelSpace_.push_back(CNhwSubspace(N, model_space_definition));
				continue; // need not to read the rest, next number in file is going to be either Nhw or the end of file
			}
			modelSpace_.push_back(CNhwSubspace(N, model_space_definition));
		}
	}
}
	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////       I d e n t F e r m C o n f I t e r a t o r       ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void IdentFermConfIterator::ShowConf() const
{
	SingleDistributionSmallVector distr;
	UN::SU3xSU2_VEC gamma;
	SU3xSU2_SMALL_VEC omega;

	getDistr(distr);
	getGamma(gamma);
	getOmega(omega);
	cout << "Distr:";
	for (size_t i = 0; i < distr.size(); ++i)
	{
		cout << (int)distr[i] << " ";
	}
	cout << endl;

	cout << "Gamma:";
	for (size_t i = 0; i < gamma.size(); ++i)
	{
		cout << gamma[i] << "  ";
	}	
	cout << endl;

	if (nOccupiedShells_ > 1)
	{
		for (size_t i = 0; i < omega.size(); ++i)
		{
			cout << omega[i] << "  ";
		}	
		cout << endl;
	}
	else
	{
		cout << "empty" << endl;
	}
}

void IdentFermConfIterator::rewindDistr()
{
	ngammas_iterated_ = 0;
	currentDistr_ = distributions_.begin(); 
	gammasStructureIter_ = gammasStructure_.begin();
	nOccupiedShells_ = nOccupiedShells();
}

void IdentFermConfIterator::nextDistr() 
{
	ngammas_iterated_ += gammasStructureIter_->nGammas;
	currentDistr_ += sizeDistr_; 
	++gammasStructureIter_;
	if (hasDistr())
	{
		nOccupiedShells_ = nOccupiedShells();
	}
}

void IdentFermConfIterator::rewindGamma()
{
	igamma_ = 0;	//	distribution has gammasStructureIter_->nGammas distinct vectors of U(N)>SU(3) x SU(2) irreps
	currentGamma_ = gammas_.begin() + gammasStructureIter_->firstGamma;	// 	if the iteration was continuous, we would not need to reinitialize currentGamma_
																		//	however, we may want to skip certain distributions ==> initialization is needed
	omegasStructureIter_ = omegasStructure_.begin() + ngammas_iterated_;
}

void IdentFermConfIterator::nextGamma()
{
	if (hasGamma())
	{
		++igamma_;
		currentGamma_ += nOccupiedShells_; // vector of U(N)>SU(3)xSU(2) irreps has nOccupiedShells_p_ elements
		++omegasStructureIter_;
	}
	if (!omegas_.empty())
	{
		omegas_.resize(0);
	}
}

void IdentFermConfIterator::rewindOmega()
{
	if (omegas_.empty())
	{
		if (nOccupiedShells_ > 1)
		{
			size_t nirreps = nOccupiedShells_ - 1; // nirreps == number of elements in omega_su3 and omega_spin and omega_su3xsu2 vectors
			size_t nomegas_su3  = omegasStructureIter_->nomegas_su3;
			size_t nomegas_spin = omegasStructureIter_->nomegas_spin;
			size_t nomegas = nomegas_su3*nomegas_spin;

			omegas_.reserve(nirreps*nomegas);
			SU3xSU2_VEC omega(nirreps);
			for (size_t iwsu3 = 0, index_su3 = omegasStructureIter_->firstOmega_su3; iwsu3 < nomegas_su3; ++iwsu3, index_su3 += nirreps)
			{
				for (size_t i = 0; i < nirreps; ++i)
				{
					omega[i].rho = omegasSU3_[index_su3 + i].rho;
					omega[i].lm  = omegasSU3_[index_su3 + i].lm;
					omega[i].mu  = omegasSU3_[index_su3 + i].mu;
				}
				for (size_t iwspin = 0, index_spin = omegasStructureIter_->firstOmega_spin; iwspin < nomegas_spin; ++iwspin, index_spin += nirreps)
				{
					for (size_t i = 0; i < nirreps; ++i)
					{
						omega[i].S2  = omegasSpin_[index_spin + i];
					}
					//	insert at the end of vector omegas_ content of vector omega
					omegas_.insert(omegas_.end(), omega.begin(), omega.end());
				}
			}
		}
		else // all protons occupy a single shell => omega is empty
		{
			omegas_.push_back(SU3xSU2::LABELS());
		}
	}
	currentOmega_ = omegas_.begin();
}

SU3xSU2::LABELS IdentFermConfIterator::getCurrentSU3xSU2() const
{
	if (nOccupiedShells_ == 1)	//	vector of inter-shell coupled SU3xSU2 is empty ==> we are dealing with a single-shell U(N)>SU3xSU2 many-body basis state
	{
		return SU3xSU2::LABELS(currentGamma_->lm, currentGamma_->mu, currentGamma_->S2);
	}
	else
	{
		return currentOmega_[nOccupiedShells_ - 2];
	}
}

int IdentFermConfIterator::getMult() const
{
	size_t ntot = 1;
	for (size_t i = 0; i < nOccupiedShells_ - 1; ++i)
	{ 
		ntot *= currentGamma_[i].mult * currentOmega_[i].rho;
	}
	return ntot*currentGamma_[nOccupiedShells_ - 1].mult;
}

unsigned char IdentFermConfIterator::HOquanta() const
{
	unsigned char n(0), i(0);
	for (DistributionsSmallVectorIter it = currentDistr_; it < currentDistr_ + sizeDistr_; ++it)
	{
		n += (*it)*(i++);
	}
	return n;
}

void IdentFermConfIterator::Set(const IdentFermConfIterator& Iter)
{
	currentDistr_ = Iter.currentDistr_;
	currentGamma_ = Iter.currentGamma_;
	currentOmegaSU3_ = Iter.currentOmegaSU3_;
	currentOmegaSpin_ = Iter.currentOmegaSpin_;
	gammasStructureIter_ = Iter.gammasStructureIter_;
	omegasStructureIter_ = Iter.omegasStructureIter_;
	if (!omegas_.empty())
	{
		omegas_.resize(0);
	}
	omegas_ = Iter.omegas_;
	currentOmega_ = omegas_.begin() + (Iter.currentOmega_ - Iter.omegas_.begin());
//	variables needed for the iteration over distributions and gammas
//	igamma_ ... counts how many gammas in a given distribuiton we iterated
//				through ==> it ranges from 0 up to nOccupiedShells_ - 1
	igamma_ = Iter.igamma_;
	ngammas_iterated_ = Iter.ngammas_iterated_;
	nOccupiedShells_ = Iter.nOccupiedShells_;
}

void IdentFermConfIterator::ShowModelSpace()
{
	for (rewindDistr(); hasDistr(); nextDistr())
	{
		cout << "Distribution: ";
		for (size_t i = 0; i < sizeDistr_; ++i)
		{
			cout << (int)currentDistr_[i] << " ";
		}
//		cout << "\t Np = " << (int) Np() << "\t #occupied shells = " << (int)nOccupiedShells_p() << endl;
		cout << "\t #occupied shells = " << (int)nOccupiedShells() << endl;

		size_t nGammas = nOccupiedShells();
		for (rewindGamma(); hasGamma(); nextGamma())
		{
			UN::SU3xSU2_VEC gamma;
			getGamma(gamma);
			for (size_t i = 0; i < gamma.size(); ++i)
			{
				cout << gamma[i] << "  ";
			}	
			cout << endl;
			for (rewindOmega(); hasOmega(); nextOmega())
			{
				SU3xSU2_SMALL_VEC omega;
				getOmega(omega);
				assert(omega.size() == nOccupiedShells_ - 1);
				if (nOccupiedShells_ > 1)
				{
					for (size_t i = 0; i < omega.size(); ++i)
					{
						cout << omega[i] << "  ";
					}	
					cout << endl;
				}
				else
				{
					cout << "empty" << endl;
				}
			}
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////       P N C o n f I t e r a t o r       ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
PNConfIterator::PNConfIterator(const PNConfIterator& Iter): 
	ncsmSU3PNBasisContainer_(Iter.ncsmSU3PNBasisContainer_), 
	nhwSubspace_(Iter.nhwSubspace_),
	protonConfIter_(Iter.protonConfIter_),
	neutronConfIter_(Iter.neutronConfIter_),
	idiag_(Iter.idiag_), ndiag_(Iter.ndiag_),
	num_PNomegas_iterated_(Iter.num_PNomegas_iterated_),
	pnOmegas_(Iter.pnOmegas_)
{
	wpn_su3_temporary_.reserve(60);
	wpn_spin_temporary_.reserve(60);
	pnOmegas_.reserve(1024);

	pnOmegas_end_ = pnOmegas_.end();
	currentPNOmega_ = pnOmegas_.begin() + (Iter.currentPNOmega_ - Iter.pnOmegas_.begin());

//	We are constructing a brand new iterator ==> initial status must include
//	contain the highest change, i.e. new proton distribution
	status_ = kNewDistr_p;
}

//	set all internal iterators on the first available configuration
void PNConfIterator::rewind()
{
	num_PNomegas_iterated_ = 0;
	rewindDistr_p();
	rewindDistr_n();
	rewindGamma_p();
	rewindGamma_n();
	rewindOmega_p();
	rewindOmega_n();
	rewindPNOmega();
//	if ndiag_ is large ==> after calling all rewind* methods, array of omega_pn
//	may be empty. In such a case we must move to the next available
//	configuration for (idiag_, ndiag_) pair of numbers.
	if (!hasPNOmega() && hasDistr_p())
	{	
//		nextPNConf iterates to the first nearest configuration for a given
//		idag_ and ndiag_
		nextPNConf();
	}
	status_ = kNewDistr_p;
}


void PNConfIterator::rewind(const PNConfIterator& Iter)
{
	protonConfIter_.Set(Iter.protonConfIter_);
	neutronConfIter_.Set(Iter.neutronConfIter_);

	num_PNomegas_iterated_ = Iter.num_PNomegas_iterated_;
	pnOmegas_ = Iter.pnOmegas_;
	pnOmegas_end_ = pnOmegas_.end();
	
	currentPNOmega_ = pnOmegas_.begin() + (Iter.currentPNOmega_ - Iter.pnOmegas_.begin());
	nhwSubspace_ = Iter.nhwSubspace_;

//	the state of the iterator is reset to the external iterator Iter	
//	and hence it is safe to assume that everything has been changed ==> 
//	status_ = kNewDistr_p;
	status_ = kNewDistr_p;
}

PNConfIterator::PNConfIterator(const CncsmSU3Basis* ncsmSU3PNBasisContainer, unsigned int idiag, unsigned int ndiag):
					ncsmSU3PNBasisContainer_(ncsmSU3PNBasisContainer), 
					protonConfIter_(ncsmSU3PNBasisContainer->firstProtonConf()), 
					neutronConfIter_(ncsmSU3PNBasisContainer->firstNeutronConf()), 
					idiag_(idiag), ndiag_(ndiag)
{
	wpn_su3_temporary_.reserve(60);
	wpn_spin_temporary_.reserve(60);
	pnOmegas_.reserve(1024);

/** One has to remember to call rewind() before iterating!
 */
//	rewind();
}

void PNConfIterator::rewindPNOmega()
{
	if (pnOmegas_.empty())
	{
		size_t iomega = num_PNomegas_iterated_;
		size_t nirreps(0);
		std::vector<SU3_VEC> allowed_su3;
		SU3xSU2::LABELS wn(neutronConfIter_.getCurrentSU3xSU2());
		SU3xSU2::LABELS wp(protonConfIter_.getCurrentSU3xSU2());
//	for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
//	return vector of allowed SU3 labels for each allowed spin: 
//	{{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
		nhwSubspace_->GetAllowedSpinsAndSU3(std::make_pair(wp.S2, wn.S2), wpn_spin_temporary_, allowed_su3);
 //	if nwpn_spin == 0 ==> {Sp Sn} is associated with an empty array of allowed spins ==> {Sp Sn} subspace is entirely out of model space
		size_t nwpn_spin = wpn_spin_temporary_.size();
		if (nwpn_spin)
		{
			SU3::Couple(wp, wn, wpn_su3_temporary_);
			SU3_VEC::const_iterator last_wpn_su3 = wpn_su3_temporary_.end();
//	iterate over allowed spins ...
			for (size_t ispin = 0; ispin < nwpn_spin; ++ispin)
			{
				if (!allowed_su3[ispin].empty())	//	==> I need to select only allowed (lm mu) 
				{
					for (SU3_VEC::const_iterator current_su3 = wpn_su3_temporary_.begin(); current_su3 < last_wpn_su3; ++current_su3)
					{
						SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
						if (current_allowed_su3 != allowed_su3[ispin].end() && (current_allowed_su3->lm == current_su3->lm && current_allowed_su3->mu == current_su3->mu))
						{
							if (idiag_ == (iomega % ndiag_))
							{
								pnOmegas_.push_back(SU3xSU2::LABELS(*current_su3, wpn_spin_temporary_[ispin]));
							}
							iomega++;
							nirreps++;
						}
					}
				}
				else	//	if array of allowed SU(3) irreps is empty ==> include ALL SU(3) irreps listed in wpn_su3_temporary_
				{
					nirreps += wpn_su3_temporary_.size();
					for (SU3_VEC::const_iterator current_su3 = wpn_su3_temporary_.begin(); current_su3 < last_wpn_su3; ++current_su3)
					{
						if (idiag_ == (iomega % ndiag_))
						{
							pnOmegas_.push_back(SU3xSU2::LABELS(*current_su3, wpn_spin_temporary_[ispin]));
						}
						iomega++;
					}
				}
			}
		}
		num_PNomegas_iterated_ += nirreps;
		pnOmegas_end_ = pnOmegas_.end();

		wpn_su3_temporary_.resize(0);
		wpn_spin_temporary_.resize(0);
	}
	currentPNOmega_ = pnOmegas_.begin();
}	

//	currentPNOmega = wp x wn --> rho_pn w_pn
//	dimPNOmega: return (rho_pn * SU3xSU2::dim(w_pn))
/*
size_t PNConfIterator::dimPNOmega() const
{
//	if (currentPNOmega_->lm + currentPNOmega_->mu > 5) // large irrep ==> get dimension from the table of SU(3)xSU(2) irreps
//	{
//		std::vector<SU3xSU2::Basis>::const_iterator it = std::lower_bound(ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.begin(), ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.end(), *currentPNOmega_);
//		assert(it < ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.end());
//		return currentPNOmega_->rho*it->dim();
//	}
//	else // small irrep ==> calculate its dimension on the fly ...
//	{
//		return currentPNOmega_->rho*SU3xSU2::Basis::getDim(*currentPNOmega_);
//	}
	std::vector<SU3xSU2::Basis>::const_iterator it = std::lower_bound(ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.begin(), ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.end(), *currentPNOmega_);
	assert(it < ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.end());
	return currentPNOmega_->rho*it->dim();
}
*/
bool PNConfIterator::hasDistr_n() 
{
	return (neutronConfIter_.hasDistr() && (nhw() <= ncsmSU3PNBasisContainer_->Nmax()));
}
void PNConfIterator::rewindDistr_n() 
{
	neutronConfIter_.rewindDistr();

	nhwSubspace_ = std::find(ncsmSU3PNBasisContainer_->nucleusModelSpace_.begin(), ncsmSU3PNBasisContainer_->nucleusModelSpace_.end(), nhw());
	while (neutronConfIter_.hasDistr() && nhwSubspace_ == ncsmSU3PNBasisContainer_->nucleusModelSpace_.end())
	{
		neutronConfIter_.nextDistr();
		nhwSubspace_ = std::find(ncsmSU3PNBasisContainer_->nucleusModelSpace_.begin(), ncsmSU3PNBasisContainer_->nucleusModelSpace_.end(), nhw());
	}
	assert(nhwSubspace_ != ncsmSU3PNBasisContainer_->nucleusModelSpace_.end());
}

void PNConfIterator::nextDistr_n()
{
	neutronConfIter_.nextDistr();

	nhwSubspace_ = std::find(ncsmSU3PNBasisContainer_->nucleusModelSpace_.begin(), ncsmSU3PNBasisContainer_->nucleusModelSpace_.end(), nhw_p() + nhw_n());
	while (neutronConfIter_.hasDistr() && nhwSubspace_ == ncsmSU3PNBasisContainer_->nucleusModelSpace_.end())
	{
		neutronConfIter_.nextDistr();
		nhwSubspace_ = std::find(ncsmSU3PNBasisContainer_->nucleusModelSpace_.begin(), ncsmSU3PNBasisContainer_->nucleusModelSpace_.end(), nhw_p() + nhw_n());
	}
}

unsigned char PNConfIterator::nhw_p() const
{ 
	return  protonConfIter_.HOquanta() - ncsmSU3PNBasisContainer_->Npmin_;
}

unsigned char PNConfIterator::nhw_n() const
{ 
	return neutronConfIter_.HOquanta() - ncsmSU3PNBasisContainer_->Nnmin_;
}

unsigned char PNConfIterator::nhw() const 
{
	return nhw_p() + nhw_n();
}


inline void PNConfIterator::nextOmega_n() 
{
	neutronConfIter_.nextOmega();
	if (!pnOmegas_.empty())
	{
		pnOmegas_.resize(0);
		pnOmegas_end_ = pnOmegas_.end();
	}
}

// Non recursive version of nextPNConf() Replacement for recursive version that
// was causing troubles
void PNConfIterator::nextPNConf()
{
	char max_status = kNewPNOmega;
	do {
		status_ = kNewPNOmega;
		nextPNOmega();
		if (!hasPNOmega())
		{
			++status_; // == status = kNewOmega_n
			nextOmega_n();
			if (!hasOmega_n())  // there are no more Omega_n for a given proton configuration
			{
				++status_; // == status = kNewOmega_p
				nextOmega_p(); // ==> we need to move to the next Omega_p 
				if (!hasOmega_p()) // if we have no more Omega_p for a given Gamma_n 
				{
					++status_; // == status = kNewGamma_n
					nextGamma_n();	// take next Gamma_n
					if (!hasGamma_n()) // if we have no more Gamma_n for a given Gamma_p
					{
						++status_; // == status = kNewGamma_p
						nextGamma_p(); // take next Gamma_p
						if (!hasGamma_p()) // if there are no more Gamma_p in given Distr_n
						{
							++status_; // == status = kNewDistr_n
							nextDistr_n();
							if (!hasDistr_n())
							{
								++status_; // == status = kNewDistr_p
								nextDistr_p();
								if (!hasDistr_p())
								{
									return;
								}
								rewindDistr_n();
							}
							rewindGamma_p();
						}
						rewindGamma_n();
					}
					rewindOmega_p();
				}
				rewindOmega_n();
			}
			rewindPNOmega();

			if (status_ > max_status)   //	if ndiag is large enough or selection rules are very restrictive ==> array of proton-neutron irreps may be empty
			{                           //  if it is empty ==> we need to generate a new set of pnomegas, but that may require 
				max_status = status_;   //	construction of a new gamma, distr etc... and hence we need to switch the status_
			}                           //	the highest change achieved
		}
	} while(!hasPNOmega());	

	if (max_status > status_)
	{
		status_ = max_status;
	}
}

/* Know issues:
   This is recursive method. If the model space is very large, yet our selection
   rules are going to include just a fraction of it, the depth of recursion 
   becomes prohobively large. 
   Example: try to iterate over 24Mg N=6 (8 4) Sp=0 Sn=0 S=0 
*/   
/*
void PNConfIterator::nextPNConf()
{
	status_ = kNewPNOmega;
	nextPNOmega();
	if (!hasPNOmega())
	{
		++status_; // == status = kNewOmega_n
		nextOmega_n();
		if (!hasOmega_n())
		{
			++status_; // == status = kNewOmega_p
			nextOmega_p();
			if (!hasOmega_p())
			{
				++status_; // == status = kNewGamma_n
				nextGamma_n();
				if (!hasGamma_n())
				{
					++status_; // == status = kNewGamma_p
					nextGamma_p();
					if (!hasGamma_p())
					{
						++status_; // == status = kNewDistr_n
						nextDistr_n();
						if (!hasDistr_n())
						{
							++status_; // == status = kNewDistr_p
							nextDistr_p();
							if (!hasDistr_p())
							{
								return;
							}
							rewindDistr_n();
						}
						rewindGamma_p();
					}
					rewindGamma_n();
				}
				rewindOmega_p();
			}
			rewindOmega_n();
		}
		rewindPNOmega();
							//	if ndiag is large enough ==> array of proton-neutron irreps may be empty
		if (!hasPNOmega()) 	// if it is empty ==> we need to generate a new set of pnomegas, but that may require 
							//	construction of a new gamma, distr etc... and hence we need to take proper care of
							//	the current status_
		{
			unsigned char old_status = status_;
			nextPNConf();
			if (old_status > status_)
			{
				status_ = old_status;
			}
		}
	}
}
*/

size_t PNConfIterator::getMult() const
{
	return protonConfIter_.getMult()*neutronConfIter_.getMult();
}

size_t PNConfIterator::getMult_p() const 
{
	return protonConfIter_.getMult();
}

size_t PNConfIterator::getMult_n() const 
{
	return neutronConfIter_.getMult();
}

SU3xSU2::LABELS PNConfIterator::getProtonSU3xSU2() const 
{
	return protonConfIter_.getCurrentSU3xSU2();
}

SU3xSU2::LABELS PNConfIterator::getNeutronSU3xSU2() const 
{
	return neutronConfIter_.getCurrentSU3xSU2();
}

/*
const SU3xSU2::Basis& PNConfIterator::getSU3xSU2PhysicalBasis(const SU3xSU2::LABELS& omega_pn_bra) const
{
	std::vector<SU3xSU2::Basis>::const_iterator it = std::lower_bound(ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.begin(), ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.end(), *currentPNOmega_);
	assert(it < ncsmSU3PNBasisContainer_->final_SU3xSU2Irreps_.end());
	return *it;
}
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////       C n c s m S U 3 B a s i s      //////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
CncsmSU3Basis::CncsmSU3Basis(	const unsigned char Z, const unsigned char N, const proton_neutron::ModelSpace& nucleusModelSpace):
								CBaseSU3Irreps(Z, N, nucleusModelSpace.back().N()),
								Z_(Z), N_(N), nucleusModelSpace_(nucleusModelSpace)
{
	assert(distributions_p_.empty()); 
	assert(distributions_n_.empty());
	assert(gammas_p_.empty());
	assert(gammas_n_.empty());
	assert(gammasStructure_p_.empty()); 
	assert(gammasStructure_n_.empty());
	assert(omegasSU3_p_.empty());
	assert(omegasSU3_n_.empty());
	assert(omegasSpin_p_.empty()); 
	assert(omegasSpin_n_.empty());
	assert(omegasStructure_p_.empty()); 
	assert(omegasStructure_n_.empty());

	unsigned char nmax = Nmax();

	CShellConfigurations ShellConfs_p(Nmax(), Z);
	tSHELLSCONFIG distr;
//	Npmin_ and Nnmin_ holds the HO quanta for the lowest [i.e. 0hw] configurations
	distr = (ShellConfs_p.GetConfigurations(0))[0];
	sizeDistr_p_ = ShellConfs_p.GetMaxShellNumber() + 1; 
	Npmin_ = 0;
	for (size_t i = 0; i < distr.size(); ++i)
	{
		Npmin_ += i*distr[i];
	}

	for (size_t n = 0; n <= nmax; ++n)
	{
		const std::vector<tSHELLSCONFIG>& ConfsNhw  = ShellConfs_p.GetConfigurations(n);
		for (std::vector<tSHELLSCONFIG>::const_iterator distribution = ConfsNhw.begin(); distribution != ConfsNhw.end(); ++distribution)
		{
			GammasWithinDistribution gammasInfo;
//	for distribution 			
			GenerateDistributionSpace(*distribution, nucleon::PROTON, gammasInfo);
			tSHELLSCONFIG dstr(*distribution);
			if (gammasInfo.nGammas > 0)
			{	
				//	ngammas = number of vectors of UN>SU3xSU2 that have been added to gammas_p_ vector == (number of UN>SU3xSU2 irreps) / (number of occupied shells)
				assert(gammasInfo.nGammas == (gammas_p_.size() - gammasInfo.firstGamma)/std::count_if(distribution->begin(), distribution->end(), std::bind2nd(std::greater<unsigned char>(), 0))); 
				gammasStructure_p_.push_back(gammasInfo);
				distributions_p_.insert(distributions_p_.end(), distribution->begin(), distribution->end());
			}
		}
	}

	CShellConfigurations ShellConfs_n(Nmax(), N);
	distr = (ShellConfs_n.GetConfigurations(0))[0];
	sizeDistr_n_ = ShellConfs_n.GetMaxShellNumber() + 1; 
	Nnmin_ = 0;
	for (size_t i = 0; i < distr.size(); ++i)
	{
		Nnmin_ += i*distr[i];
	}

	for (size_t n = 0; n <= nmax; ++n)
	{
		const std::vector<tSHELLSCONFIG>& ConfsNhw  = ShellConfs_n.GetConfigurations(n);
		for (std::vector<tSHELLSCONFIG>::const_iterator distribution = ConfsNhw.begin(); distribution != ConfsNhw.end(); ++distribution)
		{
			GammasWithinDistribution gammasInfo;
			GenerateDistributionSpace(*distribution, nucleon::NEUTRON, gammasInfo);
			tSHELLSCONFIG dstr(*distribution);
			if (gammasInfo.nGammas > 0)
			{	
				//	ngammas = number of vectors of UN>SU3xSU2 that have been added to gammas_n_ vector == (number of UN>SU3xSU2 irreps) / (number of occupied shells)
				assert(gammasInfo.nGammas == (gammas_n_.size() - gammasInfo.firstGamma)/std::count_if(distribution->begin(), distribution->end(), std::bind2nd(std::greater<unsigned char>(), 0))); 
				gammasStructure_n_.push_back(gammasInfo);
				distributions_n_.insert(distributions_n_.end(), distribution->begin(), distribution->end());
			}
		}
	}
}

CncsmSU3Basis::CncsmSU3Basis(const proton_neutron::ModelSpace& nucleusModelSpace): 	CBaseSU3Irreps(nucleusModelSpace.number_of_protons(), nucleusModelSpace.number_of_neutrons(), nucleusModelSpace.back().N()), 
																					Z_(nucleusModelSpace.number_of_protons()), N_(nucleusModelSpace.number_of_neutrons()), nucleusModelSpace_(nucleusModelSpace)
{
	assert(distributions_p_.empty()); 
	assert(distributions_n_.empty());
	assert(gammas_p_.empty());
	assert(gammas_n_.empty());
	assert(gammasStructure_p_.empty()); 
	assert(gammasStructure_n_.empty());
	assert(omegasSU3_p_.empty());
	assert(omegasSU3_n_.empty());
	assert(omegasSpin_p_.empty()); 
	assert(omegasSpin_n_.empty());
	assert(omegasStructure_p_.empty()); 
	assert(omegasStructure_n_.empty());

	unsigned char nmax = Nmax();

	CShellConfigurations ShellConfs_p(Nmax(), Z_);
	tSHELLSCONFIG distr;
//	Npmin_ and Nnmin_ holds the HO quanta for the lowest [i.e. 0hw] configurations
	distr = (ShellConfs_p.GetConfigurations(0))[0];
	sizeDistr_p_ = ShellConfs_p.GetMaxShellNumber() + 1; 
	Npmin_ = 0;
	for (size_t i = 0; i < distr.size(); ++i)
	{
		Npmin_ += i*distr[i];
	}

	for (size_t n = 0; n <= nmax; ++n)
	{
		const std::vector<tSHELLSCONFIG>& ConfsNhw  = ShellConfs_p.GetConfigurations(n);
		for (std::vector<tSHELLSCONFIG>::const_iterator distribution = ConfsNhw.begin(); distribution != ConfsNhw.end(); ++distribution)
		{
			GammasWithinDistribution gammasInfo;
//	for distribution 			
			GenerateDistributionSpace(*distribution, nucleon::PROTON, gammasInfo);
			tSHELLSCONFIG dstr(*distribution);
			if (gammasInfo.nGammas > 0)
			{	
				//	ngammas = number of vectors of UN>SU3xSU2 that have been added to gammas_p_ vector == (number of UN>SU3xSU2 irreps) / (number of occupied shells)
				assert(gammasInfo.nGammas == (gammas_p_.size() - gammasInfo.firstGamma)/std::count_if(distribution->begin(), distribution->end(), std::bind2nd(std::greater<unsigned char>(), 0))); 
				gammasStructure_p_.push_back(gammasInfo);
				distributions_p_.insert(distributions_p_.end(), distribution->begin(), distribution->end());
			}
		}
	}

	CShellConfigurations ShellConfs_n(Nmax(), N_);
	distr = (ShellConfs_n.GetConfigurations(0))[0];
	sizeDistr_n_ = ShellConfs_n.GetMaxShellNumber() + 1; 
	Nnmin_ = 0;
	for (size_t i = 0; i < distr.size(); ++i)
	{
		Nnmin_ += i*distr[i];
	}

	for (size_t n = 0; n <= nmax; ++n)
	{
		const std::vector<tSHELLSCONFIG>& ConfsNhw  = ShellConfs_n.GetConfigurations(n);
		for (std::vector<tSHELLSCONFIG>::const_iterator distribution = ConfsNhw.begin(); distribution != ConfsNhw.end(); ++distribution)
		{
			GammasWithinDistribution gammasInfo;
			GenerateDistributionSpace(*distribution, nucleon::NEUTRON, gammasInfo);
			tSHELLSCONFIG dstr(*distribution);
			if (gammasInfo.nGammas > 0)
			{	
				//	ngammas = number of vectors of UN>SU3xSU2 that have been added to gammas_n_ vector == (number of UN>SU3xSU2 irreps) / (number of occupied shells)
				assert(gammasInfo.nGammas == (gammas_n_.size() - gammasInfo.firstGamma)/std::count_if(distribution->begin(), distribution->end(), std::bind2nd(std::greater<unsigned char>(), 0))); 
				gammasStructure_n_.push_back(gammasInfo);
				distributions_n_.insert(distributions_n_.end(), distribution->begin(), distribution->end());
			}
		}
	}
}

void CncsmSU3Basis::GenerateDistributionSpace(const tSHELLSCONFIG& distribution, const nucleon::Type type, GammasWithinDistribution& gammasInfo)
{
	gammasInfo.firstGamma = (type == nucleon::PROTON) ? gammas_p_.size() : gammas_n_.size(); // this is the position where the new UN>SU3xSU2 irrep (gamma) is going to be inserted

	size_t maxHOshell = (type == nucleon::PROTON) ? sizeDistr_p_ - 1 : sizeDistr_n_ - 1;
	std::vector<UN::SU3xSU2_VEC> vAllowedBaseIrreps;

    for (size_t n = 0; n <= maxHOshell; ++n)		
    {
		unsigned A = distribution[n];// number of fermions in n-th shell
		if (A == 0) {
    		continue;
		}
		else
		{
			UN::SU3xSU2_VEC AllowedIrreps;
			GetIrreps(n, A, AllowedIrreps);
			vAllowedBaseIrreps.push_back(AllowedIrreps);
		}
	}
    // Now we generate all possible combinations of coupling out of vvSU3xSU2Irreps
    size_t NIrrepsSets = vAllowedBaseIrreps.size();
	std::vector<unsigned> vElemsPerChange(NIrrepsSets, 1); //Fill with 1 cause vElemsPerChange[0] = 1;
	std::vector<size_t> vNSizes(NIrrepsSets);
	// uNAllowedCombinations = number of all possible coupling combinations
	// uNAllowedCombinations = vvSU3xSU2Irreps[0].size()* ... *vvSU3xSU2Irreps[NIrrepsSets].size()
    unsigned uNAllowedCombinations = 1;

    for (size_t i = 0; i < NIrrepsSets; i++)
    {
		vNSizes[i] = vAllowedBaseIrreps[i].size();	
   		uNAllowedCombinations *= vNSizes[i];
   	}

    for (size_t i = 1; i < NIrrepsSets; i++)
   	{
   		// if i == 0 => vElemsPerChange[0]=1 since constructor
		vElemsPerChange[i] = vElemsPerChange[i-1]*vNSizes[i-1];
   	}

    for (unsigned index = 0; index < uNAllowedCombinations; index++)
    {
		// in each step create a set of SU3xSU2 irreps to couple 
		UN::SU3xSU2_VEC gamma;
		for (size_t i = 0; i < NIrrepsSets; i++)
		{
		    size_t iElement = (index / vElemsPerChange[i]) % vNSizes[i];
		    gamma.push_back(vAllowedBaseIrreps[i][iElement]);
   		}

		if (gamma.size() > 1)
		{
			std::vector<SU2_VEC> omega_spin;
			std::vector<SU3_VEC> omega_su3;

			gamma2su2vec(gamma, omega_spin);
			gamma2su3vec(gamma, omega_su3);

			if (!omega_spin.empty() && !omega_su3.empty())
			{
				OmegasWithinGamma omegasInfo;
				omegasInfo.firstOmega_su3  = (type == nucleon::PROTON) ? omegasSU3_p_.size() : omegasSU3_n_.size();
				omegasInfo.firstOmega_spin = (type == nucleon::PROTON) ? omegasSpin_p_.size() : omegasSpin_n_.size();
				omegasInfo.nomegas_su3 = omega_su3.size(); 
				omegasInfo.nomegas_spin = omega_spin.size(); 

				if (type == nucleon::PROTON)
				{
					gammas_p_.insert(gammas_p_.end(), gamma.begin(), gamma.end());

					omegasStructure_p_.push_back(omegasInfo);
					for (size_t i = 0; i < omega_su3.size(); ++i)
					{
						omegasSU3_p_.insert(omegasSU3_p_.end(), omega_su3[i].begin(), omega_su3[i].end());
					}
					for (size_t i = 0; i < omega_spin.size(); ++i)
					{
						omegasSpin_p_.insert(omegasSpin_p_.end(), omega_spin[i].begin(), omega_spin[i].end());
					}
				}
				else
				{
					gammas_n_.insert(gammas_n_.end(), gamma.begin(), gamma.end());

					omegasStructure_n_.push_back(omegasInfo);
					for (size_t i = 0; i < omega_su3.size(); ++i)
					{
						omegasSU3_n_.insert(omegasSU3_n_.end(), omega_su3[i].begin(), omega_su3[i].end());
					}
					for (size_t i = 0; i < omega_spin.size(); ++i)
					{
						omegasSpin_n_.insert(omegasSpin_n_.end(), omega_spin[i].begin(), omega_spin[i].end());
					}
				}
				gammasInfo.nGammas++;
			}
		}
		else	//	gamma = {a (lm mu)S} ==> omega = {1(lm mu)S}
		{
			SU3::LABELS ir(gamma[0].lm, gamma[0].mu);
			SU2::LABEL S2(gamma[0].S2);
			gammasInfo.nGammas++;
			if (type == nucleon::PROTON)
			{
				gammas_p_.insert(gammas_p_.end(), gamma.begin(), gamma.end());
				omegasStructure_p_.push_back(OmegasWithinGamma(0, 1, 0, 1));	// omega = {empty}
			}
			else
			{
				gammas_n_.insert(gammas_n_.end(), gamma.begin(), gamma.end());
				omegasStructure_n_.push_back(OmegasWithinGamma(0, 1, 0, 1));	//	omega = {empty}
			}
		}
  	}
}

/** JJ ... either Jcut of JJfixed */
SU3xSU2_VEC CncsmSU3Basis::GenerateFinal_SU3xSU2Irreps()
{
	unsigned char nmax = Nmax();
	unsigned char nhwProton, nhwNeutron, nhwTotal;
	std::vector<std::map<SU3xSU2::LABELS, unsigned long> > protonSU3Irreps(nmax + 1), neutronSU3Irreps(nmax + 1);

//	iterate over proton model space up to Nmax subspace and
//	for heach nhw subspace create a list of SU3xSU2 irreps
	for (IdentFermConfIterator protonConfIter = firstProtonConf(); protonConfIter.hasDistr(); protonConfIter.nextDistr())
	{
		nhwProton = protonConfIter.HOquanta() - Npmin_;
		if (nhwProton > nmax)
		{
			break;
		}
		for (protonConfIter.rewindGamma(); protonConfIter.hasGamma(); protonConfIter.nextGamma())
		{
			for (protonConfIter.rewindOmega(); protonConfIter.hasOmega(); protonConfIter.nextOmega())
			{
				SU3xSU2::LABELS ir(protonConfIter.getCurrentSU3xSU2());
				ir.rho = 1;
				protonSU3Irreps[nhwProton][ir] += protonConfIter.getMult();
			}
		}
	}

//	iterate over neutron model space up to Nmax subspace and
//	for heach nhw subspace create a list of SU3xSU2 irreps
	for (IdentFermConfIterator neutronConfIter = firstNeutronConf(); neutronConfIter.hasDistr(); neutronConfIter.nextDistr())
	{
		nhwNeutron = neutronConfIter.HOquanta() - Nnmin_;
		if (nhwNeutron > nmax)
		{
			break;
		}
//	if one chooses to split basis by omega_n. In such a case, one has to set
//	correct value for idiag when iterating over omega_n 
		for (neutronConfIter.rewindGamma(); neutronConfIter.hasGamma(); neutronConfIter.nextGamma())
		{
			for (neutronConfIter.rewindOmega(); neutronConfIter.hasOmega(); neutronConfIter.nextOmega())
			{
				SU3xSU2::LABELS ir(neutronConfIter.getCurrentSU3xSU2());
				ir.rho = 1;
				neutronSU3Irreps[nhwNeutron][ir] += neutronConfIter.getMult();
			}
		}
	}

	std::map<SU3xSU2::LABELS, unsigned long> finalSU3xSU2Irreps;	//	final SU3xSU2 irreps associated with the number of their occurances
//	iterate over nhw subspaces of proton-neutron model space	
	for (proton_neutron::ModelSpace::const_iterator nhwSubspace = nucleusModelSpace_.begin(); nhwSubspace < nucleusModelSpace_.end(); ++nhwSubspace)
	{
		nhwTotal = nhwSubspace->N();
		for (nhwProton = 0; nhwProton <= nhwTotal; ++nhwProton)
		{
			nhwNeutron = nhwTotal - nhwProton;
			std::map<SU3xSU2::LABELS, unsigned long>::const_iterator proton_irreps_end = protonSU3Irreps[nhwProton].end();
			std::map<SU3xSU2::LABELS, unsigned long>::const_iterator neutron_irreps_end = neutronSU3Irreps[nhwNeutron].end();
			for (std::map<SU3xSU2::LABELS, unsigned long>::const_iterator itp = protonSU3Irreps[nhwProton].begin(); itp != proton_irreps_end;  ++itp)
			{
				for (std::map<SU3xSU2::LABELS, unsigned long>::const_iterator itn = neutronSU3Irreps[nhwNeutron].begin(); itn != neutron_irreps_end;  ++itn)
				{
					unsigned long mult_p = itp->second;
					unsigned long mult_n = itn->second;

					SU2_VEC allowed_spin;
					std::vector<SU3_VEC> allowed_su3;

					SU3_VEC omega_su3;
//	for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
//	return vector of allowed SU3 labels for each allowed spin: 
//	{{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
					nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(itp->first.S2, itn->first.S2), allowed_spin, allowed_su3);
					size_t nallowed_spin = allowed_spin.size();

 //	{Sp Sn} is associated with an empty array of allowed spins ==> {Sp Sn} subspace is entirely out of model space
					if (!nallowed_spin)
					{
						continue;
					}

					SU3::Couple(itp->first, itn->first, omega_su3);
					SU3_VEC::const_iterator last_omega_su3 = omega_su3.end();
//	iterate over allowed spins ...
					for (size_t ispin = 0; ispin < nallowed_spin; ++ispin)
					{
						if (!allowed_su3[ispin].empty())	//	!empty ==> I need to select only SU(3) irreps listed in allowed_su3[ispin] = {(lm1 mu1), (lm2 mu2), ... , (lmn mun)} 
						{
							for (SU3_VEC::const_iterator current_su3 = omega_su3.begin(); current_su3 < last_omega_su3; ++current_su3)
							{
								SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
								if (current_allowed_su3 != allowed_su3[ispin].end() && (current_allowed_su3->lm == current_su3->lm && current_allowed_su3->mu == current_su3->mu))
								{
									unsigned long total_mult = mult_p * mult_n * current_su3->rho;
									finalSU3xSU2Irreps[SU3xSU2::LABELS(1, current_su3->lm, current_su3->mu, allowed_spin[ispin])] += total_mult;
								}
							}
						}
						else	//	if empty ==> all omega_su3 are allowed
						{
							for (SU3_VEC::const_iterator current_su3 = omega_su3.begin(); current_su3 < last_omega_su3; ++current_su3)
							{
								unsigned long total_mult = mult_p * mult_n * current_su3->rho;
								finalSU3xSU2Irreps[SU3xSU2::LABELS(1, current_su3->lm, current_su3->mu, allowed_spin[ispin])] += total_mult;
							}
						}
					}
				}
			}
		}
	}
//	create an ordered vector of final SU3xSU2 irreps and their basis states
	std::map<SU3xSU2::LABELS, unsigned long>::const_iterator last_irrep = finalSU3xSU2Irreps.end();
	std::map<SU3xSU2::LABELS, unsigned long>::const_iterator itFinalIrrep = finalSU3xSU2Irreps.begin();

	SU3xSU2_VEC irrep_list(finalSU3xSU2Irreps.size()); 
	for (size_t i = 0; itFinalIrrep != last_irrep; ++itFinalIrrep, ++i)
	{
		irrep_list[i] = itFinalIrrep->first;
	}
	return irrep_list;
}

void CncsmSU3Basis::ShowMemoryRequirements()
{
	size_t mem = 0;
	mem += sizeof(Distributions) + sizeof(unsigned char)*(distributions_p_.size() + distributions_n_.size());
	mem += sizeof(UN::SU3xSU2_VEC) + sizeof(UN::SU3xSU2)*(gammas_p_.size() + gammas_n_.size());
	mem += sizeof(std::vector<GammasWithinDistribution>) + sizeof(GammasWithinDistribution)*gammas_p_.size() + sizeof(UN::SU3xSU2)*gammas_n_.size();
	mem += sizeof(SU3_VEC) + sizeof(SU3::LABELS)*(omegasSU3_p_.size() + omegasSU3_n_.size());
	mem += sizeof(SU2_VEC) + sizeof(SU2::LABEL)*(omegasSpin_p_.size() + omegasSpin_n_.size());
	mem += sizeof(std::vector<OmegasWithinGamma>) + sizeof(OmegasWithinGamma)*(omegasStructure_p_.size() + omegasStructure_n_.size());
	cout << mem/(1024.0*1024.0) << " megabytes" << endl;
}

void CncsmSU3Basis::ShowProtonSU3xSU2Irreps()
{
	std::set<SU3xSU2::LABELS> allSU3xSU2;
	for (IdentFermConfIterator protonConfIter = firstProtonConf(); protonConfIter.hasDistr(); protonConfIter.nextDistr())
	{
		size_t nhwProton = protonConfIter.HOquanta() - Npmin_;
		if (nhwProton > Nmax())
		{
			assert(0); // if nhwProton > Nmax ==> wrong input parameter protonModelSpace
			break;
		}
		for (protonConfIter.rewindGamma(); protonConfIter.hasGamma(); protonConfIter.nextGamma())
		{
			for (protonConfIter.rewindOmega(); protonConfIter.hasOmega(); protonConfIter.nextOmega())
			{
				SU3xSU2::LABELS ir(protonConfIter.getCurrentSU3xSU2());
				ir.rho = 1;
				allSU3xSU2.insert(ir);
			}
		}
	}
	for(std::set<SU3xSU2::LABELS>::const_iterator ir = allSU3xSU2.begin(); ir != allSU3xSU2.end(); ++ir)
	{
		std::cout << *ir << std::endl;
	}
}

void CncsmSU3Basis::ShowNeutronSU3xSU2Irreps()
{
	std::set<std::pair<size_t, SU3xSU2::LABELS> > allSU3xSU2;
	for (IdentFermConfIterator confIter = firstNeutronConf(); confIter.hasDistr(); confIter.nextDistr())
	{
		size_t nhw = confIter.HOquanta() - Nnmin_;
		if (nhw > Nmax())
		{
			assert(0); // if nhwProton > Nmax ==> wrong input parameter protonModelSpace
			break;
		}
		for (confIter.rewindGamma(); confIter.hasGamma(); confIter.nextGamma())
		{
			for (confIter.rewindOmega(); confIter.hasOmega(); confIter.nextOmega())
			{
				SU3xSU2::LABELS ir(confIter.getCurrentSU3xSU2());
				ir.rho = 1;
				allSU3xSU2.insert(std::make_pair(nhw, ir));
			}
		}
	}
	for(std::set<std::pair<size_t, SU3xSU2::LABELS> >::const_iterator ir = allSU3xSU2.begin(); ir != allSU3xSU2.end(); ++ir)
	{
		std::cout << ir->first << " " << ir->second << std::endl;
	}
}
