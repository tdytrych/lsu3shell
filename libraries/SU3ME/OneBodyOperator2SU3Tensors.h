#ifndef ONE_BODY_OPERATOR_2_SU3_TENSORS_H
#define ONE_BODY_OPERATOR_2_SU3_TENSORS_H
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <su3.h>

//	Performs decomposition of a general one-body operator into SU(3) tensors.
//	It does not assume that the operator is scalar, (i.e. L0 == S0).
//	Output:
//
//	n1 n2 0          
//	#tensors
//	lm0 mu0 L0 S0 J0	...	NOTE: we do not assume L0 == S0 and hence need to save both quantum labels
//	a_pp^k0=1, a_nn^k0=1, a_pp^k0=2, a_nn^k0=2, ... a_pp^k0max, a_nn^k0max
//	lm0' mu0' L0' S0' J0'
//	a'_pp^k0=1, a'_nn^k0=1, a'_pp^k0=2, a'_nn^k0=2, ... a'_pp^k0max, a'_nn^k0max
//	.
//	.
//	.
template <class COneBodyMatrixElements>	
class COneBodyOperators2SU3Tensors
{
	public:
	COneBodyOperators2SU3Tensors(const COneBodyMatrixElements& oneBodyME, const int maxShell): oneBodyME_(oneBodyME), maxShell_(maxShell) {}
	void PerformSU3Decomposition(const std::string& sOutputFileName);
	void PerformSU3Decomposition_TensorWithSingleJ0M0(const std::string& sOutputFileName);
	private:
	enum TensorLabels {eLM0 = 0, eMU0 = 1, eS0 = 2, eL0 = 3, eJ0 = 4, eM0 = 5};	// (eLM0 eMU0) == (lm0 mu0)
	inline void GenerateAllCombinationsHOShells(std::vector<std::pair<int, int> >& nanbCombinations)
	{
		for (int na = 0; na <= maxShell_; ++na)
		{
			for (int nb = 0; nb <= maxShell_; ++nb)
			{
				nanbCombinations.push_back(std::make_pair(na,nb));
			}
		}
	}
	void GetCoefficients(const int na, const int nb, const std::vector<std::pair<K1L1K2L2K3, std::vector<double> > >& vCGs, const int k0, const int L0, const int S0, const int J0, const int M0, std::vector<double>& vCoeffs);

	const COneBodyMatrixElements& oneBodyME_;
	const size_t maxShell_;
};


template <class COneBodyMatrixElements>	
void COneBodyOperators2SU3Tensors<COneBodyMatrixElements>::GetCoefficients(	const int na, const int nb, const std::vector<std::pair<K1L1K2L2K3, std::vector<double> > >& vCGs, 
																			const int k0, const int L0, const int S0, const int J0, const int M0, 
																			std::vector<double>& vCoeffs)
{
	std::vector<double> vPositiveCoeffs(vCoeffs.size(), 0.0);
	std::vector<double> vNegativeCoeffs(vCoeffs.size(), 0.0);

	assert(vCoeffs.size() == 2);

//	iterate over la lb
	for (size_t ilalb = 0; ilalb < vCGs.size(); ++ilalb)
	{
		K1L1K2L2K3 Key = vCGs[ilalb].first;
		if (k0 != Key.k3()) {
			continue;
		}

		assert(vCGs[ilalb].first.k1()+1 == 1);
		assert(vCGs[ilalb].first.k2()+1 == 1);
		assert(vCGs[ilalb].second.size() == 1);	//	irA x irB --> rhomax = 1 ir0

		double dsu3cg = vCGs[ilalb].second[0];	//	dsu3cg = < (na 0) la; (0 nb) lb || (lm0 mu0) 1 L0>
		if (Negligible7(dsu3cg))	//	if |dsu3cg| < 1.e-7
		{
			continue;
		}

		int la = Key.l1();
		int lb = Key.l2();

		if (!SU2::mult(la, lb, L0))
		{
			std::cout << la << "x" << lb << "->" << L0 << std::endl;
		}

		assert(SU2::mult(la, lb, L0));

		for (int ja = abs(la - 1); ja <= la + 1; ja += 2)
		{
			for (int jb = abs(lb - 1); jb <= lb + 1; jb += 2)
			{
				if (!SU2::mult(ja, jb, J0))
				{
					continue;
				}
				
				double dsu2 = std::sqrt((ja + 1)*(jb + 1))*wigner9j(la, lb, L0, 1, 1, S0, ja, jb, J0);
				if (Negligible7(dsu2))	//	if |dsu3cg| < 1.e-7
				{
					continue;
				}

				for (int ma = -ja; ma <= ja; ma += 2)
				{
					int mb = ma - M0;
					if (mb < -jb || mb > +jb)
					{
						continue;
					}
					//	we have a single operator ==> mePP has either double or float data type 
					typename COneBodyMatrixElements::MatrixElementValue mePP(0.0);
					bool bPPMe = oneBodyME_.Me(na, la, ja, ma, nb, lb, jb, mb, PP, mePP);

					typename COneBodyMatrixElements::MatrixElementValue meNN(0.0);
					bool bNNMe = oneBodyME_.Me(na, la, ja, ma, nb, lb, jb, mb, NN, meNN);

					if (bPPMe || bNNMe)
					{
						double phase = dsu3cg*dsu2*MINUSto((-jb + mb)/2)*clebschGordan(ja, ma, jb, -mb, J0, M0);
						double dPP = phase*mePP;
						double dNN = phase*meNN;

						if (dPP > 0)
						{
							vPositiveCoeffs[PP] += dPP;
						}
						else
						{
							vNegativeCoeffs[PP] += dPP;
						}

						if (dNN > 0)
						{
							vPositiveCoeffs[NN] += dNN;
						}
						else
						{
							vNegativeCoeffs[NN] += dNN;
						}
					}
				}
			}
		}
	}
	vCoeffs[PP] = vPositiveCoeffs[PP] + vNegativeCoeffs[PP];
	vCoeffs[NN] = vPositiveCoeffs[NN] + vNegativeCoeffs[NN];
}

//	Perform decomposition of one-body operator, but in this case values of J0
//	and M0 are fixed and are not assumed to be equal. They are obtained using
//	template class parameter method COneBodyMatrixElements::JJ0() and
//	COneBodyMatrixElements::MM0()
//
//	[a+ x a]^(lm0 mu0)_{S0 L0 J0 M0}
template <class COneBodyMatrixElements>	
void COneBodyOperators2SU3Tensors<COneBodyMatrixElements>::PerformSU3Decomposition_TensorWithSingleJ0M0(const std::string& sOutputFileName)
{
	std::ofstream outfile(sOutputFileName.c_str());
   outfile << oneBodyME_.JJ0() << " " << oneBodyME_.MM0() << std::endl;
	if (!outfile)
	{
		std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
		exit(EXIT_FAILURE);
	}
	outfile.precision(10);

	std::vector<std::pair<int, int> > nanbCombinations;
	GenerateAllCombinationsHOShells(nanbCombinations);	// generate all allowed combinations of na and nb in [a^+(na 0) x a^(0 nb)]_{S0}

   su3::init();
	CSU3CGMaster SU3CGFactory;
	const int S0[2] = {0, 2};	//	one-body operator ==> S0 = 0 or S0 = 1
	SU2::LABEL JJ0 = oneBodyME_.JJ0();
	int na, nb;

	int structure[3] = {0, 0, 0};
	SU3::LABELS irA, irB;
//	iterate over na nb 
	for (size_t inanb = 0; inanb < nanbCombinations.size(); ++inanb)
	{
		na = (nanbCombinations[inanb]).first;
		nb = (nanbCombinations[inanb]).second;
		irA = SU3::LABELS(na, 0);
		irB = SU3::LABELS(0, nb);
		structure[0] = (na + 1);	// a^+	===> n + 1
		structure[1] = -1*(nb + 1);	// a	===> -n - 1;	

		SU3_VEC Irreps0;
		SU3::Couple(irA, irB, Irreps0);

		std::vector<std::pair<std::vector<double>, std::vector<int> > > nanbTensorDecomposition;
		std::vector<int> TensorLabels(5, 0);

		TensorLabels[eJ0] = oneBodyME_.JJ0();
		TensorLabels[eM0] = oneBodyME_.MM0();


//	iterate over rho0==1 (lm0 mu0)
		for (SU3_VEC::iterator ir0 = Irreps0.begin(); ir0 != Irreps0.end(); ++ir0)
		{
			TensorLabels[eLM0] = ir0->lm;
			TensorLabels[eMU0] = ir0->mu;

			assert(ir0->rho == 1);
			int max2L0 = 2*(ir0->lm + ir0->mu);	//	 2*maxL0, where maxL0 = lm0 + mu0
//	iterate over S0
			for (int iS0 = 0; iS0 <= 1; ++iS0)
			{
				TensorLabels[eS0] = S0[iS0];

				double su3xsu2phase = MINUSto(nb)*std::sqrt(S0[iS0] + 1);

				if (na > nb)	//	==> na <--> nb ==> apply relation (A.49):
				{				//	[a^+(na 0) x a^(0 nb)]^(lm0 mu0)_{*} = (-)(-)^{na + 0 + 0 + nb - lm0 - mu0 + 1/2 + 1/2 - S0} [a^(0 nb) x a^+(na 0)]^(lm0 mu0)_{*} 
					su3xsu2phase *= (-1)*MINUSto(na + nb - ir0->lm - ir0->mu + 1 - S0[iS0]/2);
				}
//	iterate over L0
				for (int L0 = 0; L0 <= max2L0; L0 += 2)
				{
					if (JJ0 < abs(L0 - S0[iS0]) || JJ0 > (L0 + S0[iS0]) )
					{
						continue;
					}

					TensorLabels[eL0] = L0;

					double so3phase = std::sqrt(L0 + 1);
					double totalphase = su3xsu2phase*so3phase;
					int k0_max = SU3::kmax(*ir0, L0/2);
					if (!k0_max) {
						continue;
					}
					std::vector<std::pair<K1L1K2L2K3, std::vector<double> > > vCGs;
				//	get SU(3) Wigner coefficients of type: <(na 0) 1 *; (0 nb) 1 *|| (lm0 mu0) * L0>_rho0=1
					SU3CGFactory.GetSO3(irA, irB, *ir0, L0, vCGs);
			
					std::vector<double> ResultingCoeffs(2*k0_max, 0.0);	//	index = k0*2 + type 
					for (int k0 = 0, index = 0; k0 < k0_max; ++k0, index += 2)
					{
						std::vector<double> vCoeffs(2); 
//	M0 = J0	!!!!
						GetCoefficients(na, nb, vCGs, k0, L0, S0[iS0], JJ0, oneBodyME_.MM0(), vCoeffs);

						ResultingCoeffs[index + PP] = totalphase * vCoeffs[PP];
						ResultingCoeffs[index + NN] = totalphase * vCoeffs[NN];
					}
					if (std::count_if(ResultingCoeffs.begin(), ResultingCoeffs.end(), Negligible) == ResultingCoeffs.size()) {
						continue;
					}
					nanbTensorDecomposition.push_back(std::make_pair(ResultingCoeffs, TensorLabels));
				}
			}
		}

		if (!nanbTensorDecomposition.empty())
		{
			if (abs(structure[0]) > abs(structure[1]))	//	in this case we done with na nb case ==> I can exchange structure ...
			{
				std::swap(structure[0], structure[1]);
			}

			outfile << structure[0] << " " << structure[1] << std::endl;
			outfile << nanbTensorDecomposition.size() << std::endl;
			for (size_t i = 0; i < nanbTensorDecomposition.size(); ++i)
			{
				outfile << nanbTensorDecomposition[i].second[eLM0] << " ";		// lm0
				outfile << nanbTensorDecomposition[i].second[eMU0] << " ";		// mu0
				outfile << nanbTensorDecomposition[i].second[eS0]  << " ";		//	S0
				outfile << nanbTensorDecomposition[i].second[eL0]  << " ";		// L0
				outfile << nanbTensorDecomposition[i].second[eJ0]  << std::endl;// J0
				for (size_t j = 0; j < nanbTensorDecomposition[i].first.size(); ++j)
				{
					outfile << nanbTensorDecomposition[i].first[j] << " ";
				}
				outfile << std::endl;
			}
		}
	}
	outfile.close();
   su3::finalize();
}


//	Perform decomposition of one-body operator into
//
//	[a+ x a]^(lm0 mu0)_{S0 L0 J0 M0 = J0}
//
//	considering all physically allowed values n1 n2 (lm0 mu0) S0 L0 J0.
//
//	NOTE: M0 == J0  !!!
//	
template <class COneBodyMatrixElements>	
void COneBodyOperators2SU3Tensors<COneBodyMatrixElements>::PerformSU3Decomposition(const std::string& sOutputFileName)
{
	std::ofstream outfile(sOutputFileName.c_str());
	if (!outfile)
	{
		std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
		exit(EXIT_FAILURE);
	}
	outfile.precision(10);

	std::vector<std::pair<int, int> > nanbCombinations;
	GenerateAllCombinationsHOShells(nanbCombinations);	// generate all allowed combinations of na and nb in [a^+(na 0) x a^(0 nb)]_{S0}

	CSU3CGMaster SU3CGFactory;
	const int S0[2] = {0, 2};	//	one-body operator ==> S0 = 0 or S0 = 1
	int na, nb;
	int structure[3] = {0, 0, 0};
	SU3::LABELS irA, irB;
//	iterate over na nb 
	for (size_t inanb = 0; inanb < nanbCombinations.size(); ++inanb)
	{
		na = (nanbCombinations[inanb]).first;
		nb = (nanbCombinations[inanb]).second;
		irA = SU3::LABELS(na, 0);
		irB = SU3::LABELS(0, nb);
		structure[0] = (na + 1);	// a^+	===> n + 1
		structure[1] = -1*(nb + 1);	// a	===> -n - 1;	

		SU3_VEC Irreps0;
		SU3::Couple(irA, irB, Irreps0);

		std::vector<std::pair<std::vector<double>, std::vector<int> > > nanbTensorDecomposition;
		std::vector<int> TensorLabels(5, 0);
//	iterate over rho0==1 (lm0 mu0)
		for (SU3_VEC::iterator ir0 = Irreps0.begin(); ir0 != Irreps0.end(); ++ir0)
		{
			TensorLabels[eLM0] = ir0->lm;
			TensorLabels[eMU0] = ir0->mu;

			assert(ir0->rho == 1);
			int max2L0 = 2*(ir0->lm + ir0->mu);	//	 2*maxL0, where maxL0 = lm0 + mu0
//	iterate over S0
			for (int iS0 = 0; iS0 <= 1; ++iS0)
			{
				TensorLabels[eS0] = S0[iS0];

				double su3xsu2phase = MINUSto(nb)*std::sqrt(S0[iS0] + 1);

				if (na > nb)	//	==> na <--> nb ==> apply relation (A.49):
				{				//	[a^+(na 0) x a^(0 nb)]^(lm0 mu0)_{*} = (-)(-)^{na + 0 + 0 + nb - lm0 - mu0 + 1/2 + 1/2 - S0} [a^(0 nb) x a^+(na 0)]^(lm0 mu0)_{*} 
					su3xsu2phase *= (-1)*MINUSto(na + nb - ir0->lm - ir0->mu + 1 - S0[iS0]/2);
				}
//	iterate over L0
				for (int L0 = 0; L0 <= max2L0; L0 += 2)
				{
					TensorLabels[eL0] = L0;

					double so3phase = std::sqrt(L0 + 1);
					double totalphase = su3xsu2phase*so3phase;
					int k0_max = SU3::kmax(*ir0, L0/2);
					if (!k0_max) {
						continue;
					}
					std::vector<std::pair<K1L1K2L2K3, std::vector<double> > > vCGs;
				//	get SU(3) Wigner coefficients of type: <(na 0) 1 *; (0 nb) 1 *|| (lm0 mu0) * L0>_rho0=1
					SU3CGFactory.GetSO3(irA, irB, *ir0, L0, vCGs);
			
					int J0min = abs(L0 - S0[iS0]);
					int J0max = L0 + S0[iS0];
//	iterate over all possible values of J0 					
					for (int J0 = J0min; J0 <= J0max; J0 += 2)
					{
						TensorLabels[eJ0] = J0;

						std::vector<double> ResultingCoeffs(2*k0_max, 0.0);	//	index = k0*2 + type 
						for (int k0 = 0, index = 0; k0 < k0_max; ++k0, index += 2)
						{
							std::vector<double> vCoeffs(2); 
//	M0 = J0	!!!!
							GetCoefficients(na, nb, vCGs, k0, L0, S0[iS0], J0, J0, vCoeffs);

							ResultingCoeffs[index + PP] = totalphase * vCoeffs[PP];
							ResultingCoeffs[index + NN] = totalphase * vCoeffs[NN];
						}
						if (std::count_if(ResultingCoeffs.begin(), ResultingCoeffs.end(), Negligible) == ResultingCoeffs.size()) {
							continue;
						}
						nanbTensorDecomposition.push_back(std::make_pair(ResultingCoeffs, TensorLabels));
					}
				}
			}
		}

		if (!nanbTensorDecomposition.empty())
		{
			if (abs(structure[0]) > abs(structure[1]))	//	in this case we done with na nb case ==> I can exchange structure ...
			{
				std::swap(structure[0], structure[1]);
			}

			outfile << structure[0] << " " << structure[1] << std::endl;
			outfile << nanbTensorDecomposition.size() << std::endl;
			for (size_t i = 0; i < nanbTensorDecomposition.size(); ++i)
			{
				outfile << nanbTensorDecomposition[i].second[eLM0] << " ";		// lm0
				outfile << nanbTensorDecomposition[i].second[eMU0] << " ";		// mu0
				outfile << nanbTensorDecomposition[i].second[eS0]  << " ";		//	S0
				outfile << nanbTensorDecomposition[i].second[eL0]  << " ";		// L0
				outfile << nanbTensorDecomposition[i].second[eJ0]  << std::endl;// J0
				for (size_t j = 0; j < nanbTensorDecomposition[i].first.size(); ++j)
				{
					outfile << nanbTensorDecomposition[i].first[j] << " ";
				}
				outfile << std::endl;
			}
		}
	}
	outfile.close();
}
#endif
