#include "memory_pool.h"

#include <SU3ME/CRMECalculator.h>
#include <SU3ME/RME.h>

thread_local boost::pool<> memory_pool::rme(sizeof(SU3xSU2::RME));

thread_local boost::pool<> memory_pool::crmecalculator(sizeof(CRMECalculator));
