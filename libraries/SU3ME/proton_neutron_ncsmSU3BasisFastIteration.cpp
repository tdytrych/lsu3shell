#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>

struct Compare_UNSU3xSU2_VEC
{
	struct Compare_UNSU3xSU2
	{
		inline bool operator() (const UN::SU3xSU2& a, const UN::SU3xSU2& b) const 
		{ 
			return (a.lm < b.lm || (((a.lm == b.lm) && a.mu < b.mu) || ((a.lm == b.lm && a.mu == b.mu && a.S2 < b.S2) || (a.lm == b.lm && a.mu == b.mu && a.S2 == b.S2 && a.mult < b.mult))));
		}
	};
	inline bool operator() (const UN::SU3xSU2_VEC& a, const UN::SU3xSU2_VEC& b) const { return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), Compare_UNSU3xSU2()); }
};

struct Compare_SU3_VEC
{
//	SU3::LABELS::operator< does not compare rho maximal multiplicity!
//	and hence I have to define my own operator< which takes into account
//	also multiplicities: Compare_SU3_With_Rho
	struct Compare_SU3_With_Rho
	{
		inline bool operator() (const SU3::LABELS& lhs, const SU3::LABELS& rhs) const {return (lhs.lm < rhs.lm || (((lhs.lm == rhs.lm) && lhs.mu < rhs.mu) || ((lhs.lm == rhs.lm && lhs.mu == rhs.mu && lhs.rho < rhs.rho)))); }
	};
	inline bool operator() (const SU3_VEC& a, const SU3_VEC& b) const { return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), Compare_SU3_With_Rho()); }
};

unsigned short initValue[4] = {0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};


void CncsmSU3BasisFastIteration::GenerateDistrGammaOmega_SortedVectors(	const CncsmSU3Basis& Basis, unsigned int idiag, unsigned int ndiag,
																		std::vector<SingleDistributionSmallVector>& distribs_p,
																		std::vector<SingleDistributionSmallVector>& distribs_n,
																		std::vector<UN::SU3xSU2_VEC>& gammas,
																		std::vector<SU3_VEC>& omegas_su3,
																		std::vector<SU2_VEC>& omegas_spin)
{
	std::set<SingleDistributionSmallVector> distribs_p_set, distribs_n_set;
	std::set<UN::SU3xSU2_VEC, Compare_UNSU3xSU2_VEC> gammas_set;
	std::set<SU3_VEC, Compare_SU3_VEC> omegas_su3_set;
	std::set<SU2_VEC> omegas_spin_set;

	unsigned char status;
	SingleDistributionSmallVector distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_SMALL_VEC omega_p, omega_n;

//	In order to make sure that vectors of gammas, omegas_su3, and omegas_spin
//	contain the same values regardless how many segments basis has, I have to
//	iterate over all possible configurations while building these vectors.
	PNConfIterator iter = Basis.firstPNConf(0, 1);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		status = iter.status();

		if (status == PNConfIterator::kNewDistr_p)
		{
			distr_p.resize(0); 
			iter.getDistr_p(distr_p);
			distribs_p_set.insert(distr_p);
		}
		if (status >= PNConfIterator::kNewGamma_p)
		{
			gamma_p.resize(0); 
			iter.getGamma_p(gamma_p);
			gammas_set.insert(gamma_p);
		}
		if (status >= PNConfIterator::kNewOmega_p)
		{
			omega_p.resize(0); 
			iter.getOmega_p(omega_p);
//			assert(omega_p.back().rho == 1);
			omegas_su3_set.insert(SU3_VEC(omega_p.begin(), omega_p.end())); 
			omegas_spin_set.insert(SU2_VEC(omega_p.begin(), omega_p.end()));
		}

		if (status >= PNConfIterator::kNewDistr_n)
		{
			distr_n.resize(0); 
			iter.getDistr_n(distr_n);
			distribs_n_set.insert(distr_n);
		}
		if (status >= PNConfIterator::kNewGamma_n)
		{
			gamma_n.resize(0); 
			iter.getGamma_n(gamma_n);
			gammas_set.insert(gamma_n);
		}
		if (status >= PNConfIterator::kNewOmega_n)
		{
			omega_n.resize(0); 
			iter.getOmega_n(omega_n);
			omegas_su3_set.insert(SU3_VEC(omega_n.begin(), omega_n.end())); 
			omegas_spin_set.insert(SU2_VEC(omega_n.begin(), omega_n.end()));
		}
	}

	assert(distribs_p.empty());
	assert(distribs_n.empty());
	assert(gammas.empty());
	assert(omegas_spin.empty());
	assert(omegas_su3.empty());

	distribs_p  .insert(distribs_p.end() , distribs_p_set.begin() , distribs_p_set.end());
	distribs_n  .insert(distribs_n.end() , distribs_n_set.begin() , distribs_n_set.end());
	gammas		.insert(gammas.end()     , gammas_set.begin()     , gammas_set.end());
	omegas_su3  .insert(omegas_su3.end() , omegas_su3_set.begin() , omegas_su3_set.end());
	omegas_spin .insert(omegas_spin.end(), omegas_spin_set.begin(), omegas_spin_set.end());
}


// construct: distribs_p_, distribs_n_, gammas_, omegas_su3_, omegas_spin_, proton_basis_, neutron_basis_
void CncsmSU3BasisFastIteration::Generate_ProtonBasis_NeutronBasis(const CncsmSU3Basis& basis, unsigned int idiag, unsigned int ndiag)
{
//	Create sorted vector of proton distributions (distribs_p_), neutron
//	distributions (distribs_n_), all unique gammas, all unique omega_su3 and spins
	GenerateDistrGammaOmega_SortedVectors(basis, idiag, ndiag, distribs_p_, distribs_n_, gammas_, omegas_su3_, omegas_spin_);

//	NUCLEON_BASIS_INDICES ... CTuple<unsigned short, 4> 
//	Construct Proton (Neutron) basis as sorted vectors of CTuple<unsigned short, 4> = {kDistr, kGamma, kOmegaSu3, kOmegaSpin}.
//	First use std::set to construct a set of unique NUCLEON_BASIS_INDICES for protons and neutrons 
//	then copy that std::set into a vector
	std::set<NUCLEON_BASIS_INDICES> proton_basis_set;
	std::set<NUCLEON_BASIS_INDICES> neutron_basis_set;
	NUCLEON_BASIS_INDICES  new_index_p, last_index_p(initValue);
	NUCLEON_BASIS_INDICES  new_index_n, last_index_n(initValue);
	
	unsigned char status;
	SingleDistributionSmallVector distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_SMALL_VEC omega_p, omega_n;
	PNConfIterator iter = basis.firstPNConf(idiag, ndiag);

	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
//	based on status generate a new NUCLEON_BASIS_INDICES for proton and neutron part of many-body state		
		status = iter.status();
		if (status == PNConfIterator::kNewPNOmega)
		{
			continue;
		}

		if (status == PNConfIterator::kNewDistr_p)
		{
			distr_p.resize(0); 
			iter.getDistr_p(distr_p);

			new_index_p[kDistr] = std::distance(distribs_p_.begin(), std::lower_bound(distribs_p_.begin(), distribs_p_.end(), distr_p));
		}
		if (status >= PNConfIterator::kNewDistr_n)
		{
			distr_n.resize(0); 
			iter.getDistr_n(distr_n);

			new_index_n[kDistr] = std::distance(distribs_n_.begin(), std::lower_bound(distribs_n_.begin(), distribs_n_.end(), distr_n));
		}

		if (status >= PNConfIterator::kNewGamma_p)
		{
			gamma_p.resize(0); 
			iter.getGamma_p(gamma_p);

			new_index_p[kGamma] = std::distance(gammas_.begin(), std::lower_bound(gammas_.begin(), gammas_.end(), gamma_p, Compare_UNSU3xSU2_VEC()));
		}

		if (status >= PNConfIterator::kNewGamma_n)
		{
			gamma_n.resize(0); 
			iter.getGamma_n(gamma_n);
			
			new_index_n[kGamma] = std::distance(gammas_.begin(), std::lower_bound(gammas_.begin(), gammas_.end(), gamma_n, Compare_UNSU3xSU2_VEC()));
		}

		if (status >= PNConfIterator::kNewOmega_p)
		{
			omega_p.resize(0); 
			iter.getOmega_p(omega_p);

			new_index_p[kOmegaSu3] = std::distance(omegas_su3_.begin(), std::lower_bound(omegas_su3_.begin(), omegas_su3_.end(), SU3_VEC(omega_p.begin(), omega_p.end()), Compare_SU3_VEC()));
			new_index_p[kOmegaSpin] = std::distance(omegas_spin_.begin(), std::lower_bound(omegas_spin_.begin(), omegas_spin_.end(), SU2_VEC(omega_p.begin(), omega_p.end())));
		}
		if (status >= PNConfIterator::kNewOmega_n)
		{
			omega_n.resize(0); 
			iter.getOmega_n(omega_n);

			new_index_n[kOmegaSu3] = std::distance(omegas_su3_.begin(), std::lower_bound(omegas_su3_.begin(), omegas_su3_.end(), SU3_VEC(omega_n.begin(), omega_n.end()), Compare_SU3_VEC()));
			new_index_n[kOmegaSpin] = std::distance(omegas_spin_.begin(), std::lower_bound(omegas_spin_.begin(), omegas_spin_.end(), SU2_VEC(omega_n.begin(), omega_n.end())));
		}

		if (new_index_p != last_index_p)
		{
			proton_basis_set.insert(new_index_p);
			last_index_p = new_index_p;
		}
		if (new_index_n != last_index_n)
		{
			neutron_basis_set.insert(new_index_n);
			last_index_n = new_index_n;
		}
	}
	
	proton_basis_.reserve(proton_basis_set.size());
	proton_basis_.insert(proton_basis_.end(), proton_basis_set.begin(), proton_basis_set.end());
/*
	for (std::set<NUCLEON_BASIS_INDICES>::iterator it = proton_basis_set.begin(); it != proton_basis_set.end(); ++it)
	{
		NUCLEON_BASIS_INDICES indices;

		indices[0] = (*it)[0];
		indices[1] = (*it)[1];
		indices[2] = (*it)[2];
		indices[3] = (*it)[3];
		proton_basis_.push_back(indices);
	}
*/	
	neutron_basis_.reserve(neutron_basis_set.size());
	neutron_basis_.insert(neutron_basis_.end(), neutron_basis_set.begin(), neutron_basis_set.end());
/*	
	for (std::set<NUCLEON_BASIS_INDICES>::iterator it = neutron_basis_set.begin(); it != neutron_basis_set.end(); ++it)
	{
		NUCLEON_BASIS_INDICES indices;

		indices[0] = (*it)[0];
		indices[1] = (*it)[1];
		indices[2] = (*it)[2];
		indices[3] = (*it)[3];
		neutron_basis_.push_back(indices);
	}
*/	
}

CncsmSU3BasisFastIteration::CncsmSU3BasisFastIteration(const proton_neutron::ModelSpace& ncsmModelSpace, const int idiag, const int ndiag): idiag_(idiag), dims_(ndiag, 0)
{
//	Create ncsm SU3xSU2 basis that constructs basis on-the-fly
	CncsmSU3Basis basis(ncsmModelSpace);
//	Create a look up table of all SU(3)xSU(2) irreps in a given model space and
//	their basis states.
	wpn_irreps_container_.CreateContainer(basis.GenerateFinal_SU3xSU2Irreps(), ncsmModelSpace.JJ());

// construct: distribs_p_, distribs_n_, gammas_, omegas_su3_, omegas_spin_, proton_basis_, neutron_basis_
	Generate_ProtonBasis_NeutronBasis(basis, idiag, ndiag);
	
	NUCLEON_BASIS_INDICES  new_index_p, last_index_p(initValue);
	NUCLEON_BASIS_INDICES  new_index_n, last_index_n(initValue);

	PROTON_NEUTRON_BASIS_INDICES indices_pn_basis;

//	Calculate dimension (including J quantum labels) of the basis
	CalculateDimAllSections(basis, dims_, ndiag, wpn_irreps_container_);

	SingleDistributionSmallVector distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_SMALL_VEC omega_p, omega_n;
	SU3xSU2::LABELS omega_pn;
	unsigned char status;

	PNConfIterator iter = basis.firstPNConf(idiag, ndiag);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
//	construct indices for proton and neutron configurations in the current
//	SU(3)xSU(2) many-body basis state		
		status = iter.status();
		//	I need to obtain omega_pn.rho, since all wpn_irreps_container_ elements have rho = 1
		omega_pn = iter.getCurrentSU3xSU2();

		if (status == PNConfIterator::kNewDistr_p)
		{
			distr_p.resize(0); 
			iter.getDistr_p(distr_p);
			new_index_p[kDistr] = std::distance(distribs_p_.begin(), std::lower_bound(distribs_p_.begin(), distribs_p_.end(), distr_p));
		}
		if (status >= PNConfIterator::kNewDistr_n)
		{
			distr_n.resize(0); 
			iter.getDistr_n(distr_n);
			new_index_n[kDistr] = std::distance(distribs_n_.begin(), std::lower_bound(distribs_n_.begin(), distribs_n_.end(), distr_n));
		}

		if (status >= PNConfIterator::kNewGamma_p)
		{
			gamma_p.resize(0); 
			iter.getGamma_p(gamma_p);
			new_index_p[kGamma] = std::distance(gammas_.begin(), std::lower_bound(gammas_.begin(), gammas_.end(), gamma_p, Compare_UNSU3xSU2_VEC()));
		}

		if (status >= PNConfIterator::kNewGamma_n)
		{
			gamma_n.resize(0); 
			iter.getGamma_n(gamma_n);
			new_index_n[kGamma] = std::distance(gammas_.begin(), std::lower_bound(gammas_.begin(), gammas_.end(), gamma_n, Compare_UNSU3xSU2_VEC()));
		}

		if (status >= PNConfIterator::kNewOmega_p)
		{
			omega_p.resize(0); 
			iter.getOmega_p(omega_p);

			new_index_p[kOmegaSu3] = std::distance(omegas_su3_.begin(), std::lower_bound(omegas_su3_.begin(), omegas_su3_.end(), SU3_VEC(omega_p.begin(), omega_p.end()), Compare_SU3_VEC()));
			new_index_p[kOmegaSpin] = std::distance(omegas_spin_.begin(), std::lower_bound(omegas_spin_.begin(), omegas_spin_.end(), SU2_VEC(omega_p.begin(), omega_p.end())));
		}
		if (status >= PNConfIterator::kNewOmega_n)
		{
			omega_n.resize(0); 
			iter.getOmega_n(omega_n);

			new_index_n[kOmegaSu3] = std::distance(omegas_su3_.begin(), std::lower_bound(omegas_su3_.begin(), omegas_su3_.end(),    SU3_VEC(omega_n.begin(), omega_n.end()),  Compare_SU3_VEC()));
			new_index_n[kOmegaSpin] = std::distance(omegas_spin_.begin(), std::lower_bound(omegas_spin_.begin(), omegas_spin_.end(), SU2_VEC(omega_n.begin(), omega_n.end())));
		}

		if (new_index_p != last_index_p)
		{
			// get index of table which holds {idistr, igamma, iomega_su3, iomega_spin} indices of proton many-body configuration 
			indices_pn_basis[kProtonBasisIndex] = std::distance(proton_basis_.begin(), std::lower_bound(proton_basis_.begin(), proton_basis_.end(), new_index_p));
			last_index_p = new_index_p;
		}
		if (new_index_n != last_index_n)
		{
			// get index of table which holds {idistr, igamma, iomega_su3, iomega_spin} indices of neutron many-body configuration 
			indices_pn_basis[kNeutronBasisIndex] = std::distance(neutron_basis_.begin(), std::lower_bound(neutron_basis_.begin(), neutron_basis_.end(), new_index_n));
			last_index_n = new_index_n;
		}

		// use multiplicity rho and the index of SU3xSU2 representation basis in wp_irreps storage to construct the index
		indices_pn_basis[kRhoSU3xSU2Index] = KEY_INTEGER(omega_pn.rho, wpn_irreps_container_.getIndexInTable(omega_pn));
		//	only irreps with non zero of basis states are added into pn_basis_
		if (wpn_irreps_container_[wpn_irreps_container_.getIndexInTable(omega_pn)].dim() > 0)
		{
			pn_basis_.push_back(indices_pn_basis);
		}
	}
}

SU3xSU2::LABELS CncsmSU3BasisFastIteration::getNeutronSU3xSU2(unsigned int current_index) const
{
	unsigned int neutron_index = neutronConfIndex(current_index);
	const SU3_VEC& omega_n_su3 = omegas_su3_[neutron_basis_[neutron_index][kOmegaSu3]];
	if (omega_n_su3.empty())
	{
		const UN::SU3xSU2_VEC* gamma_n = getGamma_n(current_index);
		assert(gamma_n->size() == 1);
		return SU3xSU2::LABELS(1, (*gamma_n)[0].lm, (*gamma_n)[0].mu, (*gamma_n)[0].S2);
	}
	else
	{
		return SU3xSU2::LABELS(omegas_su3_[neutron_basis_[neutron_index][kOmegaSu3]].back(), omegas_spin_[neutron_basis_[neutron_index][kOmegaSpin]].back());
	}
}

SU3xSU2::LABELS CncsmSU3BasisFastIteration::getProtonSU3xSU2(unsigned int current_index) const
{
	unsigned int proton_index = protonConfIndex(current_index);
	const SU3_VEC& omega_p_su3 = omegas_su3_[proton_basis_[proton_index][kOmegaSu3]];
	if (omega_p_su3.empty())	// This happens when all the protons are located on one shell
	{
		const UN::SU3xSU2_VEC* gamma_p = getGamma_p(current_index);
		assert(gamma_p->size() == 1);
		return SU3xSU2::LABELS(1, (*gamma_p)[0].lm, (*gamma_p)[0].mu, (*gamma_p)[0].S2);
	}
	else
	{
		return SU3xSU2::LABELS(omega_p_su3.back(), omegas_spin_[proton_basis_[proton_index][kOmegaSpin]].back());
	}
}

void CncsmSU3BasisFastIteration::getOmega_p(unsigned int current_index, SU3xSU2_VEC& omega_p) const
{
	assert(omega_p.empty());

	const SU3_VEC* omega_su3 = getOmega_p_Su3(current_index);
	const SU2_VEC* omega_spin = getOmega_p_Spin(current_index);

	assert(omega_su3->size() == omega_spin->size());
		
	omega_p.resize(omega_spin->size());
	for (int i = 0; i < omega_spin->size(); ++i)
	{
		omega_p[i].rho = (*omega_su3)[i].rho;
		omega_p[i].lm  = (*omega_su3)[i].lm;
		omega_p[i].mu  = (*omega_su3)[i].mu;
		omega_p[i].S2  = (*omega_spin)[i]; 
	}
}

void CncsmSU3BasisFastIteration::getOmega_n(unsigned int current_index, SU3xSU2_VEC& omega_n) const
{
	assert(omega_n.empty());

	const SU3_VEC* omega_su3 = getOmega_n_Su3(current_index);
	const SU2_VEC* omega_spin = getOmega_n_Spin(current_index);
		
	assert(omega_su3->size() == omega_spin->size());
		
	omega_n.resize(omega_spin->size());
	for (int i = 0; i < omega_spin->size(); ++i)
	{
		omega_n[i].rho = (*omega_su3)[i].rho;
		omega_n[i].lm  = (*omega_su3)[i].lm;
		omega_n[i].mu  = (*omega_su3)[i].mu;
		omega_n[i].S2 = (*omega_spin)[i]; 
	}
}

unsigned int CncsmSU3BasisFastIteration::getMult_p(unsigned int current_index) const
{
	const UN::SU3xSU2_VEC& gamma_p = gammas_[proton_basis_[(pn_basis_[current_index])[kProtonBasisIndex]][kGamma]]; 
	const SU3_VEC& omega_p = omegas_su3_[proton_basis_[(pn_basis_[current_index])[kProtonBasisIndex]][kOmegaSu3]]; 
	assert(omega_p.size() == (gamma_p.size() - 1)); // not sure what happens when gamma_p.size() == 1 ==> omega_p.size() should be 0
	unsigned int mult_total = 1;
	for (int i = 0; i < omega_p.size(); ++i)
	{
		mult_total *= omega_p[i].rho;
	}
	for (int i = 0; i < gamma_p.size(); ++i)
	{
		mult_total *= gamma_p[i].mult;
	}
	assert(mult_total);
	return mult_total;
}

unsigned int CncsmSU3BasisFastIteration::getMult_n(unsigned int current_index) const
{
	const UN::SU3xSU2_VEC& gamma_n = gammas_[neutron_basis_[pn_basis_[current_index][kNeutronBasisIndex]][kGamma]]; 
	const SU3_VEC& omega_n = omegas_su3_[neutron_basis_[pn_basis_[current_index][kNeutronBasisIndex]][kOmegaSu3]]; 
	assert(omega_n.size() == (gamma_n.size() - 1)); // not sure what happens when gamma_n.size() == 1 ==> omega_n.size() should be 0
	unsigned int mult_total = 1;
	for (int i = 0; i < omega_n.size(); ++i)
	{
		mult_total *= omega_n[i].rho;
	}
	for (int i = 0; i < gamma_n.size(); ++i)
	{
		mult_total *= gamma_n[i].mult;
	}
	assert(mult_total);
	return mult_total;
}

unsigned char CncsmSU3BasisFastIteration::status(const unsigned int current_index, const unsigned int index_last) const
{
	// if current_index == end ==> we are most likely at the end of the iteration over pn_basis_
	if (current_index == pn_basis_.size())
	{
		return 0xFF;
	}

	assert(current_index < pn_basis_.size());
	assert(index_last < pn_basis_.size());

	const NUCLEON_BASIS_INDICES& indices_p = proton_basis_[pn_basis_[current_index][kProtonBasisIndex]];
	const NUCLEON_BASIS_INDICES& indices_n = neutron_basis_[pn_basis_[current_index][kNeutronBasisIndex]];
	const NUCLEON_BASIS_INDICES& old_indices_p = proton_basis_[pn_basis_[index_last][kProtonBasisIndex]];
	const NUCLEON_BASIS_INDICES& old_indices_n = neutron_basis_[pn_basis_[index_last][kNeutronBasisIndex]];

// stat carries the highest change in the hiearchy order {dp dn gp gn wp wn wpn} of changes
// e.g. stat = gp denotes that all gp, gn, wp wn and wpn have been changed and hence code may take a proper action
	unsigned char stat = kNewPNOmega;
	if ((indices_n[kOmegaSu3] != old_indices_n[kOmegaSu3]) || (indices_n[kOmegaSpin] != old_indices_n[kOmegaSpin]))
	{
		stat = kNewOmega_n;
	}
	if (indices_p[kOmegaSu3] != old_indices_p[kOmegaSu3] || indices_p[kOmegaSpin] != old_indices_p[kOmegaSpin]) 
	{
		stat = kNewOmega_p;
	}
	if (indices_n[kGamma] != old_indices_n[kGamma]) 
	{
		stat = kNewGamma_n;
	}
	if (indices_p[kGamma] != old_indices_p[kGamma]) 
	{
		stat = kNewGamma_p;
	}
	if (indices_n[kDistr] != old_indices_n[kDistr]) 
	{
		stat = kNewDistr_n;
	}
	if (indices_p[kDistr] != old_indices_p[kDistr]) 
	{
		stat = kNewDistr_p;
	}
	return stat;
}

///////////////////////////////////////////////////////////////////////////////////
void CncsmSU3BasisFastIteration::ShowMemoryRequirements() const
{
	size_t memory_requirements = 0;

	memory_requirements += distribs_p_.size()*sizeof(SingleDistribution);
	memory_requirements += distribs_n_.size()*sizeof(SingleDistribution);

	memory_requirements += gammas_.size()*sizeof(UN::SU3xSU2_VEC);
	for (size_t i = 0; i < gammas_.size(); ++i)
	{
		memory_requirements += gammas_[i].size()*sizeof(UN::SU3xSU2);
	}

	memory_requirements += omegas_su3_.size()*sizeof(SU3_VEC);
	for (size_t i = 0; i < omegas_su3_.size(); ++i)
	{
		memory_requirements += omegas_su3_[i].size()*sizeof(SU3xSU2::LABELS);
	}

	memory_requirements += omegas_spin_.size()*sizeof(SU2_VEC);
	for (size_t i = 0; i < omegas_spin_.size(); ++i)
	{
		memory_requirements += omegas_spin_[i].size()*sizeof(SU2::LABEL);
	}

	memory_requirements += proton_basis_.size()*sizeof(NUCLEON_BASIS_INDICES);
	memory_requirements += neutron_basis_.size()*sizeof(NUCLEON_BASIS_INDICES);
	memory_requirements += pn_basis_.size()*sizeof(PROTON_NEUTRON_BASIS_INDICES);

	cout << "memory requirements: " << memory_requirements/(1024.0*1024) << " mb." << endl;
}

void CncsmSU3BasisFastIteration::ShowBasis(std::ostream& output_device)
{
	for (size_t i = 0; i < pn_basis_.size(); ++i)
	{
		output_device << pn_basis_[i][kProtonBasisIndex];
		output_device << "={" << proton_basis_[pn_basis_[i][kProtonBasisIndex]][0] << ", " << proton_basis_[pn_basis_[i][kProtonBasisIndex]][1] << ", " << proton_basis_[pn_basis_[i][kProtonBasisIndex]][2];
		output_device << ", " << proton_basis_[pn_basis_[i][kProtonBasisIndex]][3] << "}\t\t";
		
		output_device << pn_basis_[i][kNeutronBasisIndex];
		output_device << "={" << proton_basis_[pn_basis_[i][kNeutronBasisIndex]][kDistr] << ", " << proton_basis_[pn_basis_[i][kNeutronBasisIndex]][kGamma] << ", " << proton_basis_[pn_basis_[i][kNeutronBasisIndex]][kOmegaSu3];
		output_device << ", " << proton_basis_[pn_basis_[i][kNeutronBasisIndex]][kOmegaSpin] << "}\t\t";
		output_device << pn_basis_[i][kRhoSU3xSU2Index] << endl;
	}
}
