#include <SU3NCSMUtils/CTuple.h>
#include <UNU3SU3/CSU3Master.h>

#include <su3.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>

double CSU3CGMaster::m_Zero = std::numeric_limits<float>::epsilon();

// Note that the resulting structure of vCGs is sorted, that is, the order of
// [l1,l2,l3,k3,k2,k1] tuples is identical to the sorted order method
// sort(vCGs.begin(), vCGs.end(), CSU3CGMaster::CompareKeys); would produce.
size_t CSU3CGMaster::GetSO3(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3,
                            std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > >& vCGs) {
   // This order of labels (coresponds to order of loops) is required to yield
   // elements of vCGs vector in sorted order.  This has the obvious advantage
   // that one need not to invoke sort(vCGs.begin(), vCGs.end()),
   // CSU3CGMaster::CompareKeys).
   int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
   int l1, l2, l3;
   int k1, k2, k3;
   std::vector<SO3::LABEL> vl1, vl2, vl3;
   K1L1K2L2K3L3 Key;
   int max_rho = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   std::vector<double> CGrho(max_rho);

   int nl1 = SU3::GetAngularMomenta(ir1, vl1);  // number of L's in (lm1 mu1)
   int nl2 = SU3::GetAngularMomenta(ir2, vl2);  // number of L's in (lm2 mu2)
   int nl3 = SU3::GetAngularMomenta(ir3, vl3);  // number of L's in (lm3 mu3)
   size_t i1, i2, i3;                           // indices of vl1, vl2, and vl3 vectors
   std::vector<double> dCG;

   for (i1 = 0; i1 < nl1; i1++)  // iterate over all l1 in (lm1 mu1)
   {
      l1 = vl1[i1];
      int max_k1 = su3::kmax(lm1, mu1, l1);
      Key.l1(2 * l1);
      for (i2 = 0; i2 < nl2; i2++)  // iterate over all l2 in (lm2 mu2)
      {
         l2 = vl2[i2];
         int max_k2 = su3::kmax(lm2, mu2, l2);
         Key.l2(2 * l2);
         for (i3 = 0; i3 < nl3; i3++) {
            l3 = vl3[i3];
            int max_k3 = su3::kmax(lm3, mu3, l3);
            if (l3 < abs(l1 - l2) || l3 > (l1 + l2)) {
               continue;
            }
            Key.l3(2 * l3);

            su3::wu3r3w(lm1, mu1, lm2, mu2, lm3, mu3, l1, l2, l3, max_k3, max_k2, max_k1, max_rho,
                        dCG);

            size_t index = 0;
            for (k3 = 0; k3 < max_k3; k3++)  // for all values of k3
            {
               Key.k3(k3);
               for (k2 = 0; k2 < max_k2; k2++)  // for all values of k2
               {
                  Key.k2(k2);
                  for (k1 = 0; k1 < max_k1; k1++)  // for all values of k1
                  {
                     Key.k1(k1);
                     for (int irho = 0; irho < max_rho; irho++)  // for all possible multiplicities
                     {
                        CGrho[irho] = dCG[index++];  // store SU(3) wigner coefficient
                     }
                     vCGs.push_back(std::make_pair(Key, CGrho));
                  }
               }
            }
         }
      }
   }
   return vCGs.size();
}

// Returns <(lm1 mu1) ... ; (lm2 mu2) ... || (lm3 mu3) ... l3>_{...}
// Resulting data structure: vector whose elements are {[l1,l2,k3,k2,k1],
// {rho_{0},rho_{1},...rho_{max}}}
size_t CSU3CGMaster::GetSO3(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3,
                            const SO3::LABEL& L2,
                            std::vector<std::pair<K1L1K2L2K3, std::vector<double> > >& vCGs) {
   int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
   int l1, l2, l3(L2 / 2), k1, k2, k3;
   int irho;
   std::vector<SO3::LABEL> vl1, vl2;
   K1L1K2L2K3 Key;
   int max_k3 = su3::kmax(lm3, mu3, l3);
   int max_rho = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   std::vector<double> CGrho(max_rho);

   //	std::cout << ir1 << " x " << ir2 << "\t" << ir3 << std::endl;
   //	std::cout << "2*L0 = " << (int)L2 << std::endl;

   int nl1 = SU3::GetAngularMomenta(ir1, vl1);  // number of L's in (lm1 mu1)
   int nl2 = SU3::GetAngularMomenta(ir2, vl2);  // number of L's in (lm2 mu2)
   size_t i1, i2;                               // indices of vl1 and vl2 vectors
   std::vector<double> dCG;

   for (i1 = 0; i1 < nl1; i1++)  // iterate over all l1 in (lm1 mu1)
   {
      l1 = vl1[i1];
      int max_k1 = su3::kmax(lm1, mu1, l1);
      Key.l1(2 * l1);
      for (i2 = 0; i2 < nl2; i2++)  // iterate over all l2 in (lm2 mu2)
      {
         l2 = vl2[i2];
         int max_k2 = su3::kmax(lm2, mu2, l2);
         Key.l2(2 * l2);
         if (l3 < abs(l1 - l2) || l3 > (l1 + l2)) {
            continue;
         }

         su3::wu3r3w(lm1, mu1, lm2, mu2, lm3, mu3, l1, l2, l3, max_k3, max_k2, max_k1, max_rho,
                     dCG);

         size_t index = 0;
         for (k3 = 0; k3 < max_k3; k3++)  // for all values of k3
         {
            Key.k3(k3);
            for (k2 = 0; k2 < max_k2; k2++)  // for all values of k1
            {
               Key.k2(k2);
               for (k1 = 0; k1 < max_k1; k1++)  // for all values of k2
               {
                  Key.k1(k1);
                  for (irho = 0; irho < max_rho; irho++)  // for all possible multiplicities
                  {
                     CGrho[irho] = dCG[index++];  // store SU(3) wigner coefficient
                  }
                  vCGs.push_back(std::make_pair(Key, CGrho));
               }
            }
         }
      }
   }
   //	std::cout << "Returning " << vCGs.size() << " CGS" << std::endl;
   return vCGs.size();
}

/////////////////////////////////////////////////////////////////////////////////////////
struct EPS2_LM2_LM1 {
   int eps2, LM2, LM1;
};

class SU3_SU2xU1_WignerCoeffs {
   int lm1_, mu1_, lm2_, mu2_, lm3_, mu3_;
   int rhomax_;
   // {<lm1_ mu1_) * (lm2_ mu2_) * || (lm3_ mu3_) HW/HW'>}
   std::vector<double> dewu3_;

  public:
   SU3_SU2xU1_WignerCoeffs(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3);
   void Compute(int eps3, int LM3, std::vector<EPS2_LM2_LM1>& labels, std::vector<double>& cgs);
   inline int rhomax() const { return rhomax_; };
};

SU3_SU2xU1_WignerCoeffs::SU3_SU2xU1_WignerCoeffs(int lm1, int mu1, int lm2, int mu2, int lm3,
                                                 int mu3) {
   std::vector<int> j1t_cpp;
   std::vector<int> j2t_cpp;
   std::vector<int> ie2_cpp;

   lm1_ = lm1;
   lm2_ = lm2;
   lm3_ = lm3;
   mu1_ = mu1;
   mu2_ = mu2;
   mu3_ = mu3;
   rhomax_ = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   const int I3 = 1;
   // nec will computed by xewu3, but it is not needed for our purposes
   int nec;
   // Compute {<lm1_ mu1_) * (lm2_ mu2_) * || (lm3_ mu3_) HW/HW'> coefficients}
   su3::xewu3(lm1, mu1, lm2, mu2, lm3, mu3, I3, nec, rhomax_, j1t_cpp, j2t_cpp, ie2_cpp, dewu3_);
}

void SU3_SU2xU1_WignerCoeffs::Compute(int eps3, int LM3, std::vector<EPS2_LM2_LM1>& labels,
                                      std::vector<double>& cgs) {
   std::vector<int> j1smax;
   std::vector<int> j1tmax;
   std::vector<int> j2tmax;
   std::vector<int> j2smax;
   std::vector<int> indmat;
   int indmax;
   int iesmax, eps2max;

   su3::xwu3_internal<double>(lm1_, mu1_, lm2_, mu2_, lm3_, mu3_, eps3, LM3, dewu3_, rhomax_, indmax, j1smax, j1tmax,
             j2smax, j2tmax, eps2max, indmat, cgs);

   labels.clear();
   // indmax == number of eps1 LM1 eps2 LM2 sets that couple to eps3 LM3
   labels.reserve(indmax);
   EPS2_LM2_LM1 eps2_LM2_LM1;
   int dim = lm2_ + mu2_ + 1;
   iesmax = j2tmax.size();
   for (int ies = 0; ies < iesmax; ++ies) {
      eps2_LM2_LM1.eps2 = eps2max - 3 * (iesmax - (ies + 1));
      for (int j2s = 0; j2s < j2smax[ies]; ++j2s) {
         eps2_LM2_LM1.LM2 = j2tmax[ies] - 2 * j2s;
         int iesj2s = ies + dim * j2s;
         for (int j1s = 0; j1s < j1smax[iesj2s]; ++j1s) {
            eps2_LM2_LM1.LM1 = j1tmax[iesj2s] - 2 * j1s;
            labels.push_back(eps2_LM2_LM1);
         }
      }
   }
}

size_t CSU3CGMaster::GetSU2U1(
    const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3,
    std::vector<std::pair<EPS1LM1EPS2LM2EPS3LM3, std::vector<double> > >& vCGs) {
   int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
   SU3_SU2xU1_WignerCoeffs su3u1su2cgs(lm1, mu1, lm2, mu2, lm3, mu3);
   int rhomax = su3u1su2cgs.rhomax();

   std::vector<double> cgs;

   for (int p3 = 0; p3 <= lm3; ++p3) {
      for (int q3 = 0; q3 <= mu3; ++q3) {
         int eps3 = 2 * lm3 + mu3 - 3 * (p3 + q3);
         int LM3 = mu3 + p3 - q3;
         std::vector<EPS2_LM2_LM1> labels;
         su3u1su2cgs.Compute(eps3, LM3, labels, cgs);
         size_t index = 0;
         for (auto eps2_LM2_LM1 : labels) {
            int eps1 = eps3 - eps2_LM2_LM1.eps2;
            int LM1 = eps2_LM2_LM1.LM1;
            int eps2 = eps2_LM2_LM1.eps2;
            int LM2 = eps2_LM2_LM1.LM2;
            std::vector<double> CGrho(rhomax, 0.0);
            for (int rho = 0; rho < rhomax; ++rho) {
               CGrho[rho] = cgs[index++];
            }
            EPS1LM1EPS2LM2EPS3LM3 key(eps1, LM1, eps2, LM2, eps3, LM3);
            vCGs.push_back(std::make_pair(key, CGrho));
         }
      }
   }
   return vCGs.size();
}

//	This function calculates SU(3) U6 symbols.
//	NOTE: U6lm.size() == rho12*rho23*rho12_3*rho1_23
void CSU3CGMaster::Get6lm(const CSU3CGMaster::UZ6lmType Type, const SU3::LABELS& ir1,
                          const SU3::LABELS& ir2, const SU3::LABELS& ir, const SU3::LABELS& ir3,
                          const SU3::LABELS& ir12, const SU3::LABELS& ir23,
                          std::vector<double>& dUZ6lm) {
   int rho12max = SU3::mult(ir1, ir2, ir12);
   int rho23max = SU3::mult(ir2, ir3, ir23);
   int rho12_3max = SU3::mult(ir12, ir3, ir);
   int rho1_23max = SU3::mult(ir1, ir23, ir);

   int lm1(ir1.lm);
   int mu1(ir1.mu);

   int lm2(ir2.lm);
   int mu2(ir2.mu);

   int lm3(ir3.lm);
   int mu3(ir3.mu);

   int lm(ir.lm);
   int mu(ir.mu);

   int lm12(ir12.lm);
   int mu12(ir12.mu);

   int lm23(ir23.lm);
   int mu23(ir23.mu);

   if (Type == U6LM) {
      su3::wru3(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max, rho12_3max,
                rho23max, rho1_23max, dUZ6lm);
   } else {
      su3::wzu3(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max, rho12_3max,
                rho23max, rho1_23max, dUZ6lm);
   }
}
