#ifndef CSU3Master_h
#define CSU3Master_h
#include <UNU3SU3/UNU3SU3Basics.h>

#include <su3.h>

#include <cstdlib>
#include <utility>

// Note: even tough methods LM?(), eps?(), or l?() should return SU2::LABEL, U1::LABEL, SO3::LABEL,
// they return int so that one can use cout >> LM1()

struct EPS1LM1EPS2LM2 {
   EPS1LM1EPS2LM2(const SU3::SU2U1::LABELS& eps1lm1, const SU3::SU2U1::LABELS& eps2lm2)
       : epslm1(eps1lm1), epslm2(eps2lm2){};
   EPS1LM1EPS2LM2(const U1::LABEL& e1, const SU2::LABEL& lm1, const U1::LABEL& e2,
                  const SU2::LABEL& lm2)
       : epslm1(e1, lm1), epslm2(e2, lm2){};
   EPS1LM1EPS2LM2() : epslm1(), epslm2(){};

   bool operator<(const EPS1LM1EPS2LM2& rhs) const {
      return (epslm1 < rhs.epslm1) || (!(rhs.epslm1 < epslm1) && epslm2 < rhs.epslm2);
   };

   inline int eps1() const { return epslm1.eps; };
   inline int eps2() const { return epslm2.eps; };
   inline int LM1() const { return epslm1.LM; };
   inline int LM2() const { return epslm2.eps; };
   inline void eps1(const U1::LABEL& e) { epslm1.eps = e; };
   inline void eps2(const U1::LABEL& e) { epslm2.eps = e; };
   inline void LM1(const SU2::LABEL& LM) { epslm1.LM = LM; };
   inline void LM2(const SU2::LABEL& LM) { epslm2.LM = LM; };

  private:
   SU3::SU2U1::LABELS epslm1, epslm2;
};
struct EPS1LM1EPS2LM2EPS3LM3 {
   EPS1LM1EPS2LM2EPS3LM3(const SU3::SU2U1::LABELS& eps1lm1, const SU3::SU2U1::LABELS& eps2lm2,
                         const SU3::SU2U1::LABELS& eps3lm3)
       : epslm1(eps1lm1), epslm2(eps2lm2), epslm3(eps3lm3){};
   EPS1LM1EPS2LM2EPS3LM3(const U1::LABEL e1, const SU2::LABEL& lm1, const U1::LABEL& e2,
                         const SU2::LABEL& lm2, const U1::LABEL& e3, const SU2::LABEL& lm3)
       : epslm1(e1, lm1), epslm2(e2, lm2), epslm3(e3, lm3){};
   EPS1LM1EPS2LM2EPS3LM3() : epslm1(), epslm2(), epslm3(){};

   bool operator==(const EPS1LM1EPS2LM2EPS3LM3& rhs) const {
      return epslm1 == rhs.epslm1 && epslm2 == rhs.epslm2 && epslm3 == rhs.epslm3;
   }

   bool operator<(const EPS1LM1EPS2LM2EPS3LM3& rhs) const {
      /*
                      std::cout << (int)epslm1.eps << " " << (int)epslm1.LM << "  " <<
         (int)epslm2.eps << " " << (int)epslm2.LM << "  " << (int)epslm3.eps << " " <<
         (int)epslm3.LM; std::cout << " < " << (int)rhs.eps1() << " " << (int)rhs.LM1() << "  " <<
         (int)rhs.eps2() << " " << (int)rhs.LM2() << "  " << (int)rhs.eps3() << " " <<
         (int)rhs.LM3(); std::cout << " ---> returning " << ((epslm1 < rhs.epslm1) || (!(rhs.epslm1
         < epslm1) && epslm2 < rhs.epslm2) || (!(rhs.epslm1 < epslm1) && !(rhs.epslm2 < epslm2) &&
         epslm3 < rhs.epslm3)) << std::endl; std::cout <<  (!(rhs.epslm1 < epslm1) && epslm2 <
         rhs.epslm2) << std::endl; std::cout << (!(rhs.epslm1 < epslm1) && !(rhs.epslm2 < epslm2) &&
         epslm3 < rhs.epslm3) << std::endl; std::cout << ((!(rhs.epslm1 < epslm1) && epslm2 <
         rhs.epslm2) || (!(rhs.epslm1 < epslm1) && !(rhs.epslm2 < epslm2) && epslm3 < rhs.epslm3))
         << std::endl; std::cout << (epslm1 < rhs.epslm1) << std::endl; std::cout << ((epslm1 <
         rhs.epslm1) || ((!(rhs.epslm1 < epslm1) && epslm2 < rhs.epslm2) || (!(rhs.epslm1 < epslm1)
         && !(rhs.epslm2 < epslm2) && epslm3 < rhs.epslm3))) << std::endl;
      */
      return (epslm1 < rhs.epslm1) ||
             ((epslm1 == rhs.epslm1 && epslm2 < rhs.epslm2) ||
              ((epslm1 == rhs.epslm1 && epslm2 == rhs.epslm2 && epslm3 < rhs.epslm3)));
   }

   inline int eps1() const { return epslm1.eps; };
   inline int eps2() const { return epslm2.eps; };
   inline int eps3() const { return epslm3.eps; };
   inline void eps1(const U1::LABEL& e) { epslm1.eps = e; };
   inline void eps2(const U1::LABEL& e) { epslm2.eps = e; };
   inline void eps3(const U1::LABEL& e) { epslm3.eps = e; };

   inline int LM1() const { return epslm1.LM; };
   inline int LM2() const { return epslm2.LM; };
   inline int LM3() const { return epslm3.LM; };
   inline void LM1(const SU2::LABEL& LM) { epslm1.LM = LM; };
   inline void LM2(const SU2::LABEL& LM) { epslm2.LM = LM; };
   inline void LM3(const SU2::LABEL& LM) { epslm3.LM = LM; };

  private:
   SU3::SU2U1::LABELS epslm1, epslm2, epslm3;
};

struct K1L1K2L2K3 {
   K1L1K2L2K3(const SU3::SO3::LABELS& so3labels1, const SU3::SO3::LABELS& so3labels2, const char k)
       : k1l1(so3labels1), k2l2(so3labels2), K3(k) {}
   K1L1K2L2K3() : k1l1(), k2l2(), K3(0){};

   bool operator<(const K1L1K2L2K3& rhs) const {
      return ((k1l1 < rhs.k1l1) || (!(rhs.k1l1 < k1l1) && k2l2 < rhs.k2l2) ||
              (!(rhs.k1l1 < k1l1) && !(rhs.k2l2 < k2l2) && K3 < rhs.K3));
   }

   inline int k1() const { return k1l1.K; };
   inline int k2() const { return k2l2.K; };
   inline int k3() const { return K3; };
   inline void k1(const char k) { k1l1.K = k; };
   inline void k2(const char k) { k2l2.K = k; };
   inline void k3(const char k) { K3 = k; };

   inline int l1() const { return k1l1.L2; };
   inline int l2() const { return k2l2.L2; };
   inline void l1(const SO3::LABEL& l) { k1l1.L2 = l; };
   inline void l2(const SO3::LABEL& l) { k2l2.L2 = l; };

  private:
   SU3::SO3::LABELS k1l1, k2l2;
   char K3;
};

struct K1L1K2L2K3L3 {
   K1L1K2L2K3L3(const char& k1, const U1::LABEL& l1, const char& k2, const U1::LABEL& l2,
                const char& k3, const U1::LABEL& l3)
       : k1l1(k1, l1), k2l2(k2, l2), k3l3(k3, l3){};
   K1L1K2L2K3L3(const SU3::SO3::LABELS& so3labels1, const SU3::SO3::LABELS& so3labels2,
                const SU3::SO3::LABELS& so3labels3)
       : k1l1(so3labels1), k2l2(so3labels2), k3l3(so3labels3){};
   K1L1K2L2K3L3() : k1l1(), k2l2(), k3l3(){};

   bool operator<(const K1L1K2L2K3L3& rhs) const {
      return ((k1l1 < rhs.k1l1) || (!(rhs.k1l1 < k1l1) && k2l2 < rhs.k2l2) ||
              (!(rhs.k1l1 < k1l1) && !(rhs.k2l2 < k2l2) && k3l3 < rhs.k3l3));
   }
   bool operator==(const K1L1K2L2K3L3& rhs) const {
      return (k1l1 == rhs.k1l1 && k2l2 == rhs.k2l2 && k3l3 == rhs.k3l3);
   }

   inline int k1() const { return k1l1.K; };
   inline int k2() const { return k2l2.K; };
   inline int k3() const { return k3l3.K; };
   inline void k1(int k) { k1l1.K = k; };
   inline void k2(int k) { k2l2.K = k; };
   inline void k3(int k) { k3l3.K = k; };

   inline int l1() const { return k1l1.L2; };
   inline int l2() const { return k2l2.L2; };
   inline int l3() const { return k3l3.L2; };
   inline void l1(const SO3::LABEL& l) { k1l1.L2 = l; };
   inline void l2(const SO3::LABEL& l) { k2l2.L2 = l; };
   inline void l3(const SO3::LABEL& l) { k3l3.L2 = l; };

  private:
   SU3::SO3::LABELS k1l1, k2l2, k3l3;
};

// class CSU3CGMaster is derived from CBlocks and hence the constructor of
// CBlocks is called automatically (required for the proper functioning of
// fortran SU(3) subroutines).
//
// None of the SU3CGMaster::Get methods invokes clear() on data structure which
// is provided for the output results. It is responsibility of the outer subroutine
// to make sure the structure is empty. Otherwise std::vector::push_back() is applied
// and hence the size will be ever incresing.
class CSU3CGMaster {
   static double m_Zero;
  public:
   enum UZ6lmType { U6LM, Z6LM };
   ///////////////////////////// SU(3) > SO(3) //////////////////////////////////
   // Returns <(lm1 mu1) ... ; (lm2 mu2) ... || (lm3 mu3) ... l3>_{...} Resulting
   // data structure: vector whose elements are {[k1,l1,k2,l2,k3],
   // {rho_{0},rho_{1},...rho_{max}}}	NOTE: some of the resulting SU(3) Wigner
   // coefficients may be equal to zero for a certain value of \rho_{i} and hence
   // it is wrong to assume that resulting coefficients are non-zero.
   size_t GetSO3(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3,
                 const SO3::LABEL& L2,
                 std::vector<std::pair<K1L1K2L2K3, std::vector<double> > >& vCGs);

   // Returns a complete set of SU(3) Wigner coefficients <(lm1 mu1) ..., (lm2
   // mu2) ... || (lm3 mu3) ...>_{...}	NOTE: some of the resulting SU(3) Wigner
   // coefficients (stored in vector<double>) may be equal to zero for a certain
   // value of \rho_{i} and hence it is wrong to assume that resulting
   // coefficients are non-zero.
   size_t GetSO3(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3,
                 std::vector<std::pair<K1L1K2L2K3L3, std::vector<double> > >& vCGs);
   /////////////////////////// SU(3) > SU(2)xU(1) ///////////////////////////////
   size_t GetSU2U1(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3,
                   std::vector<std::pair<EPS1LM1EPS2LM2EPS3LM3, std::vector<double> > >& vCGs);

   // 	Get SU(3) C-G coefficients that are needed for r.m.e calculation.
   //	< ir1 eps1_{hw} LM1_{hw}; ir2 eps2 LM2 || ir3 eps3_{hw} LM3_{hw}>, where
   //		eps1_{hw} = 2lm1 + mu1
   //		LM1_{hw} = mu1/2
   //		eps2 = eps3_{hw} - eps1_{hw}
   //		LM2 ... provided in vector<SU2::LABEL> LM2s
   //		eps3_{hw} = 2lm3 + mu3
   //		LM3_{hw} = mu3/2
   template <typename T1, typename T2, typename T3>
   size_t RMESU2U1(const T1& ir1, const T2& ir2, const T3& ir3, std::vector<SU2::LABEL>& LM2s,
                   std::vector<std::vector<double> >& vCGs);

   void Get6lm(const UZ6lmType Type, const SU3::LABELS& ir1, const SU3::LABELS& ir2,
               const SU3::LABELS& ir, const SU3::LABELS& ir3, const SU3::LABELS& ir12,
               const SU3::LABELS& ir23, std::vector<double>& U6lm);

   template <typename T1, typename T2, typename T3>
   void GetPhi(const T1& ir1, const T2& ir2, const T3& ir3, std::vector<double>& Phi);
};

// This is just a wrapper between LSU3shell and su3lib
template <typename T1, typename T2, typename T3>
void CSU3CGMaster::GetPhi(const T1& ir1, const T2& ir2, const T3& ir3, std::vector<double>& Phi) {
   const int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
   const int rho12max = 1;
   const int rho23max = 1;
   const int rho12_3max = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   const int rho1_23max = rho12_3max;
   su3::wzu3(lm1, mu1, 0, 0, lm3, mu3, lm2, mu2, lm1, mu1, lm2, mu2, rho12max, rho12_3max, rho23max,
             rho1_23max, Phi);
}

// 	Get SU(3) C-G coefficients that are needed for r.m.e calculation.
//	< ir1 eps1_{LW} LM1_{LW}; ir2 eps2 LM2 || ir3 eps3_{LW} LM3_{LW}>, where
//		eps1_{LW} = 2lm1 + mu1
//		LM1_{LW} = mu1/2
//		eps2 = eps3_{LW} - eps1_{LW}
//		LM2 ... all possible values of LM2 for a given eps2 (stored in LM2s)
//		eps3_{LW} = 2lm3 + mu3
//		LM3_{LW} = mu3/2
//
//		OUTPUT:
//		LM2s	contains values of LM2
//		vCGs is two-dimensional vector
//		Example:
//		<ir1 eps1_{LW} LM1_{LW}; ir2 eps2 LM2 || ir3 eps3_{LW} LM3_{LW}>_{rho}
//		is stored as
//		vCGs[rho][irow], where LM2 = LM2[irow]
template <typename T1, typename T2, typename T3>
size_t CSU3CGMaster::RMESU2U1(const T1& ir1, const T2& ir2, const T3& ir3,
                              std::vector<SU2::LABEL>& LM2s,
                              std::vector<std::vector<double> >& vCGs) {
   int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
   int rhomax = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   std::vector<int> j1t;
   std::vector<int> j2t;
   std::vector<int> ie2;
   // Trick: I3 = 0 ==> xewu3 yields {<lm1_ mu1) * ;(lm2_ mu2_) * || (lm3_ mu3_) LW>}
   const int I3 = 0;
   // nec will computed by xewu3, but it is not needed for our purposes
   int nec;
   std::vector<double> dewu3;
   // Compute {<lm1_ mu1_) * (lm2_ mu2_) * || (lm3_ mu3_) LW>}
   su3::xewu3(lm1, mu1, lm2, mu2, lm3, mu3, I3, nec, rhomax, j1t, j2t, ie2, dewu3);
   // we are interested in 
   //{<lm1_ mu1_) LW * (lm2_ mu2_) * || (lm3_ mu3_) LW>}
   int eps1 = 2 * lm1 + mu1;  // eps1_{LW}
   int eps3 = 2 * lm3 + mu3;  // eps3_{LW}
   int eps2 = eps3 - eps1;    // eps2 = eps3LW - eps1LW

   LM2s.resize(0);
   vCGs.resize(rhomax);
   // find coefficients with eps2 = eps3LW - eps1LW 
   for (size_t i = 0; i < ie2.size(); ++i)
   {
      if (ie2[i] == eps2)
      {
         assert(j1t[i] == mu1);
         size_t index = i*rhomax;
         LM2s.push_back(j2t[i]);
         for (int rho = 0; rho < rhomax; ++rho)
         {
            vCGs[rho].push_back(dewu3[index + rho]);
         }
      }
   }
   return rhomax;
}

template <typename KEY>
struct less_first {
   bool operator()(const std::pair<KEY, std::vector<double> >& lhs,
                   const std::pair<KEY, std::vector<double> >& rhs) {
      return lhs.first < rhs.first;
   }
};

template <typename KEY>
struct key_less {
   bool operator()(const std::pair<KEY, std::vector<double> >& lhs, const KEY& rhs) const {
      return lhs.first < rhs;
   };
   bool operator()(const KEY& lhs, const std::pair<KEY, std::vector<double> >& rhs) const {
      return lhs < rhs.first;
   };
};
#endif
