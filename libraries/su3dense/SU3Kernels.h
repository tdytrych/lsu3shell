#ifndef SU3DENSE_SU3KERNELS_H
#define SU3DENSE_SU3KERNELS_H

#include <vector>
#include <map>
#include <utility>

#include <UNU3SU3/UNU3SU3Basics.h>
#include <LookUpContainers/WigEckSU3SO3CGTable.h>
#include <LookUpContainers/CSU39lm.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <su3dense/CWig9JLookUpTables.h>

using SpinlessRMEsCache = std::map<std::pair<SU3::LABELS, SU3::LABELS>, std::vector<double>>;

void GenerateSpinlessRMEs(int32_t a1_max, const SU3::LABELS& wp_row, int32_t a1p_max,
                          const SU3::LABELS& wp_col, const SU3::LABELS& wn, int32_t a0max,
                          const SU3::LABELS& ir0, const float* ipjp_rmes, SU3_VEC& wpn_col_vec,
                          SU3_VEC& wpn_row_vec, std::vector<double>& su39lmPtoPN,
                          SpinlessRMEsCache& l2m2_cache);

void ComputeMatrixElements(
    const lsu3::CncsmSU3xSU2Basis& row_basis, const lsu3::CncsmSU3xSU2Basis& col_basis,
    uint16_t a1_max, const SU3xSU2::LABELS& wp_row, uint16_t a1p_max, const SU3xSU2::LABELS& wp_col,
    uint32_t ipin_block, uint32_t jpin_block, int32_t a0max, int32_t k0max,
    const SU3xSU2::LABELS& ir0, int32_t ll0, const std::vector<int32_t>& jj0_vec,
    WigEckSU3SO3CGTable* su3so3_ir0l0_table,
    const std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>& wig9j_cached,
    const CWig9STable& wig9s, const SpinlessRMEsCache& lm2mu2_RMEs_cache,
    std::vector<double>& me_wpn_row_col, std::vector<double>& me);

void ComputeMatrixElements(
    const lsu3::CncsmSU3xSU2Basis& row_basis, const lsu3::CncsmSU3xSU2Basis& col_basis,
    uint16_t a1max, const SU3xSU2::LABELS& wp_row, uint16_t a1p_max, const SU3xSU2::LABELS& wp_col,
    uint32_t ipin_block, uint32_t jpin_block, uint16_t a2max, int32_t a0max, int32_t k0max,
    const SU3xSU2::LABELS& ir0, int32_t ll0, const std::vector<int32_t>& jj0_vec,
    WigEckSU3SO3CGTable* su3so3_ir0l0_table,
    const std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>& wig9j_cached,
    const CWig9STable& wig9s, const SpinlessRMEsCache& lm2mu2_RMEs_cache,
    std::vector<double>& me_wpn_row_col, std::vector<double>& me);
#endif
