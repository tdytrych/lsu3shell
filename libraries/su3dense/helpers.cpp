#include "helpers.h"
#include <iomanip>

// For a given pair of <Jf || T^(lm0 mu0)S0 || Ji> quantum numbers
// generate allowed pairs of {2L0, 2J0} quantum numbers
// that satisfy Ji x J0 --> Jf
// Example:
// Jf = 1
// Ji = 2
// ir0: (2 2)S=2
//
//  ll0_jj0vec[i]
// ==================
// i  .fist .second
// ==================
// 0    0   {4}
// 1    4   {2 4 6}
// 2    6   {2 4 6}
// 3    8   {4 6}
// ==================
void GenerateAllowedll0jj0(int32_t JJf, int32_t a0max, const SU3xSU2::LABELS& ir0, int32_t JJ0sel, int32_t JJi,
                           std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                           std::vector<int32_t>& k0max_LL0, std::vector<int32_t>& ntensors_LL0) {
   int32_t l0max = ir0.lm + ir0.mu;
   SU2_VEC jj0_vec_full;
   // maximal size of jj0_vec_full conceivable: l0max + S2
   jj0_vec_full.reserve(l0max + ir0.S2);
   for (int32_t l0 = 0; l0 <= l0max; ++l0) {
      if (!SU3::kmax(ir0, l0)) {
         continue;
      }
      jj0_vec_full.clear();
      std::vector<int32_t> jj0_vec_selected;
      SO3::Couple(2 * l0, ir0.S2, jj0_vec_full);
      for (auto jj0 : jj0_vec_full) {
         if (JJ0sel >= 0 && (jj0 != JJ0sel)) {
            continue;
         }
         if (SO3::mult(JJi, jj0, JJf)) {
            jj0_vec_selected.push_back(jj0);
         }
      }
      if (!jj0_vec_selected.empty()) {
         jj0_vec_selected.shrink_to_fit();
         ll0_jj0vec.emplace_back(2 * l0, std::move(jj0_vec_selected));
      }
   }

   if (ll0_jj0vec.empty()) {  // ==> unphysical tensor
      return;
   }

   k0max_LL0.reserve(ll0_jj0vec.size());
   ntensors_LL0.reserve(ll0_jj0vec.size());
   for (const auto& ll0_jj0s : ll0_jj0vec) {
      int32_t k0max = SU3::kmax(ir0, ll0_jj0s.first / 2);
      assert(k0max);
      int32_t ntensors = a0max * k0max * ll0_jj0s.second.size();
      k0max_LL0.push_back(k0max);
      ntensors_LL0.push_back(ntensors);
   }
   // Show content of generated arrays
   /*
      for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
         cout << "ll0:" << ll0_jj0vec[ill0].first << " jj0={";
         for (auto jj0 : ll0_jj0vec[ill0].second) {
            std::cout << jj0 << " ";
         }
         std::cout << "}";
         if (k0max_LL0[ill0] > 1) {
            std::cout << " "
                      << "k0max:" << k0max_LL0[ill0];
         }
         std::cout << std::endl;
      }
   */
}

void ShowResults(SU3xSU2::LABELS ir0, int32_t a0max,
                 const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                 const std::vector<std::vector<double>>& densities) {
   for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
      int32_t ll0 = ll0_jj0vec[ill0].first;
      int32_t nJJ0s = ll0_jj0vec[ill0].second.size();
      int32_t k0max = SU3::kmax(ir0, ll0 / 2);
      int32_t index = 0;
      for (int32_t k0 = 0; k0 < k0max; ++k0) {
         for (int32_t a0 = 0; a0 < a0max; ++a0) {
            for (int32_t iJJ0 = 0; iJJ0 < nJJ0s; ++iJJ0) {
               std::cout << "LL0:" << ll0 << " k0:" << k0 << " a0:" << a0
                         << " JJ0:" << ll0_jj0vec[ill0].second[iJJ0] << "\t"
                         << densities[ill0][index] << std::endl;
               ++index;
            }
         }
      }
   }
}

// called by su3denseP (deprecated)
// each line of otput file has structure:
// n1 n2 n3 n4 wf wi (lm0 mu0) 2S0 2L0 k0 rho0 2J0  <f||T^rho0||i>
/*
void StoreSU3tbdmes_LL0_K0_RHO0_JJ0(
    std::ofstream& file, const std::array<int, 13>& tensor_quantum_numbers,
    const SU3xSU2::LABELS& ir0, int32_t a0max,
    const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    const std::vector<std::vector<double>>& densities) {
   file.precision(13);
   for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
      int32_t ll0 = ll0_jj0vec[ill0].first;
      int32_t nJJ0s = ll0_jj0vec[ill0].second.size();
      int32_t k0max = SU3::kmax(ir0, ll0 / 2);
      // index pointing to current [k0][rh0][J0] element in tdbmes[L0]
      int32_t index = 0;
      for (int32_t k0 = 0; k0 < k0max; ++k0) {
         for (int32_t a0 = 0; a0 < a0max; ++a0) {
            for (int32_t iJJ0 = 0; iJJ0 < nJJ0s; ++iJJ0) {
               int jj0 = ll0_jj0vec[ill0].second[iJJ0];
               file << tensor_quantum_numbers[N1] << " ";
               file << tensor_quantum_numbers[N2] << " ";
               file << tensor_quantum_numbers[N3] << " ";
               file << tensor_quantum_numbers[N4] << "   ";
               file << tensor_quantum_numbers[LMF] << " ";
               file << tensor_quantum_numbers[MUF] << " ";
               file << tensor_quantum_numbers[SSF] << "   ";
               file << tensor_quantum_numbers[LMI] << " ";
               file << tensor_quantum_numbers[MUI] << " ";
               file << tensor_quantum_numbers[SSI] << "   ";
               file << tensor_quantum_numbers[LM0] << " ";
               file << tensor_quantum_numbers[MU0] << " ";
               file << tensor_quantum_numbers[SS0] << "   ";
               file << ll0 << " " << k0 << " " << a0 << " " << jj0 << " " << std::fixed
                    << std::setw(19) << densities[ill0][index] << std::endl;
               // loop iteration order corresponds to [L0][k0][a0][J0] order of tbdmes
               ++index;
            }
         }
      }
   }
}
*/
// called by su3denseP (and in future su3denseN)
// each line of otput file has structure:
// n1 n2 n3 n4 wf wi (lm0 mu0) 2S0 2L0 k0 2J0  {<f||T^rho0:0||i> ... <f||T^rho0max-1||i>}
int  StoreSU3tbdmes_K0_LL0_JJ0(
    std::ofstream& file, const std::array<int, 13>& tensor_quantum_numbers,
    const SU3xSU2::LABELS& ir0, int32_t a0max,
    const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    const std::vector<std::vector<double>>& densities) {
   file.precision(13);
   int ndensities = 0;
   for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
      int32_t ll0 = ll0_jj0vec[ill0].first;
      int32_t nJJ0s = ll0_jj0vec[ill0].second.size();
      int32_t k0max = SU3::kmax(ir0, ll0 / 2);
      for (int32_t k0 = 0; k0 < k0max; ++k0) {
         for (int32_t iJJ0 = 0; iJJ0 < nJJ0s; ++iJJ0) {
            int jj0 = ll0_jj0vec[ill0].second[iJJ0];
            file << tensor_quantum_numbers[N1] << " ";
            file << tensor_quantum_numbers[N2] << " ";
            file << tensor_quantum_numbers[N3] << " ";
            file << tensor_quantum_numbers[N4] << "   ";
            file << tensor_quantum_numbers[LMF] << " ";
            file << tensor_quantum_numbers[MUF] << " ";
            file << tensor_quantum_numbers[SSF] << "   ";
            file << tensor_quantum_numbers[LMI] << " ";
            file << tensor_quantum_numbers[MUI] << " ";
            file << tensor_quantum_numbers[SSI] << "   ";
            file << tensor_quantum_numbers[LM0] << " ";
            file << tensor_quantum_numbers[MU0] << " ";
            file << tensor_quantum_numbers[SS0] << "   ";
            file << k0 << " " << ll0 << " " << jj0 << " ";
            for (int32_t a0 = 0; a0 < a0max; ++a0) {
               //obtain index pointing to [k0][rh0][J0] element in tdbmes[L0]
               size_t index = k0 * a0max * nJJ0s + a0 * nJJ0s + iJJ0;
               file << std::fixed << std::setw(19) << densities[ill0][index] << " ";
            }
            file << std::endl;
            ndensities++;
         }
      }
   }
   return ndensities;
}

// n1 n2 w0 2L0 k0 2J0  <f||T||i>
/* 
void StoreSU3obdmes_LL0_K0_JJ0(
    std::ofstream& file, const std::array<int, 5>& tensor_quantum_numbers,
    const SU3xSU2::LABELS& ir0,
    const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    const std::vector<std::vector<double>>& densities) {
   file.precision(13);
   for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
      int32_t ll0 = ll0_jj0vec[ill0].first;
      int32_t nJJ0s = ll0_jj0vec[ill0].second.size();
      int32_t k0max = SU3::kmax(ir0, ll0 / 2);
      int32_t index = 0;
      for (int32_t k0 = 0; k0 < k0max; ++k0) {
         for (int32_t iJJ0 = 0; iJJ0 < nJJ0s; ++iJJ0) {
            int jj0 = ll0_jj0vec[ill0].second[iJJ0];
            file << tensor_quantum_numbers[0] << " ";
            file << tensor_quantum_numbers[1] << "   ";
            file << tensor_quantum_numbers[2] << " ";
            file << tensor_quantum_numbers[3] << " ";
            file << tensor_quantum_numbers[4] << "   ";
            file << ll0 << " " << k0 << " " << jj0 << " " << std::fixed << std::setw(19)
                 << densities[ill0][index] << std::endl;
            ++index;
         }
      }
   }
}
*/

// n1 n2 w0 k0 2L0 2J0  <f||T||i>
int StoreSU3obdmes_K0_LL0_JJ0(
    std::ofstream& file, const std::array<int, 5>& tensor_quantum_numbers,
    const SU3xSU2::LABELS& ir0,
    const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    const std::vector<std::vector<double>>& densities, int type) {
   file.precision(13);
   int ndensities = 0;
   for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
      int32_t ll0 = ll0_jj0vec[ill0].first;
      int32_t nJJ0s = ll0_jj0vec[ill0].second.size();
      int32_t k0max = SU3::kmax(ir0, ll0 / 2);
      int32_t index = 0;
      for (int32_t k0 = 0; k0 < k0max; ++k0) {
         for (int32_t iJJ0 = 0; iJJ0 < nJJ0s; ++iJJ0) {
            int jj0 = ll0_jj0vec[ill0].second[iJJ0];
            file << tensor_quantum_numbers[0] << " ";
            file << tensor_quantum_numbers[1] << "   ";
            file << tensor_quantum_numbers[2] << " ";
            file << tensor_quantum_numbers[3] << " ";
            file << tensor_quantum_numbers[4] << "   ";
            file << k0 << " " << ll0 << " " << jj0 << " ";
            if (type == nucleon::PROTON)
            {
               file << " p ";
            }
            else 
            {
               file << " n ";
            }
            file << std::fixed << std::setw(19) << densities[ill0][index] << std::endl;
            ndensities++;
            ++index;
         }
      }
   }
   return ndensities;
}

template <typename real_type>
void ShowMatrixElements(const std::vector<real_type>& me, std::vector<uint32_t>& positions,
                        uint32_t nrows, uint32_t ncols, int32_t ntensors) {
   for (size_t ipos = 0; ipos < positions.size(); ipos += 2) {
      const uint32_t firstRow = positions[ipos];
      const uint32_t firstColumn = positions[ipos + 1];
      uint32_t index = 0;
      for (uint32_t i = 0; i < nrows; ++i) {
         for (uint32_t j = 0; j < ncols; ++j) {
            /*
                        int32_t nzeros = 0;
                        for (int32_t icell = 0; icell < ntensors; ++icell) {
                           if (fabs(me[index + icell]) < 1.0e-7) {
                              nzeros++;
                           }
                        }
                        if (nzeros < ntensors) {
            */
            std::cout << i + firstRow << " " << j + firstColumn << " ";
            for (int icell = 0; icell < ntensors; ++icell) {
               assert(index == (i * ncols + j) * ntensors + icell);
               std::cout << me[index + icell] << " ";
            }
            std::cout << std::endl;
            //            }
            index += ntensors;
         }
      }
   }
}

template void ShowMatrixElements(const std::vector<double>& me, std::vector<uint32_t>& positions,
                                 uint32_t nrows, uint32_t ncols, int32_t ntensors);

std::vector<std::vector<double>> AllocateDensitiesVectors(
    const std::vector<int32_t>& ntensors_LL0) {
   std::vector<std::vector<double>> densities(ntensors_LL0.size());
   for (size_t ill0 = 0; ill0 < ntensors_LL0.size(); ++ill0) {
      densities[ill0].resize(ntensors_LL0[ill0], 0.0);
   }
   return densities;
}
