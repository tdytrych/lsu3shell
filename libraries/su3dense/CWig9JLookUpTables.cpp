#include "CWig9JLookUpTables.h"

void Generate9JLookUpTables(int32_t Af, int32_t Ai, int32_t SS0, int32_t JJf, int32_t JJi,
                            const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                            Wigner9JCache& wig9j_cached) {
   int32_t SSfmax = Af;
   int32_t SSimax = Ai;

   wig9j_cached.resize(ll0_jj0vec.size());
   for (size_t ill0 = 0; ill0 < ll0_jj0vec.size(); ++ill0) {
      int32_t ll0 = ll0_jj0vec[ill0].first;
      wig9j_cached[ill0].resize(SSfmax + 1);
      for (int32_t SSf = (SSfmax % 2); SSf <= SSfmax; SSf += 2) {
         wig9j_cached[ill0][SSf].resize(SSimax + 1);
         for (int32_t SSi = (SSimax % 2); SSi <= SSimax; SSi += 2) {
            if (SU2::mult(SSi, SS0, SSf)) {
               wig9j_cached[ill0][SSf][SSi].reset(
                   new CWig9JLookUpTable(ll0, SS0, JJi, JJf, SSi, SSf, ll0_jj0vec[ill0].second));
            }
         }
      }
   }
}

void ShowWig9JMemoryFootPrint(
    int32_t SS0, const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    const std::vector<std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>>& wig9j_cached) {
   // total number of allocated CWig9JLookUpTable instances
   int32_t num_allocated_instances = 0;
   // total number of unique_ptrs<CWig9JLookUpTable> instances stored in wig9j_cached
   int32_t num_unique_ptrs = 0;
   // amount of memory reserved for cache itself
   int32_t memory_structure = 0;
   // amount of memory used to store data CWig9JLookUpTable instances
   int32_t memory_data = 0;
   int32_t nll0s = wig9j_cached.size();
   int32_t numSSf = wig9j_cached.front().size();

   memory_structure = nll0s * sizeof(std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>);

   for (int ill0 = 0; ill0 < nll0s; ++ill0) {
      int32_t nJJ0s = ll0_jj0vec[ill0].second.size();
      // let's iterate over both integers and half-integer Sf values
      for (int iSSf = 0; iSSf < numSSf; ++iSSf) {
         // add into account size of data structure that holds Si values
         memory_structure += sizeof(std::vector<std::unique_ptr<CWig9JLookUpTable>>);
         // nearly half of Sf values are not physically allowd
         // and hence their Si arrays were not created
         if (!wig9j_cached[ill0][iSSf].empty()) {
            // some Si values are not physically allowed, but they still hold
            // unique_ptr<CWig9JLookUpTable> that points to nullptr
            num_unique_ptrs += wig9j_cached[ill0][iSSf].size();
            // iterate over both integers and half-integers Si values
            for (int iSSi = 0; iSSi < wig9j_cached[ill0][iSSf].size(); ++iSSi) {
               if (wig9j_cached[ill0][iSSf][iSSi]) {
                  num_allocated_instances++;
                  memory_data += wig9j_cached[ill0][iSSf][iSSi]->get_memory_footprint(nJJ0s);
               }
            }
         }
      }
   }
   memory_structure += num_unique_ptrs * sizeof(std::unique_ptr<CWig9JLookUpTable>);

   std::cout << "9J cache holds " << num_unique_ptrs
             << " unique_ptrs<CWig9JLookUpTable> ouf of which "
             << (num_unique_ptrs - num_allocated_instances) << " are nullptrs" << std::endl;
   std::cout << "Size of 9J cache data structure:" << memory_structure << " [b]" << std::endl;
   std::cout << "Size of " << num_allocated_instances
             << " of CWig9JLookUpTable and their inner data:" << memory_data << " [b]" << std::endl;
}

void Generate9SLookUpTable(int32_t Zf, int32_t Zi, int32_t N, int32_t SS0,
                           Wigner9SCached& wig9s_cached) {
   // maximal 2S1 == Z
   int32_t SS1max = Zf;
   int32_t SS1pmax = Zi;
   int32_t SS2max = N;

   wig9s_cached.resize(SS1max + 1);
   for (int32_t SS1 = (SS1max % 2); SS1 <= SS1max; SS1 += 2) {
      wig9s_cached[SS1].resize(SS1pmax + 1);
      for (int32_t SS1p = (SS1pmax % 2); SS1p <= SS1pmax; SS1p += 2) {
         if (SU2::mult(SS1p, SS0, SS1)) {
            wig9s_cached[SS1][SS1p].resize(SS2max + 1);
            for (int32_t SS2 = (SS2max % 2); SS2 <= SS2max; SS2 += 2) {
               wig9s_cached[SS1][SS1p][SS2].reset(new CWig9STable(SS1p, SS1, SS2, SS0));
            }
         }
      }
   }
   //  ShowWig9SMemoryFootPrint(ir0.S2, wig9s_cached);
}

void ShowWig9SMemoryFootPrint(int32_t SS0, const Wigner9SCached& wig9s) {
   int32_t num_allocated_instances = 0;
   int32_t num_unique_ptrs = 0;
   int32_t memory_structure = 0;
   int32_t memory_data = 0;
   int32_t structure_data = 0;

   structure_data = wig9s.size() * sizeof(std::vector<std::vector<std::unique_ptr<CWig9STable>>>);

   for (int32_t iSS1 = 0; iSS1 < wig9s.size(); ++iSS1) {
      for (int32_t iSS1p = 0; iSS1p < wig9s[iSS1].size(); ++iSS1p) {
         memory_structure += sizeof(std::vector<std::unique_ptr<CWig9STable>>);
         if (!wig9s[iSS1][iSS1p].empty()) {
            num_unique_ptrs += wig9s[iSS1][iSS1p].size();
            for (int32_t iSS2 = 0; iSS2 < wig9s[iSS1][iSS1p].size(); ++iSS2) {
               if (wig9s[iSS1][iSS1p][iSS2]) {
                  num_allocated_instances++;
                  memory_data += wig9s[iSS1][iSS1p][iSS2]->get_memory_footprint();
               }
            }
         }
      }
   }
   memory_structure += num_unique_ptrs * sizeof(std::unique_ptr<CWig9STable>);

   std::cout << "9S cache holds " << num_unique_ptrs << " unique_ptrs<CWig9STable> ouf of which "
             << (num_unique_ptrs - num_allocated_instances) << " are nullptrs" << std::endl;
   std::cout << "Size of 9S cache data structure:" << memory_structure << " [b]" << std::endl;
   std::cout << "Size of " << num_allocated_instances
             << " of CWig9STable and their inner data:" << memory_data << " [b]" << std::endl;
}

void InitializeWignerSU2Coeffs(
    const lsu3::CncsmSU3xSU2Basis& row_basis, const lsu3::CncsmSU3xSU2Basis& col_basis,
    const SU3xSU2::LABELS& ir0,
    const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    Wigner9JCache& wig9j_cached, Wigner9SCached& wig9s_cached) {
   // Initialize innner arrays needed for computation of wigner 9j coefficients
   // 2Jmax argument of wigner 9j coefficients
   // In our case the largest values are Li and Lf as J are small and
   // Si Sf < Amax/2 which will be Amax = 40

   const int32_t Af = row_basis.NProtons() + row_basis.NNeutrons();
   const int32_t Ai = col_basis.NProtons() + col_basis.NNeutrons();
   const int32_t Zf = row_basis.NProtons();
   const int32_t Zi = col_basis.NProtons();
   const int32_t JJf = row_basis.JJ();
   const int32_t JJi = col_basis.JJ();
   const int32_t N = col_basis.NNeutrons();
   assert(N == row_basis.NNeutrons());

   const int32_t SS0 = ir0.S2;

   Generate9JLookUpTables(Af, Ai, SS0, JJf, JJi, ll0_jj0vec, wig9j_cached);
   // ShowWig9JMemoryFootPrint(ir0.S2, ll0_jj0vec, wig9j_cached);

   // [S1][S1'][S2] ---> CWig9S( {2Sf 2Si}---> wig9jj{ . . Si  . . .   . . Sf} * Pi_{Si . .}
   Generate9SLookUpTable(Zf, Zi, N, SS0, wig9s_cached);
   // ShowWig9SMemoryFootPrint(ir0.S2, wig9s_cached);

}
