#include "SU3Kernels.h"

// result[af][ai][a0][rhot] ---> < a1 1 rhof ||| T^a0 ||| a1p rhoi>_rhot
// af[a1 ][rhof]  = a1 * rhofmax + rhof
// ai[a1p][rhoi] = a1p * rhoimax + rhoi
template <typename real_type>
void su39lm_x_rmes(int32_t a1max, int32_t rhofmax, int32_t a1p_max, int32_t rhoimax, int32_t a0_max,
                   int32_t rhot_max, int32_t rho1_max, const double* su39lm, const real_type* rmes,
                   double* result) {
   // strides for accessing su3 9-lm symbols
   const int32_t rhotmax_rho1max = rhot_max * rho1_max;
   const int32_t rhoimax_rhotmax_rho1max = rhoimax * rhotmax_rho1max;
   // indices used for addressing computation of input rme arrays
   const int32_t a0max_rho1max = a0_max * rho1_max;
   const int32_t a1pmax_a0max_rho1max = a1p_max * a0max_rho1max;

   int32_t index_result = 0;
   int32_t irme_a1 = 0;
   for (int32_t a1 = 0; a1 < a1max; ++a1) {
      int32_t isu3f = 0;
      for (int32_t irhof = 0; irhof < rhofmax; ++irhof) {
         int32_t irme_a1p = 0;
         for (int32_t a1p = 0; a1p < a1p_max; ++a1p) {
            int32_t isu3i = 0;
            for (int32_t irhoi = 0; irhoi < rhoimax; ++irhoi) {
               // irme ---> <a1 ||| T^{a0:0} ||| a1p >_{rho1:0}
               // irme = a1 * a1p_max * a0_max * rho1_max + a1p * a0_max * rho1_max
               int32_t irme = irme_a1 + irme_a1p;
               for (int32_t ia0 = 0; ia0 < a0_max; ++ia0) {
                  // isu3 ---> su39lm[rhof][rhoi][rhot:0][rho1:0]
                  // isu3  = rhof*rhoimax*rhotmax*rho1max + rhoi*rhotmax*rho1max
                  int32_t isu3 = isu3f + isu3i;

                  for (int32_t irhot = 0; irhot < rhot_max; ++irhot) {
                     double rme = 0.0;
                     for (int32_t rho1 = 0; rho1 < rho1_max; ++rho1) {
                        rme += su39lm[isu3++] * double(rmes[irme + rho1]);
                     }
                     result[index_result++] = rme;
                  }  // rhot

                  // irme = a1*a1p_max*a0_max*rho1_max + a1p*a0_max*rho1_max + a0*rho1_max
                  irme += rho1_max;
               }  // a0

               // isu3i = rhoi*rhotmax*rho1max
               isu3i += rhotmax_rho1max;
            }  // rhoi

            // irme_a1p = a1p * a0_max * rho1_max
            irme_a1p += a0max_rho1max;
         }  // a1p

         // isu3f = rhof*rhoimax*rhotmax*rho1max
         isu3f += rhoimax_rhotmax_rho1max;
      }  // rhof

      // irme_a1 = a1 * a1p_max * a0_max * rho1_max
      irme_a1 += a1pmax_a0max_rho1max;
   }  // a1
}

void GenerateSpinlessRMEs(int32_t a1max, const SU3::LABELS& wp_row, int32_t a1p_max,
                          const SU3::LABELS& wp_col, const SU3::LABELS& wn, int32_t a0max,
                          const SU3::LABELS& ir0, const float* ipjp_rmes,
                          // we need to supply these buffers so that we avoid
                          // frequent memory allocation across multiple threads
                          // which is normally nonscaling operation
                          SU3_VEC& wpn_col_vec, SU3_VEC& wpn_row_vec,
                          std::vector<double>& su39lmPtoPN,
                          SpinlessRMEsCache& l2m2_cache) {
   static CSU39lm<double> su39lmComputer(2500000, 2500000);
   const int32_t rho1max = SU3::mult(wp_col, ir0, wp_row);
   assert(rho1max > 0);

   wpn_col_vec.clear();
   wpn_row_vec.clear();
   // Notice that we are doing SU(3) coupling. Model space selection
   // rules cannot be applied do downselect wpn_row_vec and wpn_col_vec
   // as we do not have any information about neutron
   // spin S2 and the total spin Sf/Si.
   SU3::Couple(wp_row, wn, wpn_row_vec);
   SU3::Couple(wp_col, wn, wpn_col_vec);

   // Compute S2-less and a2-less RMEs for ALL possible wf and wi in
   // <(ip in) wf ||| T0 ||| // (jp in) wi>
   for (auto& wpn_row : wpn_row_vec) {
      int32_t rho_row_max = wpn_row.rho;
      for (auto& wpn_col : wpn_col_vec) {
         int32_t rho_col_max = wpn_col.rho;
         int32_t rhotmax = SU3::mult(wpn_col, ir0, wpn_row);
         if (!rhotmax) {
            continue;
         }

         // number of 9-(lm mu) coefficients
         size_t nsu39lm = rho_col_max * rho_row_max * rho1max * rhotmax;

         su39lmPtoPN.clear();
         su39lmPtoPN.resize(nsu39lm, 0.0);

         // Compute 9-(lm mu) symbols and store them in [rhof][rhoi][rhot][rho1] order
         su39lmComputer.Get9lmPtoPN_fit1(rho_row_max, rho_col_max, rhotmax, rho1max, wp_col, ir0,
                                         wp_row, wn, wpn_col, wpn_row, su39lmPtoPN.data());

         // number of S2- and a2- independent RMEs
         int32_t nrmes = a1max * rho_row_max * a1p_max * rho_col_max * a0max * rhotmax;
         std::vector<double> rmes(nrmes, 0.0);
         su39lm_x_rmes(a1max, rho_row_max, a1p_max, rho_col_max, ir0.rho, rhotmax, rho1max,
                       su39lmPtoPN.data(), ipjp_rmes, rmes.data());

         l2m2_cache[std::make_pair(wpn_row, wpn_col)] = std::move(rmes);
      }
   }
}

// This function computes S2-independent matrix elements
// <af wpn_row Sf kf Lf Jf || T^a0 (lm0 mu0) S0 k0 L0 J0 || ai wpn_col Si ki Li Ji>
// for all values a0 k0 and J0.
// The order of matrix elements stored in vector<rea_type> me is (T_L0)[i][j][k0][a0][J0]
// where the order of [i] rows is given by [Lf][kf][af] quantum numbers
// and similarly [j] columns are given by [Li][ki][ai] quantum numbers.
// <af wpn_row Sf kf Lf Jf || T^a0 (lm0 mu0) S0 k0 L0 J0 || ai wpn_col Si ki Li Ji> =
// = (T_L0)[Lf][kf][af][Li][ki][ai][k0][a0][J0]
////////////////////////////////////
// SU(3)>SO(3) coupling coefficients:
// WigEckSU3SO3CG* su3so3cg ---> < wpn_col ki=* Li=*; ir0 k0=* L0 || wpn_row kf=* Lf=*>rhot=*
////////////////////////////////////
// order of SU(3)>SO(3) coupling coefficients for a given quantum numbers Li L0 Lf:
// < wpn_col ki Li; ir0 k0 L0 || wpn_row kf Lf>rhot : [kf][ki][k0][rhot]
//
////////////////////////////////////
// SU(3) RMEs
// std::vector<real_type>& rmes
////////////////////////////////////
// order of SU(3) RMEs stored in array:
// <af wpn_row Sf || T^a0 ir0 || ai wpn_col>rhot: [af][ai][a0][rhot]
//
// wpn_row ----> (lmf muf)Sf
// wpn_col ----> (lmi mui)Si
//
void ComputeME_wpn_row_col(int32_t afmax, const IRREPBASIS& wpn_row, int32_t aimax,
                           const IRREPBASIS& wpn_col, int32_t rhotmax, int32_t a0max, int32_t k0max,
                           int32_t SS0, int32_t ll0, const std::vector<int32_t>& jj0_vec,
                           const CWig9JLookUpTable& wig9jcoeffs, const WigEckSU3SO3CG* su3so3cg,
                           const double* rmes, std::vector<double>& me_wpn_row_col) {
   const int32_t num_jj0s = jj0_vec.size();
   const int32_t ncols = aimax * wpn_col.dim();
   int32_t rowLfPosition = 0;
   // iterate over Lf
   // for (wpn_row.rewind(); !wpn_row.IsDone(); wpn_row.nextL()) {
   for (auto iter = wpn_row.rewind(); !wpn_row.IsDone(iter); wpn_row.nextL(iter)) {
      int32_t llf = wpn_row.L(iter);
      int32_t kfmax = wpn_row.kmax(iter);

      int32_t colLiPosition = 0;
      // iterate over Li
      for (auto iter = wpn_col.rewind(); !wpn_col.IsDone(iter);
           colLiPosition += aimax * wpn_col.kmax(iter), wpn_col.nextL(iter)) {
         int32_t lli = wpn_col.L(iter);
         // Li x L0 --> Lf ?
         if (!SO3::mult(lli, ll0, llf)) {  // if not ==> move to another Li
            continue;
         }
         int32_t kimax = wpn_col.kmax(iter);

         const double* su2factor = wig9jcoeffs.find(llf, lli);

         // order of following elements: [kf][ki][k0][rhot]
         // initially point to <ki=0 Li; k0=0 L0 || kf=0 Lf>rhot=0 i.e. [kf=0][ki=0][k0=0][rhot=0]
         double* pSU3SO3CGs = su3so3cg->SU3SO3CGs(llf, lli);

         for (int32_t kf = 0; kf < kfmax; ++kf) {
            for (int32_t ki = 0; ki < kimax; ++ki) {
               for (int32_t k0 = 0; k0 < k0max; ++k0) {
                  // we are iterating over the entire array rmes[af][ai][a0][rhot]
                  int32_t irmes = 0;
                  // Compute matrix row position
                  int32_t irow = rowLfPosition + kf * afmax;
                  for (int32_t af = 0; af < afmax; ++af) {
                     // Compute matrix column position
                     int32_t icol = colLiPosition + ki * aimax;
                     for (int32_t ai = 0; ai < aimax; ++ai) {
                        // index ---> [i][j][k0][a0][J0:0]
                        int32_t index = ((irow * ncols + icol) * k0max + k0) * a0max * num_jj0s;
                        for (int32_t a0 = 0; a0 < a0max; ++a0) {
                           // matrix_element == (T_L0)[i][j][k0][a0][J0]
                           // [i] == [Lf][kf][af]
                           // [j] == [Li][ki][ai]
                           double matrix_element = 0.0;
                           for (int32_t rhot = 0; rhot < rhotmax; ++rhot) {
                              matrix_element += pSU3SO3CGs[rhot] * rmes[irmes];
                              irmes = irmes + 1;
                           }

                           assert(index == ((irow * ncols + icol) * k0max * a0max * num_jj0s +
                                            k0 * a0max * num_jj0s + a0 * num_jj0s));

                           for (int32_t ijj0 = 0; ijj0 < num_jj0s; ++ijj0) {
                              me_wpn_row_col[index] = su2factor[ijj0] * matrix_element;
                              index = index + 1;
                           }
                        }  // a0
                        icol = icol + 1;
                     }  // ai
                     irow = irow + 1;
                  }  // af
                  // advance pointer to the next [kf][ki][k0][rhot=0]
                  pSU3SO3CGs += rhotmax;
               }  // k0
            }     // ki
         }        // kf

      }  // Li
      rowLfPosition += afmax * kfmax;
   }  // Lf
}

// Consider a2max = 1
// a2max > 1 should be computed from a2max=1 resulting matrix elements
void ComputeMatrixElements(
    const lsu3::CncsmSU3xSU2Basis& row_basis, const lsu3::CncsmSU3xSU2Basis& col_basis,
    uint16_t a1max, const SU3xSU2::LABELS& wp_row, uint16_t a1p_max, const SU3xSU2::LABELS& wp_col,
    uint32_t ipin_block, uint32_t jpin_block, int32_t a0max, int32_t k0max,
    const SU3xSU2::LABELS& ir0, int32_t ll0, const std::vector<int32_t>& jj0_vec,
    WigEckSU3SO3CGTable* su3so3_ir0l0_table,
    const std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>& wig9j_cached,
    const CWig9STable& wig9s, const SpinlessRMEsCache& lm2mu2_RMEs_cache,
    std::vector<double>& me_wpn_row_col, std::vector<double>& me) {
   // number of tensors we are computing
   const int32_t ntensors = k0max * a0max * jj0_vec.size();
   const int32_t ncols = col_basis.NumberOfStatesInBlock(jpin_block);
   SU3xSU2::LABELS wn(row_basis.getNeutronSU3xSU2(row_basis.getNeutronIrrepId(ipin_block)));

   const std::vector<std::tuple<uint8_t, uint8_t, double>>& wig9s_phases = wig9s.data_;
   auto currentSSfIter = wig9s_phases.begin();
   double spin_phase;

   uint32_t ibegin = row_basis.blockBegin(ipin_block);
   uint32_t iend = row_basis.blockEnd(ipin_block);

   uint32_t jbegin = col_basis.blockBegin(jpin_block);
   uint32_t jend = col_basis.blockEnd(jpin_block);

   uint32_t currentRow = 0;
   for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn) {
      const auto& wpn_row = row_basis.Get_Omega_pn_Basis(iwpn);
      //	find first element with {Sf:wpn_row.S2, Si, phase_spin}
      currentSSfIter = std::find_if(currentSSfIter, wig9s_phases.end(),
                                    [&wpn_row](const std::tuple<uint8_t, uint8_t, double>& i) {
                                       return std::get<0>(i) == wpn_row.S2;
                                    });

      assert(currentSSfIter != wig9s_phases.end() && std::get<0>(*currentSSfIter) == wpn_row.S2);

      int32_t afmax = a1max * 1 * SU3::mult(wp_row, wn, wpn_row);
      int32_t fmax = afmax * wpn_row.dim();

      int32_t currentColumn = 0;
      // we are interested in {Sf:wpn_row.S2 Si, phase_spin} values
      auto currentSSfSSiIter = currentSSfIter;
      spin_phase = std::get<2>(*currentSSfSSiIter);
      for (int jwpn = jbegin; jwpn < jend; ++jwpn) {
         // IRREPBASIS wpn_col(col_basis.Get_Omega_pn_Basis(jwpn));
         const auto& wpn_col = col_basis.Get_Omega_pn_Basis(jwpn);
         int32_t aimax = a1p_max * 1 * SU3::mult(wp_col, wn, wpn_col);
         int32_t imax = aimax * wpn_col.dim();

         int32_t rhotmax = SU3::mult(wpn_col, ir0, wpn_row);

         if (SU2::mult(wpn_col.S2, ir0.S2, wpn_row.S2) && rhotmax) {
            // current value of Si = wpn_col.S2 is not equal to then we need to find appropriate
            // spin_phase
            if (std::get<1>(*currentSSfSSiIter) != wpn_col.S2) {
               // find a given {Sf, Si, spin_phase}
               currentSSfSSiIter =
                   std::find_if(currentSSfSSiIter, wig9s_phases.end(),
                                [&wpn_col](const std::tuple<uint8_t, uint8_t, double>& i) {
                                   return std::get<1>(i) == wpn_col.S2;
                                });

               spin_phase = std::get<2>(*currentSSfSSiIter);
            }

            assert(currentSSfSSiIter != wig9s_phases.end() &&
                   std::get<0>(*currentSSfSSiIter) == wpn_row.S2 &&
                   std::get<1>(*currentSSfSSiIter) == wpn_col.S2);
            /*
                        assert(fabs(spin_phase -
                                    sqrt((wpn_col.S2 + 1) * (ir0.S2 + 1) * (wp_row.S2 + 1) * (wn.S2
               + 1)) *
                                        wig9jj(wp_col.S2, wn.S2, wpn_col.S2, ir0.S2, 0, ir0.S2,
               wp_row.S2,
                                               wn.S2, wpn_row.S2)) < 1.e-10);
            */
            WigEckSU3SO3CG* su3so3cgs = su3so3_ir0l0_table->GetWigEckSU3SO3CG(wpn_row, wpn_col);

            const SU3::LABELS& lkey = wpn_row;
            const SU3::LABELS& rkey = wpn_col;
            auto key = std::make_pair(lkey, rkey);

            const auto& rmes = lm2mu2_RMEs_cache.find(key);
            assert(rmes != lm2mu2_RMEs_cache.end());

            int32_t nme =
                afmax * wpn_row.dim() * aimax * wpn_col.dim() * a0max * k0max * jj0_vec.size();

            me_wpn_row_col.clear();
            me_wpn_row_col.resize(nme, 0.0);

            ComputeME_wpn_row_col(afmax, wpn_row, aimax, wpn_col, rhotmax, a0max, k0max, ir0.S2,
                                  ll0, jj0_vec, *wig9j_cached[wpn_row.S2][wpn_col.S2], su3so3cgs,
                                  rmes->second.data(), me_wpn_row_col);

            std::transform(me_wpn_row_col.begin(), me_wpn_row_col.end(), me_wpn_row_col.begin(),
                           std::bind2nd(std::multiplies<double>(), spin_phase));

            auto me_wpn_iter = me_wpn_row_col.cbegin();
            auto me_iter = me.begin() + (currentRow * ncols + currentColumn) * ntensors;
            for (int32_t i = 0; i < fmax; ++i) {
               std::copy(me_wpn_iter, me_wpn_iter + imax * ntensors, me_iter);
               me_wpn_iter += imax * ntensors;
               me_iter += ncols * ntensors;
            }
         }
         currentColumn += imax;
      }
      currentRow += fmax;
   }
}

/////////////////////////////////////////////////////////////////////////////
// a2max > 1 case!
/////////////////////////////////////////////////////////////////////////////
void ComputeME_wpn_row_col(int32_t a1max, int32_t rhofmax, int32_t a1p_max, int32_t rhoimax,
                           int32_t a2max, const IRREPBASIS& wpn_row, const IRREPBASIS& wpn_col,
                           int32_t rhotmax, int32_t a0max, int32_t k0max, int32_t SS0, int32_t ll0,
                           const std::vector<int32_t>& jj0_vec,
                           const CWig9JLookUpTable& wig9jcoeffs, const WigEckSU3SO3CG* su3so3cg,
                           const double* rmes, std::vector<double>& me_wpn_row_col) {
   const int32_t num_jj0s = jj0_vec.size();
   const int32_t afmax = a1max * a2max * rhofmax;
   const int32_t aimax = a1p_max * a2max * rhoimax;
   const int32_t ncols = aimax * wpn_col.dim();
   // position of the first state with a given Lf in the bra basis
   int32_t rowLfPosition = 0;
   // iterate over Lf
   for (auto iter = wpn_row.rewind(); !wpn_row.IsDone(iter); wpn_row.nextL(iter)) {
      int32_t llf = wpn_row.L(iter);
      int32_t kfmax = wpn_row.kmax(iter);

      // position of the first state with a given Li in the ket basis
      int32_t colLiPosition = 0;
      // iterate over Li
      for (auto iter = wpn_col.rewind(); !wpn_col.IsDone(iter); wpn_col.nextL(iter)) {
         int32_t lli = wpn_col.L(iter);
         int32_t kimax = wpn_col.kmax(iter);
         // Li x L0 --> Lf ?
         if (SO3::mult(lli, ll0, llf)) {  // if not ==> move to next Li
            const double* su2factor = wig9jcoeffs.find(llf, lli);

            // order of following elements: [kf][ki][k0][rhot]
            // initially point to <ki=0 Li; k0=0 L0 || kf=0 Lf>rhot=0 i.e.
            // [kf=0][ki=0][k0=0][rhot=0]
            double* pSU3SO3CGs = su3so3cg->SU3SO3CGs(llf, lli);

            for (int32_t kf = 0; kf < kfmax; ++kf) {
               // row position of the first state with Lf kf af=0
               int32_t irowLf_kf = rowLfPosition + kf * afmax;
               for (int32_t ki = 0; ki < kimax; ++ki) {
                  // column position of the first state with Li ki ai=0
                  int32_t icolLi_ki = colLiPosition + ki * aimax;

                  for (int32_t k0 = 0; k0 < k0max; ++k0) {
                     // We will be iterating over the entire array rmes[af][ai][a0][rhot]
                     int32_t irmes = 0;
                     for (int32_t a1 = 0; a1 < a1max; ++a1) {
                        for (int32_t rhof = 0; rhof < rhofmax; ++rhof) {
                           // Af[a1][a2][rhof]
                           // Af = a1 * a2max * rhofmax + a2*rhofmax + rhof;
                           // for a2=0 we get:
                           int Af = a1 * a2max * rhofmax + rhof;
                           for (int32_t a1p = 0; a1p < a1p_max; ++a1p) {
                              for (int32_t rhoi = 0; rhoi < rhoimax; ++rhoi) {
                                 // for a2=0 we get:
                                 int Ai = a1p * a2max * rhoimax + rhoi;
                                 for (int32_t a0 = 0; a0 < a0max; ++a0) {
                                    // matrix_element == (T_L0)[i][j][k0][a0][J0]
                                    // [i] == [Lf][kf][a1][rhof]
                                    // [j] == [Li][ki][a1][rhoi]
                                    double matrix_element = 0.0;
                                    for (int32_t rhot = 0; rhot < rhotmax; ++rhot) {
                                       matrix_element += pSU3SO3CGs[rhot] * rmes[irmes];
                                       irmes = irmes + 1;
                                    }

                                    int irow = irowLf_kf + Af;
                                    int icol = icolLi_ki + Ai;
                                    for (int a2 = 0; a2 < a2max; ++a2) {
                                       size_t index =
                                           (irow * ncols + icol) * k0max * a0max * num_jj0s +
                                           k0 * a0max * num_jj0s + a0 * num_jj0s;
                                       for (int32_t ijj0 = 0; ijj0 < num_jj0s; ++ijj0) {
                                          me_wpn_row_col[index] = su2factor[ijj0] * matrix_element;
                                          index = index + 1;
                                       }
                                       //
                                       irow += rhofmax;
                                       icol += rhoimax;
                                    }
                                 }  // a0
                              }     // rhoi
                           }        // a1p
                        }           // rhof
                     }              // a1
                     // advance pointer to the next [kf][ki][k0][rhot=0]
                     pSU3SO3CGs += rhotmax;
                  }  // k0
               }     // ki
            }        // kf
         }
         // position of next Li
         colLiPosition += aimax * kimax;
      }  // Li
      // position of next Lf
      rowLfPosition += afmax * kfmax;
   }  // Lf
}
/////////////////////////////////////////////////////////////////////////////
// a2max > 1 case!
/////////////////////////////////////////////////////////////////////////////
void ComputeMatrixElements(
    const lsu3::CncsmSU3xSU2Basis& row_basis, const lsu3::CncsmSU3xSU2Basis& col_basis,
    uint16_t a1max, const SU3xSU2::LABELS& wp_row, uint16_t a1p_max, const SU3xSU2::LABELS& wp_col,
    uint32_t ipin_block, uint32_t jpin_block, uint16_t a2max, int32_t a0max, int32_t k0max,
    const SU3xSU2::LABELS& ir0, int32_t ll0, const std::vector<int32_t>& jj0_vec,
    WigEckSU3SO3CGTable* su3so3_ir0l0_table,
    const std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>& wig9j_cached,
    const CWig9STable& wig9s, const SpinlessRMEsCache& lm2mu2_RMEs_cache,
    std::vector<double>& me_wpn_row_col, std::vector<double>& me) {
   // number of tensors we are computing
   const int32_t ntensors = k0max * a0max * jj0_vec.size();
   const int32_t ncols = col_basis.NumberOfStatesInBlock(jpin_block);
   SU3xSU2::LABELS wn(row_basis.getNeutronSU3xSU2(row_basis.getNeutronIrrepId(ipin_block)));

   const std::vector<std::tuple<uint8_t, uint8_t, double>>& wig9s_phases = wig9s.data_;
   auto currentSSfIter = wig9s_phases.begin();
   double spin_phase;

   uint32_t ibegin = row_basis.blockBegin(ipin_block);
   uint32_t iend = row_basis.blockEnd(ipin_block);

   uint32_t jbegin = col_basis.blockBegin(jpin_block);
   uint32_t jend = col_basis.blockEnd(jpin_block);

   uint32_t currentRow = 0;
   for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn) {
      const auto& wpn_row = row_basis.Get_Omega_pn_Basis(iwpn);
      //	find first element with {Sf:wpn_row.S2, Si, phase_spin}
      currentSSfIter = std::find_if(currentSSfIter, wig9s_phases.end(),
                                    [&wpn_row](const std::tuple<uint8_t, uint8_t, double>& i) {
                                       return std::get<0>(i) == wpn_row.S2;
                                    });

      assert(currentSSfIter != wig9s_phases.end() && std::get<0>(*currentSSfIter) == wpn_row.S2);

      int32_t rhofmax = SU3::mult(wp_row, wn, wpn_row);
      int32_t afmax = a1max * a2max * rhofmax;
      // number of states < (ip in) af=* wpn_row kf=* Lf=* J>
      int32_t fmax = afmax * wpn_row.dim();

      int32_t currentColumn = 0;
      // we are interested in {Sf:wpn_row.S2 Si, phase_spin} values
      auto currentSSfSSiIter = currentSSfIter;
      spin_phase = std::get<2>(*currentSSfSSiIter);
      for (int jwpn = jbegin; jwpn < jend; ++jwpn) {
         // IRREPBASIS wpn_col(col_basis.Get_Omega_pn_Basis(jwpn));
         const auto& wpn_col = col_basis.Get_Omega_pn_Basis(jwpn);
         int32_t rhoimax = SU3::mult(wp_col, wn, wpn_col);
         int32_t aimax = a1p_max * a2max * rhoimax;
         // number of states | (jp in) ai=* wpn_col ki=* Li=* J>
         int32_t imax = aimax * wpn_col.dim();

         int32_t rhotmax = SU3::mult(wpn_col, ir0, wpn_row);

         if (SU2::mult(wpn_col.S2, ir0.S2, wpn_row.S2) && rhotmax) {
            // current value of Si = wpn_col.S2 is not equal to then we need to find appropriate
            // spin_phase
            if (std::get<1>(*currentSSfSSiIter) != wpn_col.S2) {
               // find a given {Sf, Si, spin_phase}
               currentSSfSSiIter =
                   std::find_if(currentSSfSSiIter, wig9s_phases.end(),
                                [&wpn_col](const std::tuple<uint8_t, uint8_t, double>& i) {
                                   return std::get<1>(i) == wpn_col.S2;
                                });

               spin_phase = std::get<2>(*currentSSfSSiIter);
            }

            assert(currentSSfSSiIter != wig9s_phases.end() &&
                   std::get<0>(*currentSSfSSiIter) == wpn_row.S2 &&
                   std::get<1>(*currentSSfSSiIter) == wpn_col.S2);

            WigEckSU3SO3CG* su3so3cgs = su3so3_ir0l0_table->GetWigEckSU3SO3CG(wpn_row, wpn_col);

            const SU3::LABELS& lkey = wpn_row;
            const SU3::LABELS& rkey = wpn_col;
            auto key = std::make_pair(lkey, rkey);

            const auto& rmes = lm2mu2_RMEs_cache.find(key);
            assert(rmes != lm2mu2_RMEs_cache.end());

            int32_t nme = fmax * imax * a0max * k0max * jj0_vec.size();

            me_wpn_row_col.clear();
            me_wpn_row_col.resize(nme, 0.0);

            ComputeME_wpn_row_col(a1max, rhofmax, a1p_max, rhoimax, a2max, wpn_row, wpn_col,
                                  rhotmax, a0max, k0max, ir0.S2, ll0, jj0_vec,
                                  *wig9j_cached[wpn_row.S2][wpn_col.S2], su3so3cgs, rmes->second.data(),
                                  me_wpn_row_col);

            std::transform(me_wpn_row_col.begin(), me_wpn_row_col.end(), me_wpn_row_col.begin(),
                           std::bind2nd(std::multiplies<double>(), spin_phase));

            auto me_wpn_iter = me_wpn_row_col.cbegin();
            auto me_iter = me.begin() + (currentRow * ncols + currentColumn) * ntensors;
            for (int32_t i = 0; i < fmax; ++i) {
               std::copy(me_wpn_iter, me_wpn_iter + imax * ntensors, me_iter);
               me_wpn_iter += imax * ntensors;
               me_iter += ncols * ntensors;
            }
         }
         currentColumn += imax;
      }
      currentRow += fmax;
   }
}
