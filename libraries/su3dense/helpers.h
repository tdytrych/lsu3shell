#ifndef SU3DENSE_HELPERS_H
#define SU3DENSE_HELPERS_H

#include <vector>
#include <su3dense/tensor_read.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <LSU3/ncsmSU3xSU2Basis.h>

// Allocate vectors that hold resulting densities
std::vector<std::vector<double>> AllocateDensitiesVectors(const std::vector<int32_t>& ntensors_LL0);

// Input:
// a0max ... maximal multiplicity of input tensor
// ir0   ... (lm0 mu0) S0 tensor quantum numbers
// JJf   ... 2Jf total angular momentum of row (i.e. bra) wavefunction
// JJi   ... 2Ji total angular momentum of column (i.e. ket) wavefunction
//
// Algorithm: generate all physically allowed tensor components k0 L0 J0 and store them in
// the following output vectors:
//
// Output:
// ll0_jj0vec   ... ll0_jj0vec[i].first <--- 2L0
//              ... ll0_jj0vec[i]second <--- vector of 2J0's resulting from coupling L0 x S0 and
//                  satisfying Ji x L0 --> Jf
//
// k0max_LL0    ... k0max_LL0[i] <--- k0max for L0 <--- ll0_jj0vec[i].first
//
// ntensors_LL0 ... nTensors_LL0[i] <--- a0max x k0max x size(<J0,...,>)
//                  that is a number of tensors for a given L0 component
//
void GenerateAllowedll0jj0(int32_t JJf, int32_t a0max, const SU3xSU2::LABELS& ir0, int32_t JJ0sel, int32_t JJi,
                           std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                           std::vector<int32_t>& k0max_LL0, std::vector<int32_t>& ntensors_LL0);

template <typename real_type>
void ShowMatrixElements(const std::vector<real_type>& me, std::vector<uint32_t>& positions,
                        uint32_t nrows, uint32_t ncols, int32_t ntensors);

void ShowResults(SU3xSU2::LABELS ir0, int32_t a0max,
                 const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                 const std::vector<std::vector<double>>& densities);

// Store 1B densities on disk 
// n1 n2 w0 2L0 k0 2J0 order of quantum numbers
/*
void StoreSU3obdmes_LL0_K0_JJ0(std::ofstream& file, const std::array<int, 5>& tensor_quantum_numbers,
                  const SU3xSU2::LABELS& ir0,
                  const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                  const std::vector<std::vector<double>>& densities);
*/

// Store 1B densities on disk 
// n1 n2 w0 k0 2L0 2J0 order of quantum numbers
int StoreSU3obdmes_K0_LL0_JJ0(std::ofstream& file, const std::array<int, 5>& tensor_quantum_numbers,
                  const SU3xSU2::LABELS& ir0,
                  const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                  const std::vector<std::vector<double>>& densities, int type);

// Store 2B densities on disk
// n1 n2 n3 n4 lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0 2L0 k0 rho0 JJ0   <f||T^rho0||i>
/*
void StoreSU3tbdmes_LL0_K0_RHO0_JJ0(std::ofstream& file, const std::array<int, 13>& tensor_quantum_numbers,
                  const SU3xSU2::LABELS& ir0, int32_t a0max,
                  const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                  const std::vector<std::vector<double>>& densities);
*/                  
// n1 n2 n3 n4 lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0 k0 2L0 JJ0 {<f||rho0:0||i>, ... <f||T^rho0max-1||i>}
int StoreSU3tbdmes_K0_LL0_JJ0(std::ofstream& file, const std::array<int, 13>& tensor_quantum_numbers,
                  const SU3xSU2::LABELS& ir0, int32_t a0max,
                  const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                  const std::vector<std::vector<double>>& densities);
#endif
