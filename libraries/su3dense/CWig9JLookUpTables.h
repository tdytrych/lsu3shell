#ifndef SU3DENSE_CWIG9JLOOKUPTABLE_H
#define SU3DENSE_CWIG9JLOOKUPTABLE_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <memory>
#include <utility>
#include <vector>

#include <UNU3SU3/UNU3SU3Basics.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <wigxjpf.h>

class CWig9JLookUpTable {
  public:
   CWig9JLookUpTable(int32_t LL0, int32_t SS0, int32_t JJi, int32_t JJf, int32_t SSi, int32_t SSf,
                     const std::vector<int32_t>& JJ0) {
      int32_t LLfmax = JJf + SSf;
      for (int32_t LLf = 0; LLf <= LLfmax; LLf += 2) {
         int32_t LLimax = JJi + SSi;
         for (int32_t LLi = 0; LLi <= LLimax; LLi += 2) {
            if (SU2::mult(LLi, LL0, LLf) && SU2::mult(LLi, SSi, JJi) && SU2::mult(LLf, SSf, JJf)) {
               std::unique_ptr<double[]> wig9j_array(new double[JJ0.size()]);

               int32_t index = 0;
               for (auto jj0 : JJ0) {
                  double wig9j = wig9jj(LLi, SSi, JJi, LL0, SS0, jj0, LLf, SSf, JJf) *
                                 sqrt((JJi + 1) * (jj0 + 1) * (LLf + 1) * (SSf + 1));
                  wig9j_array[index++] = wig9j;
               }

               keys_.push_back(get_key(uint8_t(LLf), uint8_t(LLi)));
               data_.push_back(std::move(wig9j_array));
            }
         }
      }
      keys_.shrink_to_fit();
      data_.shrink_to_fit();
   }

   const double* find(const uint8_t LLf, const uint8_t LLi) const {
      auto key = get_key(LLf, LLi);
      auto iter = std::lower_bound(keys_.cbegin(), keys_.cend(), key);
      assert(iter != keys_.cend());
      auto index = iter - keys_.cbegin();
      return data_[index].get();
   }

   size_t get_memory_footprint(int32_t jj0_size) const {
      return sizeof(this) + keys_.size() * sizeof(uint16_t) +
             data_.size() * (sizeof(std::unique_ptr<double[]>) + sizeof(double[jj0_size]));
   }

  private:
   std::vector<uint16_t> keys_;
   std::vector<std::unique_ptr<double[]>> data_;

   uint16_t get_key(const uint8_t LLf, const uint8_t LLi) const {
      return (uint16_t(LLf) << 8) + LLi;
   }
};

using Wigner9JCache = std::vector<std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>>;

void Generate9JLookUpTables(int32_t Af, int32_t Ai, int32_t SS0, int32_t JJf, int32_t JJi,
                            const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                            Wigner9JCache& wig9j_cached);

void ShowWig9JMemoryFootPrint(
    int32_t SS0, const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    const Wigner9JCache& wig9j_cached);

class CWig9STable {
  public:
   CWig9STable(int32_t SS1p, int32_t SS1, int32_t SS2, int32_t SS0) {
      SU2_VEC SSf;
      SU2_VEC SSi;

      SU2::Couple(SS1, SS2, SSf);
      SU2::Couple(SS1p, SS2, SSi);

      for (int32_t iSSf = 0; iSSf < SSf.size(); ++iSSf) {
         for (int32_t iSSi = 0; iSSi < SSi.size(); ++iSSi) {
            if (SU2::mult(SSi[iSSi], SS0, SSf[iSSf])) {
               double spin_phase = sqrt((SSi[iSSi] + 1) * (SS0 + 1) * (SS1 + 1) * (SS2 + 1));
               spin_phase *= wig9jj(SS1p, SS2, SSi[iSSi], SS0, 0, SS0, SS1, SS2, SSf[iSSf]);
               data_.emplace_back(SSf[iSSf], SSi[iSSi], spin_phase);
            }
         }
      }
      data_.shrink_to_fit();
   }

   size_t get_memory_footprint() const {
      return sizeof(this) + sizeof(std::tuple<uint8_t, uint8_t, double>) * data_.size();
   }

   std::vector<std::tuple<uint8_t, uint8_t, double>> data_;
};

using Wigner9SCached = std::vector<std::vector<std::vector<std::unique_ptr<CWig9STable>>>>;

void Generate9SLookUpTable(int32_t Zf, int32_t Zi, int32_t N, int32_t SS0,
                           Wigner9SCached& wig9s_cached);
void InitializeWignerSU2Coeffs(
    const lsu3::CncsmSU3xSU2Basis& row_basis, const lsu3::CncsmSU3xSU2Basis& col_basis,
    const SU3xSU2::LABELS& ir0,
    const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
    Wigner9JCache& wig9j_cached, Wigner9SCached& wig9s_cached);
void ShowWig9SMemoryFootPrint(int32_t SS0, const Wigner9SCached& wig9s);

#endif
