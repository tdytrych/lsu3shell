#ifndef SU3DENSE_ALGORITHM_COMMONINGENERATOR_H
#define SU3DENSE_ALGORITHM_COMMONINGENERATOR_H

#include <algorithm>
#include <cstdint>
#include <tuple>
#include <utility>
#include <vector>

#include <UNU3SU3/UNU3SU3Basics.h>
#include <LSU3/ncsmSU3xSU2Basis.h>

// EquivalentMatrices
// ========================
// KEY: {S2, N2hw, a2max}
// VALUE: {i0, i1, i2, ... iN} vector of indices
// pointing to CommonInGenerator::intersection_t vector
// whose elements are {ROW_BLOCK_ID, COL_BLOCK_ID} pairs
using EquivalentSubmatricesMap =
    std::map<std::tuple<uint8_t, uint8_t, uint16_t>, std::vector<size_t>>;

// vector of (lm2 mu2) labels and associated blocks of equivalent matrices
using NonVanishingSubmatrices = std::vector<std::pair<SU3::LABELS, EquivalentSubmatricesMap>>;

class CommonInGenerator {
  public:
   enum { ROW_BLOCK_ID = 0, COL_BLOCK_ID = 1 };
   using record_t = std::tuple<uint32_t, uint32_t>;
   using intersection_t = std::vector<record_t>;

   CommonInGenerator(const lsu3::CncsmSU3xSU2Basis& row_basis,
                     const lsu3::CncsmSU3xSU2Basis& col_basis)
       : row_basis_(row_basis), col_basis_(col_basis) {}

   void init();
   void intersection(uint32_t ip, uint32_t jp, intersection_t& result) const;
   void nonvanishing_equivalent_submatrices(
       uint32_t ip, uint32_t jp, intersection_t& result,
       NonVanishingSubmatrices& non_vanishing_submatrices) const;

   void show_nonvanishing_submatrices(const CommonInGenerator::intersection_t& intersection,
                                      const NonVanishingSubmatrices& nonvanishing_submatrices) const;

  private:
   const lsu3::CncsmSU3xSU2Basis& row_basis_;
   const lsu3::CncsmSU3xSU2Basis& col_basis_;

   std::vector<uint32_t> ipin_ptr_;
   std::vector<uint32_t> jpjn_ptr_;

   // first -> neutron irrep id; second -> row block id
   std::vector<std::pair<uint32_t, uint32_t>> in_for_ip_;
   // first -> neutron irrep id; second -> col block id
   std::vector<std::pair<uint32_t, uint32_t>> jn_for_jp_;
};
#endif
