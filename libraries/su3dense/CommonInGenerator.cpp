#include <su3dense/CommonInGenerator.h>

void CommonInGenerator::init() {
   // helper structures for in(ip) lookup:
   uint32_t n_ip = row_basis_.pconf_size();
   uint32_t n_ipin = row_basis_.NumberOfBlocks();

   // ipin helper data structure
   enum { IPIN_IP = 0, IPIN_IN = 1, IPIN_BLOCK_ID = 2 };
   std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> ipin;
   ipin.reserve(n_ipin);
   for (uint32_t i_ipin = 0; i_ipin < n_ipin; i_ipin++) {
      // consider blocks with non-zero dimension only
      if (row_basis_.NumberOfStatesInBlock(i_ipin)) {
         ipin.emplace_back(row_basis_.getProtonIrrepId(i_ipin),
                           row_basis_.getNeutronIrrepId(i_ipin), i_ipin);
      }
   }

   std::sort(ipin.begin(), ipin.end());

   // ins for ips helper structures
   ipin_ptr_.resize(n_ip + 1, 0);
   in_for_ip_.reserve(n_ipin);

   uint32_t k = 1;
   uint32_t ip = std::get<IPIN_IP>(ipin[0]);
   in_for_ip_.emplace_back(std::get<IPIN_IN>(ipin[0]), std::get<IPIN_BLOCK_ID>(ipin[0]));
   while (true) {
      while ((k < ipin.size()) && (std::get<IPIN_IP>(ipin[k]) == ip)) {
         in_for_ip_.emplace_back(std::get<IPIN_IN>(ipin[k]), std::get<IPIN_BLOCK_ID>(ipin[k]));
         k++;
      }
      ip++;
      ipin_ptr_[ip] = k;

      if (k == ipin.size()) {
         while (ip <= n_ip) {
            ipin_ptr_[ip] = k;
            ip++;
         }
         break;
      }
   }

   // release memory
   ipin.clear();
   ipin.shrink_to_fit();

   // helper structures for jn(jp) lookup:

   uint32_t n_jp = col_basis_.pconf_size();
   uint32_t n_jpjn = col_basis_.NumberOfBlocks();

   // jpjn helper data structure
   enum { JPJN_JP = 0, JPJN_JN = 1, JPJN_BLOCK_ID = 2 };
   std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> jpjn;
   jpjn.reserve(n_jpjn);
   for (uint32_t i_jpjn = 0; i_jpjn < n_jpjn; i_jpjn++) {
      // consider blocks with non-zero dimension only
      if (col_basis_.NumberOfStatesInBlock(i_jpjn)) {
         jpjn.emplace_back(col_basis_.getProtonIrrepId(i_jpjn),
                           col_basis_.getNeutronIrrepId(i_jpjn), i_jpjn);
      }
   }
   std::sort(jpjn.begin(), jpjn.end());

   jpjn_ptr_.resize(n_jp + 1, 0);
   jn_for_jp_.reserve(n_jpjn);

   k = 1;
   uint32_t jp = std::get<JPJN_JP>(jpjn[0]);
   jn_for_jp_.emplace_back(std::get<JPJN_JN>(jpjn[0]), std::get<JPJN_BLOCK_ID>(jpjn[0]));
   while (true) {
      while ((k < jpjn.size()) && (std::get<JPJN_JP>(jpjn[k]) == jp)) {
         jn_for_jp_.emplace_back(std::get<JPJN_JN>(jpjn[k]), std::get<JPJN_BLOCK_ID>(jpjn[k]));
         k++;
      }
      jp++;
      jpjn_ptr_[jp] = k;

      if (k == jpjn.size()) {
         while (jp <= n_jp) {
            jpjn_ptr_[jp] = k;
            jp++;
         }
         break;
      }
   }

   // release memory
   jpjn.clear();
   jpjn.shrink_to_fit();
}

void CommonInGenerator::nonvanishing_equivalent_submatrices(
    uint32_t ip, uint32_t jp, intersection_t& in_intersection,
    NonVanishingSubmatrices& non_vanishing_submatrices) const {
   intersection(ip, jp, in_intersection);
   if (in_intersection.empty()) {
      return;
   }

   SU3::LABELS last_su3(254, 254, 254);
   EquivalentSubmatricesMap equivSubmatricesMap;

   for (size_t i = 0; i < in_intersection.size(); ++i) {
      uint32_t ipin_block = std::get<ROW_BLOCK_ID>(in_intersection[i]);
      uint32_t in = row_basis_.getNeutronIrrepId(ipin_block);
      SU3xSU2::LABELS current_su3(row_basis_.getNeutronSU3xSU2(in));
      auto key = std::make_tuple(current_su3.S2, row_basis_.nhw_n(in), row_basis_.getMult_n(in));

      // true ==> we have just encountered a new block of (lm2 mu2) irreps
      if (current_su3.lm != last_su3.lm || current_su3.mu != last_su3.mu) {
         if (!equivSubmatricesMap.empty()) {
            // non_vanishing_matrices[last_su3] = equivMatricesMap;
            non_vanishing_submatrices.emplace_back(
                std::make_pair(last_su3, std::move(equivSubmatricesMap)));
            equivSubmatricesMap.clear();
         }
         last_su3 = current_su3;
      }
      equivSubmatricesMap[key].push_back(i);
   }

   if (!equivSubmatricesMap.empty()) {
      non_vanishing_submatrices.emplace_back(
          std::make_pair(last_su3, std::move(equivSubmatricesMap)));
   }
}

void CommonInGenerator::intersection(uint32_t ip, uint32_t jp, intersection_t& result) const {
   result.clear();  // ?

   uint32_t in_count = ipin_ptr_[ip + 1] - ipin_ptr_[ip];
   uint32_t in_index = ipin_ptr_[ip];
   uint32_t jn_count = jpjn_ptr_[jp + 1] - jpjn_ptr_[jp];
   uint32_t jn_index = jpjn_ptr_[jp];

   if ((in_count > 0) && (jn_count > 0)) {
      while (true) {
         if (in_for_ip_[in_index].first == jn_for_jp_[jn_index].first) {
            // insert: row block id and col block id
            result.emplace_back(in_for_ip_[in_index].second, jn_for_jp_[jn_index].second);
            in_index++;
            jn_index++;
         }

         while ((in_index < ipin_ptr_[ip + 1]) &&
                (in_for_ip_[in_index].first < jn_for_jp_[jn_index].first))
            in_index++;
         if (in_index == ipin_ptr_[ip + 1]) break;

         while ((jn_index < jpjn_ptr_[jp + 1]) &&
                (in_for_ip_[in_index].first > jn_for_jp_[jn_index].first))
            jn_index++;
         if (jn_index == jpjn_ptr_[jp + 1]) break;
      }
   }

   sort(result.begin(), result.end(), [this](const record_t& rhs, const record_t& lhs) {
      auto in_lhs = row_basis_.getNeutronIrrepId(std::get<ROW_BLOCK_ID>(lhs));
      auto in_rhs = row_basis_.getNeutronIrrepId(std::get<ROW_BLOCK_ID>(rhs));
      return row_basis_.getNeutronSU3xSU2(in_lhs) < row_basis_.getNeutronSU3xSU2(in_rhs);
   });
}

void CommonInGenerator::show_nonvanishing_submatrices(
    const CommonInGenerator::intersection_t& intersection,
    const NonVanishingSubmatrices& nonvanishing_submatrices) const {
   for (const auto& wn_submatrices : nonvanishing_submatrices) {
      SU3::LABELS wn(wn_submatrices.first);
      cout << "(" << (int)wn.lm << " " << (int)wn.mu << ")" << endl;
      for (auto ss_N_amax_submatrices : wn_submatrices.second) {
         uint8_t Nn, S2;
         uint16_t a2max;
         std::tie(S2, Nn, a2max) = ss_N_amax_submatrices.first;

         std::cout << " S2:" << (int)S2;
         std::cout << "\t Nn:" << (int)Nn;
         std::cout << " a2max:" << a2max << endl;
         for (auto index : ss_N_amax_submatrices.second) {
            uint32_t ipin_block = std::get<0>(intersection[index]);
            uint32_t jpin_block = std::get<1>(intersection[index]);
            uint32_t in = row_basis_.getNeutronIrrepId(ipin_block);
            uint32_t nrows = row_basis_.NumberOfStatesInBlock(ipin_block);
            uint32_t ncols = col_basis_.NumberOfStatesInBlock(jpin_block);
            std::cout << "\t\tin:" << in << " #rows:" << nrows << " #cols:" << ncols << std::endl;
         }
      }
   }
}

