#ifndef SU3DENSE_COMMON_UTILS_TENSOR_READ_H
#define SU3DENSE_COMMON_UTILS_TENSOR_READ_H
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <fstream>
#include <vector>
#include <unordered_map>


#include <boost/functional/hash.hpp>

namespace tensorfile {
using value_t = float;
using index_t = lsu3::CncsmSU3xSU2Basis::IRREP_INDEX;
}  // namespace tensorfile

// structure holds information about groups of SU(3) equivalent proton/neutron irreps
struct CGroup
{
   SingleDistributionSmallVector distr;
   UN::SU3xSU2_VEC gamma;
   SU3_VEC omega;
   // {355, 356, 357, 358, 259}
   std::vector<uint32_t> irrep_ids;
   // {{2, 3, 2}, {2, 3, 4}, {2, 1, 0} ...
   std::vector<std::vector<int>> spins;

   void ShowContent()
   {
      std::cout << "[";
      for (size_t i = 0; i < distr.size(); ++i)
      {
         std::cout << (int)distr[i] << " ";
      }

      std::cout << "] ";
      std::cout << "gamma:{";
      for (size_t i = 0; i < gamma.size(); ++i)
      {
         std::cout << (int)gamma[i].mult << "(" << (int)gamma[i].lm;
         std::cout << " " << (int)gamma[i].mu << ")" << (int)gamma[i].S2 << " ";
      }
      std::cout << "} omega:{";
      for (size_t i = 0; i < omega.size(); ++i)
      {
         std::cout << (int)omega[i].rho << "(" << (int)omega[i].lm;
         std::cout << " " << (int)omega[i].mu << ") ";
      }
      std::cout << "}" << std::endl;
      for (size_t i = 0; i < irrep_ids.size(); ++i)
      {
         std::cout << irrep_ids[i] << " S={";
         for (size_t is = 0; is < spins[i].size(); ++is)
         {
            std::cout << (int)(spins[i])[is] << " ";
         }
         std::cout << "}" << std::endl;
      }
   }
};

// {gi, gi, {<gi||| O ||| gj>}, {{i, j, dSU2}, {i', j', dSU2'} ...}
struct GROUP_RMES
{
   uint32_t gi; // group id in bra basis
   uint32_t gj; // group id in ket basis
   std::vector<float> su3_rmes; // {<gi ||| T ||| gj>}
   // {{ip, jp, dSU2}, {ip', jp', dSU2'}, .... }}
   std::vector<std::tuple<uint32_t, uint32_t, double>> ijC;
};

// Holds SU(3) equivalent groups of rmes
// such that SU(3)(gi) = const = wgi & SU(3)(gj)=const=wgj
// for each (gi, gj) pair of U(3)-equivalent irreps
struct SU3_EQV_RMES
{
   // shared bra and ket SU3 labels
   SU3::LABELS wgi, wgj;
   // vector of {gi, gi, {<gi||| O ||| gj>}, {{i, j, dSU2}, {i', j', dSU2'} ...}
   std::vector<GROUP_RMES> gigj_rmes;
};
// This structure is being used for fast computation of proton-neutron densities
using RMES_TABLE = std::vector<SU3_EQV_RMES>;

// { {<gi ||| T ||| gj>}, {{ip, jp, dSU2}, {ip', jp', dSU2'}, .... }}
//using GROUP_RMES = std::pair<std::vector<float>, std::vector<std::tuple<uint32_t, uint32_t, double>>>;
// [gi, gj] ---> GROUP_RMES
using GIGJ_RMES = std::vector<GROUP_RMES>;

// IJ_Indices format: store {i, j, index_rm} and {rmes for all i, j values}
using IJ_Indices = std::vector<std::array<uint32_t, 3>>;
// Store hash [i, j] --> {<i ||| j || j>rmes}
using IJ_RMES = std::unordered_map<std::pair<uint32_t, uint32_t>, std::vector<float>,
                                   boost::hash<std::pair<uint32_t, uint32_t>>>;
// Quantum labels of a 2B SU(3)xSU(2) tensor
// [{a+n1 x a+n2}^(lmf muf)Sf x {tan3 x tan4}^(lmi mui)Si]^rho0:* (lm0 mu0)S0
enum adadtataLABELS { N1, N2, N3, N4, LMF, MUF, SSF, LMI, MUI, SSI, LM0, MU0, SS0 };

// Compute multiplicity of a general multi-shell tensor. This is overkill as multiplicity
// is trivially equal to rho0max in coupling (lmf muf) x (lmi mui) --> (lm0 mu0) in case of 2B
// operators.
uint32_t GetTensorMaxMult(const std::vector<SU3xSU2_VEC>& tensorLabels, const SU3xSU2_VEC& omegas);

std::string GetFilename_SU3xSU2(const std::array<int, 5>& tensor_labels);
std::string GetFilename_SU3xSU2(const std::array<int, 13>& tensor_labels, uint32_t A,
                                uint32_t Nmax);
//////////////////
/// CSR format ///
//////////////////
void ReadHeaderCSR(std::ifstream& f, int& A, int& Nmax, std::vector<std::vector<char>>& structures,
                   std::vector<SU3xSU2_VEC>& tensorLabels, SU3xSU2_VEC& omegas,
                   SU3xSU2::LABELS& ir0);
// Returns a0max ... maximal tensor multiplicity
uint32_t ReadProtonTensorRMEsCSRtoIJ_Indices(const std::string& filenameTensorRMEs,
                                 lsu3::CncsmSU3xSU2Basis& row_basis,
                                 lsu3::CncsmSU3xSU2Basis& col_basis, SU3xSU2::LABELS& ir0,
                                 IJ_Indices& ipjpindices,
                                 std::vector<tensorfile::value_t>& tensor_rmes);
// Returns a0max ... maximal tensor multiplicity
uint32_t ReadNeutronTensorRMEsCSRtoIJ_Indices(const std::string& filenameTensorRMEs,
                                  lsu3::CncsmSU3xSU2Basis& row_basis,
                                  lsu3::CncsmSU3xSU2Basis& col_basis, SU3xSU2::LABELS& ir0,
                                  IJ_Indices& injnindices,
                                  std::vector<tensorfile::value_t>& tensor_rmes);
////////////////////////
//  IJ_Indices format //
////////////////////////
// tensor_labels=<{n1, n2, n3, n4, lmi, mui, ssi, lmf muf lmf lm0 mu0 ss0}>
void LoadRMEsIJ_Indices(const std::string filename, std::array<int, 13>& quantum_numbers,
                        uint32_t& A, uint32_t& Nmax, IJ_Indices& ipjpindices,
                        std::vector<float>& rmes);
void SaveRMEs_IJIndices(const std::array<int, 13>& tensor_labels, uint32_t A, uint32_t Nmax,
                        const IJ_Indices& ipjpindices, const std::vector<float>& rmes);
/////////////////////
//  IJ_RMES format //
/////////////////////
void SaveRMEsIJ_RMES(const std::string& filename, uint32_t Af, uint32_t Ai, uint32_t Nmax,
                     const SU3xSU2::LABELS& tensor_labels, const IJ_RMES& rmes_table);
void LoadRMEsIJ_RMES(const std::string& filename, uint32_t& Af, uint32_t& Ai, uint32_t& Nmax,
                  SU3xSU2::LABELS& tensorLabels, IJ_RMES& rmes_table);
void ShowRMEsTableIJ_RMES(const IJ_RMES& rmes_table);
void ShowRMEsTableIJ_RMES(char type, const IJ_RMES &rmes);
////////////////////////
//  RMES_TABLE format //
////////////////////////
void SaveAsRMES_TABLE(const std::string& filename, uint32_t Af, uint32_t Ai, uint32_t Nmax,
                       const SU3xSU2::LABELS& tensor_labels, const std::vector<CGroup>& bra_groups,
                       const std::vector<CGroup>& ket_groups, const GIGJ_RMES& rmes_table);

void LoadRMES_TABLE(const std::string& filename, uint32_t& Af, uint32_t& Ai, uint32_t& Nmax,
                  SU3xSU2::LABELS& tensorLabels, RMES_TABLE& rmes_table);

void ShowRMES_TABLE(const RMES_TABLE& rmes_table);

void GIGJRMES2IJRMES(const GIGJ_RMES& group_rmes, IJ_RMES& ij_rmes);
#endif
