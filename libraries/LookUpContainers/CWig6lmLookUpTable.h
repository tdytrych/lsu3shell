#ifndef CWIG6LMLOOKUPTABLE_H
#define CWIG6LMLOOKUPTABLE_H

#include <su3.h>

#include <LookUpContainers/HashFixed.h>
#include <LookUpContainers/LRUCache.h>
#include <LookUpContainers/SplitLRUCache.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <UNU3SU3/CSU3Master.h>

template <typename SIX_LM_STORAGE_TYPE>
class CWig6lmLookUpTable
{
public:
	typedef SIX_LM_STORAGE_TYPE SixLMStorageType; 
	template <typename Key, typename Value>
	struct deleteArrayPolicy
	{
		void on_evict(const Key& k, const Value& v) {delete[] v;} 
		void on_delete(const Key& k, const Value& v) {delete[] v;} 
	};

	
	inline int INTEGER_KEY(const SU3::LABELS& a, const SU3::LABELS& b) const
	{
		return (a.lm << 24) | (a.mu << 16) | (b.lm << 8) | b.mu;
	}

	struct U6MemCounter
	{
		inline void KEY_2_SU3(const int key, SU3::LABELS& a, SU3::LABELS& b) const 
		{ 
			b.mu = key & 0xFF;
			b.lm = (key >> 8) & 0xFF; 
			a.mu = (key >> 16) & 0xFF; 
			a.lm = (key >> 24) & 0xFF; 
		} 

		size_t operator() (const CTuple<int, 3>& key, const SIX_LM_STORAGE_TYPE* u6coeffs_ptr) 
		{
			SU3::LABELS ir13, ir2, ir, ir4, ir0, ir24;

			KEY_2_SU3(key[0], ir13, ir2);
			KEY_2_SU3(key[1], ir, ir4);
			KEY_2_SU3(key[2], ir0, ir24);

			int rho132max 	= SU3::mult(ir13, ir2, ir0);
			int rho04max 	= SU3::mult(ir0, ir4, ir);
			int rho24max 	= SU3::mult(ir2, ir4, ir24);
			int rho1324max 	= SU3::mult(ir13, ir24, ir);

			int num_6lm_a = rho132max*rho04max*rho24max*rho1324max;

			return num_6lm_a*sizeof(SIX_LM_STORAGE_TYPE);
		}
	};

	public:
//		typedef LRUCache<CTuple<int, 3>, SIX_LM_STORAGE_TYPE*, CombineHash, deleteArrayPolicy<CTuple<int, 3>, SIX_LM_STORAGE_TYPE* > > WIGNER6_LOOK_UP_TABLE;
	//	typedef SplitLRUCache<CTuple<int, 3>, SIX_LM_STORAGE_TYPE*, CombineHash, deleteArrayPolicy<CTuple<int, 3>, SIX_LM_STORAGE_TYPE* > > WIGNER6_LOOK_UP_TABLE;
	typedef HashFixed<CTuple<int, 3>, SIX_LM_STORAGE_TYPE*, CombineHash, deleteArrayPolicy<CTuple<int, 3>, SIX_LM_STORAGE_TYPE* > > WIGNER6_LOOK_UP_TABLE;
	public:
	CWig6lmLookUpTable(const size_t number_u6_symbols_max, const size_t number_z6_symbols_max): u6lmTable_(number_u6_symbols_max), z6lmTable_(number_z6_symbols_max) {}
	CWig6lmLookUpTable() {}

   void AllocateMemory(size_t number_u6_symbols_max, size_t number_z6_symbols_max)
   {
      u6lmTable_.AllocateMemory(number_u6_symbols_max);
      z6lmTable_.AllocateMemory(number_z6_symbols_max);
   }

	void ShowInfo()
	{
		std::cout << "#z6 stored:" << z6lmTable_.nelems() << std::endl;
		std::cout << "#u6 stored:" << u6lmTable_.nelems() << std::endl;
	}

	void coeffs_info(size_t& u6_size, size_t& z6_size)
	{
		u6_size = u6lmTable_.nelems();
		z6_size = z6lmTable_.nelems();
	}

	void ReleaseMemory()
	{
		u6lmTable_.ReleaseMemory();
		z6lmTable_.ReleaseMemory();
	}


	void memory_usage(size_t& u6_size, size_t& z6_size) const
	{
		U6MemCounter u6memcntr;
		u6_size = u6lmTable_.dynamic_memory_usage(u6memcntr); 
		z6_size = z6lmTable_.dynamic_memory_usage(u6memcntr);
	}

	void ShowMemoryUsage() const
	{
		U6MemCounter u6memcntr;
		std::cout << "size of u6 \n";
		std::cout << "generated: " << (u6lmTable_.dynamic_memory_usage(u6memcntr) - u6lmTable_.static_memory_usage())/(1024.0*1024.0) << " MB."  << std::endl;
		std::cout << "allocated for hash data structures: " << u6lmTable_.static_memory_usage()/(1024.0*1024.0) << " MB."  << std::endl;

		std::cout << "size of z6 \n";
		std::cout << "generated: " << z6lmTable_.dynamic_memory_usage(u6memcntr)/(1024.0*1024.0) - z6lmTable_.static_memory_usage()/(1024.0*1024.0) << " MB."  << std::endl;
		std::cout << "allocated for hash data structures: " << z6lmTable_.static_memory_usage()/(1024.0*1024.0) << " MB."  << std::endl;
	}


	void GetWigner6lm (	const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
						const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
						const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
						const SU3::LABELS& ir0,
						int	rho132max, int rho04max, int rho24max, int rho1324max, int rho12max, int rho123max, int rho13max, int rho34max, int rho1234max,
						SIX_LM_STORAGE_TYPE** u6_a, SIX_LM_STORAGE_TYPE** u6_b, SIX_LM_STORAGE_TYPE** u6_c)
	{
      int lm(ir.lm), mu(ir.mu); 
      int lm0(ir0.lm), mu0(ir0.mu);
      int lm1(ir1.lm), mu1(ir1.mu); 
      int lm2(ir2.lm), mu2(ir2.mu); 
      int lm3(ir3.lm), mu3(ir3.mu);
      int lm4(ir4.lm), mu4(ir4.mu);
      int lm12(ir12.lm), mu12(ir12.mu);
      int lm13(ir13.lm), mu13(ir13.mu); 
      int lm24(ir24.lm), mu24(ir24.mu);
      int lm34(ir34.lm), mu34(ir34.mu);

      std::vector<double> temp_array;

		CTuple<int, 3> key_a, key_b, key_c;

		key_a[0] = INTEGER_KEY(ir13, ir2);
		key_a[1] = INTEGER_KEY(ir, ir4);
		key_a[2] = INTEGER_KEY(ir0, ir24);

		auto wig6lm = u6lmTable_.find(key_a);
		if (wig6lm == u6lmTable_.end())
		{ 
         su3::wru3(lm13, mu13, lm2, mu2, lm, mu, lm4, mu4, lm0, mu0, lm24, mu24, rho132max, rho04max, rho24max, rho1324max, temp_array);
         *u6_a = new SIX_LM_STORAGE_TYPE[temp_array.size()];
         std::copy(temp_array.begin(), temp_array.end(), *u6_a);
         u6lmTable_.insert(key_a, *u6_a);
		}
		else
		{ 
			*u6_a = *wig6lm;
		}

      temp_array.clear();

		key_b[0] = INTEGER_KEY(ir2, ir1);
		key_b[1] = INTEGER_KEY(ir0, ir3);
		key_b[2] = INTEGER_KEY(ir12, ir13);

		wig6lm = z6lmTable_.find(key_b);
		if (wig6lm == z6lmTable_.end())
		{ 
         su3::wzu3(lm2, mu2, lm1, mu1, lm0, mu0, lm3, mu3, lm12, mu12, lm13, mu13, rho12max, rho123max, rho13max, rho132max, temp_array);
			*u6_b = new SIX_LM_STORAGE_TYPE[temp_array.size()];
			std::copy(temp_array.begin(), temp_array.end(), *u6_b);
			z6lmTable_.insert(key_b, *u6_b);
		}
		else
		{ 
			*u6_b = *wig6lm;
		}

      temp_array.clear();

		key_c[0] = INTEGER_KEY(ir12, ir3);
		key_c[1] = INTEGER_KEY(ir, ir4);
		key_c[2] = INTEGER_KEY(ir0, ir34);

		wig6lm = u6lmTable_.find(key_c);
		if (wig6lm == u6lmTable_.end())
		{ 
         su3::wru3(lm12, mu12, lm3, mu3, lm, mu, lm4, mu4, lm0, mu0, lm34, mu34, rho123max, rho04max, rho34max, rho1234max, temp_array);
			*u6_c = new SIX_LM_STORAGE_TYPE[temp_array.size()];
			std::copy(temp_array.begin(), temp_array.end(), *u6_c);
			u6lmTable_.insert(key_c, *u6_c);
		}
		else
		{ 
			*u6_c = *wig6lm;
		}
	}

	private:
	WIGNER6_LOOK_UP_TABLE u6lmTable_; 
	WIGNER6_LOOK_UP_TABLE z6lmTable_;
};

#endif
