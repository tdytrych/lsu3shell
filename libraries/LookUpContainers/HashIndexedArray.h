#ifndef HASH_INDEXED_ARRAY
#define HASH_INDEXED_ARRAY

#include <array>
#include <cstdlib>
#include <tuple>
#include <vector>

//
// Software cache for (Key, Value[]) pairs where length of Value[] is <= Ncols
//
template <typename Key, typename Value, typename Hasher, size_t Nrows, size_t Ncols>
class HashIndexedArray
{
   public:
      HashIndexedArray() : cached_(Nrows), keys_(Nrows), values_(Nrows), buf_(2 * Nrows) { }

      //
      // returns (flag, ptr) where 
      // a) data needs to be stored at ptr if flag is false
      // b) data are stored at ptr if flag is true
      // 
      std::tuple<bool, Value*> find(const Key& key, size_t len)
      {
         if (len > Ncols)  // no caching in this case
         {
            buf_.resize(len);
            return { false, buf_.data() };
         }

         // caching applies:

         size_t idx = Hasher{}(key) % Nrows;  // hash-indexing

         if (!cached_[idx])  // first touch of the row
         {
            cached_[idx] = true;
            keys_[idx] = key;
            return { false, values_[idx].data() };
         }

         if (keys_[idx] != key)  // another key cached 
         {
            keys_[idx] = key;
            return { false, values_[idx].data() };
         }

         return { true, values_[idx].data() };
      }

   private:
      std::vector<bool> cached_;
      std::vector<Key> keys_;
      std::vector<std::array<Value, Ncols>> values_;

      std::vector<Value> buf_;  // storage for vectors of length > Ncols
};

#endif
