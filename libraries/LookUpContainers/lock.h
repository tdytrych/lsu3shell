#ifndef LOCK_HASH_H
#define LOCK_HASH_H

struct no_locking
{
  void acquire(){}
  void release(){}
};


#include <omp.h>
#ifndef TIME_LOCK
#include <chrono>
#endif


class openmp_locking
{
  omp_lock_t writelock;
  

 public:
#ifdef TIME_LOCK
  static std::chrono::duration<double> locktime[1024];
#endif

  openmp_locking()
  {
    omp_init_lock(&writelock);
  }


  void acquire(){
#ifdef TIME_LOCK
    std::chrono::system_clock::time_point start = std::chrono::system_clock::now();    
#endif
    omp_set_lock(&writelock);
#ifdef TIME_LOCK
    std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
    locktime[omp_get_thread_num()] += duration.count();
#endif
  }
  
  void release(){
    omp_unset_lock(&writelock);
  }

  ~openmp_locking()
  {
    omp_destroy_lock(&writelock);
  }
 
};

#endif
