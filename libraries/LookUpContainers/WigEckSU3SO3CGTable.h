#ifndef WIGNERSU3TABLE_H
#define WIGNERSU3TABLE_H
#include <UNU3SU3/UNU3SU3Basics.h>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <LookUpContainers/HashFixed.h>
#include <SU3ME/global_definitions.h>

//////////////////////////////////////////////
////////////      SU3SU3Key      /////////////
//////////////////////////////////////////////
//	stores two SU3::LABELS Bra and Ket (i.e. omegaf and omegai) in form of a
//	single integer SU3SU3Key::m_Key
struct SU3SU3Key
{
	int m_Key;
	inline SU3SU3Key(const SU3::LABELS& Bra, const SU3::LABELS& Ket) {Set(Bra, Ket);}
	inline SU3SU3Key() {m_Key=0;}
	inline void Set(const SU3::LABELS& Bra, const SU3::LABELS& Ket) 
	{ 
		m_Key = (Bra.lm << 24) | (Bra.mu << 16) | (Ket.lm << 8) | Ket.mu; 
	}
	inline void Get(SU3::LABELS& Bra, SU3::LABELS& Ket) const
	{
		Ket.mu = m_Key & 0xFF;
		Ket.lm = (m_Key >> 8) & 0xFF;
		Bra.mu = (m_Key >> 16) & 0xFF;
		Bra.lm = (m_Key >> 24) & 0xFF;
	}
	//	this allows STL to treat SU3SU3Key as integer.
	inline operator int() const {return m_Key;}
};

struct SU3SU3KeyHasher
{
  inline std::size_t operator()(const SU3SU3Key& value) const
  {
    return (unsigned int) value.operator int();
  }

};

//////////////////////////////////////////
////////////   WigEckSU3SO3CG   /////////////
//////////////////////////////////////////
//	holds reduced Wigner SU(3)>SO(3) coefficients: 	< wi *; w0 * L0 || wf *>_{*} 
//	for all possible value of ki, Li, k0, kf, Lf, rhot. 
//
//	Note that WigEckSU3SO3CG does not store values of wf, wi, w0, and L0.  The
//	values of wf and wi are stored as a key in map<SU3SU3Key, WigEckSU3SO3CG*>
//	WigEckSU3SO3CGTable::m_SU3SO3CGTable. The values of w0 and L0 are stored as
//	WigEckSU3SO3CGTable::m_IR0 and WigEckSU3SO3CGTable::m_LL0. 
//
//	Struct WigEckSU3SO3CG merely stores CG coefficients and pairs Lf Li with the starting index.
struct WigEckSU3SO3CG
{
	//	SO3::LABEL Lf and SO3::LABEL Li are packed into 2 bytes of short
	struct LfLiKey
	{
		short m_Key;
		LfLiKey(SO3::LABEL Lf, SO3::LABEL Li) {Set(Lf, Li);}
		inline void Set(const SO3::LABEL& Lf, const SO3::LABEL& Li) 
		{
			assert(!(Li%2));	// Li must be always even since LfLi key is supposed to hold 2*Li and 2*Lf
			assert(!(Lf%2));	// Li must be always even since LfLi key is supposed to hold 2*Li and 2*Lf
			m_Key = ((Lf << 8) | Li);
		}
		inline void Get(SO3::LABEL& Lf, SO3::LABEL& Li) const { Li = m_Key & 0xFF; Lf = (m_Key >> 8) & 0xFF; }
		inline operator short() const {return m_Key;}
	};
	struct LfLiCGIndex {
		struct COMPARE_LfLi
		{
			inline bool operator()(const LfLiCGIndex& lhs, const LfLiKey& rhs) const {return lhs.LfLi < rhs;}
	 		inline bool operator()(const LfLiKey& lhs, const LfLiCGIndex& rhs ) const {return lhs < rhs.LfLi;};
		};
		//	IMPORTANT to note that Lf and Li are supposed to be 2*Lf and 2*Li !!!
		LfLiKey LfLi;
		unsigned CGIndex;
		LfLiCGIndex(): LfLi(0, 0), CGIndex(0) {}
		inline void GetLfLi(SO3::LABEL& Lf, SO3::LABEL& Li) {LfLi.Get(Lf, Li);}
		inline void SetLfLi(const SO3::LABEL& Lf, const SO3::LABEL& Li) {
			assert(!(Li%2));	// Li must be always even since LfLi key is supposed to hold 2*Li and 2*Lf
			assert(!(Lf%2));	// Li must be always even since LfLi key is supposed to hold 2*Li and 2*Lf
			LfLi.Set(Lf, Li);}
		inline operator short() const {return (short)LfLi;}
	};

//	pointer to array of SU3SO3 reduced CG coefficients.  element <wf kf Lf; w0
//	k0 L0|| wf kf Lf>_rhot is stored as m_WigCoeffs[index + startingIndex], where
//	index = kf*kimax*k0max*rhotmax + ki*k0max*rhotmax + k0*rhotmax + rhot
//	and startingIndex = m_LfLiCGIndexArray(Lf, Li)
	SU3::WIGNER* m_WigCoeffs;
	unsigned short m_nWigCoeffs;
//	Each element of m_LfLiCGIndexArray stores Lf, Li packed in LfLiKey and
//	CGIndex, which is index of the first SU3SO3 reduced CG coefficient < wi 1
//	Li; w0 1 L0 || wf 1 Lf>_{1} 
	LfLiCGIndex* m_LfLiCGIndexArray;
//	m_nLfLi ... number of elements in m_LfLiCGIndexArray	
	unsigned short m_nLfLi;

	WigEckSU3SO3CG(const SU3::LABELS& omegaf, const SU3::LABELS& omegai, const SU3::LABELS& Omega0, const SO3::LABEL& LL0);
	~WigEckSU3SO3CG();

	inline SU3::WIGNER SU3SO3CG(const size_t index) const {return m_WigCoeffs[index];}
	inline SU3::WIGNER* SU3SO3CGs(SO3::LABEL Lf, SO3::LABEL Li) const
	{
		assert(!(Lf%2));	// since we suppose Lf == 2*Lf
		assert(!(Li%2));	//	since we suppose Li = 2*Li
		LfLiKey LfLi2Find(Lf, Li);
		LfLiCGIndex* pLfLiCGindex = std::lower_bound(m_LfLiCGIndexArray, m_LfLiCGIndexArray + m_nLfLi, LfLi2Find, LfLiCGIndex::COMPARE_LfLi());
		if (pLfLiCGindex != (m_LfLiCGIndexArray + m_nLfLi) && pLfLiCGindex->LfLi == LfLi2Find)
		{
			return (m_WigCoeffs + pLfLiCGindex->CGIndex);
		}
		else
		{
			return NULL;
		}
	}
	void Show(const SU3::LABELS& omegaf, const SU3::LABELS& IR0, const SO3::LABEL L0, const SU3::LABELS& omegai);
//	inline SU3::WIGNER* operator() (SO3::LABEL Lf, SO3::LABEL Li) const { return SU3SO3CGs(Lf, Li); }
//	inline operator SU3::WIGNER*() const {return m_WigCoeffs;}
//	inline SU3::WIGNER operator[] (size_t index) const {return m_WigCoeffs[index];}
};

///////////////////////////////////////////////////
////////////   WigEckSU3SO3CGTable    /////////////
///////////////////////////////////////////////////
//	for a tensor IR0 and L0 holds map of
//	wfwi, {<wi *; IR0 * L0 || wf *>_{*}}
struct WigEckSU3SO3CGTable
{
  //  typedef std::map<SU3SU3Key, WigEckSU3SO3CG*> MAPKEYCOEFF;
	template <typename Key, typename Value>
	struct deleteValuePolicy
	{
	  void on_evict(const Key& k, const Value& v) {if (v) delete v;} 
		void on_delete(const Key& k, const Value& v) {if (v) delete v;} 
	};

  typedef HashFixed<SU3SU3Key, WigEckSU3SO3CG*, SU3SU3KeyHasher, deleteValuePolicy<SU3SU3Key, WigEckSU3SO3CG* >, openmp_locking, false, false> MAPKEYCOEFF;
  MAPKEYCOEFF m_SU3SO3CGTable;
	SU3::LABELS m_IR0;
	SO3::LABEL m_LL0;	// == L0 !!! NOTE: this is not equal to 2*L0!!!
	//	return pointer to structure WigEckSU3SO3CG which maintain
	//	CG coefficients <wi *; IR0 * L0 || wf *>_{*}. If such a structure
	//	does not exist, create WigEckSU3SO3CG by computing all SU3SO3CG needed. This
	//	structure is finally stored in m_SU3SO3CGTable and pointer to WigEckSU3SO3CG created
	//	is returned.
	WigEckSU3SO3CG* GetWigEckSU3SO3CG(const SU3::LABELS& omegaf, const SU3::LABELS& omegai); //	omegaf == Bra, omegai == Ket 
	WigEckSU3SO3CGTable(const SU3::LABELS& IR0, const SO3::LABEL& LL0): m_IR0(IR0.lm, IR0.mu), m_LL0(LL0), m_SU3SO3CGTable(storage_limits::number_su3su3_coeffs) {}
//	INPUT: Li and Lf are expected to be 2*Li and 2*Lf. ... TESTED for < (4 4) *; (1 1) * 1 || (4 4) *>_* and < (4 4) *; (2 2) * 2 || (4 4) *>_*
	SU3::WIGNER SU3SO3CG(const SU3::LABELS& omegai, int ki, int Li, int k0, const SU3::LABELS& omegaf, int kf, int Lf, int rhot)
	{
		assert(!(Lf%2)); assert(!(Li%2)); assert(!(m_LL0%2));
		
		int kf_max = SU3::kmax(omegaf, (Lf >> 1));
		int ki_max = SU3::kmax(omegai, (Li >> 1));
		int k0_max = SU3::kmax(m_IR0, (m_LL0 >> 1));
		
		assert(kf < kf_max && ki < ki_max && k0 < k0_max);

		int rhot_max = SU3::mult(omegai, m_IR0, omegaf);
		SU3::WIGNER* pCGs = GetWigEckSU3SO3CG(omegaf, omegai)->SU3SO3CGs(Lf, Li);
		if (!pCGs) 
		{
			assert(0);	// this should never happen. Lf and Li are not coupled by L0!!!
			return 0.0;
		}
		else
		{
			return pCGs[kf*ki_max*k0_max*rhot_max + ki*k0_max*rhot_max + k0*rhot_max + rhot];
		}
	}
	//	this method returns pointer to array of SU3SO3CGs <wi *; w0 *L0 || wf *>_*
	//	the order of LfLi can be obtained from WigEckSU3SO3CG::m_LfLiCGIndexArray 
	//	or by iterating in the following order: Lf Li if mult(2*Li, 2*L0, 2*Lf) ==> kf ki k0 rhot
	//	return pointer to <wi * Li; w0 * L0 || wf * Lf>_*
	SU3::WIGNER* SU3SO3CGs(const SU3::LABELS& omegaf, const SU3::LABELS& omegai) 
	{
		return (GetWigEckSU3SO3CG(omegaf, omegai))->m_WigCoeffs;
	}
	SU3::WIGNER* SU3SO3CGs(const SU3::LABELS& omegaf, const SO3::LABEL& Lf, const SU3::LABELS& omegai, const SO3::LABEL& Li) 
	{
		return (GetWigEckSU3SO3CG(omegaf, omegai))->SU3SO3CGs(Lf, Li);
	}

	~WigEckSU3SO3CGTable()
	{
	  //The deallocation of the coefficients stored in the table is taken care of in the destructor of MAPKEYCOEFF thanks to the deletepolicy
	}
	size_t GetNumberSU3SO3CGs()
	{
		size_t nWigCoeffs = 0;
		MAPKEYCOEFF::iterator it = m_SU3SO3CGTable.begin();
		for (; it != m_SU3SO3CGTable.end(); ++it)
		{
		  nWigCoeffs += it.v()->m_nWigCoeffs;
		}
		return nWigCoeffs;
	}
	size_t GetNumberSU3SU3()
	{
		return m_SU3SO3CGTable.nelems();
	}
	void Show() const;
};


class CWigEckSU3SO3CGTablesLookUpContainer
{
	private:
	static std::vector<WigEckSU3SO3CGTable*> m_WigEckSU3SO3CGTables;
	public:
	CWigEckSU3SO3CGTablesLookUpContainer() {}
	CWigEckSU3SO3CGTablesLookUpContainer(const CWigEckSU3SO3CGTablesLookUpContainer&) {}
	static void ReleaseMemory();
	static void ShowInfo();
	void ShowWigEckSU3SO3CGTables();
 	WigEckSU3SO3CGTable* GetSU3SO3CGTablePointer(const SU3::LABELS& IR0, const SO3::LABEL L0);
};
#endif
