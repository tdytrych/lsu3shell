#ifndef LRU_CACHE_H
#define LRU_CACHE_H

//#include <boost/unordered_map.hpp>
#include <assert.h>
#include <LookUpContainers/lock.h>

#include <boost/functional/hash.hpp>
struct CombineHash
{
	inline std::size_t operator()(const CTuple<int, 3>& value) const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, value[0]);
		boost::hash_combine(seed, value[1]);
		boost::hash_combine(seed, value[2]);
		return seed;
	}
	inline std::size_t operator()(const CTuple<int, 5>& value) const
	{
//         return (std::size_t)c[0]^((std::size_t)c[1]<<4)^((std::size_t)c[2])^((std::size_t)c[3]<<12)^((std::size_t)c[4]);
		std::size_t seed = 0;
		boost::hash_combine(seed, value[0]);
		boost::hash_combine(seed, value[1]);
		boost::hash_combine(seed, value[2]);
		boost::hash_combine(seed, value[3]);
		boost::hash_combine(seed, value[4]);
		return seed;
	}
};

template<typename Key, typename Value>
struct no_eviction_policy
{
  void on_evict(Key& k, Value& v){}
  void on_delete(Key& k, Value& v){}
};

/**
 * This class implements an LRU cache to store (Key, Value) tuple.
 * The cache is implemented with a (custom) hash_map datastructure. A
 * (Key,Value) pair will be stored according to its hash-value
 * computed by the Hasher object (using Hasher::operator() similarly
 * to boost::unordered_map). When the cache is full, some (Key,Value)
 * pair might be evicted from the cache and the Policy object will be
 * use to perform user defined clean-up by using
 * Policy::on_evict(Key&, Value&). When the LRUCache is destructed,
 * Policy::on_delete(Key&, Value&) will be called on each (Key,Value)
 * pair still in the cache. Key must have an equality operator defined
 * (Key::operator==).
 *
 * Implementation note: The LRUCache is implemented using a double
 * linked list for handling the LRU operations and choose which
 * element should be evicted. The hashmap is implemented with a bucket
 * data structure (an array of double linked list). To avoid memory
 * operations, all the memory needed by the cache are allocated in the
 * storage array. This implies that Key and Value must have a defaut
 * constructor and an affectation operator defined (Key::operator= and
 * Value::operator==).
 *
 * The bucket is a double linked list to allow an easy eviction. It
 * can be made a single linked list but that will require to call
 * Hasher::operator() and Key::operator== more often.
 *
 * The Hasher and the Policy are constructed using the default
 * constructor. But that can be hacked to construct them by copy.
 *
 * The cache can not be resized and the number of bucket is the
 * maximum number of element in the cache. There is no use of pointers
 * internally, but indexing in the storage array with 32-bit
 * integers. If a cache larger than 2**32 elements is required, the
 * data structure will need to be adapted (most like just changing the
 * type of index). If a cache smaller than 2**16 elements is useful,
 * the index could be changed to 16-bit integers to gain space.
 *
 * If the cache is believed to have bugs, turn on the DEBUG template
 * parameter and active assertions. That might not cache all the bugs,
 * but should catch some.
 **/

/**
 * For some reason the following code causes troubles [segmentation fault] on hopper where with a current configuration _OPENMP is defined!!!
 *
#ifdef _OPENMP 
template <typename Key, typename Value, typename Hasher = CombineHash, typename Policy = no_eviction_policy<Key,Value>, typename Locking = openmp_locking, bool DEBUG=false , bool PROFILE=false>
#else
template <typename Key, typename Value, typename Hasher = CombineHash, typename Policy = no_eviction_policy<Key,Value>, typename Locking = no_locking, bool DEBUG=false , bool PROFILE=false>
#endif
**/

template <typename Key, typename Value, typename Hasher = CombineHash, typename Policy = no_eviction_policy<Key,Value>, typename Locking = openmp_locking, bool DEBUG=false , bool PROFILE=false, unsigned int ignore_bit_in_hash=0>
class LRUCache
{
  typedef int index;
public:
  typedef struct iterator {index i; LRUCache* c;
    iterator(){}
    iterator(index i, LRUCache* c):i(i),c(c){}
    Value& operator* (){return c->storage[i].v;}
    //    bool operator== (const iterator& it){return i == it.i && c == it.c;}
    bool operator== (const iterator& it){return i == it.i;} //checking cache should not be necessary

    bool operator!= (const iterator& it){return ! this->operator==( it);}

  } iterator;

  /** provide the memory requirements for LRUCache<Key, Value, ... >
      if you construct an LRUcache with the nelements parameter to the
      constructor. This assumes that Key and Value are static type
      that do not contain dynamically allocated memory.**/
  static size_t memory_requirements(size_t nelements)
  {
    return nelements*(sizeof(element) +sizeof(index)); //There should be a +sizeof(*this) but it is static
  }

  /**
   *  provide the memory requirements for LRUCache<Key, Value,
   *  ... >. This assumes that Key and Value are static type that do
   *  not contain dynamically allocated memory. This one is for a
   *  particular instance of the LRUCache.
   **/
  size_t static_memory_usage() const
  {
    return size*sizeof(element) + nb_bucket*sizeof(index) + sizeof(*this);
  }
  
  /**
   * Computes the memory usage of a particular instance of
   * LRUCache. It uses a functor passed as a parameter to compute the
   * size of all the objects contained within the Cache. It calls
   * size_t SizeOf::operator (const Key&, const Value&) function to
   * obtain the size of the dynamically allocated memory of a given
   * (Key,Value) pair. The returned value also takes into
   * consideration the "static" footprint of the cache.
   *
   * See the end of this file for a simple example on how to use it.
   **/
  template < typename SizeOf >
  size_t dynamic_memory_usage(SizeOf& so) const
  {
    size_t actsize = this->static_memory_usage();
    for (int buck = 0; buck < nb_bucket; ++buck)
      {
	index ind = bucket[buck];
	while (ind >= 0) {
	  element& e = storage[ind];
	  actsize += so (e.k, e.v);
	  ind = e.bucket_next;
	}
      }
    return actsize;
  }
private:
  struct element
  {
    index list_prev;//-1 => head of list
    index list_next;//-1 => tail of list
    index bucket_prev;//-x => head of bucket (x-1)
    index bucket_next; //-1 => tail of bucket 
    Key k;
    Value v;
  };

  element* storage;
  index list_head, list_tail;
  index empty; //-1 for none; points to single linked list
  index* bucket; //give head of bucket

  int nb_bucket;
  int size;

  Hasher h;
  Policy p;
  Locking lock;
  int nb_evict;
  int nb_find;
  int nb_insert;
  /// chose which bucket a key is affected to.
  int which_bucket(const Key& k) const {
    return (h(k)>>ignore_bit_in_hash)%nb_bucket;
  }    

  void evict()
  {
    if (DEBUG) assert (coherent());
	 if (PROFILE) nb_evict++;
    index victim = list_head;
    if (DEBUG) assert (victim >= 0);
    //the successor of victim in list is the new head of list
    list_head = storage[victim].list_next;
    storage[list_head].list_prev = -1;
    
    //victim is the new head of empty
    storage[victim].list_next = empty;
    empty = victim;
    
    //remove victim from bucket
    if (storage[victim].bucket_prev < 0)
      bucket[-storage[victim].bucket_prev-1] = storage[victim].bucket_next;
    else
      storage[storage[victim].bucket_prev].bucket_next = storage[victim].bucket_next;
    
    if (storage[victim].bucket_next >= 0)
      storage[storage[victim].bucket_next].bucket_prev = storage[victim].bucket_prev;

    //call eviction policy
    p.on_evict(storage[victim].k, storage[victim].v);
    
    if (DEBUG) assert (coherent());
  }


public:
  iterator end(){
    return iterator(-1,this);
  }
  LRUCache () {};

  LRUCache (int size)
  {
  /// initialize a cache that stores size objects. Subsequently added object will cause an eviction.
     AllocateMemory(size);
  }

  void AllocateMemory(int size)
  {
    assert (size > 0);
    storage = new element[size];
    this->size = size;

	 if (PROFILE) nb_evict = 0;
	 if (PROFILE) nb_find = 0;
	 if (PROFILE) nb_insert = 0;

    //init "empty" linked list

    list_head = -1;
    list_tail = -1;

    for (index i = 0; i < size; i++) {
      storage[i].list_next = i+1;
    }

    storage[size-1].list_next = -1;

    empty = 0;

    //init buckets

    nb_bucket = size;
    bucket = new index[nb_bucket];
    for (int i=0; i<nb_bucket; i++) {
      bucket[i] = -1;
    }

    if (DEBUG) assert(coherent());
  }

  int nelems() const
  {
	  return this->size;
  }


  ~LRUCache()
  {
    ReleaseMemory();
  }

  /// calls the eviction policy on all the objects in the cache
  void ReleaseMemory() 
  { 
    if (storage) 
      {
	//	call eviction policy
	for (index i = list_head; i != -1; i = storage[i].list_next) 
	  { 
	    p.on_delete (storage[i].k, storage[i].v);
	  }
	if (PROFILE) 
	  { 
	    std::cerr<<"LRU_cache : "<<this<<std::endl; 
	    std::cerr<<"nb_evict : "<<nb_evict<<std::endl; 
	    std::cerr<<"nb_find : "<<nb_find<<std::endl; 
	    std::cerr<<"nb_insert : "<<nb_insert<<std::endl; 
	    std::cerr<<"hit_ratio : "<<((double)(nb_find-nb_insert))/(nb_find)<<std::endl<<std::endl;
	  }
      }
    //	free own memory
    if (bucket) {delete[] bucket; bucket = NULL;}
    if (storage) {delete[] storage; storage = NULL;}
   
  }

  /// might invalidate operator by evicting one object from the cache (eviction policy will be used)
  void insert (const Key& k, const Value & v)
  {
    lock.acquire();
    if (DEBUG) assert (coherent());

	 if (PROFILE) nb_insert++;

    if (empty == -1)
      evict();

    index newelem = empty;
    if (DEBUG) assert (newelem != -1);
    //move empty further
    empty = storage[newelem].list_next;

    //affect values
    storage[newelem].k = k;
    storage[newelem].v = v;  

    //insert newelem at end of list
    storage[newelem].list_prev = list_tail;
    storage[newelem].list_next = -1;
    if (list_tail != -1)
      storage[list_tail].list_next = newelem;

    list_tail = newelem;

    //if list is empty, tail is head
    if (list_head == -1)
      list_head = list_tail;

    //insert newelem in bucket at the head
    index wbuck = which_bucket(k);
    if (DEBUG) assert (wbuck >= 0 && wbuck < nb_bucket);

    storage[newelem].bucket_next = bucket[wbuck];
    if (bucket[wbuck] != -1)
      storage[bucket[wbuck]].bucket_prev = newelem;
    storage[newelem].bucket_prev = -wbuck-1;
    bucket[wbuck] = newelem;

    if (DEBUG) assert(coherent());

    lock.release();
  }

  /// does not invalidate operators.
  /// returns end() for not found
  iterator find (const Key& k)
  {
    lock.acquire();

    index wbuck = which_bucket(k);
    if (DEBUG) assert (wbuck >= 0 && wbuck < nb_bucket);
	 if (PROFILE) nb_find++;

    index current = bucket[wbuck];

    if (DEBUG) assert(coherent());

    while (current != -1) {
      element& current_elem = storage[current];

      if (current_elem.k == k)
	{
	  //update LRU list
	  if (current != list_tail) //no update to be done otherwise
	    {
	      //remove first
	      if (current_elem.list_prev == -1) //at the beginning
		list_head = current_elem.list_next;
	      else //somewhere inside
		  storage[current_elem.list_prev].list_next = current_elem.list_next;

	      storage[current_elem.list_next].list_prev = current_elem.list_prev;

	      //then insert
	      current_elem.list_next = -1;
	      storage[list_tail].list_next = current;
	      current_elem.list_prev = list_tail;
	      list_tail = current;

	      if (DEBUG) assert(coherent());
	    }

	  //return appropriate value
	  lock.release();
	  return iterator(current, this);
	}

      current = current_elem.bucket_next;
    }

    //if I reach here, the object is not found
    lock.release();
    return end();
  }


  /// This is a function I used to make sure the cache is working
  static void test1()
  {
    std::cout<<"test1"<<std::endl;
    LRUCache<int, int, Hasher> ca(5);
    
    ca.insert (1,2);
    assert (ca.coherent());
    
    assert (*(ca.find(1)) == 2);
    assert (ca.coherent());
    
    assert (ca.find(2) == ca.end());
    assert (ca.coherent());
    
    ca.insert (2,12);
    assert (ca.coherent());
    
    ca.insert (3,2);
    assert (ca.coherent());
    
    ca.insert (4,2);
    assert (ca.coherent());
    
    ca.insert (5,2);
    assert (ca.coherent());
    
    ca.insert (6,2);
    assert (ca.coherent());
    
    assert (ca.find(1) == ca.end());
    assert (ca.coherent());
    
    ca.find(2);
    assert (ca.coherent());
    
    ca.insert(7,2);
    assert (ca.coherent());
    
    assert(*(ca.find(2)) == 12);
    assert (ca.coherent());
    
    assert (ca.find(3) == ca.end());
    assert (ca.coherent());
  }
  
  /// This is a function I used to make sure the cache is working
  static void test2()
  {
    std::cout<<"test2 (can take a couple minute)"<<std::endl;
    LRUCache<int, int, Hasher> ca(50);

    for (int i=0; i<50; i++)
      ca.insert (i, rand());

    assert (ca.coherent());

    for (int i = 50; i< 10000; i++)
      {
	ca.insert (i, rand());
	assert (ca.coherent());
	for (int j=0; j<10; j++)
	  {
	    ca.find(rand()%i);
	    assert (ca.coherent());
	  }
      }

  }

  /// This is a function I used to make sure the cache is working
  static void test3()
  {
    std::cout<<"test3"<<std::endl;
    LRUCache<int, int, Hasher> ca(50);

    ca.insert(2,12);
    assert (ca.coherent());
    ca.insert(52,18);
    assert (ca.coherent());
    ca.find(102);
    assert (ca.coherent());
  }

  /// Calling this function outputs the internal structure of the
  /// cache to stdout. Usefull for debugging only.
  void show_state()
  {
    std::cout<<"--- raw ---"<<std::endl;
    std::cout<<"list_head:"<<list_head<<" list_tail: "<<list_tail<<std::endl;
    std::cout<<"empty: "<<empty<<std::endl;

    std::cout<<"storage"<<std::endl;
    for (index i=0; i<size; i++)
      {
	element & e = storage [i];
	std::cout<<i<<" : "<<e.list_prev<<" "<<e.list_next<<" "
		 <<e.bucket_prev<<" "<<e.bucket_next<<" "
		 <<e.k<<" "<<e.v<<std::endl;
      }

    std::cout<<"bucket"<<std::endl;
    for (int i=0; i<nb_bucket; i++)
      {
	std::cout<<i<<" : "<<bucket[i]<<std::endl;
      }
    std::cout<<"--- pretty ---"<<std::endl;
    std::cout<<"empty list"<<std::endl;
    for (index i = empty; i != -1; i = storage[i].list_next)
      std::cout<<i<<" ";
    std::cout<<std::endl;
    std::cout<<"LRU list"<<std::endl;
    for (index i = list_head; i != -1; i = storage[i].list_next)
      std::cout<<i<<" ";
    std::cout<<std::endl;
    std::cout<<"LRU list (in reverse)"<<std::endl;
    for (index i = list_tail; i != -1; i = storage[i].list_prev)
      std::cout<<i<<" ";
    std::cout<<std::endl;
    std::cout<<"buckets"<<std::endl;
    for (int j=0; j< nb_bucket; j++)
      {
	index i;
	index old_i = -1;
	int nbcount = 0;
	for (i = bucket[j]; i >=0 && nbcount < size; old_i = i, i = storage[i].bucket_next, nbcount++)
	  {
	    assert (old_i != i);
	    std::cout<<i<<" ";
	  }
	i = old_i;
	std::cout<<"// ";
	for (; i >= 0; i = storage[i].bucket_prev)
	  std::cout<<i<<" ";
	std::cout<<std::endl;
      }
  }

private:
  /// returns false if an incoherency in the cache is detected. Notice
  /// that this function might not catch all inconsistencies.
  bool coherent()
  {
    {
      int nbcount = 0;
      for (index i = empty; i != -1; i = storage[i].list_next, nbcount++)
	{
	  if (nbcount > size)//no loop
	    return false;
	}
    }

    {
      int nbcount_for = 0;
      for (index i = list_head; i != -1; i = storage[i].list_next, nbcount_for ++)
	{
	  if (nbcount_for > size) //no loop
	    return false;
	  if (storage[i].bucket_prev < 0)//head of bucket properly set (weaker test)
	    if (which_bucket(storage[i].k) != -storage[i].bucket_prev - 1)
	      return false;
	}

      int nbcount_back = 0;
      for (index i = list_tail; i != -1; i = storage[i].list_prev, nbcount_back ++)
	if (nbcount_back > size) //no loop
	  return false;

      if (nbcount_back != nbcount_for)//as many element in forward list and reverse list
	return false;
    }


    {
    for (int j=0; j< nb_bucket; j++)
      {
	index i;
	int nbcount = 0;
	for (i = bucket[j]; i >=0 ; nbcount++, i = storage[i].bucket_next)
	  {
	    if (i == empty) // an element in a bucket is not in the empty list (test weaker)
	      return false;
	    if (j != which_bucket(storage[i].k)) //is the element in the right bucket
	      return false;
	    if (storage[i].bucket_next == storage[i].bucket_prev && storage[i].bucket_prev >= 0) //no cycle (test weaker)
	      return false;
	    if (nbcount > size) //no loop
	      return false;
	  }
      }      
    }

    return true;
  }

};


// // That's an inefficient implementation of LRUCache
// template <typename Key, typename Value, typename Hasher>
// class LRUCache2
// {
// private:
//   typedef boost::unordered_map<Key, std::pair<Value, int>, Hasher> HASH_MAP;

//   HASH_MAP the_map;

//   int nb_access;
//   int max_size;

// public:
//   LRUCache2 (int size)
//   {
//     nb_access = 0;
//     the_map.rehash(size+2);
//     max_size=size;
//   }

//   void evict()
//   {
//     assert (!(the_map.empty()));
//     typename HASH_MAP::iterator cand = the_map.begin();
//     for (typename HASH_MAP::iterator it = the_map.begin(); it != the_map.end(); it++)
//       {
// 	if (cand->second.second > it->second.second)
// 	  cand = it;
//       }
//     the_map.erase(cand);
//   }

//   void insert (const Key& k, const Value & v)
//   {
//     if (the_map.size() == max_size)
//       evict();

//     the_map.insert(std::make_pair(k,std::make_pair(v, ++nb_access)));
//   }

//   Value* find (const Key& k)//returns NULL for not found
//   {
//     typename HASH_MAP::iterator it = the_map.find(k);
//     if (it == the_map.end())
//       return NULL;
//     else
//       {
// 	it->second.second = ++nb_access;
// 	return &(it->second.first);
//       }
//   }
  
// };


#endif



//////////////////////////////////////////////////////////////////
/////////////Example of use of dynamic_memory_usage///////////////
//////////////////////////////////////////////////////////////////

/* #include <iostream> */
/* #include <SU3NCSMUtils/CTuple.h> */
/* #include <LookUpContainers/LRUCache.h> */

/* struct NilPotentHash { */
/*   inline std::size_t operator()(int value) const {return value;} */
/* }; */

/* struct thesizeofnothing { */
/*   size_t operator() (const int& a, const int& b) */
/*   { */
/*     return 1; */
/*   } */
/* }; */
  

/* int main() */
/* { */
/*   std::cout<<"test4"<<std::endl; */
/*   LRUCache<int, int, NilPotentHash, no_eviction_policy<int,int>, no_locking> ca(50); */
  
/*   ca.insert(2,12); */
/*   ca.insert(52,18); */

/*   std::cout<<ca.static_memory_usage()<<std::endl; */

/*   thesizeofnothing foo; */
/*   size_t s = ca.dynamic_memory_usage<thesizeofnothing> (foo); */
/*   std::cout<<s<<std::endl; */


/*   return 0; */
/* } */
//////////////////////////////////////////////////////////////////
