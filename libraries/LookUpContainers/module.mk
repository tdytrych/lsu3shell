$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h := CWig9lmLookUpTable HashFixed LRUCache  CSU39lm lock\

module_units_cpp-h := CSSTensorRMELookUpTablesContainer WigEckSU3SO3CGTable  lock
# module_units_f := 
# module_programs_cpp :=

################################################################
# library creation flag
################################################################

$(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################


$(eval $(end-module))
