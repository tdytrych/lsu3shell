#ifndef CSU39LM_H
#define CSU39LM_H
#include <UNU3SU3/UNU3SU3Basics.h>
#include <UNU3SU3/CSU3Master.h>
#include <LookUpContainers/CWig6lmLookUpTable.h>
#include <cstdlib>
#include <utility>

template <typename NINELMTYPE>
class CSU39lm
{
	// 6lm symbols are internally stored in double precision
	typedef CWig6lmLookUpTable<double> LOOK_UP_TABLE;
	LOOK_UP_TABLE sixLMSymbols_;
    public: 
	CSU39lm(const size_t number_u6_symbols_max, const size_t number_z6_symbols_max):sixLMSymbols_(number_u6_symbols_max, number_z6_symbols_max) {}
	CSU39lm() {}

   void AllocateMemory(size_t number_u6_symbols_max, size_t number_z6_symbols_max)
   {
      sixLMSymbols_.AllocateMemory(number_u6_symbols_max, number_z6_symbols_max);
   }

	void ReleaseMemory()
	{
		sixLMSymbols_.ReleaseMemory();
	}
	void ShowInfo()
	{
		sixLMSymbols_.ShowInfo();
	}

	void coeffs_info(size_t& u6_size, size_t& z6_size) {sixLMSymbols_.coeffs_info(u6_size, z6_size); }

	void memory_usage(size_t& u6_size, size_t& z6_size) { sixLMSymbols_.memory_usage(u6_size, z6_size); }
	void ShowMemoryUsage() 
	{
		sixLMSymbols_.ShowMemoryUsage(); 
	}

	void Get9lm(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
			  	const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
			  	const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
			  	NINELMTYPE* su39lm);

  void Get9lmPtoPN_fit1(int32_t rhofmax, int32_t rhoimax, int32_t rhotmax,
                        int32_t rho1max, const SU3::LABELS &wp_col,
                        const SU3::LABELS &t0, const SU3::LABELS &wp_row,
                        const SU3::LABELS &wn, const SU3::LABELS &wpn_col,
                        const SU3::LABELS &wpn_row, NINELMTYPE *su39lm);

	void Get9lm(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
			  	const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
			  	const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
				const size_t number_9lm,
			  	float* su39lm)
	{
		double tmp[number_9lm];
		Get9lm(ir1, ir2, ir12, ir3,  ir4, ir34, ir13, ir24, ir, tmp);
		std::copy(tmp, tmp + number_9lm, su39lm);
	}
};

template <typename NINELMTYPE>
void CSU39lm<NINELMTYPE>::Get9lmPtoPN_fit1(
    int32_t rhofmax, int32_t rhoimax, int32_t rhotmax, int32_t rho1max,
    const SU3::LABELS &wp_col, const SU3::LABELS &t0, const SU3::LABELS &wp_row,
    const SU3::LABELS &wn, const SU3::LABELS &wpn_col,
    const SU3::LABELS &wpn_row, NINELMTYPE *su39lm) {
  LOOK_UP_TABLE::SixLMStorageType *u6_a;
  LOOK_UP_TABLE::SixLMStorageType *u6_b;
  LOOK_UP_TABLE::SixLMStorageType *u6_c;

  // multiplicities for 6lm and z6lm coefficients
  int32_t rho123max = rhofmax;
  int32_t rho132max = rhotmax;
  sixLMSymbols_.GetWigner6lm(wp_col, t0, wp_row, wn, SU3::LABELS(0, 0), wn, wpn_col,
                             t0, wpn_row, wpn_row, rho132max, 1, 1, rhotmax,
                             rho1max, rho123max, rhoimax, 1, rhofmax, &u6_a,
                             &u6_b, &u6_c);
  int32_t n1_b = rho1max;
  int32_t n2_b = rho123max * n1_b;
  int32_t n3_b = rhoimax * n2_b;

  int32_t i9lm = 0;
  for (int32_t rhof = 0; rhof < rhofmax; ++rhof) {
    for (int32_t rhoi = 0; rhoi < rhoimax; ++rhoi) {
      for (int32_t rhot = 0; rhot < rhotmax; ++rhot) {
        for (int32_t rho1 = 0; rho1 < rho1max; ++rho1) {

          int32_t iu6_a = rhot * rho132max;
          for (int32_t rho132 = 0; rho132 < rho132max; ++rho132) {
            int32_t iu6_b = rho132 * n3_b + rhoi * n2_b + rho1;
            int32_t iu6_c = rhof * rho123max;
            NINELMTYPE temporary = 0.0;
            for (int32_t rho123 = 0; rho123 < rho123max; ++rho123) {
              temporary += u6_b[iu6_b] * u6_c[iu6_c];
              ++iu6_c;
              iu6_b += n1_b;
            }
            su39lm[i9lm] += u6_a[iu6_a] * temporary;
            ++iu6_a;
          }

          ++i9lm;
        }
      }
    }
  }
}

template <typename NINELMTYPE>
void CSU39lm<NINELMTYPE>::Get9lm(	const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
						const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
						const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
						NINELMTYPE* su39lm)
{
	LOOK_UP_TABLE::SixLMStorageType* u6_a;
	LOOK_UP_TABLE::SixLMStorageType* u6_b;
	LOOK_UP_TABLE::SixLMStorageType* u6_c;

	int rho12max 	= SU3::mult(ir1, ir2, ir12);
	int rho34max 	= SU3::mult(ir3, ir4, ir34);
	int rho13max 	= SU3::mult(ir1, ir3, ir13);
	int rho24max 	= SU3::mult(ir2, ir4, ir24);
	int rho1234max	= SU3::mult(ir12, ir34, ir);
	int rho1324max	= SU3::mult(ir13, ir24, ir);
	int nsu39lm = rho12max*rho34max*rho13max*rho24max*rho1234max*rho1324max;

	int rho123max, rho04max, rho132max; // multiplicities for 6lm and z6lm coefficients

	int index_9lm, index_u6_a, index_u6_b, index_u6_c;

	int n1_a, n2_a, n3_a;
	int n1_b, n2_b, n3_b;
	int n1_c, n2_c, n3_c;

	memset(su39lm, 0, nsu39lm*sizeof(NINELMTYPE));

	SU3_VEC ir0_vector;
	SU3::Couple(ir13, ir2, ir0_vector);

	for (SU3_VEC::const_iterator ir0 = ir0_vector.begin(); ir0 != ir0_vector.end(); ++ir0)
	{
		rho123max = SU3::mult(ir12, ir3, *ir0);
		if (!rho123max) {
			continue;
		}
		rho04max = SU3::mult(*ir0, ir4, ir);
		if (!rho04max) {
			continue;
		}
		rho132max = ir0->rho;
   
		sixLMSymbols_.GetWigner6lm(	ir1, ir2, ir12, 
									ir3, ir4, ir34, 
									ir13, ir24, ir, 
									*ir0, 
									rho132max, rho04max, rho24max, rho1324max, rho12max, rho123max, rho13max, rho34max, rho1234max,
									&u6_a, &u6_b, &u6_c);

		index_9lm = 0;

		n1_a = rho132max;
		n2_a = rho04max*n1_a;
		n3_a = rho24max*n2_a;

		n1_b = rho12max;
		n2_b = rho123max*n1_b;
		n3_b = rho13max*n2_b;

		n1_c = rho123max;
		n2_c = rho04max*n1_c;
		n3_c = rho34max*n2_c;

		for (size_t rho1324 = 0; rho1324 < rho1324max; ++rho1324)
		{
			for (size_t rho24 = 0; rho24 < rho24max; ++rho24)
			{
				for (size_t rho13 = 0; rho13 < rho13max; ++rho13)
				{
					for (size_t rho1234 = 0; rho1234 < rho1234max; ++rho1234)
					{
						for (size_t rho34 = 0; rho34 < rho34max; ++rho34)
						{
							for (size_t rho12 = 0; rho12 < rho12max; ++rho12, ++index_9lm)
							{
								for (size_t rho04 = 0; rho04 < rho04max; ++rho04)
								{
									for (size_t rho132 = 0; rho132 < rho132max; ++rho132)
									{
										for (size_t rho123 = 0; rho123 < rho123max; ++rho123)
										{
											index_u6_a = rho1324*n3_a + rho24*n2_a + rho04*n1_a + rho132;
											index_u6_c = rho1234*n3_c + rho34*n2_c + rho04*n1_c  + rho123;
											index_u6_b = rho132*n3_b  + rho13*n2_b + rho123*n1_b + rho12;
	
											su39lm[index_9lm] += u6_a[index_u6_a]*u6_b[index_u6_b]*u6_c[index_u6_c];
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
#endif
