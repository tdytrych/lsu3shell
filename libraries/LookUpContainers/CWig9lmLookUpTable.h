#ifndef CWIG9LMLOOKUPTABLE_H
#define CWIG9LMLOOKUPTABLE_H

#include <LookUpContainers/CSU39lm.h>
#include <LookUpContainers/HashFixed.h>
#include <LookUpContainers/LRUCache.h>
#include <SU3ME/global_definitions.h>

//#define SU3_9LM_HASHINDEXEDARRAY
#ifdef SU3_9LM_HASHINDEXEDARRAY
#include <LookUpContainers/HashIndexedArray.h>
#endif

template <typename DOUBLE>
class CWig9lmLookUpTable {
  public:
   template <typename Key, typename Value>
   struct deleteArrayPolicy {
      void on_evict(const Key& k, const Value& v) { delete[] v; }
      void on_delete(const Key& k, const Value& v) { delete[] v; }
   };

#ifdef SU3_9LM_HASHINDEXEDARRAY

   typedef HashIndexedArray<CTuple<int, 5>, DOUBLE /* not DOUBLE* !!! */, CombineHash, 8192, 2048> WIGNER9_LOOK_UP_TABLE;

#else  // SU3_9LM_HASHINDEXEDARRAY

//  typedef LRUCache<CTuple<int, 5>, DOUBLE*, CombineHash, deleteArrayPolicy<CTuple<int, 5>, DOUBLE*
//  > > WIGNER9_LOOK_UP_TABLE;
//  typedef SplitLRUCache<CTuple<int, 5>, DOUBLE*, CombineHash, deleteArrayPolicy<CTuple<int, 5>,
//  DOUBLE* > > WIGNER9_LOOK_UP_TABLE;

#ifdef SU3_9LM_HASHFIXED

   typedef HashFixed<CTuple<int, 5>, DOUBLE*, CombineHash,
                     deleteArrayPolicy<CTuple<int, 5>, DOUBLE*> >
       WIGNER9_LOOK_UP_TABLE;

#else  // SU3_9LM_HASHFIXED

   typedef LRUCache<CTuple<int, 5>, DOUBLE*, CombineHash,
                    deleteArrayPolicy<CTuple<int, 5>, DOUBLE*> >
       WIGNER9_LOOK_UP_TABLE;

#endif  // SU3_9LM_HASHFIXED

#endif  // SU3_9LM_HASHINDEXEDARRAY

   typedef HashFixed<CTuple<int, 3>, DOUBLE*, CombineHash,
                     deleteArrayPolicy<CTuple<int, 3>, DOUBLE*> >
       FREQUENT_LOOK_UP_TABLE;

   /**
    * Functor that calculates size of arrays that are used to store U9
    * coefficients.
    **/
   struct SizeOfU9CoeffsArray {
      inline void KEY_2_SU3(const int key, SU3::LABELS& a, SU3::LABELS& b) const {
         b.mu = key & 0xFF;
         b.lm = (key >> 8) & 0xFF;
         a.mu = (key >> 16) & 0xFF;
         a.lm = (key >> 24) & 0xFF;
      }

      size_t operator()(const CTuple<int, 3>& key, const DOUBLE* u9coeffs_ptr) {
         SU3::LABELS ket2, ket0, tensor2, tensor0, bra2, bra0;

         KEY_2_SU3(key[0], ket2, ket0);        // inverse of key[0] = INTEGER_KEY(ket2, ket0);
         KEY_2_SU3(key[1], tensor2, tensor0);  // inverse of key[1] = INTEGER_KEY(tensor2, tensor0);
         KEY_2_SU3(key[2], bra2, bra0);        // inverse of key[2] = INTEGER_KEY(bra2, bra0);
         size_t ntotal = SU3::mult(ket2, tensor2, bra2) * SU3::mult(ket0, tensor0, bra0);
         return sizeof(DOUBLE) * ntotal;
      }

      size_t operator()(const CTuple<int, 5>& key, const DOUBLE* u9coeffs_ptr) {
         SU3::LABELS bra1, ket0, ket1, bra2, tensor2, ket2, bra0, tensor0, tensor1;

         KEY_2_SU3(key[0], bra1, ket1);
         KEY_2_SU3(key[1], bra2, tensor2);
         KEY_2_SU3(key[2], ket2, bra0);
         KEY_2_SU3(key[3], tensor0, tensor1);
         ket0.mu = key[4] & 0xFF;
         ket0.lm = (key[4] >> 8) & 0xFF;

         int rhoi_max = SU3::mult(ket1, ket2, ket0);
         int rhof_max = SU3::mult(bra1, bra2, bra0);
         int rho0_max = SU3::mult(tensor1, tensor2, tensor0);
         int n2 = SU3::mult(ket1, tensor1, bra1) * SU3::mult(ket2, tensor2, bra2);
         int n3 = n2 * rhof_max;  // rho1_max*rho2_max*rhof_max
         int n4 = n3 * rhoi_max;  // rho1_max*rho2_max*rhof_max*rhoi_max
         int n5 = n4 * rho0_max;  // rho1_max*rho2_max*rhof_max*rhoi_max*rho0_max;
         int ntotal = n5 * SU3::mult(ket0, tensor0, bra0);
         return sizeof(DOUBLE) * ntotal;
      }
   };

  public:
   CWig9lmLookUpTable()
       : n2_(0),
         n3_(0),
         n4_(0),
         n5_(0),
         ntotal_(0),
         rhoi_max_(0),
         rhof_max_(0),
         rho0_max_(0),
         current_su39lm_coeffs_(0) {}

   static void AllocateMemory(bool show_sizes) {
      if (getenv("U9SIZE")) {
         storage_limits::number_9lm_coeffs = atoi(getenv("U9SIZE"));
      }
      if (getenv("U6SIZE")) {
         storage_limits::number_u6lm_coeffs = atoi(getenv("U6SIZE"));
      }
      if (getenv("Z6SIZE")) {
         storage_limits::number_z6lm_coeffs = atoi(getenv("Z6SIZE"));
      }
      if (getenv("U9SIZE_FREQ")) {
         storage_limits::number_freq_9lm_coeffs = atoi(getenv("U9SIZE_FREQ"));
      }

      if (show_sizes) {
         std::cout << "Number of 9lm coeffs in ";
#ifdef SU3_9LM_HASHFIXED
         std::cout << "HashFixed: ";
#else
         std::cout << "LRUCache: ";
#endif
         std::cout << storage_limits::number_9lm_coeffs << std::endl;
         std::cout << "Number of u6lm coeffs in HashFixed: " << storage_limits::number_u6lm_coeffs
                   << std::endl;
         std::cout << "Number of z6lm coeffs in HashFixed: " << storage_limits::number_z6lm_coeffs
                   << std::endl;
         std::cout << "Number of \"frequent\" u9lm coeff in HashFixed: " << storage_limits::number_freq_9lm_coeffs 
                   << std::endl;
      }

      frequent_wig9lmTable_.AllocateMemory(storage_limits::number_freq_9lm_coeffs);

#ifndef SU3_9LM_HASHINDEXEDARRAY
      wig9lmTable_.AllocateMemory(storage_limits::number_9lm_coeffs);
#endif

      su3lib_.AllocateMemory(storage_limits::number_u6lm_coeffs,
                             storage_limits::number_z6lm_coeffs);
   }

   static void ShowInfo() {
      su3lib_.ShowInfo();
      std::cout << "#frequent 9lm:" << frequent_wig9lmTable_.nelems() << std::endl;
#ifndef SU3_9LM_HASHINDEXEDARRAY
#ifdef SU3_9LM_HASHFIXED
      std::cout << "#other 9lm:" << wig9lmTable_.nelems() << std::endl;
#endif
#endif
   }

   static void coeffs_info(size_t& frequent_u9_size, size_t& u9_size, size_t& u6_size,
                           size_t& z6_size) {
      frequent_u9_size = frequent_wig9lmTable_.nelems();
#ifndef SU3_9LM_HASHINDEXEDARRAY

#ifdef SU3_9LM_HASHFIXED
      u9_size = wig9lmTable_.nelems();
#else
      u9_size = 1;
#endif

#else // SU3_9LM_HASHINDEXEDARRAY
      u9_size = 1;
#endif

      su3lib_.coeffs_info(u6_size, z6_size);
   }

   static void ShowMemoryUsage() {
      SizeOfU9CoeffsArray sizeOfU9array;
      std::cout << "size of frequent u9 \n";
      std::cout << "generated: "
                << frequent_wig9lmTable_.dynamic_memory_usage(sizeOfU9array) / (1024.0 * 1024.0) -
                       frequent_wig9lmTable_.static_memory_usage() / (1024.0 * 1024.0)
                << " MB." << std::endl;
      std::cout << "allocated for has data structures: "
                << frequent_wig9lmTable_.static_memory_usage() / (1024.0 * 1024.0) << " MB."
                << std::endl;

#ifndef SU3_9LM_HASHINDEXEDARRAY
      std::cout << "size of other u9 \n";
      std::cout << "generated: "
                << wig9lmTable_.dynamic_memory_usage(sizeOfU9array) / (1024.0 * 1024.0) -
                       wig9lmTable_.static_memory_usage() / (1024.0 * 1024.0)
                << " MB." << std::endl;
      std::cout << "allocated for has data structures: "
                << wig9lmTable_.static_memory_usage() / (1024.0 * 1024.0) << " MB." << std::endl;
#endif
      //	get the size of LRUcache structures for u6 and z6 coefficients
      su3lib_.ShowMemoryUsage();
   }

   static void ReleaseMemory() {
      // Destructors of static variable members will be called at the end of
      // program execution. Therefore I can not call them here. Instead I use
      // ReleaseMemory method to deallocate memory.
      su3lib_.ReleaseMemory();
#ifndef SU3_9LM_HASHINDEXEDARRAY
      wig9lmTable_.ReleaseMemory();
#endif
      frequent_wig9lmTable_.ReleaseMemory();
   }

   static void memory_usage(size_t& frequent_u9_size, size_t& u9_size, size_t& u6_size,
                            size_t& z6_size) {
      SizeOfU9CoeffsArray sizeOfU9array;
      frequent_u9_size = frequent_wig9lmTable_.dynamic_memory_usage(sizeOfU9array);
#ifndef SU3_9LM_HASHINDEXEDARRAY
      u9_size = wig9lmTable_.dynamic_memory_usage(sizeOfU9array);
#else
      u9_size = 1;
#endif
      //	get the size of LRUcache structures for u6 and z6 coefficients
      su3lib_.memory_usage(u6_size, z6_size);
   }

   //	there is no limit on number of 9lm stored in the frequent table ...
   void GetWigner9lm(const SU3xSU2::LABELS& bra2, const SU3xSU2::LABELS& tensor2,
                     const SU3xSU2::LABELS& ket2, const SU3xSU2::LABELS& bra0,
                     const SU3xSU2::LABELS& tensor0, const SU3xSU2::LABELS& ket0) {
      rhoi_max_ = rhof_max_ = rho0_max_ = 1;
      n3_ = n4_ = n5_ = n2_ = SU3::mult(ket2, tensor2, bra2);
      ntotal_ = n5_ * SU3::mult(ket0, tensor0, bra0);

      CTuple<int, 3> key;
      key[0] = INTEGER_KEY(ket2, ket0);
      key[1] = INTEGER_KEY(tensor2, tensor0);
      key[2] = INTEGER_KEY(bra2, bra0);

      typename FREQUENT_LOOK_UP_TABLE::iterator wig9lm = frequent_wig9lmTable_.find(key);
      if (wig9lm == frequent_wig9lmTable_.end()) {
         DOUBLE* su39lm_to_be_stored = new DOUBLE[ntotal_];

         su3lib_.Get9lm(SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), ket2, tensor2,
                        bra2, ket0, tensor0, bra0, ntotal_, su39lm_to_be_stored);
         //			frequent_wig9lmTable_.insert(std::make_pair(key,
         //su39lm_to_be_stored));
         frequent_wig9lmTable_.insert(key, su39lm_to_be_stored);
         current_su39lm_coeffs_ = su39lm_to_be_stored;

         //			std::cout << "Calculated frequent 9lm: ";
         //			for (size_t i = 0; i < ntotal_; ++i)
         //			{
         //				std::cout << su39lm_to_be_stored[i] << " ";
         //			}
         //			std::cout << std::endl;
      } else {
         //			current_su39lm_coeffs_ = wig9lm->second;
         current_su39lm_coeffs_ = *wig9lm;
         //			std::cout << "Obtained frequent 9lm: ";
         //			for (size_t i = 0; i < ntotal_; ++i)
         //			{
         //				std::cout << current_su39lm_coeffs_[i] << " ";
         //			}
         //			std::cout << std::endl;
      }
   }

   void GetWigner9lm(const SU3xSU2::LABELS& bra1, const SU3xSU2::LABELS& tensor1,
                     const SU3xSU2::LABELS& ket1, const SU3xSU2::LABELS& bra2,
                     const SU3xSU2::LABELS& tensor2, const SU3xSU2::LABELS& ket2,
                     const SU3xSU2::LABELS& bra0, const SU3xSU2::LABELS& tensor0,
                     const SU3xSU2::LABELS& ket0) {
      rhoi_max_ = SU3::mult(ket1, ket2, ket0);
      rhof_max_ = SU3::mult(bra1, bra2, bra0);
      rho0_max_ = SU3::mult(tensor1, tensor2, tensor0);
      n2_ = SU3::mult(ket1, tensor1, bra1) * SU3::mult(ket2, tensor2, bra2);
      n3_ = n2_ * rhof_max_;  // rho1_max*rho2_max*rhof_max
      n4_ = n3_ * rhoi_max_;  // rho1_max*rho2_max*rhof_max*rhoi_max
      n5_ = n4_ * rho0_max_;  // rho1_max*rho2_max*rhof_max*rhoi_max*rho0_max;
      ntotal_ = n5_ * SU3::mult(ket0, tensor0, bra0);
      assert(ntotal_);

      //		CTuple<int, 4> key;
      //		size_t index = (ket0.lm << 8) | ket0.mu; // 256*lm + mu
      //		typename WIGNER9_LOOK_UP_TABLE::iterator wig9lms_with_index =
      //wig9lmTable_.begin() + index;
      CTuple<int, 5> key;
      key[0] = INTEGER_KEY(bra1, ket1);
      key[1] = INTEGER_KEY(bra2, tensor2);
      key[2] = INTEGER_KEY(ket2, bra0);
      key[3] = INTEGER_KEY(tensor0, tensor1);
      key[4] = (ket0.lm << 8) | ket0.mu;  // 256*lm + mu

#ifdef SU3_9LM_HASHINDEXEDARRAY

      bool found;
      std::tie(found, current_su39lm_coeffs_) = p_wig9lmTable_->find(key, ntotal_);
      if (!found)
         su3lib_.Get9lm(ket1, tensor1, bra1, ket2, tensor2, bra2, ket0, tensor0, bra0, ntotal_, current_su39lm_coeffs_);

#else  // SU3_9LM_HASHINDEXEDARRAY

      //		typename std::map<CTuple<int, 4>, DOUBLE*>::iterator wig9lm_to_be_found =
      //wig9lms_with_index->find(key);
      //		typename ELEM_WIGNER9_LOOK_UP_TABLE::iterator wig9lm_to_be_found =
      //wig9lms_with_index->find(key);
      typename WIGNER9_LOOK_UP_TABLE::iterator wig9lm_to_be_found = wig9lmTable_.find(key);

      //		if (wig9lm_to_be_found == wig9lms_with_index->end())
      if (wig9lm_to_be_found == wig9lmTable_.end()) {
         //			if (num_9lm_stored_ < max_number_9lms_)
         //			{

         DOUBLE* su39lm_to_be_stored = new DOUBLE[ntotal_];
         su3lib_.Get9lm(ket1, tensor1, bra1, ket2, tensor2, bra2, ket0, tensor0, bra0, ntotal_,
                        su39lm_to_be_stored);
         //				wig9lms_with_index->insert(std::make_pair(key,
         //su39lm_to_be_stored));
         //	wig9lmTable_.insert(std::make_pair(key, su39lm_to_be_stored));
         wig9lmTable_.insert(key, su39lm_to_be_stored);
         current_su39lm_coeffs_ = su39lm_to_be_stored;
         //				num_9lm_stored_++;
         //			}
         //			else
         //			{
         //				su3lib_.Get9lm(ket1, tensor1, bra1, ket2, tensor2, bra2, ket0,
         //tensor0, bra0, ntotal_, out_of_structure_9lm_);
         //				current_su39lm_coeffs_ = out_of_structure_9lm_;
         //			}
         //			std::cout << "Calculated 9lm: ";
         //			for (size_t i = 0; i < ntotal_; ++i)
         //			{
         //				std::cout << su39lm_to_be_stored[i] << " ";
         //			}
         //			std::cout << std::endl;
      } else {
         //			current_su39lm_coeffs_ = wig9lm_to_be_found->second;
         current_su39lm_coeffs_ = *wig9lm_to_be_found;
         //			std::cout << "Obtained 9lm: ";
         //			for (size_t i = 0; i < ntotal_; ++i)
         //			{
         //				std::cout << current_su39lm_coeffs_[i] << " ";
         //			}
         //			std::cout << std::endl;
      }

#endif // SU3_9LM_HASHINDEXEDARRAY

   }

   inline size_t rhof_max() const { return rhof_max_; }
   inline size_t rhoi_max() const { return rhoi_max_; }
   inline size_t rho0_max() const { return rho0_max_; }
   /**	Return pointer to the beginning of the SU(3) 9-lm for fixed value of \f$\rho_{t}, \rho_{0},
    * \rho_{i}, \rho_{f}\f$.
    *
    *   This one-dimensional array is interpreted as a matrix with
    *   \f$\rho_{1}^{\max}\f$ columns and \f$\rho_{2}^{\max}\f$ rows.
    *   The matrix is multiplied by the column vector of rme of the
    *   system 1, and then by the row vector of the rme of the system2.
   */
   inline DOUBLE* GetMatrix(int rhot, int rho0, int rhoi, int rhof) {
      return &current_su39lm_coeffs_[rhot * n5_ + rho0 * n4_ + rhoi * n3_ + rhof * n2_];
   }

#ifdef SU3_9LM_HASHINDEXEDARRAY

   // must be called at the beginning of a parallel section
   static void initialize()
   {
      p_wig9lmTable_ = new WIGNER9_LOOK_UP_TABLE{};
   }

   // must be called at the end of a parallel section
   static void finalize()
   {
      delete p_wig9lmTable_;
   }

#endif

  private:
   inline int INTEGER_KEY(const SU3::LABELS& a, const SU3::LABELS& b) const {
      return (a.lm << 24) | (a.mu << 16) | (b.lm << 8) | b.mu;
   }
   //	Althought 9-(l m) are calculated in double precision, this class stores them in
   //	single precision.
   static CSU39lm<double> su3lib_;

   static FREQUENT_LOOK_UP_TABLE frequent_wig9lmTable_;

#ifdef SU3_9LM_HASHINDEXEDARRAY
   
   static WIGNER9_LOOK_UP_TABLE* p_wig9lmTable_;  // pointer !!!
   #pragma omp threadprivate(p_wig9lmTable_)

#else

   static WIGNER9_LOOK_UP_TABLE wig9lmTable_;

#endif

   size_t n2_, n3_, n4_, n5_, ntotal_, rhoi_max_, rhof_max_, rho0_max_;
   DOUBLE* current_su39lm_coeffs_;
};

template <typename DOUBLE>
typename CWig9lmLookUpTable<DOUBLE>::FREQUENT_LOOK_UP_TABLE
    CWig9lmLookUpTable<DOUBLE>::frequent_wig9lmTable_;

#ifdef SU3_9LM_HASHINDEXEDARRAY

template <typename DOUBLE>
typename CWig9lmLookUpTable<DOUBLE>::WIGNER9_LOOK_UP_TABLE* CWig9lmLookUpTable<DOUBLE>::p_wig9lmTable_;

#else

template <typename DOUBLE>
typename CWig9lmLookUpTable<DOUBLE>::WIGNER9_LOOK_UP_TABLE CWig9lmLookUpTable<DOUBLE>::wig9lmTable_;

#endif

template <typename DOUBLE>
CSU39lm<double> CWig9lmLookUpTable<DOUBLE>::su3lib_;

#endif
