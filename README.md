# lsu3shell 
Symmetry-adapted No-Core Shell Model framework for large-scale ab initio modeling of atomic nuclei using U(3) and Sp(3,R) many-nucleon basis.


### Required external libraries:

- Some BLAS/Lapack implementation ?
- [Boost](https://www.boost.org)
- [Eigen](https://eigen.tuxfamily.org)
- [GNU Scientific Library](https://www.gnu.org/software/gsl/)
- [WIGXJPF](http://fy.chalmers.se/subatom/wigxjpf/) 
- [SU3lib](https://gitlab.com/tdytrych/su3lib/)
    - Must be built with the enabled OpenMP support.
- [OMPILancz](https://gitlab.com/daniel.langr/ompilancz)
    - Header-only; no build required.
