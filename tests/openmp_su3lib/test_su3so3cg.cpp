#include <omp.h>
#include <UNU3SU3/CSU3Master.h>
#include <vector>

using namespace std;

void WigEckSU3SO3CG(const SU3::LABELS& omegaf, const SU3::LABELS& omegai, const SU3::LABELS& Omega0, const SO3::LABEL& L0, std::vector<double>& WigCoeffs)
{
	assert(!(L0%2)); // L0 must be equal 2*L0 !!!

	double dCG[9][9][9][9]; // this array is required by Fortran void wru3_

    int lmi(omegai.lm), mui(omegai.mu), lm0(Omega0.lm), mu0(Omega0.mu), lmf(omegaf.lm), muf(omegaf.mu);
	// Li, Lf, l0 contains angular momenta (and not 2*Lf, 2*Li, and 2*L0
	int Li, Lf, l0 = (L0/2); // NOTE: wu3r3w takes as argument Lf, Li, L0 not 2*Lf, 2*Li, 2*L0 !!!
	int Lfmax = omegaf.lm + omegaf.mu;
	int Limax = omegai.lm + omegai.mu;
	int ki, k0, kf, max_ki, max_k0, max_kf, max_rhot, irhot;

	WigCoeffs.resize(0);
	
	for (int Lf = 0; Lf <= Lfmax; ++Lf)
	{
		if (!SU3::kmax(omegaf, Lf))
		{
			continue;
		}
		for (int Li = 0; Li <= Limax; ++Li)
		{
			if (!SU3::kmax(omegai, Li))
			{
				continue;
			}
			if (SO3::mult(2*Li, L0, 2*Lf) == 0) // SO3::mult expects Li == 2*Li, L0 == 2*L0, and Lf = 2*Lf
			{
				continue;
			}
			memset(dCG, sizeof(dCG), 0);
			wu3r3w_(lmi, mui, lm0, mu0, lmf, muf, Li, l0, Lf, max_rhot, max_ki, max_k0, max_kf, dCG);
//			index += max_kf*max_ki*max_k0*max_rhot;
		   	for (kf = 0; kf < max_kf; kf++) // for all values of k3
			{ 
				for (ki = 0; ki < max_ki; ki++) // for all values of k2
		   		{
					for (k0 = 0; k0 < max_k0; k0++)  // for all values of k1
		   		    {
			    		for (irhot = 0; irhot < max_rhot; irhot++) // for all possible multiplicities
						{
							WigCoeffs.push_back(dCG[kf][k0][ki][irhot]); // store SU(3) wigner coefficient
    				    }
		   			}
   	    		}
    		}
		}
	}
}

int main()
{
	cout << "This code uses openMP to execute wu3r3w on multiple threads to calculate <(2 2) k1=* L1=* (2 2) k2=* L2=2 || (2 2) k3=* L3=*>_{*} and check the final results." << endl;

	blocks_();

	SU3::LABELS wf(1, 2, 2);
	SU3::LABELS wi(1, 2, 2);
	SU3::LABELS w0(1, 2, 2);
	SO3::LABEL LL0 = 4;

	vector<double> wigCoeffs_correct;
//	calculate correct values of <(2 2) k=* L=*; (2 2) k=* L=2 || (2 2) k=* L=*>_{*}
	WigEckSU3SO3CG(wf, wi, w0, LL0, wigCoeffs_correct);

#pragma omp parallel for
	for (int i = 0; i < 10000; ++i)
	{
		vector<double> wigCoeffs;
		WigEckSU3SO3CG(wf, wi, w0, LL0, wigCoeffs);

		for (int j = 0; j < wigCoeffs.size(); ++j)
		{
			if (fabs(wigCoeffs[j] - wigCoeffs_correct[j]) > 1.0e-7)
			#pragma omp critical
			{
				cerr << "Error! su3library is not thread safe! ";
				cerr <<  wigCoeffs[j] << " vs " << wigCoeffs_correct[j] << endl;
				exit(EXIT_FAILURE);
			}
		}
	}
}
