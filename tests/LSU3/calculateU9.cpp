#include "U9U6U3tests.h"
#include <UNU3SU3/CSU3Master.h>
#include <LookUpContainers/CSU39lm.h>

#include <chrono>
#include <map>

#include <boost/array.hpp>
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/serialization/array.hpp>
#include <boost/serialization/map.hpp>

using namespace std;

uint32_t number_u6_symbols(100000);
uint32_t number_z6_symbols(100000);

/*
// Returns <(lm1 mu1) ... ; (lm2 mu2) ... || (lm3 mu3) k3 l3>_{rho} Resulting
// data structure: vector whose elements are {[k1,l1,k2,l2], double}	NOTE:
// it is guaranteed that only non-zero SU(3) Wigner coefficients are returned
size_t GetSU2U1(	const SU3::LABELS& ir1, 
					const SU3::LABELS& ir2, 
					const SU3::LABELS& ir3)
			
{
int NCE1 = 9; 
int NCE2 = 13244;
int KIMAX1 = 3*NCE2;

int NCW1 = 9;
int NCW2 = 42;
int NCW3 = 9030;
int KIMAX2 = 3*NCW3;
const size_t NCW22 = NCW2*NCW2;


	int I3  = 1;
	int NEC(0);
	int max_rho(0), indmax(0);

	int j1ta[NCE2];
	int j2ta[NCE2];
	int iea[NCE2];
	int j1smax[NCW22];
	int j1tmax[NCW22];
	int j2smax[NCW2];
	int j2tmax[NCW2];
	int indmat[NCW22];
	
	double dewu3[KIMAX1];
	double dwu3[KIMAX2];

	int nCGs, eps2max;

    int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
	EPS1LM1EPS2LM2EPS3LM3 Key;

	std::vector<SU3::SU2U1::LABELS> eps3LM3;
	size_t neps3twoLM2 = SU3::GetSU2U1Labels(ir3, eps3LM3);
	int eps3, LM3, eps2, eps1, LM2, LM1;
	int irho, ind, minro, iesj2s, ies, j2s, j1s;
	size_t iposition = 0;
#ifndef AIX			
	xewu3_(lm1, mu1, lm2, mu2, lm3, mu3, I3, NEC, max_rho, indmax, dewu3, j1ta, j2ta, iea, NCE1, NCE2, KIMAX1);
#else
	xewu3 (lm1, mu1, lm2, mu2, lm3, mu3, I3, NEC, max_rho, indmax, dewu3, j1ta, j2ta, iea, NCE1, NCE2, KIMAX1);
#endif
// Order of the following loops is very important. Method uncoupling two
// tensors [constructor of class Tensor] iterates over e3 lm3 and hence we went
// them in sorted order.

	uint32_t ntot(0);
	for (size_t i = 0; i < neps3twoLM2; ++i)
	{
		eps3 	= eps3LM3[i].eps;
		Key.eps3(eps3);
		LM3 	= eps3LM3[i].LM;
		Key.LM3(LM3);
#ifndef AIX			
		xwu3_(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, NEC, dewu3, max_rho, indmax, dwu3, j1smax, j1tmax, j2smax, j2tmax, nCGs, eps2max, indmat, NCW1, NCW2, NCW3, KIMAX2);
#else 
		xwu3 (lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, NEC, dewu3, max_rho, indmax, dwu3, j1smax, j1tmax, j2smax, j2tmax, nCGs, eps2max, indmat, NCW1, NCW2, NCW3, KIMAX2);
#endif
		ntot += nCGs;
	}
	return ntot;
}


void ShowData(map<boost::array<SU3::LABELS, 9>, pair<uint32_t, uint32_t> >& u9list)
{
	typedef uint32_t NUMBER_COEFFS;
	typedef uint16_t FREQUENCY;
	map<NUMBER_COEFFS, FREQUENCY> statistics;
	
	CSU39lm<double> su3lib(number_u6_symbols, number_z6_symbols);
	uint32_t ncoeffs(0);
	uint32_t nncoeffs(0);

	cout << "ir1 ir2 ir12 ir3 ir4 ir34 ir13 ir24 ir1234 \t #occurences  \t#coeffs \t#vanishing \t {coeffs}" << endl;

	for (map<boost::array<SU3::LABELS, 9>, pair<uint32_t, uint32_t> >::iterator it = u9list.begin(); it != u9list.end(); ++it)
	{
		int nsu39lm = it->second.first;
		double su39lm[nsu39lm];

		cout << "(" << (int)it->first[0].lm << " " << (int)it->first[0].mu << ") ";
		cout << "(" << (int)it->first[6].lm << " " << (int)it->first[6].mu << ") ";
		cout << "(" << (int)it->first[3].lm << " " << (int)it->first[3].mu << ") ";
		cout << "(" << (int)it->first[1].lm << " " << (int)it->first[1].mu << ") ";
		cout << "(" << (int)it->first[7].lm << " " << (int)it->first[7].mu << ") ";
		cout << "(" << (int)it->first[4].lm << " " << (int)it->first[4].mu << ") ";
		cout << "(" << (int)it->first[2].lm << " " << (int)it->first[2].mu << ") ";
		cout << "(" << (int)it->first[8].lm << " " << (int)it->first[8].mu << ") ";
		cout << "(" << (int)it->first[5].lm << " " << (int)it->first[5].mu << ") ";
		
		cout << "\t" << it->second.second << "\t" << nsu39lm << "\t";
//		su3lib.Get9lm(it->first[0], it->first[6], it->first[3], it->first[1], it->first[7], it->first[4], it->first[2], it->first[8], it->first[5], su39lm);
	
		std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
		Get9lmWithout6lmCache(it->first[0], it->first[6], it->first[3], it->first[1], it->first[7], it->first[4], it->first[2], it->first[8], it->first[5], su39lm);
		std::chrono::duration<double> duration = (std::chrono::system_clock::now() - start);

		int nzeros = std::count_if(su39lm, su39lm + nsu39lm, Negligible);
		ncoeffs += nsu39lm;
		nncoeffs += (nsu39lm - nzeros);
		cout << nzeros << "\t" << duration.count() << "\t";
		for (int i = 0; i < nsu39lm;++i)
		{
			cout << su39lm[i] << " ";
		}
		cout << endl;

		statistics[nsu39lm] += 1;
	}
	cout << "#9lm coeffs\t frequency" << endl;
	for (map<NUMBER_COEFFS, FREQUENCY>::iterator it = statistics.begin(); it != statistics.end(); ++it)
	{
		cout << it->first << "\t" << it->second << endl;
	}
	cout << "#9-(l m): " << u9list.size() << endl;
	cout << "#coeffs: " << ncoeffs << endl;
	cout << "#nonvanishing: " << nncoeffs << endl;
}

void GenerateSU3WigCoeffs(map<boost::array<SU3::LABELS, 6>, uint32_t>& u6coeffs)
{
	set<boost::array<SU3::LABELS, 3> > wigCoeffs;
	boost::array<SU3::LABELS, 6> u6Labels;
	boost::array<SU3::LABELS, 3> key;

	for (map<boost::array<SU3::LABELS, 6>, uint32_t>::iterator it = u6coeffs.begin(); it != u6coeffs.end(); ++it)
	{
		u6Labels = it->first;
		key[0] = u6Labels[0];
		key[1] = u6Labels[1];
		key[2] = u6Labels[4];
		wigCoeffs.insert(key);

		key[0] = u6Labels[4];
		key[1] = u6Labels[3];
		key[2] = u6Labels[2];
		wigCoeffs.insert(key);

		key[0] = u6Labels[1];
		key[1] = u6Labels[3];
		key[2] = u6Labels[5];
		wigCoeffs.insert(key);

		key[0] = u6Labels[0];
		key[1] = u6Labels[5];
		key[2] = u6Labels[2];
		wigCoeffs.insert(key);
	}

	CSU3CGMaster SU3CG;
	cout << "#SU(3) wig coeffs: " << wigCoeffs.size() << endl;
	size_t ncoeffs(0);

	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	for (set<boost::array<SU3::LABELS, 3> >::iterator it = wigCoeffs.begin(); it != wigCoeffs.end(); ++it)
	{
		ncoeffs += GetSU2U1((*it)[0], (*it)[1], (*it)[2]);
	}
	std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
	
	cout << "#coeffs:" << ncoeffs << endl;
	cout << "#time 2 calculate:" << duration.count() << endl;
}
*/


/* 
 * Call method invoking F77 procedure of su3lib to calculate 9-(l m) symbol
 * without using cached 6-(l m) symbols.
 */
void CalculateU9_Without_U6Cache(U9LIST& u9list)
{
	CSU3CGMaster su3lib;
	double su39lm[9*9*9*9*9*9];

	for (U9LIST::iterator it = u9list.begin(); it != u9list.end(); ++it)
	{
		uint32_t number_coeffs = it->second.first;
		su3lib.Get9lm(it->first[0], it->first[6], it->first[3], it->first[1], it->first[7], it->first[4], it->first[2], it->first[8], it->first[5], number_coeffs, su39lm);
	}
}

/* 
 * Calculate 9-(l m) without 6-(l m) cache using F77 subroutines of su3lib to obtain 6-(l m) 
 * coeffitients and with summation implemented in C++ version.
 */
void CalculateU9_Without_U6Cache_Cpp(U9LIST& u9list)
{
	double su39lm[9*9*9*9*9*9];

	for (U9LIST::iterator it = u9list.begin(); it != u9list.end(); ++it)
	{
		uint32_t number_coeffs = it->second.first;
		Get9lmWithout6lmCache(it->first[0], it->first[6], it->first[3], it->first[1], it->first[7], it->first[4], it->first[2], it->first[8], it->first[5], su39lm);
	}
}

/* 
 * Calculate 9-(l m) using cached 6-(l m) as implemented currently in su3shell code.cache using F77 subroutines of su3lib to obtain 6-(l m) 
 */
void CalculateU9(U9LIST& u9list, CSU39lm<double>& su3lib)
{
	double su39lm[9*9*9*9*9*9];

	for (map<boost::array<SU3::LABELS, 9>, pair<uint32_t, uint32_t> >::iterator it = u9list.begin(); it != u9list.end(); ++it)
	{
		su3lib.Get9lm(it->first[0], it->first[6], it->first[3], it->first[1], it->first[7], it->first[4], it->first[2], it->first[8], it->first[5], su39lm);
	}
}

int main(int argc,char **argv)
{
	if (argc != 2)
	{
		cout << "Usage: "<< argv[0] <<" <U9 file name>" << endl;
		return EXIT_FAILURE;
	}

	cout << "Loading U9 symbols ... ";cout.flush();
	ifstream input_file(argv[1],  std::ios::binary);
	if (!input_file)
	{
		cerr << "Could not open '" << argv[1] << "' input file with 9-(lm mu) coefficients" << endl;
		return EXIT_FAILURE;
	}

	boost::archive::binary_iarchive ia(input_file);

	U9LIST u9list;
	ia  >> u9list;
	cout << "Done" << endl;

	int itype;

	cout << "Enter the way to calculate U9 symbols" << endl; 
	cout << "1: without hashing U6 symbols" << endl;
	cout << "2: hashing U6 symbols" << endl;
	cin >> itype;

	if (itype == 2)
	{
		cout << "Enten the Number of u6 symbols:";
		cin >> number_u6_symbols;
		cout << endl << "Enter the number of z6 symbols:";
		cin >> number_z6_symbols;

		CSU39lm<double> su3lib(number_u6_symbols, number_z6_symbols);
	
		cout << "#u6 symbols in hash:" << number_u6_symbols << endl;
		cout << "#z6 symbols in hash:" << number_z6_symbols << endl;

		cout << "Starting calculating " << u9list.size() << " 9-(l m) symbols ... " << endl;
		std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
		CalculateU9(u9list, su3lib);
		std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
		cout << "\tDone" << endl << "time:" << duration.count() << endl;
	}
	else
	{
		cout << "Starting calculating " << u9list.size() << " 9-(l m) symbols ... " << endl;
		std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
//		CalculateU9_Without_U6Cache(u9list);
		CalculateU9_Without_U6Cache_Cpp(u9list);
		std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
		cout << "\tDone" << endl << "time:" << duration.count() << endl;
	}
	return EXIT_SUCCESS;
}
