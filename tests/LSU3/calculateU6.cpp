#include "U9U6U3tests.h"
#include <UNU3SU3/CSU3Master.h>

#include <chrono>

#include <boost/archive/binary_iarchive.hpp> 
#include <boost/serialization/map.hpp>
#include <boost/serialization/array.hpp>

using namespace std;

void CalculateU6(U6LIST& u6list)
{
	CSU3CGMaster su3lib;
	U6LABELS u6Labels;
	std::vector<double> du6;

	for (U6LIST::iterator it = u6list.begin(); it != u6list.end(); ++it)
	{
		u6Labels = it->first;
		su3lib.Get6lm(CSU3CGMaster::U6LM, u6Labels[0], u6Labels[1], u6Labels[2], u6Labels[3], u6Labels[4], u6Labels[5], du6);
	}
}

int main(int argc,char **argv)
{
	if (argc != 2)
	{
		cout << "Usage: "<< argv[0] <<" <U6 file name>" << endl;
		return EXIT_FAILURE;
	}

	ifstream input_file(argv[1],  std::ios::binary);
	if (!input_file)
	{
		cerr << "Could not open '" << argv[1] << "' input file with U6 coefficients" << endl;
		return EXIT_FAILURE;
	}

	U6LIST u6list;
	boost::archive::binary_iarchive ia(input_file);
	ia  >> u6list;

	cout << "Starting calculating " << u6list.size() << " U6 symbols ... " << endl;
	
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	CalculateU6(u6list);
	std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;

	cout << "\tDone" << endl << "time:" << duration.count() << endl;
	return EXIT_SUCCESS;
}
