import sys

file1 = open(sys.argv[1], 'r')
lines = file1.readlines()

file2 = open(sys.argv[2], 'w')

index=0
for line in lines:
    if(len(line) > 21):
        if(line[0:21]=="Choose bandhead:Nmax:"):
            startinghw=line[21:22]
            startingline=index+1
    index=index+1

output=[]

currenthw=startinghw
for i in range(startingline,len(lines)-1):
    line = lines[i].strip()
    if(line[-2:] != 'hw'):
        lambda_mu=line.split('(')[3].split(')')[0]
        C2=line.split('(')[3].split(')')[1].strip().split()[0]
        Dim=line.split('(')[3].split(')')[1].strip().split()[1]
        C2=C2+".0"
        C2=float(C2.split('.')[0]+"."+C2.split('.')[1][0])
        Dim=int(Dim)
        if(Dim != 0):
            output.append(str(currenthw)+" "+lambda_mu+" "+str(C2))
    if(line[-2:] == 'hw'):
        currenthw=int(line[:-2])

output=list(dict.fromkeys(output))
for line in output:
    print(line, file=file2)
