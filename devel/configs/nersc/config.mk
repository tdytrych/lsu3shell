################################################################
# directory trees
################################################################
WIGSIXJPF_DIR=/project/projectdirs/m3321/wigxjpf-1.11
SU3LIB_DIR=/project/projectdirs/m3321/SU3lib
OMPILANCZ_DIR=/project/projectdirs/m3321/ompilancz

search_prefix := $(BOOST_DIR) $(GSL_DIR) $(SU3LIB_DIR)
search_dirs_include  := $(EIGEN3_DIR)/include/eigen3 $(WIGSIXJPF_DIR)/inc $(OMPILANCZ_DIR)/include
search_dirs_lib := $(WIGSIXJPF_DIR)/lib

install_prefix := $(HOME)/$(NERSC_HOST)/local/su3shell

################################################################
# machine-specific library configuration
################################################################
LDLIBS += -lgsl -lgslcblas
LDLIBS += -lboost_mpi -lboost_serialization -lboost_system -lboost_chrono
LDLIBS += -lSU3 -lwigxjpf -lquadmath

LDLIBS += -fopenmp

################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := CC
MPICXX := CC

CXXFLAGS += -O3 -std=c++14 -DNDEBUG -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp
#CXXFLAGS += -O3 -std=c++14 -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp
#CXXFLAGS +=  -g -std=c++14 
