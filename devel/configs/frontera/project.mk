################################################################
# project name
################################################################

project_name := su3shell

################################################################
# modules -- list of directories in which to search 
# for module.mk include files
################################################################

# libraries
# Caution: Order is important since used also in linking.
modules := libraries/su3dense libraries/LSU3 libraries/SU3ME libraries/LookUpContainers libraries/UNU3SU3 libraries/SU3NCSMUtils

#programs
modules += \
  programs/LSU3shell \
  programs/tools \
  programs/downstreams \
  programs/upstreams \
  programs/Densities \
#  programs/su3ncsm_textIO \
#  programs/su3ncsm_hdf5IO \
#  tests/LSU3 \
#  programs/tools \
#  programs/upstreams \
#  programs/su3ncsm_hdf5IO \
#  programs/downstreams \
################################################################
# extras -- list of extra files to be included
# in distribution tar file
################################################################

extras := su3shell-install.txt devel

################################################################
# additional project-specific make settings and rules
################################################################
