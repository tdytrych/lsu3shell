################################################################
# directory trees
################################################################
SHARED_DIR=/work2/02872/tdytrych/shared

WIGSIXJPF_DIR=$(SHARED_DIR)/wigxjpf-1.11
SU3LIB_DIR=$(SHARED_DIR)/SU3lib
OMPILANCZ_DIR=$(SHARED_DIR)/ompilancz


search_prefix := $(BOOST_ROOT) $(TACC_GSL_DIR) $(SU3LIB_DIR)
search_dirs_include := $(WIGSIXJPF_DIR)/inc $(OMPILANCZ_DIR)/include $(TACC_EIGEN_INC)
search_dirs_lib :=  $(WIGSIXJPF_DIR)/lib 
install_prefix := $(current-dir)

################################################################
# machine-specific library configuration
################################################################
#LDLIBS += -lgsl -llapack -lblas -lgslcblas -fopenmp
LDLIBS += -lgsl -lgslcblas -fopenmp
LDLIBS += -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lpthread -qopenmp
LDLIBS += -lboost_mpi -lboost_system -lboost_serialization -lboost_chrono 
LDLIBS += -lSU3 -lquadmath -lwigxjpf 
#LDLIBS += -lpetsc -lslepc
################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := mpicxx
MPICXX := mpicxx

CXXFLAGS += -std=c++14 -O3 -shared-intel -DNDEBUG -DHAVE_INLINE -qopenmp
#CXXFLAGS += -O3 -std=c++14 -DNDEBUG -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp
#CXXFLAGS += -O3 -std=c++14 -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp
#CXXFLAGS +=  -g -std=c++14 -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp  -fsanitize=address
#CXXFLAGS +=  -g -std=c++14 -ffast-math -funroll-loops -fopenmp
