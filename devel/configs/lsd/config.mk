################################################################
# directory trees
################################################################
#WIGXJPFDIR=/home/dytrych/work/wigxjpf-1.11
#SU3LIB_ROOT=/home/dytrych/work/su3lib
OMPILANCZ_DIR=/home/dytrych/work/ompilancz

search_prefix := $(SLEPC_DIR) $(PETSC_DIR)
search_prefix += $(SU3LIB_DIR)
search_dirs_include  := /home/dytrych/local/include $(WIGXJPF_DIR)/inc $(OMPILANCZ_DIR)/include
search_dirs_lib :=  /home/dytrych/local/boost_1_73_0/stage/lib $(WIGXJPF_DIR)/lib

install_prefix := $(current-dir)

################################################################
# machine-specific library configuration
################################################################
LDLIBS += -lgsl -llapack -lblas -lgslcblas -fopenmp
LDLIBS += -lboost_mpi -lboost_system -lboost_serialization -lboost_chrono 
LDLIBS += -lSU3 -lquadmath
LDLIBS += -lwigxjpf 
LDLIBS += -lpetsc -lslepc
################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := mpic++
MPICXX := mpic++ 

CXXFLAGS += -O3 -std=c++14 -DNDEBUG -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp
#CXXFLAGS += -O3 -std=c++14 -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp
#CXXFLAGS +=  -g -std=c++14 -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp  -fsanitize=address
#CXXFLAGS +=  -g -std=c++14 -ffast-math -funroll-loops -fopenmp

################################################################
# FORTRAN compiler-specific configuration
################################################################

FC := mpif77
FFLAGS += -O3 -ffast-math -funroll-loops -fopenmp
#FFLAGS += -g -fopenmp
fortran_libs := -lmpi_usempif08 -lmpi_usempi_ignore_tkr -lmpi_mpifh -lgfortran
