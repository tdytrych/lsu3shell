\section{Implementation}
Data structures representing proton (\ref{pconfs}), neutron (\ref{nconfs}) and
proton-neutron (\ref{pnbasisI} \& \ref{pnbasisJ}) tables are members 
of class \verb+ncsmSU3xSU2Basis+. This class also provides an interface that
facilitates accessing quantum numbers and iterating over basis states.
\subsection{Proton and Neutron Tables}
\label{pnconfs-impl}
Proton and neutron tables are implemented as vectors of tuples: 
\begin{Verbatim}[frame=single, fontsize=\small]
typedef cpp0x::tuple<uint16_t, uint16_t, uint32_t, uint32_t>  NUCLEON_BASIS_INDICES

std::vector<NUCLEON_BASIS_INDICES> pconfs_
std::vector<NUCLEON_BASIS_INDICES> nconfs_
\end{Verbatim}
Elements of tuple \verb+NUCLEON_BASIS_INDICES+ points to the auxiliary tables
of \verb+ncsmSU3xSU2Basis+ which hold all the quantum numbers required for
unambiguous definition of a given proton or neutron irreps.  Note that
\pconfs$[\symip]$ (\nconfs$[\symin]$) doesn't represent just a single irrep,
but rather, in general, multiple irreps sharing the same quantum numbers yet
corresponding to different multiplicities that occur in their construction.  The number
of irreps is given as \pconfs$[\symip].\alpha_{p,\max}$
(\nconfs$[\symin].\alpha_{n, \max}$).

\renewcommand{\arraystretch}{1.2}
\begin{table}
\begin{center}
\begin{tabular}{r|l}
\emph{interface} & \emph{implementation} \\ \hline
\pconfs$[\symip].\symalpm$ & \verb+ncsmSU3xSU2Basis::getMult_p(+$\symip$\verb+)+ \\
\pconfs$[\symip].\symlap$ & \verb+ncsmSU3xSU2Basis::getLambda_p(+$\symip$\verb+)+ \\
\pconfs$[\symip].\symmup$ & \verb+ncsmSU3xSU2Basis::getMu_p(+$\symip$\verb+)+ \\
\pconfs$[\symip].\symSp$ &  \verb+ncsmSU3xSU2Basis::getSS_p(+$\symip$\verb+)+ \\
\pconfs$[\symip].(\symomp\symSp)$ &  \verb+ncsmSU3xSU2Basis::getProtonSU3xSU2(+$\symip$\verb+)+ 
\end{tabular}
\caption{Interace of proton table \texttt{pconfs}.}
\label{tab:pconf_interface}
\end{center}
\end{table}


\renewcommand{\arraystretch}{1.2}
\begin{table}
\begin{center}
\begin{tabular}{r|l}
\emph{interface} & \emph{implementation} \\ \hline
\nconfs$[\symin].\symalnm$ & \verb+ncsmSU3xSU2Basis::getMult_n(+$\symin$\verb+)+ \\
\nconfs$[\symin].\symlan$ & \verb+ncsmSU3xSU2Basis::getLambda_n(+$\symin$\verb+)+ \\
\nconfs$[\symin].\symmun$ & \verb+ncsmSU3xSU2Basis::getMu_n(+$\symin$\verb+)+ \\
\nconfs$[\symin].\symSn$ &  \verb+ncsmSU3xSU2Basis::getSS_n(+$\symin$\verb+)+ \\
\nconfs$[\symin].(\symomn\symSn)$ &  \verb+ncsmSU3xSU2Basis::getNeutronSU3xSU2(+$\symin$\verb+)+ 
\end{tabular}
\caption{Interace of neutron table \texttt{nconfs}.}
\label{tab:nconf_interface}
\end{center}
\end{table}

\subsection{Proton-Neutron Table}
\subsection{Data Structures}
\label{pnbasis_structure}
Proton-neutron table is implemented by three vectors and a custom data structure: 
\begin{Verbatim}[frame=single, fontsize=\small, samepage=true]
std::vector<uint32_t> pnbasis_ipin_
std::vector<uint16_t> pnbasis_nirreps_
std::vector<uint16_t> wpn_

SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container_;
\end{Verbatim}
Elements of vector \verb+pnbasis_ipin_+ are $(i_{p},i_{n})$ indices that allow a user
to access the corresponding data in the proton and neutron tables, \verb+pconf[+$i_{p}$\verb+]+
and \verb+nconf[+$i_{n}$\verb+]+, via the interface of \verb+ncsmSU3xSU2Basis+ 
described in Tables~\ref{tab:pconf_interface} and~\ref{tab:nconf_interface}. 
A $k$th pair $(i_{p},i_{n})$ is stored in \verb+pnbasis_ipin_+ as:
\begin{align*}
\text{\texttt{pnbasis\_ipin\_[}}2k\text{\texttt{]}}&\rightarrow i_{p} \\
\text{\texttt{pnbasis\_ipin\_[}}2k+1\text{\texttt{]}}&\rightarrow i_{n}.
\end{align*}
Each pair of proton and neutron irreps, represented by $(i_{p},i_{n})$ indices, 
gives rise to an array of proton-neutron irreps, which we denote as $\left(\omega, S\right)^{+}$,
\begin{displaymath}
(i_{p},i_{n})\rightarrow \left(\omega, S\right)^{+}.
\end{displaymath}
For a $k$th pair $\left(i_{p},i_{n}\right)$, the number of elements 
in $\left(\omega, S\right)^{+}$ array is stored in 
\verb+pnbasis_nirreps_[+$k$\verb+]+. 
The auxiliary data structure \verb+wpn_irreps_container_+ (implementation this custom 
datatype can be found in \verb+libraries/SU3ME/SU3xSU2basis.h+) behaves as an array of $(\omega,S)$
irreps and their basis states $(\kappa, L)^{+}$:
\begin{displaymath}
\text{\texttt{wpn\_irreps\_container\_}}: \left(\left(\omega, S\right)\rightarrow \left(\kappa,L\right)^{+}\right)^{+}.
\end{displaymath}
It should be noted that almost all proton-neutron irreps $(\omega, S)$ occur
multiple times as they can be created by different pairs of $(i_{p},i_{n})$
indices. In order to avoid storing copies of identical data, we use the 
vector \verb+wpn_+ to store indices pointing to elements of 
\verb+wpn_irreps_container_+.
This means that, e.g., $i$th element of \verb+wpn_+ contains an index, e.g., $j$, which represents
an irrep $(\omega,S)$ along with its basis states: 
\begin{align*}
\text{\texttt{wpn\_[}}i\text{\texttt{]}} & \rightarrow j \\
\text{\texttt{wpn\_irreps\_container\_[}}j\text{\texttt{]}} & \rightarrow
\left(\omega, S\right)\rightarrow \left(\kappa,L\right)^{+}.
\end{align*}
\subsection{Notes}
We do not store a position $pos$ (row/column index) of a first state created by
$(i_{p}, i_{n})$ irreps as this can be easily calculated on-the-fly during
iteration over the basis states. Also note that each pair $(i_{p}, i_{n})$ of proton
and neutron irreps creates $\rho_{\max}$ instances of a proton-neutron irrep
$(\omega,S)$, where $\rho_{\max}$ is obtained as: 
\begin{displaymath}
\rho_{\max}=\text{\texttt{SU3::mult(}}\pconfs[\symip].\symomp,\,
\nconfs[\symin].\symomn,\, \omega\text{\texttt{)}}.  
\end{displaymath}

\clearpage
\subsection{Iteration Over Basis States}
Implementation of iteration over $(i_{p},i_{n})$ and  associated \((\omega,
S)\sp{+}\) using interface of ncsmSU3xSU2Basis. 

\begin{alltt}
{\color{blue}// Parameters for the construction of ncsmSU3xSU2Basis container} 
{\color{blue}//     ncsmModelSpace: structure with model space selection rules}
{\color{blue}//     ndiag: number of segments that basis is split into}
{\color{blue}//     idiag: current segment of the basis}
lsu3::CncsmSU3xSU2Basis  basis(ncsmModelSpace, idiag, ndiag);

uint16\_t ap\_max;       {\color{blue}// \(\alpha\sb{p,\max}\)} 
uint16\_t an\_max;       {\color{blue}// \(\alpha\sb{n,\max}\)}
uint32\_t ip, in;       {\color{blue}// \(i\sb{p},i\sb{n}\)}
uint16\_t wS\_dim;       {\color{blue}// |pnbasis[\(i\sb{p},i\sb{n}\)][\(\omega S\)]|}
uint32\_t ipos;         {\color{blue}// pnbasis[\(i\sb{p},i\sb{n}\)][\(\omega S\)].pos}

{\color{blue}// initialize ipos by the position of a first state of idiag section of the basis}
ipos = basis.getFirstStateId();

const uint32\_t* pnbasis\_ipin = basis.ipin\_begin(); {\color{blue}// &ncsmSU3xSU2Basis::pnbasis\_ipin\_[0]}
const uint16\_t* number\_wpn = basis.number\_wpn\_begin(); {\color{blue}// &ncsmSU3xSU2Basis::pnbasis\_nirreps\_[0]}
const uint16\_t* wpn = basis.wpn\_begin();              {\color{blue}// &ncsmSU3xSU2Basis::wpn\_[0]}

uint32\_t inumber\_wpn(0);      {\color{blue}// current position in number\_wpn array}
uint32\_t iwpn(0);             {\color{blue}// current position in wpn array}
uint32\_t i(0);                {\color{blue}// current position in pnbasis\_ipin array}
{\color{blue}// loop over all unique \((i\sb{p},i\sb{n})\) pairs in basis}
for (i = 0; i < basis.ipin\_size(); i += 2, inumber\_wpn++)
\verb+{+
    ip = pnbasis\_ipin[i];
    in = pnbasis\_ipin[i+1];
    ap\_max = basis.getMult\_p(ip);
    an\_max = basis.getMult\_n(in);
    {\color{blue}// number of irreps in array \((\omega S)\sp{+}\) associated with \((i\sb{p},i\sb{n})\) pair of irreps}
    uint16\_t nwpn = number\_wpn[inumber\_wpn]; 
    {\color{blue}// loop over \((\omega S)\sp{+}\)}	
    for (int j = 0; j < nwpn; ++j, ++iwpn) 
    \verb+{+
        {\color{blue}// omega\_pn: \(\rho\sb{\max},\omega, S\)}	
        SU3xSU2::LABELS omega\_pn = basis.getOmega\_pn(ip, in, wpn[iwpn]);
        {\color{blue}// |pnbasis[\(i\sb{p},i\sb{n}\)][\(\omega S\)]|=\(\alpha\sb{p,\max}\times\alpha\sb{n,\max}\times\rho\sb{\max}\times \dim(\omega,S)\)}
        wS\_dim = ap\_max*an\_max*omega\_pn.rho*basis.omega\_pn\_dim(wpn[iwpn]);
        ipos += wS\_dim;
    \verb+}+
\verb+}+
\end{alltt}

\clearpage
