\documentclass[11pt,a4paper,oneside]{article}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}
\usepackage{xcolor}
\usepackage{mathtools}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\IR}[1]{\left(\lambda_{#1}\, \mu_{#1}\right)}
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}

\newcommand{\CG}[6]
	{
	{ C }_{ #1 #2 #3 #4 }^{ #5 #6 }	
	}

\newcommand{\WignerTHREEj}[6]
	{
	\left(
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right)
	}

\newcommand{\WignerSIXj}[6]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right\}
	}

\newcommand{\WignerNINEj}[9]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6 \\
   			#7 & #8 & #9
		\end{array}
	\right\}
	}

\newcommand{\half}[0]
	{
	  \frac{ 1 }{ 2 }	
	}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{{\texttt{su3denseP}}}
\author{Tom\'{a}\v{s} Dytrych}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
This code computes proton \SU{3} one-body density matrix elements (OBDMEs),
\begin{equation}
  \RedME
  {J_{f}}
  {\left[a^{\dagger (n_1\,0)}_{p\half} \times \tilde{a}^{(0\,n_2)}_{p\half}\right]^{(\lambda_0\,\mu_0)S_{0}}_{k_{0}L_{0}J_0}}
  {J_{i}}, \label{eq:OBDMEs}
\end{equation}
or two-body density matrix elements (TBDMEs),
\begin{equation}
  \RedME
  {J_{f}}
{\left[ \left\{ a^{\dagger (n_1\,0)}_{p\half} \times a^{\dagger (n_2\,0)}_{p\half}\right\}^{(\lambda_f\,\mu_f)}_{S_{f}} \times \left\{\tilde{a}^{(0\,n_3)}_{p\half} \times \tilde{a}^{(0\,n_4)}_{p\half}\right\}^{(\lambda_i\,\mu_i)}_{S_{i}}  \right]^{\rho_{0}(\lambda_0\,\mu_0)S_{0}}_{k_{0}L_{0}J_{0}}}
  {J_{i}}
%  \equiv \RedME {J_{f}} {\hat{T}_{p}} {J_{i}}
, \label{eq:TBDMEs}
\end{equation}
for a pair of \texttt{LSU3shell} eigenstates, $|J_{f}\rangle$ and $|J_{i}\rangle$, and for all $\rho_0, k_{0}, L_{0},J_{0}$ quantum numbers permited by $J_{i}\times J_{0}\rightarrow J_{f}$ coupling.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Input Parameters}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\texttt{su3denseP} has six input parameters,
\begin{displaymath}
\texttt{
 <bra model space> <bra wfn> <ket model space> <ket wfn> <particle rank> <tensor list file> {<$J_{0}$>}
}
\end{displaymath}
with the following meaning:
\begin{itemize}
\item 
\texttt{<bra model space> <bra wfn>}: specify bra wave function $\langle J_{f}|$. 
\item 
\texttt{<ket model space> <ket wfn>}: specify ket wave function $|J_{i}\rangle$.
\item 
\texttt{<particle rank>}: 1 ... one-body tensors; 2 ... two-body tensors.
\item
\texttt{<tensor list file>}: file with a list of quantum labels specifying target tensors. 
\begin{itemize}
\item
In case two-body (\texttt{2B}) tensors, each line must contain the following 13 quantum numbers:
\begin{equation}
n_1 \quad n_2 \quad n_3 \quad n_4 \quad \lambda_f \quad \mu_f \quad 2S_f \quad \lambda_i \quad \mu_i \quad 2S_i \quad \lambda_0 \quad \mu_0 \quad 2S_0,
\label{eq:2Blabels}
\end{equation}
Note: use \texttt{Generata\_adadtata\_SU3list} to create an input file with selected \texttt{2B} proton \SU{3} tensor labels.
\item
In case of one-body (\texttt{1B}) tensors each line consists of 5 quantum numbers:
\begin{equation}
n_1 \quad n_2 \quad \lambda_0 \quad \mu_0 \quad 2S_0.
\label{eq:1Blabels}
\end{equation}
Note: use \texttt{Generata\_adta\_SU3list} to create an input file with selected proton \texttt{1B} \SU{3} tensor labels.
\end{itemize}
Each tensor in \texttt{<tensor list file>} is associated with a unique file that contains a table of rmes computed for a complete set of $\Un{3}\times\SU{2}$ irreps of $Z$ identical nucleons in $N_{\max}$ model space. If such file is not found in the current directory, code prints an error message and continues with the next tensor.
\item 
\texttt{<}$J_{0}$\texttt{>}: optional argument specifying $J_{0}$ value of tensors whose OBDMEs will be computed. If not provided all $J_{0}$ such that $J_{i}\times J_{0} \rightarrow J_{f}$ will be computed.
\end{itemize}

\label{sec:InputParams}
\section{Parallelization}
The code is parallelized by OpenMP library. The system variable \texttt{OMP\_NUM\_THREADS} then defines the number of OpenMP threads.  It should not be difficult to parallelize this code also across multiple MPI processes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Output File}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The resulting OBDMEs and TBDMEs are stored in an output text files named 
\begin{itemize}
\item \texttt{<tensor list file>.su3\_pp\_obdmes} for \texttt{1B} tensor
\item \texttt{<tensor list file>.su3\_pp\_tbdmes} for \texttt{2B} tensor. 
\end{itemize}
For \texttt{2B} operators, each line in the output file has the following structure:
\begin{equation}
n_1 \,\, n_2 \,\, n_3 \,\, n_4 \quad \lambda_f \,\, \mu_f \,\, 2S_f \quad \lambda_i \,\, \mu_i \,\, 2S_i \quad \lambda_0 \,\, \mu_0 \,\, 2S_0 \quad k_{0} \,\, 2L_{0} \,\, 2J_{0} \quad \quad 
  \RedME
  {J_{f}}
{\hat{T}^{\vec{\rho}_{0}}_{p}}
  {J_{i}},
\end{equation}
where $\RedME {J_{f}}{\hat{T}^{\vec{\rho}_{0}}_{p}}{J_{i}}$ symbolically denotes $\rho_{0}^{\max}$ TBDMEs~(\ref{eq:TBDMEs}).
For \texttt{1B} operators, the output file is made up of lines with the following structure:
\begin{equation}
n_1 \,\, n_2 \quad \lambda_0 \,\, \mu_0 \,\, 2S_0 \quad k_{0} \,\, 2L_{0} \,\, 2J_{0} \quad \quad 
  \RedME
  {J_{f}}
  {\left[a^{\dagger (\eta_1\,0)}_{p\half} \times \tilde{a}^{(0\,\eta_2)}_{p\half}\right]^{(\lambda_0\,\mu_0)S_{0}}_{k_{0}L_{0}J_0}}
  {J_{i}}
\end{equation}
Note that the output file will contain an error message, if a table of rmes for a given tensor does not exist.
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Usage Example}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Suppose one wants to compute \SU{3} and \SU{2} proton TBDMEs for the positive parity $J_{0}=0$ tensors and the ground state of $^{12}$C obtained in $N_{\max}=4$ model space.
\subsection*{Preliminary Step: create tables of rmes for target \texttt{2B} \SU{3} tensors}
%The code \texttt{su3denseP} uses tables of \SU{3} rmes computed in the complete $N_{\max}$ model space of $Z$ identical nucleons, which in this case represent $Z=6$ protons. The code expects to have these tables provided within the current working directory in a form of binary files that contain 2B tensors
%\begin{displaymath}
%\TRME{i}
%{\left[ \left\{ a^{\dagger (n_1\,0)}_{\half} \times a^{\dagger (n_2\,0)}_{\half}\right\}^{(\lambda_f\,\mu_f)}_{S_{f}} \times \left\{\tilde{a}^{(0\,n_3)}_{\half} \times \tilde{a}^{(0\,n_4)}_{\half}\right\}^{(\lambda_i\,\mu_i)}_{S_{i}}  \right]^{\rho_{0}(\lambda_0\,\mu_0)S_{0}}}
%{j}_{\bar{\rho}},
%\end{displaymath}
%where $i,j$ denote \Un{3}$\times$\SU{2} irreps for a system of $Z$ identical nucleons spanning $0 \le N\le N_{\max}$ spaces. Here is the description of the process of their computation. This process needs to be done only once for a given number of identical nucleons and $N_{\max}$ cutoff. Resulting tables of rmes should be stored permanently on disk and used as an input for TBDMEs computations. \\
%
First, one needs to create a list of target \SU{3} tensors. To generate a list of all positive parity two-nucleon \SU{3} tensors that contain $J_{0}=0$ for $p$-shell nuclei and $N_{\max}=4$ model space, 
\medskip

\noindent \texttt{{\color{blue}Generate\_adadtata\_SU3list}} \\
\noindent \texttt{ enter valence shell HO number:1} \\
\noindent \texttt{ enter Nmax:4}\\
\noindent \texttt{ Parity (1 ... positive, -1 ... negative):1}\\
\noindent \texttt{ SU(3) selection (0 ... (lm0 + mu0) even, 1 ... (lm0 + mu0) odd, -1 ... all):-1}\\
\noindent \texttt{ Select tensors that have specific J0 value? Enter 2J0 or -1 to include all tensors:0}
\noindent \texttt{ Generate recoupled tensors for ComputeMultiShellRecoupledRME (1 ... yes):1}
\medskip

\noindent
This will create two files. 
\begin{enumerate}
\item \texttt{1vshell\_4Nmax\_1parity\_allSU3\_with\_J0\_0.su3\_tensors} \\
Contains a list of $a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}$ selected \SU{3} tensors. In this particular case, there are $6321$ tensors.
\item \texttt{1vshell\_4Nmax\_1parity\_allSU3\_with\_J0\_0.su3\_tensors.Recoupled} \\
A file with $a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}$ tensors transformed into HO recoupled form.
\end{enumerate}
\medskip

\noindent
Next, one computes rmes for $Z=6$ identical nucleons in $N_{\max}=4$ model space for a given set of target recoupled \texttt{2B} tensors 
\medskip

\noindent\texttt{{\color{blue}ComputeMultiShellRecoupledRMEs} 6 4 2 1vshell\_4Nmax\_1parity\_allSU3\_with\_J0\_0.su3\_tensors.Recoupled}
\medskip

\noindent
Finally, one needs to transform rmes of recoupled \texttt{2B} tensors into rmes of tensors given in  $a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}$ form.
\medskip

\noindent\texttt{{\color{blue}GenerateMultiShellRMEs\_adadtata} 6 4 1vshell\_4Nmax\_1parity\_allSU3\_with\_J0\_0.su3\_tensors}
\medskip

\noindent
This process needs to be done only once for $Z=6$ identical nucleons and $N_{\max}$ cutoff. Resulting tables of rmes should be stored permanently on disk for their later use in computation of OBDMEs and TBDMEs.
\subsection*{Step 1: computation of \SU{3} TBDMEs}
To compute TBDMEs for proton \SU{3} tensors specified in the previous step, one runs:
\medskip

\noindent
\texttt{{\color{blue}su3denseP} 12C\_Nmax04\_JJ0.model\_space eigenvector01.dat 12C\_Nmax04\_JJ0.model\_space eigenvector01.dat 2 1vshell\_4Nmax\_1parity\_allSU3\_with\_J0\_0.su3\_tensors}
\subsection*{Step 2: Transform TBDMEs for \SU{3} tensors into \SU{2} tensors}
First, create a list of target \SU{2} tensors. In our case this corresponds to $p$-shell nuclei, $N_{\max}=4$, positive parity and $J_{0}=0$.
To create such a list of tensors, one needs to run:
\medskip

\noindent
\texttt{{\color{blue}Generate\_adadtata\_SU2list} PPNN 1 4 1 0 > su2\_pp\_tensors}
\medskip

\noindent
Transform TBDMEs of two-protons \SU{3} tensors to \SU{2}.
\medskip

\noindent
\texttt{{\color{blue}TBDMEsSU3toSU2} 1vshell\_4Nmax\_1parity\_allSU3\_with\_J0\_0.su3\_tensors.su3\_pp\_tbdmes su2\_pp\_tensors}\\
\medskip

\noindent
Resulting computing times for a single process are listed in the following table: 
\begin{center}
\begin{tabular}{ |c|c| } 
 \hline
 \texttt{\color{blue}ComputeMultiShellRecoupledRMEs} & 9s \\ 
 \texttt{\color{blue}GenerateMultiShellRMEs\_adadtata} & {\color{red}108m} \\ 
 \hline
 \texttt{\color{blue}su3denseP} & 144s \\ 
 \texttt{\color{blue}TBDMEsSU3toSU2} & 2s \\ 
 \hline
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preparing Input Wave Functions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
It is important to note that \texttt{su3denseP} assumes that input wave functions have order of basis states given by \texttt{NDIAG=1} parameter. However, large-scale \texttt{LSU3shell} computations have \texttt{NDIAG}$>1$. Therefore, one needs to use code \texttt{wfn2ndiag} to generate \texttt{su3denseP} compatible wave functions from eigenstates of \texttt{LSU3shell}. \\
\noindent
\textbf{Example:} \\
\noindent
Suppose that $^{12}$C complete $N_{\max}=4$ space wave function ($J=4$) \texttt{eigenvector01.dat} was generated with \texttt{NDIAG=3}. To generate wave function ready for \texttt{su3denseP}, one needs to run: \\
\vspace{0.1cm}
\\
\noindent
\texttt{wfn2ndiag 12C\_Nmax04\_JJ0.model\_space eigenvector01.dat 3 eigenvector01\_ndiag1.dat 1}
\end{document}
