\documentclass{report}
%\usepackage{algorithmic}
\usepackage{amsmath,amssymb,graphicx,color,epsfig}
\textheight     240.0mm
\textwidth      200.0mm 
\topmargin        -20.0mm
\oddsidemargin    -20.0mm
\evensidemargin    -20.0mm
\parindent        0.0mm

\DeclareMathOperator{\Tr}{Tr}
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\vgamma}[0]{\boldsymbol{\gamma}}
\newcommand{\vomega}[0]{\boldsymbol{\omega}}

%
% group notation
%
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\On}[1]{\ensuremath{\mathrm{O}( #1 )}}
\newcommand{\Spn}[1]{\ensuremath{\mathrm{Sp}( #1 )}}
\newcommand{\SpR}[1]{\ensuremath{\mathrm{Sp}( #1,\mathbb{R} )}}

%
% algebra notation
%

\newcommand{\IR}[1]{\ensuremath{(\lambda_{#1}\,\mu_{#1})}}
\newcommand{\su}[1]{\ensuremath{\mathfrak{su}( #1 )}}
\newcommand{\un}[1]{\ensuremath{\mathfrak{u}( #1 )}}
\newcommand{\so}[1]{\ensuremath{\mathfrak{so}( #1 )}}
\newcommand{\on}[1]{\ensuremath{\mathfrak{o}( #1 )}}
\newcommand{\spn}[1]{\ensuremath{\mathfrak{sp}( #1 )}}
\newcommand{\spR}[1]{\ensuremath{\mathfrak{sp}( #1, \mathbb{R} )}}

%
% boson and fermion
%
\newcommand{\bdag}{\ensuremath{ {b^\dagger} }}
\newcommand{\adagg}{\ensuremath{ {a^\dagger} }} % extra space outside the dagger
\newcommand{\adag}{\ensuremath{ a^\dagger }}

%
% CG and Racah
%
\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\ket}[1]{\ensuremath{\left| #1 \right\rangle}}
\newcommand{\bra}[1]{\ensuremath{\left\langle #1 \right|}}
\newcommand{\braket}[2]{\ensuremath{\left\langle #1 | #2 \right\rangle}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}

%
% Latin abbr.
%
\newcommand{\ie}{\emph{i.e.}}
\newcommand{\eg}{\emph{e.g.}}
\newcommand{\etal}{\emph{et al.}}
\newcommand{\etc}{\emph{etc.}}

%
% Miscellany
%
\newcommand{\half}{\ensuremath{\textstyle{\frac{1}{2}}}}
\newcommand{\threehalves}{\ensuremath{\textstyle{\frac{3}{2}}}}

\newcommand{\one}{\ensuremath{\mathit{1}}}
\newcommand{\two}{\ensuremath{\mathit{2}}}

\newcommand{\betb}{\begin{tabular}{p{4.0cm}p{9.0cm}}}
\newcommand{\entb}{\end{tabular}}
\newcommand{\MPC}[2]{\begin{minipage}[t]{#1\textwidth}
  \begin{center}~#2\end{center}\end{minipage}}
\newcommand{\MPL}[2]{~\begin{minipage}[t]{#1\textwidth}#2\end{minipage}}

\newcommand{\matrixindex}[2]{\ensuremath{\left(\mathcal{#1} #2 \right)}}

\newcommand{\bsigma}{\boldsymbol{\sigma}}
\newcommand{\bn}{\boldsymbol{n}}
\newcommand{\bomega}{\boldsymbol{\omega}}
\newcommand{\ba}{\ensuremath{\boldsymbol{a}}}
\newcommand{\adagger}{\ensuremath{a^\dagger}}
\newcommand{\badagger}{\ensuremath{\boldsymbol{a}^\dagger}}

% normalization coefficient for SU(3) 2-fermion state
\newcommand{\N}[1]{\ensuremath{\mathcal{N}_{#1} }}

\newcommand{\LS}[1]{\left(#1\half\right)}
\newcommand{\irrep}[2]{\left(#1 #2\right)}

\newcommand{\can}[1]{\ensuremath{\epsilon_{#1}\Lambda_{#1}M_{\Lambda_{#1}}}}

%
% color scheme
%
\newcommand{\red}[1]{{ \color{red} #1 }}
\newcommand{\green}[1]{{ \color{green} #1 }}
\newcommand{\blue}[1]{{ \color{blue} #1 }}
\newcommand{\magenta}[1]{{ \color{magenta} #1 }}

\begin{document}
\chapter{Calculation of Expectation Values/Transitions of One-Body Operators}
A one-body spherical tensor can be written in \SU{3} tensorial form as:
\begin{equation}
\hat{O}^{J_{0}}_{M_{0}}=
\sum_{
	\begin{array}{c}
	t_{z}=\left\{\pi,\nu\right\} \\
n_{a}n_{b}\\
		\IR{0} \\
		\kappa_{0}L_{0}S_{0}
\end{array}
}
\alpha^{t_{z}}_{\{...\}}
\left[a^{\dagger(n_{a}\,0)}_{t_{z}\half}\times \tilde{a}^{(0\,n_{b})}_{t_{z}\half}\right]^{\IR{0}}_{\kappa_{0}L_{0}S_{0}J_{0}M_{0}}
\end{equation}
where $\{...\}$ is a shorthand notation for $n_{a}n_{b}\IR{0}\kappa_{0}L_{0}S_{0}J_{0}M_{0}$ labels. The coefficients in expansion,
$\alpha^{t_{z}}_{\{...\}}$, can be evaluated as
\begin{equation}
\alpha^{t_{z}}_{\{...\}}=\!\!\!\!\!\!\!\!\!\!\!\!\!\!\!\!\!\!
\sum_{
	\begin{array}{c}
		l_{a}l_{b} \\
		j_{a}j_{b} \\
		m_{a}-m_{b}=M_{0}
	\end{array}
	}
\!\!\!\!\!\!	
\!\!\!\!\!\!	
(-)^{n_{b}-j_{b}+m_{b}}
\RedCG{(n_{a}\,0)l_{a}}{(0\,n_{b})l_{b}}{\IR{0}\kappa_{0}L_{0}}
\Pi_{j_{a}j_{b}L_{0}S_{0}}
\Wigninej
{l_{a}}{l_{b}}{L_{0}}
{\half}{\half}{S_{0}}
{j_{a}}{j_{b}}{J_{0}}
C_{j_{a}m_{a}j_{b}-m_{b}}^{J_{0}M_{0}}
\ME{n_{a}l_{a}j_{a}m_{a}}{\hat{O}^{J_{0}}_{M_{0}}}{n_{b}l_{b}j_{b}m_{b}}_{t_{z}}
\label{eq:SU3OBdecomposition}
\end{equation}
Due to the algorithm of \texttt{SU3NCSM}, each pair of fermion
creation and annihilation operators must be ordered according to the harmonic
oscillator shell numbers $n_{a}, n_{b}$ in the increasing order. If
$n_{a} > n_{b}$, one has to switch the order of creation and annihilation
operators, which involves multiplying expansion coefficient by the phase
$(-)(-)^{n_{a} + n_{b} -\lambda_{0} - \mu_{0} + 1 - S}$.  This is done during
\SU{3} tensorial decomposition as implemented by 
\begin{displaymath}
\text{\texttt{COneBodyOperators2SU3Tensors<COneBodyMatrixElements>::PerformSU3Decomposition}}.
\end{displaymath}
\medskip

\noindent
Our aim is to calculate $\RedME{\nu_{F} J_{F}}{\hat{O}^{J_{0}}}{\nu_{I}
J_{I}}$, where $\ket{\nu J}$ is an eigenvector obtained by the \texttt{SU3NCSM}
code.  Each eigenstate is expanded in terms of \SU{3} basis states of
\texttt{SU3NCSM}, \begin{displaymath} \ket{\nu J}=\sum_{i=1}^{\dim}
c_{i}\ket{\omega_{i} J}, \end{displaymath} where $\omega$ includes all quantum
labels of a given \SU{3} basis state.  Note that $M$ is missing since Hamiltonian is a
scalar and hence the expansion does not depend on $M$. The calculation
ultimately reduces into vector-matrix-vector multiplication
\begin{displaymath}
\begin{pmatrix} c_{1}^{F} \dots c_{n}^{F} \end{pmatrix} \times
\begin{pmatrix} \\ \RedME{\omega^{F}J_{F}}
{\hat{O}^{J_{0}}}
{\omega^{I}J_{I}} \\ \\ \end{pmatrix} \times \begin{pmatrix} c_{1}^{I} \\ \vdots
\\ c_{n}^{I} \\ \end{pmatrix}.
\end{displaymath}

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{matrix_structure.pdf}
\end{center}
\caption[width=0.3\textwidth]
{
The block structure of the operator matrix $\RedME{\omega^{F}J_{F}}
{\hat{O}^{J_{0}}} {\omega^{I}J_{I}}$ is defined by the first two input parameters
of the code, $n^{\text{bra}}$ and $n^{\text{ket}}$,  which specify number of
segments into which bra and ket basis is split. Each process is assigned one
block.}
\label{fig:Structure}
\end{figure}

\section{Electric Quadrupole Moment and Transiton}
This code generates \SU{3} tensor decomposition of the one-body operator
\begin{equation}
\frac{Q_{2\mu}}{b_{0}^{2}}=\frac{1}{b_{0}^{2}}\sqrt{\frac{16\pi}{5}}\sum_{i=1}^{A}\tilde{e}_{i} r_{i}^{2}Y_{2\mu}(\theta_{i},\phi_{i})=\frac{1}{b_{0}^{2}}\sqrt{6}\sum_{i=1}^{A}\tilde{e}_{i}\left[{\bf r}_{i}\times {\bf r}_{i}\right]_{2\mu},
\end{equation}
where the position operator of the $i$th nucleon ${\bf r}_{i}$ is a spherical
tensor with $L=1$, and $b_{0}$ is the harmonic oscillator length given as
\begin{displaymath}
b_{0}=\sqrt{\frac{\hbar}{m\Omega}} = \sqrt{\frac{(197.327)^{2}\left[\text{MeV}^{2}\text{fm}^{2}\right]}{938.92\left[\text{MeV}\right]\times\hbar\Omega \left[\text{MeV}\right]}}.
\end{displaymath}
Also note that if $\tilde{e}_{i}=1$ for all nucleons,
we have
\begin{displaymath}
C^{(1\,1)}_{2\mu}=\frac{Q_{2\mu}}{\sqrt{3}}.
\end{displaymath}
The resulting file is then used as an input for \texttt{observables} family of codes that first calculate
\begin{displaymath}
\left[\frac{1}{b_{0}^{2}}\frac{\RedME{\nu_{f}J_{f}}{Q_{2}}{\nu_{i}J_{i}}}{\sqrt{2J_{f}+1}}\right].
\end{displaymath}
Using this result, one can obtain quadrupole moment and transition B(E2) values as
\begin{align}
Q(J)&=C_{JJ20}^{JJ} b_{0}^{2} \left[\frac{1}{b_{0}^{2}}\frac{\RedME{\nu J}{Q_{2}}{\nu J}}{\sqrt{2J+1}}\right] \quad \text{in units of} \left[\text{e}\,\text{fm}^{2}\right] 
\label{eq:QJ} \\
B(E2;J_{i}\rightarrow J_{f})&=\frac{1}{2J_{i}+1}\frac{5}{16\pi}b_{0}^{4}(2J_{f}+1)\left| \left[\frac{1}{b_{0}^{2}}\frac{\RedME{\nu_{f}J_{f}}{Q_{2}}{\nu_{i}J_{i}}}{\sqrt{2J_{f}+1}}\right] \right|^{2}
\quad \text{in units of} \left[\text{e}^{2}\,\text{fm}^{4}\right] \label{eq:BE2}
\end{align}
Note that in some cases MFDn results for BE2 values are provided without the factor of $1/(2J_{i}+1)$.

\section{Magnetic Dipole Moment and Transiton}
This code generates \SU{3} tensor decomposition of the one-body operator
\begin{equation}
M_{1\mu}=\sqrt{\frac{3}{4\pi}}\sum_{i=1}^{A}\left(g^{l}_{i}l_{i\mu} + g^{s}_{i} s_{i\mu}\right),
\end{equation}
where $g_{\pi}^{s}=5.5857\mu_{N}$, $g_{\nu}^{s}=-3.8263\mu_{N}$, $g^{l}_{\pi}=1\mu_{N}$, and $g^{l}_{\nu}=0$.
The resulting file is then used as an input for \texttt{observablesSU3} family of codes, which first calculates
\begin{displaymath}
\left[\frac{\RedME{\nu_{f}J_{f}}{M_{1}}{\nu_{i}J_{i}}}{\sqrt{2J_{f}+1}}\right],
\end{displaymath}
and utilizes this result in the formula for the magnetic dipole moment~\cite{Suhonen, Heyde} 
\begin{displaymath}
\mu=\ME{JJ}{\sum_{i=1}^{A}\left(g_{i}^{l}l_{i\mu} + g^{s}_{i}s_{i\mu}\right)}{JJ}
\end{displaymath}
to obtain magnetic dipole moment and transition B(M1) values as
\begin{align}
M1(J)&=\sqrt{\frac{4\pi}{3}}C_{JJ10}^{JJ} \left[\frac{\RedME{\nu J}{M_{1}}{\nu J}}{\sqrt{2J+1}}\right] \quad \left[\mu_{N}\right] \label{eq:M1J}\\
B(M1;J_{i}\rightarrow J_{f})&=\left(\frac{1}{2J_{i}+1}\right)(2J_{f}+1)\left| \left[\frac{\RedME{\nu_{f}J_{f}}{M_{1}}{\nu_{i}J_{i}}}{\sqrt{2J_{f}+1}}\right] \right|^{2}
\quad  \left[\mu_{N}^{2}\right],
\label{eq:BM1}
\end{align}
Note that MFDn sometimes seems to be using formula for B(M1) which does not include $1/(2J_{i}+1)$ factor.

\section{\texttt{transitions\_BE2\_BM1\_mascot\_MPI}}
To calculate BE2 and BM1 transition strenghts in using formula~(\ref{eq:BE2}) and formula~(\ref{eq:BM1}) one has to run the following utility: \\
\begin{center}
\texttt{transitions\_BE2\_BM1\_mascot\_MPI <filename>}, \\
\end{center}
where \texttt{<filename>} is the name of an input text file whose structure is
described in Table~\ref{tab:transSU3Input}.  User should check that the number
of processors reserved to carry out the calculation is equal to
$\left(\texttt{ndiag}_{\texttt{bra}}\cdot
\texttt{ndiag}_{\texttt{ket}}\right)$.  Values of the latter two parameters are
provided in the input file.  Code exits immediately if there is a mismatch in
the number of processors.
\begin{table}
\begin{center}
\begin{tabular}{ccc}
\hline \hline
\multicolumn{3}{l}{\texttt{<transition type>}} \\
\multicolumn{3}{l}{either BE2 or BM1} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections utilized for computing wave functions (bra)} \\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections utilized for computing wave functions (ket)} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections that will be used in the current obd calculation (bra)} \\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections that will be used in the current obd calculation (ket)} \\
\\
\multicolumn{3}{l}{\texttt{<filename>}} \\
\multicolumn{3}{l}{the name of a file with model space definition (bra)} \\
\multicolumn{3}{l}{\texttt{<filename>}} \\
\multicolumn{3}{l}{the name of a file with model space definition (ket)} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of bra-ket eigenstate pairs} \\
\multicolumn{3}{l}{\texttt{<filename (bra)>} \texttt{<filename (ket)>}} \\
\multicolumn{3}{l}{\hspace{0.9cm}$\vdots\hspace{3cm}\vdots$} \\
\multicolumn{3}{l}{\texttt{<filename (bra)>} \texttt{<filename (ket)>}}  \\
\\
\multicolumn{3}{l}{\texttt{<}$\hbar\Omega$\texttt{> if transition\_type=BE2}} \\
\multicolumn{3}{l}{harmonic oscillator energy in MeV; this parameter is not required for BM1} \\
\hline \hline
\end{tabular}
\caption{Structure of \texttt{transitions\_BE2\_BM1\_mascot\_MPI} input file.}
\label{tab:transSU3Input}
\end{center}
\end{table}

\section{\texttt{rmsE2M1observables\_mascot\_MPI}}
To calculate the electric quadrupole moment~(\ref{eq:QJ}), the magnetic dipole moment~(\ref{eq:M1J}), or the root-means-square matter radii in femtometers, one has 
to run the following utility: \\
\begin{center}
\texttt{rmsE2M1observables\_mascot\_MPI <filename>}, \\
\end{center}
where \texttt{<filename>} is the name of an input text file whose structure is described in Table~\ref{tab:rmsE2M1SU3Input}.
User should check that the number of processors reserved to carry out the calculation is equal to
$\texttt{ndiag(ndiag+1)/2}$. The number of sections of many-particle basis, $\texttt{ndiag}$, is provided in the input file.  
Code exits immediately if there is a mismatch in the number of processors.

\begin{table}
\begin{center}
\begin{tabular}{ccc}
\hline \hline
\multicolumn{3}{l}{\texttt{<operator type>}} \\
\multicolumn{3}{l}{one of the following: E2 M1 RMS} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections utilized for computing input wave functions} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections that will be used in the current obd calculation} \\
\\
\multicolumn{3}{l}{\texttt{<filename>}} \\
\multicolumn{3}{l}{the name of a file with model space definition} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of input wave functions} \\
\multicolumn{3}{l}{\texttt{<filename>}}  \\
\multicolumn{3}{l}{\hspace{0.9cm}$\vdots$} \\
\multicolumn{3}{l}{\texttt{<filename>}}  \\
\\
\multicolumn{3}{l}{\texttt{<}$\hbar\Omega$\texttt{> if transition\_type=(E2 or RMS)}} \\
\multicolumn{3}{l}{harmonic oscillator energy in MeV; this parameter is not required for M1} \\
\hline \hline
\end{tabular}
\caption{Structure of \texttt{rmsE2M1observables\_mascot\_MPI} input file.}
\label{tab:rmsE2M1SU3Input}
\end{center}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Calculation of One-Body Densities} 
Some operators of interest are functions, and hence their expansion
coefficients $\alpha^{t_{z}}_{\{...\}}$ are not constant.  For instance, electron
scattering form factors are functions of momentum transfer $q$, the nuclear
density operator is a function of coordinates $\bf{r}$, etc...  Therefore, 
it is convenient to calculate a set of \SU{3} one-body density matrix elements (obds),
$\RedME{\nu_{F} J_{F}}
{\left[a^{\dagger(n_{a}\,0)}_{t_{z}\half}\times \tilde{a}^{(0\,n_{b})}_{t_{z}\half}\right]^{\IR{0}S_{0}}_{\kappa_{0}L_{0}J_{0}}}
{\nu_{I} J_{I}}$, 
in the first step, and then employ
\begin{equation}
\RedME{\nu_{F}J_{F}}{\hat{O}^{J_{0}}}{\nu_{I}J_{I}}=
\!\!\!\!\!
\sum_{
	\begin{array}{c}
	t_{z}=\left\{\pi,\nu\right\} \\
n_{a}n_{b}\\
		\IR{0} \\
		\kappa_{0}L_{0}S_{0}
\end{array}
}
\!\!\!\!\!
\alpha^{t_{z}}_{\{...\}}
\RedME{\nu_{F} J_{F}}
{\left[a^{\dagger(n_{a}\,0)}_{t_{z}\half}\times \tilde{a}^{(0\,n_{b})}_{t_{z}\half}\right]^{\IR{0}S_{0}}_{\kappa_{0}L_{0}J_{0}}}
{\nu_{I} J_{I}}
\label{eq:OBDME}
\end{equation}
to get a final result.  This approach also allows for computing \SU{2} obds compatible with 
\texttt{trdens/MFDn} family of codes by means of the formula
(\ref{eq:su2tosu3}).

\section{\texttt{ObdSU3Calculator\_MPI}}
To calculate a set of SU(3) obds
\begin{equation}
	\RedME{\alpha_{f}J_{f}}{\left[a^{\dagger(n_{a}\,0)}_{t_{z}\half}\times \tilde{a}^{(0\,n_{b})}_{t_{z}\half}\right]^{\IR{0}}_{\kappa_{0}L_{0}S_{0}J_{0}}}{\alpha_{i}J_{i}}
	\label{rel:SU3obd}
\end{equation}
one has to run the following utility: \\
\begin{center}
\texttt{ObdSU3Calculator\_MPI <filename>}, \\
\end{center}
where \texttt{<filename>} is the name of an input text file whose structure is described in Table~\ref{tab:ObdSU3Input}
\begin{table}
\begin{center}
\begin{tabular}{ccc}
\hline \hline
\multicolumn{3}{l}{\texttt{<filename>}} \\
\multicolumn{3}{l}{the name of a file with a list of tensor labels that define \SU{3} obds} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections utilized for computing wave functions (bra)} \\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections utilized for computing wave functions (ket)} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections that will be used in the current obd calculation (bra)} \\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of basis sections that will be used in the current obd calculation (ket)} \\
\\
\multicolumn{3}{l}{\texttt{<filename>}} \\
\multicolumn{3}{l}{the name of a file with model space definition (bra)} \\
\multicolumn{3}{l}{\texttt{<filename>}} \\
\multicolumn{3}{l}{the name of a file with model space definition (ket)} \\
\\
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of bra-ket eigenstate pairs} \\
\multicolumn{3}{l}{\texttt{<filename (bra)>} \texttt{<filename (ket)>}} \\
\multicolumn{3}{l}{\hspace{0.9cm}$\vdots\hspace{3cm}\vdots$} \\
\multicolumn{3}{l}{\texttt{<filename (bra)>} \texttt{<filename (ket)>}} \\
\hline \hline
\end{tabular}
\caption{Structure of \texttt{ObdSU3Calculator} input file.}
\label{tab:ObdSU3Input}
\end{center}
\end{table}
\begin{table}
\begin{center}
\begin{tabular}{ccc}
\hline \hline
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of $\SU{3}$ obds} \\ 
\\
\multicolumn{3}{l}{\texttt{set<vector<int>, char>}} \\
\multicolumn{3}{l}{$n_{a}\quad n_{b}\quad\,\lambda_{0}\quad\,\mu_{0}\quad\,2S_{0}\quad\,\kappa_{0}\quad\,2L_{0}\quad\,2J_{0}\quad$\, \texttt{type}$\leftarrow$('p' or 'n')} \\
\multicolumn{3}{l}{$\vdots$} \\
\multicolumn{3}{l}{$n'_{a}\quad n'_{b}\quad\,\lambda'_{0}\quad\,\mu'_{0}\quad\,2S'_{0}\quad\,\kappa'_{0}\quad\,2L'_{0}\quad\,2J'_{0}\quad$\, \texttt{type}$\leftarrow$('p' or 'n')} \\
\hline \hline
\end{tabular}
\end{center}
\caption{Structure of a file with a list of tensor labels that define \SU{3} obds.}
\label{tab:listSU3labelsObd}
\end{table}
The code calculates all the \SU{3} obds that occur in a list provided in an
external file (see Table~\ref{tab:listSU3labelsObd}), for each pair of bra and ket wave functions. 
Resulting obds are stored for each pair of eigenfunctions in a file \texttt{obdme\_<int>.out},
where \texttt{<int>}$=0,1,\dots \texttt{npairs-1}$.
The structure of the output files is based on the one detailed in 
Table~\ref{tab:listSU3labelsObd} with the additional $\texttt{<double>}$
value (resulting obd) at the end of each row with \SU{3} quantum labels.
The output files can be used as an input for \texttt{TransformObdSU3toSU2} program.

Note that one of the parameters that must be provided by the user is the number of
sections in bra and ket basis utilized for the calculation of the wave functions.
This information can be found in $\texttt{ndiag}$ files
produced automatically by the \texttt{su3shell} code and located in a resulting
directory along with eigensolutions.

\texttt{ObdSU3Calculator\_MPI} is a parallel code that uses MPI library. The
total number of processes for a given production run is determined by the
product of the two parameters specified in its input file, the number of basis
sections for bra and ket. 

\subsection{Implementation Notes}
In case when $n_{a}>n_{b}$, \texttt{ObdSU3Calculator\_MPI} uses relation 
\begin{equation}
\left[a^{\dagger(n_{a}\,0)}_{t_{z}\half}\times \tilde{a}^{(0\,n_{b})}_{t_{z}\half}\right]^{\IR{0}}_{\kappa_{0}L_{0}S_{0}J_{0}M_{0}}=-(-)^{n_{a}+n_{b}-\lambda_{0}-\mu_{0}+1-S_{0}}
\left[\tilde{a}^{(0\,n_{b})}_{t_{z}\half}\times a^{\dagger(n_{a}\,0)}_{t_{z}\half}\right]^{\IR{0}}_{\kappa_{0}L_{0}S_{0}J_{0}M_{0}}
\end{equation}
to calculate \SU{3} odb~(\ref{rel:SU3obd}). 

\section{\texttt{TransformObdSU3toSU2}}
\begin{table}
\begin{center}
\begin{tabular}{ccc}
\hline\hline
\multicolumn{3}{l}{\texttt{<int>}} \\
\multicolumn{3}{l}{number of $\SU{2}$ obd labels} \\ 
\\
\multicolumn{3}{l}{\texttt{set<vector<int>, char>}} \\
\multicolumn{3}{l}{$n_{a}\quad 2l_{a}\quad\,2j_{a}\quad\,n_{b}\quad\,2l_{b}\quad\,2j_{b}\quad\,2J_{0}\quad$\, \texttt{type}$\leftarrow$('p' or 'n')} \\
\multicolumn{3}{l}{$\vdots$} \\
\multicolumn{3}{l}{$n'_{a}\quad 2l'_{a}\quad\,2j'_{a}\quad\,n'_{b}\quad\,2l'_{b}\quad\,2j'_{b}\quad\,2J'_{0}\quad$\, \texttt{type}$\leftarrow$('p' or 'n')} \\
\hline\hline
\end{tabular}
\caption{Structure of a file with a list of tensor labels that define \SU{2} obds.}
\label{tab:listSU2Obd}
\end{center}
\end{table}
In order to transform a set of \SU{3} obds for a single bra-ket pair into a set of \SU{2} obds, one can use \\
\begin{center} 
\texttt{TransformObdSU3toSU2 <filename:SU(2) obd labels> <filename:SU{3} obds>}. \\
\end{center} 
The structure of the first input file is described in
Table~\ref{tab:listSU2Obd}.  It is assumed that the second file contains all
the \SU{3} obds that are required for the calculation of a given set of \SU{2}
obds.  The code utilizes relation 
\begin{multline}
\RedME{J_{f}}{\left[a^{\dagger}_{n_{1}l_{1}j_{1}}\times \tilde{a}_{n_{2}l_{2}j_{2}}\right]^{J_{0}}}{J_{i}}
=
(\blue{\mp})(-)^{n_{2}}\!\!\!\!\!\!
\sum_{
	\begin{array}{c}
	(\lambda_{0}\,\mu_{0})S_{0} \\
	\kappa_{0}L_{0}
	\end{array}
}\!\!\!\!\!\!\!
\Pi_{j_{1}j_{2}L_{0}S_{0}}\RedCG{(n_{1}\,0)l_{1}}{(0\,n_{2})l_{2}}{(\lambda_{0}\,\mu_{0})\kappa_{0}L_{0}}
\Wigninej{l_{1}}{l_{2}}{L_{0}}{\half}{\half}{S_{0}}{j_{1}}{j_{2}}{J_{0}} \\
\RedME{J_{f}}
{\left[a^{\dagger(n_{1}\,0)}_{\half}\times\tilde{a}^{(0\,n_{2})}_{\half}\right]^{(\lambda_{0}\,\mu_{0})S_{0}}_{\kappa_{0}L_{0}J_{0}}}
{J_{i}}. 
\label{eq:su2tosu3}
\end{multline}
This utility caltulates and then prints \SU{2} obds on a standard output. 
\subsection{Phases}
\label{subsectionPhases}
\subsubsection{Positive At Origin}
The creation and annihilation operators exploited by the \texttt{su3shell} package
create (annihilate) a nucleon in the
hamonic oscillator (HO) single-particle wave function with the radial part
positive at infinity.  Note that \texttt{MFDn/trdens} suite of codes works with the radial
part positive at origin.  The \SU{2} obds thus have to be multiplied by a
factor 
\begin{displaymath}
(-)^{\frac{1}{2}(n_{a}-l_{a}+n_{b}-l_{b})} 
\end{displaymath} 
before they could be provided as an input to \texttt{MFDn} suite.  This
transformation is carried out by \texttt{TransformObdSU3toSU2} if the
preprocessor directive \texttt{POSITIVE\_AT\_ORIGIN} is set. Note that 
this directive is implicitly disabled in the source code.

\subsubsection{Annihilation Operator as SU(2) Tensor}
We are using the following relation that expresses annihilation operator as \SU{3} tensor:
\begin{equation}
\left(a^{\dagger}_{nljm_{j}}\right)^{\dagger}=a_{nljm_{j}}=(-1)^{n-j+m_{j}}\tilde{a}^{(0\,n)}_{lj-m_{j}}.
\label{eq:2SU3TCouplJ}
\end{equation}
On the other hand James Vary and others utilize the following convention that associates annihilation operator with \SU{2} tensor:
\begin{equation}
a_{nljm_{j}}=(-)^{j+m_{j}}\tilde{a}_{nlj-m_{j}}.
\label{eq:SU2Tensor}
\end{equation}
Provided that annihilation operators on the left hand side of (\ref{eq:2SU3TCouplJ}) and (\ref{eq:SU2Tensor}) are 
equal, we have
\begin{displaymath}
(-)^{j+m_{j}}\tilde{a}_{nlj-m_{j}}=(-1)^{n-j+m_{j}}\tilde{a}^{(0\,n)}_{lj-m_{j}},
\end{displaymath}
and hence
\begin{equation}
\tilde{a}_{nlj-m_{j}}=(-1)^{n+2m_{j}}\tilde{a}^{(0\,n)}_{lj-m_{j}}=(-)(-1)^{n}\tilde{a}^{(0\,n)}_{lj-m_{j}},
\label{eq:relation}
\end{equation}
since $(-)^{2m_{j}}=(-)$.
\medskip

\noindent
If one tries to derive formula provided by Anna Hayes in
Phys.  Rev. C 81 (2010) 054301, which reads
\begin{equation}
\RedME{J_{f}}{\left[a^{\dagger(n_{1}\,0)}_{\half}\times \tilde{a}^{(0\,n_{2})}_{\half}\right]^{(\lambda_{0}\,\mu_{0})S_{0}}_{\kappa_{0}L_{0}J_{0}}}{J_{i}}
=\sum_{l_{1}l_{2}}(-)^{n_{2}}\RedCG{(n_{1}\,0)l_{1}}{(0\,n_{2})l_{2}}{(\lambda_{0}\,\mu_{0})\kappa_{0}L_{0}}\sum_{j_{1}\,j_{2}}
\Unininej{l_{1}}{\half}{j_{1}}{l_{2}}{\half}{j_{2}}{L_{0}}{S_{0}}{J_{0}}
\RedME{J_{f}}{\left[a^{\dagger}_{n_{1}l_{1}j_{1}}\times \tilde{a}_{n_{2}l_{2}j_{2}}\right]^{J_{0}}}{J_{i}},
\label{eq:Hayes}
\end{equation}
one obtains the additional overall phase $-1$, provided formula (\ref{eq:relation}) is used.
The relation~(\ref{eq:Hayes}) is recovered if one uses 
\begin{displaymath}
\tilde{a}_{nlj-m_{j}}=(+)(-1)^{n}\tilde{a}^{(0\,n)}_{lj-m_{j}},
\end{displaymath}
and the relation between annihilation operator and corresponding \SU{2} tensor becomes
\begin{equation}
a_{nljm_{j}}=(-1)^{j-m_{j}}\tilde{a}_{nlj-m_{j}}.
\label{eq:SU2Hayes}
\end{equation}
\texttt{TransformObdSU3toSU2} uses two preprocessor directives,
\texttt{SU3SHELL\_NATIVE} and \texttt{MFDN\_NATIVE}, to set the overall
phase of~(\ref{eq:su2tosu3}) to be equal to $(+)$ and $(-)$ for
\texttt{SU3SHELL\_NATIVE} (set implicitly) and \texttt{MFDN\_NATIVE} respectively.
The former directive produces obds in terms of \SU{2} tensors that fulfill 
relation~(\ref{eq:SU2Hayes}), while the latter directive complies with relation~(\ref{eq:SU2Tensor}).

\section{\texttt{Generate\_obdlists\_nmax\_JJ0}}
To generate files with a list of \SU{3} and \SU{2} obd quantum labels (detailed
in Table~\ref{tab:listSU3labelsObd} and Table~\ref{tab:listSU2Obd}) for maximal
major HO shell $n_{\max}$, and total angular momentum $J_{0}$ one has to run
the following utility: \\
\begin{center}
\texttt{\texttt{Generate\_obdlists\_nmax\_JJ0} <$n_{\max}$>   <$2J_{0}$>    <parity conserving>   <output files prefix>}, \\
\end{center}
where the parameter $\texttt{<parity conserving>}$ takes value \texttt{0} or
\texttt{1}.  In the former case, the program will generate obd labes for all
the possible combinations of major HO shell numbers $n_{a}\le n_{\max}$ and
$n_{b}\le n_{\max}$, while only parity conserving obd quantum numbers (i.e.
$n_{a}+n_{b}$=even number) are considered for the latter choice.  The two
output files have \texttt{<output files prefix>.su2\_list} and \texttt{<output
files prefix>.su3\_list} naming scheme and and they can be used as an input to
\texttt{TransformObdSU3toSU2} and \texttt{ObdSU3Calculator} codes.

\chapter{Alternative Approach to Calculating BE2 and BM1 Transitions}
Alternatively, one can employ \texttt{OneBodyOperator2ObdList} and
\texttt{transitions\_BE2\_BM1\_obds} to obtain BE2 and BM1 transition rates.
\begin{enumerate}
\item  \texttt{EMObservables2SU3Tensors <1 or 2> <$n_{\max}$>} \\ 
Generate \SU{3} tensorial decomposition of $Q_{L=2 \mu=0}$ in case of
\texttt{<1>} or $M_{L=1 \mu=0}$ for \texttt{<2>} operator and store it in
\texttt{Q2full\_1b\_nmax8\_mu0} file.
\item	Edit \texttt{Q2full\_1b\_nmax8\_mu0} file and add \texttt{4 0} as the first line for \texttt{BE2}.
\item \texttt{OneBodyOperator2ObdList <Q2full\_1b\_nmax8\_mu0> <Q2\_obd.su3\_list>}
\item Create input file for \texttt{ObdSU3Calculator\_MPI}, e.g. \texttt{BE2.input}, according to Table~\ref{tab:ObdSU3Input}. 
\item \texttt{ObdSU3Calculator\_MPI <BE2.input>}
\item For all files generated in the previous step, i.e. \texttt{obdme\_?}, edit the first line so that it contains the number of obds. Note that 
this number can be seen on the first line of \texttt{<Q2\_obd.su3\_list>}.
\item \texttt{transitions\_BE2\_BM1\_obds BE2 Q2full\_1b\_nmax8\_mu0 obdme\_?.out}
\end{enumerate}

\chapter{Alternative Approach to Calculating Nucleon Rms Radii}
\begin{enumerate}
\item \texttt{L2\_LS\_B+B\_AB00\_2SU3Tensors}  \\
Generate \SU{3} tensorial decomposition of $\sum_{i}^{A} r_{i}^{2}$ operator. Resulting decomposition can be found in \texttt{r2me\_1b\_Nmax<?>} file.
\item	Edit \texttt{r2me\_1b\_Nmax<?>} file and add \texttt{0 0} as the first line.
\item \texttt{OneBodyOperator2ObdList r2me\_1b\_Nmax<?>} $\eta_{\max}$ \texttt{1 > r2\_obds.su3\_list} \\
Create a file with a list of quantum labels associated with one-body \SU{3} tensors occurring in the \SU{3} tensorial decomposition of $\sum_{i}^{A} r_{i}^{2}$ operator.
Note that the last argument is set to $1$ in order to exclude all \SU{3} one-body terms that are a mere conjugates of some other terms in the decomposition. The densities of conjugate terms can 
be obtained from the non-conjugate ones using the following relation:
as 
\begin{displaymath}
\langle \psi_{J} || \left[a^{\dagger (\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half}\right]^{(\lambda_0\,\mu_0)S_{0}}_{\kappa_0 L_0 J_0}||\psi_{J}\rangle
= (-)^{S_0 + J_0}
\langle \psi_{J} || \left[a^{\dagger (\eta_{2}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{1})}_{\half}\right]^{(\mu_0\,\lambda_0)S_{0}}_{\kappa_0 L_0 J_0}||\psi_{J}\rangle
\end{displaymath}
\item Create input file for \texttt{ObdSU3Calculator\_MPI\_OpenMP}, e.g. \texttt{r2.input}, according to Table~\ref{tab:ObdSU3Input}. 
\item \texttt{ObdSU3Calculator\_MPI\_OpenMP <r2.input>}
\item For all files generated in the previous step, i.e. \texttt{r2.input\_obdme\_<?>.out}, edit the first line so that it contains the number of obds. Note that 
this number can be seen on the first line of \texttt{<r2\_obds.su3\_list>}.
\item \texttt{rmsE2M1observables\_obds r2.input\_obdme\_<?>.out r2me\_1b\_Nmax<?>}
\end{enumerate}



\appendix
\chapter{Appendix}
\section{Operator Input File Structure}
\subsubsection{One-Body}
Tensorial decomposition of one-body operator is stored in a text file.  The
first entry is $2J_{0}$ and $2M_{0}$, i.e twice the value of the total angular momentum and its projection, followed
by the data structures describing particular components of the \SU{3} tensorial decomposition.
This is illustrated in Table \ref{tab:nanbOBDMEapproachI}.
\begin{table}[h]
\begin{center}
\begin{tabular}{ccc}
\hline\hline
\multicolumn{3}{l}{\texttt{char[2]}} \\
\multicolumn{3}{l}{$(n_{a}+1)\,\,\,-(n_{b}+1)$} \\ 
\\
\multicolumn{3}{l}{\texttt{unsigned char}} \\
\multicolumn{3}{l}{\#tensors} \\
\\
\texttt{SU3xSU2::LABELS} &  \texttt{SU2::LABEL} & \texttt{SO3::LABEL} \\
\IR{0} & 2$S_{0}$ & 2$L_{0}$ \\
\\
\multicolumn{3}{l}{\texttt{vector<double>}} \\
\multicolumn{3}{l} { $\alpha^{\pi}_{\kappa_{0}=0}, \alpha^{\nu}_{\kappa_{0}=0}, \dots, \alpha^{\pi}_{\kappa_{0}^{\max}}, \alpha^{\nu}_{\kappa_{0}^{\max}}$}\\
\hline\hline
\end{tabular}
\caption{Structure of the input file ($n_{a} < n_{b}$)}
\label{tab:nanbOBDMEapproachI}
\end{center}
\end{table}
The order in which the tensor strengths are stored in the input file is obsolete
and I plan to change it in the future. It is computationally much more convenient to
store first all proton coefficients $\alpha^{\pi}_{\kappa_{0}=0}, \dots, \alpha^{\pi}_{\kappa_{0}^{\max}}$ followed 
by the neutron coefficients $\alpha^{\nu}_{\kappa_{0}=0},\dots, \alpha^{\nu}_{\kappa_{0}^{\max}}$. In fact, 
this is the ordering compatible with \texttt{SU3NCSM} code. Right now after reading interactions/operators input files, we need to call the method 
\texttt{CInteractionPPNN::TransformTensorStrengthsIntoPP\_NN\_structure} to perform the transformation: 
\begin{displaymath}
\alpha^{\pi}_{\kappa_{0}=0}, \alpha^{\nu}_{\kappa_{0}=0}, \dots, \alpha^{\pi}_{\kappa_{0}^{\max}}, \alpha^{\nu}_{\kappa_{0}^{\max}} \longrightarrow 
\alpha^{\pi}_{\kappa_{0}=0}, \dots, \alpha^{\pi}_{\kappa_{0}^{\max}}, \alpha^{\nu}_{\kappa_{0}=0},\dots, \alpha^{\nu}_{\kappa_{0}^{\max}}.
\end{displaymath}

\subsubsection{Two-Body}
I need to derive and implement a general formula for decomposition of any
operator into \SU{3} tensorial form.  Formulas and codes I am using right now
are valid only for scalar, i.e. $J_{0}=0$, operators. Recoupling code is going to
work since it is independent of $\SU{2}/\SO{3}$ labels, but it may require some tweaking.
Overall, this is not going to be difficult, but rather tedious and time consuming.
\medskip

\noindent
The first record in the input file will be $2J_{0}$ and $2M_{0}$. The rest is
going to be identical to spherical \SU{3} tensor with addition of $2L_{0}$,
since we no longer have $S_{0}=L_{0}$.

\section{Formula Sheet}
\begin{multline}
\RedME{J_{f}}{\left[a^{\dagger(n_{1}\,0)}_{\half}\times \tilde{a}^{(0\,n_{2})}_{\half}\right]^{(\lambda_{0}\,\mu_{0})S_{0}}_{\kappa_{0}L_{0}J_{0}}}{J_{i}}
=
\blue{(\mp)}(-)^{n_{2}}
\Pi_{L_{0}S_{0}}\sum_{l_{1}l_{2}}\RedCG{(n_{1}\,0)l_{1}}{(0\,n_{2})l_{2}}{(\lambda_{0}\,\mu_{0})\kappa_{0}L_{0}}
\sum_{j_{1}\,j_{2}}
\Pi_{j_{1}j_{2}}
\Wigninej{l_{1}}{l_{2}}{L_{0}}{\half}{\half}{S_{0}}{j_{1}}{j_{2}}{J_{0}} \\
\times
\RedME{J_{f}}
{\left[a^{\dagger}_{n_{1}l_{1}j_{1}}\times \tilde{a}_{n_{2}l_{2}j_{2}}\right]^{J_{0}}}
{J_{i}}
\label{eq:su3tosu2}
\end{multline}


\begin{thebibliography}{1}
\bibitem{AHPRC} A. C. Hayes, A. A. Kwiatkowski, Phys. Rev. C 81 ($2010$), 054301.
\bibitem{Suhonen} Juoni Suhonen, From Nucleons to Nucleus, ($2007$) 
\bibitem{Heyde} Kris Heyde, The Nuclear Shell Model, ($1994$) 
\end{thebibliography}
\end{document}
