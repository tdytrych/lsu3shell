\documentclass[11pt,a4paper,oneside]{article}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}
\usepackage{amssymb}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}
\renewcommand*{\marginfont}{\color{red}}

\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}

\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}
\newcommand{\half}[0]{\frac{1}{2}}

\definecolor{lightgray}{gray}{0.97}
\definecolor{forestgreen}{RGB}{0,128,0}
\definecolor{navyblue}{RGB}{0,0,128}
\definecolor{brick}{RGB}{128,0,0}
\definecolor{prepro}{RGB}{128,0,128}

\lstset{language=C++,
  basicstyle=\ttfamily,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  mathescape=true,
  morecomment=[l]{!\ }% Comment only with space after !
}

\title{{\texttt{su3densePN\_adadtata\_pnnp}}}
\author{Tom\'{a}\v{s} Dytrych}
\begin{document}
\maketitle
This code computes proton-neutron \SU{3} two-body density matrices (TBDMEs)
\begin{equation}
  \RedME
  {J_{f}}
{\left[ 
\left\{ 
a^{\dagger(n^{\pi}_{1}\,0)}_{\pi\half} \times a^{\dagger(n^{\nu}_{2}\,0)}_{\nu\half}
\right\}^{(\lambda_f\,\mu_f)}_{S_{f}} 
\times 
\left\{
\tilde{a}^{(0\,n^{\nu}_3)}_{\nu\half} \times \tilde{a}^{(0\,n^{\pi}_{4})}_{\pi\half}
\right\}^{(\lambda_i\,\mu_i)}_{S_{i}}  
\right]^{\rho_{0}(\lambda_0\,\mu_0)S_{0}}_{k_{0}L_{0}J_{0}}}
  {J_{i}} \label{eq:TBDMEs}
\end{equation}
for a pair of  \texttt{LSU3shell} eigenstates $|J_{f}\rangle$ and $|J_{i}\rangle$.
\section{Input Parameters}
\texttt{su3densePN\_adadtata\_pnnp} has five input parameters,
\begin{displaymath}
\texttt{
<bra model space> <bra wfn> <ket model space> <ket wfn> <tensor list file>
}
\end{displaymath}

\noindent
{\bf Input Parameters:}
\begin{itemize}
\item 
\texttt{<bra model space> <bra wfn>}: specify bra wave function $\langle J_{f}|$. 
\item 
\texttt{<ket model space> <ket wfn>}: specify ket wave function $|J_{i}\rangle$.
\item
\texttt{<tensor list file>}: file with a list of quantum labels specifying target tensors. Each line must contain the following 13 quantum numbers:
\begin{equation}
n^{\pi}_1 \quad n^{\nu}_2 \quad n^{\nu}_3 \quad n^{\pi}_4 \quad \lambda_f \quad \mu_f \quad 2S_f \quad \lambda_i \quad \mu_i \quad 2S_i \quad \lambda_0 \quad \mu_0 \quad 2S_0 \quad \kappa_{0} \quad 2L_{0} \quad 2J_{0} \label{eq:inputline}
\end{equation}
Note that code computes TBDMEs~(\ref{eq:TBDMEs}) for all tensor multiplicities $\rho_{0}$.
\end{itemize}
\section{Output File}
The resulting TBDMEs are stored in an output text file named 
\begin{itemize}
\item
\texttt{<tensor list file>.su3\_pn\_tbdmes}, 
\end{itemize}
which contains quantum numbers of proton-neutron \SU{3}$\times$\SU{2} tensor operators and associated TBDMEs:
\begin{equation}
n^{\pi}_1 \,\, n^{\nu}_2 \,\, n^{\nu}_3 \,\, n^{\pi}_4 \quad \lambda_f \,\, \mu_f \,\, 2S_f \quad \lambda_i \,\, \mu_i \,\, 2S_i \quad  \lambda_0 \,\, \mu_0 \,\, 2S_0 \quad \kappa_{0} \,\, 2L_{0} \,\, 2J_{0} \quad \RedME{J_{F}}{\hat{T}^{\vec{\rho}_{0}}}{J_{i}},
\end{equation}
where $\hat{T}^{\vec{\rho}_{0}}$ symbolically denotes $\rho_{0}^{\max}$ TBMEs~(\ref{eq:TBDMEs}).
%\section{Preparing tensor labels: \texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0}}
%Obviously, the number of proton-neutron two-body tensors in relation~(\ref{eq:TBDMEs}) can be quite large. One can use code \texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0} to generate a list of quantum numbers of selected target tensors. Please see its documentation for more information on its use.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Usage Example}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Suppose one wants to compute \SU{3} and \SU{2} proton-neutron TBDMEs for the ground state of $^{12}$C obtained in $N_{\max}=4$ model space.
\subsection*{Preliminary Step: prepare tables with all possible \SU{3} rmes for a system of $Z$ and $N$ identical nucleons}
One first needs to create a file containing list of recoupled $a^{\dagger}\tilde{a}$ \SU{3} tensors that is compatible with \texttt{ComputeMultiShellRecoupledRMEs}. As computation of rmes is very efficient, and needs to be done once-in-a-lifetime for a given number of nucleons and $N_{\max}$, it is advisable to simply generate all one-body \SU{3} tensors. To do that, one needs to run:
\medskip

\noindent
\texttt{{\color{blue}Generate\_adta\_SU3list}} \\
\noindent \texttt{ enter valence shell HO number:1} \\
\noindent \texttt{ enter Nmax:4} \\
\noindent \texttt{ Parity (1 ... positive, -1 ... negative, 0 ... both parities):0} \\
\noindent \texttt{ Select tensors that have specific J0 value. Enter 2J0 or -1 to include all tensors:-1} \\
\noindent \texttt{ Generate recoupled one-body tensors for ComputeMultiShellRecoupledRME (1 ... yes):1} \\
\medskip

\noindent
\texttt{Generate\_adta\_SU3list} will generate two files
\begin{itemize}
\item \texttt{1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors} \\
\noindent
List of $n_{a}\,n_{b}\,\lambda_{0}\,\mu_0\,2S_{0}$ quantum numbers of all possible \SU{3} one-body tensors $\left[a^{\dagger (n_{a}\,0)}_{\half}\times \tilde{a}^{(0\,n_{b})}_{\half}\right]^{(\lambda_{0}\,\mu_{0})}_{S_{0}}$ that are relevant for $p$-shell nuclei and $N_{\max}=4$ model space.
\item \texttt{1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors.Recoupled} \\ 
\noindent Contains list of target tensors expressed in HO recoupled form.
\end{itemize}
\medskip

\noindent
In the next step, one-body rmes for $Z=N=6$ identical nucleons are computed. \\
\noindent \texttt{{\color{blue}ComputeMultiShellRecoupledRMEs} 6 4 1 } \texttt{1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors.Recoupled}

\subsection*{Step 1: create file with quantum labels of target proton-neutron \SU{3} and \SU{2} tensors}
\begin{enumerate}
\item Create a list of target \SU{3} tensors. \\ 
To generate a list of all possible positive parity proton-neutron $J_{0}=0$ \SU{3} tensors (as in our case $J_{f}=J_{i}=0$),
\medskip

\noindent
\texttt{{\color{blue}Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0} 1 4 1 -1 0 > su3\_pn\_tensors}
\item Create a list of target \SU{2} tensors. \\
To generate a list of all possible positive parity proton-neutron $J_{0}=0$ \SU{2} tensors 
\medskip

\noindent
\texttt{{\color{blue}Generate\_adadtata\_SU2list} PN 1 4 1 0 > su2\_pn\_tensors}
\end{enumerate}
\subsection*{Step 2: compute TBDMEs}
\begin{enumerate}
\item Compute proton-neutron \SU{3} TBDMEs
\medskip

\noindent
\texttt{{\color{blue}su3densePN\_adadtata\_pnnp}} \texttt{12C\_Nmax04\_JJ0.model\_space} \texttt{eigenvector01.dat} \texttt{12C\_Nmax04\_JJ0.model\_space} \texttt{eigenvector01.dat} \texttt{su3\_pn\_tensors}
\item Transform \SU{3} TBDMEs into \SU{2} TBDMEs 
\medskip

\noindent
\texttt{{\color{blue}TBDMEsSU3toSU2} su3\_pn\_tensors.su3\_pn\_tbdmes su2\_pn\_tensors}\\
\end{enumerate}
Resulting computing times are given in fhe following table:
\begin{center}
\begin{tabular}{ |c|c| } 
 \hline
 \texttt{\color{blue}ComputeMultiShellRecoupledRMEs} & 0.16s \\ 
 \hline
 \texttt{\color{blue}su3densePN\_adadtata\_pnnp} & {\color{red}22.5 hours!} \\ 
 \texttt{\color{blue}TBDMEsSU3toSU2} & 15 s \\ 
 \hline
\end{tabular}
\end{center}
One can see that a given implementation of \texttt{su3densePN\_adadtata\_pnnp} has a poor performance. Replacing the current algorithm with a more efficient one is the next objective.

\section{Preparing Input Wave Functions}
It is important to note that \texttt{su3densePN\_adadtata\_pnnp} assumes that input wave functions have order of basis states given by \texttt{NDIAG=1} parameter. However, large-scale \texttt{LSU3shell} computations have \texttt{NDIAG}$>1$. Therefore, one needs to use code \texttt{wfn2ndiag} to generate \texttt{su3denseP} compatible wave functions from eigenstates of \texttt{LSU3shell}. \\
\noindent
\textbf{Example:} \\
\noindent
Suppose that $^{12}$C complete $N_{\max}=4$ space wave function ($J=4$) \texttt{eigenvector01.dat} was generated with \texttt{NDIAG=3}. To generate wave function ready for \texttt{su3denseP}, one needs to run: \\
\vspace{0.1cm}
\\
\noindent
\texttt{wfn2ndiag 12C\_Nmax04\_JJ0.model\_space eigenvector01.dat 3 eigenvector01\_ndiag1.dat 1}
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% I M P L E M E N T A T I O N %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implementation Notes}
\subsection{Organization of input tensors quantum numbers}
\begin{center}
{\large
\begin{lstlisting}
void ReadListOfTensors
\end{lstlisting}
}
\end{center}
Read quantum numbers~(\ref{eq:inputline}) associated with proton-neutron tensors~(\ref{eq:TBDMEs}) and create a datastructure that holds input tensors in memory.
\medskip

\noindent
{\bf Input/Output:}
\begin{eqnarray*}
\text{\texttt{filename}} &\rightarrow& \text{ specifies file with tensor quantum numbers.} \\
\text{\texttt{tensors}} &\leftarrow& \text{ output data structure with tensor quantum numbers of densities.}
\end{eqnarray*}
The datatype of \texttt{tensors} is following:
\begin{lstlisting}
typedef std::map<N, map<WFWIW0, std::vector<K0LL0JJ0>>> INPUT_TENSORS
\end{lstlisting}
The definition of datatypes \texttt{N} and \texttt{WFWIW0} are following:
\begin{eqnarray*}
\text{\texttt{N}}&\rightarrow &\left\{n_{1}^{\pi}, n_{2}^{\nu}, n_{3}^{\nu}, n_{4}^{\pi}\right\} \\
\text{\texttt{WFWIW0}}&\rightarrow& (\lambda_{f}\,\mu_{f})2S_{f}\equiv\text{\texttt{wf}} \quad (\lambda_{i}\,\mu_{i})2S_{i}\equiv\text{\texttt{wi}} \quad \rho_{0}^{\max}(\lambda_{0}\,\mu_{0})2S_{0}\equiv\text{\texttt{w0}}
\end{eqnarray*}
Notice tensor multiplicity $\rho_{0}^{\max}$ in \texttt{WFWIW0}. Its value depends on $\omega_{f}$ and $\omega_{i}$.

\subsection{Recoupling}
Generally, tensors~(\ref{eq:TBDMEs}) are specified in a way that is not suitable for a direct computation of TBDMEs. We need to make sure tensors are expressed in an {\texttt{LSU3shell} `compatible' form. In the case of proton-neutron operator it means that proton and neutron parts are separated, and, furthermore, the order of $a^{\dagger}$ and $\tilde{a}$ is such that harmonic oscillator (HO) shell quantum numbers are given in an increasing order. This can be symbolically written as:
\begin{equation}
\left[ 
\left\{ 
a^{\dagger(n^{\pi}_{1}\,0)}_{\pi\half} \times a^{\dagger(n^{\nu}_{2}\,0)}_{\nu\half}
\right\}^{\omega_{f}}
\!\!
\!\!
\times 
\left\{
\tilde{a}^{(0\,n^{\nu}_3)}_{\nu\half} \times \tilde{a}^{(0\,n^{\pi}_{4})}_{\pi\half}
\right\}^{\omega_{i}}
\right]^{\rho_{0}\omega_{0}}
\!\!\!\!=  
\sum_{\omega_{\pi}\omega_{\nu}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{\omega_{\pi}\omega_{\nu}}
\left[\left(a^{\dagger}_{\pi} \tilde{a}_{\pi}\right)^{\omega_{\pi}}\!\!\!\! \times \left(a^{\dagger}_{\nu} \tilde{a}_{\nu}\right)^{\omega_{\nu}}\right]^{\rho_0'\omega_{0}},\label{eq:Recoupling}
\end{equation}
where $\left(a^{\dagger}\tilde{a}\right)$ signifies that operators are recoupled according to HO shells.
{\large
\begin{lstlisting}
void RecoupleTensor
\end{lstlisting}
}
\medskip

\noindent
{\bf Input/Output:}
\begin{eqnarray*}
\text{\texttt{n1, n2, n3, n4}} &\rightarrow& n_{1}^{\pi},\,n_{2}^{\nu},\, n_{3}^{\nu},\, n_{4}^{\pi} \\
\text{\texttt{wf, wi, w0}} &\rightarrow& \omega_{f},\, \omega_{i},\, \rho_{0}^{\max}\omega_{0} \\
\text{\texttt{tensor\_expansion}} &\leftarrow& \text{data representing right hand side of relation~(\ref{eq:Recoupling})}
\end{eqnarray*}
The datatype of \texttt{tensor\_expansion} is following:
\begin{lstlisting}
typedef std::map<TENSOR_LABELS,  std::vector<double>> 
\end{lstlisting}
The definition and meaning of datatypes \texttt{TENSOR\_LABELS} and \texttt{std::vector<double>} are following:
\begin{eqnarray*}
\text{\texttt{TENSOR\_LABELS}}=\left\{\text{\texttt{IR1}}:\omega_{\pi} \text{\texttt{, IR2}}:\omega_{\nu}\text{\texttt{, IR0}}: \rho_{0}^{'\max}\omega_{0}\right\} &\rightarrow & \left[\left(a^{\dagger}_{\pi} \tilde{a}_{\pi}\right)^{\omega_{\pi}}\!\!\!\! \times \left(a^{\dagger}_{\nu} \tilde{a}_{\nu}\right)^{\omega_{\nu}}\right]^{\rho_0'\omega_{0}} \\
\text{\texttt{std::vector<double>}}&\rightarrow& d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}
\end{eqnarray*}
It is important to note that all tensors with the same \texttt{N}, but different \texttt{WFWIW0}, can be expressed using the same set of recoupled tensors $\left\{\omega_{\pi}\omega_{\nu}\right\}$.
\newpage
\subsection{Organization of resulting TBDMEs}
\begin{displaymath}
\text{\texttt{using RESULTING\_TBDMES = map}}\left[\left\{\omega_{f},\omega_{i},\omega_{0}\right\}\right]\rightarrow 
\left\{
\begin{array}{c}
 \text{\texttt{vector}}\langle\left\{k_{0},L_{0},J_{0}\right\}\rangle\\
 \text{\texttt{vector}}
 \langle
 \vec{T}_{k_{0}L_{0}J_{0}}
 \rangle\\
\end{array}\right\}
\end{displaymath}
where
\begin{displaymath}
 \vec{T}_{k_{0}L_{0}J_{0}}\equiv
  \RedME
  {J_{f}}
{\left[ 
\left\{ 
a^{\dagger(n^{\pi}_{1}\,0)}_{\pi\half} \times a^{\dagger(n^{\nu}_{1}\,0)}_{\nu\half}
\right\}^{\omega_{f}}
\times 
\left\{
\tilde{a}^{(0\,n^{\nu}_2)}_{\nu\half} \times \tilde{a}^{(0\,n^{\pi}_{2})}_{\pi\half}
\right\}^{\omega_{i}}  
\right]^{\vec{\rho_{0}}\omega_{0}}_{k_{0}L_{0}J_{0}}}
  {J_{i}}
\end{displaymath}
and it is represented as \texttt{vector<double>} with $\rho_{0}^{\max}$ elements. 
\subsection{Organization of recoupled TBDMEs}
Algorithm is structure in such a way that one needs only TBDMEs for ``LSU3shell compatible'' tensors only for a fixed set $\omega_{\pi}\omega_{\nu}\omega_{0}$, 
\begin{displaymath}
\vec{R}_{k_{0}L_{0}J_{0}}\equiv \RedME{J_{f}}{\left[\left(a^{\dagger}_{\pi} \tilde{a}_{\pi}\right)^{\omega_{\pi}}\!\!\!\! \times \left(a^{\dagger}_{\nu} \tilde{a}_{\nu}\right)^{\omega_{\nu}}\right]^{\vec{\rho_0}'\omega_{0}}_{k_{0}L_{0}J_{0}}}{J_{i}}.
\end{displaymath}
and it is represented as \texttt{vector<double>} with $\rho_{0}'^{\max}$ elements. The data structure for recoupled TBDMEs storage is implemented as
\begin{displaymath}
\text{\texttt{using RECOUPLED\_TBDMES = map}}\left[\left\{k_{0},L_{0},J_{0}\right\}\right]\rightarrow 
 \text{\texttt{vector}} \langle \vec{R}_{k_{0}L_{0}J_{0}} \rangle
\end{displaymath}
\newpage
\subsection{Algorithm}
{
\begin{lstlisting}
iterate over $\vec{n}\in$ INPUT_TENSORS
{
  iterate over $\left\{\omega_{f},\omega_{i},\omega_{0}\right\}\in \vec{n}$
  {
     insert $\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$, vector<$\left\{k_{0},L_{0},J_{0}\right\}$>, vector<$\vec{T}_{k_{0}L_{0}J_{0}}=\vec{0}$> $\rightarrow\text{\texttt{RESULTING\_TBDMES}}$
     RecoupleTensor($\vec{n}\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$) $\rightarrow$ map<$\left\{\omega_{\pi},\omega_{\nu},\omega_{0}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}}$> 
     copy std::map<$\left\{\omega_{\pi},\omega_{\nu},\omega_{0}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}}$>, $\left\{\omega_{f},\omega_{i},\omega_{0}\right\}\rightarrow$ map<{$\omega_{\pi},\omega_{\nu}$},vector$\left\{\left\{\omega_{f},\omega_{i},\omega_{0}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}\right\}$>
  } 

  iterate over $\left\{\omega_{\pi},\omega_{\nu}\right\}\in$ map<{$\omega_{\pi},\omega_{\nu}$},vector$\left\{\left\{\omega_{f},\omega_{i},\omega_{0}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}\right\}$>
  {
     sort(vector$\left\{\left\{\omega_{f},\omega_{i},\omega_{0}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}\right\}$) according to $\omega_{0}$
  }

  iterate over $\left\{\omega_{\pi},\omega_{\nu}\right\}\in$ map<{$\omega_{\pi},\omega_{\nu}$},vector$\left\{\left\{\omega_{f},\omega_{i},\omega_{0}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}\right\}$>
  {
     $\omega_{\pi} \rightarrow$ proton rme table given in CSR format$\rightarrow$ IJ_RMES
     $\omega_{\nu} \rightarrow$ neutron rme table given in CSR format $\rightarrow$ IJ_RMES
     iterate over $\left\{\left\{\omega_{f},\omega_{i},\omega_{0}={\color{blue}\text{const}}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}\right\}$ blocks
     {
        $\forall \left\{k_{0},L_{0},J_{0}\right\}\in\vec{n}\left\{\omega_{f},\omega_{i},\omega_{0}={\color{blue}\text{const}}\right\}$ compute $\text{\texttt{map}}\left[\left\{k_{0},L_{0},J_{0}\right\}\right]\rightarrow \text{\texttt{vector}} \langle \vec{R}_{k_{0}L_{0}J_{0}} \rangle$
        iterate over $\left\{\omega_{f},\omega_{i},\omega_{0}\right\}\in\left\{\left\{\omega_{f},\omega_{i},\omega_{0}={\color{blue}\text{const}}\right\}, d^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}\rho_{0}'}\right\}$ block
        {
           vector<{$k_{0},L_{0},J_{0}$}> $\leftarrow$ RESULTING_TBDMES[$\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$].first
           vector<$\vec{T}_{k_{0}L_{0}J_{0}}$> $\leftarrow$ RESULTING_TBDMES[$\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$].second
           iterate over $\left\{k_{0},L_{0},J_{0}\right\}\in$ vector<{$k_{0},L_{0},J_{0}$}>
           {
             $\vec{R}_{k_{0}L_{0}J_{0}}\leftarrow\text{\texttt{map}}\left[\left\{k_{0},L_{0},J_{0}\right\}\right]\rightarrow \text{\texttt{vector}} \langle \vec{R}_{k_{0}L_{0}J_{0}} \rangle$
             iterate over $\rho_{0}$
             {
               $T^{\rho_{0}}_{k_{0}L_{0}J_{0}}+= \vec{d}^{\omega_{\pi}\omega_{\nu}}_{\rho_{0}}\cdot \vec{R}_{k_{0}L_{0}J_{0}}$
             }
           }
        }
     }
  }
  store RESULTING_TBDMES
}
\end{lstlisting}
}
\end{document}
