\documentclass[onecolumn,floatfix,amsmath,amssymb,superscriptaddress]{article}

%\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}
\usepackage{mathtools}

\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\IR}[1]{\left(\lambda_{#1}\, \mu_{#1}\right)}
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}

\newcommand{\CG}[6]
	{
	{ C }_{ #1 #2 #3 #4 }^{ #5 #6 }	
	}

\newcommand{\WignerTHREEj}[6]
	{
	\left(
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right)
	}

\newcommand{\WignerSIXj}[6]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right\}
	}

\newcommand{\WignerNINEj}[9]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6 \\
   			#7 & #8 & #9
		\end{array}
	\right\}
	}

\newcommand{\half}[0]
	{
	  \frac{ 1 }{ 2 }	
	}

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Two-body triple reduced matrix elements for Alexis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bigskip

\maketitle
The code {\texttt{SU3RME\_Alexis\_2B} computes triple reduced matrix elements (trmes)
\begin{equation}
  \TRME
  {(i_{p}\, i_{n}) \alpha' N' (\lambda' \, \mu') S'_{p} S'_{n} S'}
  {\left[ \left\{ a^{\dagger (n_1\,0)}_{\half} \times a^{\dagger (n_2\,0)}_{\half}\right\}^{(\lambda_f\,\mu_f)}_{S_{f}} \times \left\{\tilde{a}^{(0\,n_3)}_{\half} \times \tilde{a}^{(0\,n_4)}_{\half}\right\}^{(\lambda_i\,\mu_i)}_{S_{i}}  \right]^{\rho_{0}(\lambda_0\,\mu_0)}_{S_{0}}}
  {(j_p\, j_n) \alpha N(\lambda \, \mu)S_{p}S_{n} S} _{\bar{\rho}}.
  \label{TBRMEs}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Input parameters}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
There are two input parameters of {\texttt{SU3RME\_Alexis\_2B}} code:
\begin{enumerate}
\item
Filename with model space definition. It must have $2J$ value set to -1!
\item 
Filename with a list of two-body tensors.
\end{enumerate}
The value of $2J$ must be set to -1 in the model space definition file. The reason is that the algorithm for basis construction excludes irreps that do not contain basis states with a given $2J$ value. If one set $2J=-1$, all values of $J$ are allowed
and consequently no irreps are eliminated. This ensures that reduced matrix elements for all irreps spanning a given model space are computed.

Each line of the input file with tensors contains the quantum numbers given in the following order: 
\begin{equation}
n_1 \quad n_2 \quad n_3 \quad n_4 \quad \lambda_f \quad \mu_f \quad 2S_f \quad \lambda_i \quad \mu_i \quad 2S_i \quad \lambda_0 \quad \mu_0 \quad 2S_0.
\label{InputTensor}
\end{equation}
Each line represents $3\times\rho_{0}^{\max}$ tensors, as we carry out calculations simultaneously for 3 tensor species: proton, neutron and proton-neutron, and for all possible multiplicities $0\le\rho_0<\rho^{\max}_{0}$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Output files}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For each line of the input file with tensor labels~(\ref{InputTensor}), the code generates $3\times \rho_{0}^{\max}$ output files with the following naming convention:
\begin{displaymath}
n_{1} \_ n_{2} \_ n_{3} \_ n_{4} \text{\texttt{--}} \lambda_f \_ \mu_f \_ 2S_{f} \text{\texttt{--}} \lambda_i \_ \mu_{i} \_ 2S_{i} \text{\texttt{--}} \rho_0 \_ \lambda_0 \_ \mu_0 \_ 2S_0.\text{\texttt{type}} 
\end{displaymath}
where
\begin{displaymath}
\text{\texttt{type}} \in
\begin{cases}
\texttt{pp} & \quad \text{for proton tensor} \\
\texttt{nn} & \quad \text{for neutron tensor} \\
\texttt{pn} & \quad \text{for proton-neutron tensor}
\end{cases}
\end{displaymath}
An output file contains values of the trmes (\ref{TBRMEs}) for a given $\rho_{0}$ tensor component and all many-nucleon bra and ket basis irreps that span a given model space. Note that bra and ket irreps are sorted according to their $N(\lambda \, \mu)S_p S_n S$ quantum numbers and hence output files contain trmes for blocks of bra and ket irreps with constant \Un{3}$\times\SU{2}_{S_p}\times \SU{2}_{S_n} \times \SU{2}_{S}$ quantum numbers.

The output file format is defined as follows. It is a text file. For each bra and ket pair with nonvanishing trmes, data are stored in form of bra and ket irreps quantum labels followed by an array of trmes:
\begin{eqnarray}
\nonumber
&i_p\, i_n\, \alpha'^{\max} \, 2S'_p \, 2S'_n \, 2S' \, N' \, \lambda' \, \mu' \, j_p \, j_n \,  \alpha^{\max} \, 2S_p \, 2S_n \, 2S \, N \, \lambda \, \mu \\
\nonumber
&\left\{\TRME{\alpha'}{\hat{V}}{\alpha}_{\bar{\rho}} \right\}
\end{eqnarray}
Here $\left\{\TRME{\alpha'}{\hat{V}}{\alpha}_{\bar{\rho}} \right\}$ symbolically signifies an array of trmes with $\alpha'^{\max}\times \alpha^{\max} \times \bar{\rho}^{\max}$ elements. The order of trmes in the array is given as:
\begin{displaymath}
\TRME{\alpha'}{\hat{V}}{\alpha}_{\bar{\rho}} = \left\{\TRME{\alpha'}{\hat{V}}{\alpha}_{\bar{\rho}} \right\}\left[\alpha'\alpha^{\max}\bar{\rho}^{\max} + \alpha\bar{\rho}^{\max} + \bar{\rho}\right], 
\end{displaymath}
where $0\le\alpha'<\alpha'^{\max}$, $0\le\alpha<\alpha^{\max}$, $0\le \bar{\rho} < \bar{\rho}^{\max}$. %, where $(\lambda\,\mu)\times (\lambda_0\,\mu_0) \rightarrow \bar{\rho}^{\max} (\lambda'\,\mu')$. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implementation details}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------
\subsection{Recoupling}
%----------------------
Generally, the tensors are specified in a way that is not always suitable for a direct computation of trmes. First, we need to make sure tensors are expressed in an {\texttt{LSU3shell} `compatible' form, that is, with harmonic oscillator quantum numbers given in an increasing order. This means that for each input tensor, we need to generate its recoupled form for all $\rho_0^{\max}$ components. This can be symbolically written as:
\begin{eqnarray}
\left[a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}\right]^{\rho_{0}\IR{0}}_{S_{0}}
&=& 
\sum_{\left\{\dots\right\}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{\left\{\dots\right\}}
\left[\hat{\text{R}}^{\left\{\dots\right\}}\right]^{\rho_0'\IR{0}}_{S_{0}},
\end{eqnarray}
where the symbol $\left\{\dots\right\}$ signifies spin and SU(3) quantum numbers needed to specify recoupled tensors, and $\hat{R}$ denotes tensors recoupled to {\texttt{LSU3shell} `compatible' form. The resulting trmes are then computed as
\begin{equation}
\TRME{\alpha'}{\left[a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}\right]^{\rho_{0}\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}}=
\sum_{\left\{\dots\right\}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{\left\{\dots\right\}}
\TRME{\alpha'}{\left[\hat{\text{R}}^{\left\{\dots\right\}}\right]^{\rho_0'\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}}.
\label{eq:rmescomputation}
\end{equation}

For example, the following proton (neutron) tensor
\begin{equation}
\left[ \left\{ a^{\dagger (3\,0)}_{\half} \times a^{\dagger (4\,0)}_{\half}\right\}^{(3\,2)}_{1} \times \left\{\tilde{a}^{(0\,3)}_{\half} \times \tilde{a}^{(0\,4)}_{\half}\right\}^{(2\,3)}_{1}  \right]^{\rho_{0}(2\,2)}_{2},
\label{eq:TensorExample}
\end{equation}
has $\rho_0^{\max}=2$ and hence we need to carry out recoupling for $\rho_0=0$ and $\rho_0=1$ operators thereby obtaining two linear combinations of the following recoupled tensors:
\begin{equation}
\left[\left\{ a^{\dagger (3\,0)}_{\half} \times \tilde{a}^{(0\,3)}_{\half}\right\}^{\IR{13}}_{S_{13}} \times \left\{a^{\dagger (4\,0)}_{\half} \times \tilde{a}^{(0\,4)}_{\half}\right\}^{\IR{24}}_{S_{24}}  \right]^{\rho'_{0}(2\,2)}_{2}.
\label{eq:TensorRecoupled}
\end{equation}
This means that $\{\dots\}\equiv\IR{13}S_{13}\IR{24}S_{24}$. The expansion of $\rho_0=0$ and $\rho_0=1$ operators~(\ref{eq:TensorExample}) in terms of recoupled tensors~(\ref{eq:TensorRecoupled}) is shown in the Table~\ref{tab:rhomax1}, Table~\ref{tab:rhomax2}, and Table~\ref{tab:rhomax3}. 

\begin{table}
\begin{center}
\caption{Expansion of $\rho_0=0$ and $\rho_0=1$ tensors~(\ref{eq:TensorExample}) in terms of recoupled tensors~(\ref{eq:TensorRecoupled}) with $\rho_{0}'^{\max}=1$}
\label{tab:rhomax1}
\begin{tabular}{|c|r|r|}
\hline
$\IR{13}S_{13} \IR{24}S_{24}$ & $d_{\rho_0=0 \rho_0'}$ & $d_{\rho_0=1 \rho_0'}$ \\
\hline 
\hline
(0 0)1 (2 2) 1 & -0.245 & -0.006 \\ 
(1 1)1 (1 1) 1 & -0.227 & -0.185 \\ 
(1 1)1 (3 3) 1 &  0.121 &  0.234 \\ 
(2 2)1 (0 0) 1 & -0.131 &  0.207 \\ 
(2 2)1 (4 4) 1 & -0.060 & -0.289 \\ 
(3 3)1 (1 1) 1 &  0.040 & -0.039 \\ 
\hline
\hline
\end{tabular}
\caption{Expansion of $\rho_0=0$ and $\rho_0=1$ tensors~(\ref{eq:TensorExample}) in terms of recoupled tensors~(\ref{eq:TensorRecoupled}) with $\rho_{0}'^{\max}=2$}
\label{tab:rhomax2}
\begin{tabular}{|c|c|c|}
\hline
$\IR{13}S_{13} \IR{24}S_{24}$ & $d_{\rho_0=0 \rho_0'}$ & $d_{\rho_0=1 \rho_0'}$ \\
\hline 
\hline
(1 1)1 (2 2) 1 & \{0, 0.415\} & \{0, -0.121\}  \\ 
(2 2)1 (1 1) 1 & \{0, 0.332\} & \{0, -0.126\}  \\ 
(2 2)1 (3 3) 1 & \{0, -0.081\} & \{0  -0.196\} \\ 
(3 3)1 (2 2) 1 & \{0, -0.159\} & \{0, -0.389\} \\ 
(3 3)1 (4 4) 1 & \{0, -0.346\} & \{0, -0.237\} \\ 
\hline
\hline
\end{tabular}
\caption{Expansion of $\rho_0=0$ and $\rho_0=1$ tensors~(\ref{eq:TensorExample}) in terms of recoupled tensors~(\ref{eq:TensorRecoupled}) with $\rho_{0}'^{\max}=3$}
\label{tab:rhomax3}
\begin{tabular}{|c|c|c|}
\hline
$\IR{13}S_{13} \IR{24}S_{24}$ & $d_{\rho_0=0 \rho_0'}$ & $d_{\rho_0=1 \rho_0'}$ \\
\hline 
\hline
(2 2)1 (2 2) 1 & \{-0.131,  0, 0.082\} & \{-0.260, 0, 0.466\}  \\ 
(3 3)1 (1 3) 1 & \{0.071, 0, -0.623\} & \{0.307, 0, 0.357\}  \\ 
\hline
\hline
\end{tabular}
\end{center}
\end{table}

The implementation of recoupling is provided by the class {\texttt{SU3InteractionRecoupler}}. The method {\texttt{SU3InteractionRecoupler::Insert\_adad\_aa\_Tensor}} takes as an input tensor labels along with associated coefficients given in the following order:$\left\{c_{\text{p}}^{\rho_0=0}, c_{\text{n}}^{\rho_0=0}, c_{\text{pn}}^{\rho_0=0},\dots,\,c_{\text{p}}^{\rho_{0}=\rho_{0}^{\max}-1}, c_{\text{n}}^{\rho_{0}=\rho_{0}^{\max}-1}, c_{\text{pn}}^{\rho_{0}=\rho_{0}^{\max}-1}\right\}$. This input is interpreted as a single operator that has the following form:
\begin{displaymath}
\sum_{0\le \rho_{0}<\rho_{0}^{\max}} \left( c_{p}^{\rho_{0}}\left[a^{\dagger}_{\text{p}}a^{\dagger}_{\text{p}}\tilde{a}_{\text{p}} \tilde{a}_{\text{p}}\right]^{\rho_{0}\IR{0}}_{S_{0}} + c_{\text{n}}^{\rho_{0}}\left[a^{\dagger}_{\text{n}}a^{\dagger}_{\text{n}}\tilde{a}_{\text{n}} \tilde{a}_{\text{n}}\right]^{\rho_{0}\IR{0}}_{S_{0}}  + c_{\text{pn}}^{\rho_{0}}\left[a^{\dagger}_{\text{p}}a^{\dagger}_{\text{n}}\tilde{a}_{\text{n}} \tilde{a}_{\text{p}}\right]^{\rho_{0}\IR{0}}_{S_{0}}\right).
\end{displaymath}
What we need is to obtain expansion in terms of recoupled tensors for each $\rho_0$ component of the input tensor.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solution 1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The function {\texttt{RecoupleTensorStoreOnDisk}} takes as an input quantum labels~(\ref{InputTensor}) along with $\rho_{0}^{\max}$ and $\rho_0$, recouples the $\rho_0$ component of the tensor, and stores resulting expansion into two files:
\begin{eqnarray*}
&n_{1} \_ n_{2} \_ n_{3} \_ n_{4} \text{\texttt{--}} \lambda_f \_ \mu_f \_ 2S_{f} \text{\texttt{--}} \lambda_i \_ \mu_{i} \_ 2S_{i} \text{\texttt{--}} \rho_0 \_ \lambda_0 \_ \mu_0 \_ 2S_0.\text{\texttt{PPNN}} \\
&n_{1} \_ n_{2} \_ n_{3} \_ n_{4} \text{\texttt{--}} \lambda_f \_ \mu_f \_ 2S_{f} \text{\texttt{--}} \lambda_i \_ \mu_{i} \_ 2S_{i} \text{\texttt{--}} \rho_0 \_ \lambda_0 \_ \mu_0 \_ 2S_0.\text{\texttt{PN}}. 
\end{eqnarray*}

To obtain an expansion of the $\rho_0$th tensor component, we set $c^{\rho_0}_{p}=c^{\rho_0}_{n}=c^{\rho_0}_{pn}=1$ and the rest of coefficients are equal to zero. As a results, {\texttt{SU3InteractionRecoupler::Insert\_adad\_aa\_Tensor}} carry out recoupling of $\rho_0$ component only (simultaneously for proton, neutron, and proton-neutron parts):
\begin{eqnarray}
\nonumber
\left[a^{\dagger}_{\text{p}}a^{\dagger}_{\text{p}}\tilde{a}_{\text{p}} \tilde{a}_{\text{p}}\right]^{\rho_{0}\IR{0}}_{S_{0}} &=&
\sum_{\left\{\dots\right\}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{\left\{\dots\right\}}
\left[\hat{\text{R}}_{p}^{\left\{\dots\right\}}\right]^{\rho_0'\IR{0}}_{S_{0}} \\
\nonumber
\left[a^{\dagger}_{\text{n}}a^{\dagger}_{\text{n}}\tilde{a}_{\text{n}} \tilde{a}_{\text{n}}\right]^{\rho_{0}\IR{0}}_{S_{0}} &=&
\sum_{\left\{\dots\right\}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{\left\{\dots\right\}}
\left[\hat{\text{R}}_{n}^{\left\{\dots\right\}}\right]^{\rho_0'\IR{0}}_{S_{0}}
\\
\nonumber
\left[a^{\dagger}_{\text{p}}a^{\dagger}_{\text{n}}\tilde{a}_{\text{n}} \tilde{a}_{\text{p}}\right]^{\rho_{0}\IR{0}}_{S_{0}}&=&
\sum_{\left\{\dots\right\}_{pn}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{\left\{\dots\right\}_{pn}}
\left[\hat{\text{R}}_{pn}^{\left\{\dots\right\}_{pn}}\right]^{\rho_0'\IR{0}}_{S_{0}}
\end{eqnarray}
This means that we call {\texttt{RecoupleTensorStoreOnDisk}} for a given input tensor $\rho_0^{\max}$ times, at each step producing {\texttt{.PPNN}} and {\texttt{.PN}} files that contain labels of recoupled tensors and their expansion coefficients $d^{\{\dots\}}_{\rho_0\rho_0'}$. Consequently, we read these input files and carry out computations of trmes according to~(\ref{eq:rmescomputation}). The obvious disadvantage of this approach is that we have to call the subroutine for computing trmes $\rho_{0}^{\max}$ times, while it would make more sense to call it only once and handle all $\rho_0^{\max}$ components simultaneously. However, this would require much more elaborate development.

In our calculations, we set \texttt{k\_dependent\_tensor\_strenghts = false}} as our expansion coefficients for a given recoupled tensor depend only on $\rho_0'$ quantum numbers. The input {\texttt{.PPNN}} files contain the following order of expansion coefficients:
\begin{displaymath}
\text{\texttt{coeffs}}_{\rho_0}^{\left\{\dots\right\}}=\left\{d_{\rho_0 \rho_0'=0}^{p}, d_{\rho_0 \rho_0'=0}^{n},d_{\rho_0 \rho_0'=1}^{p}, d_{\rho_0 \rho_0'=1}^{n},\dots\right\}
\end{displaymath}
After reading into memory, we call {\texttt{CInteractionPPNN::TransformTensorStrengthsIntoPP\_NN\_structure}} to get the following order:
\begin{displaymath}
\text{\texttt{coeffs}}_{\rho_0}=\left\{d_{\rho_0 \rho_0'=0}^{p}, d_{\rho_0 \rho_0'=1}^{p},\dots,d^{p}_{\rho_0 \rho_0'=\rho_0'^{\max}-1},  d_{\rho_0 \rho_0'=0}^{n},d_{\rho_0 \rho_0'=1}^{n},\dots, d^{n}_{\rho_0 \rho_0'=\rho_0'^{\max}-1}\right\}
\end{displaymath}
Note that $d^{p}_{\rho_0 \rho_0'}=d^{n}_{\rho_0 \rho_0'}$. The resulting trmes are computed as
\begin{eqnarray}
\TRME{\alpha'}{\left[a^{\dagger}_p a^{\dagger}_p \tilde{a}_p \tilde{a}_p \right]^{\rho_{0}\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}}&=&
\sum_{\left\{\dots\right\}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{p\left\{\dots\right\}}
\TRME{\alpha'}{\left[\hat{\text{R}}_{p}^{\left\{\dots\right\}}\right]^{\rho_0'\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}} \\
\TRME{\alpha'}{\left[a^{\dagger}_n a^{\dagger}_n \tilde{a}_n \tilde{a}_n \right]^{\rho_{0}\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}}&=&
\sum_{\left\{\dots\right\}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{n\left\{\dots\right\}}
\TRME{\alpha'}{\left[\hat{\text{R}}_{n}^{\left\{\dots\right\}}\right]^{\rho_0'\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}} \\
\TRME{\alpha'}{\left[a^{\dagger}_p a^{\dagger}_n \tilde{a}_n \tilde{a}_p \right]^{\rho_{0}\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}}&=&
\sum_{\left\{\dots\right\}_{pn}}
\sum_{\rho_{0}'}
d_{\rho_0 \rho_0'}^{pn\left\{\dots\right\}}
\TRME{\alpha'}{\left[\hat{\text{R}}_{pn}^{\left\{\dots\right\}_{pn}}\right]^{\rho_0'\IR{0}}_{S_{0}}}{\alpha}_{\bar{\rho}} 
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solution 2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
An alternative solution, yet more tedious to implement, is the following:
\begin{enumerate}
\item Carry out recoupling for each $\rho_0$ tensor component and obtain its recoupled expansion directly from {\texttt{SU3InteractionRecoupler}} and store them in some datastructure.
\item Utilize {\texttt{SU3InteractionRecoupler}} to create an input file {\texttt{filename.PPNN}} and {\texttt{filename.PN}}, but coefficients will be never used. During computation I will compute trmes for each recoupled tensor, but its coefficients
will be taken from datatype datastructure created in Step 1.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{RME computation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Function {\texttt{ComputeResultingRMEs}} implements formula (\ref{eq:rmescomputation}) to compute trmes (\ref{TBRMEs}) of a given tensor for all possible values of $0\le\alpha'<\alpha'^{\max}$, $0\le\alpha<\alpha^{\max}$, and $0\le \bar{\rho} < \bar{\rho}^{\max}$. This function takes as an input a set of trmes along with associated expansion coefficients. This is implemented as {\texttt{vector<MECalculatorData>}} data structure. Its content can be schematically written as
\begin{displaymath}
\left\{
\left\{
\TRME
{\alpha'}
{\hat{\text{R}}_{1}^{\rho_0'}}
{\alpha}_{\bar{\rho}}\right\}, \left\{d^{R_{1}}_{\rho_{0}'}\right\}\right\},
\left\{
\left\{
\TRME
{\alpha'}
{\hat{\text{R}}_{2}^{\rho_0'}}
{\alpha}_{\bar{\rho}}\right\}, \left\{d^{R_{2}}_{\rho_{0}'}\right\}\right\},
\end{displaymath}
where one-dimensional arrays with trmes have the following order of elements:
\begin{displaymath}
\left\{
\TRME
{\alpha'}
{\hat{\text{R}}^{\rho_0'}}
{\alpha}_{\bar{\rho}}
\right\}
\rightarrow
\text{\texttt{rmes}}[\alpha'][\alpha][\rho_0'][\bar{\rho}].
\end{displaymath}
Expansion coefficients are stored as one-dimensional arrays with the following order:
\begin{displaymath}
\left\{d^{R}_{\rho_{0}'}\right\}\rightarrow \text{\texttt{coeffs}}[\rho_{0}'].
\end{displaymath}

Resulting trmes are stored as one-dimensional arrays with the following order of elements:
\begin{displaymath}
\TRME
{\alpha'}
{\left[a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}\right]^{\rho_{0}\IR{0}}_{S_{0}}}
{\alpha}_{\bar{\rho}}
\rightarrow
\text{\texttt{rmes}}[\alpha'][\alpha][\bar{\rho}].
\end{displaymath}
\end{document}
