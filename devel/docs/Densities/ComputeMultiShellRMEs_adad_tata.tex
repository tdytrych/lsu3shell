\documentclass[onecolumn,floatfix,amsmath,amssymb,superscriptaddress]{revtex4}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\IR}[1]{\left(\lambda_{#1}\, \mu_{#1}\right)}
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}

\newcommand{\CG}[6]
	{
	{ C }_{ #1 #2 #3 #4 }^{ #5 #6 }	
	}

\newcommand{\WignerTHREEj}[6]
	{
	\left(
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right)
	}

\newcommand{\WignerSIXj}[6]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right\}
	}

\newcommand{\WignerNINEj}[9]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6 \\
   			#7 & #8 & #9
		\end{array}
	\right\}
	}

\newcommand{\half}[0]
	{
	  \frac{ 1 }{ 2 }	
	}

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{{\texttt{ComputeMultiShellRMEs\_adad\_tata}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bigskip

\maketitle
This code computes multi-shell reduced matrix elements (rmes) of operators $a^{\dagger}a^{\dagger}$/$\tilde{a}\tilde{a}$ for a system of identical nucleons.  Namely, it computes
\begin{displaymath}
  \TRME
  {i}
  {\left[a^{\dagger (\eta_1\,0)}_{\half} \times a^{\dagger (\eta_2\,0)}_{\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}}
  {j}_{\bar{\rho}} 
  \quad \text{or} \quad
  \TRME
  {i}
  {\left[\tilde{a}^{(\eta_1\,0)}_{\half} \times \tilde{a}^{(\eta_2\,0)}_{\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}}
  {j}_{\bar{\rho}},
\end{displaymath}
where $i\in 0\hbar\Omega,1\hbar\Omega,\dots, N_{\max}\hbar\Omega$ and $j\in 0\hbar\Omega,1\hbar\Omega,\dots, N_{\max}\hbar\Omega$ denote indices associated with (a set of equivalent) \SU{3}$\times$\SU{2} bra and ket irreps for a system of protons or neutrons.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Input parameters}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\texttt{ComputeMultiShellRMEs\_adad\_tata} has eight input parameters,
\begin{displaymath}
\texttt{<bra model space> <ket model space> <+/-($\eta_{1}$+1)> <+/-($\eta_{2}$+1)> <p/n> <$\lambda_{0}$> <$\mu_0$> <2$S_0$>}
\end{displaymath}
With the following meaning:
\begin{itemize}
\item 
\texttt{<bra model space> <ket model space>}: these model space files determine the number of protons (neutrons), and $N_{\max}$ cutoff parameter that define bra and ket model spaces and associated basis of \SU{3}$\times$\SU{2} irreps. It is important to note that the complete set of irreps is considered and no selection rules are applied. Both model spaces must have the same $N_{\max}$ parameter.
\item
\texttt{<+/-($\eta_{1}$+1)> <+/-($\eta_{2}$+1)> <p/n>}: defines harmonic oscillator shells $\eta_{1}$ and $\eta_{2}$, operator type (\texttt{+} $\rightarrow a^{\dagger}$, - $\rightarrow \tilde{a}$), and whether protons or neutrons are considered. For example 
\begin{eqnarray}
\texttt{+3 +3 p} &\rightarrow& \text{compute}\quad
\TRME {i_p} {\left[a^{\dagger (2\,0)}_{p\half} \times a^{\dagger (2\,0)}_{p\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}}
  {j_p}_{\bar{\rho}} \\
\texttt{+1 +2 n} &\rightarrow& \text{compute}\quad
\TRME {i_n} {\left[a^{\dagger (0\,0)}_{n\half} \times a^{\dagger (1\,0)}_{n\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}}
  {j_n}_{\bar{\rho}} \\
\texttt{-3 -2 n} &\rightarrow& \text{compute}\quad
\TRME {i_n} {\left[\tilde{a}^{\dagger (0\,2)}_{n\half} \times \tilde{a}^{(0\,1)}_{n\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}}
  {j_n}_{\bar{\rho}}
\end{eqnarray}
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Output files}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The resulting rmes are stored in an output binary file. This file can be subsequently used by {\texttt{su3densePN\_slow\_silly}} code that computes nuclear densities. The output file name is set by the structure of operator and bra and ket spaces.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implementation details}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Internally, if input $\eta_{1} > \eta_{2}$, the code adopts the following relation:
\begin{displaymath}
  \left[a^{\dagger (\eta_1\,0)}_{\half} \times a^{\dagger (\eta_2\,0)}_{\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}=(-)^{\eta_{1}+\eta_{2}-\lambda_{0}-\mu_{0}-S_0}\left[a^{\dagger (\eta_2\,0)}_{\half} \times a^{\dagger (\eta_1\,0)}_{\half}\right]^{(\lambda_0\,\mu_0)}_{S_{0}}.
\end{displaymath}
\end{document}
