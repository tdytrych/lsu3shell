\documentclass[11pt,a4paper,oneside]{report}

%\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}
\usepackage{amssymb}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}
\renewcommand*{\marginfont}{\color{red}}

\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}

\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}
\newcommand{\half}[0]{\frac{1}{2}}

\definecolor{lightgray}{gray}{0.97}
\definecolor{forestgreen}{RGB}{0,128,0}
\definecolor{navyblue}{RGB}{0,0,128}
\definecolor{brick}{RGB}{128,0,0}
\definecolor{prepro}{RGB}{128,0,128}

\lstset{language=C++,
  basicstyle=\ttfamily,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  mathescape=true,
  morecomment=[l]{!\ }% Comment only with space after !
}

\begin{document}
\section*{\texttt{Formats used for storage of reduced matrix elements of proton or neutron irreps}}
The tables of reduced matrix elements (rmes) generated for a system of identical nucleons by various codes in \texttt{programs/Densities} comes in {\color{red}three} different formats: 
\begin{itemize}
\item CSR
\item \texttt{IJ\_Indices}
\item \texttt{IJ\_RMEs}
\end{itemize}
This is very cumbersome and confusing. This document describe their structure and process of generation as well as codes that use particular kind. The ultimate goal, thought, should be to adopt a single (and best) format based on memory/speed/convenience benchmarks.
\section*{\texttt{CSR} format}
\subsection*{Creation: \texttt{ComputeMultiShellRMEs.cpp}}
\subsection*{Header}
\begin{lstlisting}
int A, Nmax
Data needed to construct CTensorStructure:
uint64_t nshells
for each shell structure of $a^{+}$/$\tilde{a}$ operators
for each shell $\vec{\Gamma}\equiv$SU3xSU2_VEC of single-shell operators
$\vec{\Omega}$ ... vector of SU3xSU2::LABELS of intershell coupling
\end{lstlisting}
Header is stored on disk by calling function
\begin{lstlisting}
uint64_t SaveHeader(int A, int Nmax, CTensorStructure *tensor, MPI_File &fh)
\end{lstlisting}
which is implemented in \texttt{Densities/ComputeMultiShellRMEs.cpp}. 
\subsection*{RMEs data}
Rmes data are stored on disk using calls of MPI I/O in function 
\begin{lstlisting}
void CalculateMultiShellRME_PP
\end{lstlisting}
Data are represented as
\begin{lstlisting}
uint64_t n_rows, n_cols 
uint64_t global_values, global_colinds
uint64_t real_bytesize, index_bytesize
std::vector<float> values(global_values) ... $\TRME{\alpha_{i}\,i}{T^{\rho_{0}}}{\alpha_{j}\,j}_{\bar{\rho}}$
std::vector<tensorfile::index_t> colinds(global_colinds)
std::vector<tensorfile::index_t> rowptrs_values(n_rows + 1)
std::vector<tensorfile::index_t> rowptrs_colinds(n_rows + 1)
\end{lstlisting}
\subsection*{Loading}
The CSR format is used only for storing rmes data on disk. When needed, CSR table is loaded internally and returned as data given in \texttt{IJ\_Indices} format. Relevant functions can be found in \texttt{su3dense\_tensor\_read.h/cpp}.
\begin{lstlisting}
void ReadHeaderCSR
void ReadProtonTensorRMEsCSR $\rightarrow$ IJ_Indices & vector<float>
void ReadNeutronTensorRMEsCSR $\rightarrow$ IJ_Indices & vector<float>
\end{lstlisting}

\subsection*{Using} 
Files with rmes given in CSR format are expected by the following codes:
\begin{itemize}
\item \texttt{su3denseP}: input files with rmes of one-body proton operators.
\item \texttt{su3densePN\_adadtata\_pnnp}: input files with rmes of both proton and neutron one-body operators.
\item \texttt{GenerateMultiShellRMEs\_adadtata}: input files with rmes of two-nucleon protons operators.
\item \texttt{ReadRMEs}: input files with rmes of one- or two-nucleon operators.
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   IJ_Indices   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\texttt{IJ\_Indices} format}
\subsection*{Creation: \texttt{GenerateMultiShellRMEs\_adadtata}}
Notice that this code takes as input two-body rmes stored on disk using CSR format. However, after recoupling transformation resulting rmes are stored on disk in \texttt{IJ\_Indices} format. Obviously, this format is used for storing rmes of $a^{\dagger}a^{\dagger}\tilde{a}\tilde{a}$, i.e. two-body, tensors.  The file with rmes data is generated by calling function
\begin{lstlisting}
void SaveRMEs_IJIndices
\end{lstlisting}
\subsection*{Format}
\begin{lstlisting}
int n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0;
uint32_t A, Nmax;
size_t nij_pairs
std::vector<{i, j, index}> IJ_Indices(nij_pairs)
size_t nrmes
std::vector<float> rmes(nrmes)
\end{lstlisting}
Data are represented as
\begin{lstlisting}
std::vector<std::array<uint32_t, 3>> ij_indices
std::vector<float> rmes
\end{lstlisting}
Note that using \texttt{uint32\_t} for an index pointing to an array of rmes limits size of rms to 4,294,967,295 elements. This translates to 15GB, which is large for now, but it has to be changed to \texttt{size\_t}.
{\color{red}TODO: \texttt{array<uint32\_t, 3>} $\rightarrow$ \texttt{tuple<uint32\_t, uint32\_t, size\_t>}.}
\subsection*{Loading}
Implemented in \texttt{su3dense\_tensor\_read.h/cpp}.
\begin{lstlisting}
void LoadRMEsIJ_Indices
\end{lstlisting}
\subsection*{Using} 
\texttt{su3denseP}: input file with rme tables of $\left[\left\{a^{\dagger}a^{\dagger}\right\}\left\{\tilde{a}\tilde{a}\right\}\right]$ operators produced by \texttt{GenerateMultiShellRMEs\_adadtata}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   IJ_RMES   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\texttt{IJ\_RMES} format}
\subsection*{Creation:} 
\begin{itemize}
\item \texttt{GenerateMultiShellRMEs\_adad\_tata} 
\item \texttt{ComputeMultiShellRMEs\_ad\_ta}
\end{itemize}
\subsection*{Interface:} 
\begin{lstlisting}
void SaveRMEsIJ_RMES
void ReadRMEsIJ_RMES
\end{lstlisting}
Their implementation can be found in \texttt{su3dense/tensor\_read.cpp}.
\vspace{0.7cm}
\subsection*{Format:}
\begin{lstlisting}
uint32_t Af, Ai, Nmax
SU3xSU2::LABELS $\omega_{0}$
size_t $n$ // number of {i, j} pairs
std::pair<uint32_t, uint32_t> ij$_{1}$
size_t size$_1$
float rmes[size$_{1}$]
$\vdots$
std::pair<uint32_t, uint32_t> ij$_{n}$
size_t size$_{n}$
float rmes[size$_{n}$]
\end{lstlisting}
Notice that this format does not include information on detailed structure of a given operator. For instance both \texttt{CSR} and \texttt{IJ\_Indices} formats contain information on their inner algebraic structure.
Also, note that the number of rmes for a given pair \texttt{i,j} can be computed as \texttt{size}$=\alpha_{i}^{\max}\alpha_{j}^{\max}\rho_{0}^{\max}\bar{\rho}^{\max}$ and hence it is not essential to store \texttt{size} for
each pair of indices.

Rme table data are represented by the following single datastructure
\begin{lstlisting}
hash[{uint32_t i, uint32_t j} --> vector<float>rmes]
\end{lstlisting}

\vspace{0.7cm}
\subsection*{Using:} 
\begin{itemize}
\item \texttt{su3densePN\_slow\_silly}: expects \texttt{IJ\_RMES} format for input files with rme tables of general proton and neutron operators.
\item \texttt{su3densePN\_adadtata\_pnnp}: key computational kernel requires proton and neutron rme tables of $\left(a^{\dagger}\tilde{a}\right)$ operators in \texttt{IJ\_RMES} data format. At the same time, input files with rme tables are provided in CSR format.
We transform \texttt{IJ\_Indices} format into \texttt{IJ\_RMES}.
\end{itemize}
\end{document}
