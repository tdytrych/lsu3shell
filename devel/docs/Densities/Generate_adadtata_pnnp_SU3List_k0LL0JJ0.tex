\documentclass[11pt,a4paper,oneside]{report}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}
\usepackage{amssymb}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}
\renewcommand*{\marginfont}{\color{red}}

\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}

\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}
\newcommand{\half}[0]{\frac{1}{2}}

\definecolor{lightgray}{gray}{0.97}
\definecolor{forestgreen}{RGB}{0,128,0}
\definecolor{navyblue}{RGB}{0,0,128}
\definecolor{brick}{RGB}{128,0,0}
\definecolor{prepro}{RGB}{128,0,128}

\lstset{language=[77]Fortran,
  basicstyle=\ttfamily,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  mathescape=true,
  morecomment=[l]{!\ }% Comment only with space after !
}

\begin{document}
\section*{\texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0}}
This code generates a list of quantum labels of \SU{3}$\times$\SU{2} proton-neutron tensors 
\begin{equation}
n_1^{\pi} \quad n_2^{\nu} \quad n_3^{\nu} \quad n_4^{\pi} \quad \lambda_f \quad \mu_f \quad 2S_f \quad \lambda_i \quad \mu_i \quad 2S_i \quad \lambda_0 \quad \mu_0 \quad 2S_0 \quad \kappa_{0} \quad 2L_{0} \quad 2J_{0}
\end{equation}
that are associated with $\rho_{0}^{\max}$ tensors
\begin{equation}
\left[ 
\left\{ 
a^{\dagger(n_{1}^{\pi}\,0)}_{\half} \times a^{\dagger(n_{2}^{\nu}\,0)}_{\half}
\right\}^{(\lambda_f\,\mu_f)}_{S_{f}} 
\times 
\left\{
\tilde{a}^{(0\,n_3^{\nu})}_{\half} \times \tilde{a}^{(0\,n_{4}^{\pi})}_{\half}
\right\}^{(\lambda_i\,\mu_i)}_{S_{i}}  
\right]^{\rho_{0}(\lambda_0\,\mu_0)S_{0}}_{k_{0}L_{0}J_{0}}.
\end{equation}
\section*{Input Parameters}
\texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0} has following input parameters:
\begin{displaymath}
\texttt{<valence shell> <Nmax> <parity> <SU3 selection> <list of selected }2J_{0}\texttt{ values>}
\end{displaymath}

\noindent
{\bf Input Parameters:}
\begin{itemize}
\item 
\texttt{<valence shell>}: harmonic oscillator shell number of a valence shell.
\item 
\texttt{<Nmax>}: model space for which a given list of tensors are active. 
\item
\texttt{<parity>}: \\ 
1 $\dots$ positive parity operators; -1 $\dots$ negative parity operators.
\item
\texttt{<SU3 selection>}: \\ 
0 $\dots$ select tensors with $\lambda_{0}+\mu_{0}$ even number (e.g. interaction); 1 $\dots$  select tensors with $\lambda_{0}+\mu_{0}$ odd number; -1 $\dots$ select all tensors.
\item 
\texttt{<list of selected } $2J_{0}$\texttt{ values>}: List of selected 2$J_{0}$ values; -1 to generate all possible $J_{0}$ values.
\end{itemize}
\section*{Example}
\begin{itemize}
\item
To generate positive parity two-nucleon tensors valid for $p$-shell nuclei in $N_{\max}=4$  model space, with $\lambda_{0}+\mu_{0}$ even and with $J_{0}=0$ values:
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0 1 4 1 0 0}}
\end{displaymath}
\item
To generate positive parity two-nucleon tensors valid for $p$-shell nuclei in $N_{\max}=4$  model space, with $\lambda_{0}+\mu_{0}$ even and with $J_{0}=0$ and $J_{0}=2$ values:
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0 1 4 1 0 0 4}}
\end{displaymath}
\end{itemize}

\section*{Testing}
The number of proton-neutron $\SU{3}\times\SU{2}$ tensors generated for a given set of input parameters with \texttt{<SU3 selection>=-1} must be equal to the number of proton-neutron \SU{2} tensors generated for the same input parameters.
For example, 
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_pnnp\_SU3list\_k0LL0JJ0 1 4 1 -1 0}}
\end{displaymath}
generates $28808$ tensors. At the same time,
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_SU2list PN 1 4 1 0 | wc -l}}
\end{displaymath}
prints out $28808$ tensors as well.
\end{document}
