\documentclass[11pt,a4paper,oneside]{article}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}
\usepackage{amssymb}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}


\DeclareMathOperator{\Tr}{Tr}
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\vgamma}[0]{\boldsymbol{\gamma}}
\newcommand{\vomega}[0]{\boldsymbol{\omega}}

%
% group notation
%
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\On}[1]{\ensuremath{\mathrm{O}( #1 )}}
\newcommand{\Spn}[1]{\ensuremath{\mathrm{Sp}( #1 )}}
\newcommand{\SpR}[1]{\ensuremath{\mathrm{Sp}( #1,\mathbb{R} )}}

%
% algebra notation
%

\newcommand{\IR}[1]{\ensuremath{(\lambda_{#1}\,\mu_{#1})}}
\newcommand{\su}[1]{\ensuremath{\mathfrak{su}( #1 )}}
\newcommand{\un}[1]{\ensuremath{\mathfrak{u}( #1 )}}
\newcommand{\so}[1]{\ensuremath{\mathfrak{so}( #1 )}}
\newcommand{\on}[1]{\ensuremath{\mathfrak{o}( #1 )}}
\newcommand{\spn}[1]{\ensuremath{\mathfrak{sp}( #1 )}}
\newcommand{\spR}[1]{\ensuremath{\mathfrak{sp}( #1, \mathbb{R} )}}

%
% boson and fermion
%
\newcommand{\bdag}{\ensuremath{ {b^\dagger} }}
\newcommand{\adagg}{\ensuremath{ {a^\dagger} }} % extra space outside the dagger
\newcommand{\adag}{\ensuremath{ a^\dagger }}
\newcommand{\B}{\ensuremath{\bullet}}
\newcommand{\C}{\ensuremath{\circ}}

%
% CG and Racah
%
\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
%\newcommand{\ket}[1]{\ensuremath{\left| #1 \right\rangle}}
%\newcommand{\bra}[1]{\ensuremath{\left\langle #1 \right|}}
%\newcommand{\braket}[2]{\ensuremath{\left\langle #1 | #2 \right\rangle}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}

%
% Latin abbr.
%
\newcommand{\ie}{\emph{i.e.}}
\newcommand{\eg}{\emph{e.g.}}
\newcommand{\etal}{\emph{et al.}}
\newcommand{\etc}{\emph{etc.}}

%
% Miscellany
%
\newcommand{\half}{\ensuremath{\textstyle{\frac{1}{2}}}}
\newcommand{\threehalves}{\ensuremath{\textstyle{\frac{3}{2}}}}

\newcommand{\one}{\ensuremath{\mathit{1}}}
\newcommand{\two}{\ensuremath{\mathit{2}}}

\newcommand{\betb}{\begin{tabular}{p{4.0cm}p{9.0cm}}}
\newcommand{\entb}{\end{tabular}}
\newcommand{\MPC}[2]{\begin{minipage}[t]{#1\textwidth}
  \begin{center}~#2\end{center}\end{minipage}}
\newcommand{\MPL}[2]{~\begin{minipage}[t]{#1\textwidth}#2\end{minipage}}

\newcommand{\matrixindex}[2]{\ensuremath{\left(\mathcal{#1} #2 \right)}}

\newcommand{\bsigma}{\boldsymbol{\sigma}}
\newcommand{\bn}{\boldsymbol{n}}
\newcommand{\bomega}{\boldsymbol{\omega}}
\newcommand{\ba}{\ensuremath{\boldsymbol{a}}}
\newcommand{\adagger}{\ensuremath{a^\dagger}}
\newcommand{\badagger}{\ensuremath{\boldsymbol{a}^\dagger}}

% normalization coefficient for SU(3) 2-fermion state
\newcommand{\N}[1]{\ensuremath{\mathcal{N}_{#1} }}

\newcommand{\LS}[1]{\left(#1\half\right)}
\newcommand{\irrep}[2]{\left(#1 #2\right)}

\newcommand{\can}[1]{\ensuremath{\epsilon_{#1}\Lambda_{#1}M_{\Lambda_{#1}}}}

%
% color scheme
%
\newcommand{\red}[1]{{ \color{red} #1 }}
\newcommand{\green}[1]{{ \color{green} #1 }}
\newcommand{\blue}[1]{{ \color{blue} #1 }}
\newcommand{\magenta}[1]{{ \color{magenta} #1 }}

\lstset{language=C++,
  basicstyle=\ttfamily,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  mathescape=true,
  morecomment=[l]{!\ }% Comment only with space after !
}

\title{\texttt{TBDMEsSU3toSU2} \\ Conversion of \SU{3} two-body density matrix elements to \SU{2}}
\author{Tom\'{a}\v{s} Dytrych}
\date{\today}

\begin{document}
\maketitle
\section*{Input Parameters}
This code takes as an input two filenames:
\begin{displaymath}
\texttt{
<}\SU{3}\texttt{ TBDMEs file> <file with list of }\SU{2}\texttt{ tensors>}
\end{displaymath}
\begin{itemize}
\item 
\texttt{<}$\SU{3}$\texttt{ TBDMEs file>}: file with tables of \SU{3} TBDMEs produced either by \texttt{su3denseP} or \texttt{su3densePN\_adadtata\_pnnp} codes. 
\item
\texttt{<file with list of }$\SU{2}$\texttt{ tensors>}: this file contains quantum labels of target two-body \SU{2} tensors,
\begin{equation}
n_1 \, l_{1}\, 2j_{1} \quad n_{2}\,l_{2}\,2j_{2} \quad 2J_{f} \quad n_3\, l_{3}\, 2j_{3} \quad n_4\, l_{4}\, 2j_{4} \quad 2J_{i} \quad 2J_{0}
\end{equation}
This file can be produced, e.g., by \texttt{Generate\_adadtata\_SU2list} code.
\end{itemize}
It is important to note that user is responsible to make sure all non-vanishing \SU{3} TBDMEs needed for the construction of target \SU{2} TBDMEs are given in \texttt{<}$\SU{3}$\texttt{ TBDMEs file>}. 
\section*{Output File}
The resulting \SU{2} TBDMEs are stored in an output text files named 
\begin{itemize}
\item \texttt{<file with list of }\SU{2}\texttt{ tensors>.tbdmes}
\end{itemize}
Each line in the output file has the following structure:
\begin{equation}
n_1 \, l_{1}\, 2j_{1} \quad n_{2}\,l_{2}\,2j_{2} \quad 2J_{f} \quad n_3\, l_{3}\, 2j_{3} \quad n_4\, l_{4}\, 2j_{4} \quad 2J_{i} \quad 2J_{0} \quad \RedME{J_{f}}{\hat{T}}{J_{i}}
\end{equation}
\newpage
\section{Implementation notes}
\subsection{Computational Strategy}
Formula derived by Alexis reads,
\begin{multline}
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger}_{n^{\pi}_{1}l^{\pi}_{1}j^{\pi}_{1}}
a^{\dagger}_{n^{\nu}_{2}l^{\nu}_{2}j^{\nu}_{2}}
\right\}^{J_{1}}
\left\{
\tilde{a}_{n^{\nu}_{3}l^{\nu}_{3}j^{\nu}_{3}}
\tilde{a}_{n^{\pi}_{4}l^{\pi}_{4}j^{\pi}_{4}}
\right\}^{J_{2}}
\right]^{J_{0}}
}
{J_{i}}
 = \\
(-1)^{n_{3}^{\nu} + n_{4}^{\pi}}
\!\!\!\!\!\!
\sum_{
\begin{array}{c}
\rho_{0}\omega_{0} \\
\kappa_{0} L_{0} S_{0}
\end{array}
}
\!\!\!\!\!\!
\sum_{
\begin{array}{c}
\omega_{i}\omega_{f}\\
S_{i} S_{f}
\end{array}
}
\!\!\!\!\!\!
\sum_{
\begin{array}{c}
k_{f} L_{f} \\
k_{i} L_{i}
\end{array}
}
\Pi_{L_{0}S_{0}J_{1}J_{2}}
\Pi_{j^{\pi}_{1}j^{\nu}_{2}L_{f}S_{f}}
\Pi_{j^{\nu}_{3}j^{\pi}_{4}L_{i}S_{i}}
\RedCG{\omega_{f}k_{f}L_{f}}{\omega_{i}k_{i}L_{i}}{\omega_{0}k_{0}L_{0}}_{\rho_{0}} \\
\RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}k_{f}L_{f}}
\RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}k_{i}L_{i}}
\Wigninej
{L_{f}}{L_{i}}{L_{0}}
{S_{f}}{S_{i}}{S_{0}}
{J_{1}}{J_{2}}{J_{0}}
\Wigninej
{l_{1}^{\pi}}{\half}{j_{1}^{\pi}}
{l_{2}^{\nu}}{\half}{j_{2}^{\nu}}
{L_{f}}{S_{f}}{J_{1}}
\Wigninej
{l_{3}^{\nu}}{\half}{j_{3}^{\nu}}
{l_{4}^{\pi}}{\half}{j_{4}^{\pi}}
{L_{i}}{S_{i}}{J_{2}} \\
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger(n_{1}^{\pi}0)}_{\frac{1}{2}}
a^{\dagger(n^{\nu}_{2} 0)}_{\frac{1}{2}}
\right\}^{\omega_{f}}_{S_{f}}
\left\{
\tilde{a}^{(0\,n^{\nu}_{3})}_{\frac{1}{2}}
\tilde{a}^{(0\,n^{\pi}_{4})}_{\frac{1}{2}}
\right\}^{\omega_{i}}_{S_{i}}
\right]^{\rho_{0}\omega_{0}S_{0}}_{k_{0}L_{0}J_{0}}
}
{J_{i}}
\end{multline}
Both F77 as well as new C++ \SU{3} libraries have the following order of \SU{3}$\supset$\SO{3} Wigner coefficients:
\begin{displaymath}
\RedCG{\omega_{f}k_{f}L_{f}}{\omega_{i}k_{i}L_{i}}{\omega_{0}k_{0}L_{0}}_{\rho_{0}}\left[k_{0}\right]\left[k_{i}\right]\left[k_{f}\right]\left[\rho_{0}\right].
\end{displaymath}
To iterate over Wigner coefficients in order in which they are stored in memory, we first need rewrite the formula as:
\begin{multline}
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger}_{n^{\pi}_{1}l^{\pi}_{1}j^{\pi}_{1}}
a^{\dagger}_{n^{\nu}_{2}l^{\nu}_{2}j^{\nu}_{2}}
\right\}^{J_{1}}
\left\{
\tilde{a}_{n^{\nu}_{3}l^{\nu}_{3}j^{\nu}_{3}}
\tilde{a}_{n^{\pi}_{4}l^{\pi}_{4}j^{\pi}_{4}}
\right\}^{J_{2}}
\right]^{J_{0}}
}
{J_{i}}
 = 
(-1)^{n_{3}^{\nu} + n_{4}^{\pi}}\Pi_{J_{1}J_{2}}
\Pi_{j^{\pi}_{1}j^{\nu}_{2}j^{\nu}_{3}j^{\pi}_{4}}
\\
\sum_{
\begin{array}{c}
\omega_{f}\omega_{i} \\
\omega_{0} 
\end{array}
}
\sum_{
\begin{array}{c}
S_{f} S_{i} \\ 
S_{0}
\end{array}
}
\sum_{k_{0}L_{0}}
\Pi_{S_{f}S_{i}S_{0}L_{0}}
\sum_{\rho_{0}}
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger(n_{1}^{\pi}0)}_{\frac{1}{2}}
a^{\dagger(n^{\nu}_{2} 0)}_{\frac{1}{2}}
\right\}^{\omega_{f}}_{S_{f}}
\left\{
\tilde{a}^{(0\,n^{\nu}_{3})}_{\frac{1}{2}}
\tilde{a}^{(0\,n^{\pi}_{4})}_{\frac{1}{2}}
\right\}^{\omega_{i}}_{S_{i}}
\right]^{\rho_{0}\omega_{0}S_{0}}_{k_{0}L_{0}J_{0}}
}
{J_{i}}
\\
\sum_{L_{f} L_{i}}
\Pi_{L_{f}L_{i}}
\Wigninej
{L_{f}}{L_{i}}{L_{0}}
{S_{f}}{S_{i}}{S_{0}}
{J_{1}}{J_{2}}{J_{0}}
\Wigninej
{l_{1}^{\pi}}{\half}{j_{1}^{\pi}}
{l_{2}^{\nu}}{\half}{j_{2}^{\nu}}
{L_{f}}{S_{f}}{J_{1}}
\Wigninej
{l_{3}^{\nu}}{\half}{j_{3}^{\nu}}
{l_{4}^{\pi}}{\half}{j_{4}^{\pi}}
{L_{i}}{S_{i}}{J_{2}} 
\\
\sum_{k_{i}} 
\RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}k_{i}L_{i}}
\sum_{k_{f}}
\RedCG{\omega_{f}k_{f}L_{f}}{\omega_{i}k_{i}L_{i}}{\omega_{0}k_{0}L_{0}}_{\rho_{0}} 
\RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}k_{f}L_{f}}
\end{multline}
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% I M P L E M E N T A T I O N %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Important datastructures}
\subsection{\SU{3} tbdmes storage}
\SU{3} tbdmes,
\begin{displaymath}
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger({\color{orange}n_{1}^{\pi}}\, 0)}_{\frac{1}{2}}
a^{\dagger({\color{orange}n^{\nu}_{2}} 0)}_{\frac{1}{2}}
\right\}^{{\color{blue}\omega_{f}}}_{{\color{teal}S_{f}}}
\left\{
\tilde{a}^{(0\,{\color{orange}n^{\nu}_{3}})}_{\frac{1}{2}}
\tilde{a}^{(0\,{\color{orange}n^{\pi}_{4}})}_{\frac{1}{2}}
\right\}^{{\color{blue}\omega_{i}}}_{{\color{teal}S_{i}}}
\right]^{\rho_{0}{\color{blue}\omega_{0}}{\color{teal}S_{0}}}_{k_{0}L_{0}{\color{orange}J_{0}}}
}
{J_{i}},
\end{displaymath}
are stored in the following datastructure:
\begin{lstlisting}
using N1N2N3N4JJ0_TBDMES = std::map<N1N2N3N4JJ0, WFWIW0_6SPINS_TBDMES>
\end{lstlisting}
where the key of this map is an array of 5 integer numbers with the following meaning:
\begin{eqnarray*}
\text{\texttt{N1N2N3N4JJ0}}&\rightarrow &\left\{{\color{orange}n_{1}^{\pi}}, {\color{orange}n_{2}^{\nu}}, {\color{orange}n_{3}^{\nu}}, {\color{orange}n_{4}^{\pi}}, 2{\color{orange}J_{0}}\right\} 
\end{eqnarray*}
The definition of value datatype is following:
\begin{lstlisting}
using WFWIW0_6SPINS_TBDMES = std::map<WFWIW0, std::array<TBDMES, 6>>
\end{lstlisting}
The key are \SU{3} labels 
\begin{eqnarray*}
\text{\texttt{WFWIW0}}&\rightarrow &\left\{{\color{blue}\omega_{f}}, {\color{blue}\omega_{i}}, {\color{blue}\omega_{0}}\right\} 
\end{eqnarray*}
and associated value is an array with 6 elements, each corresponding to a different combination of $({\color{teal}S_{f}}, {\color{teal}S_{i}}, {\color{teal}S_{0}}) \in \left\{(0,0,0), (1, 1, 0), (0, 1, 1), (1, 0, 1), (1, 1, 1), (1, 1, 2)\right\}$.
Each element of this array holds 
\begin{lstlisting}
struct TBDMES
{
   std::vector<double> su3_tbdmes;
   std::vector<int> k0l0; 
};
\end{lstlisting}
whose two member variables have the following meaning:
\begin{eqnarray*}
\text{\texttt{su3\_tbdmes}}&\rightarrow &  
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger({\color{orange}n_{1}^{\pi}}\, 0)}_{\frac{1}{2}}
a^{\dagger({\color{orange}n^{\nu}_{2}} 0)}_{\frac{1}{2}}
\right\}^{{\color{blue}\omega_{f}}}_{{\color{teal}S_{f}}}
\left\{
\tilde{a}^{(0\,{\color{orange}n^{\nu}_{3}})}_{\frac{1}{2}}
\tilde{a}^{(0\,{\color{orange}n^{\pi}_{4}})}_{\frac{1}{2}}
\right\}^{{\color{blue}\omega_{i}}}_{{\color{teal}S_{i}}}
\right]^{{\color{blue}\omega_{0}}{\color{teal}S_{0}}}_{{\color{orange}J_{0}}}
}
{J_{i}}[k_{0}L_{0}][\vec{\rho}_{0}] \\
\text{\texttt{k0l0}}&\rightarrow &  \left\{k_{0},L_{0},k_{0}',L_{0}',\dots\right\}.
\end{eqnarray*}
Notice that I do not assume a particular order of $L_{0}$ values and \SU{3} tbdmes may not contain all possible $k_{0}\le k_{0}^{\max}$. The size of \texttt{su3\_tbdmes} vector is equal to $\rho_{0}^{\max}\times$\texttt{k0l0.size()}.

\subsection{$\RedCG{\omega_{f}k_{f}L_{f}}{\omega_{i}k_{i}L_{i}}{\omega_{0}k_{0}L_{0}}_{\rho_{0}}$ Wigner coefficients}
The coupling coefficients are stored in structure
\begin{displaymath}
\text{\texttt{std::map<}}\left\{{\color{blue}\omega_{f}},{\color{blue}\omega_{i}},{\color{blue}\omega_{0}}\right\}\text{\texttt{, vector<SU3CGS>>}}
\end{displaymath}
Structure \texttt{vector<SU3CGS>} is indexed by the orbital angular momentum $L_{0}$, that is, 
\begin{displaymath}
\text{\texttt{vector<SU3CGS>}}\left[{\color{teal}L_{0}}\right]\rightarrow  \RedCG{{\color{blue}\omega_{f}}k_{f}L_{f}}{{\color{blue}\omega_{i}}k_{i}L_{i}}{{\color{blue}\omega_{0}}k_{0}{\color{teal}L_{0}}}_{\rho_{0}}.
\end{displaymath}
Therefore, the size of vector is set to $\lambda_{0}+\mu_{0}+1$, as $L_{0}^{\max}=\lambda_{0}+\mu_{0}$. If $L_{0} \notin \omega_{0}$, then
associated element is an empty datastructure. The structure \texttt{SU3CGS} holds coupling coefficients and their ${L_{f}, L_{i}}$ quantum numbers 
\begin{lstlisting}
struct SU3CGS
{
   std::vector<double> su3cgs;
   std::vector<LFLI> lfli_vec;
};
\end{lstlisting}
This means that for a given $\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$ and $L_{0}$, its member variables have following meaning:
\begin{eqnarray*}
\text{\texttt{su3cgs}} &\rightarrow& \left\{ \RedCG{{\color{blue}\omega_{f}}k_{f} L_{f}}{{\color{blue}\omega_{i}}k_{i}L_{i}}{{\color{blue}\omega_{0}}k_{0}{\color{teal}L_{0}}}_{\rho_{0}} \right\}
\equiv
\RedCG{{\color{blue}\omega_{f}}}{{\color{blue}\omega_{i}}}{{\color{blue}\omega_{0}}{\color{teal}L_{0}}}[L_{f}L_{i}][k_{0}][k_{i}][k_{f}][{\rho_{0}}] \\
\text{\texttt{lfli\_vec}}&\rightarrow& \left\{ \left\{L_{f}L_{i}\right\}, \left\{L_{f}'L_{i}'\right\},\dots\right\}
\end{eqnarray*}
It can be seen that each possible $L_{f} \times L_{i} \rightarrow {\color{teal}L_{0}}$ pair there are associated $k_{0}^{\max}k_{i}^{\max}k_{f}^{\max}\rho_{0}^{\max}$ Wigner coefficients.
The loops in the algorithm are ordered in such a way that one iterates from first to the last element of Wigner coefficients.
The Wigner coefficients and $\left\{L_{f},L_{i}\right\}$ pairs are generated by method
\begin{displaymath}
\text{\texttt{ComputeSU3CGs(}} \left\{{\color{blue}\omega_{f}}, {\color{blue}\omega_{i}}, {\color{blue}\omega_{0}}\right\}, {\color{teal}L_{0}})
\end{displaymath}
Note that this method computes coefficients for all $L_{f}\in \omega_{f}$ and $L_{i}\in \omega_{i}$ values. Therefore while iterating over $L_{f} L_{i}$ pairs, one needs to select only those that satisfy $l_{1}\times l_{2} \rightarrow L_{f}$ and $l_{3}\times l_{4} \rightarrow L_{i}$.
\subsection{$\RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}k_{f}L_{f}}$ and $\RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}k_{i}L_{i}}$ Wigner coefficients}
These simple Wigner coefficients are computed for fixed pair of values $l_{1}^{\pi}, l_{2}^{\nu}$ and  $l_{3}^{\nu}, l_{4}^{\pi}$, respectively. They are represented by structure
\begin{lstlisting}
struct SU3CGs_Simple
{
   std::vector<double> su3cgs_L3K3;
   std::vector<size_t> index;
};
\end{lstlisting}
where the two member variables have the following meaning:
\begin{eqnarray*}
\text{\texttt{su3cgs\_L3K3}} &\rightarrow& \left\{\RedCG{(\lambda_{1}\,\mu_{1})L_{1}}{(\lambda_{2}\,\mu_{2})L_{2}}{(\lambda_{3}\,\mu_{3})k_{3}L_{3}}\right\}
\equiv
\RedCG{(\lambda_{1}\,\mu_{1})L_{1}}{(\lambda_{2}\,\mu_{2})L_{2}}{(\lambda_{3}\,\mu_{3})}[L_{3}][k_{3}] \\
\text{\texttt{index}}[L_{3}]&\rightarrow& \text{index of element } \RedCG{(\lambda_{1}\,\mu_{1})L_{1}}{(\lambda_{2}\,\mu_{2})L_{2}}{(\lambda_{3}\,\mu_{3})}[L_{3}][0] \text{ in \texttt{su3cgs\_L3K3} vector.}
\end{eqnarray*}
It is important to note that \texttt{su3cgs\_L3K3} has $\lambda_{3}+\mu_{3}+1$ elements. Note that if $L_{3}\notin (\lambda_{3}\,\mu_{3})$ or $|L_{1}-L_{2}| > L_{3} > L_{1}+L_{2}$ then \texttt{index}[$L_{3}$] is equal to -1.
It is assumed coupling $(\lambda_{1}\,\mu_{1})\times (\lambda_{2}\,\mu_{2})\rightarrow (\lambda_{3}\,\mu_{3})$ has multiplicity $\rho_{3}^{\max}=1$, $k_{1}^{\max}=1$, and $k_{2}^{\max}=1$.
Obviously, this structure enables one to quickly access Wigner coefficient with a given $L_{3}$ value and $k_{3}=0$,
\begin{displaymath}
\text{\texttt{su3cgs\_L3K3[index[}}L_{3}\text{\texttt{]]}} = \RedCG{(\lambda_{1}\,\mu_{1})L_{1}}{(\lambda_{2}\,\mu_{2})L_{2}}{(\lambda_{3}\,\mu_{3})k_{3}=0L_{3}}
\end{displaymath}
The Wigner coefficients are generated by method
\begin{displaymath}
\text{\texttt{ComputeSU3CGs}}(\lambda_{1}, \mu_{1}, L_{1}, \lambda_{2}, \mu_{2}, L_{2}, \lambda_{3},\mu_{3}).
\end{displaymath}
\subsection{Transformation implementation}
Implementation uses the following naming convention:
\begin{eqnarray*}
\text{\texttt{sum\_kf}}[\vec{\rho}_{0}] &=& \sum_{k_{f}} \RedCG{\omega_{f}k_{f}L_{f}}{\omega_{i}k_{i}L_{i}}{\omega_{0}k_{0}L_{0}}[\vec{\rho}_{0}] \RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}k_{f}L_{f}} \\
\text{\texttt{sum\_kikf}}[\vec{\rho}_{0}] &=& \sum_{k_{i}} \RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}k_{i}L_{i}}\times \text{\texttt{sum\_kf}}[\vec{\rho}_{0}] \\
\text{\texttt{sum\_LfLi}}[\vec{\rho}_{0}] &=& \sum_{L_{f} L_{i}} \Pi_{L_{f}L_{i}} \Wigninej {L_{f}}{L_{i}}{L_{0}} {S_{f}}{S_{i}}{S_{0}} {J_{1}}{J_{2}}{J_{0}} \Wigninej {l_{1}^{\pi}}{\half}{j_{1}^{\pi}} {l_{2}^{\nu}}{\half}{j_{2}^{\nu}} {L_{f}}{S_{f}}{J_{1}} \Wigninej {l_{3}^{\nu}}{\half}{j_{3}^{\nu}} {l_{4}^{\pi}}{\half}{j_{4}^{\pi}} {L_{i}}{S_{i}}{J_{2}}\times \text{\texttt{sum\_kikf}}[\vec{\rho}_{0}]  \\
\text{\texttt{sum\_rho0}} &=& \sum_{\rho_{0}}
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger(n_{1}^{\pi}0)}_{\frac{1}{2}}
a^{\dagger(n^{\nu}_{2} 0)}_{\frac{1}{2}}
\right\}^{\omega_{f}}_{S_{f}}
\left\{
\tilde{a}^{(0\,n^{\nu}_{3})}_{\frac{1}{2}}
\tilde{a}^{(0\,n^{\pi}_{4})}_{\frac{1}{2}}
\right\}^{\omega_{i}}_{S_{i}}
\right]^{\rho_{0}\omega_{0}S_{0}}_{k_{0}L_{0}J_{0}}
}
{J_{i}}\times \text{\texttt{sum\_LfLi}}[\rho_{0}] \\
\text{\texttt{su2\_tbdme}} &=& 
\sum_{
\begin{array}{c}
\omega_{f}\omega_{i} \\
\omega_{0} 
\end{array}
}
\sum_{
\begin{array}{c}
S_{f} S_{i} \\ 
S_{0}
\end{array}
}
\sum_{k_{0}L_{0}}
\Pi_{S_{f}S_{i}S_{0}L_{0}}
\times
\text{\texttt{sum\_rho0}}
\end{eqnarray*}
and hence
\begin{equation}
\RedME{J_{f}}
{
\left[
\left\{
a^{\dagger}_{n^{\pi}_{1}l^{\pi}_{1}j^{\pi}_{1}}
a^{\dagger}_{n^{\nu}_{2}l^{\nu}_{2}j^{\nu}_{2}}
\right\}^{J_{1}}
\left\{
\tilde{a}_{n^{\nu}_{3}l^{\nu}_{3}j^{\nu}_{3}}
\tilde{a}_{n^{\pi}_{4}l^{\pi}_{4}j^{\pi}_{4}}
\right\}^{J_{2}}
\right]^{J_{0}}
}
{J_{i}}
 = \\
(-1)^{n_{3}^{\nu} + n_{4}^{\pi}}\Pi_{J_{1}J_{2}}
\Pi_{j^{\pi}_{1}j^{\nu}_{2}j^{\nu}_{3}j^{\pi}_{4}}
\times
\text{\texttt{su2\_tbdme}}
\end{equation}

\newpage
\subsection{Algorithm}
\begin{lstlisting}
su3_tbdmes.find($\left\{n_{1},n_{2},n_{3},n_{4}, J_{0}\right\}$)$\rightarrow$ map<$\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$, TBDMES[6]>
iterate over elements of map<$\left\{\omega_{f},\omega_{i},\omega_{0}\right\}$, TBDMES[6]>$\color{blue}\rightarrow\sum_{\omega_{f}\omega_{i}\omega_{0}}$
{
  if $\omega_{f}$ new then $\forall L_{f}\in \omega_{f}$ compute $\RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}k_{f}L_{f}}$
  if $\omega_{i}$ new then $\forall L_{i}\in \omega_{i}$ compute $\RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}k_{i}L_{i}}$
  find vector<{$\RedCG{\omega_{f}}{\omega_{i}}{\omega_{0}}$}> in map<$\left\{\omega_{f}, \omega_{i}, \omega_{0}\right\}$, vector<{$\RedCG{\omega_{f}}{\omega_{i}}{\omega_{0}}$}>>
  iterate over $0\le$iSpin$\le5$$\color{blue}\rightarrow \sum_{S_{f}S_{i}S_{0}}$
  {
     iterate over $k_{0}L_{0} \in$ TBDMES[iSpin].k0l0$\color{blue}\rightarrow\sum_{k_{0}L_{0}}$
     {
        if {$\RedCG{\omega_{f}}{\omega_{i}}{\omega_{0}}$}[$L_0$] empty $\Rightarrow$ compute {$\RedCG{\omega_{f}}{\omega_{i}}{\omega_{0}}$}[$L_{0}$]
        sum_LfLi[$\vec{\rho}_{0}$]=0
        index_LfLi = 0
        iterate over $\left\{L_{f}L_{i}\right\}\in${$\RedCG{\omega_{f}}{\omega_{i}}{\omega_{0}}$}[$L_0$], index_LfLi +=$k_{0}^{\max}*k_{i}^{\max}*k_{f}^{\max}*\rho_{0}^{\max}$ $\color{blue}\rightarrow\sum_{L_{f}L_{i}}$
        {
           $k_{f}^{\max}$=kmax($\omega_{f},L_{f}$)
           $k_{i}^{\max}$=kmax($\omega_{i},L_{i}$)
           if !($l_{1}\times l_{2}\rightarrow L_{f}$) || !($l_{3}\times l_{4}\rightarrow L_{i}$) $\Rightarrow$ continue
           if !($L_{f}\times S_{f}\rightarrow J_{1}$) || !($L_{i}\times S_{i}\rightarrow J_{2}$) $\Rightarrow$ continue
           get_first_coeff($L_{f}$)$\rightarrow \RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}}[L_{f}][k_{f}=0]$
           get_first_coeff($L_{i}$)$\rightarrow \RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}}[L_{i}][k_{i}=0]$
           index_LfLik0 = index_LfLi + $k_{0}*k_{i}^{\max}*k_{f}^{\max}*\rho_{0}^{\max}$
           index_LfLik0 $\rightarrow \RedCG{\omega_{f}}{\omega_{i}}{\omega_{0}L_{0}}[L_{f}L_{i}][k_{0}][k_{i}=0][k_{f}=0][\rho_{0}=0]$
           sum_kfki[$\vec{\rho}_{0}$]=0
           iterate over $0 \le k_{i} < k_{i}^{\max}$$\color{blue}\rightarrow \sum_{k_{i}}$
           {
              sum_kf[$\vec{\rho}_{0}$]=0
              iterate over $0 \le k_{f} < k_{f}^{\max}$$\color{blue}\rightarrow \sum_{k_{f}}$
              {
                 sum_kf[$\vec{\rho}_{0}$]+=$\RedCG{\omega_{f}k_{f}L_{f}}{\omega_{i}k_{i}L_{i}}{\omega_{0}k_{0}L_{0}}[\vec{\rho}_{0}] \RedCG{(n^{\pi}_{1}\,0) l^{\pi}_1}{(n^{\nu}_{2}\,0) l^{\nu}_{2}}{\omega_{f}k_{f}L_{f}}$
              }
              sum_kfki[$\vec{\rho}_{0}$]+=$\RedCG{(0\,n^{\nu}_{3}) l^{\nu}_{3}}{(0\,n^{\pi}_{4}) l^{\pi}_{4}}{\omega_{i}}[L_{i}][k_{i}]$ sum_kf[$\vec{\rho}_{0}$]
           }
           sum_LfLi[$\vec{\rho}_{0}$]+=$\Pi_{L_{f}L_{i}}\Wigninej{L_{f}}{L_{i}}{L_{0}}{S_{f}}{S_{i}}{S_{0}}{J_{1}}{J_{2}}{J_{0}} \Wigninej{l_{1}^{\pi}}{\half}{j_{1}^{\pi}} {l_{2}^{\nu}}{\half}{j_{2}^{\nu}}{L_{f}}{S_{f}}{J_{1}} \Wigninej{l_{3}^{\nu}}{\half}{j_{3}^{\nu}}{l_{4}^{\pi}}{\half}{j_{4}^{\pi}}{L_{i}}{S_{i}}{J_{2}}$sum_kfki[$\vec{\rho}_{0}$]
        }
        sum_rho0=$\sum_{\rho_{0}} \RedME{J_{f}} { \left[ \left\{ a^{\dagger(n_{1}^{\pi}0)}_{\frac{1}{2}} a^{\dagger(n^{\nu}_{2} 0)}_{\frac{1}{2}} \right\}^{\omega_{f}}_{S_{f}} \left\{ \tilde{a}^{(0\,n^{\nu}_{3})}_{\frac{1}{2}} \tilde{a}^{(0\,n^{\pi}_{4})}_{\frac{1}{2}} \right\}^{\omega_{i}}_{S_{i}} \right]^{\rho_{0}\omega_{0}S_{0}}_{k_{0}L_{0}J_{0}} } {J_{i}}\times \text{\texttt{sum\_LfLi}}[\rho_{0}]$
        su2_tbdme+=$\Pi_{S_{f}S_{i}S_{0}}\Pi_{L_{0}}$sum_rho0
     }
  }
}
result=$(-)^{n_{3}^{\nu}+n_{4}^{\pi}}\Pi_{J_{1}J_{2}}\Pi_{j_{1}j_{2}j_{3}j_{4}}$*su2_tbdme 
\end{lstlisting}
\end{document}
