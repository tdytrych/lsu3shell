\documentclass[11pt,a4paper,oneside]{report}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}
\usepackage{amssymb}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}
\renewcommand*{\marginfont}{\color{red}}

\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}

\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}
\newcommand{\half}[0]{\frac{1}{2}}

\definecolor{lightgray}{gray}{0.97}
\definecolor{forestgreen}{RGB}{0,128,0}
\definecolor{navyblue}{RGB}{0,0,128}
\definecolor{brick}{RGB}{128,0,0}
\definecolor{prepro}{RGB}{128,0,128}

\lstset{language=[77]Fortran,
  basicstyle=\ttfamily,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  mathescape=true,
  morecomment=[l]{!\ }% Comment only with space after !
}

\begin{document}
\section*{\texttt{Generate\_adadtata\_SU2list}}
Generate input for \texttt{TBDMEsSU3toSU2} code, which transforms \SU{3}$\times$\SU{2} TBDMEs to the target \SU{2} TBDMEs. 

\texttt{Generate\_adadtata\_SU2list} code prints out a list of \SU{2} two-body tensors for proton-neutron and two-identical-particle cases.
Namely, it prints quantum labels
\begin{equation}
n_1 \, l_{1}\, 2j_{1} \quad n_{2}\,l_{2}\,2j_{2} \quad 2J_{f} \quad n_3\, l_{3}\, 2j_{3} \quad n_4\, l_{4}\, 2j_{4} \quad 2J_{i} \quad 2J_{0}
\end{equation}
In the case of proton-neutron system this would represent tensor
\begin{displaymath}
\left[
\left\{
a^{\dagger}_{n^{\pi}_{1}l^{\pi}_{1}j^{\pi}_{1}}
a^{\dagger}_{n^{\nu}_{2}l^{\nu}_{2}j^{\nu}_{2}}
\right\}^{J_{1}}
\left\{
\tilde{a}_{n^{\nu}_{3}l^{\nu}_{3}j^{\nu}_{3}}
\tilde{a}_{n^{\pi}_{4}l^{\pi}_{4}j^{\pi}_{4}}
\right\}^{J_{2}}
\right]^{J_{0}}.
\end{displaymath}
\noindent
{\color{red}NOTE:} For two-identical-particle tensors, code generates only tensors with $n_{1} \le n_{2}$ and $n_{3} \le n_{4}$ as other combinations can be obtained from this set by interchanging order of creation/annihilation operators accompanied by multiplying with a corresponding phase. It is easy to tweak the code to generate tensors for all possible combinations of harmonic oscillator shells.
\medskip

\noindent
{\color{red}NOTE:} If $n_{1}=n_{2}$ or $n_{3}=n_{4}$, code generates only antisymmetric \SU{2} tensors, that is, $J_{f}$ or $J_{i}$ can be even numbers only. \\
\section*{Input Parameters}
\texttt{Generate\_adadtata\_SU2list} has following input parameters:
\begin{displaymath}
\texttt{
<type> <valence shell> <Nmax> <parity> <list of selected }2J_{0}\texttt{>}
\end{displaymath}

\noindent
{\bf Input Parameters:}
\begin{itemize}
\item 
\texttt{<type>}: \\
PN $\dots$ proton-neutron case; PPNN $\dots$ two identical nucleons.
\item 
\texttt{<valence shell>}: harmonic oscillator shell number of a valence shell.
\item 
\texttt{<Nmax>}: model space for which a given list of tensors are active. 
\item
\texttt{<parity>}: \\
$+1$ $\dots$ positive parity operators; $-1$ $\dots$ negative parity operators
\item
\texttt{<list of selected } $2J_{0}$\texttt{>}:  List of selected 2$J_{0}$ values; -1 to generate all possible $J_{0}$ values.
\end{itemize}
\section*{Example}
To generate positive parity two-nucleon \SU{2} tensors valid for $p$-shell nuclei in $N_{\max}=4$ model space and $J_{0}=0$:
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_SU2list PPNN 1 4 1 0}}
\end{displaymath}
To generate negative parity proton-neutron \SU{2} tensors valid for $p$-shell nuclei in $N_{\max}=4$  model space and $J_{0}=0, 1$ values:
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_SU2list PN 1 4 -1 0 2}}
\end{displaymath}

\section*{Testing}
Note that the number of generated proton-neutron and two-nucleon \SU{2} tensors was compared with the number of their \SU{3} counterparts and in both cases codes generate the same number of tensors. 
\end{document}
