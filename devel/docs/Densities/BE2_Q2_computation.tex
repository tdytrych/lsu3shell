\documentclass[11pt,a4paper,oneside]{article}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}
\usepackage{xcolor}
\usepackage{mathtools}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\IR}[1]{\left(\lambda_{#1}\, \mu_{#1}\right)}
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}

\newcommand{\CG}[6]
	{
	{ C }_{ #1 #2 #3 #4 }^{ #5 #6 }	
	}

\newcommand{\WignerTHREEj}[6]
	{
	\left(
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right)
	}

\newcommand{\WignerSIXj}[6]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6
		\end{array}
	\right\}
	}

\newcommand{\WignerNINEj}[9]
	{
	\left\{
		\begin{array}{ccc}
			#1 & #2 & #3 \\
   			#4 & #5 & #6 \\
   			#7 & #8 & #9
		\end{array}
	\right\}
	}

\newcommand{\half}[0]
	{
	  \frac{ 1 }{ 2 }	
	}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Fast computation of B($E2$) and $Q(J)$}
\author{Tom\'{a}\v{s} Dytrych}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
Suppose one wants to compute B($E2;J_{i}=2_{1}^{+}\rightarrow J_{i}=0_{gs}^{+}$) nd $Q(J_{1}^{+}=2)$ in $^{12}$C and $N_{\max}=4$ model space.
\subsection*{Preliminary step: prepare tables with \SU{3} rmes of one-body operators for a system of $Z$ identical nucleons}
As computation of \SU{3} proton rmes for one-body operators is very fast, we can simply generate a list of all possible one-body \SU{3} tensors relevant for a given $N_{\max}$ model space. To do that, one needs to run:
\medskip

\noindent
\texttt{{\color{blue}Generate\_adta\_SU3list}} \\
\noindent \texttt{ enter valence shell HO number:1} \\
\noindent \texttt{ enter Nmax:4} \\
\noindent \texttt{ Parity (1 ... positive, -1 ... negative, 0 ... both parities):0} \\
\noindent \texttt{ Select tensors that have specific J0 value. Enter 2J0 or -1 to include all tensors:-1} \\
\noindent \texttt{ Generate recoupled one-body tensors for ComputeMultiShellRecoupledRME (1 ... yes):1} \\
\medskip

\noindent
\texttt{Generate\_adta\_SU3list} will generate two files
\begin{itemize}
\item \texttt{1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors} \\
\noindent
List of $n_{a}\,n_{b}\,\lambda_{0}\,\mu_0\,2S_{0}$ quantum numbers of all possible \SU{3} one-body tensors $\left[a^{\dagger (n_{a}\,0)}_{\half}\times \tilde{a}^{(0\,n_{b})}_{\half}\right]^{(\lambda_{0}\,\mu_{0})}_{S_{0}}$ that are relevant for $p$-shell nuclei and $N_{\max}=4$ model space.
\item \texttt{1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors.Recoupled} \\ 
\noindent Contains list of target tensors expressed in HO recoupled form.
\end{itemize}
\medskip

\noindent
In the next step, one-body rmes for $Z=6$ identical nucleons in $N_{\max}=4$ model space are computed. \\

\noindent \texttt{{\color{blue}ComputeMultiShellRecoupledRMEs} 6 4 1 } \texttt{1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors.Recoupled}
\medskip

\noindent
At this moment we have complete set of one-body rmes for $Z=6$ nucleons and can therefore evaluate any observable expressible in terms of proton one-body operators.
\medskip

\noindent
However, in our case we are interested only in tensors that occur in $\hat{Q}$ tensor operators. We will therefore select only tensors relevant for $\hat{Q}$ tensor operator and store them in file \texttt{q2.su3\_a+ta\_tensors}:
\medskip

\noindent
\texttt{{\color{blue}grep} '2 0 0\$\textbackslash\textbar 0 2 0\$\textbackslash\textbar1 1 0\$' 1vshell\_4Nmax\_both\_parities.su3\_a+ta\_tensors > Q2.su3\_a+ta\_tensors}
\medskip

\noindent

\subsection*{Computing B($E2$)}
\begin{enumerate}
\item \texttt{{\color{blue}EMObservables2SU3Tensors} 1 5} \\
This will generate \SU{3} tensorial decomposition of $Q_{L=2 \mu=0}$ operator with the
highest HO shell $\eta_{\max}=5$, which corresponds to $p$-shell and $N_{\max}=4$.
\item	Edit \texttt{Q2full\_1b\_nmax8\_mu0} file and add \texttt{4 0} as the first line for \texttt{BE2}.
\item \texttt{{\color{blue}su3denseP} 12C\_Nmax04\_JJ0.model\_space JJ0/eigenvector01\_ndiag1.dat 12C\_Nmax04\_JJ4.model\_space JJ4/eigenvector01\_ndiag1.dat 1 Q2.su3\_a+ta\_tensors 2} \\
\noindent Thiw will create \texttt{Q2.su3\_a+ta\_tensors.su3\_pp\_obdmes}, which stores resulting OBDMEs.
\item Edit the first line of file \texttt{Q2.su3\_a+ta\_tensors.su3\_pp\_obdmes} so that it contains the number of obds.
\item 
\texttt{{\color{blue}transitions\_BE2\_BM1\_obds} BE2 Q2full\_1b\_nmax8\_mu0 \texttt{Q2.su3\_a+ta\_tensors.su3\_pp\_obdmes}}
\end{enumerate}

\subsection*{Computing $Q(J)$}
First two steps are common with computing B($E2$).
\begin{enumerate}
\item \texttt{{\color{blue}su3denseP} 12C\_Nmax04\_JJ4.model\_space JJ4/eigenvector01\_ndiag1.dat 12C\_Nmax04\_JJ4.model\_space JJ4/eigenvector01\_ndiag1.dat 1 Q2.su3\_a+ta\_tensors 2} \\
\noindent Thiw will create \texttt{Q2.su3\_a+ta\_tensors}, which stores resulting OBDMEs.
\item Edit the first line of file \texttt{Q2.su3\_a+ta\_tensors.su3\_pp\_obdmes} so that it contains the number of obds.
\item 
\texttt{{\color{blue}rmsE2M1observables\_obds} Q2.su3\_a+ta\_tensors.su3\_pp\_obdmes Q2full\_1b\_nmax8\_mu0}
\end{enumerate}
Resulting runtimes for a single process are following:
\begin{center}
\begin{tabular}{|c|c|c|c|} 
 \hline
          & \texttt{\color{blue}su3denseP} & \texttt{\color{blue}ObdSU3Calculator\_MPI\_OpenMP} & speedup \\ 
 \hline
 \hline
 B($E2;2^{+}_{1}\rightarrow 0^{+}_{\text{gs}}$) & 0.8s & 1854s & 2317 \\
 $Q(J=2^{+}_{1})$ & 1.3s & 2701s & 2077 \\
 \hline
\end{tabular}
\end{center}
\section*{Preparing Input Wave Functions}
It is important to note that \texttt{su3densePN\_adadtata\_pnnp} assumes that input wave functions have order of basis states given by \texttt{NDIAG=1} parameter. However, large-scale \texttt{LSU3shell} computations have \texttt{NDIAG}$>1$. Therefore, one needs to use code \texttt{wfn2ndiag} to generate \texttt{su3denseP} compatible wave functions from eigenstates of \texttt{LSU3shell}. \\
\noindent
\textbf{Example:} \\
\noindent
Suppose that $^{12}$C complete $N_{\max}=4$ space wave function ($J=4$) \texttt{eigenvector01.dat} was generated with \texttt{NDIAG=3}. To generate wave function ready for \texttt{su3denseP}, one needs to run: \\
\vspace{0.1cm}
\\
\noindent
\texttt{wfn2ndiag 12C\_Nmax04\_JJ0.model\_space eigenvector01.dat 3 eigenvector01\_ndiag1.dat 1}

\end{document}
