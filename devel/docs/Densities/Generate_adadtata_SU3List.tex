\documentclass[11pt,a4paper,oneside]{report}

\usepackage{IEEEtrantools}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{braket}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}
\usepackage{amssymb}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}
\renewcommand*{\marginfont}{\color{red}}

\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}

\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}
\newcommand{\half}[0]{\frac{1}{2}}

\definecolor{lightgray}{gray}{0.97}
\definecolor{forestgreen}{RGB}{0,128,0}
\definecolor{navyblue}{RGB}{0,0,128}
\definecolor{brick}{RGB}{128,0,0}
\definecolor{prepro}{RGB}{128,0,128}

\lstset{language=[77]Fortran,
  basicstyle=\ttfamily,
  keywordstyle=\color{red},
  commentstyle=\color{blue},
  mathescape=true,
  morecomment=[l]{!\ }% Comment only with space after !
}

\begin{document}
\section*{\texttt{Generate\_adadtata\_SU3list}}
This code generates a list of quantum labels of \SU{3}$\times$\SU{2} two-body tensors (proton-proton and neutron-neutron) compatible with \texttt{GenerateMultiShellRMEs\_adadtata} and \texttt{su3denseP} codes:
\begin{equation}
n_1 \quad n_2 \quad n_3 \quad n_4 \quad \lambda_f \quad \mu_f \quad 2S_f \quad \lambda_i \quad \mu_i \quad 2S_i \quad \lambda_0 \quad \mu_0 \quad 2S_0 
\end{equation}
{\color{red}NOTE:} Only two-nucleon tensors with $n_{1} \le n_{2}$ and $n_{3} \le n_{4}$ are generated. All other combinations can be obtained from this set by multiplying with a corresponding phase. It is easy to tweak the code to generate all combinations 
of HO shells.
\medskip

\noindent
{\color{red}NOTE:} If $n_{1}==n_{2}$ or $n_{3}==n_{4}$, code generates only antisymmetric same-shell \SU{3}$\times$\SU{2} tensors, that is, those with $\lambda_{f}+\mu_{f}+S_{f}$ or $\lambda_{i}+\mu_{i}+S_{i}$ equal to an even number.  \\
\medskip

\noindent
{\color{red}NOTE:} Currently only \SU{3}$\times$\SU{2} tensors $(\lambda_{0}\,\mu_{0})S_{0}$ that contain $J_{0}=0$ are generated as only they are needed for a realistic Hamiltonian. I plan to remove this constraint once the new version of \texttt{ComputeMultiShellRecoupledRMEs} is able to handle a general tensor.
\section*{Input Parameters}
\texttt{Generate\_adadtata\_SU3list} has following input parameters:
\begin{displaymath}
\texttt{
 <valence shell> <Nmax> <parity> <SU3 selection>
}
\end{displaymath}

\noindent
{\bf Input Parameters:}
\begin{itemize}
\item 
\texttt{<valence shell>}: harmonic oscillator shell number of a valence shell.
\item 
\texttt{<Nmax>}: model space for which a given list of tensors are active. 
\item
\texttt{<parity>}: \\
$+1$ $\dots$ positive parity operators; $-1$ $\dots$ negative parity operators
\item
\texttt{<SU3 selection>}: \\ 
0 $\dots$ select tensors with $\lambda_{0}+\mu_{0}$ even number (e.g. interaction); 1 $\dots$  select tensors with $\lambda_{0}+\mu_{0}$ odd number; -1 $\dots$ select all tensors.
\end{itemize}
\section*{Example}
To generate positive parity two-nucleon tensors valid for $p$-shell nuclei in $N_{\max}=4$  model space and select those with $\lambda_{0}+\mu_{0}$ even number:
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_SU3list 1 4 1 0}}
\end{displaymath}
\section*{Testing}
If one uncomments the last line of the code, the number of generated two nucleon $\SU{3}\times\SU{2}$ tensors with $J_{0}=0$ will be printed out. This number, when generated with \texttt{<SU3 selection>=-1}, must be equal to the number of two-nucleon \SU{2} tensors generated for the same input parameters for $J_{0}=0$.
For example, 
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_SU3list 1 4 1 -1}}
\end{displaymath}
will print out $7144$ tensors. At the same time,
\begin{displaymath}
\text{\texttt{Generate\_adadtata\_SU2list PPNN 1 4 1 0 | wc -l}}
\end{displaymath}
prints out $7144$ $J_{0}=0$ tensors as well.
\end{document}
