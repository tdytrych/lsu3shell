\documentclass[11pt,a4paper,oneside]{report}

\usepackage[left=12mm,right=32mm,top=25mm,bottom=25mm,includefoot]{geometry}

\usepackage[
	colorlinks,
	bookmarksnumbered
]{hyperref}

\usepackage{color}
\usepackage{pdflscape}
\usepackage{layout}
\usepackage{listings}
\usepackage{relsize}
\usepackage[dvipsnames]{xcolor}
\usepackage[backgroundcolor=Dandelion]{todonotes}
\usepackage{amsmath}
\usepackage{alltt}
\usepackage{fancyvrb}

\usepackage{marginnote}
\renewcommand*{\marginfont}{\color{red}}

%\usepackage[notref,notcite,color]{showkeys}
%\definecolor{labelkey}{rgb}{1,0.4,1}
%\renewcommand*\showkeyslabelformat[1]{\fbox{\normalfont\scriptsize\ttfamily#1}}

%\input defs.tex

%\allowdisplaybreaks

\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

%\reversemarginpar

% lstlistings options

\definecolor{lightgray}{gray}{0.97}
\definecolor{forestgreen}{RGB}{0,128,0}
\definecolor{navyblue}{RGB}{0,0,128}
\definecolor{brick}{RGB}{128,0,0}
\definecolor{prepro}{RGB}{128,0,128}

\lstset{%
  basicstyle=\ttfamily,
  backgroundcolor=\color{lightgray},
  columns=flexible,
  emph={execute,executor,for_id,for_id_impl,for_id_impl_1,for_id_impl_2},
  emphstyle=\color{brick},
  escapeinside={{(*}{*)}},
  frame=single,
  framerule=0pt,
  keywordstyle=\color{navyblue},
  commentstyle=\color{forestgreen}\it,
  language=C++,
% lineskip=-1pt,
  mathescape=true,
  morekeywords={defined,uint8_t,uint16_t,uint32_t,uint64_t},
  numberstyle={\footnotesize},
  showstringspaces=false
}

% head

\begin{document}
\section*{Task}
Given a distribution of fermions for initial (\texttt{Ket\_confs}) and final
(\texttt{Bra\_confs}) states over harmonic oscillator (HO) shells
(\texttt{hoShells}), we want to select a group of one- and two-body tensors $\hat{T}$
 that can connect those two distributions:
\begin{displaymath}
\hat{T}\in \left[\left\{a^{+}_{i}a^{+}_{j}a_{k}a_{l}\right\},  \left\{ a^{+}_{i}a_{j}\right\}\right] \quad \text{such that} \quad \text{\texttt{Bra\_confs}}=\hat{T}\times \text{\texttt{Ket\_confs}},
\end{displaymath}
where $i,j,k,l$ denote major harmonic oscillator shell numbers.

\section*{Tensor Groups}
Tensors can be divided into groups according to the number of HO shells they
act on, and according to the number of changes, denoted as $\Delta$, they induce in a
particle distribution.
\begin{enumerate}
\item single-shell 
	\begin{itemize}
		\item $\Delta=0$: $(a^{+}_{i}a^{+}_{i}a_{i}a_{i})$, and $(a^{+}_{i}a_{i})$.
	\end{itemize}
\item two-shells 
	\begin{itemize}
		\item $\Delta=0$: $(a^{+}_{i}a_{i})(a^{+}_{j}a_{j})$.
		\item $\Delta=2$: $(a^{+}_{i}a^{+}_{i}a_{i})(a_{j})$, $(a^{+}_{i})(a^{+}_{j}a_{j}a_{j})$, and $(a^{+}_{i})(a_{j})$.
		\item $\Delta=4$: $(a^{+}_{i}a^{+}_{i})(a_{j}a_{j})$.
	\end{itemize}
\item three-shells 
	\begin{itemize}
		\item $\Delta=2$: $(a^{+}_{i}a_{i})(a^{+}_{k})(a_{l})$.
		\item $\Delta=4$: $(a^{+}_{i})(a^{+}_{j})(a_{k}a_{k})$, $(a^{+}_{i}a^{+}_{i})(a_{k})(a_{l})$.
	\end{itemize}
\item four-shell 
	\begin{itemize}
		\item $\Delta=4$: $(a^{+}_{i})(a^{+}_{j})(a_{k})(a_{l})$.
	\end{itemize}
\end{enumerate}
Please note that while the creation and annihilation operators in tensors are
stored in such an order that HO shells are increasing, the previous table
clearly does not impose this rule since it is irrelevant for this discussion.

\subsection*{Implementation}
{\small
\begin{lstlisting}
class CInteractionPPNN
{
   	std::vector<CTensorGroup*> m_TensorGroups[9];
}
\end{lstlisting}
}
\noindent
Each element of array \texttt{CInteractionPPNN::m\_TensorGroups} contains a group of tensors that act on $n$ shells and have a given $\Delta$.
The index of array is calculated as $\Delta + n$:
{\small
\begin{lstlisting}
// calculate $\Delta$ and number of shells n
int index = $\Delta$ + n;
std::vector<CTensorGroup*> tensors = m_TensorGroups[index];
\end{lstlisting}
}

\section*{Algorithm}
\subsection*{Step 1}
For a given pair \texttt{Bra\_confs} and \texttt{Ket\_confs} particle distributions calculate the number of changes $\Delta$ as
\begin{displaymath}
\Delta=\sum_{i} |\text{\texttt{Bra\_confs[i]}} - \text{\texttt{Ket\_confs[i]}}|.
\end{displaymath}
\medskip
{\textbf{Example:}} \\
\texttt{Bra\_confs}$=\left\{2, 4, 3\right\}$ and \texttt{Ket\_confs}$=\left\{2, 4, 3\right\}$ $\rightarrow \Delta=0$. \\
\texttt{Bra\_confs}$=\left\{2, 4, 0, 2\right\}$ and \texttt{Ket\_confs}$=\left\{1, 4, 1, 2\right\}$ $\rightarrow \Delta=2$. \\
\texttt{Bra\_confs}$=\left\{2, 4, 0, 0\right\}$ and \texttt{Ket\_confs}$=\left\{0, 4, 1, 1\right\}$ $\rightarrow \Delta=4$. \\
\texttt{Bra\_confs}$=\left\{2, 4, 0, 0\right\}$ and \texttt{Ket\_confs}$=\left\{0, 2, 2, 2\right\}$ $\rightarrow \Delta=8$. 
\subsection*{Step 2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{$\Delta>4$}
It is impossible for two-body tensors to connect two distributions and hence we
are done.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{$\Delta=0$}
Iterate over single-shell and $\Delta=0$ two-shells tensors and select those that can couple.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{$\Delta=2$}
Iterate over two-shells and three-shells tensors that have $\Delta=2$ and select those that can couple.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{$\Delta=4$}
In this case the exact type of tensor, i.e. two-shell, three-shell, or
four-shell, that can connect two distributions can be resolved uniquely. One
constructs three arrays \texttt{dA}, \texttt{shellsT}, and
\texttt{nAnnihilators}:
{\small
\begin{lstlisting}
std::vector<unsigned char> nAnnihilators; // number of annihilation operators in each active shell
std::vector<char> dA;
std::vector<unsigned char> shellsT;	//	HO shells with active single-shell tensors.

dA.reserve(Bra_confs.size());
shellsT.reserve(Bra_confs.size());
nAnnihilators.reserve(Bra_confs.size());

for (int i = 0; i < Bra_confs.size(); ++i)
{
    diff = Bra_confs[i] - Ket_confs[i];
    if (diff)
    {
        dA.push_back(diff);
        shellsT.push_back(hoShells[i]);
        nAnnihilators.push_back(abs(diff)*(diff < 0)));
    }
}
\end{lstlisting}
}
\noindent
The selection of a tensor that can couple given distributions is simple:
{\small
\begin{lstlisting}
n = dA.size(); // n == number of shells that a tensor can act on
tensors = tensorStorage[$\Delta$][n]; // Search n-shells tensors with $\Delta=4$
for (int i = 0; i < tensors.size(); ++i)
{
    if (dA == tensors[i].m_dA && shellsT == tensors[i].m_ShellsT \
		&& nAnnihilators == tensors[i].m_nAnnihilators)
    {
        // tensor stored at tensors[i] can connect Ket_confs with Bra_confs
    }
}
\end{lstlisting}
}
\end{document}
