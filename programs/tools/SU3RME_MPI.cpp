/****************************************************************

- 6/9/17 (mac):
  + Add binary rme output.
  + Add global statistics counters.
  + Add basic walltime diagnostics.
  + Add optional mode argument ("count", "text", "binary", ...).
- 6/17/17 (mac):
  + Update binary mode output: add header, shorten indexing
    integer type, make storage single/double switchable.
- 10/19/18 (mac/aem):
  + Upgrade RMEIndexType from unsigned short int to unsigned int.

****************************************************************/

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <LookUpContainers/lock.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <regex>
#include <stack>
#include <stdexcept>
#include <vector>

////////////////////////////////////////////////////////////////
// global mode and statistics variables
////////////////////////////////////////////////////////////////

// output mode
bool g_enable_write = true;
bool g_output_binary = false;
int g_binary_format_code = 1;
int g_binary_float_precision = 8;
typedef unsigned int
    RMEIndexType;  // note: unsigned short int overflows, leading to multiplicity errors

// statistics accumulators
bool g_zero_threshold = 1e-8;  // for output diagnostics
int g_num_tensors = 0;
int g_num_groups_allowed = 0;
int g_num_groups_nonzero = 0;
int g_num_rmes_allowed = 0;
int g_num_rmes_nonzero = 0;

////////////////////////////////////////////////////////////////
// helper functions for binary output
//
//   from io.h
//   https://github.com/nd-nuclear-theory/mcutils.git
//   commit 6e0520f
////////////////////////////////////////////////////////////////

template <typename tDataType>
void WriteBinary(std::ostream& os, tDataType data)
// Write binary data item to stream.
//
// Note that, if the template parameter is omitted, the data type
// of the value given for data will determine the output type, but
// explicitly giving the template parameter casts the data to the
// given data type tDataType.  Explicitly giving the template
// parameter is recommended both to document the data format in
// the output file and to avoid any ambiguity of the output type.
//
// Arguments:
//   os (input): binary stream for output
//   data (input): data value to output
//
// Ex:
//   mcutils::WriteBinary<float>(out_stream,value);
{
   os.write(reinterpret_cast<const char*>(&data), sizeof(data));
}

////////////////////////////////////////////////////////////////
// simple timing with clock()
//
//   from profiling.h
//   https://github.com/nd-nuclear-theory/mcutils.git
//   commit 3e69fb7
////////////////////////////////////////////////////////////////

#include <ctime>

////////////////////////////////////////////////////////////////
// calculation code
////////////////////////////////////////////////////////////////

std::vector<double> ComputeRME(std::vector<MECalculatorData>& rmeCoeffsPNPN)
// Compute RMEs for a single pair of bra and ket irrep subspaces (i.e.,
// subspaces of equivalent irreps, distinguished by upstream quantum numbers),
// for a single operator (e.g., unit tensor).
//
// rmeCoeffsPNPN: expansion of operator in a+a+aa terms
{
   // These values must be constant for each tensor
   int32_t bra_max = rmeCoeffsPNPN[0].rmes_->m_bra_max;
   int32_t ket_max = rmeCoeffsPNPN[0].rmes_->m_ket_max;
   int32_t rhot_max = rmeCoeffsPNPN[0].rmes_->m_rhot_max;
   // tensor multiplicity is however tensor dependent
   int32_t rho0_max;

   // resulting rmes do not have rho0 dependence
   int32_t nrmes_to_compute = bra_max * ket_max * rhot_max;
   std::vector<double> result_rmes(nrmes_to_compute, 0.0);

   // iterate over PP NN and PN tensors each has the same lm0 mu0 and S0
   // quantum labels but it can have a different rho0max multiplicity
   for (size_t itensor = 0; itensor < rmeCoeffsPNPN.size(); ++itensor) {
      assert(bra_max == rmeCoeffsPNPN[itensor].rmes_->m_bra_max);
      assert(ket_max == rmeCoeffsPNPN[itensor].rmes_->m_ket_max);
      assert(rhot_max == rmeCoeffsPNPN[itensor].rmes_->m_rhot_max);

      rho0_max = rmeCoeffsPNPN[itensor].rmes_->m_tensor_max;
      int32_t rme_index = 0;
      for (int i = 0; i < bra_max; ++i) {
         for (int j = 0; j < ket_max; ++j) {
            for (int irho0 = 0; irho0 < rho0_max; ++irho0) {
               // vector with rhot_max elements
               // rmes = < i ||| rho0 ||| j>[i][j][rho0] = {...}
               RME::DOUBLE* rmes = rmeCoeffsPNPN[itensor].rmes_->GetVector(i, j, irho0);
               TENSOR_STRENGTH coeff_rho0 = rmeCoeffsPNPN[itensor].coeffs_[irho0];
               for (int irhot = 0; irhot < rhot_max; ++irhot) {
                  result_rmes[rme_index + irhot] += (double)coeff_rho0 * (double)rmes[irhot];
               }
            }
            rme_index += rhot_max;
         }
      }
   }
   return result_rmes;
}

void CalculateRME(const std::string& rme_file_name, const CInteractionPPNN& interactionPPNN,
                  const CInteractionPN& interactionPN, const lsu3::CncsmSU3xSU2Basis& bra,
                  const lsu3::CncsmSU3xSU2Basis& ket, int dN0, const SU3xSU2::LABELS& w0)
// Iterate over bra and ket irrep subspaces (i.e., subspaces of equivalent
// irreps, distinguished by upstream quantum numbers), and for each pair of
// irrep subspaces connected by given operator (e.g., unit tensor), calculate
// RMEs of the given operator between that pair, and write these to file.
//
// Calls: ComputeRME
{
   // set up statistics for this tensor
   int num_groups_allowed = 0;
   int num_rmes_allowed = 0;
   int num_groups_nonzero = 0;
   int num_rmes_nonzero = 0;
   double mpi_tensor_start_time = MPI_Wtime();

   // open output file
   std::cout << "opening " << rme_file_name << std::endl;
   std::ofstream out_file;
   if (g_enable_write) {
      std::ios_base::openmode mode_argument = std::ios_base::out;
      if (g_output_binary) mode_argument |= std::ios_base::binary;
      out_file.open(rme_file_name, mode_argument);
   }
   if (!out_file) {
      std::cerr << "Could not open file '" << rme_file_name << "'!" << std::endl;
      return;
   }

   // write binary file header
   if (g_enable_write && g_output_binary) {
      // file format code
      WriteBinary<int>(out_file, g_binary_format_code);
      // floating point precision
      WriteBinary<int>(out_file, g_binary_float_precision);
   }

   std::vector<unsigned char> hoShells_n, hoShells_p;
   std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
   std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

   std::vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

   unsigned char num_vacuums_J_distr_p;
   unsigned char num_vacuums_J_distr_n;
   std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP,
       selected_tensorsNN;
   std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn,
       selected_tensors_n_pn;

   std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
   std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

   std::vector<MECalculatorData> rmeCoeffsPNPN;

   SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

   uint16_t max_mult_p = bra.getMaximalMultiplicity_p();
   uint16_t max_mult_n = bra.getMaximalMultiplicity_n();

   InitializeIdenticalOperatorRME(identityOperatorRMEPP, max_mult_p*max_mult_p);
   InitializeIdenticalOperatorRME(identityOperatorRMENN, max_mult_n*max_mult_n);

   double mpi_tensor_end_setup_time = MPI_Wtime();

   // main block-by-block calculation

   SingleDistributionSmallVector distr_ip, distr_in, distr_jp, distr_jn;
   UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
   SU3xSU2_SMALL_VEC vW_ip, vW_in, vW_jp, vW_jn;

   unsigned char deltaP, deltaN;

   const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
   const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

   int32_t icurrentDistr_p, icurrentDistr_n;
   int32_t icurrentGamma_p, icurrentGamma_n;

   int32_t row_irrep = 0;
   for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      //		We don't have to check whether bra.NumberOfStatesInBlock(ipin_block) > 0
      // since we are interested
      //		in reduced matrix elements. The value of J is irrelevant.
      uint32_t ip = bra.getProtonIrrepId(ipin_block);
      uint32_t in = bra.getNeutronIrrepId(ipin_block);
      int32_t Nhw_bra = bra.nhw_p(ip) + bra.nhw_n(in);
      uint16_t nrow_irreps = bra.NumberPNIrrepsInBlock(ipin_block);

      SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

      uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max());
      uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max());

      uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max());
      uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max());

      uint32_t last_jp(std::numeric_limits<uint32_t>::max());
      uint32_t last_jn(std::numeric_limits<uint32_t>::max());

      //	Generally, ket space can be different from bra space. For this reason we loop over
      // all ket jpjn blocks.
      int32_t col_irrep = 0;
      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         uint32_t jp = ket.getProtonIrrepId(jpjn_block);
         uint32_t jn = ket.getNeutronIrrepId(jpjn_block);
         int32_t Nhw_ket = ket.nhw_p(jp) + ket.nhw_n(jn);
         uint16_t ncol_irreps = ket.NumberPNIrrepsInBlock(jpjn_block);

         if ((Nhw_ket + dN0) != Nhw_bra) {
            col_irrep = col_irrep + ncol_irreps;
            continue;
         }

         SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
         SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

         uint16_t ajp_max = ket.getMult_p(jp);
         uint16_t ajn_max = ket.getMult_n(jn);

         if (jp != last_jp) {
            icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
            icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

            if (ilastDistr_p != icurrentDistr_p) {
               distr_ip.resize(0);
               bra.getDistr_p(ip, distr_ip);
               gamma_ip.resize(0);
               bra.getGamma_p(ip, gamma_ip);
               vW_ip.resize(0);
               bra.getOmega_p(ip, vW_ip);

               distr_jp.resize(0);
               ket.getDistr_p(jp, distr_jp);
               hoShells_p.resize(0);
               deltaP = TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p,
                   num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
            }

            if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p) {
               if (deltaP <= 4) {
                  if (!selected_tensorsPP.empty()) {
                     std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(),
                                   CTensorGroup::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensorsPP.resize(0);

                  if (!selected_tensors_p_pn.empty()) {
                     std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(),
                                   CTensorGroup_ada::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensors_p_pn.resize(0);

                  gamma_jp.resize(0);
                  ket.getGamma_p(jp, gamma_jp);
                  TransformGammaKet_SelectByGammas(
                      hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP,
                      tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp,
                      selected_tensorsPP, selected_tensors_p_pn);
               }
            }

            if (deltaP <= 4) {
               Reset_rmeCoeffs(rmeCoeffsPP);
               Reset_rmeIndex(rme_index_p_pn);

               vW_jp.resize(0);
               ket.getOmega_p(jp, vW_jp);
               TransformOmegaKet_CalculateRME(
                   distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP,
                   selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
            }
            ilastDistr_p = icurrentDistr_p;
            ilastGamma_p = icurrentGamma_p;
         }

         if (jn != last_jn) {
            icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
            icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

            if (ilastDistr_n != icurrentDistr_n) {
               distr_in.resize(0);
               bra.getDistr_n(in, distr_in);
               gamma_in.resize(0);
               bra.getGamma_n(in, gamma_in);
               vW_in.resize(0);
               bra.getOmega_n(in, vW_in);

               distr_jn.resize(0);
               ket.getDistr_n(jn, distr_jn);
               hoShells_n.resize(0);
               deltaN = TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n,
                   num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
            }

            if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n) {
               if (deltaN <= 4) {
                  if (!selected_tensorsNN.empty()) {
                     std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(),
                                   CTensorGroup::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensorsNN.resize(0);

                  if (!selected_tensors_n_pn.empty()) {
                     std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(),
                                   CTensorGroup_ada::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensors_n_pn.resize(0);

                  gamma_jn.resize(0);
                  ket.getGamma_n(jn, gamma_jn);
                  TransformGammaKet_SelectByGammas(
                      hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN,
                      tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn,
                      selected_tensorsNN, selected_tensors_n_pn);
               }
            }

            if (deltaN <= 4) {
               Reset_rmeCoeffs(rmeCoeffsNN);
               Reset_rmeIndex(rme_index_n_pn);

               vW_jn.resize(0);
               ket.getOmega_n(jn, vW_jn);
               TransformOmegaKet_CalculateRME(
                   distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN,
                   selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
            }

            ilastDistr_n = icurrentDistr_n;
            ilastGamma_n = icurrentGamma_n;
         }

         //	loop over wpn that result from coupling ip x in
         int32_t ibegin = bra.blockBegin(ipin_block);
         int32_t iend = bra.blockEnd(ipin_block);
         for (int32_t iwpn = ibegin, i = 0; iwpn < iend; ++iwpn, ++i) {
            SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
            IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));

            int32_t jbegin = ket.blockBegin(jpjn_block);
            int32_t jend = ket.blockEnd(jpjn_block);
            for (int32_t jwpn = jbegin, j = 0; jwpn < jend; ++jwpn, ++j) {
               SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));

               if (!SU2::mult(omega_pn_J.S2, w0.S2, omega_pn_I.S2) ||
                   !SU3::mult(omega_pn_J, w0, omega_pn_I)) {
                  continue;
               }

               IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));

               if (deltaP + deltaN <= 4) {
                  Reset_rmeCoeffs(rmeCoeffsPNPN);
                  if (in == jn && !rmeCoeffsPP.empty()) {
                     //	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.
                     CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
                     //	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}]
                     //|||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}
                     Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP,
                                                        identityOperatorRMENN, rmeCoeffsPNPN);
                  }

                  if (ip == jp && !rmeCoeffsNN.empty()) {
                     //	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.
                     CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
                     //	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}]
                     //|||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}
                     Calculate_Identity_x_Neutron_MeData(
                         omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
                  }

                  if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty()) {
                     // This function assumes coefficients depend on rho0 only.
                     // It also disregard SU(3) clebsch gordan coefficients as
                     // we do not use the Wigner-Eckhart theorem.
                     CalculatePNOperatorRMECoeffs(interactionPN, omega_pn_I, omega_pn_J,
                                                  rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
                     //                     CalculatePNInteractionMeData(interactionPN, omega_pn_I,
                     //                     omega_pn_J,
                     //                                                  rme_index_p_pn,
                     //                                                  rme_index_n_pn,
                     //                                                  rmeCoeffsPNPN);
                  }

                  if (!rmeCoeffsPNPN.empty()) {
                     std::vector<double> rmes = ComputeRME(rmeCoeffsPNPN);

                     // check if any rmes are above zero threshold
                     bool nonzero = false;
                     for (auto rme : rmes) {
                        // Caution: Important to use std::abs() rather than integer abs().
                        nonzero |= (std::abs(rme) > g_zero_threshold);
                     }

                     // count rmes
                     num_groups_allowed += 1;
                     num_rmes_allowed += rmes.size();
                     if (nonzero) {
                        num_groups_nonzero += 1;
                        num_rmes_nonzero += rmes.size();
                     }

                     // write out rmes
                     if (g_enable_write) {
                        if (g_output_binary) {
                           // write indexing
                           int row_index = row_irrep + i;
                           assert(row_index == static_cast<RMEIndexType>(row_index));
                           WriteBinary<RMEIndexType>(out_file, row_index);
                           int col_index = col_irrep + j;
                           assert(col_index == static_cast<RMEIndexType>(col_index));
                           WriteBinary<RMEIndexType>(out_file, col_index);
                           assert(rmes.size() == static_cast<RMEIndexType>(rmes.size()));
                           WriteBinary<RMEIndexType>(out_file, rmes.size());

                           // write rmes
                           for (auto rme : rmes) {
                              if (g_binary_float_precision == 4)
                                 WriteBinary<float>(out_file, rme);
                              else if (g_binary_float_precision == 8)
                                 WriteBinary<double>(out_file, rme);
                           }
                        } else {
                           out_file << row_irrep + i << " " << col_irrep + j << " ";
                           for (auto rme : rmes) {
                              out_file << rme << " ";
                           }
                           out_file << "size: " << rmes.size() << std::endl;
                           out_file << std::endl;
                        }
                     }
                  }
               }
            }
         }
         col_irrep = col_irrep + ncol_irreps;
         last_jp = jp;
         last_jn = jn;
      }
      row_irrep = row_irrep + nrow_irreps;
   }

   double mpi_tensor_end_iteration_time = MPI_Wtime();

   Reset_rmeCoeffs(rmeCoeffsPP);
   Reset_rmeCoeffs(rmeCoeffsNN);
   Reset_rmeCoeffs(rmeCoeffsPNPN);
   //	delete arrays allocated in identityOperatorRME?? structures
   delete[] identityOperatorRMEPP.m_rme;
   delete[] identityOperatorRMENN.m_rme;
   if (g_enable_write) {
      out_file.close();
   }

   // process statistics for this tensor
   g_num_tensors += 1;
   g_num_groups_allowed += num_groups_allowed;
   g_num_rmes_allowed += num_rmes_allowed;
   double mpi_tensor_end_time = MPI_Wtime();
   int my_rank;
   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
   std::cout << "STAT: (tensor)"
             << " rank " << my_rank << " file " << rme_file_name << std::setprecision(5)
             << std::showpoint << " time_end_setup "
             << mpi_tensor_end_setup_time - mpi_tensor_start_time << " time_end_iteration "
             << mpi_tensor_end_iteration_time - mpi_tensor_start_time << " time_end "
             << mpi_tensor_end_time - mpi_tensor_start_time << " num_groups_allowed "
             << num_groups_allowed << " num_rmes_allowed " << num_rmes_allowed
             << " num_groups_nonzero " << num_groups_nonzero << " num_rmes_nonzero "
             << num_rmes_nonzero << std::endl;
}

void CalculateRMEs(const lsu3::CncsmSU3xSU2Basis& bra, const lsu3::CncsmSU3xSU2Basis& ket,
                   const std::string& operator_prefix, const CBaseSU3Irreps& baseSU3Irreps,
                   int nmax, int dN0, const SU3xSU2::LABELS& w0)
// Wrapper to load and construct given operator, then invoke CalculateRME (see
// docstring for CalculateRME).
//
// Calls: CalculateRME
{
   //	stringstream interaction_log_file_name;
   //	interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
   //	ofstream interaction_log_file(interaction_log_file_name.str().c_str());
   std::ofstream interaction_log_file("/dev/null");

   //	since root process will read rme files, it is save to let this
   //	process to create missing rme files (for PPNN interaction) if they do not exist
   bool log_is_on = false;
   CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);

   //	PN interaction is read after PPNN, and hence all rmes should be already in memory.
   //	if rme file does not exist, then false

   bool generate_missing_rme = true;
   CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on,
                                interaction_log_file);

   std::string ppnn_file_name(operator_prefix + ".PPNN");
   std::string pn_file_name(operator_prefix + ".PN");

   interactionPPNN.LoadTwoBodyOperator(ppnn_file_name);
   interactionPN.AddOperator(pn_file_name);

   //	interactionPN.ShowTensorGroupsAndTheirTensorStructures();

   interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

   std::string rme_file_name = operator_prefix + ".rme";
   CalculateRME(rme_file_name, interactionPPNN, interactionPN, bra, ket, dN0, w0);
}

////////////////////////////////////////////////////////////////
// main program
////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
   ////////////////////////////////////////////////////////////////
   // MPI setup
   ////////////////////////////////////////////////////////////////

   MPI_Init(&argc, &argv);
   int my_rank, nprocs;
   MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

   double mpi_start_time = MPI_Wtime();

   ////////////////////////////////////////////////////////////////
   // global configuration
   ////////////////////////////////////////////////////////////////

   MECalculatorData dummy;
   int jj0 = dummy.JJ0();
   int mm0 = dummy.MM0();

   if (jj0 != 0 || mm0 != 0) {
      return EXIT_FAILURE;
   }

   // defined in libraries/SU3ME/global_definitions.cpp
   // true : tensor strengths do depend on kmax (besides multiplicity rho0)
   // false: tensor strengths do not depend on kmax
   k_dependent_tensor_strenghts = false;
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   ////////////////////////////////////////////////////////////////
   // process arguments
   ////////////////////////////////////////////////////////////////

   if (nprocs < 2) {
      std::cerr << "Master-slave program requires at least 2 MPI processes!" << std::endl;
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
   }

   if (su3shell_data_directory == NULL) {
      if (my_rank == 0) {
         std::cerr << "System variable 'SU3SHELL_DATA' was not defined!" << std::endl;
         MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      }
   }

   if (argc != 4 + 1) {
      if (my_rank == 0) {
         std::cerr << "Usage: " << argv[0]
                   << " <bra model space>  <ket model space> <file with operator prefixes and "
                      "labels> <output mode>"
                   << std::endl;
         std::cerr << std::endl;
         std::cerr << "This code takes as an input operators with a constant SU(3)xSU(2)"
                   << std::endl;
         std::cerr << "quantum numbers (lm0 mu)S0 and computes its SU(3)xSU(2) reduced matrix "
                      "elements (RMEs)"
                   << std::endl;
         std::cerr << "for all selected proton-neutron irreps.  Resulting RMEs are computed as a "
                   << std::endl;
         std::cerr << "sum of RMEs for all terms (made up of SU(3)xSU(2) coupled sequences of "
                      "a+/ta operators)."
                   << std::endl;
         std::cerr << std::endl;
         std::cerr << "Input arguments:" << std::endl;
         std::cerr << "<bra model space> & <ket model space>: model spaces with the value of 2J "
                      "set to 255."
                   << std::endl;
         std::cerr << "This means that all values of J are selected. As this code computes only "
                   << std::endl;
         std::cerr
             << "SU(3)-reduced matrix elements, this means that all SU(3) irreps are considered"
             << std::endl;
         std::cerr << "<file with operator prefixes and labels>: file with a list of prefixes "
                      "followed by dN0 lm0 mu0 2S0 quantum numbers (one per line)."
                   << " This file is usually generated by \"generate_lsu3shell_relative_operators\"."
                   << std::endl;
         std::cerr << "<output mode>:" << std::endl;
         std::cerr << "             \"count\": suppress rme output" << std::endl;
         std::cerr << "             \"text\": rme output as text" << std::endl;
         std::cerr << "             \"binary-4\" | \"binary\": rme output binary (4-byte float)" << std::endl;
         std::cerr << "             \"binary-8\": rme output binary (8-byte float)" << std::endl;
         MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      }
   }

   std::string bra_space_definition_file_name(argv[1]);
   std::string ket_space_definition_file_name(argv[2]);
   std::string operator_list_file_name(argv[3]);

   // output mode argument
   //
   //   "count": suppress rme output
   //   "text": rme output as text
   //   "binary-4" | "binary": rme output binary (4-byte float)
   //   "binary-8": rme output binary (8-byte float)

   std::string mode_string(argv[4]);
   std::cout << "Run mode: " << mode_string << std::endl;
   if (mode_string == "count") {
      g_enable_write = false;
   } else if (mode_string == "text") {
      g_enable_write = true;
      g_output_binary = false;
   } else if ((mode_string == "binary") || (mode_string == "binary-4")) {
      g_enable_write = true;
      g_output_binary = true;
      g_binary_float_precision = 4;
   } else if (mode_string == "binary-8") {
      g_enable_write = true;
      g_output_binary = true;
      g_binary_float_precision = 8;
   } else {
      std::cerr << "Unrecognized mode argument" << std::endl;
      std::exit(EXIT_FAILURE);
   }

   ////////////////////////////////////////////////////////////////
   // calculation
   ////////////////////////////////////////////////////////////////
   su3::init();
   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   static const int tag_work = 0;
   static const int tag_finished = 1;

   static const int mbs = 2000;  // maximum buffer size

   // common start barrier for debugging/profiling
   MPI_Barrier(MPI_COMM_WORLD);
   double mpi_compute_barrier_time = MPI_Wtime();

   // MASTER PART:
   if (my_rank == 0) {
      std::ifstream operator_list_file(operator_list_file_name);

      int working_slaves = 0;

      // auxiliary data:
      char buffer[mbs];
      MPI_Status status;

      while (operator_list_file) {
         std::string operator_prefix;
         int dN0, lm0, mu0, SS0;
         operator_list_file >> operator_prefix >> dN0 >> lm0 >> mu0 >> SS0;

         assert(operator_prefix.size() + 1 <= mbs);

         if (operator_prefix.empty()) continue;

         // initial distribution:
         if (working_slaves < nprocs - 1) {
            MPI_Send(operator_prefix.c_str(), operator_prefix.size() + 1, MPI_CHAR,
                     working_slaves + 1, tag_work, MPI_COMM_WORLD);
            MPI_Send(&dN0, 1, MPI_INT, working_slaves + 1, tag_work, MPI_COMM_WORLD);
            MPI_Send(&lm0, 1, MPI_INT, working_slaves + 1, tag_work, MPI_COMM_WORLD);
            MPI_Send(&mu0, 1, MPI_INT, working_slaves + 1, tag_work, MPI_COMM_WORLD);
            MPI_Send(&SS0, 1, MPI_INT, working_slaves + 1, tag_work, MPI_COMM_WORLD);

            working_slaves++;
            continue;
         }

         // send another work
         MPI_Recv(buffer, mbs, MPI_CHAR, MPI_ANY_SOURCE, tag_finished, MPI_COMM_WORLD, &status);

         MPI_Send(operator_prefix.c_str(), operator_prefix.size() + 1, MPI_CHAR, status.MPI_SOURCE,
                  tag_work, MPI_COMM_WORLD);
         MPI_Send(&dN0, 1, MPI_INT, status.MPI_SOURCE, tag_work, MPI_COMM_WORLD);
         MPI_Send(&lm0, 1, MPI_INT, status.MPI_SOURCE, tag_work, MPI_COMM_WORLD);
         MPI_Send(&mu0, 1, MPI_INT, status.MPI_SOURCE, tag_work, MPI_COMM_WORLD);
         MPI_Send(&SS0, 1, MPI_INT, status.MPI_SOURCE, tag_work, MPI_COMM_WORLD);
      }

      // wait for working slaves:
      while (working_slaves > 0) {
         MPI_Recv(buffer, mbs, MPI_CHAR, MPI_ANY_SOURCE, tag_finished, MPI_COMM_WORLD,
                  MPI_STATUS_IGNORE);
         working_slaves--;
      }

      // finish slaves:
      for (int i = 1; i < nprocs; i++)
         MPI_Send(buffer, 0, MPI_CHAR, i, tag_finished, MPI_COMM_WORLD);

   }
   // SLAVE PART:
   else {
      // set up timing for slave task
      double mpi_slave_start_time = MPI_Wtime();

      int idiag = 0;
      int jdiag = 0;
      int ndiag = 1;

      //	Construct BRA model space
      proton_neutron::ModelSpace bra_ncsmModelSpace;
      lsu3::CncsmSU3xSU2Basis bra;
      bra_ncsmModelSpace.Load(bra_space_definition_file_name);
      bra.ConstructBasis(bra_ncsmModelSpace, idiag, ndiag);

      if (bra.JJ() != 255) {
         std::cerr << "Error: bra model space must have 2J set to 255!" << std::endl;
         exit(EXIT_FAILURE);
      }

      //	Construct KET model space
      proton_neutron::ModelSpace ket_ncsmModelSpace;
      lsu3::CncsmSU3xSU2Basis ket;
      ket_ncsmModelSpace.Load(ket_space_definition_file_name);
      ket.ConstructBasis(ket_ncsmModelSpace, jdiag, ndiag);

      if (ket.JJ() != 255) {
         std::cerr << "Error: ket model space must have 2J set to 255!" << std::endl;
         exit(EXIT_FAILURE);
      }

      int nmax = std::max(bra.Nmax(), ket.Nmax());
      CBaseSU3Irreps baseSU3Irreps(bra.NProtons(), bra.NNeutrons(), nmax);

      int dN0, lm0, mu0, SS0;
      double mpi_slave_last_completed_task_time = MPI_Wtime();
      while (true) {
         char buffer[mbs];
         MPI_Status status;

         MPI_Recv(buffer, mbs, MPI_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

         if (status.MPI_TAG == tag_finished) break;

         MPI_Recv(&dN0, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
         MPI_Recv(&lm0, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
         MPI_Recv(&mu0, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
         MPI_Recv(&SS0, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

         std::string operator_prefix(buffer);

         CalculateRMEs(bra, ket, operator_prefix, baseSU3Irreps, nmax, dN0,
                       SU3xSU2::LABELS(1, lm0, mu0, SS0));

         MPI_Send(buffer, 0, MPI_CHAR, 0, tag_finished, MPI_COMM_WORLD);

         mpi_slave_last_completed_task_time = MPI_Wtime();  // update last completed task time
      }

      // process statistics for this rank
      double mpi_slave_end_time = MPI_Wtime();
      std::cout << "STAT: (slave)"
                << " rank " << my_rank << std::setprecision(5) << std::showpoint
                << " time_to_completed_last_task "
                << mpi_slave_last_completed_task_time - mpi_slave_start_time << " time_to_release "
                << mpi_slave_end_time - mpi_slave_start_time << " num_tensors " << g_num_tensors
                << " num_groups_allowed " << g_num_groups_allowed << " num_rmes_allowed "
                << g_num_rmes_allowed << " num_groups_nonzero " << g_num_groups_nonzero
                << " num_rmes_nonzero " << g_num_rmes_nonzero << std::endl;
   }

   double mpi_almost_end_time = MPI_Wtime();

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();  // clears memory allocated for U9, U6, and Z6
                                                      // coefficients
   CSSTensorRMELookUpTablesContainer::ReleaseMemory();  // clear memory allocated for single-shell
                                                        // SU(3) rmes
   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory allocated for SU(3)>SO(3)
                                                           // coefficients
   double mpi_end_time = MPI_Wtime();

   std::cout << "STAT: (process)"
             << " rank " << my_rank << std::setprecision(5) << std::showpoint
             << " compute_barrier_time " << mpi_compute_barrier_time - mpi_start_time
             << " almost_time " << mpi_almost_end_time - mpi_start_time << " time "
             << mpi_end_time - mpi_start_time << std::endl;

   ////////////////////////////////////////////////////////////////
   // termination
   ////////////////////////////////////////////////////////////////

   MPI_Finalize();
   su3::finalize();

   return EXIT_SUCCESS;
}
