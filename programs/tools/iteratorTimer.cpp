#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>

#include <stdexcept>
#include <cmath>
#include <vector>
#include <stack>
#include <ctime>

using namespace std;

void Iterate( const CncsmSU3Basis& Basis, const unsigned int ndiag, const unsigned int idiag, const unsigned int jdiag,
			  const string& outputFileName, const SU3xSU2::IrrepsContainer<IRREPBASIS>& irreps_basis)
{
	cout << "Resulting time: " << endl;
	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_ket_distr_p;
	unsigned char num_vacuums_ket_distr_n;
	SingleDistribution distr_p_ket, distr_n_ket;
	SingleDistribution distr_p_bra, distr_n_bra;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;
	unsigned int ap_bra, an_bra;
	unsigned int ap_ket, an_ket;
	SU3xSU2::LABELS omega_pn_bra, omega_pn_ket;

	unsigned long stateId_bra(0);
	unsigned int dim_bra_irrep = 0;

	unsigned char status_bra_basis;
	UN::SU3xSU2_VEC gamma_p_bra, gamma_n_bra;

	UN::SU3xSU2_VEC	gamma_p_bra_vacuum; 
	SU3xSU2_VEC omega_p_bra_vacuum;
	UN::SU3xSU2_VEC	gamma_n_bra_vacuum; 
	SU3xSU2_VEC omega_n_bra_vacuum;

	SU3xSU2_VEC omega_p_bra, omega_n_bra;

	unsigned int dim_ket_irrep = 0;
	unsigned char status_ket_basis;
	UN::SU3xSU2_VEC gamma_p_ket, gamma_n_ket;
	SU3xSU2_VEC omega_p_ket, omega_n_ket;

	unsigned char deltaP, deltaN;

	unsigned long number_nonzero_me(0);
	PNConfIterator ket = Basis.firstPNConf(jdiag, ndiag);
	PNConfIterator bra = Basis.firstPNConf(idiag, ndiag);

	
	cout << "Resulting time: " << endl;
	unsigned long dim_bra = CalculateBasisDim(bra, irreps_basis);
	unsigned long dim_ket = CalculateBasisDim(ket, irreps_basis);
	cout << "Resulting time: " << endl;
	
	ofstream fresults(outputFileName.c_str());
	fresults.precision(10);
	fresults << "%%MatrixMarket matrix coordinate real symmetric" << endl;
	fresults << dim_bra << " " << dim_ket << " here you must enter number of nonzero matrix elements" << endl;


	for (bra.rewind(); bra.hasPNConf(); bra.nextPNConf())
	{
		dim_bra_irrep = irreps_basis.rhomax_x_dim(bra.getCurrentSU3xSU2());
		status_bra_basis = bra.status();
		switch (status_bra_basis)
		{
			case PNConfIterator::kNewDistr_p: 
			case PNConfIterator::kNewDistr_n: 
			case PNConfIterator::kNewGamma_p: 
			case PNConfIterator::kNewGamma_n: 
			case PNConfIterator::kNewOmega_p: 
				ap_bra = bra.getMult_p();
			case PNConfIterator::kNewOmega_n: 
				an_bra = bra.getMult_n();
			case PNConfIterator::kNewPNOmega:
				omega_pn_bra = bra.getCurrentSU3xSU2();	
		}
		if (dim_bra_irrep == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}

		unsigned long stateId_ket((idiag == jdiag) ? stateId_bra : 0);
		size_t afmax = ap_bra*an_bra*omega_pn_bra.rho;
		IRREPBASIS braSU3xSU2basis(irreps_basis.getSU3xSU2PhysicalBasis(omega_pn_bra));
		size_t num_rows_in_block = afmax*braSU3xSU2basis.dim();
		vector<vector<float> > vals_local(num_rows_in_block);	// how to resize elements ?
		vector<vector<size_t> > col_ind_local(num_rows_in_block);

		for ((idiag == jdiag) ? ket.rewind(bra): ket.rewind(); ket.hasPNConf(); ket.nextPNConf())
		{
			dim_ket_irrep = irreps_basis.rhomax_x_dim(ket.getCurrentSU3xSU2());
			status_ket_basis = ket.status();
			stateId_bra += ap_bra*an_bra*dim_bra_irrep; 
		}
		fresults << stateId_bra << '\n';
	}
}

int main(int argc,char **argv)
{
	if (argc != 3)
	{
		cout << "Name of the file with the model space definition are the required input parameters." << endl;
		return EXIT_FAILURE;
	}

	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);

	unsigned int ndiag = 29;
	unsigned int idiag = 1;
	unsigned int jdiag = 2;

	std::vector<unsigned long> dims(ndiag, 0);
	CncsmSU3Basis Basis(ncsmModelSpace);
	SU3xSU2::IrrepsContainer<IRREPBASIS> irreps_basis(Basis.GenerateFinal_SU3xSU2Irreps(), ncsmModelSpace.JJ());
	CalculateDimAllSections(Basis, dims, ndiag, irreps_basis);

	time_t start, end;
	time(&start);
	cout << "Resulting time: " << endl;
	Iterate(Basis, ndiag, idiag, jdiag, argv[2], irreps_basis);
	time(&end);
	double dif = difftime (end,start);
	cout << "Resulting time: " << dif << " seconds." << endl;
}
