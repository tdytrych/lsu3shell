#include <SU3ME/ComputeOperatorMatrix.h>
#include <mpi.h>

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/OperatorLoader.h>

#include <su3.h>

#include <boost/mpi.hpp>
//	To be able to load and distribute basis from a binary archive file
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <algorithm>  // std::plus
#include <chrono>
#include <cmath>
#include <ctime>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <vector>

enum { Nhw, SSp, SSn, SS, LM, MU };
enum { NA, NB, LM0, MU0, SS0 };
int nprotons, nneutrons, Nmax, jj;

//	create a file compatible with one-body SU(3) decomposition of an
//	interaction, with just a single SU(3) tensor {a+a}
int CreateInputFileForDensity(int na, int nb, int lm0, int mu0, int SS0) {
   int phase = 1;
   // We need to "fill" k0 L0 and J0 quantum numbers.
   // Technically, they are not needed but legacy structures expects them anyway.
   SU3::LABELS irrep(1, lm0, mu0);
   int l;
   int k0max;
   for (l = 0; l <= (lm0 + mu0); ++l) {
      k0max = SU3::kmax(irrep, l);
      if (k0max) {
         break;
      }
   }
   int LL0 = 2 * l;
   int JJ0 = LL0 + SS0;

   std::ofstream dummy_ada_file("dummy_ada");
   dummy_ada_file << JJ0 << " " << JJ0 << endl;

   std::vector<float> dcoeffs(2 * k0max, 0.0);
   float su3xsu2phase = 1.0;
   char structure[2];
   structure[0] = (na + 1);       // a^+	===> n + 1
   structure[1] = -1 * (nb + 1);  // a	===> -n - 1;

   //	we need to swap tensors
   if (na > nb) {
      //	coefficient in front of the tensor will be multiplied by a phase:
      //	[a^+(na 0) x a^(0 nb)]^(lm0 mu0)_{*} = (-)*(-)^{na + 0 + 0 + nb - lm0 - mu0 + 1/2 +
      // 1/2 - S0} [a^(0 nb) x a^+(na 0)]^(lm0 mu0)_{*}
      phase = -MINUSto(na + nb - lm0 - mu0 + 1 - SS0 / 2);
      //	swap the two elements of structure: n_{a} <--> n_{b}
      std::swap(structure[0], structure[1]);
   }

   dummy_ada_file << (int)structure[0] << " " << (int)structure[1] << endl;
   dummy_ada_file << "1" << endl;  // number of tensors that will follow: 1
   dummy_ada_file << lm0 << " " << mu0 << " " << SS0 << " " << LL0 << " " << JJ0 << endl;
   //	save coefficients in an outdated yet internally still used ordering scheme: k0*rho0max*2 +
   // rho*2 + type, where rho0max = 1
   for (int ik0 = 0; ik0 < k0max; ++ik0) {
      dummy_ada_file << "1.0 1.0" << endl;
   }
   dummy_ada_file << std::endl;
   return phase;
}

std::vector<std::array<int, 6>> CreateListofU3SU2subspaces(const std::string& model_space_file) {
   proton_neutron::ModelSpace ncsmModelSpace(model_space_file);

   nprotons = ncsmModelSpace.number_of_protons();
   nneutrons = ncsmModelSpace.number_of_neutrons();

   lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);
   Nmax = basis.Nmax();
   jj = basis.JJ();
   std::set<std::array<int, 6>> unique_u3su2;

   for (int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++) {
      if (!basis.NumberOfStatesInBlock(ipin_block)) {
         continue;
      }
      uint32_t ip = basis.getProtonIrrepId(ipin_block);
      uint32_t in = basis.getNeutronIrrepId(ipin_block);
      int N = basis.nhw_p(ip) + basis.nhw_n(in);
      SU3xSU2::LABELS irrep_p(basis.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS irrep_n(basis.getNeutronSU3xSU2(in));
      int ssp(irrep_p.S2);
      int ssn(irrep_n.S2);

      for (int iwpn = basis.blockBegin(ipin_block); iwpn < basis.blockEnd(ipin_block); ++iwpn) {
         SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
         int lm = omega_pn.lm;
         int mu = omega_pn.mu;
         int ss = omega_pn.S2;
         unique_u3su2.insert({N, ssp, ssn, ss, lm, mu});
      }
   }

   return std::vector<std::array<int, 6>>(unique_u3su2.begin(), unique_u3su2.end());
}

std::vector<std::array<int, 5>> ReadListOfTensors(const std::string& filename) {
   std::vector<std::array<int, 5>> tensor_list;
   std::ifstream file(filename);
   if (!file) {
      std::cerr << "Could not open '" << filename << "' input file!" << std::endl;
      exit(EXIT_FAILURE);
   }

   while (true) {
      int na, nb, lm0, mu0, ss0;
      file >> na >> nb >> lm0 >> mu0 >> ss0;
      if (file) {
         tensor_list.push_back({na, nb, lm0, mu0, ss0});
      } else {
         break;
      }
   }
   return tensor_list;
}

void ComputeRMEs(const std::array<int, 6>& bra_space, const std::array<int, 5>& tensor,
                 const std::array<int, 6>& ket_space, int phase,
                 const CInteractionPPNN& interactionPPNN, const CInteractionPN& interactionPN,
                 std::ofstream& proton_file, std::ofstream& neutron_file) {
   int nhw_bra = bra_space[Nhw];
   int ssp_bra = bra_space[SSp];
   int ssn_bra = bra_space[SSn];
   int ss_bra = bra_space[SS];
   int lm_bra = bra_space[LM];
   int mu_bra = bra_space[MU];
   proton_neutron::ModelSpace bra_ncsmModelSpace(nprotons, nneutrons, nhw_bra, ssp_bra, ssn_bra,
                                                 ss_bra, lm_bra, mu_bra, jj);
   lsu3::CncsmSU3xSU2Basis bra(bra_ncsmModelSpace, 0, 1);

   int nhw_ket = ket_space[Nhw];
   int ssp_ket = ket_space[SSp];
   int ssn_ket = ket_space[SSn];
   int ss_ket = ket_space[SS];
   int lm_ket = ket_space[LM];
   int mu_ket = ket_space[MU];
   proton_neutron::ModelSpace ket_ncsmModelSpace(nprotons, nneutrons, nhw_ket, ssp_ket, ssn_ket,
                                                 ss_ket, lm_ket, mu_ket, jj);
   lsu3::CncsmSU3xSU2Basis ket(ket_ncsmModelSpace, 0, 1);
   ///////////////////////////
   // L E T S    G O !!!!
   ///////////////////////////
   std::vector<unsigned char> hoShells_n, hoShells_p;
   std::vector<CTensorGroup *> tensorGroupsPP, tensorGroupsNN;
   std::vector<CTensorGroup_ada *> tensorGroups_p_pn, tensorGroups_n_pn;

   std::vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

   unsigned char num_vacuums_J_distr_p;
   unsigned char num_vacuums_J_distr_n;
   std::vector<std::pair<CRMECalculator *, CTensorGroup::COEFF_DOUBLE *>> selected_tensorsPP,
       selected_tensorsNN;
   std::vector<std::pair<CRMECalculator *, unsigned int>> selected_tensors_p_pn,
       selected_tensors_n_pn;

   std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
   std::vector<std::pair<SU3xSU2::RME *, unsigned int>> rme_index_p_pn, rme_index_n_pn;

   std::vector<MECalculatorData> rmeCoeffsProton;
   std::vector<MECalculatorData> rmeCoeffsNeutron;

   SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

   uint16_t max_mult_p = bra.getMaximalMultiplicity_p();
   uint16_t max_mult_n = bra.getMaximalMultiplicity_n();

   InitializeIdenticalOperatorRME(identityOperatorRMEPP, max_mult_p*max_mult_p);
   InitializeIdenticalOperatorRME(identityOperatorRMENN, max_mult_n*max_mult_n);

   SingleDistributionSmallVector distr_ip, distr_in, distr_jp, distr_jn;
   UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
   SU3xSU2_SMALL_VEC vW_ip, vW_in, vW_jp, vW_jn;

   unsigned char deltaP, deltaN;

   const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
   const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

   int32_t icurrentDistr_p, icurrentDistr_n;
   int32_t icurrentGamma_p, icurrentGamma_n;

   int32_t row_irrep = 0;
   for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      //		We don't have to check whether bra.NumberOfStatesInBlock(ipin_block) > 0
      // since we are interested
      //		in reduced matrix elements. The value of J is irrelevant.
      uint32_t ip = bra.getProtonIrrepId(ipin_block);
      uint32_t in = bra.getNeutronIrrepId(ipin_block);
      uint16_t nrow_irreps = bra.NumberPNIrrepsInBlock(ipin_block);

      SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

      uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max());
      uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max());

      uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max());
      uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max());

      uint32_t last_jp(std::numeric_limits<uint32_t>::max());
      uint32_t last_jn(std::numeric_limits<uint32_t>::max());

      //	Generally, ket space can be different from bra space. For this reason we loop over
      // all ket jpjn blocks.
      int32_t col_irrep = 0;
      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         uint32_t jp = ket.getProtonIrrepId(jpjn_block);
         uint32_t jn = ket.getNeutronIrrepId(jpjn_block);
         uint16_t ncol_irreps = ket.NumberPNIrrepsInBlock(jpjn_block);

         SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
         SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

         uint16_t ajp_max = ket.getMult_p(jp);
         uint16_t ajn_max = ket.getMult_n(jn);

         if (jp != last_jp) {
            icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
            icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

            if (ilastDistr_p != icurrentDistr_p) {
               distr_ip.resize(0);
               bra.getDistr_p(ip, distr_ip);
               gamma_ip.resize(0);
               bra.getGamma_p(ip, gamma_ip);
               vW_ip.resize(0);
               bra.getOmega_p(ip, vW_ip);

               distr_jp.resize(0);
               ket.getDistr_p(jp, distr_jp);
               hoShells_p.resize(0);
               deltaP = TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p,
                   num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
            }

            if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p) {
               if (deltaP <= 4) {
                  if (!selected_tensorsPP.empty()) {
                     std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(),
                                   CTensorGroup::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensorsPP.resize(0);

                  if (!selected_tensors_p_pn.empty()) {
                     std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(),
                                   CTensorGroup_ada::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensors_p_pn.resize(0);

                  gamma_jp.resize(0);
                  ket.getGamma_p(jp, gamma_jp);
                  TransformGammaKet_SelectByGammas(
                      hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP,
                      tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp,
                      selected_tensorsPP, selected_tensors_p_pn);
               }
            }

            if (deltaP <= 4) {
               Reset_rmeCoeffs(rmeCoeffsPP);
               Reset_rmeIndex(rme_index_p_pn);

               vW_jp.resize(0);
               ket.getOmega_p(jp, vW_jp);
               TransformOmegaKet_CalculateRME(
                   distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP,
                   selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
            }
            ilastDistr_p = icurrentDistr_p;
            ilastGamma_p = icurrentGamma_p;
         }

         if (jn != last_jn) {
            icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
            icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

            if (ilastDistr_n != icurrentDistr_n) {
               distr_in.resize(0);
               bra.getDistr_n(in, distr_in);
               gamma_in.resize(0);
               bra.getGamma_n(in, gamma_in);
               vW_in.resize(0);
               bra.getOmega_n(in, vW_in);

               distr_jn.resize(0);
               ket.getDistr_n(jn, distr_jn);
               hoShells_n.resize(0);
               deltaN = TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n,
                   num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
            }

            if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n) {
               if (deltaN <= 4) {
                  if (!selected_tensorsNN.empty()) {
                     std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(),
                                   CTensorGroup::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensorsNN.resize(0);

                  if (!selected_tensors_n_pn.empty()) {
                     std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(),
                                   CTensorGroup_ada::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensors_n_pn.resize(0);

                  gamma_jn.resize(0);
                  ket.getGamma_n(jn, gamma_jn);
                  TransformGammaKet_SelectByGammas(
                      hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN,
                      tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn,
                      selected_tensorsNN, selected_tensors_n_pn);
               }
            }

            if (deltaN <= 4) {
               Reset_rmeCoeffs(rmeCoeffsNN);
               Reset_rmeIndex(rme_index_n_pn);

               vW_jn.resize(0);
               ket.getOmega_n(jn, vW_jn);
               TransformOmegaKet_CalculateRME(
                   distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN,
                   selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
            }

            ilastDistr_n = icurrentDistr_n;
            ilastGamma_n = icurrentGamma_n;
         }

         //	loop over wpn that result from coupling ip x in
         int32_t ibegin = bra.blockBegin(ipin_block);
         int32_t iend = bra.blockEnd(ipin_block);
         for (int32_t iwpn = ibegin, i = 0; iwpn < iend; ++iwpn, ++i) {
            SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
            IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));

            int32_t jbegin = ket.blockBegin(jpjn_block);
            int32_t jend = ket.blockEnd(jpjn_block);
            for (int32_t jwpn = jbegin, j = 0; jwpn < jend; ++jwpn, ++j) {
               SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
               IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));

               if (deltaP + deltaN <= 4) {
                  Reset_rmeCoeffs(rmeCoeffsProton);
                  if (in == jn && !rmeCoeffsPP.empty()) {
                     //	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.
                     CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
                     //	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}]
                     //|||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}
                     Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP,
                                                        identityOperatorRMENN, rmeCoeffsProton);
                  }

                  Reset_rmeCoeffs(rmeCoeffsNeutron);
                  if (ip == jp && !rmeCoeffsNN.empty()) {
                     //	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.
                     CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
                     //	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}]
                     //|||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}
                     Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J,
                                                         identityOperatorRMEPP, rmeCoeffsNN,
                                                         rmeCoeffsNeutron);
                  }

                  if (!rmeCoeffsProton.empty()) {
                     // These values must be constant for each tensor
                     int32_t bra_max = rmeCoeffsProton[0].rmes_->m_bra_max;
                     int32_t ket_max = rmeCoeffsProton[0].rmes_->m_ket_max;
                     int32_t rhot_max = rmeCoeffsProton[0].rmes_->m_rhot_max;

                     for (size_t itensor = 0; itensor < rmeCoeffsProton.size(); ++itensor) {
                        int rho0_max = rmeCoeffsProton[itensor].rmes_->m_tensor_max;
                        assert(rho0_max == 1);
                        for (int i = 0; i < bra_max; ++i) {
                           for (int j = 0; j < ket_max; ++j) {
                              proton_file << ip << " " << in << " " << i << " " << ssp_bra << " "
                                          << ssn_bra << " " << ss_bra << " " << nhw_bra << " "
                                          << lm_bra << " " << mu_bra;
                              proton_file << " " << jp << " " << jn << " " << j << " " << ssp_ket
                                          << " " << ssn_ket << " " << ss_ket << " " << nhw_ket
                                          << " " << lm_ket << " " << mu_ket << " ";
                              for (int irho0 = 0; irho0 < rho0_max; ++irho0) {
                                 // vector with rhot_max elements
                                 // rmes = < i ||| rho0 ||| j>[i][j][rho0] = {...}
                                 RME::DOUBLE* rmes =
                                     rmeCoeffsProton[itensor].rmes_->GetVector(i, j, irho0);
                                 for (int irhot = 0; irhot < rhot_max; ++irhot) {
                                    proton_file << phase * rmes[irhot] << " ";
                                 }
                                 proton_file << std::endl;
                              }
                           }
                        }
                     }
                  }

                  if (!rmeCoeffsNeutron.empty()) {
                     // These values must be constant for each tensor
                     int32_t bra_max = rmeCoeffsNeutron[0].rmes_->m_bra_max;
                     int32_t ket_max = rmeCoeffsNeutron[0].rmes_->m_ket_max;
                     int32_t rhot_max = rmeCoeffsNeutron[0].rmes_->m_rhot_max;

                     for (size_t itensor = 0; itensor < rmeCoeffsNeutron.size(); ++itensor) {
                        int rho0_max = rmeCoeffsNeutron[itensor].rmes_->m_tensor_max;
                        assert(rho0_max == 1);
                        for (int i = 0; i < bra_max; ++i) {
                           for (int j = 0; j < ket_max; ++j) {
                              neutron_file << ip << " " << in << " " << i << " " << ssp_bra << " "
                                           << ssn_bra << " " << ss_bra << " " << nhw_bra << " "
                                           << lm_bra << " " << mu_bra;
                              neutron_file << " " << jp << " " << jn << " " << j << " " << ssp_ket
                                           << " " << ssn_ket << " " << ss_ket << " " << nhw_ket
                                           << " " << lm_ket << " " << mu_ket << " ";
                              for (int irho0 = 0; irho0 < rho0_max; ++irho0) {
                                 // vector with rhot_max elements
                                 // rmes = < i ||| rho0 ||| j>[i][j][rho0] = {...}
                                 RME::DOUBLE* rmes =
                                     rmeCoeffsNeutron[itensor].rmes_->GetVector(i, j, irho0);
                                 for (int irhot = 0; irhot < rhot_max; ++irhot) {
                                    neutron_file << phase * rmes[irhot] << " ";
                                 }
                                 neutron_file << std::endl;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
         col_irrep = col_irrep + ncol_irreps;
         last_jp = jp;
         last_jn = jn;
      }
      row_irrep = row_irrep + nrow_irreps;
   }
   Reset_rmeCoeffs(rmeCoeffsPP);
   Reset_rmeCoeffs(rmeCoeffsNN);
   Reset_rmeCoeffs(rmeCoeffsProton);
   Reset_rmeCoeffs(rmeCoeffsNeutron);
   //	delete arrays allocated in identityOperatorRME?? structures
   delete[] identityOperatorRMEPP.m_rme;
   delete[] identityOperatorRMENN.m_rme;
}

int main(int argc, char** argv) {
   boost::mpi::environment env(argc, argv);
   boost::mpi::communicator mpi_comm_world;
   if (argc != 3) {
      cout << "Usage: " << argv[0] << " <model space> <tensor list file>" << endl;
      cout << "Note:";
      cout << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can "
              "set the "
              "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
           << endl;
      return EXIT_FAILURE;
   }
   int my_rank = mpi_comm_world.rank();
   int nprocs = mpi_comm_world.size();

   if (nprocs != 1) {
      if (my_rank == 0) {
         std::cerr << "Only one MPI process allowed at this moment!" << std::endl;
         mpi_comm_world.abort(EXIT_FAILURE);
      }
   }

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   std::vector<std::array<int, 6>> u3xsu2_subspaces = CreateListofU3SU2subspaces(argv[1]);
   /*
      for (const auto& u3su2 : u3xsu2_subspaces)
      {
         std::cout << u3su2[Nhw] << " " << u3su2[SSp] << " " << u3su2[SSn] << " " << u3su2[SS] << "
      " << u3su2[LM] << " " << u3su2[MU] << std::endl;
      }
   */
   std::vector<std::array<int, 5>> tensors = ReadListOfTensors(argv[2]);
   /*
      for (const auto& tensor : tensors)
      {
         std::cout << tensor[NA] << " " << tensor[NB] << " " << tensor[LM0] << " " << tensor[MU0] <<
      " " << tensor[SS0] << std::endl;
      }
   */

   std::ofstream output_file("/dev/null");
   bool allow_generate_missing_rme_files = (mpi_comm_world.size() == 1);
   CBaseSU3Irreps baseSU3Irreps(nprotons, nneutrons, Nmax);

   su3::init();
   for (const auto& tensor : tensors) {
      std::cout << "Tensor: {a+" << tensor[NA] << " x ta" << tensor[NB] << "}^(" << tensor[LM0]
                << " " << tensor[MU0] << ") 2S0:" << tensor[SS0] << std::endl;

      std::stringstream proton_filename;
      proton_filename << "a+" << tensor[NA] << "ta" << tensor[NB] << "_" << tensor[LM0] << "_"
                      << tensor[MU0] << "_" << tensor[SS0] << "_proton";
      std::stringstream neutron_filename;
      neutron_filename << "a+" << tensor[NA] << "ta" << tensor[NB] << "_" << tensor[LM0] << "_"
                       << tensor[MU0] << "_" << tensor[SS0] << "_neutron";

      int phase =
          CreateInputFileForDensity(tensor[NA], tensor[NB], tensor[LM0], tensor[MU0], tensor[SS0]);

      CInteractionPPNN interactionPPNN(baseSU3Irreps, true, output_file);
      // 	TODO: PN should be removed. It is redundant but for the sake of fast implementation
      // I am keeping it here.
      CInteractionPN interactionPN(baseSU3Irreps, allow_generate_missing_rme_files, true,
                                   output_file);
      try {
         COperatorLoader operatorLoader;
         //		TODO: implement a loading method that loads a single one-body operator
         operatorLoader.AddOneBodyOperator("dummy_ada", 1.0);
         operatorLoader.Load(0, interactionPPNN, interactionPN,
                             false);  // false ==> do not print output messages
      } catch (const std::logic_error& e) {
         std::cerr << e.what() << std::endl;
         env.abort(-1);
         return EXIT_FAILURE;
      }

      std::ofstream proton_file(proton_filename.str());
      if (!proton_file) {
         std::cerr << "Could not open file '" << proton_filename.str() << std::endl;
         return EXIT_FAILURE;
      }
      std::ofstream neutron_file(neutron_filename.str());
      if (!neutron_file) {
         std::cerr << "Could not open file '" << neutron_filename.str() << std::endl;
         return EXIT_FAILURE;
      }

      int dN = tensor[NA] - tensor[NB];
      for (const auto& bra_space : u3xsu2_subspaces) {
         for (const auto& ket_space : u3xsu2_subspaces) {
            std::cout << "<" << bra_space[Nhw] << "hw SSp:" << bra_space[SSp]
                      << " SSn:" << bra_space[SSn] << " SS:" << bra_space[SS] << "("
                      << bra_space[LM] << " " << bra_space[MU] << ")|";
            std::cout << " |" << ket_space[Nhw] << "hw SSp:" << ket_space[SSp]
                      << " SSn:" << ket_space[SSn] << " SS:" << ket_space[SS] << "("
                      << ket_space[LM] << " " << ket_space[MU] << ")> \t";
            if (bra_space[Nhw] != (ket_space[Nhw] + dN)) {
               std::cout << "HO energies do not couple" << std::endl;
               continue;
            }
            if (!SU2::mult(ket_space[SS], tensor[SS0], bra_space[SS])) {
               std::cout << "Spins do not couple" << std::endl;
               continue;
            }
            if (!SU3::mult(SU3::LABELS(ket_space[LM], ket_space[MU]),
                           SU3::LABELS(tensor[LM0], tensor[MU0]),
                           SU3::LABELS(bra_space[LM], bra_space[MU]))) {
               std::cout << "SU(3) irreps do not couple" << std::endl;
               continue;
            }

            ComputeRMEs(bra_space, tensor, ket_space, phase, interactionPPNN, interactionPN,
                        proton_file, neutron_file);

            std::cout << "DONE" << std::endl;
         }
      }
   }
   su3::finalize();
   /*
      CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();
      CSSTensorRMELookUpTablesContainer::ReleaseMemory();

      CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();
   */
   return EXIT_SUCCESS;
}
