#include <array>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <unordered_map>
#include <vector>

#include <boost/functional/hash.hpp>

#include <boost/math/constants/constants.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/number.hpp>

namespace mp = boost::multiprecision;

// arbitrary precision integer
using INT = mp::cpp_int;

// Floating-point types for <n nz m| n l m> calculation
// long double ... precision issues for n > 20
using FLOAT = mp::cpp_bin_float_double_extended;
// quad precision ... for n = 40, expanded states normalized with error up to 10^-14, n=50 with an error up to 3.8 x 10^-9
//using FLOAT = mp::cpp_bin_float_long_double;
// for n = 50 expanded states normalized to 10^-19
//using FLOAT = mp::number<mp::cpp_bin_float<200> >;

using std::abs;
using std::pow;
using std::sqrt;
using std::tgamma;

// vector with table of factorials represented by arbitrary precision integer
static std::vector<INT> ifac_table(200, 0);

// return n!
// QUESTION for Daniel: Why not to use boost::math:tgamma ?
INT ifac(int n) {
   // n beyond current limit of factorial table => resize
   if (n >= ifac_table.size()) ifac_table.resize(n + 1, 0);

   if (ifac_table[n] == 0) {  // => n! not computed yet
      INT temp = 1;
      for (int i = 2; i <= n; i++) temp *= i;
      ifac_table[n] = temp;
   }
   return ifac_table[n];
}

static std::vector<FLOAT> gamma_n_plus_half_table(200, 0.0);

static const FLOAT gamma05 = boost::math::tgamma(FLOAT(0.5));

// calculates Gamma(n + 1/2)
FLOAT gamma_n_plus_half(int n) {
   if (n >= gamma_n_plus_half_table.size()) gamma_n_plus_half_table.resize(n + 1, 0.0);

   if (gamma_n_plus_half_table[n] == 0.0) {
      FLOAT temp = 1;
      for (int i = 0; i < n; i++) temp *= FLOAT(i) + 0.5;
      gamma_n_plus_half_table[n] = temp * gamma05;
   }
   return gamma_n_plus_half_table[n];
}

// calculates (n + 1/2)!
FLOAT ffac_n_plus_half(int n) { return gamma_n_plus_half(n + 1); }

// calculates Gamma(n/2) where n is integer
FLOAT gamma_n_half(int n) {
   if (n % 2 == 1)
      return gamma_n_plus_half(n / 2);  // odd case
   else
      return FLOAT(ifac(n / 2 - 1));  // even case
}

// n_rho = 0, 1, 2, .... ==> this function returns integer
// User is responsible for checking that n - nz - |m| is an even number
int calculate_n_rho(int n, int n_z, int m) {
   assert((n - n_z - abs(m)) >= 0);
   assert(((n - n_z - abs(m))) % 2 == 0);
   return (n - n_z - abs(m)) / 2;
}

/**
 * n
 * n_z \in [0, n]
 * m \in [-l, l]
 * l \in {n, n - 2, n - 4, …, 0 or 1}
 *
 * n - n_z - |m| must be an even number
 */
long double nnzm2nlm(int n, int n_z, int l, int m) {
   assert((n - n_z - std::abs(m)) % 2 == 0);
   
   // 1/pi^(1/4)=pi^{-1/4}
   static const FLOAT pi_to_negative1o4 = mp::pow(boost::math::constants::pi<FLOAT>(), -0.25);

   FLOAT phase = 1.0;
   if (m < 0) {
      m = std::abs(m);
      if ((m % 2) == 1) phase = -1.0;
   }

   int n_rho = (n - n_z - std::abs(m)) / 2;
   int n_r = (n - l) / 2;

   FLOAT result = 0.0;
   for (int k_rho = 0; k_rho <= n_rho; ++k_rho) {
      FLOAT r_result = 0.0;
      for (int k_r = 0; k_r <= n_r; ++k_r) {
         FLOAT z_result = 0.0;
         for (int k_z = 0; k_z <= n_z / 2; ++k_z) {
            FLOAT l_result = 0.0;
            for (int k_l = 0; k_l <= (l - m) / 2; ++k_l)  // updated upper bound
            {
               FLOAT sub_result = 1.0;

               // sub_result *= pow(-1.0L, m + k_rho + k_r + k_z + k_l);
               if ((m + k_rho + k_r + k_z + k_l) % 2 == 1) sub_result *= -1;

               sub_result *= FLOAT(ifac(2 * l - 2 * k_l));
               sub_result /= FLOAT(ifac(l - k_l) * ifac(l - 2 * k_l - m) * ifac(k_l));

               sub_result *= gamma_n_half(n_z - 2 * k_z + l - 2 * k_l - m + 1);
               sub_result /= gamma_n_half(m + 2 * k_rho + n_z + l - 2 * k_z - 2 * k_l + 3);

               l_result += sub_result;
            }

            // multiply by 2^(n_z - 2k_z)
            INT i = 1;
            i <<= n_z - 2 * k_z;
            l_result *= FLOAT(i);

            l_result /= FLOAT(ifac(n_z - 2 * k_z) * ifac(k_z));
            l_result *= gamma_n_half(m + l + 2 * k_rho + 2 * k_r + n_z - 2 * k_z + 3);
            z_result += l_result;
         }

         z_result /= FLOAT(ifac(n_r - k_r)) * ffac_n_plus_half(k_r + l) * FLOAT(ifac(k_r));
         r_result += z_result;
      }

      r_result /= FLOAT(ifac(n_rho - k_rho) * ifac(k_rho));
      result += r_result;
   }

   result *= mp::sqrt((2.0 * FLOAT(ifac(n_r))) / gamma_n_plus_half(n_r + l + 1));
   result *= mp::sqrt(FLOAT((2 * l + 1) * ifac(l - m)) / FLOAT(ifac(l + m)));
   result *=
       mp::sqrt(FLOAT(ifac(n_rho)) / FLOAT((INT(1) << n_z) * ifac(n_z) * ifac(n_rho + m)));

   result *= (FLOAT(ifac(n_rho + m)) * ffac_n_plus_half(n_r + l) * FLOAT(ifac(n_z)));
   result /= FLOAT(INT(1) << (l + 1));

   result *= pi_to_negative1o4;

   result *= phase;

   return result.template convert_to<long double>();
}

// n1 = n
// n2 = n tilde
long double Ip(int n1, int n_z1, int n2, int n_z2, int m, long double omega_x,
                   long double omega) {
   int n_rho1 = calculate_n_rho(n1, n_z1, m);
   int n_rho2 = calculate_n_rho(n2, n_z2, m);

   long double result = 0.0L;
   for (int k_rho1 = 0; k_rho1 <= n_rho1; ++k_rho1) {
      for (int k_rho2 = 0; k_rho2 <= n_rho2; ++k_rho2) {
         // @todo use if with modulo instead of (-1)^x?
         long double sub_result = 1.0L;
         sub_result *= pow(-1.0L, k_rho1 + k_rho2);
         sub_result *= pow(2.0L, k_rho1 + k_rho2 + m);
         sub_result *= (long double)ifac(n_rho1 + m) / (long double)(ifac(n_rho1 - k_rho1) * ifac(k_rho1 + m) * ifac(k_rho1));
         sub_result *= (long double)ifac(n_rho2 + m) / (long double)(ifac(n_rho2 - k_rho2) * ifac(k_rho2 + m) * ifac(k_rho2));
         sub_result *= (long double)ifac(k_rho1 + k_rho2 + m);
         sub_result *= (pow(omega, k_rho1) * pow(omega_x, k_rho2)) /
                       pow(omega + omega_x, k_rho1 + k_rho2 + m + 1.0L);
         result += sub_result;
      }
   }
   return result;
}

// n_z1 = n_z
// n_z2 = n_z tilde
long double Iz(int n_z1, int n_z2, long double omega_z, long double omega) {
   long double result = 0.0L;
   for (int k1 = 0; k1 <= n_z1 / 2; ++k1) {
      // this follows from \delta_{n_z1 - 2*k1, n_z2 - 2*k2} ==> k2 = (n_z2 - n_z1 ....)/2
      int k2 = (n_z2 - n_z1 + 2 * k1);
      // k2 is an integer such that 0 <= k2 <= n_2
      if (k2 < 0 || k2 > n_z2) 
      {
         continue;
      }
      if (k2 % 2) // k2 = (dnz - nz + 2k1)/2 is not an integer
      {
         continue;
      }

      k2 /= 2;

      long double sub_result = 1.0L;
      sub_result *= pow(-1.0L, k2);
      sub_result *= pow(2.0L, n_z1 - 2 * k1);
      sub_result *= pow(sqrt((2.0L * omega) / (omega + omega_z)), n_z1 - 2 * k1);
      sub_result *= pow(sqrt((2.0L * omega_z) / (omega + omega_z)), n_z2 - 2 * k2);
      sub_result *= pow((omega - omega_z) / (omega + omega_z), k1 + k2);
      sub_result *= (long double)(ifac(n_z2) * ifac(n_z1)) / (long double)(ifac(n_z1 - 2 * k1) * ifac(k1) * ifac(k2));
      result += sub_result;
   }

   result *= sqrt((2.0L * boost::math::constants::pi<long double>()) / (omega + omega_z));

   return result;
}

// n1   --->  n
// n_z1 --->  nz
// n2   ---> ~n
// n_z2 ---> ~nz
// This function computes the following transformation bracket: <~n ~nz m | n nz m>
// Note: long double precision for floating points operations 
// Note: boost::multiprecision::cpp_int is used to store factorials
long double dndnzm2nnzm(int n2, int n_z2, int n1, int n_z1, int m, long double omega_x,
                        long double omega_z, long double omega) {
   assert((n2 - n_z2 - abs(m)) % 2 == 0);
   assert((n2 - n_z2 - abs(m)) >= 0);
   assert((n1 - n_z1 - abs(m)) % 2 == 0);
   assert((n1 - n_z1 - abs(m)) >= 0);

   if ((n_z2 % 2) != (n_z1 % 2)) {
      return 0.0;
   }

   m = abs(m);

   int n_rho1 = calculate_n_rho(n1, n_z1, m);
   int n_rho2 = calculate_n_rho(n2, n_z2, m);

   long double result = 1.0L;

   // 2.0/sqrt(M_PI) * (w wx)^(|m|+1)/2
   result *= (2.0L / boost::math::constants::root_pi<long double>()) * pow(omega * omega_x, (m + 1.0L) / 2.0L);
   result *= pow(omega * omega_z, 0.25L);
   result *= sqrt((long double)ifac(n_rho2) / (pow(2.0L, n_z2) * (long double)(ifac(n_z2) * ifac(n_rho2 + m))));
   result *= sqrt((long double)ifac(n_rho1) / (pow(2.0L, n_z1) * (long double)(ifac(n_z1) * ifac(n_rho1 + m))));
   result *= Ip(n1, n_z1, n2, n_z2, m, omega_x, omega);
   result *= Iz(n_z1, n_z2, omega_z, omega);

   return result;
}

// <~n ~l m | n l m>
long double dndlm2nlm(int dn, int dl, int n, int l, int m, long double omega_x,
                     long double omega_z, long double omega) {
   long double result = 0.0L;
   int absm = abs(m);
   // 0 <= (~n - ~nz - |m|)/2 which must be an integer
   for (int dn_z = (dn - absm)%2; dn_z <= (dn - absm); dn_z += 2) {
      // result_dnz = \sum_{n_z} <~n ~nz m | n nz m><n nz m|n l m>
      long double result_dnz = 0.0L;
      // since |m| <= n - nz ==> nz <= (n - |m|)
      // <~n ~nz m | n nz m> vanishes if ~nz and nz are not both even or odd.
      // this loop iterates over odd or even numbers depending if ~nz is odd or even
      for (int n_z = (dn_z % 2); n_z <= (n - absm); n_z += 2) {
         // n_rho1 must be an integer
         if ((n - n_z - absm) % 2) {
            continue;
         }
         long double sub_result = 1.0L;
         // <~n ~nz m | n nz m>
         sub_result *= dndnzm2nnzm(dn, dn_z, n, n_z, m, omega_x, omega_z, omega);
         // <n nz m| n l m>
         sub_result *= nnzm2nlm(n, n_z, l, m);
         result_dnz += sub_result;
      }
       // <~n ~nz m | ~n ~l m> = <~n ~l m | ~n ~nz m>
      result_dnz *= nnzm2nlm(dn, dn_z, dl, m);
      result += result_dnz;
   }
   return result;
}



struct nlm_spherical {
   int n;
   int l;
   int m;

   nlm_spherical(int n, int l, int m) : n(n), l(l), m(m) {}
};

struct nlm_deformed {
   int n;
   int l;
   int m;

   nlm_deformed(int n, int l, int m) : n(n), l(l), m(m) {}
};

template <typename T>
std::vector<T> generate_nlms(int n_max) {
   std::vector<T> nlms;
   for (int n = 0; n <= n_max; ++n) {
      for (int l = n % 2; l <= n; l += 2) {
         for (int m = -l; m <= l; ++m) {
            if (m >= 0) {
               nlms.emplace_back(n, l, m);
            }
         }
      }
   }
   return nlms;
}

long double dndlm2nlm(const nlm_deformed& nlm_def, const nlm_spherical& nlm_spher,
                      long double omega_x, long double omega_z, long double omega) {
   int dn = nlm_def.n;
   int dl = nlm_def.l;

   int n = nlm_spher.n;
   int l = nlm_spher.l;
   int m = nlm_spher.m;

//   return overlap_(n, l, dn, dl, m, omega_x, omega_z, omega);
   return dndlm2nlm(dn, dl, n, l, m, omega_x, omega_z, omega);
}


void test_dndlm2nlm(int argc, char** argv)
{
   if (argc != 5)
   {
      std::cout << "Usage: " << argv[0] << " <w>  <wz>  <~nmax>  <nmax >" << std::endl;
      std::cout << "Expand |~n ~l m> basis states as \\sum_{n=0}^{n_max} \\sum_{l} <n l m| ~n ~l m> |n l m>" << std::endl;
      std::cout << "Test whether \\sum_{n=0}^{n_max} \\sum_{l} | <n l m| ~n ~l m> |^2 ~ 1" << std::endl;
      return;
   }
   // if ((n_z + l - m) is odd) result = 0
   // n nz m l
   std::ios_base::sync_with_stdio(false);
   std::cout << std::setprecision(std::numeric_limits<long double>::digits10 + 1);

   long double omega = atof(argv[1]);
   long double omega_z = atof(argv[2]);
   // expand |~n ~l m> = \sum_{n l m} <n l m| ~n ~l m> |n l m>
   // for ~n <= dnmax
   int dnmax = atoi(argv[3]);
   int nmax = atoi(argv[4]);

   long double omega_x = sqrt(pow(omega, 3) / omega_z);

   std::cout << "w: " << omega << " MeV" << std::endl;
   std::cout << "wz: " << omega_z << " MeV" << std::endl;
   std::cout << "wx = wy: " << omega_x << " MeV" << std::endl;
   std::cout << "Expanding |~n ~l m> states up to ~n_max:" << dnmax << std::endl;
   std::cout << "Testing if \\sum_{n=0}^{n_max} \\sum_{l} |<n l m|~n ~l m>|^2 ~ 1." << dnmax << std::endl;

   std::vector<nlm_deformed> deformed_nlms = generate_nlms<nlm_deformed>(dnmax);
   std::vector<nlm_spherical> nlms = generate_nlms<nlm_spherical>(nmax);

   for (const auto& nlm_def : deformed_nlms) {
      std::cout << "Expanding |~n:" << nlm_def.n << "~l:" << nlm_def.l << " m:" << nlm_def.m << ">" << std::endl;
      long double norm = 0.0L;
      for (const auto& nlm_spher : nlms) {
         if (nlm_def.m != nlm_spher.m) {
            continue;
         }

         // check that deformed and spherical shells have the same parity
         if (abs(nlm_def.n - nlm_spher.n) % 2 == 1) {
            continue;
         }

         // <n l m | ~n ~l m> is equal to <~n ~l m|n l m>
         long double overlap_result = dndlm2nlm(nlm_def, nlm_spher, omega_x, omega_z, omega);
         norm += overlap_result*overlap_result;
         std::cout << std::setw(2) << nlm_def.n << ' ' << std::setw(2) << nlm_def.l << ' '
                   << std::setw(2) << nlm_def.m << ' ' << std::setw(2) << nlm_spher.n << ' '
                   << std::setw(2) << nlm_spher.l << ' ' << std::setw(2) << nlm_spher.m << ' '
                   << std::setw(2) << overlap_result << std::endl;
      }
      std::cout << std::setw(2) << nlm_def.n << ", " << std::setw(2) << nlm_def.l
                << ", " << std::setw(2) << nlm_def.m << "  norm:" << norm << '\n';
   }
}


// Test <n nz m | n l m> values by generating transformation 
// brackets for a given shell n and checking whether
// \sum_{l} |<n nz m|n l m>|^2 = 1 for all possible |n nz m> states.
void test_nnzm2nlm(int n) {
   long double max_norm_m_1 = 0.0;
   std::cout << "Printing all <n nz m | n l m> values for harmonic oscillator shell n:" << n << std::endl;
   // iterate over <n nz m| basis states
   // first loop over allowd nz values
   for (int nz = 0; nz <= n; ++nz) {
      // nrho = (n - nz - |m|)/2
      // nrho can take only positive integers
      // As a result
      // 1) (n - nz) >= |m|
      // 2) (n - nz - |m|) is an even number
    
      // absm = |m| <= (n - nz)
      for (int absm = 0; absm <= (n - nz); ++absm) {
         if ((n - nz - absm) % 2) {  // (n - nz - |m|) is an odd number
            continue;
         }
         int nrho = (n - nz - absm) / 2;
         for (int m = -absm; m <= absm; m += 2 * absm) {
            std::cout << "Expanding |n:" << n << " nz:" << nz << " m:" << m << "> state" << std::endl;
// for a given <n nz m| state iterate over all |n l m> states and compute
// <n nz m|n l m> and its square. 
// |n nz m> = \sum_{l} <n nz m|n l m> |n l m>
            long double norm = 0;
            for (int l = n % 2; l <= n; l += 2) {
               if (abs(m) <= l) {
//                  long double overlap = nnzm2nlm_old(n, nz, l, m);
                  long double overlap = nnzm2nlm(n, nz, l, m);
                  std::cout << "<" << n << " " << nz << " " << m << "|" << n << " " << l << " " << m
                            << ">:" << overlap << std::endl;
                  norm += overlap * overlap;
               }
            }
            long double norm_m_1 = fabs(norm - 1.0L);
// Check if \sum_{l} |<n nz m| n l m>|^2 = 1            
            std::cout << "norm:" << norm << ", |norm-1|: " << norm_m_1 << std::endl;
            if (norm_m_1 > max_norm_m_1)
            {
               max_norm_m_1 = norm_m_1;
            }
            if (m == 0) {
               break;
            }
         }
      }
   }
   std::cout << "Maximal value of |norm - 1|:" << max_norm_m_1 << std::endl;
}

// Test <n nz m | n l m> results against the values provided by 
// S. Dong, Phys. Let. A 376 (2012) 1262–1268.
void test_nnzm2nlm_Shi() {
   // vector of {n, nz, l, m} labels associated with particular <n nz m | n l m> transformation bracket
   std::vector<std::array<int, 4>> labels_set = {
       {1, 0, 1, 1},  {1, 1, 1, 0},  {1, 0, 1, -1}, {2, 0, 2, 2},  {2, 1, 2, 1},  {2, 2, 2, 0},
       {2, 2, 0, 0},  {2, 0, 2, 0},  {2, 0, 0, 0},  {2, 1, 2, -1}, {2, 0, 2, -2}, {3, 0, 3, 3},
       {3, 1, 3, 2},  {3, 2, 3, 1},  {3, 2, 1, 1},  {3, 0, 3, 1},  {3, 0, 1, 1},  {3, 1, 3, 0},
       {3, 1, 1, 0},  {3, 3, 3, 0},  {3, 3, 1, 0},  {3, 2, 3, -1}, {3, 2, 1, -1}, {3, 0, 3, -1},
       {3, 0, 1, -1}, {3, 1, 3, -2}, {3, 0, 3, -3}};
   // holds <n nz m | n l m> transformation bracket values as provided by S. Dong
   // labels_set[index] ---> {n, nz, l m} 
   // resultsShi[index] = <n nz m | n l m>
   std::vector<long double> resultsShi = {-1,
                                          1,
                                          1,
                                          +1,
                                          -1,
                                          sqrt(2.0 / 3.0),
                                          -sqrt(1.0 / 3.0),
                                          sqrt(1.0 / 3.0),
                                          sqrt(2.0 / 3.0),
                                          +1,
                                          +1,
                                          -1,
                                          1,
                                          -sqrt(4.0 / 5.0),
                                          sqrt(1.0 / 5.0),
                                          -sqrt(1.0 / 5.0),
                                          -sqrt(4.0 / 5.0),
                                          sqrt(3.0 / 5.0),
                                          sqrt(2.0 / 5.0),
                                          sqrt(2.0 / 5.0),
                                          -sqrt(3.0 / 5.0),
                                          sqrt(4.0 / 5.0),
                                          -sqrt(1.0 / 5.0),
                                          sqrt(1.0 / 5.0),
                                          sqrt(4.0 / 5.0),
                                          1,
                                          1};
   // iterate over S. Dong's results and compare
   // with results of our subroutine
   size_t index = 0;
   for (auto labels : labels_set) {
      int n = labels[0];
      int nz = labels[1];
      int l = labels[2];
      int m = labels[3];
      long double result = nnzm2nlm(n, nz, l, m);
//      long double result = nnzm2nlm_old(n, nz, l, m);
      if (fabs(result - resultsShi[index]) > 1.0e-16) {
         std::cerr << std::setprecision(26);
         std::cerr << "Test failure! for <n:" << n << " nz:" << nz << " m:" << m << "|n:" << n
                   << " l:" << l << " m:" << m << ">" << std::endl;
         std::cerr << "Our result:" << result << " Shi's result:" << resultsShi[index] << std::endl;
         exit(EXIT_FAILURE);
      } else {
         std::cout << "<" << n << " " << nz << " " << m << "|" << n << " " << l << " " << m
                   << ">   " << result << " check:" << resultsShi[index] << std::endl;
      }
      index++;
   }
   std::cout << "Test successfull" << std::endl;
}

void test_dndnzm2nnzm() {
   long double wx, wz, w;
   std::cout << "omega:";
   std::cin >> w;
   std::cout << "omega_z:";
   std::cin >> wz;
   wx = sqrt(pow(w, 3) / wz);

   int n, nz;
   std::cout << "Generate non-deformed |n nz m> states for n:";
   std::cin >> n;

   int dnmax;
   std::cout << "Expansion in deformed basis |dn dnz m> up to dnmax:";
   std::cin >> dnmax;

   // for a given n iterate over |n nz m> states
   for (int nz = 0; nz <= n; ++nz) {
      for (int absm = 0; absm <= (n - nz); ++absm) {
         if ((n - nz - absm) % 2)  // ==> n_rho not an integer
         {
            continue;
         }
         for (int m = -absm; m <= absm; m += 2 * absm) {
            std::cout << "Expansion of |" << n << " " << nz << " " << m << "> state:" << std::endl;
            long double prob = 0.0;
            for (int dn = 0; dn <= dnmax; ++dn) {
               for (int dnz = 0; dnz <= dn; ++dnz) {
                  if (abs(m) > (dn - dnz))  // ==> n_rho < 0 !
                  {
                     break;
                  }
                  // n_rho2 must be an integer
                  if ((dn - dnz - abs(m)) % 2)  // ==> n_rho is not an integer
                  {
                     continue;
                  }
                  if ((dnz % 2) != (nz % 2)) {
                     continue;
                  }

                  long double coeff = dndnzm2nnzm(dn, dnz, n, nz, m, wx, wz, w);
                  prob += (coeff * coeff);

                  // What if a given |dn dnz m> does not exist!!!!
                  std::cout << "<" << dn << " " << dnz << " " << m << "|" << n << " " << nz << " "
                            << m << ">:";
                  std::cout << coeff << "\t" << prob << "\t" << fabs(prob - 1.0) << std::endl;
               }
            }
            if (m == 0) {
               break;
            }
         }
      }
   }
}

int main(int argc, char** argv) {
   test_dndlm2nlm(argc, argv);
   return 1;
//   test_nnzm2nlm_Shi();
//   return 1;
   int n = atoi(argv[1]);
   test_nnzm2nlm(n);
}
