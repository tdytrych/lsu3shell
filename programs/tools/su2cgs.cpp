#include <SU3NCSMUtils/clebschGordan.h>
#include <su3.h>
#include <iostream>

using namespace std;

int main() 
{
   su3::init();
	int j1, j2, j3;
	int mj1, mj2, mj3;
	do 
	{
	    cout << "Enter 2*j1" << endl;
	    cin >> j1;
	    cout << "Enter 2*mj1" << endl;
	    cin >> mj1;
	    cout << "Enter 2*j2" << endl;
	    cin >> j2;
	    cout << "Enter 2*mj2" << endl;
	    cin >> mj2;
	    cout << "Enter 2*j3" << endl;
	    cin >> j3;
	    cout << "Enter 2*mj3" << endl;
	    cin >> mj3;

		cout << "<" << j1 << "/2 " << mj1 << "/2 " << j2 << "/2 " << mj2 << "/2|" << j3 << "/2 " << mj3 << "/2 > = ";
		cout << clebschGordan(j1, mj1, j2, mj2, j3, mj3) << endl;

	} while(true);
   su3::finalize();
}
