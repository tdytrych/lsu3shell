#include <UNU3SU3/CSU3Master.h>

#include <su3.h>

#include <iostream>
#include <algorithm>

using namespace std;

int main() 
{
	int lm1, mu1, lm2, mu2, lm3, mu3;
	vector<double> Phi;
	size_t nPhi;
	CSU3CGMaster SU3Lib;

    cout << "Enter (lm1 mu1) " << endl;
    cin >> lm1 >> mu1;
    cout << "Enter (lm2 mu2) " << endl;
    cin >> lm2 >> mu2;
    cout << "Enter (lm3 mu3) " << endl;
    cin >> lm3 >> mu3;

	SU3::LABELS ir1(lm1, mu1);
	SU3::LABELS ir2(lm2, mu2);
	SU3::LABELS ir3(lm3, mu3);

	int rho0max = SU3::mult(ir1, ir2, ir3);
	int rho0pmax = rho0max;
//	nPhi = rho12max * rho12_3max * rho13max   * rho13_2max;
//			   |             |          |          |
//		       |           rho0         |        rho0p 
//			   |             |          |          |
//			   V             V          V          V
	nPhi =     1     *    rho0max  *    1   *  rho0pmax;
	if (!rho0max) 
	{
		cerr << ir1 << " x " << ir2 << " does not couple to " << ir3 << endl;
	}
	Phi.resize(nPhi);
   su3::init();
	SU3Lib.GetPhi(ir1, ir2, ir3, Phi);
   su3::finalize();

	cout << "Phi[(lm1 mu1), (lm2 mu2), (lm3, mu3)]" << endl;
//	int na = rho12max;
//              |	
//              |	
//              |	
//              V
//	int na =    1;
//	int nb = rho12_3max*na;
//              |	     |
//              |	     |
//              |	     |
//              V        V
//	int nb = rho0max   * 1;
//	int nc = rho23max*nb;
//              |      |
//              |      |
//              V      V
//	int nc =    1 * rho0max;

	cout << rho0max << endl;
	for (size_t irho0 = 0; irho0 < rho0max; ++irho0)
	{		
		for (size_t irho0p = 0; irho0p < rho0pmax; ++irho0p)
		{
//			index =   rho12 + rho12_3 * na + rho13 * nb     + rho13_2 *  nc; 
//		        	    |        |      |       |                |        |
//			            |        |      |       |                |        |
//	      			    |        |      |       |                |        |
//	       			    V        V      V       V                V        V
//			int i =     0   +  irho0 *  1 +     0 * rho0max + irho0p  * rho0max; 
			int i = irho0p*rho0max + irho0;
			if (i == 0) 
			{
				cout << "Phi[" << ir1 << "; " << ir2 << "; " << ir3 << "]_{rho0 = " << irho0 + 1 << " rho0p = " << irho0p+1; 
				cout << "} = " << Phi[i] << "\t\t" << "index = " << i << endl;
			}
			else 
			{
				cout << "\t\t\t\t " << irho0 + 1 << "         " << irho0p + 1 << "  " << "\t" << Phi[i]  << "\t\t" << "index = " << i << endl;
			}
		}
	}
}
