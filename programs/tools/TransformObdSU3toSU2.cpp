#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3ME/SU3xSU2Basis.h>
#include <UNU3SU3/CSU3Master.h>
#include <LookUpContainers/CSU39lm.h>

#include <algorithm>

#define SU3SHELL_NATIVE
//#define MFDN_NATIVE

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;
using std::map;

typedef CTuple<int, 7> SU3KEY;	// na nb lm0 mu0 SS0 k0 LL0 

CSU3CGMaster SU3CG;

//	Load a SU(3) obd file for a certain {<Jf|, |Ji>} pair.
void LoadSU3obdInputFile(const string& filename, vector<map<SU3KEY, double> >& proton_su3obd, vector<map<SU3KEY, double> >& neutron_su3obd)
{
	string type;
	int JJ0;
	cout << "Opening " << filename << endl;

	ifstream input_file(filename.c_str());
	if (!input_file)
	{
		cerr << "Could not open '" << filename << "' input file!" << endl;
		exit(EXIT_FAILURE);
	}
	int number_su3obd;
	double value;

	input_file >> number_su3obd;
	for (int i = 0; i < number_su3obd; ++i)
	{
		SU3KEY key;
		input_file >> key[0]; // na
		input_file >> key[1]; // nb
		input_file >> key[2]; // lm0
		input_file >> key[3]; // mu0
		input_file >> key[4]; // SS0
		input_file >> key[5]; // k0
		input_file >> key[6]; // LL0
		input_file >> JJ0;
		input_file >> type;
		input_file >> value;
		if (type == "p")
		{
			proton_su3obd[JJ0].insert(make_pair(key, value));
		}
		else if (type == "n")
		{
			neutron_su3obd[JJ0].insert(make_pair(key, value));
		}
	}
}

double CalculateSU2Obd(int na, int lla, int jja, int nb, int llb, int jjb, int JJ0, vector<map<SU3KEY, double> >& su3obdme)
{
	assert(SU2::mult(jja, jjb, JJ0));

	SU3KEY key;
	key[0] = na;
	key[1] = nb;

	double result(0.0);
	double su3cg;
	SU3::LABELS ir1(1, na, 0), ir2(1, 0, nb);

	SU3xSU2_VEC lm0mu0_irreps;
	SU3xSU2::Couple(SU3xSU2::LABELS(1, na, 0, 1), SU3xSU2::LABELS(1, 0, nb, 1), lm0mu0_irreps);
	for (int i = 0; i < lm0mu0_irreps.size(); ++i)
	{
		SU3xSU2::BasisJfixed lm0mu0(lm0mu0_irreps[i], JJ0);
		if (lm0mu0.dim() == 0)
		{
			continue;
		}
		key[2] = lm0mu0.lm;
		key[3] = lm0mu0.mu;
		key[4] = lm0mu0.S2;

		vector<pair<K1L1K2L2K3L3, vector<double> > > vCGs;// vector of SU(3) Wigner coefficients. Each element has
		SU3CG.GetSO3(ir1, ir2, lm0mu0, vCGs);

		std::sort(vCGs.begin(), vCGs.end(), less_first<K1L1K2L2K3L3>());

		for (lm0mu0.rewind(); !lm0mu0.IsDone(); lm0mu0.nextL())
		{
			int LL0 = lm0mu0.L();
			key[6] = LL0;
			if (!SU2::mult(lla, llb, LL0))
			{
				continue;
			}

			double dPi = std::sqrt((jja + 1.0)*(jjb + 1)*(LL0 + 1)*(lm0mu0.S2 + 1));
			double dSu2Wig9j = wigner9j(lla, llb, LL0, 1, 1, lm0mu0.S2, jja, jjb, JJ0);
			for (int k0 = 0; k0 < lm0mu0.kmax(); ++k0)
			{
				K1L1K2L2K3L3 su3cgkey(0, lla, 0, llb, k0, LL0);
				vector<pair<K1L1K2L2K3L3, vector<double> > >::const_iterator cit = std::lower_bound(vCGs.begin(), vCGs.end(), su3cgkey, key_less<K1L1K2L2K3L3>());
				if (cit != vCGs.end() && su3cgkey == cit->first)
				{
					assert(cit->second.size() == 1); // cit->second.size() = rh0_max for coupling (na 0) x (0 nb) .... always 1
					su3cg = cit->second[0];
				}
				else
				{
					cout << "su3library returns all SU(3) coefficients. So how come that this SU(3) cgs does not exist ???";
					su3cg = 0.0;
				}
				key[5] = k0;
				map<SU3KEY, double>::const_iterator it = (su3obdme[JJ0]).find(key);
				double obdme;
				if (it == (su3obdme[JJ0]).end())
				{
					cout << "not found" << endl;
				}
				else
				{
					obdme = it->second;
				}
				result += dPi*su3cg*dSu2Wig9j*obdme;
			}
		}
	}
	
// since su3shell code uses positive at infinity 
// convention we implicitly have the following
// preprocessor directive commented
// #define POSITIVE_AT_ORIGIN  

// However, it seems that Anna Hayes codes are compatible with this convention
#define POSITIVE_AT_ORIGIN  

#ifdef POSITIVE_AT_ORIGIN
	int phase1 = (na - lla/2 + nb - llb/2)/2;
	result *= MINUSto(phase1);
#endif

#ifdef SU3SHELL_NATIVE	
	return (+1)*MINUSto(nb)*result;
#elif defined(MFDN_NATIVE) 
	return (-1)*MINUSto(nb)*result;
#endif	
}

int main(int argc,char **argv)
{
   su3::init();
	if (argc != 3)
	{
		cout << "Usage: "<< argv[0] <<" <filename: SU(2) obd labels>  <filename: SU(3) obds" << endl;
		cout << "Structure of the input file:" << endl;
		cout << "<filename: SU(2) obd labels> .... list of {na lla jja nb jjb llb JJ0 p/n} obd to be calculated" << endl;
		cout << "<filename: SU(3) obds> .... list of {na nb lm0 mu0 2S0 k0 2L0 2J0 'p/n'  < af Jf|| || ai Ji> }  with a fixed af Jf and ai Ji." << endl;
		return EXIT_FAILURE;
	}
//	open file with the list of su2 obds, i.e. na lla jja nb llb jjb JJ0 	
	ifstream su2obd_list_file(argv[1]);
	if (!su2obd_list_file)
	{
		cerr << "Could not open '" << argv[1] << "' input file!" << endl;
		return EXIT_FAILURE;
	}


//	obdsu3_p/n[JJ0] = maps of {na nb lm0 mu0 SS0 k0 LL0} -----> < Jf ||{a+a}^(lm0 mu0)|| Ji>
	vector<map<SU3KEY, double> > obdsu3_p(100, map<SU3KEY, double>());
	vector<map<SU3KEY, double> > obdsu3_n(100, map<SU3KEY, double>());

//	Load su3 obds {na nb lm0 mu0 SS0 k0 LL0} --> <Jf ||  || Ji>	from the file 'su3odb_file_name' into 
//	the two vectors of maps associated with protons and neutrons.
	LoadSU3obdInputFile(argv[2], obdsu3_p, obdsu3_n);

//	open file with the list of su2 obds, i.e. na lla jja nb llb jjb JJ0 	
	int number_su2obds;
//	read how how many su2 obds will be needed to generate	
	su2obd_list_file >> number_su2obds;
	for (int i = 0; i < number_su2obds; ++i)
	{
		int na, lla, jja, nb, llb, jjb, JJ0;
		string type;
		su2obd_list_file >> na >> lla >> jja >> nb >> llb >> jjb >> JJ0 >> type;
		cout << "na:" << na << "  lla:" << lla << "  jja:"  << jja << "  nb:" << nb << "  llb:" << llb << "  jjb:" << jjb << "  JJ0:" << JJ0 << "  " << type;
		if (type == "p")
		{
			cout << "\t\t" << CalculateSU2Obd(na, lla, jja, nb, llb, jjb, JJ0, obdsu3_p) << endl;
		}
		else if (type == "n")
		{
			cout << "\t\t" << CalculateSU2Obd(na, lla, jja, nb, llb, jjb, JJ0, obdsu3_n) << endl;
		}
	}
#ifdef POSITIVE_AT_ORIGIN
	cout << "\%\%  HO single-particle wfns are considered positive at origin" << endl; 
#else
	cout << "\%\%  HO single-particle wfns are considered positive at infinity (su3shell convention)." << endl; 
#endif

#ifdef SU3SHELL_NATIVE	
	cout << "\%\% Convention: a_{nljm}=(-)^(j-m) a_{j-m} (su3shell convention}" << endl; 
//	return +MINUSto(nb)*result;
#elif defined(MFDN_NATIVE) 
	cout << "\%\% Convention: a_{nljm}=(-)^(j+m) a_{j-m}" << endl; 
//	return (-1)*MINUSto(nb)*result;
#endif	
   su3::finalize();
}
