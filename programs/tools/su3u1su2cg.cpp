#include <UNU3SU3/CSU3Master.h>

#include <su3.h>

#include <algorithm>
#include <iostream>

using namespace std;

void ShowWignerSU3U1SU2coeffs(const SU3::LABELS& ir1, const SU3::LABELS& ir2,
                              const SU3::LABELS& ir3) {
   int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
   vector<pair<EPS1LM1EPS2LM2EPS3LM3, std::vector<double> > > vCGs;
   CSU3CGMaster SU3CG;
   su3::init();
   size_t n = SU3CG.GetSU2U1(ir1, ir2, ir3, vCGs);
   su3::finalize();
   int eps1, eps2, eps3, LM1, LM2, LM3, rho_max;

   cout << "<lm1mu1 eps1 LM1;lm2mu2 eps2 LM2||lm3mu3 eps3 LM3>rho" << endl;

   //	cout.precision(std::numeric_limits<double>::digits10);
   cout.precision(15);

   for (size_t i = 0; i < n; i++) {
      EPS1LM1EPS2LM2EPS3LM3 Key(vCGs[i].first);
      vector<double> CGrho(vCGs[i].second);
      rho_max = CGrho.size();
      eps1 = Key.eps1();
      eps2 = Key.eps2();
      eps3 = Key.eps3();

      LM1 = Key.LM1();
      LM2 = Key.LM2();
      LM3 = Key.LM3();

      for (int irho = 0; irho < rho_max; irho++)  // print out CGs
      {
         if ((i == 0) && (irho == 0)) {
            cout << " " << lm1 << "  " << mu1 << "   " << eps1;
            cout << "  " << LM1 << "  " << lm2 << "  " << mu2 << "   " << eps2 << "  " << LM2
                 << "   ";
            cout << lm3 << "  " << mu3 << "   " << eps3 << "  " << LM3 << "  " << (irho + 1) << "\t"
                 << CGrho[irho] << endl;
         } else if (irho == 0) {
            cout << "        " << eps1 << "  " << LM1 << "         " << eps2 << "  " << LM2;
            cout << "          " << eps3 << "  " << LM3 << "  " << (irho + 1) << "\t" << CGrho[irho]
                 << endl;
         } else {
            cout << "                                         " << (irho + 1) << "\t" << CGrho[irho]
                 << endl;
         }
      }
      cout << endl;
   }
}

int main() {
   int lm1, mu1, lm2, mu2, lm3, mu3;

   cout << "Enter (lm1 mu1) " << endl;
   cin >> lm1 >> mu1;
   cout << "Enter (lm2 mu2) " << endl;
   cin >> lm2 >> mu2;
   cout << "Enter (lm3 mu3) " << endl;
   cin >> lm3 >> mu3;

   SU3::LABELS ir1(lm1, mu1);
   SU3::LABELS ir2(lm2, mu2);
   SU3::LABELS ir3(lm3, mu3);

   int rho_max = SU3::mult(ir1, ir2, ir3);
   if (!rho_max) {
      cout << "(" << ir1.lm << " ";
      cout << ir1.mu << ") x "
           << "(";
      cout << ir2.lm << " " << ir2.mu << ") does not yield (" << ir3.lm << " " << ir3.mu << ")"
           << endl;
      return 0;
   }

   ir3.rho = 0;
   ShowWignerSU3U1SU2coeffs(ir1, ir2, ir3);
   return EXIT_SUCCESS;
}
