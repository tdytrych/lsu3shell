#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/ModelSpaceExclusionRules.h>
#include <SU3ME/global_definitions.h>
#include <ctime>
#include <stack>
#include <vector>
using namespace std;

void ListBasisStateLabels(const lsu3::CncsmSU3xSU2Basis& basis) {
   //	loop over (ip, in) pairs
   for (int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++) {
      if (!basis.NumberOfStatesInBlock(ipin_block)) {
         continue;
      }
      uint32_t ip = basis.getProtonIrrepId(ipin_block);
      uint32_t in = basis.getNeutronIrrepId(ipin_block);

      int N = basis.nhw_p(ip) + basis.nhw_n(in);

      uint16_t ap_max = basis.getMult_p(ip);
      uint16_t an_max = basis.getMult_n(in);
      SU3xSU2::LABELS irrep_p(basis.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS irrep_n(basis.getNeutronSU3xSU2(in));
      int ssp(irrep_p.S2);
      int ssn(irrep_n.S2);

      for (int iwpn = basis.blockBegin(ipin_block); iwpn < basis.blockEnd(ipin_block); ++iwpn) {
         SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
         int lm = omega_pn.lm;
         int mu = omega_pn.mu;
         int ss = omega_pn.S2;
         int a0_max = ap_max * an_max * omega_pn.rho;

         IRREPBASIS su3xsu2_basis(basis.Get_Omega_pn_Basis(iwpn));
         for (su3xsu2_basis.rewind(); !su3xsu2_basis.IsDone(); su3xsu2_basis.nextL()) {
            int ll = su3xsu2_basis.L();
            for (int k = 0; k < su3xsu2_basis.kmax(); ++k) {
               for (int a0 = 0; a0 < a0_max; ++a0) {
                  cout << N << " " << ssp << " " << ssn << " " << ss << " " << lm << " " << mu
                       << " " << k << " " << ll << " " << a0 << std::endl;
               }
            }
         }
      }
   }
}

int main(int argc, char** argv) {
   if (argc != 2) {
      cout << "List quantum labels Nhw 2Sp 2Sn 2S lm mu k 2L a0 of many-nucleon basis states.\n";
      cout << "An ith row carries out labels of ith basis state. All states carry a constant value "
              "of J. \n";
      cout << "Usage: " << argv[0] << " <model space definition>" << endl;
      return EXIT_FAILURE;
   }

   proton_neutron::ModelSpace ncsmModelSpace(argv[1]);
   lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);
   cout << "Nhw 2Sp 2Sn 2S lm mu k 2L a0" << endl;
   ListBasisStateLabels(basis);
   return EXIT_SUCCESS;
}
