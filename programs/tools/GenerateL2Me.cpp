#include <cmath>
#include <SU3NCSMUtils/clebschGordan.h>
#include <SU3ME/global_definitions.h>
#include <iostream>

using namespace std;

inline void Get_nlj(const int index, int& n, int& l, int& j)
{
	n = (-1 + sqrt(1.0 + 8.0*(index-1)))/2;
	int i = (index-1) - n*(n+1)/2;
	if (n%2)
	{
		l = 1 + 2*(i/2);
		j = (i % 2) ? (2*l + 1) : (2*l - 1);
	}
	else
	{
		l = 2*((i+1)/2);
		j = (i % 2) ? 2*l - 1: 2*l + 1;
	}
}

//	calculate <n1' l1' j1' m1' t1'; n2' l2' j2' m2' t2'| L^2 | n1 l1 j1 m1 t1; n2 l2 j2 m2 t2> using relation (9) in docs/meL2.pdf
//	This formula was THOROUGHLY TESTED by comparing to results of L2 matrix element generator of SymGen code, which implement
//	a different (more cumbersome) formula
double L2mscheme(	const int n1p, const int l1p, const int j1p, const int m1p, const int t1p,
					const int n2p, const int l2p, const int j2p, const int m2p, const int t2p,
					const int n1, const int l1, const int j1, const int m1, const int t1, 
					const int n2, const int l2, const int j2, const int m2, const int t2)
{
	double dResult(0.0);
	bool bNonvanishing = ((n1p == n1) & (n2p == n2) & (t1p == t1) & (t2p == t2) & (l1p == l1) & (l2p == l2));
	if (bNonvanishing) 
	{
		//	sum over mu = +1, -1, 0 reduces to just one term with  mu == m1p - m1
		int mu = m1p - m1;
		if (mu == (m2 - m2p) && abs(mu) <= 2)
		{
			dResult += MINUSto(-(mu/2))*clebschGordan(j1, m1, 2, +mu, j1p, m1p)*clebschGordan(j2, m2, 2, -mu, j2p, m2p);
			//	Warning: for high l1 and l2 values, sqrt(...) may turn nan ==> TODO: implement calculation using logarithm
			double phase = 2.0*MINUSto(1 + (j1 + l1 + j2 + l2)/2) * sqrt((unsigned)((j1 + 1) * (j2 + 1) * (l1/2) * ((l1/2) + 1) * (l1 + 1) * (l2/2) * (l2/2 + 1) * (l2 + 1)));
			dResult *= phase*wigner6j(l1, 1, j1, j1p, 2, l1) * wigner6j(l2, 1, j2, j2p, 2, l2);
		}
		dResult += ((l1/2)*((l1/2) + 1) + (l2/2)*((l2/2) + 1))*((j1p == j1) & (j2p == j2) & (m1p == m1) & (m2p == m2));
	}
	return dResult;
}

//	Calculate normalized & antisymmetrized matrix element
//	<n1' l1' j1' m1' t1'; n2' l2' j2' m2' t2'| L^2 | n1 l1 j1 m1 t1; n2 l2 j2 m2 t2>_{NAS} 
//		= <n1' l1' j1' m1' t1'; n2' l2' j2' m2' t2'| L^2 | n1 l1 j1 m1 t1; n2 l2 j2 m2 t2> - <n1' l1' j1' m1' t1'; n2' l2' j2' m2' t2'| L^2 | n2 l2 j2 m2 t2; n1 l1 j1 m1 t1>
double L2mscheme_NAS(	const int n1p, const int l1p, const int j1p, const int m1p, const int t1p,
						const int n2p, const int l2p, const int j2p, const int m2p, const int t2p,
						const int n1, const int l1, const int j1, const int m1, const int t1, 
						const int n2, const int l2, const int j2, const int m2, const int t2)
{
	return L2mscheme(n1p, l1p, j1p, m1p, t1p, n2p, l2p, j2p, m2p, t2p, n1, l1, j1, m1, t1, n2, l2, j2, m2, t2) - L2mscheme(n1p, l1p, j1p, m1p, t1p, n2p, l2p, j2p, m2p, t2p, n2, l2, j2, m2, t2, n1, l1, j1, m1, t1);
}


//	calculate matrix elements of L^2 operator in j-coupled & normalized &
//	antisymmetrized basis using formula (1) from docs/meL2.pdf
double L2jcoupled_NAS(	const int na, const int la, const int ja, const int ta,
						const int nb, const int lb, const int jb, const int tb,
						const int nc, const int lc, const int jc, const int tc, 
						const int nd, const int ld, const int jd, const int td,
						const int J)
{
	int MJ(J);
	double dResult(0.0);	
	for (int ma = -ja; ma <= ja; ma += 2)
	{
		int mb = MJ - ma;	//	==> ma + mb == MJ
		if (abs(mb) > jb) {	//	==> mb is non-physical
			continue;
		}
		for (int mc = -jc; mc <= jc; mc += 2)
		{
			int md = MJ - mc;	//	==> mc + md == MJ
			if (abs(md) > jd) {	//	==> md is non-physical
				continue;
			}
			double dCG1 = clebschGordan(ja, ma, jb, mb, J, MJ); 
			double dCG2 = clebschGordan(jc, mc, jd, md, J, MJ); 
			if (!Negligible(dCG1) && !Negligible(dCG2))
			{
				dResult += dCG1*dCG2*L2mscheme_NAS(na, la, ja, ma, ta, nb, lb, jb, mb, tb, nc, lc, jc, mc, tc, nd, ld, jd, md, td);
			}
		}
	}
	double Nab = 1.0/(sqrt(1.0 + ( (na == nb) & (la == lb) & (ja == jb) & (ta == tb)) ));
	double Ncd = 1.0/(sqrt(1.0 + ( (nc == nd) & (lc == ld) & (jc == jd) & (tc == td)) ));
	return Nab*Ncd*dResult;
//	factor sqrt(2*J + 1) is ommited since we define
//	< J MJ| O | J MJ'> = C * < J|| O || J>
//	double PiJ = sqrt(J + 1);
//	return PiJ*Nab*Ncd*dResult;
}


//	calculate matrix elements of L^2 operator in j-coupled & normalized &
//	antisymmetrized basis using formula (1) from docs/meL2.pdf
double L2jcoupled_NAS(const int a, const int ta, const int b, const int tb, const int c, const int tc, const int d, const int td, const int J)
{
	int na, la, ja;
	int nb, lb, jb;
	int nc, lc, jc;
	int nd, ld, jd; 
	int MJ(J);

	Get_nlj(a, na, la, ja);	//	one must be careful here ... Get_nlj returns the value of l, but L2mscheme requires l == 2*l
	la *= 2;	//	==> need to multiply by two
	Get_nlj(b, nb, lb, jb);
	lb *= 2;
	Get_nlj(c, nc, lc, jc);
	lc *= 2;
	Get_nlj(d, nd, ld, jd);
	ld *= 2;

	double dResult(0.0);	
	
	for (int ma = -ja; ma <= ja; ma += 2)
	{
		int mb = MJ - ma;	//	==> ma + mb == MJ
		if (abs(mb) > jb) {	//	==> mb is non-physical
			continue;
		}
		for (int mc = -jc; mc <= jc; mc += 2)
		{
			int md = MJ - mc;	//	==> mc + md == MJ
			if (abs(md) > jd) {	//	==> md is non-physical
				continue;
			}
			double dCG1 = clebschGordan(ja, ma, jb, mb, J, MJ); 
			double dCG2 = clebschGordan(jc, mc, jd, md, J, MJ); 
			if (!Negligible(dCG1) && !Negligible(dCG2))
			{
				dResult += dCG1*dCG2*L2mscheme_NAS(na, la, ja, ma, ta, nb, lb, jb, mb, tb, nc, lc, jc, mc, tc, nd, ld, jd, md, td);
			}
		}
	}
	double Nab = 1.0/(sqrt(1.0 + ( (na == nb) & (la == lb) & (ja == jb) & (ta == tb)) ));
	double Ncd = 1.0/(sqrt(1.0 + ( (nc == nd) & (lc == ld) & (jc == jd) & (tc == td)) ));
	return Nab*Ncd*dResult;
//	factor sqrt(2*J + 1) is ommited since we define
//	< J MJ| O | J MJ'> = C * < J|| O || J>
//	double PiJ = sqrt(J + 1);
//	return PiJ*Nab*Ncd*dResult;
}





//	For proton-proton & neutron-neutron two-body basis, we calculate the upper diagonal matrix:
//         1 1    1 2     1 3     1 4     2 2     2 3    2 4     3 3     3 4     4 4
//	1 1    x      x       x       x       x       x      x       x       x       x
//	1 2           x       x       x       x       x      x       x       x       x
//	1 3                   x       x       x       x      x       x       x       x
//	1 4                           x       x       x      x       x       x       x
//	2 2                                   x       x      x       x       x       x
//	2 3                                           x      x       x       x       x
//	2 4                                                  x       x       x       x
//	3 3                                                          x       x       x
//	3 4                                                                  x       x
//	4 4                                                                          x
//	
//	and hence besides a <= b and c <= d, we store only those matrix elements that satisfy: 
//
//	((a < b) || ((a == c) && b <= d)))
//	
//	This can be seen, e.g., from the structure of the third row which is being stored:
//	<1 3 || || 1 3>, <1 3|| ||1 4>  ... I1 == I3 && I2 <= I4
//	<1 3 || || 2 2>, <1 3|| ||2 4>, <1 3|| ||3 3>, <1 3|| ||3 4>, <1 3|| ||4 4> .... I1 < I3

//	Generate L^2 matrix elements for proton-proton and neutron-neutron wave functions
void GenerateL2meJcoupled_ppnn_NAS(int nmax)
{
	int n, l;
	int ja, jb, jc, jd;
//	Step #1: get the first index for which n == nmax + 1
	int Imax = 1;
	for (int n(0), l, j; n <= nmax; Get_nlj(Imax++, n, l, j));
	Imax--;

//	Proton-proton && neutron-neutron ==> a <= b && c <=d
	for (int a = 1; a < Imax; ++a)
	{
		Get_nlj(a, n, l, ja);
		for (int b = a; b < Imax; ++b)	// |a b J> = (-)^phase |b a> ==> basis ={ |a <= b J> ... }
		{
			Get_nlj(b, n, l, jb);
			for (int c = a; c < Imax; ++c)
			{
				Get_nlj(c, n, l, jc);
				for (int d = (a == c) ? b: c; d < Imax; ++d)
				{
					Get_nlj(d, n, l, jd);
					int Jmax = std::min((ja + jb), (jc + jd));
					int Jmin = std::max(abs(ja-jb), abs(jc-jd));
					for (int J = Jmin; J <= Jmax; J += 2)
					{
						double dL2ppnn 	= L2jcoupled_NAS(a, 0, b, 0, c, 0, d, 0, J);	// <pp| L^2 |pp> = <nn|L^2|nn>
						if (!Negligible(dL2ppnn))
						{
							cout << "<" << a << " " << b << " J=" << J << "||L^2||" << c << " " << d << " J=" << J << "> = " << dL2ppnn << endl;
						}
					}
				}
			}
		}
	}
}

void GenerateL2meJcoupled_ppnn_NAS_fast(int nmax)
{
	int n, l;
	int na, nb, nc, nd;
	int la, lb, lc, ld;
	int ja, jb, jc, jd;
//	Step #1: get the first index for which n == nmax + 1
	int Imax = 1;
	for (int n(0), l, j; n <= nmax; Get_nlj(Imax++, n, l, j));
	Imax--;

//	Proton-proton && neutron-neutron ==> a <= b && c <=d
	for (int a = 1; a < Imax; ++a)
	{
		Get_nlj(a, na, la, ja);
		la *= 2;
		for (int b = a; b < Imax; ++b)	// |a b J> = (-)^phase |b a> ==> basis ={ |a <= b J> ... }
		{
			Get_nlj(b, nb, lb, jb);
			lb *= 2;
			for (int c = a; c < Imax; ++c)
			{
				Get_nlj(c, nc, lc, jc);
				lc *= 2;
				for (int d = (a == c) ? b: c; d < Imax; ++d)
				{
					Get_nlj(d, nd, ld, jd);
					ld *= 2;
					int Jmax = std::min((ja + jb), (jc + jd));
					int Jmin = std::max(abs(ja-jb), abs(jc-jd));
					for (int J = Jmin; J <= Jmax; J += 2)
					{
						double dL2ppnn 	= L2jcoupled_NAS(na, la, ja, 0, nb, lb, jb, 0, nc, lc, jc, 0, nd, ld, jd, 0, J);	// <pp| L^2 |pp> = <nn|L^2|nn>
						if (!Negligible(dL2ppnn))
						{
							cout << "<(a=" << a << ") na=" << na << " la=" << la << " ja=" << ja;
							cout << "; (b=" << b << ") nb=" << nb << " lb=" << lb << " jb=" << jb << " J=" << J << "|| L^2 ||";
							cout << "(c=" << c << ") nc=" << nc << " lc=" << lc << " jc=" << jc;
							cout << ";(d=" << d << ") nd=" << nd << " ld=" << ld << " jd=" << jd << " J=" << J << "> = " << dL2ppnn << endl;
						}
					}
				}
			}
		}
	}
}


//	Generate L^2 matrix elements for proton-neutron wave functions
void GenerateL2meJcoupled_pn_NAS(int nmax)
{
	int n, l;
	int ja, jb, jc, jd;
//	Step #1: get the first index for which n == nmax + 1
	int Imax = 1;
	for (int n(0), l, j; n <= nmax; Get_nlj(Imax++, n, l, j));
	Imax--;

//	Proton-proton && neutron-neutron ==> a <= b && c <=d
	for (int a = 1; a < Imax; ++a)
	{
		Get_nlj(a, n, l, ja);
		for (int b = 1; b < Imax; ++b)	// |a b J> = (-)^phase |b a> ==> basis ={ |a <= b J> ... }
		{
			Get_nlj(b, n, l, jb);
			for (int c = a; c < Imax; ++c)
			{
				Get_nlj(c, n, l, jc);
				for (int d = (a == c) ? b: 1; d < Imax; ++d)
				{
					Get_nlj(d, n, l, jd);
					int Jmax = std::min((ja + jb), (jc + jd));
					int Jmin = std::max(abs(ja-jb), abs(jc-jd));
					for (int J = Jmin; J <= Jmax; J += 2)
					{
						double dL2pn 	= L2jcoupled_NAS(a, 0, b, 1, c, 0, d, 1, J);
						if (!Negligible(dL2pn))
						{
							cout << "<" << a << " " << b << " J=" << J << "||L^2||" << c << " " << d << " J=" << J << "> = ";
							cout << dL2pn << endl;
						}
					}
				}
			}
		}
	}
}

int main()
{
	cout << endl;
	cout << "proton-proton & neutron-neutron L^2 matrix elements: " << endl;
	GenerateL2meJcoupled_ppnn_NAS_fast(7);
	cout << endl;
	cout << "proton-neutron L^2 matrix elements: " << endl;
	GenerateL2meJcoupled_pn_NAS(7);
}

