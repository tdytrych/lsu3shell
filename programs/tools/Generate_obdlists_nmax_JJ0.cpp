#include <iostream>
#include <fstream>
#include <string>
#include <SU3ME/global_definitions.h>
#include <SU3ME/SU3xSU2Basis.h>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ofstream;

int main(int argc,char **argv)
{
	if (argc != 5)
	{
		cout << "Usage: "<< argv[0] <<" <nmax>     <JJ0>    <selection rule>  <output_file_prefix>" << endl;
		cout << "0 ... all tensors" << endl;
		cout << "1 ... all the parity conserving (a+a) tensors; i.e.  n1 + n2 == even" << endl;
		return EXIT_FAILURE;
	}
	
	int nmax, JJ0, type;

	nmax = atoi(argv[1]);
	JJ0 = atoi(argv[2]);
	type = atoi(argv[3]);
	string output_file_prefix(argv[4]);
	int number_su3obd_needed = 0;
	
	string su3_file_name(output_file_prefix);
	string su2_file_name(output_file_prefix);
	su3_file_name += ".su3_list";
	su2_file_name += ".su2_list";

	ofstream su3_file(su3_file_name.c_str());
	ofstream su2_file(su2_file_name.c_str());

	if (!su3_file)
	{
		cout << "Error ... not able to open '" << su3_file_name << endl;
		return EXIT_FAILURE;
	}
	if (!su2_file)
	{
		cout << "Error ... not able to open '" << su2_file_name << endl;
		return EXIT_FAILURE;
	}

	for (int na = 0; na <= nmax; ++na)
	{
		for (int nb = 0; nb <= nmax; ++nb)
		{
			SU3xSU2_VEC lm0mu0_irreps;
			SU3xSU2::Couple(SU3xSU2::LABELS(1, na, 0, 1), SU3xSU2::LABELS(1, 0, nb, 1), lm0mu0_irreps);
			for (int i = 0; i < lm0mu0_irreps.size(); ++i)
			{
				SU3xSU2::BasisJfixed lm0mu0(lm0mu0_irreps[i], JJ0);
				for (lm0mu0.rewind(); !lm0mu0.IsDone(); lm0mu0.nextL())
				{
					for (int k0 = 0; k0 < lm0mu0.kmax(); ++k0)
					{
						if (type == 0) 
						{
							number_su3obd_needed += 2;
						}
						else if (type == 1)
						{
							if ((na + nb)%2 == 0)
							{
								number_su3obd_needed += 2;
							}
						}
					}
				}
			}
		}
	}
	
	su3_file << number_su3obd_needed << endl;
	for (int na = 0; na <= nmax; ++na)
	{
		for (int nb = 0; nb <= nmax; ++nb)
		{
			if (type == 1 && (na+nb)%2 == 1)
			{
				continue;
			}
			SU3xSU2_VEC lm0mu0_irreps;
			SU3xSU2::Couple(SU3xSU2::LABELS(1, na, 0, 1), SU3xSU2::LABELS(1, 0, nb, 1), lm0mu0_irreps);
			for (int i = 0; i < lm0mu0_irreps.size(); ++i)
			{
				SU3xSU2::BasisJfixed lm0mu0(lm0mu0_irreps[i], JJ0);
				for (lm0mu0.rewind(); !lm0mu0.IsDone(); lm0mu0.nextL())
				{
					for (int k0 = 0; k0 < lm0mu0.kmax(); ++k0)
					{
						su3_file << na << " " << nb << " " << (int)lm0mu0.lm << " " << (int)lm0mu0.mu << " " << (int)lm0mu0.S2 << " " << k0 << " " << lm0mu0.L() << " " << JJ0 << " " << "p" << endl;
						su3_file << na << " " << nb << " " << (int)lm0mu0.lm << " " << (int)lm0mu0.mu << " " << (int)lm0mu0.S2 << " " << k0 << " " << lm0mu0.L() << " " << JJ0 << " " << "n" << endl;
					}
				}
			}
		}
	}

	su2_file << number_su3obd_needed << endl;
	for (int na = 0; na <= nmax; ++na)
	{
		for (int nb = 0; nb <= nmax; ++nb)
		{
			if (type == 1 && (na+nb)%2 == 1)
			{
				continue;
			}	
			for (int la = na; la >= 0; la -= 2)
			{
				for (int ja = abs(2*la - 1); ja <= 2*la + 1; ja += 2) 
				{
					for (int lb = nb; lb >= 0; lb -= 2)
					{
						for (int jb = abs(2*lb - 1); jb <= 2*lb + 1; jb += 2) 
						{
							if (JJ0 >= abs(ja-jb) && JJ0 <= (ja+jb))
							{
								su2_file << na << " " << 2*la << " " << ja << " " << nb << " " << 2*lb << " " << jb << " " << JJ0 << " " << "p" << endl;
								su2_file << na << " " << 2*la << " " << ja << " " << nb << " " << 2*lb << " " << jb << " " << JJ0 << " " << "n" << endl;
							}
						}
					}
				}
			}
		}
	}

	su3_file << "na nb lm0 mu0 SS0 k0 LL0 JJ0 p/n" << endl;
	su2_file << "na lla jja nb llb jjb JJ0 p/n" << endl;
	su3_file << "JJ0 = 2*J0: " << JJ0 << endl;
	su2_file << "JJ0 = 2*J0: " << JJ0 << endl;
	su3_file << "the highest shell number: " << nmax << endl;
	su2_file << "the highest shell number: " << nmax << endl;
	if (type == 0)
	{
		su3_file << "All possible n1 l1 j1 n2 l2 j2 J0 combinations generated" << endl;
		su2_file << "All possible n1 l1 j1 n2 l2 j2 J0 combinations generated" << endl;
	}
	else if (type == 1)
	{
		su3_file << "Only parity conserving terms (i.e. n1 + n2 is even) generated" << endl;
		su2_file << "Only parity conserving terms (i.e. n1 + n2 is even) generated" << endl;
	}
}
