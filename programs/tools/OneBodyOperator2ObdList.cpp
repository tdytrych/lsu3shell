#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <fstream>
#include <iostream>
#include <set>
#include <tuple>

using std::cout;
using std::endl;
using std::ifstream;
using std::fstream;
using std::ofstream;

struct OneBodyTerm
{
	enum Type {kProton, kNeutron};

	int nd_, nt_, lm0_, mu0_, ss0_, k0_, ll0_, jj0_, type_;

	OneBodyTerm(int nd, int nt, int lm0, int mu0, int ss0, int k0, int ll0, int jj0, int type):
	nd_(nd), nt_(nt), lm0_(lm0), mu0_(mu0), ss0_(ss0), k0_(k0), ll0_(ll0), jj0_(jj0), type_(type) {}

	OneBodyTerm conjugate()
	{
		// [ [a+^(nd 0)} x ta^(0 nt)]^(lm0 mu0)ss0 k0 ll0 jj0 ]+ = (-)^(phase) x [a+^(nt 0)} x ta^(0 nd)]^(mu0 lm0)ss0 k0 ll0 jj0
		return OneBodyTerm(nt_, nd_, mu0_, lm0_, ss0_, k0_, ll0_, jj0_, type_);
	}
};

bool operator < (const OneBodyTerm& lhs, const OneBodyTerm& rhs)
{
	return std::tie(lhs.nd_, lhs.nt_, lhs.lm0_, lhs.mu0_, lhs.ss0_, lhs.k0_, lhs.ll0_, lhs.jj0_, lhs.type_) < std::tie(rhs.nd_, rhs.nt_, rhs.lm0_, rhs.mu0_, rhs.ss0_, rhs.k0_, rhs.ll0_, rhs.jj0_, rhs.type_);
}
bool operator == (const OneBodyTerm& lhs, const OneBodyTerm& rhs)
{
	return std::tie(lhs.nd_, lhs.nt_, lhs.lm0_, lhs.mu0_, lhs.ss0_, lhs.k0_, lhs.ll0_, lhs.jj0_, lhs.type_) == std::tie(rhs.nd_, rhs.nt_, rhs.lm0_, rhs.mu0_, rhs.ss0_, rhs.k0_, rhs.ll0_, rhs.jj0_, rhs.type_);
}


int main(int argc, char** argv)
{
	if (argc != 3 && argc != 4)
	{
		cout << "Usage: "<< argv[0] <<" <filename>  <nmax> [ <1> ]" << endl;
		cout << endl;
		cout << "<filename> ... file with one-body operator" << endl;
		cout << "<nmax> ... maximal HO shell to include in a+ta operator" << endl;
		cout << "<1> ... optional parameter to exclude conjugate a+a terms." << endl;
      cout << "If this optional parameter is not specified, conjugate terms are included." << std::endl;
		return EXIT_FAILURE;
	}

	int nmax = atoi(argv[2]);
	if (nmax < 0 || nmax > 32)
	{
		cout << "The maximal HO shell number out of range nmax:" << nmax << endl;
		return EXIT_FAILURE;
	}

	ifstream one_body_operator(argv[1]);
	if (!one_body_operator)
	{
		cout << "Could not open '" << argv[1] << "' file that contains a one-body operator" << endl;
		return 0;
	}

	bool excludeConjugate = false; // implicitly we are including even conjugate counterparts of adta terms
	if (argc == 4 && atoi(argv[3]) == 1)
	{
		excludeConjugate = true;
	}

	int lm0, mu0, SS2, k0max, LL0, JJ0, total_JJ0, total_MM0;
	int n1, n2, nd, nt;
	int nTensors, nCoeffs;
	SU3xSU2::LABELS tensor_term_labels;
	int num_tensor_terms = 0;
	
	std::set<OneBodyTerm> adta_set;

	one_body_operator >> total_JJ0 >> total_MM0;

	while (true)
	{
		one_body_operator >> n1 >> n2;
//	obd list must have shell of creation operator first
		if (one_body_operator.eof()) {
			break;
		}

		if (n1 < 0)
		{
			std::swap(n1, n2);
		}
		nd = abs(n1) - 1;
		nt = abs(n2) - 1;

		one_body_operator >> nTensors;

//		iterate over tensors in the CTensorGroup in the input file 
		for (int i = 0; i < nTensors; ++i, ++num_tensor_terms)
		{
			one_body_operator >> lm0 >> mu0 >> SS2 >> LL0 >> JJ0;

			k0max = nCoeffs = SU3::kmax(SU3::LABELS(lm0, mu0), LL0/2);
			std::vector<float> dCoeffsP(nCoeffs), dCoeffsN(nCoeffs);
			for (int j = 0; j < nCoeffs; ++j)
			{
				one_body_operator >> dCoeffsP[j];
				one_body_operator >> dCoeffsN[j];
			}

			if (nd > nmax || nt > nmax)
			{
				continue;
			}

			if (std::count_if(dCoeffsP.begin(), dCoeffsP.end(), Negligible) != dCoeffsP.size())
			{
				for (int k0 = 0; k0 < k0max; ++k0)
				{
					OneBodyTerm adta(nd, nt, lm0, mu0, SS2, k0, LL0, JJ0, OneBodyTerm::kProton);
					if (adta_set.find(adta) == adta_set.end()) // ==> adta is not present in the set yet
					{
						bool conjugateIsNotPresent = (adta_set.find(adta.conjugate()) == adta_set.end());	// conjugate of adta is also not in the set
						if (conjugateIsNotPresent || !excludeConjugate)
						{
							adta_set.insert(adta);
						}
					}
//					cout << nd << " " << nt << " " << lm0 << " " << mu0 << " " << SS2 << " " << k0 << " " << LL0 << " " << JJ0 << " " << "p" <<  endl;
				}
			}
			if (std::count_if(dCoeffsN.begin(), dCoeffsN.end(), Negligible) != dCoeffsN.size())
			{
				for (int k0 = 0; k0 < k0max; ++k0)
				{
					OneBodyTerm adta(nd, nt, lm0, mu0, SS2, k0, LL0, JJ0, OneBodyTerm::kNeutron);
					if (adta_set.find(adta) == adta_set.end()) // ==> adta is not present in the set yet
					{
						bool conjugateIsNotPresent = (adta_set.find(adta.conjugate()) == adta_set.end());	// conjugate of adta is also not in the set
						if (conjugateIsNotPresent || !excludeConjugate)
						{
							adta_set.insert(adta);
						}
					}
//					cout << nd << " " << nt << " " << lm0 << " " << mu0 << " " << SS2 << " " << k0 << " " << LL0 << " " << JJ0 << " " << "p" <<  endl;
				}
			}
		}
	}


	cout << adta_set.size() << endl;
	for (auto adta = adta_set.begin(); adta != adta_set.end(); ++adta)
	{
		cout << adta->nd_ << " " << adta->nt_ << " " << adta->lm0_ << " " << adta->mu0_ << " " << adta->ss0_ << " " << adta->k0_ << " " << adta->ll0_ << " " << adta->jj0_ << " ";
		if (adta->type_ == OneBodyTerm::kProton)
		{
			cout << "p" <<  endl;
		}
		else
		{
			cout << "n" <<  endl;
		}
	}
}
