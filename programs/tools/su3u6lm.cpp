#include <UNU3SU3/CSU3Master.h>
#include <su3.h>
#include <iostream>
#include <algorithm>

using namespace std;

int main() 
{
	int lm1, mu1, lm2, mu2, lm3, mu3, lm, mu, lm12, mu12, lm23, mu23;
	vector<double> U6lm;
	size_t nU6lm;
	CSU3CGMaster SU3Lib;

    cout << "Enter (lm1 mu1) " << endl;
    cin >> lm1 >> mu1;
    cout << "Enter (lm2 mu2) " << endl;
    cin >> lm2 >> mu2;
    cout << "Enter (lm mu) " << endl;
    cin >> lm >> mu;
    cout << "Enter (lm3 mu3) " << endl;
    cin >> lm3 >> mu3;
    cout << "Enter (lm12 mu12) " << endl;
    cin >> lm12 >> mu12;
    cout << "Enter (lm23 mu23) " << endl;
    cin >> lm23 >> mu23;

	SU3::LABELS ir1(lm1, mu1);
	SU3::LABELS ir2(lm2, mu2);
	SU3::LABELS ir(lm, mu);
	SU3::LABELS ir3(lm3, mu3);
	SU3::LABELS ir12(lm12, mu12);
	SU3::LABELS ir23(lm23, mu23);

	int rho12max 	= SU3::mult(ir1, ir2, ir12);
	int rho23max 	= SU3::mult(ir2, ir3, ir23);
	int rho12_3max 	= SU3::mult(ir12, ir3, ir);
	int rho1_23max 	= SU3::mult(ir1, ir23, ir);

	if (!rho12max) 
	{
		cerr << ir1 << " x " << ir2 << " does not couple to " << ir12 << endl;
	}
	if (!rho23max) 
	{
		cerr << ir2 << " x " << ir3 << " does not couple to " << ir23 << endl;
	}
	if (!rho12_3max) 
	{
		cerr << ir12 << " x " << ir3 << " does not couple to " << ir << endl;
	}
	if (!rho1_23max) 
	{
		cerr << ir1 << " x " << ir23 << " does not couple to " << ir << endl;
	}

	nU6lm = rho12max*rho12_3max*rho23max*rho1_23max;
	U6lm.resize(nU6lm);
   su3::init();
	SU3Lib.Get6lm(CSU3CGMaster::U6LM, ir1, ir2, ir, ir3, ir12, ir23, U6lm);
   su3::finalize();

	cout << "U((l1 m1) (l2 m2) (lm mu) (l3 m3);(l12 m12)r12 r12_3 (l23 m23)r23 r1_23)" << endl;
	int na = rho12max;
	int nb = rho12_3max*na;
	int nc = rho23max*nb;
	for (size_t rho12 = 0; rho12 < rho12max; ++rho12)
		for (size_t rho12_3 = 0; rho12_3 < rho12_3max; ++rho12_3)
			for (size_t rho23 = 0; rho23 < rho23max; ++rho23)
				for (size_t rho1_23 = 0; rho1_23 < rho1_23max; ++rho1_23)
				{
					int i = rho12 + rho12_3*na + rho23*nb + rho1_23*nc; 
					if (i == 0) 
					{
						cout << "  " << ir1 << "   " << ir2 << "   " << ir << "   " << ir3 << ";   " << ir12;
						cout << rho12 + 1 << " " << rho12_3 + 1 << " " << ir23 << " " << rho23 + 1 << " " << rho1_23 + 1 << "\t" << U6lm[i] << endl;
					}
					else 
					{
						cout << "                                        ";
						cout << rho12 + 1 << " " << rho12_3 + 1 << "       " << rho23 + 1 << " " << rho1_23 + 1 << "\t" << U6lm[i] << endl;
					}
				}
}
