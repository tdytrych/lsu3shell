#include <UNU3SU3/CSU3Master.h>
#include <su3.h>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <iostream>

using namespace std;

void ShowWignerSU3SO3coeffs(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir3)
{
	int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu);
	vector<pair<K1L1K2L2K3L3, vector<double> > > vCGs;// vector of SU(3) Wigner coefficients. Each element has
	CSU3CGMaster SU3CG;
   su3::init();
	size_t n = SU3CG.GetSO3(ir1, ir2, ir3, vCGs);
   su3::finalize();
	int l1, l2, l3, k1, k2, k3, rho_max;

	cout << "<lm1mu1 k1 L1;lm2mu2 k2 L2||lm3mu3 k3 L3>rho" << endl;

   bool first = true;
   std::cout << std::setprecision(8);
   std::cout << "<(lm1 mu1) k1 L1; (lm2 mu2) k2 L2 || (lm3 mu3) k3 L3> rho = 1 ... "
             << (int)SU3::mult(ir1, ir2, ir3) << std::endl;
   //	cout.precision(std::numeric_limits<double>::digits10);

   for (size_t i = 0; i < n; i++) {
      K1L1K2L2K3L3 Key(vCGs[i].first);
      vector<double> CGrho(vCGs[i].second);
      rho_max = CGrho.size();
      l1 = Key.l1() / 2;
      l2 = Key.l2() / 2;
      l3 = Key.l3() / 2;
      k1 = Key.k1() + 1;
      k2 = Key.k2() + 1;
      k3 = Key.k3() + 1;
      if (first) {
         std::cout << std::setw(4) << lm1 << "  " << mu1;
         std::cout << std::setw(6) << k1 << " " << l1;
         std::cout << std::setw(6) << lm2 << "  " << mu2;
         std::cout << std::setw(6) << k2 << " " << l2;
         std::cout << std::setw(8) << lm3 << "  " << mu3;
         std::cout << std::setw(6) << k3 << " " << l3;
         first = false;
      } else {
         std::cout << std::setw(13) << k1 << " " << l1;
         std::cout << std::setw(15) << k2 << " " << l2;
         std::cout << std::setw(17) << k3 << " " << l3;
      }
      for (int irho = 0; irho < rho_max; irho++)  // for all possible multiplicities
      {
         std::cout << std::fixed << std::setw(13) << std::setfill(' ') << CGrho[irho];
      }
      std::cout << std::endl;
   }
}

int main() {
   int lm1, mu1, lm2, mu2, lm3, mu3;

   cout << "Enter (lm1 mu1) " << endl;
   cin >> lm1 >> mu1;
   cout << "Enter (lm2 mu2) " << endl;
   cin >> lm2 >> mu2;
   cout << "Enter (lm3 mu3) " << endl;
   cin >> lm3 >> mu3;

   SU3::LABELS ir1(lm1, mu1);
   SU3::LABELS ir2(lm2, mu2);
   SU3::LABELS ir3(lm3, mu3);

   int rho_max = SU3::mult(ir1, ir2, ir3);
   if (!rho_max) {
      cout << "(" << ir1.lm << " ";
      cout << ir1.mu << ") x "
           << "(";
      cout << ir2.lm << " " << ir2.mu << ") does not yield (" << ir3.lm << " " << ir3.mu << ")"
           << endl;
      return 0;
   }

   ir3.rho = 0;

   ShowWignerSU3SO3coeffs(ir1, ir2, ir3);
}
