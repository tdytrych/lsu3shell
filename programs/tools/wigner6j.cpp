#include <SU3NCSMUtils/clebschGordan.h>
#include <cassert>
#include <iostream>
#include <algorithm>

#include <su3.h>
extern "C"
{
#include <wigxjpf.h>
}

using namespace std;

int main()
{
   su3::init();

	int j1, j2, j3, j4, j5, j6;

	cin >> j1;
	cin >> j2;
	cin >> j3;
	cin >> j4;
	cin >> j5;
	cin >> j6;

	cout << "legacy:  " << wigner6j(j1, j2, j3, j4, j5, j6) << endl;
	cout << "wigxjpf: " << wig6jj(j1, j2, j3, j4, j5, j6) << endl;

   su3::finalize();
}
