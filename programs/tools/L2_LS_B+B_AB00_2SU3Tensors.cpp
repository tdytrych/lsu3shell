#include <SU3ME/ScalarOperators2SU3Tensors.h>
#include <sstream>

using namespace std;

const string extension_sshell("_sshell.unRecoupled");
const string extension_pshell("_pshell.unRecoupled");
const string extension_sdshell("_sdshell.unRecoupled");

const string BpBTwoBody("b1+b2_Plus_b1b+2_2b_Nmax");
const string LSTwoBody("LS_2b_Nmax");
const string L2TwoBody("2LaLbme_2b_Nmax");
const string rirjTwoBody("rirjme_2b_Nmax");

const string BpBOneBody("b+b_1b_Nmax");
const string LSOneBody("LS_1b_Nmax");
const string L2OneBody("L2me_1b_Nmax");
const string r2OneBody("r2me_1b_Nmax");

const string AB00TwoBody("AB00_2b_Nmax");
const string AB00OneBody("AB00_1b_Nmax");

int main()
{
	int iOperator, Nmax, valence_shell, nmax;
	stringstream sFilename2B;
	stringstream sFilename1B;

	cout << "What operator you want to decompose ?" << endl;
	cout << "1 ... L^2" << endl;
	cout << "2 ... L.S" << endl;
	cout << "3 ... Ax[B+.B]" << endl;
	cout << "4 ... r^2 and (r_i . r_j)" << endl;
	cout << "5 ... [A^(2 0) x B^(0 2)]^(0 0)" << endl;
	cin >> iOperator;
	assert(iOperator >= 1 && iOperator <= 5); 
	cout << "Enter Nmax " << endl;
	cin >> Nmax;
	assert(Nmax > 0 && Nmax <= 12);
	cout << "Enter valence shell number" << endl;
	cin >> valence_shell;
	assert(valence_shell >= 0 && valence_shell <= 2);
	nmax = Nmax + valence_shell;

	string extension;

	switch (valence_shell)
	{
		case 0: extension = extension_sshell; break;
		case 1: extension = extension_pshell; break;
		case 2: extension = extension_sdshell;
	}

	switch (iOperator)
	{
		case 1: 
			sFilename2B << L2TwoBody << Nmax << extension;
			sFilename1B << L2OneBody << Nmax;
			Generate_2LaLb_TwoBodyMe(Nmax, valence_shell, sFilename2B.str()); 
			L2OneBodyOperator2SU3(nmax, sFilename1B.str()); 
			break;
		case 2: 	
			sFilename2B << LSTwoBody << Nmax << extension;
			sFilename1B << LSOneBody << Nmax;
			Generate_l1s2_p_s1l2_TwoBodyMe(Nmax, valence_shell, sFilename2B.str()); 
			LSOneBodyOperator2SU3(nmax, sFilename1B.str()); 
			break;
		case 3:
			sFilename2B << BpBTwoBody << Nmax << extension;
			sFilename1B << BpBOneBody << Nmax;
			Generate_bd1b2_p_b1bd2_TwoBodyMe(Nmax, valence_shell, sFilename2B.str()); 
			bdbOneBodyOperator2SU3(nmax, sFilename1B.str()); 
			break;
		case 4:
			sFilename2B << rirjTwoBody << Nmax << extension;
			sFilename1B << r2OneBody << Nmax;
			Generate_rirj_TwoBodyMe(Nmax, valence_shell, sFilename2B.str()); 
			r2OneBodyOperator2SU3(nmax, sFilename1B.str()); 
			break;
		case 5:	
			sFilename2B << AB00TwoBody << Nmax << extension;
			sFilename1B << AB00OneBody << Nmax;
			Generate_AB00_TwoBodyMe(Nmax, valence_shell, sFilename2B.str()); 
			AB00OneBodyOperator2SU3(nmax, sFilename1B.str()); 
	}
}
