#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
using namespace std;

void Print(const CTuple<int, 4>& lmS)
{
	cout << lmS[3] << "(" << lmS[0] << " " << lmS[1] << ")" << lmS[2];
}

void PrintState(const vector<CTuple<int, 4> >& Labels_p, const vector<CTuple<int, 4> >& Labels_n,  const SU3xSU2::LABELS& omega_pn)
{
	cout << "[";
	size_t index;
	size_t nOmegas_p = (Labels_p.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_p; ++i)
	{
		cout << "{";
	}
	Print(Labels_p[0]);
	if (nOmegas_p > 0)
	{
		cout << " x ";
		Print(Labels_p[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_p - 1; ++i)
		{
			Print(Labels_p[index]); cout << " x ";
			Print(Labels_p[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_p[index]);
	}

	cout << "] x ["; 
	size_t nOmegas_n = (Labels_n.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_n; ++i)
	{
		cout << "{";
	}
	Print(Labels_n[0]);
	if (nOmegas_n > 0)
	{
		cout << " x ";
		Print(Labels_n[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_n - 1; ++i)
		{
			Print(Labels_n[index]); cout << " x ";
			Print(Labels_n[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_n[index]);
	}
	cout << "] " << (int)omega_pn.rho << "(" << (int)omega_pn.lm << " " << (int)omega_pn.mu << ")" << (int)omega_pn.S2; 
}

////////////////////////////////////////////////////////////////////////
//	The following set of functions is merely for the output purposes.  I needed
//	to do this quickly, so it is messy ... but seems to work at least good
//	enough for the testing/debbuging rme calculation purposes ...
//
void Set(CTuple<int, 4>& lmS, const UN::SU3xSU2& gamma)
{
	lmS[0] = gamma.lm; lmS[1] = gamma.mu; lmS[2] = gamma.S2; lmS[3] = gamma.mult;
}
void Set(CTuple<int, 4>& lmS, const SU3xSU2::LABELS& omega)
{
	lmS[0] = omega.lm; lmS[1] = omega.mu; lmS[2] = omega.S2; lmS[3] = omega.rho;
}
//	Take gamma and omega and creates a single array of four integers with structure
//
//	Labels = { 	{gamma[0].lm, gamma[0].mu, gamma[0].S2, gamma[0].mult}, 
//				{gamma[1].lm, gamma[1].mu, gamma[1].S2, gamma[1].mult}, 
//				{omega[0].lm, omega[0].mu, omega[0].S2, omega[0].rho}, 
//				{gamma[2].lm, gamma[2].mu, gamma[2].S2, gamma[2].mult}, 
//				{omega[1].lm, omega[1].mu, omega[1].S2, omega[1].rho}, 
//				.
//				.
//				.
//				{omega[#shells - 1].lm, omega[#shells - 1].mu, omega[#shells - 1].S2, omega[#shells - 1].rho}, 
template<class T>
void Output(const T& gamma, const SU3xSU2_VEC& omega, vector<CTuple<int, 4> >& Labels)
{
	CTuple<int, 4> lmS;

	Set(lmS, gamma[0]); Labels.push_back(lmS); 

	if (omega.empty())
	{
		return;
	}

	Set(lmS, gamma[1]); Labels.push_back(lmS);

	for (size_t iomega = 0; iomega < omega.size() - 1; ++iomega)
	{
		Set(lmS, omega[iomega]); Labels.push_back(lmS);
		Set(lmS, gamma[iomega+2]); Labels.push_back(lmS);
	}
	Set(lmS, (omega.back()));
	Labels.push_back(lmS); 
}

void Print( const SingleDistribution& distr_p, const SingleDistribution& distr_n)
{
	cout << "[";
	for (size_t i = 0; i < distr_p.size() - 1; ++i)
	{
		cout << (int)distr_p[i] << " ";
	}
	cout << (int)distr_p.back() << "] x [";

	for (size_t i = 0; i < distr_n.size() - 1; ++i)
	{
		cout << (int)distr_n[i] << " ";
	}
	cout << (int)distr_n.back() << "]" << endl;
}

void Print(const UN::SU3xSU2_VEC& gamma_p, const SU3xSU2_VEC& omega_p, const UN::SU3xSU2_VEC& gamma_n, const SU3xSU2_VEC& omega_n, const SU3xSU2::LABELS& omega_pn)
{
	vector<CTuple<int, 4> > Labels_p;
	vector<CTuple<int, 4> > Labels_n;

	Output(gamma_p, omega_p, Labels_p);
	Output(gamma_n, omega_n, Labels_n);

	PrintState(Labels_p, Labels_n, omega_pn);
}


struct Less_SU3
{
	inline bool operator() (const SU3::LABELS& l, const SU3::LABELS& r) const {return l.C2() < r.C2() || (l.C2() == r.C2() && l < r);}
};


void ShowExpandSp3RCommandList(const CncsmSU3Basis& basis, uint32_t nprotons, uint32_t nneutrons, uint32_t JJ, SU3xSU2::IrrepsContainer<IRREPBASIS>& wpn_irreps_container)
{
	std::map<CTuple<int, 6>, unsigned int> u3su2_irreps;
	CTuple<int, 6> nhwSpSnSlmmu;

	PNConfIterator iter = basis.firstPNConf(0, 1);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();
		SU3xSU2::LABELS wpn = iter.getCurrentSU3xSU2();

		int ap = iter.getMult_p();
		int an = iter.getMult_n();
		
		nhwSpSnSlmmu[0] = iter.nhw();
		nhwSpSnSlmmu[1] = wp.S2;
		nhwSpSnSlmmu[2] = wn.S2; 
		nhwSpSnSlmmu[3] = wpn.S2;
		nhwSpSnSlmmu[4] = wpn.lm;
		nhwSpSnSlmmu[5] = wpn.mu;

		int dim = ap * an * wpn_irreps_container.rhomax_x_dim(iter.getCurrentSU3xSU2());
		u3su2_irreps[nhwSpSnSlmmu] += dim;
	}

	for (std::map<CTuple<int, 6>, unsigned int>::iterator it = u3su2_irreps.begin(); it != u3su2_irreps.end(); ++it)
	{
		CTuple<int, 6> labels = it->first;
		unsigned int dim = it->second;
//	number of Sp(3,R) states == number of lowest lying eigenstates of -[A x B] + 5000Ncm			
		uint32_t nev;
// Values tuned for a single process decomposition using testbed2 system		
		if (dim < 25000)
		{
			nev = 1500;
		}
		else if (dim > 25000 && dim < 90000)
		{
			nev = 700;
		}
		else if (dim > 90000)
		{
			nev = 350;
		}
		cout << "ExpandSp3R " << nprotons << " " << nneutrons;
		cout << " " << labels[0] << " " << labels[1] << " " << labels[2] << " " << labels[3] << " " << labels[4] << " " << labels[5] << " " << JJ << " " << 5000 << " " << nev << endl;
	}
}

void ShowU3SU2Subspaces(const CncsmSU3Basis& basis)
{
	std::map<CTuple<int, 6>, unsigned int> u3su2_irreps;
	CTuple<int, 6> nhwSpSnSlmmu;

	PNConfIterator iter = basis.firstPNConf(0, 1);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();
		SU3xSU2::LABELS wpn = iter.getCurrentSU3xSU2();

		int ap = iter.getMult_p();
		int an = iter.getMult_n();
		
		nhwSpSnSlmmu[0] = iter.nhw();
		nhwSpSnSlmmu[1] = wp.S2;
		nhwSpSnSlmmu[2] = wn.S2; 
		nhwSpSnSlmmu[3] = wpn.S2;
		nhwSpSnSlmmu[4] = wpn.lm;
		nhwSpSnSlmmu[5] = wpn.mu;

		int nirreps = ap*an*wpn.rho;

		u3su2_irreps[nhwSpSnSlmmu] += nirreps;
	}

	for (std::map<CTuple<int, 6>, unsigned int>::iterator it = u3su2_irreps.begin(); it != u3su2_irreps.end(); ++it)
	{
		CTuple<int, 6> labels = it->first;
		unsigned int nirreps = it->second;
		cout << labels[0] << " " << labels[1] << " " << labels[2] << " " << labels[3] << " " << labels[4] << " " << labels[5] << " " << nirreps << endl;
	}
}

void ShowCMInvariantSpSnSlmmuSubspaces(const CncsmSU3Basis& basis, SU3xSU2::IrrepsContainer<IRREPBASIS>& wpn_irreps_container, const bool bShowDimensions = true)
{
	std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> > spsns_su3;

	unsigned int ap, an;
	unsigned char status;
	unsigned long idim = 0;
	unsigned int irrep_dim = 0;
	unsigned int nomega_pn = 0;
	SingleDistributionSmallVector distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_SMALL_VEC omega_p, omega_n;
	SU3xSU2::LABELS omega_pn;
	PNConfIterator iter = basis.firstPNConf(0, 1);

	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		assert(iter.hasPNOmega());

		status = iter.status();
		switch (status)
		{
			case PNConfIterator::kNewDistr_p: distr_p.resize(0); iter.getDistr_p(distr_p);
			case PNConfIterator::kNewDistr_n: 
				distr_n.resize(0); 
				iter.getDistr_n(distr_n); 
			case PNConfIterator::kNewGamma_p: gamma_p.resize(0); iter.getGamma_p(gamma_p);
			case PNConfIterator::kNewGamma_n: gamma_n.resize(0); iter.getGamma_n(gamma_n);
			case PNConfIterator::kNewOmega_p: omega_p.resize(0); iter.getOmega_p(omega_p); ap = iter.getMult_p();
			case PNConfIterator::kNewOmega_n: omega_n.resize(0); iter.getOmega_n(omega_n); an = iter.getMult_n();
		}
		omega_pn = iter.getCurrentSU3xSU2();
		irrep_dim = wpn_irreps_container.rhomax_x_dim(omega_pn);	// rhomax * dim[(lm mu)S]
		if (irrep_dim == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();
		CTuple<SU2::LABEL, 3> spsns;
		spsns[0] = wp.S2; spsns[1] = wn.S2; spsns[2] = omega_pn.S2;
		
		std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> >::iterator selected_spsns = spsns_su3.find(spsns);
		if (selected_spsns == spsns_su3.end())
		{
			std::map<SU3::LABELS, size_t, Less_SU3> setsu3;
			setsu3.insert(make_pair(SU3::LABELS(1, omega_pn.lm, omega_pn.mu), ap*an*irrep_dim));
			spsns_su3.insert(std::make_pair(spsns, setsu3));
		}
		else
		{
			selected_spsns->second[SU3::LABELS(1, omega_pn.lm, omega_pn.mu)] += ap*an*irrep_dim;
		}
		idim += ap*an*irrep_dim; 
	}
	cout << "number of omega_pn] = " << iter.num_PNomegas_iterated() << endl;

	size_t	number_cm_subspaces(0);
	cout << "List of subspaces Sp Sn S (lm mu)" << endl;
	size_t dim_check = 0;
	for ( std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> >::iterator it = spsns_su3.begin(); it !=spsns_su3.end(); ++it)
	{
		std::cout << "Sp=" << (int)it->first[0] << " Sn=" << (int)it->first[1] << " S=" << (int)it->first[2] << "\t";
		for (std::map<SU3::LABELS, size_t, Less_SU3>::iterator ir = it->second.begin(); ir != it->second.end(); ++ir)
		{
			std::cout << "(" << (int)ir->first.lm << " " << (int)ir->first.mu << ")";
			dim_check += ir->second;
			if (bShowDimensions)
			{
				cout << "[" << ir->second << "]  ";
			}
			number_cm_subspaces++;
		}
		std::cout << std::endl;
	}
	cout << "Basis size ... " << idim << "\t check: " << dim_check << endl;
	cout << "Number of distinct Sp Sn S (lm mu) subspaces: " << number_cm_subspaces << endl;

	unsigned long dim = CalculateBasisDim(iter, wpn_irreps_container);
	cout << dim << ", ";
}

void Generate_N_SpSnS_lm_mu_dim_included_Table(const CncsmSU3Basis& basis_full, const CncsmSU3Basis& basis_included, SU3xSU2::IrrepsContainer<IRREPBASIS>& wpn_irreps_container)
{
	vector<std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> > > spsns_su3_excluded(basis_full.Nmax() + 1);
	vector<std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> > > spsns_su3_included(basis_included.Nmax() + 1);

	int inhw;
	unsigned int ap, an;
	unsigned char status;
	unsigned long idim = 0;
	unsigned int irrep_dim = 0;
	unsigned int nomega_pn = 0;
	SingleDistributionSmallVector distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_SMALL_VEC omega_p, omega_n;
	SU3xSU2::LABELS omega_pn;

{
	PNConfIterator iter = basis_full.firstPNConf(0, 1);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		inhw = iter.nhw();
		assert(iter.hasPNOmega());

		status = iter.status();
		switch (status)
		{
			case PNConfIterator::kNewDistr_p: distr_p.resize(0); iter.getDistr_p(distr_p);
			case PNConfIterator::kNewDistr_n: 
				distr_n.resize(0); 
				iter.getDistr_n(distr_n); 
			case PNConfIterator::kNewGamma_p: gamma_p.resize(0); iter.getGamma_p(gamma_p);
			case PNConfIterator::kNewGamma_n: gamma_n.resize(0); iter.getGamma_n(gamma_n);
			case PNConfIterator::kNewOmega_p: omega_p.resize(0); iter.getOmega_p(omega_p); ap = iter.getMult_p();
			case PNConfIterator::kNewOmega_n: omega_n.resize(0); iter.getOmega_n(omega_n); an = iter.getMult_n();
		}
		omega_pn = iter.getCurrentSU3xSU2();
		irrep_dim = wpn_irreps_container.rhomax_x_dim(omega_pn);	// rhomax * dim[(lm mu)S]
		if (irrep_dim == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();
		CTuple<SU2::LABEL, 3> spsns;
		spsns[0] = wp.S2; spsns[1] = wn.S2; spsns[2] = omega_pn.S2;
		
		std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> >::iterator selected_spsns = spsns_su3_excluded[inhw].find(spsns);
		if (selected_spsns == spsns_su3_excluded[inhw].end())
		{
			std::map<SU3::LABELS, size_t, Less_SU3> setsu3;
			setsu3.insert(make_pair(SU3::LABELS(1, omega_pn.lm, omega_pn.mu), ap*an*irrep_dim));
			spsns_su3_excluded[inhw].insert(std::make_pair(spsns, setsu3));
		}
		else
		{
			selected_spsns->second[SU3::LABELS(1, omega_pn.lm, omega_pn.mu)] += ap*an*irrep_dim;
		}
		idim += ap*an*irrep_dim; 
	}
}
{

	PNConfIterator iter = basis_included.firstPNConf(0, 1);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		inhw = iter.nhw();
		assert(iter.hasPNOmega());

		status = iter.status();
		switch (status)
		{
			case PNConfIterator::kNewDistr_p: distr_p.resize(0); iter.getDistr_p(distr_p);
			case PNConfIterator::kNewDistr_n: 
				distr_n.resize(0); 
				iter.getDistr_n(distr_n); 
			case PNConfIterator::kNewGamma_p: gamma_p.resize(0); iter.getGamma_p(gamma_p);
			case PNConfIterator::kNewGamma_n: gamma_n.resize(0); iter.getGamma_n(gamma_n);
			case PNConfIterator::kNewOmega_p: omega_p.resize(0); iter.getOmega_p(omega_p); ap = iter.getMult_p();
			case PNConfIterator::kNewOmega_n: omega_n.resize(0); iter.getOmega_n(omega_n); an = iter.getMult_n();
		}
		omega_pn = iter.getCurrentSU3xSU2();
		irrep_dim = wpn_irreps_container.rhomax_x_dim(omega_pn);	// rhomax * dim[(lm mu)S]
		if (irrep_dim == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();
		CTuple<SU2::LABEL, 3> spsns;
		spsns[0] = wp.S2; spsns[1] = wn.S2; spsns[2] = omega_pn.S2;
		
		std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> >::iterator selected_spsns = spsns_su3_included[inhw].find(spsns);
		if (selected_spsns == spsns_su3_included[inhw].end())
		{
			std::map<SU3::LABELS, size_t, Less_SU3> setsu3;
			setsu3.insert(make_pair(SU3::LABELS(1, omega_pn.lm, omega_pn.mu), ap*an*irrep_dim));
			spsns_su3_included[inhw].insert(std::make_pair(spsns, setsu3));
		}
		else
		{
			selected_spsns->second[SU3::LABELS(1, omega_pn.lm, omega_pn.mu)] += ap*an*irrep_dim;
		}
		idim += ap*an*irrep_dim; 
	}
}

	std::map<std::vector<int>, std::pair<size_t, bool> > spsns_su3_Table;	// TODO sorting according to N C3(lm mu) Sp Sn S ... sorting by C3 in order to distinguish (lm mu) from (mu lm)

	for (inhw = 0; inhw < spsns_su3_excluded.size(); ++inhw)
	{
		if (spsns_su3_excluded[inhw].empty())
		{
			continue;
		}
		for (std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> >::iterator it = spsns_su3_excluded[inhw].begin(); it !=spsns_su3_excluded[inhw].end(); ++it)
		{
			std::vector<int> labels(6); // N Sp Sn S lm mu ... {dim, bIsIncluded} 
			labels[0] = inhw;
			labels[1] = (int)it->first[0]; // Sp
			labels[2] = (int)it->first[1]; // Sn
			labels[3] = (int)it->first[2]; // S
			for (std::map<SU3::LABELS, size_t, Less_SU3>::iterator ir = it->second.begin(); ir != it->second.end(); ++ir)
			{
				labels[4] = (int)ir->first.lm;
				labels[5] = (int)ir->first.mu;
				pair<size_t, bool> dim_IsIncluded(make_pair(ir->second, false));

				spsns_su3_Table[labels] = dim_IsIncluded;
			}
		}
	}
	for (inhw = 0; inhw < spsns_su3_included.size(); ++inhw)
	{
		if (spsns_su3_included[inhw].empty())
		{
			continue;
		}
		for (std::map<CTuple<SU2::LABEL, 3>, std::map<SU3::LABELS, size_t, Less_SU3> >::iterator it = spsns_su3_included[inhw].begin(); it !=spsns_su3_included[inhw].end(); ++it)
		{
			std::vector<int> labels(6); // N Sp Sn S lm mu ... {dim, bIsIncluded} 
			labels[0] = inhw;
			labels[1] = (int)it->first[0]; // Sp
			labels[2] = (int)it->first[1]; // Sn
			labels[3] = (int)it->first[2]; // S
			for (std::map<SU3::LABELS, size_t, Less_SU3>::iterator ir = it->second.begin(); ir != it->second.end(); ++ir)
			{
				labels[4] = (int)ir->first.lm;
				labels[5] = (int)ir->first.mu;
				pair<size_t, bool> dim_IsIncluded(make_pair(ir->second, true));

				spsns_su3_Table[labels] = dim_IsIncluded;
			}
		}
	}


	for ( std::map<std::vector<int>, std::pair<size_t, bool> >::iterator it = spsns_su3_Table.begin(); it !=spsns_su3_Table.end(); ++it)
	{
		vector<int> labels = it->first;
		pair<size_t, bool> dim_IsIncluded = it->second;
		cout << labels[0] << " " << labels[1] << " " << labels[2] << " " << labels[3] << " " << labels[4] << " " << labels[5] << " " << dim_IsIncluded.first << "\t" << dim_IsIncluded.second << endl;
	}
}

int main(int argc, char* argv[])
{
	if (argc != 3 && argc != 4)
	{
		cout << endl;
	  	cout << "Usage: " << endl;
		cout << argv[0] << " <0> <selected model space file name>   ---> List dimensions of U(3)xSU(2) subspaces" << endl;
		cout << argv[0] << " <1> <selected model space file name>   ---> List number of irreps in U(3)xSU(2) subspaces" << endl;
	  	cout << argv[0] << " <2> <full model space file name> <selected model space file name>   ---> Generate a table for dimension figure" << endl;
		cout << argv[0] << " <3> <selected model space file name>   ---> List number of irreps in U(3)xSU(2) subspaces with ExpandSp3R command" << endl;
		cout << endl;
		return 0;
	}
	
	int action = atoi(argv[1]);

	if (action == 0)
	{
		proton_neutron::ModelSpace ncsmModelSpace(argv[2]);
		CncsmSU3Basis  basis(ncsmModelSpace);
		SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container(basis.GenerateFinal_SU3xSU2Irreps(), ncsmModelSpace.JJ());
		const bool bShowDimensions = true;
		ShowCMInvariantSpSnSlmmuSubspaces(basis, wpn_irreps_container, bShowDimensions);
	}
	else if (action == 1) 
	{
		proton_neutron::ModelSpace ncsmModelSpace;
		bool without_output = true;
		ncsmModelSpace.Load(argv[2], without_output);
		CncsmSU3Basis  basis(ncsmModelSpace);
		ShowU3SU2Subspaces(basis);
	}
	else if (action == 2)
	{
		proton_neutron::ModelSpace ncsmFullModelSpace, ncsmSelectedModelSpace;
		bool without_output = true;

		ncsmFullModelSpace.Load(argv[2], without_output);
		ncsmSelectedModelSpace.Load(argv[3], without_output);

		assert(ncsmFullModelSpace.JJ() == ncsmSelectedModelSpace.JJ());

		CncsmSU3Basis  basis_full(ncsmFullModelSpace);
		CncsmSU3Basis  basis_included(ncsmSelectedModelSpace);
		SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container(basis_full.GenerateFinal_SU3xSU2Irreps(), ncsmFullModelSpace.JJ());
		Generate_N_SpSnS_lm_mu_dim_included_Table(basis_full, basis_included, wpn_irreps_container);
	}
	else if (action == 3)
	{
		proton_neutron::ModelSpace ncsmModelSpace(argv[2]);
		CncsmSU3Basis  basis(ncsmModelSpace);
		SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container(basis.GenerateFinal_SU3xSU2Irreps(), ncsmModelSpace.JJ());
		uint32_t nprotons, nneutrons, jj;
		nprotons  = ncsmModelSpace.Z();
		nneutrons = ncsmModelSpace.N();
		jj = ncsmModelSpace.JJ();
		ShowExpandSp3RCommandList(basis, nprotons, nneutrons, jj, wpn_irreps_container);
	}
	return EXIT_SUCCESS;
}
