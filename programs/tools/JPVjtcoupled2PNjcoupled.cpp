#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/JTCoupled2BMe_Hermitian_SingleOperator.h>

using namespace std;

void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, vector<int>& nanbncnd)
{
	int maxShell = valence_shell + Nmax;

	for (int na = 0; na <= maxShell; ++na)
	{
		for (int nb = 0; nb <= maxShell; ++nb)
		{
			int adxad_Nhw = 0;
			if (na > valence_shell)
			{
				adxad_Nhw += (na - valence_shell);
			}
			if (nb > valence_shell)
			{
				adxad_Nhw += (nb - valence_shell);
			}
			if (adxad_Nhw > Nmax)
			{
				continue;
			}

			for (int nc = 0; nc <= maxShell; ++nc)
			{
				for (int nd = 0; nd <= maxShell; ++nd)
				{
					int taxta_Nhw = 0;
					if (nc > valence_shell)
					{
						taxta_Nhw += (nc - valence_shell);
					}
					if (nd > valence_shell)
					{
						taxta_Nhw += (nd - valence_shell);
					}
					if (taxta_Nhw > Nmax)
					{
						continue;
					}
					
					// check if the parity is conserved
					if ((na+nb)%2 == (nc+nd)%2) // if true ==> store {na, nb, nc, nd} combination
					{
						nanbncnd.push_back(na);
						nanbncnd.push_back(nb);
						nanbncnd.push_back(nc);
						nanbncnd.push_back(nd);
					}
				}
			}
		}
	}
}



int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << endl;
		cout << "This program takes as an input file in JT-coupled formad [decomposition_ready] and creates file in J-coupled scheme which is ready for further processing.\n" << endl;
		cout << "Correct usage: " << argv[0] << "<in: JTcoupled filename> <Nmax> <valence shell> <out: pnJ-coupled filename>" << endl;
		return 1;
	}

	uint32_t ntotal(0);

	JTCoupled2BMe_Hermitian_SingleOperator<double> jtcoupledME(argv[1]);
	int Nmax = atoi(argv[2]);
	int valence_shell = atoi(argv[3]);
	std::ofstream output_file(argv[4], std::ofstream::binary);

	vector<int> nanbncnd;
	GenerateAllCombinationsHOShells(Nmax, valence_shell, nanbncnd);

	j_coupled_bra_ket::TwoBodyLabels labelsJ;
	for (int i = 0; i < nanbncnd.size(); i += 4)
	{
		int na = nanbncnd[i + 0];
		int nb = nanbncnd[i + 1];
		int nc = nanbncnd[i + 2];
		int nd = nanbncnd[i + 3];

		output_file.write((char*)&nanbncnd[i], 4*sizeof(int));

		vector<uint16_t> vec_i1, vec_i2, vec_i3, vec_i4; 
		vector<uint8_t> vec_J;
		vector<double> vec_Vpp, vec_Vnn, vec_Vpn;

		for (int la = na%2; la <= na; la += 2)
		{
			for (int lb = nb%2; lb <= nb; lb += 2)
			{
				for (int lc = nc%2; lc <= nc; lc += 2)
				{
					for (int ld = nd%2; ld <= nd; ld += 2)
					{
//						|la + 1/2 - (lb + 1/2)| = |la - lb|
//						|la + 1/2 - (lb - 1/2)| = |la - lb + 1|
//						|la - 1/2 - (lb + 1/2)| = |la - lb -1|
//						|la - 1/2 - (lb - 1/2)| = |la - lb|
						int Jabmin = std::min(abs(la - lb), std::min(abs(la - lb + 1), abs(la - lb - 1)));
						int Jcdmin = std::min(abs(lc - ld), std::min(abs(lc - ld + 1), abs(lc - ld - 1)));
						int Jmax = std::min(la+lb, lc+ld)+1;

						for (int J = std::max(Jabmin, Jcdmin); J <= Jmax; J += 1)
						{
							labelsJ[j_coupled_bra_ket::J] = J;

							for (int jja = abs(2*la - 1); jja <= 2*la + 1; jja += 2)
							{
								for (int jjb = abs(2*lb - 1); jjb <= 2*lb + 1; jjb += 2)
								{
									if (!SU2::mult(jja, jjb, 2*J))
									{
										continue;
									}
									labelsJ[j_coupled_bra_ket::I1] = jtcoupledME.Get_Index(na, la, jja);
									labelsJ[j_coupled_bra_ket::I2] = jtcoupledME.Get_Index(nb, lb, jjb);
									for (int jjc = abs(2*lc - 1); jjc <= 2*lc + 1; jjc += 2)
									{
										for (int jjd = abs(2*ld - 1); jjd <= 2*ld + 1; jjd += 2)
										{
											if (!SU2::mult(jjc, jjd, 2*J))
											{
												continue;
											}
											labelsJ[j_coupled_bra_ket::I3] = jtcoupledME.Get_Index(nc, lc, jjc);
											labelsJ[j_coupled_bra_ket::I4] = jtcoupledME.Get_Index(nd, ld, jjd);
										
											double Vpp(0), Vnn(0), Vpn(0);
											bool me_exist = jtcoupledME.MeJ(labelsJ, Vpp, Vnn, Vpn);
											if (me_exist)
											{
												vec_i1.push_back(labelsJ[j_coupled_bra_ket::I1]);
												vec_i2.push_back(labelsJ[j_coupled_bra_ket::I2]);
												vec_i3.push_back(labelsJ[j_coupled_bra_ket::I3]);
												vec_i4.push_back(labelsJ[j_coupled_bra_ket::I4]);
												vec_J.push_back(labelsJ[j_coupled_bra_ket::J]);
												vec_Vpp.push_back(Vpp);
												vec_Vnn.push_back(Vnn);
												vec_Vpn.push_back(Vpn);
/*
												cout << la << " " << lb << " " << lc << " " << ld << "\t\t";
												cout << J << "\t\t" << jja << "/2 " << jjb << "/2 " << jjc << "/2 " << jjd << "/2\t\t";
*/												
/*												
												cout << (int)labelsJ[j_coupled_bra_ket::I1] << " " << (int)labelsJ[j_coupled_bra_ket::I2] << " ";
												cout << (int)labelsJ[j_coupled_bra_ket::I3] << " " << (int)labelsJ[j_coupled_bra_ket::I4] << " ";
												cout << (int)labelsJ[j_coupled_bra_ket::J] << "\t\t";
*/												

//												cout << Vpp << " " << Vnn << " " << Vpn << endl;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		uint32_t number_matrix_elements = vec_i1.size();
		ntotal += number_matrix_elements;
		output_file.write((char*)&number_matrix_elements, sizeof(uint32_t));
		if (number_matrix_elements)
		{
			output_file.write((char*)&vec_i1[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_i2[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_i3[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_i4[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_J[0] , number_matrix_elements*sizeof(uint8_t));
			output_file.write((char*)&vec_Vpp[0], number_matrix_elements*sizeof(double));
			output_file.write((char*)&vec_Vnn[0], number_matrix_elements*sizeof(double));
			output_file.write((char*)&vec_Vpn[0], number_matrix_elements*sizeof(double));

		}
	}
	cout << "number of matrix elements stored:" << ntotal << endl;
	return EXIT_SUCCESS;
}
