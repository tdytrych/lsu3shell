#include <mpi.h>

#include <SU3ME/ComputeOperatorMatrix.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/OperatorLoader.h>
#include <SU3ME/SU3InteractionRecoupler.h>

#include <su3.h>

#include <boost/mpi.hpp>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ctime>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

enum { Nhw, SSp, SSn, SS, LM, MU };
// order of quantum labels for definition of input tensor quantum labels
enum { N1, N2, N3, N4, LMF, MUF, SSF, LMI, MUI, SSI, LM0, MU0, SS0 };
int nprotons, nneutrons, Nmax;
// -1 ==> all U(3) x SU(2) x SU(2) x SU(2) irreps specified in <model space> will be included.
const int jj_irrelevant = -1;

// For a selected set of tensors (given as rmes) and their tensor strengths compute resulting rmes.
// MECalculatorData: < ... a'... ||| V^rho0' |||... a ...>_rhot, coeffs[rho0']
std::vector<double> ComputeResultingRMEs(std::vector<MECalculatorData>& rmeCoeffsPNPN) {
   // These values must be constant for each tensor
   int32_t bra_max = rmeCoeffsPNPN[0].rmes_->m_bra_max;
   int32_t ket_max = rmeCoeffsPNPN[0].rmes_->m_ket_max;
   int32_t rhot_max = rmeCoeffsPNPN[0].rmes_->m_rhot_max;
   // tensor multiplicity is however tensor dependent
   int32_t rho0p_max;

   // resulting rmes do not have rho0 dependence
   int32_t nrmes_to_compute = bra_max * ket_max * rhot_max;
   std::vector<double> result_rmes(nrmes_to_compute, 0.0);

   // iterate over PP NN and PN tensors each has the same lm0 mu0 and S0
   // quantum labels but it can have a different rho0max multiplicity
   for (size_t itensor = 0; itensor < rmeCoeffsPNPN.size(); ++itensor) {
      assert(bra_max == rmeCoeffsPNPN[itensor].rmes_->m_bra_max);
      assert(ket_max == rmeCoeffsPNPN[itensor].rmes_->m_ket_max);
      assert(rhot_max == rmeCoeffsPNPN[itensor].rmes_->m_rhot_max);

      rho0p_max = rmeCoeffsPNPN[itensor].rmes_->m_tensor_max;
      int32_t rme_index = 0;
      for (int i = 0; i < bra_max; ++i) {
         for (int j = 0; j < ket_max; ++j) {
            for (int irho0p = 0; irho0p < rho0p_max; ++irho0p) {
               // vector with rhot_max elements
               // rmes = < i ||| rho0 ||| j>[i][j][rho0] = {...}
               RME::DOUBLE* rmes = rmeCoeffsPNPN[itensor].rmes_->GetVector(i, j, irho0p);
               TENSOR_STRENGTH coeff_rho0p = rmeCoeffsPNPN[itensor].coeffs_[irho0p];
               for (int irhot = 0; irhot < rhot_max; ++irhot) {
                  result_rmes[rme_index + irhot] += (double)coeff_rho0p * (double)rmes[irhot];
               }
            }
            rme_index += rhot_max;
         }
      }
   }
   return result_rmes;
}

// For a given input tensor and its rho0^th component create .PPNN and .PN files containing
// labels of recoupled tensors and their expansion coefficients.
std::string RecoupleTensorStoreOnDisk(const std::array<int, 13>& tensor, int rho0max, int rho0) {
   int n1(tensor[N1]);
   int n2(tensor[N2]);
   int n3(tensor[N3]);
   int n4(tensor[N4]);
   int lmf(tensor[LMF]);
   int muf(tensor[MUF]);
   int ssf(tensor[SSF]);
   int lmi(tensor[LMI]);
   int mui(tensor[MUI]);
   int ssi(tensor[SSI]);
   int lm0(tensor[LM0]);
   int mu0(tensor[MU0]);
   int ss0(tensor[SS0]);

   SU3xSU2::LABELS wf(1, lmf, muf, ssf);
   SU3xSU2::LABELS wi(1, lmi, mui, ssi);
   SU3xSU2::LABELS w0(rho0max, lm0, mu0, ss0);

   int nCoeffs = 3 * rho0max;
   std::vector<double> CoeffsPPNNPN(nCoeffs, 0.0);
   int index = rho0 * 3;
   // We want to find recoupling of the rho0^th component of the input tensor
   // coeffs = {cpp^rho0:0, cnn^rho0:0, cpn^rho0:0, cpp^rho0:1, cnn^rho0:1, cpn^rho0:1, ...
   // Set the approriate coefficients to 1.0 and rest is set to zero
   CoeffsPPNNPN[index] = 1.0;
   CoeffsPPNNPN[index + 1] = 1.0;
   CoeffsPPNNPN[index + 2] = 1.0;

   char n1n2n3n4[4];
   n1n2n3n4[0] = n1;
   n1n2n3n4[1] = n2;
   n1n2n3n4[2] = n3;
   n1n2n3n4[3] = n4;

   SU3InteractionRecoupler Recoupler;
   int bSuccess = Recoupler.Insert_adad_aa_Tensor(n1n2n3n4, wf, wi, w0, CoeffsPPNNPN);
   if (!bSuccess) {
      // Very unlikely to happen as recoupler has complete set of rules for two-body tensors
      std::cout << "IMPLEMENT: ";

      std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
      std::cout << "\t" << wf << " x " << wi << " -> " << w0;
      std::cout << "\t"
                << "Sf = " << ssf << "/2  Si = " << ssi << "/2 S0 = " << ss0 << "/2";
      std::cout << std::endl;
      return 0;
   }

   std::stringstream ss_filename_prefix;
   ss_filename_prefix << n1 << "_" << n2 << "_" << n3 << "_" << n4;
   ss_filename_prefix << "--" << lmf << "_" << muf << "_" << ssf;
   ss_filename_prefix << "--" << lmi << "_" << mui << "_" << ssi;
   ss_filename_prefix << "--" << rho0 << "_" << lm0 << "_" << mu0 << "_" << ss0;

   // remove tensors with expansion coefficient |c| < 1.0e-9
   Recoupler.RemoveTensorsWithAmplitudesLess(9.0e-7);
   Recoupler.Save(ss_filename_prefix.str() + ".PPNN", TENSOR::PPNN);
   Recoupler.Save(ss_filename_prefix.str() + ".PN", TENSOR::PN);
   return ss_filename_prefix.str();
}

// Create list of quantum labels {Nhw, 2Sp, 2Sn, 2S, lm, mu} ....
// that constitute a given model space
std::vector<std::array<int, 6>> CreateListofU3SU2subspaces(const std::string& model_space_file) {
   proton_neutron::ModelSpace ncsmModelSpace(model_space_file);
   // 2J = -1 --> all U(3) x SU(2)p x SU(2)n x SU(2) irreps will be included
   if (ncsmModelSpace.JJ() != (unsigned char)jj_irrelevant) {
      std::cerr << "Model space definition file '" << model_space_file
                << "' must have 2J value set to -1!" << std::endl;
      exit(EXIT_FAILURE);
   }
   nprotons = ncsmModelSpace.number_of_protons();
   nneutrons = ncsmModelSpace.number_of_neutrons();
   // Construct model space basis for ndiag=1
   lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);
   Nmax = basis.Nmax();
   std::set<std::array<int, 6>> unique_u3su2;

   for (uint32_t ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++) {
      uint32_t ip = basis.getProtonIrrepId(ipin_block);
      uint32_t in = basis.getNeutronIrrepId(ipin_block);
      int N = basis.nhw_p(ip) + basis.nhw_n(in);
      SU3xSU2::LABELS irrep_p(basis.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS irrep_n(basis.getNeutronSU3xSU2(in));
      int ssp(irrep_p.S2);
      int ssn(irrep_n.S2);

      for (uint32_t iwpn = basis.blockBegin(ipin_block); iwpn < basis.blockEnd(ipin_block);
           ++iwpn) {
         SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
         int lm = omega_pn.lm;
         int mu = omega_pn.mu;
         int ss = omega_pn.S2;
         unique_u3su2.insert({N, ssp, ssn, ss, lm, mu});
      }
   }

   // copy content of the set of unique {Nhw, 2Sp, 2Sn, 2S, lm, mu} subspaces into resulting vector
   // of Nhw ssp, ssn, ss, lm, mu;
   return std::vector<std::array<int, 6>>(unique_u3su2.begin(), unique_u3su2.end());
}

// Read {n1 n2 n3 n4 (lmf muf)2Sf (lmi mui)2Si (lm0 mu0) 2S0} quantum numbers of tensors from a text
// file
std::vector<std::array<int, 13>> ReadListOfTensors(const std::string& filename) {
   std::vector<std::array<int, 13>> tensor_list;
   std::ifstream file(filename);
   if (!file) {
      std::cerr << "Could not open '" << filename << "' input file!" << std::endl;
      exit(EXIT_FAILURE);
   }

   while (true) {
      int n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0;
      file >> n1 >> n2 >> n3 >> n4 >> lmf >> muf >> ssf >> lmi >> mui >> ssi >> lm0 >> mu0 >> ss0;
      if (file) {
         tensor_list.push_back(
             std::array<int, 13>({n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0}));
      } else {
         break;
      }
   }
   return tensor_list;
}

// Compute < * N' 2Sp' 2Sn' 2S' (lm' mu')||| V ||| * N 2Sp 2Sn 2S (lm mu) >_rhot
// where * stands for all possible <(ip in) a'| and |(jp jn) a> in bra and ket model spaces
// interactionPN: V_pn expanded in terms of recoupled tensors
// interactionPPNN: V_pp and V_nn expanded in terms of recoupled tensors
void ComputeRMEs(
    // input:
    const std::array<int, 6>& bra_space,      // <N' 2Sp' 2Sn' 2S' (lm' mu')| quantum numbers
    const std::array<int, 6>& ket_space,      // |N 2Sp 2Sn 2S (lm mu)> quantum numbers
    const CInteractionPPNN& interactionPPNN,  // V_pp & V_nn expanded in terms of recoupled tensors
    const CInteractionPN& interactionPN,      // V_pn expanded in terms of recoupled tensors
    // output:
    std::ofstream& proton_file, std::ofstream& neutron_file, std::ofstream& protonneutron_file) {
   // Create a basis of <Nhw' (lm' mu') 2Sp' 2Sn' 2S'| subspace
   int nhw_bra = bra_space[Nhw];
   int ssp_bra = bra_space[SSp];
   int ssn_bra = bra_space[SSn];
   int ss_bra = bra_space[SS];
   int lm_bra = bra_space[LM];
   int mu_bra = bra_space[MU];
   proton_neutron::ModelSpace bra_ncsmModelSpace(nprotons, nneutrons, nhw_bra, ssp_bra, ssn_bra,
                                                 ss_bra, lm_bra, mu_bra,
                                                 jj_irrelevant);  // == -1 -> all irreps
   // Create a basis of <Nhw' (lm' mu') 2Sp' 2Sn' 2S'| subspace
   lsu3::CncsmSU3xSU2Basis bra(bra_ncsmModelSpace, 0, 1);

   // Create a basis of |Nhw (lm mu) 2Sp 2Sn 2S> subspace
   int nhw_ket = ket_space[Nhw];
   int ssp_ket = ket_space[SSp];
   int ssn_ket = ket_space[SSn];
   int ss_ket = ket_space[SS];
   int lm_ket = ket_space[LM];
   int mu_ket = ket_space[MU];
   proton_neutron::ModelSpace ket_ncsmModelSpace(nprotons, nneutrons, nhw_ket, ssp_ket, ssn_ket,
                                                 ss_ket, lm_ket, mu_ket,
                                                 jj_irrelevant);  // == -1 -> all irreps
   // Create a basis of |Nhw (lm mu) 2Sp 2Sn 2S> subspace
   lsu3::CncsmSU3xSU2Basis ket(ket_ncsmModelSpace, 0, 1);

   std::vector<unsigned char> hoShells_n, hoShells_p;
   std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
   std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

   std::vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

   unsigned char num_vacuums_J_distr_p;
   unsigned char num_vacuums_J_distr_n;
   std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*>> selected_tensorsPP,
       selected_tensorsNN;
   std::vector<std::pair<CRMECalculator*, unsigned int>> selected_tensors_p_pn,
       selected_tensors_n_pn;

   std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
   std::vector<std::pair<SU3xSU2::RME*, unsigned int>> rme_index_p_pn, rme_index_n_pn;

   std::vector<MECalculatorData> rmeCoeffsPN_p;
   std::vector<MECalculatorData> rmeCoeffsPN_n;
   std::vector<MECalculatorData> rmeCoeffsPN_pn;

   SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

   uint16_t max_mult_p = bra.getMaximalMultiplicity_p();
   uint16_t max_mult_n = bra.getMaximalMultiplicity_n();

   InitializeIdenticalOperatorRME(identityOperatorRMEPP, max_mult_p*max_mult_p);
   InitializeIdenticalOperatorRME(identityOperatorRMENN, max_mult_n*max_mult_n);

   SingleDistributionSmallVector distr_ip, distr_in, distr_jp, distr_jn;
   UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
   SU3xSU2_SMALL_VEC vW_ip, vW_in, vW_jp, vW_jn;

   unsigned char deltaP, deltaN;

   const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
   const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

   int32_t icurrentDistr_p, icurrentDistr_n;
   int32_t icurrentGamma_p, icurrentGamma_n;

   // iterate over blocks of irreps generated by coupling proton and neutron irreps (ip in)
   for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      // we use the fact that our basis is made up of equivalent N (lm mu) Sp Sn S irreps
      // number of irreps wpn: (ip x in) --> {wpn} is equal to 0 or 1
      int32_t ibegin = bra.blockBegin(ipin_block);
      int32_t iend = bra.blockEnd(ipin_block);
      if (ibegin == iend) { // an empty ipin_block
         continue;
      }
      // since ip in may generate a single N S (lm mu) irrep
      assert((iend - ibegin) == 1);
      uint32_t ip = bra.getProtonIrrepId(ipin_block);
      uint32_t in = bra.getNeutronIrrepId(ipin_block);
      // omega_pn_I.rho = multiplicity due to coupling (ip x in) -> wpn_I
      SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, ibegin));

      SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

      uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max());
      uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max());

      uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max());
      uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max());

      uint32_t last_jp(std::numeric_limits<uint32_t>::max());
      uint32_t last_jn(std::numeric_limits<uint32_t>::max());

      // iterate over blocks of irreps generated by coupling proton and neutron irreps (jp jn)
      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         int32_t jbegin = ket.blockBegin(jpjn_block);
         int32_t jend = ket.blockEnd(jpjn_block);
         if (jbegin == jend) {
            continue;
         }
         assert((jend - jbegin) == 1);
         uint32_t jp = ket.getProtonIrrepId(jpjn_block);
         uint32_t jn = ket.getNeutronIrrepId(jpjn_block);
         // omega_pn_J.rho = multiplicity due to coupling (jp x jn) -> wpn_J
         SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jbegin));

         SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
         SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

         uint16_t ajp_max = ket.getMult_p(jp);
         uint16_t ajn_max = ket.getMult_n(jn);

         if (jp != last_jp) {
            icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
            icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

            if (ilastDistr_p != icurrentDistr_p) {
               distr_ip.resize(0);
               bra.getDistr_p(ip, distr_ip);
               gamma_ip.resize(0);
               bra.getGamma_p(ip, gamma_ip);
               vW_ip.resize(0);
               bra.getOmega_p(ip, vW_ip);

               distr_jp.resize(0);
               ket.getDistr_p(jp, distr_jp);
               hoShells_p.resize(0);
               deltaP = TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p,
                   num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
            }

            if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p) {
               if (deltaP <= 4) {
                  if (!selected_tensorsPP.empty()) {
                     std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(),
                                   CTensorGroup::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensorsPP.resize(0);

                  if (!selected_tensors_p_pn.empty()) {
                     std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(),
                                   CTensorGroup_ada::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensors_p_pn.resize(0);

                  gamma_jp.resize(0);
                  ket.getGamma_p(jp, gamma_jp);
                  TransformGammaKet_SelectByGammas(
                      hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP,
                      tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp,
                      selected_tensorsPP, selected_tensors_p_pn);
               }
            }

            if (deltaP <= 4) {
               Reset_rmeCoeffs(rmeCoeffsPP);
               Reset_rmeIndex(rme_index_p_pn);

               vW_jp.resize(0);
               ket.getOmega_p(jp, vW_jp);
               TransformOmegaKet_CalculateRME(
                   distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP,
                   selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
            }
            ilastDistr_p = icurrentDistr_p;
            ilastGamma_p = icurrentGamma_p;
         }

         if (jn != last_jn) {
            icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
            icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

            if (ilastDistr_n != icurrentDistr_n) {
               distr_in.resize(0);
               bra.getDistr_n(in, distr_in);
               gamma_in.resize(0);
               bra.getGamma_n(in, gamma_in);
               vW_in.resize(0);
               bra.getOmega_n(in, vW_in);

               distr_jn.resize(0);
               ket.getDistr_n(jn, distr_jn);
               hoShells_n.resize(0);
               deltaN = TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n,
                   num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
            }

            if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n) {
               if (deltaN <= 4) {
                  if (!selected_tensorsNN.empty()) {
                     std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(),
                                   CTensorGroup::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensorsNN.resize(0);

                  if (!selected_tensors_n_pn.empty()) {
                     std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(),
                                   CTensorGroup_ada::DeleteCRMECalculatorPtrs());
                  }
                  selected_tensors_n_pn.resize(0);

                  gamma_jn.resize(0);
                  ket.getGamma_n(jn, gamma_jn);
                  TransformGammaKet_SelectByGammas(
                      hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN,
                      tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn,
                      selected_tensorsNN, selected_tensors_n_pn);
               }
            }

            if (deltaN <= 4) {
               Reset_rmeCoeffs(rmeCoeffsNN);
               Reset_rmeIndex(rme_index_n_pn);

               vW_jn.resize(0);
               ket.getOmega_n(jn, vW_jn);
               TransformOmegaKet_CalculateRME(
                   distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN,
                   selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
            }

            ilastDistr_n = icurrentDistr_n;
            ilastGamma_n = icurrentGamma_n;
         }

         if (deltaP + deltaN <= 4) {
            Reset_rmeCoeffs(rmeCoeffsPN_p);
            if (in == jn && !rmeCoeffsPP.empty()) {
               //	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.
               CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
               //	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}]
               //|||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}
               Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP,
                                                  identityOperatorRMENN, rmeCoeffsPN_p);
            }

            Reset_rmeCoeffs(rmeCoeffsPN_n);
            if (ip == jp && !rmeCoeffsNN.empty()) {
               //	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.
               CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
               //	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}]
               //|||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}
               // This works even for k_dependent_tensor_strenghts = false
               Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP,
                                                   rmeCoeffsNN, rmeCoeffsPN_n);
            }

            Reset_rmeCoeffs(rmeCoeffsPN_pn);
            if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty()) {
               // This does not work for k_dependent_tensor_strenghts = false (unless
               // kmax(S0) is always equal to 1)!
               //							CalculatePNInteractionMeData(interactionPN,
               // omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPN_pn);
               //
               //							This function computes tensor
               //rmes and assumes coefficients are only
               // rho0-dependent 							Also it does
               // not prepare SU(3)>SO(3) clebsch-gordan coefficients as they are not needed
               CalculatePNOperatorRMECoeffs(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn,
                                            rme_index_n_pn, rmeCoeffsPN_pn);
            }

            if (!rmeCoeffsPN_p.empty()) {
               // These values must be constant for each tensor
               int32_t bra_max = rmeCoeffsPN_p[0].rmes_->m_bra_max;
               int32_t ket_max = rmeCoeffsPN_p[0].rmes_->m_ket_max;
               int32_t rhot_max = rmeCoeffsPN_p[0].rmes_->m_rhot_max;
               proton_file << ip << " " << in << " " << bra_max << " " << ssp_bra << " " << ssn_bra
                           << " " << ss_bra << " " << nhw_bra << " " << lm_bra << " " << mu_bra
                           << " ";
               proton_file << jp << " " << jn << " " << ket_max << " " << ssp_ket << " " << ssn_ket
                           << " " << ss_ket << " " << nhw_ket << " " << lm_ket << " " << mu_ket
                           << std::endl;
               std::vector<double> rmesp = ComputeResultingRMEs(rmeCoeffsPN_p);
               assert(rmesp.size() == bra_max * ket_max * rhot_max);
               for (auto rme : rmesp) {
                  proton_file << rme << std::endl;
               }
            }

            if (!rmeCoeffsPN_n.empty()) {
               // These values must be constant for each tensor
               int32_t bra_max = rmeCoeffsPN_n[0].rmes_->m_bra_max;
               int32_t ket_max = rmeCoeffsPN_n[0].rmes_->m_ket_max;
               int32_t rhot_max = rmeCoeffsPN_n[0].rmes_->m_rhot_max;
               neutron_file << ip << " " << in << " " << bra_max << " " << ssp_bra << " " << ssn_bra
                            << " " << ss_bra << " " << nhw_bra << " " << lm_bra << " " << mu_bra
                            << " ";
               neutron_file << jp << " " << jn << " " << ket_max << " " << ssp_ket << " " << ssn_ket
                            << " " << ss_ket << " " << nhw_ket << " " << lm_ket << " " << mu_ket
                            << std::endl;
               std::vector<double> rmesn = ComputeResultingRMEs(rmeCoeffsPN_n);
               assert(rmesn.size() == bra_max * ket_max * rhot_max);
               for (auto rme : rmesn) {
                  neutron_file << rme << std::endl;
               }
            }

            if (!rmeCoeffsPN_pn.empty()) {
               // These values must be constant for each tensor
               int32_t bra_max = rmeCoeffsPN_pn[0].rmes_->m_bra_max;
               int32_t ket_max = rmeCoeffsPN_pn[0].rmes_->m_ket_max;
               int32_t rhot_max = rmeCoeffsPN_pn[0].rmes_->m_rhot_max;
               protonneutron_file << ip << " " << in << " " << bra_max << " " << ssp_bra << " "
                                  << ssn_bra << " " << ss_bra << " " << nhw_bra << " " << lm_bra
                                  << " " << mu_bra << " ";
               protonneutron_file << jp << " " << jn << " " << ket_max << " " << ssp_ket << " "
                                  << ssn_ket << " " << ss_ket << " " << nhw_ket << " " << lm_ket
                                  << " " << mu_ket << std::endl;
               std::vector<double> rmespn = ComputeResultingRMEs(rmeCoeffsPN_pn);
               assert(rmespn.size() == bra_max * ket_max * rhot_max);
               for (auto rme : rmespn) {
                  protonneutron_file << rme << std::endl;
               }
            }
         }
         last_jp = jp;
         last_jn = jn;
      }
   }
   Reset_rmeCoeffs(rmeCoeffsPP);
   Reset_rmeCoeffs(rmeCoeffsNN);
   Reset_rmeCoeffs(rmeCoeffsPN_p);
   Reset_rmeCoeffs(rmeCoeffsPN_n);
   //	delete arrays allocated in identityOperatorRME?? structures
   delete[] identityOperatorRMEPP.m_rme;
   delete[] identityOperatorRMENN.m_rme;
}

int main(int argc, char** argv) {
   boost::mpi::environment env(argc, argv);
   boost::mpi::communicator mpi_comm_world;
   if (argc != 3) {
      cout << "Usage: " << argv[0] << " <model space> <tensor list file>" << endl;
      cout << endl;
      cout << "Compute reduced matrix elements (rmes) for a set of tensors provided in <tensor "
              "list file> and U(3) x SU(2)p x SU(2)n x SU(2) irreps that constitute a given <model "
              "space>."
           << std::endl;
      cout << "NOTE:Value of 2J specified in <model space> must be equal to -1 to consider all "
              "irreps spanning a given model space."
           << std::endl;
      cout << "Note:";
      cout << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can "
              "set the "
              "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
           << endl;
      return EXIT_FAILURE;
   }
   int my_rank = mpi_comm_world.rank();
   int nprocs = mpi_comm_world.size();

   if (nprocs != 1) {
      if (my_rank == 0) {
         std::cerr << "Only one MPI process allowed at this moment!" << std::endl;
         mpi_comm_world.abort(EXIT_FAILURE);
      }
   }

   // .PPNN and .PN files with recoupled tensors have coefficients independent of k quantum number.
   // as they represent a linear combination of SU(3)xSU(2) tensors, i.e. are not labeled by k0 L0
   // J0 M0 quantum numbers
   k_dependent_tensor_strenghts = false;

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   // create a list of U(3) x SU(2)p x SU(2)n x SU(2) labels of subspaces
   // that constitute a given model space
   // each element contains Nhw lm mu 2Sp 2Sn 2S
   std::vector<std::array<int, 6>> u3xsu2_subspaces = CreateListofU3SU2subspaces(argv[1]);
   // read a list of tensors that we need to compute
   std::vector<std::array<int, 13>> tensors = ReadListOfTensors(argv[2]);
   /*
      for (const auto& u3su2 : u3xsu2_subspaces)
      {
         std::cout << u3su2[Nhw] << " " << u3su2[SSp] << " " << u3su2[SSn] << " " << u3su2[SS] << "
      " << u3su2[LM] << " " << u3su2[MU] << std::endl;
      }
   */

   std::ofstream output_file("/dev/null");
   bool allow_generate_missing_rme_files = (mpi_comm_world.size() == 1);
   CBaseSU3Irreps baseSU3Irreps(nprotons, nneutrons, Nmax);

   su3::init();
   // iterate over tensors
   for (const auto& tensor : tensors) {
      int rho0max = SU3::mult(SU3::LABELS(1, tensor[LMF], tensor[MUF]),
                              SU3::LABELS(1, tensor[LMI], tensor[MUI]),
                              SU3::LABELS(1, tensor[LM0], tensor[MU0]));

      std::cout << "Tensor: [{a+" << tensor[N1] << "a+" << tensor[N2] << "}^(" << tensor[LMF] << " "
                << tensor[MUF] << ")" << tensor[SSF];
      std::cout << " x {ta" << tensor[N3] << "ta" << tensor[N4] << "}^(" << tensor[LMI] << " "
                << tensor[MUI] << ")" << tensor[SSI];
      std::cout << "]^rho0max:" << rho0max << "(" << tensor[LM0] << " " << tensor[MU0] << ")"
                << tensor[SS0] << std::endl;

      // Iterate over rho0 multiplicities
      for (int rho0 = 0; rho0 < rho0max; ++rho0) {
         std::cout << "\t rho0:" << rho0 << std::endl;

         // create .PPNN and .PN files that contain expansion of a rho0 component of an input tensor
         // in terms of tensors with HO shells recoupled in an increasing order
         std::string filename_prefix = RecoupleTensorStoreOnDisk(tensor, rho0max, rho0);

         // filenames for proton, neutron, and proton-neutron rmes
         std::string proton_filename(filename_prefix + ".pp");
         std::string neutron_filename(filename_prefix + ".nn");
         std::string protonneutron_filename(filename_prefix + ".pn");

         CInteractionPPNN interactionPPNN(baseSU3Irreps, true, output_file);
         CInteractionPN interactionPN(baseSU3Irreps, allow_generate_missing_rme_files, true,
                                      output_file);

         // load file with recoupled expansion of the input proton and neutron tensor
         interactionPPNN.LoadTwoBodyOperators(
             my_rank, std::vector<std::pair<std::string, float>>(
                          1, std::make_pair(filename_prefix + ".PPNN",
                                            1.0)));  // this will load all rme tables into memory
         // transform expansion coefficients from {cp, cn, cp', cn', ...} --> {cp, cp', ..., cn,
         // cn', ...}
         interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();
         // This methos loads file with recoupled expansion of the input proton-neutron tensor
         interactionPN.AddOperators(my_rank, std::vector<std::pair<std::string, float>>(
                                                 1, std::make_pair(filename_prefix + ".PN", 1.0)));

         std::ofstream proton_file(proton_filename);
         if (!proton_file) {
            std::cerr << "Could not open file '" << proton_filename << "'" << std::endl;
            return EXIT_FAILURE;
         }
         std::ofstream neutron_file(neutron_filename);
         if (!neutron_file) {
            std::cerr << "Could not open file '" << neutron_filename << "'" << std::endl;
            return EXIT_FAILURE;
         }

         std::ofstream protonneutron_file(protonneutron_filename);
         if (!neutron_file) {
            std::cerr << "Could not open file '" << protonneutron_filename << "'" << std::endl;
            return EXIT_FAILURE;
         }

         int dN = tensor[N1] + tensor[N2] - tensor[N3] - tensor[N4];
         // iterate over < N' 2Sp' 2Sn' 2S' (lm' mu') | spaces
         for (const auto& bra_space : u3xsu2_subspaces) {
            // iterate over | N 2Sp 2Sn 2S (lm mu) > spaces
            for (const auto& ket_space : u3xsu2_subspaces) {
               // Apply selection rules based on U(3) HO excitations
               if (bra_space[Nhw] != (ket_space[Nhw] + dN)) {
                  continue;
               }
               // S x S0 --> S' ?
               if (!SU2::mult(ket_space[SS], tensor[SS0], bra_space[SS])) {
                  continue;
               }
               // (lm mu) x (lm0 mu0) --> (lm' mu') ?
               if (!SU3::mult(SU3::LABELS(ket_space[LM], ket_space[MU]),
                              SU3::LABELS(tensor[LM0], tensor[MU0]),
                              SU3::LABELS(bra_space[LM], bra_space[MU]))) {
                  continue;
               }

               std::cout << "<" << bra_space[Nhw] << "hw SSp:" << bra_space[SSp]
                         << " SSn:" << bra_space[SSn] << " SS:" << bra_space[SS] << "("
                         << bra_space[LM] << " " << bra_space[MU] << ")|";
               std::cout << " |" << ket_space[Nhw] << "hw SSp:" << ket_space[SSp]
                         << " SSn:" << ket_space[SSn] << " SS:" << ket_space[SS] << "("
                         << ket_space[LM] << " " << ket_space[MU] << ")> \t";

               // Compute < * N' 2Sp' 2Sn' 2S' (lm' mu')||| V ||| * N 2Sp 2Sn 2S (lm mu) >_rhot
               // for V=Vpp, Vnn, and Vpn.
               // Interaction tensor is supplied as interactionPPNN and interactionPN classes
               ComputeRMEs(bra_space, ket_space, interactionPPNN, interactionPN, proton_file,
                           neutron_file, protonneutron_file);

               std::cout << "DONE" << std::endl;
            }
         }
      }
   }
   su3::finalize();
   return EXIT_SUCCESS;
}
