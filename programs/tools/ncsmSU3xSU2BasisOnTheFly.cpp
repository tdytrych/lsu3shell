#include <SU3ME/ModelSpaceExclusionRules.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
using namespace std;

void Print(const CTuple<int, 4>& lmS)
{
	cout << lmS[3] << "(" << lmS[0] << " " << lmS[1] << ")" << lmS[2];
}

void PrintState(const vector<CTuple<int, 4> >& Labels_p, const vector<CTuple<int, 4> >& Labels_n,  const SU3xSU2::LABELS& omega_pn)
{
	cout << "[";
	size_t index;
	size_t nOmegas_p = (Labels_p.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_p; ++i)
	{
		cout << "{";
	}
	Print(Labels_p[0]);
	if (nOmegas_p > 0)
	{
		cout << " x ";
		Print(Labels_p[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_p - 1; ++i)
		{
			Print(Labels_p[index]); cout << " x ";
			Print(Labels_p[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_p[index]);
	}

	cout << "] x ["; 
	size_t nOmegas_n = (Labels_n.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_n; ++i)
	{
		cout << "{";
	}
	Print(Labels_n[0]);
	if (nOmegas_n > 0)
	{
		cout << " x ";
		Print(Labels_n[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_n - 1; ++i)
		{
			Print(Labels_n[index]); cout << " x ";
			Print(Labels_n[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_n[index]);
	}
	cout << "] " << (int)omega_pn.rho << "(" << (int)omega_pn.lm << " " << (int)omega_pn.mu << ")" << (int)omega_pn.S2; 
}

////////////////////////////////////////////////////////////////////////
//	The following set of functions is merely for the output purposes.  I needed
//	to do this quickly, so it is messy ... but seems to work at least good
//	enough for the testing/debbuging rme calculation purposes ...
//
void Set(CTuple<int, 4>& lmS, const UN::SU3xSU2& gamma)
{
	lmS[0] = gamma.lm; lmS[1] = gamma.mu; lmS[2] = gamma.S2; lmS[3] = gamma.mult;
}
void Set(CTuple<int, 4>& lmS, const SU3xSU2::LABELS& omega)
{
	lmS[0] = omega.lm; lmS[1] = omega.mu; lmS[2] = omega.S2; lmS[3] = omega.rho;
}
//	Take gamma and omega and creates a single array of four integers with structure
//
//	Labels = { 	{gamma[0].lm, gamma[0].mu, gamma[0].S2, gamma[0].mult}, 
//				{gamma[1].lm, gamma[1].mu, gamma[1].S2, gamma[1].mult}, 
//				{omega[0].lm, omega[0].mu, omega[0].S2, omega[0].rho}, 
//				{gamma[2].lm, gamma[2].mu, gamma[2].S2, gamma[2].mult}, 
//				{omega[1].lm, omega[1].mu, omega[1].S2, omega[1].rho}, 
//				.
//				.
//				.
//				{omega[#shells - 1].lm, omega[#shells - 1].mu, omega[#shells - 1].S2, omega[#shells - 1].rho}, 
template<class T>
void Output(const T& gamma, const SU3xSU2_SMALL_VEC& omega, vector<CTuple<int, 4> >& Labels)
{
	CTuple<int, 4> lmS;

	Set(lmS, gamma[0]); Labels.push_back(lmS); 

	if (omega.empty())
	{
		return;
	}

	Set(lmS, gamma[1]); Labels.push_back(lmS);

	for (size_t iomega = 0; iomega < omega.size() - 1; ++iomega)
	{
		Set(lmS, omega[iomega]); Labels.push_back(lmS);
		Set(lmS, gamma[iomega+2]); Labels.push_back(lmS);
	}
	Set(lmS, (omega.back()));
	Labels.push_back(lmS); 
}

void Print( const SingleDistributionSmallVector& distr_p, const SingleDistributionSmallVector& distr_n)
{
	cout << "[";
	for (size_t i = 0; i < distr_p.size() - 1; ++i)
	{
		cout << (int)distr_p[i] << " ";
	}
	cout << (int)distr_p.back() << "] x [";

	for (size_t i = 0; i < distr_n.size() - 1; ++i)
	{
		cout << (int)distr_n[i] << " ";
	}
	cout << (int)distr_n.back() << "]" << endl;
}

void Print(const UN::SU3xSU2_VEC& gamma_p, const SU3xSU2_SMALL_VEC& omega_p, const UN::SU3xSU2_VEC& gamma_n, const SU3xSU2_SMALL_VEC& omega_n, const SU3xSU2::LABELS& omega_pn)
{
	vector<CTuple<int, 4> > Labels_p;
	vector<CTuple<int, 4> > Labels_n;

	Output(gamma_p, omega_p, Labels_p);
	Output(gamma_n, omega_n, Labels_n);

	PrintState(Labels_p, Labels_n, omega_pn);
}


struct Less_SU3
{
	inline bool operator() (const SU3::LABELS& l, const SU3::LABELS& r) const {return l.C2() < r.C2() || (l.C2() == r.C2() && l < r);}
};

void IterateOverBasis(const CncsmSU3Basis& Basis, const SU3xSU2::IrrepsContainer<IRREPBASIS>& wpn_irreps_container, unsigned long firstStateId, unsigned int idiag, unsigned int ndiag)
{
	unsigned int ap, an;
	unsigned char status;
	unsigned long idim = 0;
	unsigned int irrep_dim = 0;
	unsigned int nomega_pn = 0;
	
	unsigned int nirreps = 0;

	SingleDistributionSmallVector distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_SMALL_VEC omega_p, omega_n;
	SU3xSU2::LABELS omega_pn;
	PNConfIterator iter = Basis.firstPNConf(idiag, ndiag);

	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		assert(iter.hasPNOmega());

		omega_pn = iter.getCurrentSU3xSU2();
		irrep_dim = wpn_irreps_container.rhomax_x_dim(omega_pn);	// rhomax * dim[(lm mu)S]
		status = iter.status();
		switch (status)
		{
			case PNConfIterator::kNewDistr_p: distr_p.resize(0); iter.getDistr_p(distr_p);
			case PNConfIterator::kNewDistr_n: 
				distr_n.resize(0); 
				iter.getDistr_n(distr_n); 
				Print(distr_p, distr_n); // in 99.99% cases, a given distr_p distr_n will is not going to be an empty
			case PNConfIterator::kNewGamma_p: gamma_p.resize(0); iter.getGamma_p(gamma_p);
			case PNConfIterator::kNewGamma_n: gamma_n.resize(0); iter.getGamma_n(gamma_n);
			case PNConfIterator::kNewOmega_p: omega_p.resize(0); iter.getOmega_p(omega_p); ap = iter.getMult_p();
			case PNConfIterator::kNewOmega_n: omega_n.resize(0); iter.getOmega_n(omega_n); an = iter.getMult_n();
		}
		if (irrep_dim == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}
		nirreps++;
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();

		Print(gamma_p, omega_p, gamma_n, omega_n, omega_pn);
		cout << "\t\t" << firstStateId + idim;
		cout << "\t\t" << ap*an*irrep_dim << endl;
		idim += ap*an*irrep_dim; 
	}
	cout << "number of iterations [i.e. #omega_pn] = " << iter.num_PNomegas_iterated() << endl;
	cout << "number of iterations with dim[irrep] > 0 " << nirreps << endl;
}

int main(int argc,char **argv)
{
	if (argc != 2 && argc != 4)
	{
		cout << "Usage: " << argv[0] << " <model space definition> [<ndiag> <idiag>]" << endl;
		cout << "Implicitly ndiag=1 and idiag=0." << endl;
		return EXIT_FAILURE;
	}

	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);

	int ndiag(1);
	int idiag(0);
	if (argc == 4)
	{ 
		ndiag = atoi(argv[2]); 
		idiag = atoi(argv[3]);
	}
	cout << "ndiag = " << ndiag << " isection = " << idiag << endl;
	std::cout << "Constructing basis container ..." << std::endl;

	CncsmSU3Basis  Basis(ncsmModelSpace);
	SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container(Basis.GenerateFinal_SU3xSU2Irreps(), ncsmModelSpace.JJ());
	Basis.ShowMemoryRequirements();
	
	std::vector<unsigned long> dims(ndiag, 0);
	CalculateDimAllSections(Basis, dims, ndiag, wpn_irreps_container);
	unsigned long firstStateId = std::accumulate(dims.begin(), dims.begin() + idiag, (unsigned long)0); 
	IterateOverBasis(Basis, wpn_irreps_container, firstStateId, idiag, ndiag);

	cout << "Total size of the model space ... " << std::accumulate(dims.begin(), dims.end(), 0) << endl;
	cout << "dimension of section=" << idiag << " : " << dims[idiag] << endl;
//	wpn_irreps_container.ShowIrreps();
}
