#include <SU3NCSMUtils/clebschGordan.h>
#include <cassert>
#include <iostream>
#include <algorithm>

#define TEST_GSL

#ifdef TEST_GSL 
#include <gsl/gsl_sf.h>
#endif

#include <su3.h>
extern "C"
{
#include <wigxjpf.h>
}

using namespace std;

int main()
{
   su3::init();

	int j1, j2, j3, j4, j5, j6, j7, j8, j9;

	cin >> j1;
	cin >> j2;
	cin >> j3;
	cin >> j4;
	cin >> j5;
	cin >> j6;
	cin >> j7;
	cin >> j8;
	cin >> j9;

	cout << "Legacy:  " << wigner9j(j1, j2, j3, j4, j5, j6, j7, j8, j9) << endl;
	cout << "wigxjpf: " << wig9jj(j1, j2, j3, j4, j5, j6, j7, j8, j9) << endl;
#ifdef TEST_GSL	
	gsl_sf_result d;
	cout << "Using GSL:" << gsl_sf_coupling_9j_e(j1, j2, j3, j4, j5, j6, j7, j8, j9, &d) << endl;
	cout << "Using GSL:" << d.val << endl;
#endif	
   su3::finalize();
}
