$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := ComputeHamiltonianMeSp3R_nonDiag_MPI_OpenMP Sp3RGen_slepc_MPI GenerateSp3RkLJStates computeOverlaps eigensolver Sp3Rwfn2SU3wfn

# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))
