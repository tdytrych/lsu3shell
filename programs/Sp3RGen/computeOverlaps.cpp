#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/std.h>

#include <dirent.h>
#include <stdio.h>

#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

using namespace std;

typedef std::array<int, 3> SP3R_LABELS;
enum SP3R_LABELS_INDICES { kNsig = 0, kLMsig = 1, kMUsig = 2 };

typedef std::array<int, 6> U3xSU2_LABELS;    // Nhw lm mu 2Sp 2Sn 2S
typedef std::array<int, 6> SP3RxSU2_LABELS;  // N_sigmahw lm_sigma mu_sigma 2Sp 2Sn 2S
enum LABELS_INDICES { kN = 0, kLM = 1, kMU = 2, kSSp = 3, kSSn = 4, kSS = 5 };

typedef std::map<U3xSU2_LABELS, std::map<int, std::vector<SP3R_LABELS>>> MAP;

inline double C2SU3(int lm, int mu) {
   return (2.0 / 3.0) * (lm * lm + mu * mu + lm * mu + 3 * lm + 3 * mu);
}

inline double C2Sp3R(int lm, int mu, float N) {
   return C2SU3(lm, mu) + (1.0 / 3.0) * N * N - 4.0 * N;
}

// Calculate total harmonic oscillator energy of nuclei at Nhw = n0hw space.
float getN0(int nprotons, int nneutrons, int n0hw) {
   // N0 ... harmonic oscillator energy of a valence space configurations
   float N0 = 0;
   std::vector<uint8_t> proton_valence_distr;
   std::vector<uint8_t> neutron_valence_distr;

   for (int n = 0; true; ++n) {
      int8_t nferms = (n + 1) * (n + 2);
      if (nprotons > nferms) {
         proton_valence_distr.push_back(nferms);
         nprotons -= nferms;
      } else {
         proton_valence_distr.push_back(nprotons);
         break;
      }
   }

   for (int n = 0; true; ++n) {
      int8_t nferms = (n + 1) * (n + 2);
      if (nneutrons > nferms) {
         neutron_valence_distr.push_back(nferms);
         nneutrons -= nferms;
      } else {
         neutron_valence_distr.push_back(nneutrons);
         break;
      }
   }

   for (int n = 0; n < proton_valence_distr.size(); ++n) {
      N0 += proton_valence_distr[n] * (n + 1.5);
   }
   for (int n = 0; n < neutron_valence_distr.size(); ++n) {
      N0 += neutron_valence_distr[n] * (n + 1.5);
   }
   //	now we have to add additional energy [n0hw] energy and subtract
   //	center-of-mass HO energy
   return (N0 + n0hw) - 1.5;
}

void Get20Coupling(int r, SU3_VEC& wn) {
   int n1, n2, n3;
   for (n1 = 2 * r; n1 >= 0; n1 -= 2) {
      for (n2 = 0; n2 <= n1; n2 += 2) {
         for (n3 = 0; n3 <= n2; n3 += 2) {
            if (n1 + n2 + n3 == 2 * r) {
               wn.push_back(SU3::LABELS(1, (n1 - n2), (n2 - n3)));
            }
         }
      }
   }
}

// Iterate over basis of U(3)xSU(2) irreps and initialize data structure MAP,
// which is a map: U3xSU2_LABELS -------> map: 10*Nab ---> {N_sigma (lm_sigma
// mu_sigma) ... }.  This means that for each unique subspace Nhw Sp Sn S lm
// mu, one creates a map with key 10*NAB:0 and a vector with a single element
// Ns (lms mus) = N (lm mu) that corresponds to the bendhead Sp(3,R) state.
void GenerateMapSpSnSlmmu(lsu3::CncsmSU3xSU2Basis& basis, MAP& u3su2_to_NAB_sp3r) {
   const uint32_t number_blocks = basis.NumberOfBlocks();
   // iterate over basis of U(3)xSU(2) proton-neutron irreps
   for (int iblock = 0; iblock < number_blocks; ++iblock) {
      uint32_t ip = basis.getProtonIrrepId(iblock);
      uint32_t in = basis.getNeutronIrrepId(iblock);

      int Nhw = basis.nhw_p(ip) + basis.nhw_n(in);
      int ssp = (basis.getProtonSU3xSU2(ip)).S2;
      int ssn = (basis.getNeutronSU3xSU2(in)).S2;

      uint32_t ibegin = basis.blockBegin(iblock);
      uint32_t iend = basis.blockEnd(iblock);
      // for each U(3)xSU(2) irrep
      for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn) {
         SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
         U3xSU2_LABELS u3su2;
         u3su2[kN] = Nhw;
         u3su2[kLM] = omega_pn.lm;
         u3su2[kMU] = omega_pn.mu;
         u3su2[kSSp] = ssp;
         u3su2[kSSn] = ssn;
         u3su2[kSS] = omega_pn.S2;

         // Let's see if Nhw (lm mu) Sp Sn S is already in our map
         auto u3su2_to_NAB_iter = u3su2_to_NAB_sp3r.find(u3su2);

         // if not in MAP ==> create a 10*NAB --> sigma map with a single
         // element {0, Nsigma (lm_sigma mu_sigma) = N (lm mu)}
         if (u3su2_to_NAB_iter == u3su2_to_NAB_sp3r.end()) {
            //	Here I suppose that each U(3) subspace has symplectic bandheads
            //	This is however does not seem to be valid as, e.g. in 6Li I have
            //	encountered subspaces that did not have any bandheads
            SP3R_LABELS sp3r_labels;
            // Nsigma (lm_sigma mu_sigma) = Nhw (lm mu)
            sp3r_labels[kNsig] = u3su2[kN];
            sp3r_labels[kLMsig] = u3su2[kLM];
            sp3r_labels[kMUsig] = u3su2[kMU];
            // store a bandhead
            std::map<int, std::vector<SP3R_LABELS>> nab_sp3r_map;
            // bandhead ==> 10Nab = 0
            nab_sp3r_map[0] = std::vector<SP3R_LABELS>(1, sp3r_labels);
            u3su2_to_NAB_sp3r[u3su2] = nab_sp3r_map;
         }
      }
   }
}

// For each U(3)xSU(2) labels in MAP create a map of possible Sp(3,R) irreps
// that can give rise to a state of such U(3)xSU(2) symmetry, where 10*NAB is
// used as a key.  NOTE: single NAB can be associated with two different
// Sp(3,R) irreps!  therefore, each NAB has associated a vector of Sp(3,R)
// irreps.  If size of that vector > 1 ==> one cannot distinguish which state
// belongs to which Sp(3,R) irrep.
//
// typedef std::map<U3xSU2_LABELS, std::map<int, std::vector<SP3R_LABELS>>>
// MAP;
void GenerateSp3R(lsu3::CncsmSU3xSU2Basis& basis,
                  const std::set<U3xSU2_LABELS>& sp3r_bandheads_list, MAP& u3su2_to_NAB_sp3r) {
   double n0 = getN0(basis.NProtons(), basis.NNeutrons(), 0);
   // compute N_sigma corresponding to 0hw subspace
   // Iterate over all U(3)xSU(2) subspaces that make up a given model space
   // Note we consider even spaces that do not contain a given J. This is
   // because even if a bandhead does not contain J, higher lying Sp(3,R) basis
   // states may.
   for (const auto& sp3r_bandhead : sp3r_bandheads_list) {
      //	Generate U3 quantum numbers of Sp(3,R) bandhead
      const U3xSU2_LABELS& u3su2 = sp3r_bandhead;
      // quantum labels of the bandhead N0(lm0 mu0)SpSnS == N(lm mu)SpSnS
      int N0hw = u3su2[kN];
      int lm0 = u3su2[kLM];
      int mu0 = u3su2[kMU];
      int SSp = u3su2[kSSp];
      int SSn = u3su2[kSSn];
      int SS = u3su2[kSS];

      // bandhead labels
      SP3R_LABELS sp3r;
      sp3r[kNsig] = N0hw;
      sp3r[kLMsig] = lm0;
      sp3r[kMUsig] = mu0;

      // U(3)xSU(2) labels of Sp(3,R)xSU(2) > U(3)xSU(2) basis states
      U3xSU2_LABELS sp3r_u3su2;
      sp3r_u3su2[kSSp] = SSp;
      sp3r_u3su2[kSSn] = SSn;
      sp3r_u3su2[kSS] = SS;

      double a2 = C2Sp3R(lm0, mu0, n0 + N0hw);

      // For a given Sp(3, R) irrep: ir_sigma, generate all possible basis
      // state labels Sp(3,R) > U(3)
      SU3::LABELS ir_sigma(1, lm0, mu0);

      // iterate over (N0+2) <= N <= Nmax
      for (int nhw = N0hw + 2; nhw <= basis.Nmax(); nhw += 2) {
         sp3r_u3su2[kN] = nhw;
         int r = (nhw - N0hw) / 2;
         SU3_VEC wn;
         Get20Coupling(r, wn);
         for (int i = 0; i < wn.size(); ++i) {
            SU3_VEC w;
            // get final SU(3) symmetry due to coupling (lm0 mu0) x (lmn mun)
            // --> (lm mu)
            SU3::Couple(ir_sigma, wn[i], w);
            //	iterate over products of (lm_sig mu_sig) x (lm_n mu_n)
            for (int j = 0; j < w.size(); ++j) {
               sp3r_u3su2[kLM] = w[j].lm;
               sp3r_u3su2[kMU] = w[j].mu;
               // is resulting U(3)xSU(2) irrep nhw Sp Sn S (lm mu) in basis
               // model space ?
               auto selected_u3su2_iter = u3su2_to_NAB_sp3r.find(sp3r_u3su2);
               // yes, it is!
               if (selected_u3su2_iter != u3su2_to_NAB_sp3r.end()) {
                  double a1 = C2Sp3R(w[j].lm, w[j].mu, (n0 + nhw));
                  double nab = (1.0 / (2.0 * sqrt(6.0))) * (a1 - a2);
                  //                  int keyNab = floor(10*nab + 0.5);
                  int keyNab = 10 * nab;

                  auto nab_sp3r = selected_u3su2_iter->second.find(keyNab);
                  // Sp(3,R) Nab just encountered is not located in u3su2 space
                  // yet
                  if (nab_sp3r == selected_u3su2_iter->second.end()) {
                     // store associated Sp(3,R) irrep
                     (selected_u3su2_iter->second)[keyNab] = std::vector<SP3R_LABELS>(1, sp3r);
                  } else {  // Nab already stored
                     std::vector<SP3R_LABELS>& sp3r_irreps = nab_sp3r->second;
                     // if sp3r bandhead not not found in vector
                     if (std::find(sp3r_irreps.begin(), sp3r_irreps.end(), sp3r) ==
                         sp3r_irreps.end()) {
                        // add it
                        sp3r_irreps.push_back(sp3r);
                     }
                  }
               }
            }
         }
      }
   }
}

// std::map<U3xSU2_LABELS, std::map<int, std::vector<SP3R_LABELS>>> MAP;
void Show(MAP& u3su2_to_NAB_sp3r) {
   for (const auto& it : u3su2_to_NAB_sp3r) {
      U3xSU2_LABELS u3su2 = it.first;
      cout << u3su2[kN] << "hw"
           << " SSp:" << u3su2[kSSp] << " SSn:" << u3su2[kSSn] << " SS:" << u3su2[kSS];
      cout << "(" << u3su2[kLM] << " " << u3su2[kMU] << ") " << endl;
      if (it.second.empty()) {
         cout << "empty" << endl;
      } else {
         for (auto nab_sp3r_irreps : it.second) {
            cout << "Nab*10: " << nab_sp3r_irreps.first;
            for (auto sp3r : nab_sp3r_irreps.second) {
               cout << "\tNs: " << sp3r[0] << " (" << sp3r[1] << " " << sp3r[2] << ")"
                    << " ";
            }
            cout << endl;
         }
      }
   }
}

int has_valid_name(const std::string& u3su2dir_name) {
   size_t found = u3su2dir_name.find("hw");
   if (found == std::string::npos) {
      return false;
   }

   found = u3su2dir_name.find("_SSp");
   if (found == std::string::npos) {
      return false;
   }

   found = u3su2dir_name.find("_SSn");
   if (found == std::string::npos) {
      return false;
   }

   found = u3su2dir_name.find("_SS");
   if (found == std::string::npos) {
      return false;
   }

   return true;
}

// u3su2_subspace_dir: we assume *nwh_SSp*_SSn*_SS*_*_* naming pattern for
// directories that hold Sp(3,R) states. The last two "*" signs are associated
// with lm and mu labels.
void GetLabels(const std::string& u3su2_subspace_dir, int& nhw, int& ssp, int& ssn, int& ss,
               int& lm, int& mu) {
   size_t found_p = u3su2_subspace_dir.find("p");
   size_t found_n = u3su2_subspace_dir.rfind("n");
   size_t found_S = u3su2_subspace_dir.rfind("S");
   size_t found_last_ = u3su2_subspace_dir.rfind("_");
   size_t found_second_last_ = u3su2_subspace_dir.rfind("_", found_last_ - 1);

   assert(found_p != std::string::npos && found_n != std::string::npos &&
          found_S != std::string::npos && found_last_ != std::string::npos &&
          found_second_last_ != std::string::npos);

   nhw = std::stoi(u3su2_subspace_dir);
   ssp = std::stoi(u3su2_subspace_dir.substr(found_p + 1));
   ssn = std::stoi(u3su2_subspace_dir.substr(found_n + 1));
   ss = std::stoi(u3su2_subspace_dir.substr(found_S + 1));
   lm = std::stoi(u3su2_subspace_dir.substr(found_second_last_ + 1));
   mu = std::stoi(u3su2_subspace_dir.substr(found_last_ + 1));

   assert(nhw >= 0 && ssp >= 0 && ssn >= 0 && ss >= 0 && lm >= 0 && mu >= 0);

   //   cout << "Nhw:" << nhw << " SSp:" << ssp << " SSn:" << ssn << " SS:" <<
   //   ss << " lm:" << lm << " mu:" << mu << endl;
}

// Assume following naming order:
// Z*_N*_*hw_SSp*_SSn*_SS*_lm*_mu*_NUMBER_eigen*.dat_k*LL*JJ*
bool IsSp3RState(const std::string& filename) {
   size_t found_Z = (filename.find("Z") != std::string::npos);
   size_t found_N = (filename.find("N") != std::string::npos);
   size_t found_eigen = (filename.find("eigen") != std::string::npos);
   size_t found_dat = (filename.find("dat") != std::string::npos);
   size_t found_k = (filename.find("k") != std::string::npos);
   size_t found_LL = (filename.find("LL") != std::string::npos);
   size_t found_JJ = (filename.find("JJ") != std::string::npos);
   size_t found_lm = (filename.find("_lm") != std::string::npos);
   size_t found_mu = (filename.find("_mu") != std::string::npos);

   int isSp3RState = found_Z && found_N && found_eigen && found_dat && found_k && found_LL &&
                     found_mu && found_lm && found_JJ && has_valid_name(filename);
   return isSp3RState;
}

double GetNAB(const std::string& filename) {
   // Example of filenane:
   // Z3_N3_8hw_SSp1_SSn1_SS2_lm4_mu0_-10.61420_eigen1.dat_k0LL0JJ2
   size_t found_u = filename.rfind("u");
   size_t found__ = filename.find("_", found_u + 1);
   double nab = std::stod(filename.substr(found__ + 1));
   // NAB stored in file name is negative for Sp(3,R) irreps or equal to zero
   // for bandhead states
   assert(nab < 0.0 || nab < 0.01);

   // we need to multiply by -1 to get actual Nab value
   return -1.0 * nab;
}

// Compute overlaps of <wfn> spanning <bra> model space with vectors (given as
// files in <u3su2_wfns_in_directory>) that span Nhw Sp Sn S (lm mu) space of
// equivalent U(3)xSU(2) irreps. Store resulting overlaps in <overlaps> vector.
void calculateOverlapsU3xSU2wnfs(const std::vector<float>& wfn, const lsu3::CncsmSU3xSU2Basis& bra,
                                 int nhw, int ssp, int ssn, int ss, int lm, int mu,
                                 const std::vector<string>& u3su2_wfns_in_directory,
                                 std::vector<double>& overlaps) {
   cout.precision(8);
   cout.setf(ios::fixed);

   proton_neutron::ModelSpace ncsmModelSpace(bra.NProtons(), bra.NNeutrons(), nhw, ssp, ssn, ss, lm,
                                             mu, bra.JJ());
   lsu3::CncsmSU3xSU2Basis ket;
   // Note that we assume input U(3)xSU(2) wfns are provided with ndiag:1 order
   // of basis states
   ket.ConstructBasis(ncsmModelSpace, 0, 1);

   if (ket.dim() == 0) {
      std::cout << "Dimension of U3xSU2 subspace with Nhw:" << nhw << " 2Sp:" << ssp
                << " 2Sn:" << ssn << " 2S:" << ss << " (" << lm << " " << mu << ") is equal to 0!" << endl;
      return;
   }

   const lsu3::CncsmSU3xSU2Basis& J_basis = ket;
   const lsu3::CncsmSU3xSU2Basis& I_basis = bra;

   uint32_t irrep_dim;
   const uint32_t number_ipin_blocks = I_basis.NumberOfBlocks();  // variables starting with i
   const uint32_t number_jpjn_blocks = J_basis.NumberOfBlocks();  // variables starting with j
   std::vector<uint32_t> matching_bra_coeff_begin;
   // this vector contains indices of coefficients in bra wfn. Element at index
   // i, holds index poiting to a first coefficient in bra wfn that has the
   // same set of [U(N) SU(3)]_p x [U(N) SU(3)]_n --> SU(3) quantum numbers.
   // The lenght of following sequence is given my ap_max x an_max x rho_max x
   // dim(SU(3)xSU(2) for given J) The lenght of the vector
   // matching_bra_coeff_begin is thus less equal to dimension of ket space.
   matching_bra_coeff_begin.reserve(J_basis.getModelSpaceDim());

   uint32_t blockFirstRow(0);
   //	iterate over [jp jn] blocks of states
   for (unsigned int jpjn_block = 0, ipin_block = 0;
        jpjn_block < number_jpjn_blocks && ipin_block < number_ipin_blocks; jpjn_block++) {
      uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
      uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);

      //	Iterate from the current position in bigger basis, try to find (jp jn)
      //	block. We use the fact that blocks in basis are sorted in an
      //	increasing order.
      uint32_t ip(0), in(0);
      for (; ipin_block < number_ipin_blocks;
           ++ipin_block)  // ipin_block will be pointing to the next block one step ahead
      {
         ip = I_basis.getProtonIrrepId(ipin_block);
         in = I_basis.getNeutronIrrepId(ipin_block);
         if (ip == jp && in == jn) {
            break;  // blockFirstRow still points at the beggining of the (ip in) block
         }
         // increase index blockFirstRow to point to the first basis state of
         // the next block or to the end of the basis.
         blockFirstRow += I_basis.NumberOfStatesInBlock(ipin_block);
      }
      // either ip == jp && in == jn or we are at the end, i.e. ipin_block ==
      // number_ipin_blocks && blockFirstRow == dim[bra]
      if (ip == jp && in == jn) {
         //	we found matching pair (jp jn) == (ip in) ==> we will need to
         //	calculate its dimension ==> prepare ap_max & an_max
         uint32_t aip_max = I_basis.getMult_p(ip);  // must be qual to J_basis.getMult_p(jp)
         uint32_t ain_max = I_basis.getMult_n(in);  // must be qual to J_basis.getMult_n(jn)

         uint32_t jbegin = J_basis.blockBegin(jpjn_block);
         uint32_t jend = J_basis.blockEnd(jpjn_block);

         uint32_t ibegin = I_basis.blockBegin(ipin_block);
         uint32_t iend = I_basis.blockEnd(ipin_block);

         bool matching_states_found = false;

         uint32_t ipos_current(blockFirstRow);

         for (uint32_t jwpn = jbegin, iwpn = ibegin; jwpn < jend && iwpn < iend; ++jwpn) {
            SU3xSU2::LABELS omega_pn_J(J_basis.getOmega_pn(jp, jn, jwpn));

            for (; iwpn < iend; ++iwpn, ipos_current += irrep_dim) {
               SU3xSU2::LABELS omega_pn_I(I_basis.getOmega_pn(ip, in, iwpn));
               irrep_dim = aip_max * ain_max * omega_pn_I.rho * I_basis.omega_pn_dim(iwpn);

               matching_states_found = (omega_pn_J == omega_pn_I);
               if (matching_states_found) {
                  break;
               }
            }
            if (matching_states_found) {
               // matching_bra_coeff_begin[i] contains an index of the first
               // coefficient corresponding with (ip in wpnI) == (jp jn wpnJ)
               // irrep in ket space
               matching_bra_coeff_begin.push_back(ipos_current);
            }
         }
      }
   }

   if (matching_bra_coeff_begin.empty()) {
      std::cout << "U3xSU2 subspace with Nhw:" << nhw << " 2Sp:" << ssp
                << " 2Sn:" << ssn << " 2S:" << ss << " (" << lm << " " << mu << ") does not belong to wfn model space!" << endl;
      return;
   }


   vector<float> wfn_smaller(J_basis.getModelSpaceDim());

   //  prob_results[i] .... |<bra | ket_{i}>|^2
   std::vector<double> prob_results;
   //	Read ket wfn files and for each calculate its probability in bra wfn
   for (const auto& smaller_file_name : u3su2_wfns_in_directory) {
      fstream wfn_smaller_file(smaller_file_name.c_str(), std::ios::in | std::ios::binary);
      if (!wfn_smaller_file) {
         cerr << "Could not open file '" << smaller_file_name << "' !" << endl;
         exit(EXIT_FAILURE);
      }
      wfn_smaller_file.read((char*)&wfn_smaller[0], J_basis.getModelSpaceDim() * sizeof(float));

      double prob = 0;
      uint32_t imatching_bra_index = 0, icurrent = 0;
      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
         uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);

         uint32_t jbegin = J_basis.blockBegin(jpjn_block);
         uint32_t jend = J_basis.blockEnd(jpjn_block);
         for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn) {
            SU3xSU2::LABELS omega_pn(J_basis.getOmega_pn(jp, jn, jwpn));

            uint32_t icurr_bigger = matching_bra_coeff_begin[imatching_bra_index++];

            uint32_t ajp_max = J_basis.getMult_p(jp);
            uint32_t ajn_max = J_basis.getMult_n(jn);
            uint32_t idim = ajp_max * ajn_max * omega_pn.rho * J_basis.omega_pn_dim(jwpn);

            for (int j = 0; j < idim; ++j, ++icurrent) {
               prob += (double)wfn_smaller[icurrent] * (double)wfn[icurr_bigger + j];
            }
         }
      }
      overlaps.push_back(prob);
   }
   assert (overlaps.size() == u3su2_wfns_in_directory.size());
}

// calculate overlaps with all *.dat files in <u3su2_subspace_dir>
void CalculateOverlapsDirectory(const std::vector<float>& wfn, const lsu3::CncsmSU3xSU2Basis& basis,
                                const std::string& main_directory,
                                const std::string& u3su2_subspace_dir,
                                std::vector<string>& u3su2_wfns_in_directory,
                                std::vector<double>& nab_in_directory,
                                std::vector<double>& overlaps) {
   int nhw, ssp, ssn, ss, lm, mu;
   GetLabels(u3su2_subspace_dir, nhw, ssp, ssn, ss, lm, mu);

   std::string full_path = main_directory + u3su2_subspace_dir;

   DIR* dir = opendir(full_path.c_str());
   if (dir == nullptr) {
      std::cout << "Could not open directory '" << full_path << "'" << std::endl;
      return;
   }
   // step #1: get list of all *dat_k* files and their Nab values in
   // u3su2_subspace_dir
   struct dirent* ent;
   // read filenames of all the files in <u3su2_subspace_dir>
   while ((ent = readdir(dir)) != NULL) {
      std::string filename = ent->d_name;
      // if their name look like Sp(3,R) states include them in the list
      if (IsSp3RState(filename)) {
         // TODO: check they all have the same Z, N, J
         nab_in_directory.push_back(GetNAB(filename));
         u3su2_wfns_in_directory.push_back(full_path + filename);
      }
   }
   closedir(dir);

   // there are no *dat* files in a given directory => empty directory, skip
   // it.
   if (!u3su2_wfns_in_directory.empty()) {
      // Compute overlaps of wfn with vectors spanning Nhw 2Sp 2Sn 2S lm mu
      // subspace of equivalent U(3)xSU(2) irreps.
      calculateOverlapsU3xSU2wnfs(wfn, basis, nhw, ssp, ssn, ss, lm, mu, u3su2_wfns_in_directory,
                                  overlaps);
   }
}

void Overlaps_To_Sp3RxSU2_Probabilities(
    int ssp, int ssn, int ss, const std::map<int, std::vector<SP3R_LABELS>>& nab_to_sp3r_irreps,
    const std::vector<double>& nab_in_directory, const std::vector<std::string>& u3su2_wfns_in_directory, const std::vector<double>& overlaps,
    std::map<SP3RxSU2_LABELS, double>& sp3rsu2_irreps_prob,
    std::map<SP3RxSU2_LABELS, uint32_t>& sp3rsu2_irreps_number_states,
    uint32_t& number_sp3r_states_unassigned, double& unassigned_prob) {
   // iterate over overlaps
   for (size_t i = 0; i < overlaps.size(); ++i) {
      double nab = nab_in_directory[i];
      //      int keyNab = floor(10*nab + 0.5);
      int keyNab = 10 * nab;

      // find a list of Sp(3,R) irreps associated with
      auto nab_sp3r_irreps = nab_to_sp3r_irreps.find(keyNab);
      if (nab_sp3r_irreps == nab_to_sp3r_irreps.end()) {
        number_sp3r_states_unassigned += 1;
        unassigned_prob += 100.0 * overlaps[i] * overlaps[i];

         std::cout
             << "Error!  Could not find key 10 * Nab : " << keyNab
             << " in a map of(integer) 10 * Nab keys and associated vectors of Sp(3, R) labels."
             << std::endl;
         continue;
      }
      const std::vector<SP3R_LABELS>& sp3r_irreps = nab_sp3r_irreps->second;

      // there is only one unique Sp(3,R) irrep that yields given Nab
      if (sp3r_irreps.size() == 1) {
         SP3R_LABELS sp3r(sp3r_irreps[0]);
         SP3RxSU2_LABELS sp3rsu2;
         sp3rsu2[kNsig] = sp3r[kNsig];
         sp3rsu2[kLMsig] = sp3r[kLMsig];
         sp3rsu2[kMUsig] = sp3r[kMUsig];
         sp3rsu2[kSSp] = ssp;
         sp3rsu2[kSSn] = ssn;
         sp3rsu2[kSS] = ss;
         sp3rsu2_irreps_prob[sp3rsu2] += 100.0 * overlaps[i] * overlaps[i];
         sp3rsu2_irreps_number_states[sp3rsu2] += 1;
         //
         for (const auto& sp3r_label : sp3r_irreps) {
            int Ns = sp3r_label[kNsig];
            int lms = sp3r_label[kLMsig];
            int mus = sp3r_label[kMUsig];
            std::cout << Ns << "(" << lms << " " << mus << ") " << ssp << " " << ssn << " " << ss
                      << "\t";
         }
         std::cout << 100.0 * overlaps[i] * overlaps[i] << "\t" << u3su2_wfns_in_directory[i] << std::endl;

      } else {
         number_sp3r_states_unassigned += 1;
         unassigned_prob += 100.0 * overlaps[i] * overlaps[i];
      }
   }
}

// generate a set of Nhw 2Sp 2Sn 2S lm mu quantum numbers of U(3)xSU(2) irreps
// that exist in a given model space of basis. Do not care about value of J!
void GenerateU3xSU2SubspacesList(const proton_neutron::ModelSpace& ncsmModelSpace,
                                 const lsu3::CncsmSU3xSU2Basis& basis,
                                 std::set<U3xSU2_LABELS>& u3su2_irreps_in_model_space) {
   // Np lmp mup 2Sp
   std::set<std::array<int, 4>> proton_irreps;
   // Nn lmn mun 2Sn
   std::set<std::array<int, 4>> neutron_irreps;

   for (int ip = 0; ip < basis.pconf_size(); ++ip) {
      int Nphw = basis.nhw_p(ip);
      SU3xSU2::LABELS wp(basis.getProtonSU3xSU2(ip));
      proton_irreps.insert({Nphw, wp.lm, wp.mu, wp.S2});
   }

   for (int in = 0; in < basis.nconf_size(); ++in) {
      int Nnhw = basis.nhw_n(in);
      SU3xSU2::LABELS wn(basis.getNeutronSU3xSU2(in));
      neutron_irreps.insert({Nnhw, wn.lm, wn.mu, wn.S2});
   }

   SU3_VEC wpn_su3;
   std::vector<SU3_VEC> allowed_su3;
   SU2_VEC allowed_spins;

   for (const auto& proton_irrep : proton_irreps) {
      int Nphw = proton_irrep[0];
      int lmp = proton_irrep[1];
      int mup = proton_irrep[2];
      int SSp = proton_irrep[3];
      for (const auto& neutron_irrep : neutron_irreps) {
         int Nnhw = neutron_irrep[0];
         int lmn = neutron_irrep[1];
         int mun = neutron_irrep[2];
         int SSn = neutron_irrep[3];

         int Nhw = Nphw + Nnhw;
         if (Nhw > basis.Nmax()) {
            break;  // fermionic irreps are stored in an increasing order according to the number of
                    // oscillator quanta
         }

         auto nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
         if (nhwSubspace == ncsmModelSpace.end()) {
            continue;
         }

         allowed_spins.clear();
         allowed_su3.clear();
         nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(SSp, SSn), allowed_spins, allowed_su3);
         uint32_t number_spins = allowed_spins.size();
         if (number_spins) {
            wpn_su3.clear();
            SU3::Couple(SU3::LABELS(1, lmp, mup), SU3::LABELS(1, lmn, mun), wpn_su3);

            for (uint32_t ispin = 0; ispin < number_spins; ++ispin) {
               int SS = allowed_spins[ispin];
               if (!allowed_su3[ispin].empty())  //	==> I need to select only allowed(lm mu)
               {
                  for (const auto& current_su3 : wpn_su3) {
                     auto current_allowed_su3 = std::lower_bound(
                         allowed_su3[ispin].begin(), allowed_su3[ispin].end(), current_su3);
                     if (current_allowed_su3 != allowed_su3[ispin].end() &&
                         (current_allowed_su3->lm == current_su3.lm &&
                          current_allowed_su3->mu == current_su3.mu)) {
                        u3su2_irreps_in_model_space.insert(
                            {Nhw, current_su3.lm, current_su3.mu, SSp, SSn, SS});
                     }
                  }
               } else  //	if array of allowed SU(3) irreps is empty == > include ALL SU(3)
                       //irreps listed in wpn_su3
               {
                  for (const auto& current_su3 : wpn_su3) {
                     u3su2_irreps_in_model_space.insert(
                         {Nhw, current_su3.lm, current_su3.mu, SSp, SSn, SS});
                  }
               }
            }
         }
      }
   }
}

int main(int argc, char** argv) {
   if (argc != 4) {
      cout << "Compute overlaps between LSU3shell wfns (ndiag:1) that span a given model space, "
              "and Sp(3,R)>U(3) states stored in <directory>/*hw_SSp*_SSn*_SS*_*_*/ directories.";
      cout << endl;
      cout << "It is assumed Sp(3,R)>U(3) files have the following naming pattern "
              ": Z* _N* _* hw_SSp* _SSn* _SS* _lm* _mu* _<floating point number> _eigen*.dat_k* "
              "LL* JJ*";
      cout << endl;
      cout << "Usage: " << argv[0] << " <wfn> <model space> <directory>\n";
      cout << "<wfn> ... wave function for ndiag:1\n";
      cout << "<model space> ... model space spanned by<wfn>\n ";
      cout << " < directory > ... directory with Sp(3, R) states organized in directories as "
              "described above.\n";
      return EXIT_FAILURE;
   }
   std::string wfn_filename(argv[1]);
   std::string model_space_file_name(argv[2]);
   std::string main_directory(argv[3]);
   main_directory += "/";

   MAP u3su2_to_NAB_sp3r;

   proton_neutron::ModelSpace ncsmModelSpace;
   lsu3::CncsmSU3xSU2Basis basis;
   bool modelSpaceProvided = model_space_file_name.find(".model_space") !=
                             std::string::npos;  // true if the first argument contains
   if (!modelSpaceProvided)                      // ==> we need to load basis states from files
   {
      cout << "Reading basis from '" << MakeBasisName(model_space_file_name, 0, 1) << "'."
           << std::endl;
      basis.LoadBasis(model_space_file_name, 0, 1);
   } else {
      ncsmModelSpace.Load(model_space_file_name);
      basis.ConstructBasis(ncsmModelSpace, 0, 1);
   }

   // generate a list of Nhw 2Sp 2Sn 2S lm mu U(3)xSU(2) quantum numbers that
   // occur in model space NOTE: we include all possible irreps even if they do
   // not contain a given J of basis
   std::set<U3xSU2_LABELS> u3su2_irreps_in_model_space;
   GenerateU3xSU2SubspacesList(ncsmModelSpace, basis, u3su2_irreps_in_model_space);

   fstream wfn_file(argv[1], std::ios::in | std::ios::binary);
   if (!wfn_file) {
      std::cout << "Could not open '" << argv[1] << "' file." << std::endl;
      return EXIT_FAILURE;
   }
   vector<float> wfn(basis.getModelSpaceDim());
   wfn_file.read((char*)&wfn[0], basis.getModelSpaceDim() * sizeof(float));

   float n0 = getN0(basis.NProtons(), basis.NNeutrons(),
                    0);  // compute N_sigma corresponding to 0hw subspace

   // create map whose elements are all possible Nhw Sp Sn S lm mu subspaces
   // that compose basis.
   GenerateMapSpSnSlmmu(basis, u3su2_to_NAB_sp3r);
   GenerateSp3R(basis, u3su2_irreps_in_model_space, u3su2_to_NAB_sp3r);

   //   Show(u3su2_to_NAB_sp3r);

   DIR* dir = opendir(main_directory.c_str());
   if (dir == nullptr) {
      std::cout << "Directory '" << main_directory << "' does not exist." << std::endl;
   }

   // map of N_sigma (lm_sigma mu_sigma) 2Sp 2Sn 2S Sp(3,R) x SU(2) labels and
   // associated total probability
   std::map<SP3RxSU2_LABELS, double> sp3rsu2_irreps_prob;
   std::map<SP3RxSU2_LABELS, uint32_t> sp3rsu2_irreps_number_states;
   uint32_t number_sp3r_states_unassigned = 0;
   double unassigned_prob = 0;
   int dim_sp3r_basis = 0;
   // Read directories stored in main_directory
   struct dirent* entry = readdir(dir);
   while (entry != NULL) {
      if (entry->d_type == DT_DIR) {
         std::string u3su2_subspace_dir(entry->d_name);
         u3su2_subspace_dir += "/";
         int HasValidName = has_valid_name(u3su2_subspace_dir);
         // HasValidName if it contains "_SSp" "_SSn" "_SS" and "hw" strings
         if (HasValidName) {
            cout << u3su2_subspace_dir << endl;

            // list of *dat* files in directory
            // main_directory/u3su2_subspace_dir/
            std::vector<string> u3su2_wfns_in_directory;
            // NAB values for *dat* files
            std::vector<double> nab_in_directory;
            // overlaps of *dat* files
            std::vector<double> overlaps;

            CalculateOverlapsDirectory(wfn, basis, main_directory, u3su2_subspace_dir,
                                       u3su2_wfns_in_directory, nab_in_directory, overlaps);
            if (!overlaps.empty()) {
               int nhw, ssp, ssn, ss, lm, mu;
               GetLabels(u3su2_subspace_dir, nhw, ssp, ssn, ss, lm, mu);
               assert(lm >= 0 && lm < 255);
               assert(mu >= 0 && mu < 255);
               U3xSU2_LABELS u3su2;
               u3su2[kN] = nhw;
               u3su2[kLM] = lm;
               u3su2[kMU] = mu;
               u3su2[kSSp] = ssp;
               u3su2[kSSn] = ssn;
               u3su2[kSS] = ss;

               auto nab_to_sp3r = u3su2_to_NAB_sp3r.find(u3su2);
               if (nab_to_sp3r != u3su2_to_NAB_sp3r.end()) {
                  Overlaps_To_Sp3RxSU2_Probabilities(
                      ssp, ssn, ss, nab_to_sp3r->second, nab_in_directory, u3su2_wfns_in_directory, overlaps,
                      sp3rsu2_irreps_prob, sp3rsu2_irreps_number_states,
                      number_sp3r_states_unassigned, unassigned_prob);
               }
            }
            dim_sp3r_basis += overlaps.size();
         }
      }
      entry = readdir(dir);
   }
   closedir(dir);

   // copy results from sp3rsu2_irreps.prob into vector of pairs and sort
   // according to probabilities.
   std::vector<std::pair<SP3RxSU2_LABELS, double>> su3su2_sortedby_prob;
   su3su2_sortedby_prob.resize(sp3rsu2_irreps_prob.size());
   std::copy(sp3rsu2_irreps_prob.begin(), sp3rsu2_irreps_prob.end(), su3su2_sortedby_prob.begin());
   std::sort(su3su2_sortedby_prob.begin(), su3su2_sortedby_prob.end(),
             [](const std::pair<SP3RxSU2_LABELS, double>& a,
                const std::pair<SP3RxSU2_LABELS, double>& b) { return a.second > b.second; });

   int number_sp3r_identified = 0;
   double prob_sum = 0.0;
   uint32_t dim_sum = 0;

   for (const auto& sp3rsu2_prob : su3su2_sortedby_prob) {
      SP3RxSU2_LABELS sp3rsu2 = sp3rsu2_prob.first;
      prob_sum += sp3rsu2_prob.second;
      dim_sum  += sp3rsu2_irreps_number_states[sp3rsu2];
      std::cout << "Nsigma:" << sp3rsu2[kNsig] << " (" << sp3rsu2[kLMsig] << " " << sp3rsu2[kMUsig]
                << ") "
                << "2Sp:" << sp3rsu2[kSSp] << " 2Sn:" << sp3rsu2[kSSn] << " 2S:" << sp3rsu2[kSS]
                << "\t" << sp3rsu2_prob.second << "\t " << sp3rsu2_irreps_number_states[sp3rsu2]
                << "\t\t" << prob_sum << "\t" << dim_sum
                << std::endl;
      number_sp3r_identified += sp3rsu2_irreps_number_states[sp3rsu2];
   }
   std::cout << "Total number of Sp(3,R) states:" << dim_sp3r_basis << std::endl;
   std::cout << "Ouf of which " << number_sp3r_identified
             << " states can be directly associated with Ns(lms mus) Sp Sn S labels " << std::endl;
   std::cout << "Number of unassigned Sp(3,R) states:" << number_sp3r_states_unassigned
             << " and their total probability:" << unassigned_prob << std::endl;
}
