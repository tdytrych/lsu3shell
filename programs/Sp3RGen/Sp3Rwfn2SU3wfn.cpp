#include <LSU3/ncsmSU3xSU2Basis.h>
#include <array>
#include <iostream>
#include <string>
#include <vector>

// values obtained from <model space> input file
uint32_t JJ0, nneutrons, nprotons;

// sp3r_basis_filename:
// (1) dim (2) filenames associated with Nhw 2Sp 2Sn 2S lm mu JJ0 subspaces of
// Sp(3,R) basis states.
// u3xsu2_segments[i] ---> Nhw 2Sp 2Sn 2S lm mu i0 dim associated with U(3)xSU(2) Sp(3,R) basis
// states
// Note that files in sp3r_basis_filename may not come in increasing order in i0.
void ReadSp3RBasis(const std::string& sp3r_basis_filename,
                   std::vector<std::array<uint32_t, 8>>& u3xsu2_segments,
                   std::vector<std::string>& sp3r_basis_states) {
   std::fstream sp3r_basis(sp3r_basis_filename);

   if (!sp3r_basis) {
      std::cerr << "Could not open '" << sp3r_basis_filename
                << "' file that contains description of Sp(3,R) basis.\n";
      exit(EXIT_FAILURE);
   }
   uint32_t sp3r_dim;
   sp3r_basis >> sp3r_dim;
   if (sp3r_dim <= 0) {
      std::cerr << "\nError: number of Sp(3,R) states provided in file '" << sp3r_basis_filename
                << "' seems to be set to " << sp3r_dim << ".\n";
      exit(EXIT_FAILURE);
   }
   // sp3r_basis_states.size == sp3r_dim provided in sp3r_basis_filename file
   sp3r_basis_states.resize(sp3r_dim);
   while (true) {
      // segment == group of U(3)xSU(2) identical Sp(3,R) states
      std::string basis_segment_filename;
      sp3r_basis >> basis_segment_filename;
      if (!sp3r_basis) {
         break;
      }

//      std::cout << basis_segment_filename << std::endl;

      std::fstream sp3r_basis_segment(basis_segment_filename);
      if (!sp3r_basis_segment) {
         std::cerr << "Could not open '" << basis_segment_filename
                   << "' file that contains a segment of Sp(3,R) basis.\n";
         exit(EXIT_FAILURE);
      }

      uint32_t Z, N, Nhw, ssp, ssn, ss, lm, mu, jj0, i0, dim;

      sp3r_basis_segment >> Z >> N >> Nhw >> ssp >> ssn >> ss >> lm >> mu >> jj0 >> i0 >> dim;
      if (jj0 != JJ0) {
         std::cerr << "Model space is given for 2J0:" << JJ0
                   << " but a given segment of Sp(3,R) basis has 2J0:" << jj0 << "!\n";
         exit(EXIT_FAILURE);
      }
      if (Z != nprotons) {
         std::cerr << "Model space is given for Z:" << nprotons << " but '"
                   << basis_segment_filename << "' has Z:" << Z << std::endl;
         exit(EXIT_FAILURE);
      }
      if (N != nneutrons) {
         std::cerr << "Model space is given for N:" << nneutrons << " but '"
                   << basis_segment_filename << "' has N:" << N << std::endl;
         exit(EXIT_FAILURE);
      }

      //      std::cout << "Nhw:" << Nhw << " SSp:" << ssp << " SSn:" << ssn << " SS:" << ss << " ("
      //      << lm << " " << mu << ") i0:" << i0 << " dim:" << dim << std::endl;

      if (!sp3r_basis) {
         std::cerr << "File '" << basis_segment_filename << "' has wrong structure ..."
                   << std::endl;
         exit(EXIT_FAILURE);
      }

      u3xsu2_segments.push_back(std::array<uint32_t, 8>({Nhw, ssp, ssn, ss, lm, mu, i0, dim}));

      std::string sp3r_state_filename;
      for (uint32_t i = 0; i < dim; ++i) {
         if (!sp3r_basis) {
            std::cerr << "List of Sp(3,R) basis states in '" << basis_segment_filename
                      << "' does not contain " << dim << " names as suggested." << std::endl;
            exit(EXIT_FAILURE);
         }
         sp3r_basis_segment >> sp3r_state_filename;
         sp3r_basis_states[i0 + i] = sp3r_state_filename;
      }
   }
}

// wfn: resulting wave function expressed in U(3)xSU(2) basis of LSU3shell
// ket: basis of a given Nhw 2Sp 2Sn 2S lm mu J0 subspace
// u3su2_wfn: projection of input wave function onto Nhw 2Sp 2Sn 2S lm mu J0 subace U(3)xSU(2) basis
void U3SU2wfn2Total(std::vector<float>& wfn, const lsu3::CncsmSU3xSU2Basis& bra,
                    const lsu3::CncsmSU3xSU2Basis& ket, const std::vector<double>& u3su2_wfn) {
   const lsu3::CncsmSU3xSU2Basis& J_basis = ket;
   const lsu3::CncsmSU3xSU2Basis& I_basis = bra;

   uint32_t irrep_dim;
   const uint32_t number_ipin_blocks = I_basis.NumberOfBlocks();  // variables starting with i
   const uint32_t number_jpjn_blocks = J_basis.NumberOfBlocks();  // variables starting with j
   std::vector<uint32_t> matching_bra_coeff_begin;
   // this vector contains indices of coefficients in bra wfn. Element at index
   // i, holds index poiting to a first coefficient in bra wfn that has the
   // same set of [U(N) SU(3)]_p x [U(N) SU(3)]_n --> SU(3) quantum numbers.
   // The lenght of following sequence is given my ap_max x an_max x rho_max x
   // dim(SU(3)xSU(2) for given J) The lenght of the vector
   // matching_bra_coeff_begin is thus less equal to dimension of ket space.
   matching_bra_coeff_begin.reserve(J_basis.getModelSpaceDim());

   uint32_t blockFirstRow(0);
   //	iterate over [jp jn] blocks of states
   for (unsigned int jpjn_block = 0, ipin_block = 0;
        jpjn_block < number_jpjn_blocks && ipin_block < number_ipin_blocks; jpjn_block++) {
      if (!J_basis.NumberOfStatesInBlock(jpjn_block)) {
         continue;
      }
      uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
      uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);
      //	Iterate from the current position in bigger basis, try to find (jp jn)
      //	block. We use the fact that blocks in basis are sorted in an
      //	increasing order.
      uint32_t ip(0), in(0);
      for (; ipin_block < number_ipin_blocks;
           ++ipin_block)  // ipin_block will be pointing to the next block one step ahead
      {
         ip = I_basis.getProtonIrrepId(ipin_block);
         in = I_basis.getNeutronIrrepId(ipin_block);
         if (ip == jp && in == jn) {
            break;  // blockFirstRow still points at the beggining of the (ip in) block
         }
         // increase index blockFirstRow to point to the first basis state of
         // the next block or to the end of the basis.
         blockFirstRow += I_basis.NumberOfStatesInBlock(ipin_block);
      }
      // either ip == jp && in == jn or we are at the end, i.e. ipin_block ==
      // number_ipin_blocks && blockFirstRow == dim[bra]
      if (ip == jp && in == jn) {
         //	we found matching pair (jp jn) == (ip in) ==> we will need to
         //	calculate its dimension ==> prepare ap_max & an_max
         uint32_t aip_max = I_basis.getMult_p(ip);  // must be qual to J_basis.getMult_p(jp)
         uint32_t ain_max = I_basis.getMult_n(in);  // must be qual to J_basis.getMult_n(jn)

         uint32_t jbegin = J_basis.blockBegin(jpjn_block);
         uint32_t jend = J_basis.blockEnd(jpjn_block);
         // TODO: utilize that jbegin = jend - 1 ==> exactly one iteraction ==> no loop over jpn
         // needed

         uint32_t ibegin = I_basis.blockBegin(ipin_block);
         uint32_t iend = I_basis.blockEnd(ipin_block);

         bool matching_states_found = false;

         uint32_t ipos_current(blockFirstRow);

         for (uint32_t jwpn = jbegin, iwpn = ibegin; jwpn < jend && iwpn < iend; ++jwpn) {
            SU3xSU2::LABELS omega_pn_J(J_basis.getOmega_pn(jp, jn, jwpn));

            for (; iwpn < iend; ++iwpn, ipos_current += irrep_dim) {
               SU3xSU2::LABELS omega_pn_I(I_basis.getOmega_pn(ip, in, iwpn));
               irrep_dim = aip_max * ain_max * omega_pn_I.rho * I_basis.omega_pn_dim(iwpn);

               matching_states_found = (omega_pn_J == omega_pn_I);
               if (matching_states_found) {
                  break;
               }
            }
            if (matching_states_found) {
               // matching_bra_coeff_begin[i] contains an index of the first
               // coefficient corresponding with (ip in wpnI) == (jp jn wpnJ)
               // irrep in ket space
               matching_bra_coeff_begin.push_back(ipos_current);
            } else {
               std::cout << "We should never end up here!" << std::endl;
               exit(EXIT_FAILURE);
            }
         }
      }
   }

   if (matching_bra_coeff_begin.empty()) {
      std::cout << "U3xSU2 subspace does not belong to wfn model space!" << std::endl;
      return;
   }

   uint32_t imatching_bra_index = 0, icurrent = 0;
   for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
      uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
      uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);

      uint32_t jbegin = J_basis.blockBegin(jpjn_block);
      uint32_t jend = J_basis.blockEnd(jpjn_block);
      for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn) {
         SU3xSU2::LABELS omega_pn(J_basis.getOmega_pn(jp, jn, jwpn));

         uint32_t icurr_bigger = matching_bra_coeff_begin[imatching_bra_index++];

         uint32_t ajp_max = J_basis.getMult_p(jp);
         uint32_t ajn_max = J_basis.getMult_n(jn);
         uint32_t idim = ajp_max * ajn_max * omega_pn.rho * J_basis.omega_pn_dim(jwpn);

         for (int j = 0; j < idim; ++j, ++icurrent) {
            wfn[icurr_bigger + j] = u3su2_wfn[icurrent];
         }
      }
   }
}

int main(int argc, char** argv) {
   if (argc != 5) {
      std::cout << "Usage: " << argv[0]
                << " <model space>  <Sp3R basis file> <Sp3R wfn filename> <SU(3) wfn filename>\n";
      std::cout << "<model space> ... model space made up of all U3xSU2 subspaces that occur in "
                   "Sp(3,R) basis\n";
      std::cout
          << "<Sp3R basis file> \n\tFile that contains (1) number of Sp(3,R) basis states and (2) "
             "files associated with  Sp(3,R) basis states with identical U(3)xSU(2) symmetry. \n"
             "\tStructure of files is described in help message produced by "
             "ComputeHamiltonianMeSp3R_nonDiag_MPI_OpenMP.";
      std::cout << "\n<Sp3R wfn filename> ... wave function to be transformed into SU(3) basis\n ";
      std::cout << "\n<SU3 wfn filename> ... resulting wave function in SU(3) coupled basis\n ";
      return EXIT_FAILURE;
   }

   std::string su3_model_space_filename(argv[1]);
   std::string sp3r_basis_filename(argv[2]);
   std::string sp3r_wfn_filename(argv[3]);
   std::string su3_wfn_filename(argv[4]);

   // file for Sp(3,R) decomposition
   std::string sp3r_decomp_filename("expansion_"+sp3r_wfn_filename);

   // Each segment is defined by
   // Nhw 2Sp 2Sn 2S lm mu i0 dim
   // i0: position of segment 1st state in Sp(3,R) basis
   // dim: number of states in segment
   std::vector<std::array<uint32_t, 8>> u3xsu2_segments;
   // vector of Sp(3,R) basis states filenames
   std::vector<std::string> sp3r_basis_states;

   proton_neutron::ModelSpace su3totalModelSpace;
   su3totalModelSpace.Load(su3_model_space_filename);
   lsu3::CncsmSU3xSU2Basis su3_basis;
   su3_basis.ConstructBasis(su3totalModelSpace, 0, 1);
   JJ0 = su3_basis.JJ();
   nprotons = su3_basis.NProtons();
   nneutrons = su3_basis.NNeutrons();

   std::cout << "Reading Sp(3,R) basis states ...";
   std::cout.flush();
   // Read content of file that describes Sp(3,R) basis in terms of
   // files that describes groups of states with identical U(3)xSU(2) symmetry
   ReadSp3RBasis(sp3r_basis_filename, u3xsu2_segments, sp3r_basis_states);
   std::cout << " Done." << std::endl;
   uint32_t sp3r_dim = sp3r_basis_states.size();
   std::cout << "Number of Sp(3,R) basis states: " << sp3r_dim << std::endl;

   // resulting wave function expressed in U(3)xSU(2) basis
   std::vector<float> su3_wfn(su3_basis.getModelSpaceDim(), 0.0);
   // input wave function expressed in Sp(3,R) basis
   std::vector<float> sp3r_wfn(sp3r_dim, 0.0);

   // read input wave function expressed in Sp(3,R) basis
   std::fstream sp3r_wfn_file(
       sp3r_wfn_filename.c_str(),
       std::ios::in | std::ios::binary |
           std::ios::ate);  // open at the end of file so we can get file size
   if (!sp3r_wfn_file) {
      std::cerr << "Error: could not open '" << sp3r_wfn_filename << "' Sp(3,R) wfn file!"
                << std::endl;
      return EXIT_FAILURE;
   }
   size_t size = sp3r_wfn_file.tellg();
   size_t nelems = size / sizeof(float);

   if (size % sizeof(float) || (nelems != sp3r_dim)) {
      std::cerr << "Error: size of file '" << sp3r_wfn_filename << "' indicates dim:" << nelems;
      std::cerr << " However dim of Sp(3,R) basis as given by file '" << sp3r_basis_filename
                << "' is equal to " << sp3r_dim << "!\n";
      return EXIT_FAILURE;
   }

   // set stream to beginning of file
   sp3r_wfn_file.seekg(0, std::ios::beg);
   // read input wave function in Sp(3,R) basis
   sp3r_wfn_file.read((char*)sp3r_wfn.data(), nelems * sizeof(float));

   std::ofstream decomp_file(sp3r_decomp_filename.c_str());
   if (!decomp_file)
   {
      std::cerr << "Could not open file '" << sp3r_decomp_filename << "' to store Sp(3,R) decomposition" << std::endl;
   }
   for (size_t i = 0; i < sp3r_wfn.size(); ++i)
   {
      decomp_file << sp3r_wfn[i] << " " << sp3r_basis_states[i] << std::endl;
   }

   // check if it is properly normalized ...
   double norm = std::inner_product(sp3r_wfn.begin(), sp3r_wfn.end(), sp3r_wfn.begin(), 0.0);
   std::cout << "Checking if Sp(3,R) wave function is normalized ... ";
   if (fabs(norm - 1.0) > 0.0001) {
      std::cerr << "\nError: Sp(3,R) wave function is not normalized!" << std::endl;
      return EXIT_FAILURE;
   } else {
      std::cout << " OK" << std::endl;
   }

   for (const auto& segment : u3xsu2_segments) {
      uint32_t nhw, ssp, ssn, ss, lm, mu, i0, dim;
      nhw = segment[0];
      ssp = segment[1];
      ssn = segment[2];
      ss = segment[3];
      lm = segment[4];
      mu = segment[5];
      i0 = segment[6];
      dim = segment[7];
      std::cout << "Nhw:" << nhw << " SSp:" << ssp << " SSn:" << ssn << " SS:" << ss << " (" << lm
                << " " << mu << ") i0:" << i0 << " dim:" << dim << std::endl;
      proton_neutron::ModelSpace u3su2ModelSpace(su3_basis.NProtons(), su3_basis.NNeutrons(), nhw,
                                                 ssp, ssn, ss, lm, mu, JJ0);

      // ket: basis of Nhw 2Sp 2Sn 2S (lm mu) equivalent states
      lsu3::CncsmSU3xSU2Basis ket;
      ket.ConstructBasis(u3su2ModelSpace, 0, 1);
      assert(ket.dim() == ket.getModelSpaceDim());
      if (ket.dim() == 0) {
         std::cerr << "Dimension of U3xSU2 subspace with Nhw:" << nhw << " 2Sp:" << ssp
                   << " 2Sn:" << ssn << " 2S:" << ss << " (" << lm << " " << mu
                   << ") is equal to 0!" << std::endl;
         return (EXIT_FAILURE);
      }

      // u3su2_wfn = sum_{i} c_i |Sp(3,R) Nhw 2Sp 2Sn 2S (lm mu) * J0>i
      // where c_i coefficients are from input wave function
      std::vector<double> u3su2_wfn(ket.getModelSpaceDim(), 0);
      // Sp(3,R) state expressed in Nhw 2Sp 2Sn 2S (lm mu) basis
      std::vector<float> sp3r_basis_state_in_u3su2(ket.getModelSpaceDim(), 0);
      // read set of Sp(3,R) basis states spanning Nhw 2Sp 2Sn 2S lm mu U(3)xSU(2) subspace
      // NOTE: Sp(3,R) states have position i0 ... i0 + dim -1
      for (uint32_t i = 0; i < dim; ++i) {
         std::fstream sp3r_basis_file(
             sp3r_basis_states[i0 + i].c_str(),
             std::ios::in | std::ios::binary |
                 std::ios::ate);  // open at the end of file so we can get file size

         if (!sp3r_basis_file) {
            std::cerr << "Could not open file '" << sp3r_basis_states[i0 + i] << "'!\n";
            return EXIT_FAILURE;
         }

         size_t size = sp3r_basis_file.tellg();
         size_t nelems = size / sizeof(float);

         if (size % sizeof(float) || (nelems != ket.getModelSpaceDim())) {
            std::cerr << "Error: size of Sp(3,R) basis file '" << sp3r_basis_states[i0 + i] << "'";
            std::cerr << " indicates that dim[Nhw 2Sp 2Sn 2S lm mu 2J]:" << nelems;
            std::cerr << " However dim of given model space is equal to " << ket.getModelSpaceDim()
                      << "!\n";
            return EXIT_FAILURE;
         }

         sp3r_basis_file.seekg(0, std::ios::beg);
         sp3r_basis_file.read((char*)sp3r_basis_state_in_u3su2.data(), nelems * sizeof(float));
         double coeff = sp3r_wfn[i0 + i];
         /*
                  std::transform(
                      sp3r_basis_state_in_u3su2.begin(), sp3r_basis_state_in_u3su2.end(),
            u3su2_wfn.begin(),
                      u3su2_wfn.begin(),
                      [=](float val1, double val2) -> double { return coeff * (double)val1 + val2;
            });
         */
         for (size_t i = 0; i < sp3r_basis_state_in_u3su2.size(); i++) {
            u3su2_wfn[i] += coeff * (double)sp3r_basis_state_in_u3su2[i];
         }
      }
      // u3su2_wfn is expressed in terms of Nhw 2Sp 2Sn 2S (lm mu) * J basis states
      // store its coefficients in proper positions in basis of su3_basis
      U3SU2wfn2Total(su3_wfn, su3_basis, ket, u3su2_wfn);
      //      norm = std::inner_product(su3_wfn.begin(), su3_wfn.end(), su3_wfn.begin(), 0.0);
      //      std::cout << "Norm: " << norm << std::endl;
   }
   // store su3_wfnao disk

   norm = std::inner_product(su3_wfn.begin(), su3_wfn.end(), su3_wfn.begin(), 0.0);
   std::cout << "Norm check: " << norm << std::endl;
   if (fabs(norm - 1.0) > 0.0001) {
      std::cerr << "Error: resulting SU(3) wave function is not normalized to 1!" << std::endl;
   }
   std::fstream resulting_wfn_file(su3_wfn_filename.c_str(),
                                   std::ios::binary | std::ios::out | std::ios::trunc);
   if (!resulting_wfn_file) {
      std::cerr << "Error: could not open file '" << su3_wfn_filename
                << "' to store resulting wave function in SU(3) coupled basis." << std::endl;
      return EXIT_FAILURE;
   }
   resulting_wfn_file.write((char*)&su3_wfn[0], su3_wfn.size() * sizeof(float));
   std::cout << "Storing resulting SU(3) wave function into '" << su3_wfn_filename << "' file."
             << std::endl;
   return EXIT_SUCCESS;
}
