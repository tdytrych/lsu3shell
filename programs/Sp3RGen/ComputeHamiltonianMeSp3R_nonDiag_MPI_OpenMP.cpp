#include <LSU3/BroadcastDataContainer.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <mpi.h>
#include <omp.h>

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/OperatorLoader.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <su3.h>

#include <boost/mpi.hpp>
//	To be able to load and distribute basis from a binary archive file
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <algorithm>  // std::plus
#include <array>
#include <chrono>
#include <cmath>
#include <ctime>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <vector>

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;
using std::array;

struct U3xU2Params {
   static const int Z   = 0;
   static const int N   = 1;
   static const int Nhw = 2;
   static const int SSp = 3;
   static const int SSn = 4;
   static const int SS  = 5;
   static const int LM  = 6;
   static const int MU  = 7;
   static const int JJ  = 8;
};

// each thread executes this function on the submatrix of an operator
double bra_x_Observable_x_ket_per_thread(
    const lsu3::CncsmSU3xSU2Basis& bra, const vector<float>& bra_wfn, const vector<float>& ket_wfn,
    // data structures describing sparse submatrix on CSR format
    const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs,
    // rows are organized into groups of (ip in) states. For each block we store its size and
    // starting position in basis.
    // ipin_block_ids holds indices of non vanishing row blocks stored in CSR structure
    const vector<uint32_t>& ipin_block_ids) {
   vector<double> observable_x_ket(row_ptrs.size() - 1, 0);

   assert(vals.size() == row_ptrs.back());
   assert(observable_x_ket.size() == row_ptrs.size() - 1);

   for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow) {
      for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival) {
         observable_x_ket[irow] += (double)vals[ival] * (double)ket_wfn[column_indices[ival]];
      }
   }

   double dresult = 0.0;
   int row0;
   int nrows;
   int i = 0;
   for (size_t iblock = 0; iblock < ipin_block_ids.size(); ++iblock) {
      row0 = bra.BlockPositionInSegment(ipin_block_ids[iblock]);
      nrows = bra.NumberOfStatesInBlock(ipin_block_ids[iblock]);
      for (int irow = 0; irow < nrows; ++irow, ++i) {
         dresult += (double)bra_wfn[row0 + irow] * (double)observable_x_ket[i];
      }
   }
   return dresult;
}

bool ReadWfnSection(const string& wfn_file_name, const lsu3::CncsmSU3xSU2Basis& basis,
                    const int ndiag, const int isection, vector<float>& wfn) {
   //	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
   assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

   fstream wfn_file(wfn_file_name.c_str(),
                    std::ios::in | std::ios::binary |
                        std::ios::ate);  // open at the end of file so we can get file size
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
      return false;
   }

   size_t size = wfn_file.tellg();
   size_t nelems = size / sizeof(float);

   if (size % sizeof(float) || (nelems != basis.getModelSpaceDim())) {
      cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x "
           << sizeof(float) << " = " << basis.getModelSpaceDim() * sizeof(float) << " bytes."
           << endl;
      cout << "The actual size of the file: " << size << " bytes!";
      return false;
   }

   wfn.reserve(nelems);
   // full wave function generated for ndiag_wfn=1
   std::vector<float> wfn_full(nelems, 0.0);
   wfn_file.seekg(0, std::ios::beg);
   wfn_file.read((char*)&wfn_full[0], nelems * sizeof(float));

   uint32_t ipos;
   uint16_t number_of_states_in_block;
   uint32_t number_ipin_blocks = basis.NumberOfBlocks();
   //	This loop iterates over blocks that belong to isection of basis split into ndiag segments
   //	I am using the fact that for isection of the basis split into ndiag segment has the
   //	following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
   for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag) {
      //	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
      ipos = basis.BlockPositionInSegment(ipin_block);
      number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
      wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
   }

   //	trim excess memory
   vector<float>(wfn).swap(wfn);

   return true;
}

int ReadWfns(const vector<string> wfns_filenames, const lsu3::CncsmSU3xSU2Basis& full_basis,
             int ndiag, int isegment, vector<vector<float>>& wfns) {
   assert(wfns.size() == wfns_filenames.size());
   for (size_t i = 0; i < wfns_filenames.size(); ++i) {
      // Note that each wave function in wfns_bra contains only (my_row)th section of bra model
      // space that is made out of ndiag_bra sections
      bool ok = ReadWfnSection(wfns_filenames[i], full_basis, ndiag, isegment, wfns[i]);
      if (!ok) {
         return 0;
      }
   }
   return 1;
}

bool LoadInputFile(const string& basis_definition_filename, array<int, 9>& space_params,
                   int32_t& first_state_position, vector<string>& wfn_filenames) {
   ifstream basis_file(basis_definition_filename.c_str());
   if (!basis_file) {
      cout << "Could not open '" << basis_definition_filename << "' input file!" << endl;
      return false;
   }

   //  The order of input parameters in the input file: Z N Nhw 2Sp 2Sn 2S lm mu 2J;
   basis_file >> space_params[U3xU2Params::Z];
   basis_file >> space_params[U3xU2Params::N];
   basis_file >> space_params[U3xU2Params::Nhw];
   basis_file >> space_params[U3xU2Params::SSp];
   basis_file >> space_params[U3xU2Params::SSn];
   basis_file >> space_params[U3xU2Params::SS];
   basis_file >> space_params[U3xU2Params::LM];
   basis_file >> space_params[U3xU2Params::MU];
   basis_file >> space_params[U3xU2Params::JJ];

   if (!basis_file) {
      cout << "Error while reading input parameters of model space from '"
           << basis_definition_filename << "' file!\n";
      cout.flush();
      return false;
   }

   // read position of first Sp(3,R) state in our basis
   basis_file >> first_state_position;

   int nstates;
   basis_file >> nstates;
   if (!basis_file) {
      cout << "Error while reading the number of Sp(3,R) states!\n";
      cout << "basis definition file:'" << basis_definition_filename << "'\n";
      cout.flush();
      return false;
   }

   wfn_filenames.reserve(nstates);
   for (int i = 0; i < nstates; ++i) {
      string name;

      basis_file >> name;
      if (!basis_file) {
         cout << "Error while reading the name of " << i << "th Sp(3,R) state!\n";
         cout << "basis definition file:'" << basis_definition_filename << "'\n";
         cout.flush();
         return false;
      }
      wfn_filenames.push_back(name);
   }
   return true;
}

int ReadInputParamsAndConstructBasis(
    int argc, char** argv, string& hamiltonian_file_name, int& ndiag_bra, int& ndiag_ket,
    proton_neutron::ModelSpace& bra_ncsmModelSpace, proton_neutron::ModelSpace& ket_ncsmModelSpace,
    lsu3::CncsmSU3xSU2Basis& bra_basis, lsu3::CncsmSU3xSU2Basis& ket_basis,
    int32_t& bra_basis_position, int32_t& ket_basis_position, vector<string>& bra_wfn_filenames,
    vector<string>& ket_wfn_filenames, bool& identicalBraKetSpace) {
   const int root = 0;
   int my_rank;

   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

   int allOK = 1;

   if (argc != 6) {
      if (my_rank == 0) {
         cout << "\n";
         cout << "Usage: " << argv[0]
              << " <Hamiltonian> <bra basis file> <ndiag bra> <ket basis file> <ndiag ket>\n\n";
         cout << "Structure of <bra/ket basis file> input file:\n";
         cout << "--------------------------------\n";
         cout << "<Z> <N> <Nhw> <lm> <mu> <SSp> <SSn> <SS> <JJ>\n";
         cout << "<position> position of the first Sp(3,R) state in our basis\n";
         cout << "<number> of Sp3R basis states\n";
         cout << "<Sp3R[0] filename>\n";
         cout << "<Sp3R[1] filename>\n";
         cout << ".\n.\n.\n.\n.";
         cout << "It is assumed that expansion of Sp3R states in terms of U(3) configurations is "
                 "given with ndiag=1 order."
              << endl;
         cout << "This is the case if the Sp3R states are generated by GenerateSp3RkLJStates code."
              << endl;
         cout.flush();
      }
      return EXIT_FAILURE;
   }

   hamiltonian_file_name = argv[1];
   std::string bra_basis_definition(argv[2]);
   ndiag_bra = atoi(argv[3]);
   std::string ket_basis_definition(argv[4]);
   ndiag_ket = atoi(argv[5]);

   int nprocs;
   MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

   if (nprocs != ndiag_bra * ndiag_ket) {
      if (my_rank == root) {
         cout << "Total number of MPI processes must be equal to the product of <ndiag bra> "
                 "(#sections in bra space) and <ndiag_ket> (#sections in ket space)!"
              << endl;
         cout << "(#sections in bra space) = " << ndiag_bra << endl;
         cout << "(#sections in ket space) = " << ndiag_ket << endl;
         cout << "#processes = " << nprocs << endl;
      }
      return EXIT_FAILURE;
   }

   array<int, 9> bra_space_params;
   array<int, 9> ket_space_params;

   //	Root process reads input files describing bra Sp(3,R) basis
   if (my_rank == root) {
      bool success1 = LoadInputFile(bra_basis_definition, bra_space_params, bra_basis_position,
                                    bra_wfn_filenames);
      if (!success1) {
         cout << "Error while reading BRA basis definition file.\n";
         allOK = 0;
      }
   }
   MPI_Bcast(&allOK, 1, MPI_INT, root, MPI_COMM_WORLD);
   if (allOK == 0) {
      return EXIT_FAILURE;
   }

   //	Root process reads input files describing ket Sp(3,R) basis
   if (my_rank == root) {
      bool success2 = LoadInputFile(ket_basis_definition, ket_space_params, ket_basis_position,
                                    ket_wfn_filenames);
      if (!success2) {
         cout << "Error while reading KET basis definition file.\n";
         allOK = 0;
      }
   }
   MPI_Bcast(&allOK, 1, MPI_INT, root, MPI_COMM_WORLD);
   if (allOK == 0) {
      return EXIT_FAILURE;
   }

   if (my_rank == root) {
      if (bra_space_params[U3xU2Params::Z] != ket_space_params[U3xU2Params::Z] ||
          bra_space_params[U3xU2Params::N] != ket_space_params[U3xU2Params::N]) {
         cout << "Mismatch in number of protons [Zbra:" << bra_space_params[U3xU2Params::Z]
              << " Zket:" << ket_space_params[U3xU2Params::Z];
         cout << "] or neutrons [Nbra:" << bra_space_params[U3xU2Params::N]
              << " Nket:" << ket_space_params[U3xU2Params::N] << "]!\n";
         cout.flush();
         allOK = 0;
      }
   }
   MPI_Bcast(&allOK, 1, MPI_INT, root, MPI_COMM_WORLD);
   if (allOK == 0) {
      return EXIT_FAILURE;
   }

   //	Broadcast data that describe bra Sp(3,R) basis
   MPI_Bcast(&bra_space_params[0], bra_space_params.size(), MPI_INT, root, MPI_COMM_WORLD);
   BroadcastDataContainer(bra_wfn_filenames, "filenames of bra wavefunctions");

   //	Broadcast data that describe ket Sp(3,R) basis
   MPI_Bcast(&ket_space_params[0], ket_space_params.size(), MPI_INT, root, MPI_COMM_WORLD);
   BroadcastDataContainer(ket_wfn_filenames, "filenames of ket wavefunctions");

   identicalBraKetSpace = (bra_space_params == ket_space_params);

   int nprotons = bra_space_params[U3xU2Params::Z];
   int nneutrons = bra_space_params[U3xU2Params::N];
   int nhw = bra_space_params[U3xU2Params::Nhw];
   int ssp = bra_space_params[U3xU2Params::SSp];
   int ssn = bra_space_params[U3xU2Params::SSn];
   int ss = bra_space_params[U3xU2Params::SS];
   int lm = bra_space_params[U3xU2Params::LM];
   int mu = bra_space_params[U3xU2Params::MU];
   int jj = bra_space_params[U3xU2Params::JJ];
   bra_ncsmModelSpace =
       proton_neutron::ModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, jj);

   nprotons = ket_space_params[U3xU2Params::Z];
   nneutrons = ket_space_params[U3xU2Params::N];
   nhw = ket_space_params[U3xU2Params::Nhw];
   ssp = ket_space_params[U3xU2Params::SSp];
   ssn = ket_space_params[U3xU2Params::SSn];
   ss = ket_space_params[U3xU2Params::SS];
   lm = ket_space_params[U3xU2Params::LM];
   mu = ket_space_params[U3xU2Params::MU];
   jj = ket_space_params[U3xU2Params::JJ];
   ket_ncsmModelSpace =
       proton_neutron::ModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, jj);

   if (bra_space_params[U3xU2Params::JJ] != ket_space_params[U3xU2Params::JJ]) {
      if (my_rank == root) {
         cout << "Error bra JJ:" << bra_space_params[U3xU2Params::JJ]
              << " is not equal to ket JJ:" << ket_space_params[U3xU2Params::JJ] << "\n";
      }
      return EXIT_FAILURE;
   }

   uint16_t my_row, my_col;
   my_row = my_rank / ndiag_ket;
   my_col = my_rank % ndiag_ket;

   MPI_Comm ROW_COMM, COL_COMM;
   MPI_Comm_split(MPI_COMM_WORLD, my_row, my_col,
                  &ROW_COMM);  // processes in row_comm share my_row value
   MPI_Comm_split(MPI_COMM_WORLD, my_col, my_row,
                  &COL_COMM);  // processes in col_comm share my_col value

   //	bra_basis: my_row^th segment of U(3)xSU(2) basis for BRA space
   bra_basis.ConstructBasis(bra_ncsmModelSpace, my_row, ndiag_bra, COL_COMM);
   //	ket_basis: my_col^th segment of U(3)xSU(2) basis for KET space
   ket_basis.ConstructBasis(ket_ncsmModelSpace, my_col, ndiag_ket, ROW_COMM);

   return EXIT_SUCCESS;
}

void ReadInputWfns(int ndiag_bra, int ndiag_ket,
                   const proton_neutron::ModelSpace& bra_ncsmModelSpace,
                   const proton_neutron::ModelSpace& ket_ncsmModelSpace,
                   const lsu3::CncsmSU3xSU2Basis& bra_basis,
                   const lsu3::CncsmSU3xSU2Basis& ket_basis,
                   const vector<string>& bra_wfn_filenames, const vector<string>& ket_wfn_filenames,
                   vector<vector<float>>& wfns_bra, vector<vector<float>>& wfns_ket) {
   const int root = 0;
   int my_rank;

   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

   //  Note that each wave function in wfns_bra contains only (my_row)th section of bra model space
   //  that is made out of ndiag_bra sections
   //	E.G.: <2>10 20Ne
   //	bra: J=8 ===> single wfn ... 194MB
   //	ket: J=4 ===> single wfn ... 160MB
   //	ndiag_bra=ndiag_ket=150
   //	hw=25, 20, 15 ===> size: 3 x (194/150 + 160/150) MB = 7.08 MB
   //	hw=10, 12.5, 15, 17.5, 20, 22.5, 25 ====> size: 7 x (194 + 160) = 16.52 MB

   int my_row = my_rank / ndiag_ket;
   int my_col = my_rank % ndiag_ket;

   MPI_Comm ROW_COMM, COL_COMM;
   MPI_Comm_split(MPI_COMM_WORLD, my_row, my_col,
                  &ROW_COMM);  // processes in row_comm share my_row value
   MPI_Comm_split(MPI_COMM_WORLD, my_col, my_row,
                  &COL_COMM);  // processes in col_comm share my_col value

   int my_row_rank, my_col_rank;

   MPI_Comm_rank(ROW_COMM, &my_row_rank);
   MPI_Comm_rank(COL_COMM, &my_col_rank);

   if (my_rank == root) {
      cout << "Creating basis of bra and ket model spaces with a single segment to be able to read "
              "input files ...\n";
      cout.flush();
   }

   std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
   {
      if (my_row_rank == 0) {
         lsu3::CncsmSU3xSU2Basis full_basis;
         // ReadWfnSection assumes full_basis will describe the entire basis
         // WARNING: !!! This can be a huge data structure !!!!
         // For instance <2>10 basis of 20-Ne takes 6.6 GB of disk space !!
         full_basis.ConstructBasis(bra_ncsmModelSpace, bra_basis, 0, 1);
         bool ok = ReadWfns(bra_wfn_filenames, full_basis, ndiag_bra, my_row, wfns_bra);
         if (!ok) {
            std::cout << "Error while reading bra wfns from files" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
         }
      }
   }

   {
      if (my_col_rank == 0) {
         lsu3::CncsmSU3xSU2Basis full_basis;
         // ReadWfnSection assumes full_basis will describe the entire basis
         // WARNING: !!! This can be a huge data structure !!!!
         // For instance <2>10 basis of 20-Ne takes 6.6 GB of disk space !!
         full_basis.ConstructBasis(ket_ncsmModelSpace, ket_basis, 0, 1);
         bool ok = ReadWfns(ket_wfn_filenames, full_basis, ndiag_ket, my_col, wfns_ket);
         if (!ok) {
            std::cout << "Error while reading ket wfns from files" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
         }
      }
   }
   std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
   if (my_rank == 0) {
      cout << "done in " << duration.count() << endl;
   }

   for (size_t i = 0; i < wfns_bra.size(); ++i)
   {
      int size = wfns_bra[i].size();
      MPI_Bcast((void*)&size, 1, MPI_INT, 0, ROW_COMM); 
      if (my_row_rank != 0)
      {
         wfns_bra[i].resize(size);
      }
      MPI_Bcast((void*)wfns_bra[i].data(), size, MPI_FLOAT, 0, ROW_COMM); 
   }

   for (size_t i = 0; i < wfns_ket.size(); ++i)
   {
      int size = wfns_ket[i].size();
      MPI_Bcast((void*)&size, 1, MPI_INT, 0, COL_COMM);

      if (my_col_rank != 0)
      {
         wfns_ket[i].resize(size);
      }
      MPI_Bcast((void*)wfns_ket[i].data(), size, MPI_FLOAT, 0, COL_COMM);
   }
}

void ShowMatrix(const vector<float>& vals, const vector<size_t>& column_indices,
                const vector<size_t>& row_ptrs, ofstream& file) {
   for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow) {
      for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival) {
         file << irow + 1 << " " << column_indices[ival] + 1 << " " << vals[ival] << endl;
      }
   }
}

void ComputeSp3RMatrixElements(bool identicalBraKetSpace, const lsu3::CncsmSU3xSU2Basis& bra_basis,
                               const vector<vector<float>>& wfns_bra,
                               const vector<vector<float>>& wfns_ket, const vector<float>& vals,
                               const vector<size_t>& column_indices, const vector<size_t>& row_ptrs,
                               const vector<uint32_t>& ipin_block_ids,
                               vector<float>& results_mpi_process) {
   int nrows = wfns_bra.size();
   int ncols = wfns_ket.size();
   if (identicalBraKetSpace) {  // compute upper triangular matrix
      for (int i = 0; i < nrows; ++i) {
         for (int j = i; j < ncols; ++j) {
            // each thread computes value of <bra | Operator_thread | ket>
            float result_thread =
                bra_x_Observable_x_ket_per_thread(bra_basis, wfns_bra[i], wfns_ket[j], vals,
                                                  column_indices, row_ptrs, ipin_block_ids);
// add contribution due to thread into <bra | Operator_mpi_process | ket> for a given MPI process
#pragma omp atomic
            results_mpi_process[i * ncols + j] += result_thread;
         }
      }
   } else {
      for (int i = 0; i < nrows; ++i) {
         for (int j = 0; j < ncols; ++j) {
            // each thread computes value of <bra | Operator_thread | ket>
            float result_thread =
                bra_x_Observable_x_ket_per_thread(bra_basis, wfns_bra[i], wfns_ket[j], vals,
                                                  column_indices, row_ptrs, ipin_block_ids);
// add contribution due to thread into <bra | Operator_mpi_process | ket> for a given MPI process
#pragma omp atomic
            results_mpi_process[i * ncols + j] += result_thread;
         }
      }
   }
}

void StoreResults(const string& filename, const vector<float>& vals, int32_t bra_basis_position, int32_t ket_basis_position, int32_t nrows, int32_t ncols) {
   vector<int32_t> nnz_indices;
   vector<int32_t> rows;
   vector<int32_t> cols;

   int32_t index = 0;
   for (int32_t i = 0; i < nrows; i++) {
      for (int32_t j = 0; j < ncols; j++, index++) {
         if (!Negligible6(vals[index]))
         {
            nnz_indices.push_back(index);
            rows.push_back(bra_basis_position + i);
            cols.push_back(ket_basis_position + j);
         }
      }
   }

   int64_t nnz = nnz_indices.size();
   if (nnz == 0)
   {
      return; // => no output file created
   }

   fstream output_file(filename, std::ios::out | std::ios::binary);
   output_file.write((char*)&nnz, sizeof(int64_t));
   output_file.write((char*)&rows[0], sizeof(int32_t) * rows.size());
   output_file.write((char*)&cols[0], sizeof(int32_t) * cols.size());
   for (auto i : nnz_indices) {
      output_file.write((char*)&vals[i], sizeof(float));
   }
}

int main(int argc, char** argv) {
   boost::mpi::environment env(argc, argv);

   int my_rank;
   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

   int ndiag_bra;
   proton_neutron::ModelSpace bra_ncsmModelSpace;
   lsu3::CncsmSU3xSU2Basis bra_basis;
   int ndiag_ket;
   proton_neutron::ModelSpace ket_ncsmModelSpace;
   lsu3::CncsmSU3xSU2Basis ket_basis;

   bool identicalBraKetSpace;

   string hamiltonian_file_name;

   int32_t bra_basis_position, ket_basis_position;
   vector<string> bra_wfn_filenames;
   vector<string> ket_wfn_filenames;

   if (ReadInputParamsAndConstructBasis(
           argc, argv, hamiltonian_file_name, ndiag_bra, ndiag_ket, bra_ncsmModelSpace,
           ket_ncsmModelSpace, bra_basis, ket_basis, bra_basis_position, ket_basis_position,
           bra_wfn_filenames, ket_wfn_filenames, identicalBraKetSpace) == EXIT_FAILURE) {
      MPI_Finalize();
      return EXIT_FAILURE;
   }

   vector<vector<float>> wfns_bra(bra_wfn_filenames.size());
   vector<vector<float>> wfns_ket(ket_wfn_filenames.size());

   ReadInputWfns(ndiag_bra, ndiag_ket, bra_ncsmModelSpace, ket_ncsmModelSpace, bra_basis, ket_basis,
                 bra_wfn_filenames, ket_wfn_filenames, wfns_bra, wfns_ket);

   int nrows = wfns_bra.size();
   int ncols = wfns_ket.size();

   //	since root process will read rme files, it is save to let this
   //	process to create missing rme files (for PPNN interaction) if they do not exist
   //	=> true
   std::ofstream output_file("/dev/null");
   bool log_is_on = false;
   bool generate_missing_rme = true;

   CBaseSU3Irreps baseSU3Irreps(bra_basis.NProtons(), bra_basis.NNeutrons(),
                                std::max(bra_basis.Nmax(), ket_basis.Nmax()));
   CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, output_file);
   CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on, output_file);
   su3::init();
   try {
      CRunParameters run_params;
      int A = bra_basis.NProtons() + bra_basis.NNeutrons();
      run_params.LoadHamiltonian(my_rank, hamiltonian_file_name, interactionPPNN, interactionPN, A);
   } catch (const std::logic_error& e) {
      std::cout << e.what() << std::endl;
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
   }
   su3::finalize();

   //	The order of coefficients is given as follows:
   // 	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
   //	TransformTensorStrengthsIntoPP_NN_structure turns that into:
   // 	index = type*k0max*rho0max + k0*rho0max + rho0
   interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

   if (my_rank == 0) {
      // Check minimal and maximal rme values. If they are too large then
      // do not proceed.
      float min, max;
      interactionPPNN.min_max_values(min, max);
      std::cout << "minimal rme value read from tables: " << min << std::endl;
      std::cout << "maximal rme value read from tables: " << max << std::endl;
      if (fabs(min) > 100 || fabs(max) > 100) {
         std::cerr << "Error: magnitude of minimal/maximal rme values are suspitious!" << std::endl;
         std::cerr << "Data stored in rme tables may be corrupted." << std::endl;
         MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      }
   }

   vector<float> results_mpi_process(nrows * ncols, 0);
   vector<float> results_total(nrows * ncols, 0);
#pragma omp parallel
   {
      su3::init_thread();
      std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
      vector<float> vals;
      vector<size_t> column_indices;
      vector<size_t> row_ptrs;
      // CSR data structure stored in vals, column_indices, row_ptrs describes a submatrix defined
      // by set of (wp x wn) bra basis configurations
      // identification number of wp x wn block of states will be stored in ipin_block_ids
      vector<uint32_t> ipin_block_ids;

      row_ptrs.reserve(bra_basis.dim() + 1);

      CalculateME_Scalar_FullMatrix_OpenMP(interactionPPNN, interactionPN, bra_basis, ket_basis,
                                           vals, column_indices, row_ptrs, ipin_block_ids);

      row_ptrs.push_back(vals.size());
#pragma omp barrier
      su3::finalize_thread();
      std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
      if (0 == omp_get_thread_num()) {
         cout << "Process: " << my_rank
              << ". Time to compute matrix elements of Hamiltonian in U(3) basis: " << duration.count()
              << endl;
      }

      //	In case we need to have matrix in Matrix Market format
      //		if (my_rank == 0)
      //		{
      //			ofstream log_file("vbc_full_matrix.mtx");
      //			log_file << std::fixed;
      //			log_file << std::setprecision(8);
      //			ShowMatrix(vals, column_indices, row_ptrs, log_file);
      //		}

      start = std::chrono::system_clock::now();
      ComputeSp3RMatrixElements(identicalBraKetSpace, bra_basis, wfns_bra, wfns_ket, vals,
                                column_indices, row_ptrs, ipin_block_ids, results_mpi_process);
#pragma omp barrier
      duration = std::chrono::system_clock::now() - start;
      if (0 == omp_get_thread_num()) {
         cout << "Process: " << my_rank
              << ". Time to compute matrix elements of Hamiltonian in Sp(3,R) basis: " << duration.count()
              << endl;
      }
   }  // END OF OPENMP
   MPI_Reduce(&results_mpi_process[0], &results_total[0], results_total.size(), MPI_FLOAT, MPI_SUM,
              0, MPI_COMM_WORLD);

#define SHOW_RESULTING_MATRIX
#ifdef SHOW_RESULTING_MATRIX
   if (my_rank == 0) {
      std::string filename(argv[2]);
      filename += "_";
      filename += argv[4];
      filename += ".ijme";
      std::ofstream matrix_file(filename);

      if (identicalBraKetSpace) {
         for (int i = 0; i < nrows; ++i) {
            for (int j = i; j < ncols; ++j)  // IMPLICIT BEHAVIOUR: show upper triangular matrix
            {
//               cout << i + bra_basis_position << " " << j + ket_basis_position << " " << results_total[i * ncols + j] << endl;
               matrix_file << i + bra_basis_position << " " << j + ket_basis_position << " " << results_total[i * ncols + j] << std::endl;
            }
         }
      } else {
         for (int i = 0; i < nrows; ++i) {
            for (int j = 0; j < ncols; ++j)  // IMPLICIT BEHAVIOUR: show upper triangular matrix
            {
//               cout << i + bra_basis_position << " " << j + ket_basis_position << " " << results_total[i * ncols + j] << endl;
               matrix_file << i + bra_basis_position << " " << j + ket_basis_position << " " << results_total[i * ncols + j] << std::endl;
            }
         }
      }
   }
#endif
   if (my_rank == 0) {
      std::string filename(argv[2]);
      filename += "_";
      filename += argv[4];
      filename += ".dat";
      StoreResults(filename, results_total, bra_basis_position, ket_basis_position, nrows, ncols);
      std::cout << "SUCCESS" << std::endl;
   }
   MPI_Finalize();
   return EXIT_SUCCESS;
}
