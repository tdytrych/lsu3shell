#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <su3.h>

#include <cassert>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <iomanip>  // std::setprecision

#include "slepceps.h"

#include <boost/mpi.hpp>

//	Sp(3,R) states are k L JJ independent. We therefore should compute just one Sp(3,R) state per each
//	Sp(3,R) irrep and get all k L JJ basis states via GenerateSp3RkLJ code.
//	For this reason it is not recommended to produce J-dependent states via uncommenting following #ifdef
//	#define useJJ

using namespace std;

static char help[] = "###### MPI version of the Sp(3,R) states generator ######\n\n" "Command line options that must be provided:\n"
" -Z <Z> ... number of protons \n"
" -N <N> ... number of neutrons \n"
" -Nhw <Nhw> ... HO excitations \n"
" -lm <lm> ... SU(3) lambda \n"
" -mu <mu> ... SU(3) mu \n"
" -SSp <SSp> ... twice proton spin \n"
" -SSn <SSn> ... twice neutron spin \n"
" -SS <SS> ... twice total spin \n"
" optional parameters: \n"
#ifdef useJJ
" -JJ <JJ> ... twice total angular momentum. If not provided its value is set to maximal JJ whic is equal to 2*(lm + mu) + SS \n"
#endif
" -lambda <int>  ... coefficient in front of the Lawson term Ncm\n\n"
" optional parameters: \n"
"-eps_nev <value> ... number of eigenvectors \n\n";

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col)
{
	int executing_process_id(0);
	for (size_t i = 0; i < ndiag; ++i)
	{
		row = 0;
		for (col = i; col < ndiag; ++col, ++row, ++executing_process_id)
		{
			if (my_rank == executing_process_id)
			{
				return;
			}
		}
	}
}

#ifdef useJJ
bool ObtainInputParameters(PetscInt &nprotons, PetscInt &nneutrons, PetscInt &nhw, PetscInt &ssp, PetscInt &ssn, PetscInt &ss, PetscInt &lm, PetscInt &mu, PetscInt &JJ, PetscInt &lambda)
#else	
bool ObtainInputParameters(PetscInt &nprotons, PetscInt &nneutrons, PetscInt &nhw, PetscInt &ssp, PetscInt &ssn, PetscInt &ss, PetscInt &lm, PetscInt &mu, PetscInt &lambda)
#endif	
{
	enum Order {Z, N, Nhw, LM, MU, SSp, SSn, SS};

	string errorMessages[8];
	errorMessages[Z] = "Error: Number of protons -Z <Z> is missing in input arguments.";
	errorMessages[N] = "Error: Number of neutrons -N <N> is missing in input arguments.";
	errorMessages[Nhw] = "Error: Number of HO excitations quanta -Nhw <Nhw> of Sp(3,R) states is missing in input arguments.";
	errorMessages[LM] = "Error: Missing SU(3) quantum number lambda -lm <lm> in input arguments.";
	errorMessages[MU] = "Error: Missing SU(3) quantum number mu -mu <mu> in input arguments.";
	errorMessages[SSp] = "Error: Missing intrinsic proton spin quantum number -SSp <2*Sp> in input arguments.";
	errorMessages[SSn] = "Error: Missing instrinsic neutron spin quantum numbers -SSn <2*Sn> in input arguments.";
	errorMessages[SS] = "Error: Missing 2S quantum number -SS <2*S> in input arguments.";

//	Obtain mandatory input parameters
	PetscBool found[8];
	PetscErrorCode ierr;

	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-Z", &nprotons, &found[Z]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-N", &nneutrons, &found[N]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-Nhw", &nhw, &found[Nhw]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-lm", &lm, &found[LM]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-mu", &mu, &found[MU]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-SSp", &ssp, &found[SSp]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-SSn", &ssn, &found[SSn]); CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-SS", &ss, &found[SS]); CHKERRQ(ierr);

	for (int i = 0; i < 8; ++i)
	{
		if (!found[i])
		{
			cerr << errorMessages[i] << endl;
		}
	}

	if (!found[0] || !found[1] || !found[2] || !found[3] || !found[4] || !found[5] || !found[6] || !found[7])
	{
		return false;
	}

//	Now check the optional input parameters
	PetscBool foundOptional;
#ifdef useJJ	
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-JJ", &JJ, &foundOptional); CHKERRQ(ierr);
	if (!foundOptional)
	{
		JJ = -1;
	}
#endif	
	ierr = PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-lambda", &lambda, &foundOptional); CHKERRQ(ierr);
	if (!foundOptional)
	{
		lambda = 5000;
	}

	return true;
}

//	For the time being let's assume basis construction from the scratch is fast enough.
void ConstructBasis(int ndiag, int idiag, int jdiag, const proton_neutron::ModelSpace &ncsmModelSpace, lsu3::CncsmSU3xSU2Basis& bra, lsu3::CncsmSU3xSU2Basis& ket)
{
	std::chrono::system_clock::time_point start  = std::chrono::system_clock::now();

	ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
	bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);	

	if (idiag == 0 && jdiag == 0)
	{
		std::chrono::duration<double> duration;
		duration = std::chrono::system_clock::now() - start;
		cout << "Time to construct basis: ... " << duration.count() << endl;
	}
}

// sorting algorithms: 3-pointer version for sparse coordinate storage scheme
template <typename T, typename U, typename Comp>
void insert_sort_ptr_3(T* rows, T* cols, U* res, unsigned length, Comp cmp)
{
    unsigned min = 0;
    for (unsigned i = 1; i < length; ++i)
        if (cmp(rows[i], cols[i], rows[min], cols[min])) 
            min = i;

    std::swap(rows[0], rows[min]);
    std::swap(cols[0], cols[min]);
    std::swap(res [0], res [min]);

    unsigned first = 0;
    while (++first < length)
        for (unsigned j = first; cmp(rows[j], cols[j], rows[j - 1], cols[j - 1]); --j) {
            std::swap(rows[j - 1], rows[j]);
            std::swap(cols[j - 1], cols[j]);
            std::swap(res [j - 1], res [j]);
        }
}

template <typename T, typename U, typename Comp>
void hybrid_sort_ptr_3(T* rows, T* cols, U* res, unsigned length, Comp cmp)
{
    if (length > 1) {
        unsigned left = 0;
        unsigned right = length - 1;
        unsigned pivot = left++;

        if (length <= 16) {
            insert_sort_ptr_3(rows, cols, res, length, cmp);
            return;
        }

        unsigned middle = length >> 1;
        std::swap(rows[pivot], rows[middle]);
        std::swap(cols[pivot], cols[middle]);
        std::swap(res [pivot], res [middle]);

        while (left != right) {
            if (cmp(rows[left], cols[left], rows[pivot], cols[pivot])) {
                ++left;
            }
            else {
                while ((left != right) && cmp(rows[pivot], cols[pivot], rows[right], cols[right]))
                    --right;
                std::swap(rows[left], rows[right]);
                std::swap(cols[left], cols[right]);
                std::swap(res [left], res [right]);
            }
        }

        if (left > 0)
            --left;

        std::swap(rows[pivot], rows[left]);
        std::swap(cols[pivot], cols[left]);
        std::swap(res [pivot], res [left]);

        hybrid_sort_ptr_3(&rows[0],     &cols[0],     &res[0],     left + 1,          cmp);
        hybrid_sort_ptr_3(&rows[right], &cols[right], &res[right], length - left - 1, cmp);
    }
}

// comparator for lexicographical ordering
template <typename U>
struct comparator_rowcol_ptr : public std::function<bool(U, U, U, U)>
{
    bool operator()(U row1, U col1, U row2, U col2)
    {
        if (row1 < row2)
            return true;
        else if (row1 == row2)
            return (col1 < col2);
        else
            return false;
    }
}; 

struct custom_monitor_data
{
    int rank;
    uintmax_t iter;
    uintmax_t nev;
	std::chrono::system_clock::time_point start;
    double max, min, sum;
};

// custom eignesolver monitor funciton
PetscErrorCode custom_monitor(EPS, PetscInt iter, PetscInt nconv,
        PetscScalar*, PetscScalar*, PetscReal* errest, PetscInt nest, void* data)
{
    custom_monitor_data* monitor_data = (custom_monitor_data*)data;

    if (iter == 0) {
        monitor_data->iter = 0;
        monitor_data->sum = 0.0;
    }
    else {
        std::chrono::duration<double> duration = std::chrono::system_clock::now() - monitor_data->start;
        monitor_data->sum += duration.count();

        if (iter == 1) {
            monitor_data->min = duration.count();
            monitor_data->max = duration.count();
        }
        else {
            if (duration.count() < monitor_data->min)
                monitor_data->min = duration.count();
            if (duration.count() > monitor_data->max)
                monitor_data->max = duration.count();
        }

        uintmax_t temp = (monitor_data->nev > nest) ? nest : monitor_data->nev;

        if (monitor_data->rank == 0) 
            std::cout << "Iter: " << std::setw(4) << iter // monitor_data->iter
                << ", conv. pairs: " << std::setw(4) << nconv 
                << ", pair(0) error est:  " << std::scientific << errest[0]
                << ", pair(" << (temp - 1) << ") error est:  " << std::scientific << errest[temp - 1]
                << ", time: " << std::fixed << std::setw(10) << std::setprecision(6) << duration.count() << " [s]"
                << std::endl;
    }
    
    monitor_data->iter++;
    monitor_data->start = std::chrono::system_clock::now();
    
    return 0;
}

int main(int argc, char* argv[]) 
{
	int my_rank, nprocs, idiag, jdiag;

	boost::mpi::environment env(argc, argv);
 	boost::mpi::communicator mpi_comm_world;

//	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    PetscErrorCode ierr;

	SlepcInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

//	su3shell_data_directory ... global variable that defines directory with operators/interactions in SU(3) tensor form
	if (su3shell_data_directory == NULL)
	{
		if (my_rank == 0)
		{
			cerr << "System variable 'SU3SHELL_DATA' was not defined!" << endl;
		} 
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	PetscBool found;
	PetscInt nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, lambda;
#ifdef useJJ	
	PetscInt JJ;
#endif	
//	Obtain input parameters that define U(3) space where Sp(3,R) states will be expanded
#ifdef useJJ
	if (!ObtainInputParameters(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, JJ, lambda))
#else		
	if (!ObtainInputParameters(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, lambda))
#endif		
	{
		if (my_rank == 0)
		{
			std::cerr << help << endl;
		}
		ierr = SlepcFinalize(); CHKERRQ(ierr);
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	stringstream ss_base_file_name;
#ifdef useJJ	
	ss_base_file_name << "Z" << nprotons << "_N" << nneutrons << "_" << nhw << "hw_SSp" << ssp << "_SSn" << ssn << "_SS" << ss << "_lm" << lm << "_mu" << mu << "_JJ" << JJ;
#else
	ss_base_file_name << "Z" << nprotons << "_N" << nneutrons << "_" << nhw << "hw_SSp" << ssp << "_SSn" << ssn << "_SS" << ss << "_lm" << lm << "_mu" << mu;
#endif	

	int ndiag = (-1 + sqrt(1 + 8*nprocs))/2; 
	if (my_rank == 0)
	{
		ofstream ndiag_file("ndiag");
		ndiag_file << ndiag;
	}

	if (fabs(ndiag - (double)(-1.0 + sqrt(1 + 8*nprocs))/2.0) > 1.0e-7) 
	{ 
		if (my_rank == 0) 
		{ 
			cerr << "number of processes = " << nprocs << " is wrong: must be equal to ndiag*(ndiag+1)/2, where ndiag is the number of diagonal processes." << endl; 
		} 
  		mpi_comm_world.abort(EXIT_FAILURE);
	} 

	MapRankToColRow_MFDn_Compatible(ndiag, my_rank, idiag, jdiag);

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

//	Load the definition of [A x B]^(0 0) operator
	CBaseSU3Irreps baseSU3Irreps(nprotons, nneutrons, nhw); 
	ofstream interaction_log_file("/dev/null");
	bool log_is_on = false;
	bool generate_missing_rme = false;	

	CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);
	CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on, interaction_log_file);
	COperatorLoader operatorLoader;

   su3::init();
	try 
	{
		operatorLoader.AddAB00(-1.0);
		operatorLoader.AddNcm(nprotons + nneutrons, lambda);
		operatorLoader.Load(my_rank, interactionPPNN, interactionPN);
	}
	catch (const std::exception& e) 
	{
        	cerr << e.what() << std::endl;
		ierr = SlepcFinalize(); CHKERRQ(ierr);
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		return EXIT_FAILURE;
	}

//	While in other LSU3shell codes we launch
//	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure(), we don't
//	have to do it as in case of [AxB]^(00 and Ncm^(0 0) as both are SU(3)
//	scalar (0 0) tensors and as a result we are always guaranteed to have k0max	== rho0max == 1

//	Construct bra and ket basis 
	lsu3::CncsmSU3xSU2Basis bra_basis, ket_basis;
#ifdef useJJ	
	proton_neutron::ModelSpace ncsmModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, JJ);
#else	
	proton_neutron::ModelSpace ncsmModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu, -1);
#endif	
	ConstructBasis(ndiag, idiag, jdiag, ncsmModelSpace, bra_basis, ket_basis);

	unsigned long firstStateId_bra = bra_basis.getFirstStateId(); 
	unsigned long firstStateId_ket = ket_basis.getFirstStateId();

	uint64_t dim = bra_basis.getModelSpaceDim();
	if (dim == 0)
	{
		if (my_rank == 0)
		{
			cerr << "Given set of input parameters produce model space with dim = 0" << endl;
		}
		return EXIT_FAILURE;
	}

	if (my_rank == 0)
	{
		std::cout << "Calculating <" << nhw << "hw(" << lm << " " << mu << ")Sp:" << ssp << " Sn:" << ssn << " S:" << ss << "|| (-1).[AxB]^(00) + (" << lambda << ").Ncm ||";
		std::cout << nhw << "hw(" << lm << " " << mu << ")Sp:" << ssp << " Sn:" << ssn << " S:" << ss << ">" << std::endl;
		std::cout << "size: " << dim << " x " << dim << std::endl;
		std::cout.flush();
	}

//	Calculate matrix elements
	std::vector<float> vals; 
	std::vector<size_t> column_indices; 
	std::vector<size_t> row_ptr;
	row_ptr.push_back(0);

	cout << "Process " << my_rank << " starts calculation of me" << endl;
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

	size_t local_nnz = CalculateME_Scalar_UpperTriang(interactionPPNN, interactionPN, bra_basis, ket_basis, firstStateId_bra, idiag, firstStateId_ket, jdiag, vals, column_indices, row_ptr);

	std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
	cout << "Resulting time: " << duration.count() << endl;

	size_t total_nnz;
	size_t num_frequent_u9, frequent_u9_size, max_num_frequent_u9;
	size_t num_u9, u9_size, max_num_u9; 
	size_t num_u6, u6_size, max_num_u6;
	size_t num_z6, z6_size, max_num_z6;
	CWig9lmLookUpTable<RME::DOUBLE>::coeffs_info(num_frequent_u9, num_u9, num_u6, num_z6);

	boost::mpi::reduce(mpi_comm_world, local_nnz, total_nnz, std::plus<uintmax_t>(), 0);
	boost::mpi::reduce(mpi_comm_world, num_frequent_u9, max_num_frequent_u9, boost::mpi::maximum<size_t>(), 0);
    	boost::mpi::reduce(mpi_comm_world, num_u9, max_num_u9, boost::mpi::maximum<size_t>(), 0);
    	boost::mpi::reduce(mpi_comm_world, num_u6, max_num_u6, boost::mpi::maximum<size_t>(), 0);
    	boost::mpi::reduce(mpi_comm_world, num_z6, max_num_z6, boost::mpi::maximum<size_t>(), 0);
/*
	MPI_Reduce(&local_nnz, &total_nnz, 1, MPI_UNSIGNED, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&num_frequent_u9, &max_num_frequent_u9, 1, MPI_UNSIGNED, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&num_u9, &max_num_u9, 1, MPI_UNSIGNED, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&num_u6, &max_num_u6, 1, MPI_UNSIGNED, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&num_z6, &max_num_z6, 1, MPI_UNSIGNED, MPI_MAX, 0, MPI_COMM_WORLD);
*/
	CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);

	size_t memory_usage_coeffs = frequent_u9_size + u9_size + u6_size + z6_size;
	size_t max_memory_usage_coeffs;
	boost::mpi::reduce(mpi_comm_world, memory_usage_coeffs, max_memory_usage_coeffs, boost::mpi::maximum<size_t>(), 0);
//	MPI_Reduce(&memory_usage_coeffs, &max_memory_usage_coeffs, 1, MPI_UNSIGNED, MPI_MAX, 0, MPI_COMM_WORLD);

	if (my_rank == 0) 
	{
		size_t total_csr_size = total_nnz*sizeof(float) + total_nnz*sizeof(int) + dim*sizeof(int);
       		std::cout << "Total size of Hamiltonian matrix in CRS format: " << total_csr_size/(1024.0*1024.0) << " MB." << std::endl;
		std::cout << "Number of non vanishing matrix elements:" << total_nnz << std::endl;
		std::cout << "\n\nmax number of u9: " << max_num_u9 << std::endl;
		std::cout << "max number of u6: " << max_num_u6 << std::endl;
		std::cout << "max number of z6: " << max_num_z6 << std::endl;
		std::cout << "max number of frequent u9: " << max_num_frequent_u9 << std::endl;
		std::cout << "max amount of memory for SU(3) coeffs: " << max_memory_usage_coeffs/(1024.0*1024.0) << " MB.\n\n";
	}

   su3::finalize();
	CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory(); // clears memory allocated for U9, U6, and Z6 coefficients
	CSSTensorRMELookUpTablesContainer::ReleaseMemory(); // clear memory allocated for single-shell SU(3) rmes
	CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory(); // clear memory allocated for SU(3)>SO(3) coefficients

// calculate balanced row-wise matrix partitioning
    MPI_Barrier(MPI_COMM_WORLD);

	start = std::chrono::system_clock::now();

    // global matrix dimension
    uintmax_t m = dim;
    uintmax_t n = dim;
    // local matrix dimension
    uintmax_t m_local = bra_basis.dim();
    uintmax_t n_local = ket_basis.dim();
    // local matrix offset
    uintmax_t m_offset = firstStateId_bra;
    uintmax_t n_offset = firstStateId_ket;

    // aliases
    auto& col_inds = column_indices;

    auto& row_ptrs = row_ptr;
    // check sizes
    if ((local_nnz != vals.size()) || (local_nnz != col_inds.size()))
        throw std::runtime_error("Size mismatch between local_nnz and vals[] or col_inds[].");

    auto rank = my_rank;
    auto P = nprocs;
 // uintmax_t nnz = total_nnz; // valid only for p_0 !!!
    size_t nnz;
    MPI_Allreduce(&local_nnz, &nnz, 1, boost::mpi::get_mpi_datatype<size_t>(), MPI_SUM, MPI_COMM_WORLD);

    // number of local nonzero elements for all rows
    std::vector<uintmax_t> local_rows_nnz(m, 0);
    for (uintmax_t local_row = 0; local_row < m_local; local_row++) 
        local_rows_nnz[m_offset + local_row] = row_ptrs[local_row + 1] - row_ptrs[local_row];

    // number of global nonzero elements for all rows
    std::vector<uintmax_t> rows_nnz(m, 0);
 // boost::mpi::all_reduce(mpi_comm_world, &(local_rows_nnz[0]), m, &(rows_nnz[0]), std::plus<uintmax_t>());
    MPI_Allreduce(&(local_rows_nnz[0]), &(rows_nnz[0]), m, boost::mpi::get_mpi_datatype<uintmax_t>(), MPI_SUM, MPI_COMM_WORLD);
    // release memory
    std::vector<uintmax_t>().swap(local_rows_nnz);

    // first row index for each process
    std::vector<uintmax_t> first_row(P + 1, 0);
    uintmax_t partial_row_sum = 0;
    uintmax_t row = 0;
    uintmax_t p = 0;
    while (row < m) {
        partial_row_sum += rows_nnz[row];
        double temp = double((p + 1) * nnz) / (double)P;
        if ((double)partial_row_sum > temp) {
            p++;
            if (p > (P - 1))
                break;

            first_row[p] = row;

            if (p >= (P - 1))
                break;
        }
        row++;
    }
    first_row[P] = m;

    // release memory
    std::vector<uintmax_t>().swap(rows_nnz);

    MPI_Barrier(MPI_COMM_WORLD);

    duration = std::chrono::system_clock::now() - start;
    if (rank == 0)
        std::cout << "Process " << rank << " balanced distribution calc time: " << duration.count() << " [s]" << std::endl;

// convert local matrix to coo
    auto& cols = col_inds;

    // local to global indexing
/*
for (int k = 0; k < local_nnz; k++)
    if ((n_offset + col_inds[k]) >= n) {
        std::cerr << n_offset << " + " << col_inds[k] << " >= " << n << std::endl;
        throw std::runtime_error("Something wrong here.");
    }

    for (auto iter = cols.begin(), enditer = cols.end(); iter != enditer; ++iter)
        *iter += n_offset;
*/
    // global row indexes
    std::vector<size_t> rows(local_nnz);
    for (uintmax_t local_row = 0; local_row < m_local; local_row++)
        for (uintmax_t row_ptr = row_ptrs[local_row]; row_ptr < row_ptrs[local_row + 1]; row_ptr++)
            rows[row_ptr] = local_row + m_offset;

// redistribute elements according to calculated row-wise partitioning
	start = std::chrono::system_clock::now();

    // send counts to all processors
    p = 0;
    std::vector<int> sendcnts(P, 0);
    for (uintmax_t local_row = 0; local_row < m_local; local_row++) {
        uintmax_t row_nnz = row_ptrs[local_row + 1] - row_ptrs[local_row];
        uintmax_t row = local_row + m_offset;
        while (row >= first_row[p + 1])
            p++;
        sendcnts[p] += row_nnz;
    }

    // send displacements to all processors
    std::vector<int> sdispls(P, 0);
    for (uintmax_t k = 1; k < P; k++)
        sdispls[k] = sdispls[k - 1] + sendcnts[k - 1];

    // receive counts
    std::vector<int> recvcnts(P);
    MPI_Alltoall(&(sendcnts[0]), 1, MPI_INT, &(recvcnts[0]), 1, MPI_INT, MPI_COMM_WORLD);

    // receive displacements
    std::vector<int> rdispls(P, 0);
    for (uintmax_t k = 1; k < P; k++)
        rdispls[k] = rdispls[k - 1] + recvcnts[k - 1];

    // total elements to be received
    uintmax_t total = 0;
    for (uintmax_t k = 0; k < P; k++)
        total += recvcnts[k];

    std::vector<size_t> cols_(total);
    std::vector<size_t> rows_(total);
    std::vector<float>  vals_(total);

    // redistribute data
    MPI_Datatype mdt = boost::mpi::get_mpi_datatype<size_t>();
    MPI_Alltoallv(&(rows [0]), &(sendcnts[0]), &(sdispls[0]), mdt,
                  &(rows_[0]), &(recvcnts[0]), &(rdispls[0]), mdt, MPI_COMM_WORLD);
    MPI_Alltoallv(&(cols [0]), &(sendcnts[0]), &(sdispls[0]), mdt,
                  &(cols_[0]), &(recvcnts[0]), &(rdispls[0]), mdt, MPI_COMM_WORLD);
    mdt = MPI_FLOAT;
    MPI_Alltoallv(&(vals [0]), &(sendcnts[0]), &(sdispls[0]), mdt,
                  &(vals_[0]), &(recvcnts[0]), &(rdispls[0]), mdt, MPI_COMM_WORLD);

    // release memory 
    std::vector<int>().swap(sendcnts);
    std::vector<int>().swap(sdispls);
    std::vector<int>().swap(recvcnts);
    std::vector<int>().swap(rdispls);

    // release original CSR matrix
    std::vector<float >().swap(vals);
    std::vector<size_t>().swap(column_indices);
    std::vector<size_t>().swap(row_ptr);
    std::vector<size_t>().swap(rows);

    MPI_Barrier(MPI_COMM_WORLD);

    duration = std::chrono::system_clock::now() - start;
    if (rank == 0)
        std::cout << "Process " << rank << " elements redistribution time: " << duration.count() << " [s]" << std::endl;

// sort elements lexicographically     
	start = std::chrono::system_clock::now();

    hybrid_sort_ptr_3(&rows_[0], &cols_[0], &vals_[0], rows_.size(), comparator_rowcol_ptr<size_t>());

    duration = std::chrono::system_clock::now() - start;
    if (rank == 0)
        std::cout << "Process " << rank << " elements ordering time: " << duration.count() << " [s]" << std::endl;

// calculate numbers of nonzeros for rows
    m_local = first_row[rank + 1] - first_row[rank];
    m_offset = first_row[rank];
    local_nnz = rows_.size();

    // balanced report
    size_t max_local_nnz, min_local_nnz, avg_local_nnz;
    MPI_Reduce(&local_nnz, &max_local_nnz, 1, boost::mpi::get_mpi_datatype<size_t>(), MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&local_nnz, &min_local_nnz, 1, boost::mpi::get_mpi_datatype<size_t>(), MPI_MIN, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        avg_local_nnz = nnz / P;
        std::cout << "Nonzeros per process: " 
            << "min = " << min_local_nnz << ", "
            << "avg = " << avg_local_nnz << ", "
            << "max = " << max_local_nnz << std::endl;
    }

    // release memory
    std::vector<uintmax_t>().swap(first_row);

    std::vector<PetscInt> d_nnz(m_local, 0);
    std::vector<PetscInt> o_nnz(m_local, 0);

    for (uintmax_t k = 0; k < local_nnz; k++) {
// TODO: remove checks
if ((rows_[k] < m_offset) && (rows_[k] >= (m_offset + m_local))) 
    throw std::runtime_error("Something wrong here (1).");

if (rows_[k] < m_offset) 
    throw std::runtime_error("Something wrong here (2).");

        uintmax_t local_row = rows_[k] - m_offset;

if (local_row >= m_local) 
    throw std::runtime_error("Something wrong here (3).");
        
        if ((cols_[k] >= m_offset) && (cols_[k] < (m_offset + m_local))) 
            d_nnz[local_row]++;
        else
            o_nnz[local_row]++;
    } 

// construct PETSc matrix
	start = std::chrono::system_clock::now();

    Mat A;
    ierr = MatCreate(PETSC_COMM_WORLD, &A); CHKERRQ(ierr);
    ierr = MatSetType(A, MATMPISBAIJ); CHKERRQ(ierr);
 // ierr = MatSetSizes(A, m_local, PETSC_DECIDE, m, n); CHKERRQ(ierr); // WRONG !!!
    ierr = MatSetSizes(A, m_local, m_local, PETSC_DETERMINE, PETSC_DETERMINE); CHKERRQ(ierr);
 // ierr = MatSetBlockSize(A, 1); CHKERRQ(ierr); // 1 by default
    ierr = MatMPISBAIJSetPreallocation(A, 1, 0, &d_nnz[0], 0, &o_nnz[0]); CHKERRQ(ierr);

    // release memory
    std::vector<PetscInt>().swap(d_nnz);
    std::vector<PetscInt>().swap(o_nnz);

    // set values
	PetscPrintf(PETSC_COMM_WORLD, "Setting matrix values... ");
    for (uintmax_t k = 0; k < local_nnz; k++)
        ierr = MatSetValue(A, rows_[k], cols_[k], vals_[k], INSERT_VALUES); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD, "[DONE]\n");

	PetscPrintf(PETSC_COMM_WORLD, "Starting assembly... ");
    ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD, "[DONE]\n");

    MPI_Barrier(MPI_COMM_WORLD);

    duration = std::chrono::system_clock::now() - start;
    if (rank == 0)
        std::cout << "Process " << rank << " PETSc matrix construction: " << duration.count() << " [s]" << std::endl;

    // release memory
    std::vector<size_t>().swap(rows_);
    std::vector<size_t>().swap(cols_);
    std::vector<float >().swap(vals_);

// eigensolver
    // setup
    EPS eps;
    ierr = EPSCreate(PETSC_COMM_WORLD, &eps); CHKERRQ(ierr);
    ierr = EPSSetOperators(eps, A, PETSC_NULL); CHKERRQ(ierr);
    ierr = EPSSetProblemType(eps, EPS_HEP); CHKERRQ(ierr);
 // ierr = EPSSetTolerances(eps, PETSC_IGNORE, PETSC_DECIDE); CHKERRQ(ierr);
 // ierr = EPSSetType(eps, EPSKRYLOVSCHUR); CHKERRQ(ierr); // -eps_type, krylovschur by default

    ierr = EPSSetWhichEigenpairs(eps, EPS_SMALLEST_REAL); CHKERRQ(ierr);

    // set -eps_nev, -eps_max_it, etc.
    ierr = EPSSetFromOptions(eps); CHKERRQ(ierr);

    PetscInt nev, ncv, mpd;
    ierr = EPSGetDimensions(eps, &nev, &ncv, &mpd); CHKERRQ(ierr);
    if (rank == 0) 
        std::cout << "nev = " << nev << ", ncv = " << ncv << ", mpd = " << mpd << std::endl;

    // setup custom monitor
    custom_monitor_data monitor_data;
    monitor_data.rank = rank;
    monitor_data.nev = nev;
    ierr = EPSMonitorSet(eps, custom_monitor, (void*)(&monitor_data), NULL); CHKERRQ(ierr);

    // solve
	start = std::chrono::system_clock::now();
    ierr = EPSSolve(eps); CHKERRQ(ierr);
    duration = std::chrono::system_clock::now() - start;
    if (rank == 0) {
        std::cout << "Process " << rank << " eigensolver time: " << duration.count() << " [s]" << std::endl;
        std::cout << "Iterations time: " 
           << " min = " << monitor_data.min << " [s], "
           << " avg = " << (monitor_data.sum / (double)monitor_data.iter) << " [s], "
           << " max = " << monitor_data.max << " [s]" << std::endl;
    }

    // solution information
    PetscInt maxit, its, nconv;
    PetscReal tol;
    ierr = EPSGetIterationNumber(eps, &its); CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD, "Number of iterations of the method: %lu\n", its);
    ierr = EPSGetTolerances(eps, &tol, &maxit); CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD, "Stopping condition: tol = %.4g, maxit = %lu\n", tol, maxit);
    ierr = EPSGetConverged(eps, &nconv); CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD, "Number of converged eigenpairs: %lu\n", nconv);

// get eigenvalues and eigenvectors
    duration = std::chrono::system_clock::now() - start;

    PetscScalar eig_re, eig_im;
	Vec xr, xi;

    ierr = MatGetVecs(A, PETSC_NULL, &xr); CHKERRQ(ierr);
    ierr = MatGetVecs(A, PETSC_NULL, &xi); CHKERRQ(ierr);

    for (int k = 0; k < nconv; k++) {
        ierr = EPSGetEigenpair(eps, k, &eig_re, &eig_im, xr, xi); CHKERRQ(ierr);

        PetscReal error;
        ierr = EPSComputeRelativeError(eps, k, &error); CHKERRQ(ierr);

        if (eig_im < 1e-8) {
			PetscPrintf(PETSC_COMM_WORLD, "Eigenvalue number %3lu: %9f, relative error: %12g\n",
                    k + 1, eig_re, error);

            // save vector elements
            PetscInt size;
            ierr = VecGetSize(xr, &size); CHKERRQ(ierr);
            PetscInt local_size;
            ierr = VecGetLocalSize(xr, &local_size); CHKERRQ(ierr);
            const PetscInt* ranges;
            ierr = VecGetOwnershipRanges(xr, &ranges); CHKERRQ(ierr);

            // send elements to root
            if ((rank > 0) && (local_size > 0)) {
                PetscScalar* a;
                ierr = VecGetArray(xr, &a); CHKERRQ(ierr);
                MPI_Send((void*)a, local_size, boost::mpi::get_mpi_datatype<PetscScalar>(), 0, 0, MPI_COMM_WORLD);
                ierr = VecRestoreArray(xr, &a); CHKERRQ(ierr);
            }
            else {
                // output file
                stringstream ss_file_name;
                ss_file_name << ss_base_file_name.str() << "_" << std::fixed << std::setprecision(5) << eig_re << "_eigen" << k; 
				
				ss_file_name << ".dat"; 
				std::ios_base::openmode open_mode = (std::ios::binary | std::ios::trunc);

                ofstream f(ss_file_name.str(), open_mode);

                float value;

                // process local elements
                PetscScalar* x;
                ierr = VecGetArray(xr, &x); CHKERRQ(ierr);
                for (uintmax_t i = 0; i < m_local; ++i)
                {
                    value = x[i]; 
					f.write((const char*)&value, sizeof(float));
                }
                ierr = VecRestoreArray(xr, &x); CHKERRQ(ierr);

                // process remote elements
                std::vector<PetscScalar> y;

                for (int p = 1; p < P; p++) {
                    uintmax_t m_remote = ranges[p + 1] - ranges[p];
                    if (m_remote > 0) {
                        y.resize(m_remote);
                        MPI_Status status;
                        MPI_Recv(&y[0], m_remote, boost::mpi::get_mpi_datatype<PetscScalar>(),
                                p, 0, MPI_COMM_WORLD, &status);
                    }

                    for (uintmax_t i = 0; i < m_remote; ++i)
                    {
                        value = y[i]; 
						f.write((const char*)&value, sizeof(float));
                    }
                }
            }
        }
        else
			PetscPrintf(PETSC_COMM_WORLD, "Eigenvalue number %3lu: %9f+%9fi, relative error: %12g)\n",
                    k + 1, eig_re, eig_im, error);

    }

    duration = std::chrono::system_clock::now() - start;
    if (rank == 0)
        std::cout << "Process " << rank << " results processing time: " << duration.count() << " [s]" << std::endl;

    ierr = VecDestroy(&xr); CHKERRQ(ierr);
    ierr = VecDestroy(&xi); CHKERRQ(ierr);
    ierr = EPSDestroy(&eps); CHKERRQ(ierr);
    ierr = MatDestroy(&A); CHKERRQ(ierr);

 // MPI_Finalize();
    ierr = SlepcFinalize(); CHKERRQ(ierr);

	return EXIT_SUCCESS;
}
