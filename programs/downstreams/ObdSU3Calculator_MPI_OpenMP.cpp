#include <mpi.h>
#include <SU3ME/ComputeOperatorMatrix.h>

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <su3.h>

#include <boost/mpi.hpp>
//	To be able to load and distribute basis from a binary archive file
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 

#include <stdexcept>
#include <sstream>
#include <cmath>
#include <vector>
#include <stack>
#include <ctime>
#include <algorithm> // std::plus
#include <memory> 
#include <chrono>

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;

/*
float bra_x_Observable_x_ket(const vector<float>& bra_wfn, const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs, const vector<float>& ket_wfn)
{
	vector<float> observable_x_ket(bra_wfn.size(), 0); // size equal to bra vector 

	assert(vals.size() == row_ptrs.back());
	assert(observable_x_ket.size() == row_ptrs.size() - 1);

	for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow)
	{
		for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival)
		{
			observable_x_ket[irow] += vals[ival]*ket_wfn[column_indices[ival]];
		}
	}

	float dresult = std::inner_product(bra_wfn.begin(), bra_wfn.end(), observable_x_ket.begin(), 0.0);
	return dresult;
}
*/

// each thread executes this function on the submatrix of an operator
double bra_x_Observable_x_ket_per_thread(const lsu3::CncsmSU3xSU2Basis& bra, const vector<float>& bra_wfn, const vector<float>& ket_wfn,
					// data structures describing sparse submatrix on CSR format
					const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs,
					// rows are organized into groups of (ip in) states. For each block we store its size and starting position in basis.
					// ipin_block_ids holds indices of non vanishing row blocks stored in CSR structure
					const vector<uint32_t> ipin_block_ids)
{
	vector<double> observable_x_ket(row_ptrs.size()-1, 0);

	assert(vals.size() == row_ptrs.back());
	assert(observable_x_ket.size() == row_ptrs.size() - 1);

	for (int irow = 0; irow < row_ptrs.size() - 1; ++irow)
	{
		for (int ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival)
		{
			observable_x_ket[irow] += (double)vals[ival]*(double)ket_wfn[column_indices[ival]];
		}
	}

	double dresult = 0.0;
	int row0;
	int nrows;
	int i = 0;
	for (int iblock = 0; iblock < ipin_block_ids.size(); ++iblock)
	{
		row0 = bra.BlockPositionInSegment(ipin_block_ids[iblock]);
		nrows = bra.NumberOfStatesInBlock(ipin_block_ids[iblock]);
		for (int irow = 0; irow < nrows; ++irow, ++i)
		{
			dresult += bra_wfn[row0+irow]*observable_x_ket[i];
		}
	}
	return dresult;
}


//	TODO: this is an outdated ... root should create InteractionPPNN and broadcast it to all the collaborating processors
//
//	create a file compatible with one-body SU(3) decomposition of an
//	interaction, with just a single SU(3) tensor {a+a}			
void CreateInputFileForDensity(int na, int nb, int lm0, int mu0, int SS0, int k0, int LL0, int JJ0, string& type)
{
	ofstream dummy_ada_file("dummy_ada");
	dummy_ada_file << JJ0 << " " << JJ0 << endl;

	int k0max = SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2);
	vector<float> dcoeffs(2*k0max, 0.0);
	float su3xsu2phase = 1.0;
	char structure[2];	
	structure[0] = (na + 1);	// a^+	===> n + 1
	structure[1] = -1*(nb + 1);	// a	===> -n - 1;	

//	we need to swap tensors 
	if (na > nb)
	{
//	coefficient in front of the tensor will be multiplied by a phase:
//	[a^+(na 0) x a^(0 nb)]^(lm0 mu0)_{*} = (-)*(-)^{na + 0 + 0 + nb - lm0 - mu0 + 1/2 + 1/2 - S0} [a^(0 nb) x a^+(na 0)]^(lm0 mu0)_{*} 
		su3xsu2phase = -MINUSto(na + nb - lm0 - mu0 + 1 - SS0/2);
//	swap the two elements of structure: n_{a} <--> n_{b} 		
		std::swap(structure[0], structure[1]);
	}
	dummy_ada_file << (int)structure[0] << " " << (int)structure[1] << endl;
	dummy_ada_file << "1" << endl; // number of tensors that will follow: 1
	dummy_ada_file << lm0 << " " << mu0 << " " << SS0 << " " << LL0 << " " << JJ0 << endl;
//	save coefficients in an outdated yet internally still used ordering scheme: k0*rho0max*2 + rho*2 + type, where rho0max = 1  	
	for (int ik0 = 0; ik0 < k0max; ++ik0)
	{
		if (ik0 == k0)
		{
			if (type == "p")
			{
				dummy_ada_file << su3xsu2phase << " 0.0 ";
			}
			else if (type == "n")
			{
				dummy_ada_file << "0.0 " << su3xsu2phase << " ";
			}
			else
			{
				assert(0);
			}
		}
		else
		{
			dummy_ada_file << "0.0 0.0 " << endl;
		}
	}
	dummy_ada_file << endl;
}


bool ReadWfnSection(const string& wfn_file_name, const lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, const int ndiag, const int isection, vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

// 	this is actually more then we really need ...
	wfn.reserve(nelems); 
	float* wfn_full;
	wfn_full = new float[nelems];
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a file that contains order
//	of basis states for ndiag=1	
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		
//		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn_full[ipos], number_of_states_in_block*sizeof(float));
		}
	}

//	This loop iterates over blocks that belong to isection of basis split into ndiag segments
//	I am using the fact that for isection of the basis split into ndiag segment has the
//	following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
	for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag)
	{
//	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
		ipos = basis.BlockPositionInSegment(ipin_block);
		number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
		wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
	}

//	trim excess memory
	vector<float>(wfn).swap(wfn);

	delete []wfn_full;

	return true;
}

int ReadWfns(const vector<string> wfns, const lsu3::CncsmSU3xSU2Basis& full_basis, int ndiag_wfn, int ndiag, int isegment, vector<vector<float> >& wfns_bra)
{
	bool ok;
	for (int i = 0; i < wfns.size(); ++i)
	{
		// Note that each wave function in wfns_bra contains only (my_row)th section of bra model space that is made out of ndiag_bra sections 
		ok = ReadWfnSection(wfns[i], full_basis, ndiag_wfn, ndiag, isegment, wfns_bra[i]);
		if (!ok)
		{
			return false;
		}
	}
	return ok;
}


bool LoadInputFile(	const string& input_file_name,
					string& list_densities_file_name,
					int& ndiag_bra_wfn,
					int& ndiag_ket_wfn,
					int& ndiag_bra,
					int& ndiag_ket,
					string& bra_model_space_filename,
					string& ket_model_space_filename, 
					vector<string>& bra_wfn_filenames, vector<string>& ket_wfn_filenames)
{
	boost::mpi::communicator comm_world;

	ifstream input_file(input_file_name.c_str());
	if (!input_file)
	{
		cerr << "Could not open '" << input_file_name << "' input file!" << endl;
		return false;
	}

//	Open file with a list of na nb lm0 mu0 SS0 k0 LL0 JJ0 quantum numbers
	input_file >> list_densities_file_name;
	std::fstream list_densities_file(list_densities_file_name.c_str());
	if (!list_densities_file)
	{
		cerr << "Could not open '" << list_densities_file_name << "' input file!" << endl;
		return false;
	}

	input_file >> ndiag_bra_wfn >> ndiag_ket_wfn >> ndiag_bra >> ndiag_ket;
	int nprocs = comm_world.size();

	if (nprocs != ndiag_bra*ndiag_ket)
	{
		cerr << "Total number of MPI processes must be equal to the product of (#sections in bra space) and (#sections in ket space)!" << endl;
		cerr << "(#sections in bra space) = " << ndiag_bra << endl;
		cerr << "(#sections in ket space) = " << ndiag_ket << endl;
		cerr << "#processes = " << nprocs << endl;
		return false;
	}


	input_file >> bra_model_space_filename >> ket_model_space_filename;
	int num_bra_ket_wfns;
	input_file >> num_bra_ket_wfns;

	bra_wfn_filenames.reserve(num_bra_ket_wfns);
	ket_wfn_filenames.reserve(num_bra_ket_wfns);

	for (int i = 0; i < num_bra_ket_wfns; ++i)
	{
		string sbra_wfn_name;
		string sket_wfn_name;

		input_file >> sbra_wfn_name >> sket_wfn_name;
		bra_wfn_filenames.push_back(sbra_wfn_name);
		ket_wfn_filenames.push_back(sket_wfn_name);
	}
	return true;
}

void ModelSpaceLoad(int my_rank, string& model_space_filename, string& basis_filename, proton_neutron::ModelSpace& ncsmModelSpace)
{
	std::string model_space_buffer;
	int model_space_buffer_length;
//	Let's start with model space file for bra space	
	if (my_rank == 0)
	{
		ncsmModelSpace.Load(model_space_filename);

		std::ostringstream ss;
		boost::archive::binary_oarchive oa(ss);
		oa << ncsmModelSpace; 
		//	copy memory content of a binary archive into a buffer
		model_space_buffer = ss.str();
		model_space_buffer_length = model_space_buffer.size();
	}

	MPI_Bcast((void*)&model_space_buffer_length, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if (my_rank != 0)
	{
		model_space_buffer.resize(model_space_buffer_length);
	}

	MPI_Bcast((void*)model_space_buffer.data(), model_space_buffer_length, MPI_CHAR, 0, MPI_COMM_WORLD);
	if (my_rank != 0)
	{
		std::istringstream model_space_ss(model_space_buffer);
		boost::archive::binary_iarchive model_space_oa(model_space_ss);
		model_space_oa >> ncsmModelSpace;
	}
}

int main(int argc,char **argv)
{
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator comm_world;

	int my_rank = comm_world.rank();

	if (argc != 2)
	{
		if (my_rank == 0)
		{
			cout << "Usage: "<< argv[0] <<" <input file name>" << endl;
			cout << "Structure of input file:" << endl;
			cout << "<filename> with a list of {na nb lm0 mu0 SS0 k0 ll0 jj0}" << endl;
			cout << "<ndiag_bra_wfn> number of sections utilized to produce bra wfns			<ndiag_ket_wfn> number of sections utilized to produce ket wfns" << endl;
			cout << "<ndiag_bra> number of sections in bra space               			<ndiag_ket> number of sections in ket space" << endl;
			cout << "<filename> bra model space               					<filename> ket model space" << endl;
			cout << "<number> of bra-ket pairs" << endl;
			cout << "<filename> bra wfn 											<filename> ket wfn" << endl;
			cout << ". 															." << endl;
			cout << ". 															." << endl;
			cout << ". 															." << endl;
//			cout << "<hw value> in the case of E2 transitions" << endl;
			cout << endl;
			cout << "WARNING: When using a pregenerated basis, " << argv[0] << " attempts to open and read an associated model space definition file:\'<filename>.model_space\'." << endl;
			cout << "Make sure it exists before running the code!" << endl;
         cout << endl;
         cout << "Note:";
         cout << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can set the "
                 "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
              << endl;
		}
		env.abort(-1);
		return EXIT_FAILURE;
	}

	string list_densities_file_name;
	int ndiag_bra_wfn, ndiag_ket_wfn, ndiag_bra, ndiag_ket;
	vector<string> bra_wfn_filenames;
	vector<string> ket_wfn_filenames;
	string bra_model_space_filename, bra_basis_filename; 
	string ket_model_space_filename, ket_basis_filename; 

	if (my_rank == 0)
	{
		bool success = LoadInputFile(argv[1], list_densities_file_name, ndiag_bra_wfn, ndiag_ket_wfn, ndiag_bra, ndiag_ket, bra_model_space_filename, ket_model_space_filename, bra_wfn_filenames, ket_wfn_filenames);
		if (!success)
		{
			env.abort(-1);
			return EXIT_FAILURE;
		}
	}
	
	boost::mpi::broadcast(comm_world, bra_model_space_filename, 0);
	boost::mpi::broadcast(comm_world, ket_model_space_filename, 0);

   MPI_Bcast((void*)&ndiag_bra_wfn, 1, MPI_INT, 0, MPI_COMM_WORLD);
   MPI_Bcast((void*)&ndiag_ket_wfn, 1, MPI_INT, 0, MPI_COMM_WORLD);
   MPI_Bcast((void*)&ndiag_bra, 1, MPI_INT, 0, MPI_COMM_WORLD);
   MPI_Bcast((void*)&ndiag_ket, 1, MPI_INT, 0, MPI_COMM_WORLD);

//	boost::mpi::broadcast(comm_world, ndiag_bra_wfn, 0);
//	boost::mpi::broadcast(comm_world, ndiag_ket_wfn, 0);
//	boost::mpi::broadcast(comm_world, ndiag_bra, 0);
//	boost::mpi::broadcast(comm_world, ndiag_ket, 0);
	boost::mpi::broadcast(comm_world, bra_wfn_filenames, 0);
	boost::mpi::broadcast(comm_world, ket_wfn_filenames, 0);

	std::vector<std::shared_ptr<std::ofstream>> densities_output_files;
	if (my_rank == 0)
	{
		for (size_t i = 0; i < bra_wfn_filenames.size(); ++i)
		{
			std::ostringstream filename;
			filename << argv[1] << "_obdme_" << i << ".out";
			densities_output_files.push_back(std::make_shared<std::ofstream>(filename.str().c_str()));
			if (!*(densities_output_files.back()))
			{
				cerr << "Could not produce resulting files" << endl;
				env.abort(-1);
				return EXIT_FAILURE;
			} 
         (densities_output_files.back())->precision(10);
		}
	}

//  Note that each wave function in wfns_bra contains only (my_row)th section of bra model space that is made out of ndiag_bra sections 
//	E.G.: <2>10 20Ne
//	bra: J=8 ===> single wfn ... 194MB
//	ket: J=4 ===> single wfn ... 160MB
//	ndiag_bra=ndiag_ket=150
//	hw=25, 20, 15 ===> size: 3 x (194/150 + 160/150) MB = 7.08 MB
//	hw=10, 12.5, 15, 17.5, 20, 22.5, 25 ====> size: 7 x (194 + 160) = 16.52 MB
	vector<vector<float> > wfns_bra(bra_wfn_filenames.size());
	vector<vector<float> > wfns_ket(ket_wfn_filenames.size());

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);
	proton_neutron::ModelSpace bra_ncsmModelSpace, ket_ncsmModelSpace;

	ModelSpaceLoad(my_rank, bra_model_space_filename, bra_basis_filename, bra_ncsmModelSpace);
	ModelSpaceLoad(my_rank, ket_model_space_filename, ket_basis_filename, ket_ncsmModelSpace);


	std::ofstream output_file("/dev/null");
	bool allow_generate_missing_rme_files = (comm_world.size() == 1);

	uint16_t my_row, my_col;
	my_row = my_rank / ndiag_ket;
	my_col = my_rank % ndiag_ket;

	MPI_Comm ROW_COMM, COL_COMM;
	MPI_Comm_split(comm_world, my_row, my_col, &ROW_COMM);	// processes in row_comm share my_row value
	MPI_Comm_split(comm_world, my_col, my_row, &COL_COMM);	// processes in col_comm share my_col value


//	Construct basis
	lsu3::CncsmSU3xSU2Basis ket_basis, bra_basis;

   // One-body density operators may not be symmetric and 
   // bra and ket basis states may span two different model spaces.
   // We map a set of (ndiag_bra x ndiag_ket) MPI processes to a rectangular matrix.
   //
   // Example: ndiag_bra = 3 & ndiag_ket = 5
   // ======= 
   //
   // Then MPI ranks are ordered as follows:
   //                my_col
   //             0  1  2  3  4
   //     my_row  ---------------
   //      0      0  1  2  3  4    
   //      1      5  6  7  8  9  
   //      2      10 11 12 13 14
   //
   // All processes in MPI_COMM_WORLD will construct "my_row"th section of 
   // bra basis. At the end we set dims_[my_row] = 0 for all processes except
   // those in the first column (i.e. my_rank = 0 5 10) and use MPI_Allreduce
   // to distribute dims_ array between all processes. For details see
   // libraries/LSU3shell/ncsmSU3xSU2Basis.cpp line 946 ...
	bra_basis.ConstructBasis(bra_ncsmModelSpace, my_row, ndiag_bra, MPI_COMM_WORLD, [&](int, int){return my_col == 0;});

	ket_basis.ConstructBasis(ket_ncsmModelSpace, my_col, ndiag_ket, MPI_COMM_WORLD, [&](int, int){return my_row == 0;});

	int JJbra = bra_basis.JJ();
	int JJket = ket_basis.JJ();

	assert(bra_basis.Nmax() == ket_basis.Nmax());

	CBaseSU3Irreps baseSU3Irreps(bra_basis.NProtons(), bra_basis.NNeutrons(), bra_basis.Nmax()); 

	boost::mpi::communicator row_comm(ROW_COMM, boost::mpi::comm_take_ownership); 
	boost::mpi::communicator col_comm(COL_COMM, boost::mpi::comm_take_ownership);
	int my_row_rank, my_col_rank;

	my_row_rank = row_comm.rank();
	my_col_rank = col_comm.rank();

	if (my_rank == 0)
	{
		cout << "Creating complete basis of model space to be able to read an input files ...\n";cout.flush();
	}
   auto start = std::chrono::system_clock::now();
	{
		lsu3::CncsmSU3xSU2Basis full_basis;
// construct full basis on-the-fly sequentially ...
		if (my_row_rank == 0)
		{
			if (my_rank == 0) 
			{
				cout << "All MPI processes from the first column are constructing bra basis with a single segment on-the-fly" << endl;
			}
			// ReadWfnSection assumes full_basis will describe the entire basis
			// WARNING: !!! This can be a huge data structure !!!!
			// For instance <2>10 basis of 20-Ne takes 6.6 GB of disk space !!
			full_basis.ConstructBasis(bra_ncsmModelSpace, bra_basis, 0, 1);
		}
		if (my_row_rank == 0)
		{
			bool ok = ReadWfns(bra_wfn_filenames, full_basis, ndiag_bra_wfn, ndiag_bra, my_row, wfns_bra);
			if (!ok)
			{
				env.abort(-1);
				return EXIT_FAILURE;
			}
		}
	}

	{
		lsu3::CncsmSU3xSU2Basis full_basis;
// construct full basis on-the-fly sequentially
		if (my_col_rank == 0)
		{
			if (my_rank == 0)
			{
				cout << "All MPI processes from the first row are constructing ket basis with a single segment on-the-fly" << endl;
			}
			// ReadWfnSection assumes full_basis will describe the entire basis
			// WARNING: !!! This can be a huge data structure !!!!
			// For instance <2>10 basis of 20-Ne takes 6.6 GB of disk space !!
			full_basis.ConstructBasis(ket_ncsmModelSpace, ket_basis, 0, 1);
		}
		if (my_col_rank == 0)
		{
			bool ok = ReadWfns(ket_wfn_filenames, full_basis, ndiag_ket_wfn, ndiag_ket, my_col, wfns_ket);
			if (!ok)
			{
				env.abort(-1);
				return EXIT_FAILURE;
			}
		}
	}
	std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
	if (my_rank == 0)
	{
		cout << "done in " << duration.count() << endl;
	}

   for (int i = 0; i < wfns_bra.size(); ++i)
   {
      int size = wfns_bra[i].size();
      MPI_Bcast((void*)&size, 1, MPI_INT, 0, ROW_COMM); 
      if (my_row_rank != 0)
      {
         wfns_bra[i].resize(size);
      }
      MPI_Bcast((void*)wfns_bra[i].data(), size, MPI_FLOAT, 0, ROW_COMM); 
   }
// For some reason this does not work @ LSD machine!!!
// ==> I decided to implement it using MPI_Bcast calls
//	boost::mpi::broadcast(row_comm, wfns_bra, 0);

   for (int i = 0; i < wfns_ket.size(); ++i)
   {
      int size = wfns_ket[i].size();
      MPI_Bcast((void*)&size, 1, MPI_INT, 0, COL_COMM);

      if (my_col_rank != 0)
      {
         wfns_ket[i].resize(size);
      }
      MPI_Bcast((void*)wfns_ket[i].data(), size, MPI_FLOAT, 0, COL_COMM);
   }
#ifndef NDEBUG
//TODO: rewrite using for_each(....)
   for (int i = 0; i < wfns_bra.size(); ++i)
   {
      if (bra_basis.dim() != wfns_bra[i].size())
      {
         std::cerr << "rank:" << my_rank << " wfns_bra[" << i << "].size():" <<  wfns_bra[i].size();
         std::cerr  << "    dim(bra basis):" << bra_basis.dim() << std::endl;
         env.abort(-1);
         return EXIT_FAILURE;
      }
      if (ket_basis.dim() != wfns_ket[i].size())
      {
         std::cerr << "wfns_ket[" << i << "].size():" <<  wfns_ket[i].size();
         std::cerr  << "    dim(ket basis):" << ket_basis.dim() << std::endl;
         env.abort(-1);
         return EXIT_FAILURE;
      }
   }
#endif
	std::fstream list_densities_file;
	int number_of_obds;
	if (my_rank == 0)
	{
		list_densities_file.open(list_densities_file_name.c_str());
		list_densities_file >> number_of_obds;
	}
	boost::mpi::broadcast(comm_world, number_of_obds, 0);

	int na, nb, lm0, mu0, SS0, k0, LL0, JJ0; 
	string type;
	for (int idensity = 0; idensity < number_of_obds; ++idensity)
	{
		if (my_rank == 0)
		{
			list_densities_file >> na >> nb >> lm0 >> mu0 >> SS0 >> k0 >> LL0 >> JJ0 >> type;
//	check if (na 0) x (0 nb) --> (lm0 mu0) and the other quantum numbers
			if (!SU3::mult(SU3::LABELS(1, na, 0), SU3::LABELS(1, 0, nb), SU3::LABELS(1, lm0, mu0)))
			{
				cout << "(" << na << " 0) x (0 " << nb << ") cannot yield (lm0 mu0)=(" << lm0 << " " << mu0 << ")!" << endl;
				env.abort(-1);
				return EXIT_FAILURE;
			}
//	Does 1/2 x 1/2 --> S0 ???		
			if (!SU2::mult(1, 1, SS0))
			{
				cout << "1/2 x 1/2 cannot yield S0=" << SS0/2.0 << endl;
				env.abort(-1);
				return EXIT_FAILURE;
			}
//	Does LL0 exist in (lm0 mu0) ?
			if (!SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2))
			{
				cout << "(lm0 mu0)=(" << lm0 << " " << mu0 << ") irrep does not contain LL0=" << LL0 << "!" << endl; 
				env.abort(-1);
				return EXIT_FAILURE;
			}
//	Is 0 < k0 < kmax ?
			if (k0 > SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2) || k0 < 0)
			{
				cout << "The value of k0=" << k0 << " does not make sense!" << endl;
				cout << "kmax(lm0 mu0)=(" << lm0 << " " << mu0 << ") for LL0=" << LL0 << " is equal to kmax=" << SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2) << endl; 
				env.abort(-1);
				return EXIT_FAILURE;
			}
//	Does SS0 x LL0 ---> JJ0 ?
			if (!SU2::mult(SS0, LL0, JJ0))
			{
				cout << "LL0=" << LL0 << " x SS0=" << SS0 << " cannot yield " << "JJ=" << JJ0 << "!" << endl;
				env.abort(-1);
				return EXIT_FAILURE;
			}

			cout << "na=" << na << " nb=" << nb << " (" << lm0 << " " << mu0 << ") SS0=" << SS0 << " k0=" << k0 << " LL0=" << LL0 << " JJ0=" << JJ0 << " " << type << endl;
			cout.flush();

			if (!SU2::mult(JJket, JJ0, JJbra))
			{
				float total = 0.0;
				for (size_t i = 0; i < bra_wfn_filenames.size(); ++i)
				{
/** use bra_sections and ket_sections data structures to read wave functions properly */ 
					*densities_output_files[i] << na << " " << nb << " " << lm0 << " " << mu0 << " " << SS0 << " " << k0 << " " << LL0 << " " << JJ0 << " " << type;
					*densities_output_files[i] << "\t" << total << endl;
					cout << "\t<" << bra_wfn_filenames[i] << "|| T ||" << ket_wfn_filenames[i] << ">";
					cout << "\t" << total << endl;
				}
				continue;
			}
//	create a file compatible with one-body SU(3) decomposition of an
//	interaction, with just a single SU(3) tensor {a+a}			
			CreateInputFileForDensity(na, nb, lm0, mu0, SS0, k0, LL0, JJ0, type);
		}
//	I don't have to call barrier, since only root will read the input file
//	"dummy_ada" (see code of CInteractionPPNN::AddOneBodyOperators)

      // just in case we need to construct rmes ==> we need to call su3lib
      su3::init();
		CInteractionPPNN interactionPPNN(baseSU3Irreps, true, output_file);
// 		TODO: PN should be removed. It is redundant but for the sake of fast implementation I am keeping it here. 
		CInteractionPN interactionPN(baseSU3Irreps, allow_generate_missing_rme_files, true, output_file);
		try
		{
			COperatorLoader operatorLoader;
//		TODO: implement a loading method that loads a single one-body operator 
			operatorLoader.AddOneBodyOperator("dummy_ada", 1.0);
			operatorLoader.Load(my_rank, interactionPPNN, interactionPN, false); // false ==> do not print output messages
		}
		catch (const std::logic_error& e) 
		{	 
	   		std::cerr << e.what() << std::endl;
			env.abort(-1);
			return EXIT_FAILURE;
    	}
      su3::finalize();

//		The order of coefficients is given as follows:
// 		index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//		TransformTensorStrengthsIntoPP_NN_structure turns that into:
// 		index = type*k0max*rho0max + k0*rho0max + rho0
		interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

		vector<float> results_mpi_process(bra_wfn_filenames.size(), 0);
		vector<float> results_total(bra_wfn_filenames.size(), 0);


      start = std::chrono::system_clock::now();

//////////////////////////  OPENMP ///////////////////////////////
// Each thread computes a part of matrix splitted between threads by blocks of
// states that are result of (wp x wn) coupling.
		#pragma omp parallel
		{
			vector<float> vals;	
			vector<size_t> column_indices;
			vector<size_t> row_ptrs;
// CSR data structure stored in vals, column_indices, row_ptrs describes a submatrix defined by set of (wp x wn) bra basis configurations
// identification number of wp x wn block of states will be stored in ipin_block_ids
			vector<uint32_t> ipin_block_ids;

			row_ptrs.reserve(wfns_bra[0].size()+1);
//			row_ptrs.reserve(bra_basis.dim()+1);
			row_ptrs.push_back(0);

         su3::init_thread();
#ifdef SU3_9LM_HASHINDEXEDARRAY
                        CWig9lmLookUpTable<float>::initialize();
#endif
			CalculateME_NonScalar_FullMatrix_OpenMP_OneBodyOnly(interactionPPNN, interactionPN, bra_basis, ket_basis, vals, column_indices, row_ptrs, ipin_block_ids);
#ifdef SU3_9LM_HASHINDEXEDARRAY
                        CWig9lmLookUpTable<float>::finalize();
#endif

// make sure all threads are done using su3 and su2 libraries         
#pragma omp barrier         
         su3::finalize_thread();

			for (size_t i = 0; i < bra_wfn_filenames.size(); ++i)
			{
// each thread computes value of <bra | Operator_thread | ket>
				float result_thread = bra_x_Observable_x_ket_per_thread(bra_basis, wfns_bra[i], wfns_ket[i], vals, column_indices, row_ptrs, ipin_block_ids);
// add contribution due to thread into <bra | Operator_mpi_process | ket> for a given MPI process
				#pragma omp atomic
				results_mpi_process[i] += result_thread;
			}
		}
////////////////////// END OF OPENMP ///////////////////////////

// root process obtains total sum of results for each mpi process
		MPI_Reduce(&results_mpi_process[0], &results_total[0], results_total.size(), MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

		if (my_rank == 0)
		{
         std::cout.precision(10);
			for (size_t i = 0; i < bra_wfn_filenames.size(); ++i)
			{
				*densities_output_files[i] << na << " " << nb << " " << lm0 << " " << mu0 << " " << SS0 << " " << k0 << " " << LL0 << " " << JJ0 << " " << type;
				*densities_output_files[i] << "\t" << results_total[i] << endl;
				cout << "\t<" << bra_wfn_filenames[i] << "|| T ||" << ket_wfn_filenames[i] << ">";
				cout << "\t" << results_total[i] << endl;
			}
		}
	}

//	Finally write some information at the end of each output file
	if (my_rank == 0)
	{
		for (size_t i = 0; i < bra_wfn_filenames.size(); ++i)
		{
			*densities_output_files[i] << JJbra << " " << JJket << endl;
			*densities_output_files[i] << bra_model_space_filename << " " << ket_model_space_filename << endl;
			*densities_output_files[i] << bra_wfn_filenames[i] << " " << ket_wfn_filenames[i] << endl;
		}
      duration = std::chrono::system_clock::now() - start;
      std::cout << "Time to compute densities: " << duration.count() << " seconds." << std::endl;
	}
}
