#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <fstream>
#include <iostream>
#include <map>
#include <string>

#include <LSU3/std.h>

using namespace cpp0x;
//using namespace std;


//#define SAVE_MATRIX_AS_TEXT

/* 
	\f$b_{0}=\sqrt{\frac{197.327}{938.92\hbar\Omega}}\f$
*/	
inline double HoLength_fm(double hw)
{
	return sqrt((197.327*197.327)/(938.92*hw)); // hw must be given in MeV; result units: fm
}
/**
    This function calculates
	\f$  \frac{1}{J_{i} + 1}(2J_{f}+1)b_{0}^{4}\left[\frac{1}{b_{0}^{2}}\frac{\lange J||Q_{2}||J\rangle}{\sqrt{2J + 1}}\right]^{2}\f$
*/
double CalculateBE2(double total, SU2::LABEL JJi, SU2::LABEL JJf, double b0)
{
	double b04 = b0*b0*b0*b0;
//  In order to be compatible with MFDn results, I have to omit 1/(2Ji + 1) factor
//	return (5.0/(16.0*M_PI))*b04*(JJf + 1.0)*(total*total);

//	This result may differ from MFDn by factor of 1/(2Ji+1)
	return ((JJf + 1.0)/(JJi+1.0))*(5.0/(16.0*M_PI))*b04*(total*total);
}

/**
    This function calculates
	\f$ \frac{1}{2J_{i} + 1}(2J_{f}+1)[\frac{\lange J||M_{1}||J\rangle}{\sqrt{2J + 1}}\right]^{2} \f$
	in units of \f$\left[\mu^{2}_{N}\right]\f$.
*/
double CalculateBM1(double total, SU2::LABEL JJi, SU2::LABEL JJf)
{
//  In order to be compatible with MFDn results, I have to omit 1/(2Ji + 1) factor
//	return (JJf + 1.0)*(total*total);

//	This result may differ from MFDn by factor of 1/(2Ji+1)
	return ((JJf + 1.0)/(JJi + 1.0))*(total*total);
}

int main(int argc, char** argv)
{
//	(n1, n2, lm0, mu0, SS0, k0, LL0, JJ0, type='p' or 'n') and associated obd
//	Note that (n1, n2) = {-(nt + 1), (nd + 1)} in case (nt < nd) or {(nd + 1), -(nt + 1)} in case (nd < nt)
    std::map<tuple<unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, char>, double > obds_map; 

	if (argc != 4)
	{
		std::cout << "Usage: "<< argv[0] <<"<observable> <filename: one-body operator> <filename: obds>" << std::endl;
		std::cout << "where observable is either BE2 or BM1" << std::endl;
		return EXIT_FAILURE;
	}

	std::string operator_type_name(argv[1]);
	if (operator_type_name != "BE2"  && operator_type_name != "BM1")
	{
		std::cout << "'" << argv[1] << "' is an unknown type of operator" << std::endl;
		std::cout << "Observables implemented: BE2 and BM1." << std::endl;
		return 0;
	}

    std::ifstream one_body_operator_file(argv[2]);
	if (!one_body_operator_file)
	{
		std::cout << "Could not open '" << argv[2] << "' file that contains a one-body operator" << std::endl;
		return 0;
	}
    std::ifstream obds_file(argv[3]);
	if (!one_body_operator_file)
	{
		std::cout << "Could not open '" << argv[3] << "' file with one-body densities." << std::endl;
		return 0;
	}

	int nd, nt, lm0, mu0, SS0, k0, LL0, JJ0, total_MM0, total_JJ0, JJi, JJf, k0max;
	int n1, n2;
	char type;
	const char PP = 'p', NN = 'n';
	int number_obds, nTensors;
	float obd_value;
	float phase;
	float coeffN, coeffP;
	float dsu2me = 0.0;

//	Read from file with a list of obds and construct a map
	obds_file >> number_obds;
	for (int i = 0; i < number_obds; ++i)
	{
		obds_file >> nd >> nt >> lm0 >> mu0 >> SS0 >> k0 >> LL0 >> JJ0 >> type;
//		std::cout << nd << " " << nt << " " << lm0 << " " << mu0 << " " << SS0 << " " << k0 << " " << LL0 << " " << JJ0 << type << std::endl;
		if (type != PP && type != NN)
		{
			std::cout << "Wrong type entered: '" << type << "'" << std::endl;
			return 0;
		}
		obds_file >> obd_value;
//		std::cout << obd_value << std::endl;
		
		if (nd > nt)	// order of shells in SU(3)-code decomposition of one-body operators is always in increasing HO quanta
		{
			n1 = -(nt + 1);
			n2 = nd + 1;
			phase = -MINUSto(nt + nd - lm0 - mu0 + 1 - SS0/2);
//			std::cout << "phase:" << phase << std::endl;
		}
		else
		{
			n1 = nd + 1;
			n2 = -(nt + 1);
			phase = 1.0;
		}
		obds_map[tie(n1, n2, lm0, mu0, SS0, k0, LL0, JJ0, type)] = phase*obd_value;
	}
	obds_file >> JJf >> JJi;

	one_body_operator_file >> total_JJ0 >> total_MM0;
	if (!SU2::mult(JJi, total_JJ0, JJf))
	{
		std::cout << "Tensor operator with 2J0:" << total_JJ0 << " can not couple the ket state 2Ji:" << JJi << " with the final bra state 2Jf:" << JJf << std::endl;
		return 0;
	}

	std::cout << "{coeff} x {< JJf:" << JJf << "|| {a+ x ta} || JJi:" << (int)JJi << ">}" << std::endl;

//	Read all operator tensor terms and their coefficients
	while (true)
	{
		one_body_operator_file >> n1 >> n2;
		int nd, nt;

		if (n1 > 0) 
		{
			nd = n1 - 1;
			nt = abs(n2)-1;
		}
		else	// n1 < 0 
		{
			nd = n2 - 1;
			nt = abs(n1) - 1;
		}

		if (one_body_operator_file.eof()) {
			break;
		}

		one_body_operator_file >> nTensors;

//		iterate over tensors in the CTensorGroup in the input file 
		for (int i = 0; i < nTensors; ++i)
		{
			one_body_operator_file >> lm0 >> mu0 >> SS0 >> LL0 >> JJ0;
			if (JJ0 != total_JJ0)
			{
				std::cout << "Error one-body operator must have a constant J0 quantum number" << std::endl;
				return 0;
			}

			k0max = SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2);
			for (int k0 = 0; k0 < k0max; ++k0)
			{
				one_body_operator_file >> coeffP;
				if (!Negligible(coeffP))
				{
					obd_value = obds_map[tie(n1, n2, lm0, mu0, SS0, k0, LL0, JJ0, PP)];
					dsu2me += coeffP*obd_value;
					std::cout << "{" << coeffP << "} x {" << obd_value << "}" << "\t\t" << coeffP*obd_value << "\t\t\t";
					std::cout << "{a+(" << nd << " 0)1/2 x ta(0 " << nt << ")1/2}(" << (int)lm0 << " " << (int)mu0 << ")SS0:" << (int)SS0 << " k0:" << (int)k0 << " LL0:" << (int)LL0 << " JJ0:" << (int)JJ0 << std::endl;
				}
				one_body_operator_file >> coeffN;
				if (!Negligible(coeffN))
				{
					obd_value = obds_map[tie(n1, n2, lm0, mu0, SS0, k0, LL0, JJ0, NN)];
					dsu2me += coeffN*obd_value;
				}
			}
		}
	}
	if (operator_type_name == "BE2")
	{
		float hw, b0;
		std::cout << "< JJf:" << JJf << "|| Q_{J0=2} || JJi:" << JJi << "> = " << dsu2me << std::endl; 
		std::cout << "Enter HO energy [MeV] hw:";
		std::cin >> hw;	// calculating electric quadrupole moment requires the value of HO strength
		b0 = HoLength_fm(hw);
		std::cout << "hw = " << hw << "\tb0=" << b0 << " [fm]." << std::endl << std::endl;
		std::cout << "Jf = ";
		switch (JJf%2)
		{
			case 0: std::cout << JJf/2; break;
			case 1: std::cout << JJf << "/2 ";
		}
		std::cout << "\t" << "Ji = ";
		switch (JJi%2)
		{
			case 0: std::cout << JJi/2; break;
			case 1: std::cout << JJi << "/2 ";
		}
		std::cout << std::endl << std::endl;
		std::cout << "B(E2; Ji --> Jf) [e^2 fm^4] :" << CalculateBE2(dsu2me, JJi, JJf, b0) << std::endl;
	}
	else if (operator_type_name == "BM1")
	{
		std::cout << "Jf = ";
		switch (JJf%2)
		{
			case 0: std::cout << (int)JJf/2; break;
			case 1: std::cout << (int)JJf << "/2 ";
		}
		std::cout << "\t" << "Ji = ";
		switch (JJi%2)
		{
			case 0: std::cout << (int)JJi/2; break;
			case 1: std::cout << (int)JJi << "/2 ";
		}
		std::cout << std::endl << std::endl;
		std::cout << "B(M1; Ji --> Jf) [mu_{N}^2]:" << CalculateBM1(dsu2me, JJi, JJf) << std::endl;
	}
}
