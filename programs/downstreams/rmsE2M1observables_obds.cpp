#include <su3.h>
#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3NCSMUtils/clebschGordan.h>

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <set>
#include <tuple>

/* 
	\f$b_{0}=\sqrt{\frac{197.327}{938.92\hbar\Omega}}\f$
*/	
inline double HoLength_fm(double hw)
{
	return sqrt((197.327*197.327)/(938.92*hw)); // hw must be given in MeV; result units: fm
}
/**
    This function calculates
	\f$  C_{JJ\,20}^{JJ}b_{0}^{2}\left[\frac{1}{b_{0}^{2}}\frac{\lange J||Q_{2}||J\rangle}{\sqrt{2J + 1}}\right]\f$
*/
double CalculateE2(double total, SU2::LABEL JJ, double b0)
{
	return clebschGordan(JJ, JJ, 4, 0, JJ, JJ)*b0*b0*total;
}
/**
    This function calculates
	\f$ \sqrt{\frac{4\pi}{3}}C_{JJ\,10}^{JJ}\left[\frac{\langle J||M_{1}||J\rangle}{\sqrt{2J + 1}}\right]\f$
	in units of \f$\left[\mu_{N}\right]\f$.
*/
double CalculateM1(double total, SU2::LABEL JJ)
{
	return sqrt((4*M_PI)/3.0)*clebschGordan(JJ, JJ, 2, 0, JJ, JJ)*total;
}


enum Type {kProtons, kNeutrons, kNucleons};

// This structure holds quantum labels of 
//
// [a+^(nd_ 0) x ta^(0 nt_)]^(lm0_ mu0_)S0_ k0_ L0_ J0_ type 
//
struct adXta
{
	int nd_, nt_, lm0_, mu0_, ss0_, k0_, ll0_, jj0_, type_;

	adXta(int nd, int nt, int lm0, int mu0, int ss0, int k0, int ll0, int jj0, int type):
	nd_(nd), nt_(nt), lm0_(lm0), mu0_(mu0), ss0_(ss0), k0_(k0), ll0_(ll0), jj0_(jj0), type_(type) {}

//	return quantum number of the Hermitian conjugate [a+ x ta] term
	adXta conjugate()
	{
		return adXta(nt_, nd_, mu0_, lm0_, ss0_, k0_, ll0_, jj0_, type_);
	}
};

bool operator < (const adXta& lhs, const adXta& rhs)
{
	return std::tie(lhs.nd_, lhs.nt_, lhs.lm0_, lhs.mu0_, lhs.ss0_, lhs.k0_, lhs.ll0_, lhs.jj0_, lhs.type_) < std::tie(rhs.nd_, rhs.nt_, rhs.lm0_, rhs.mu0_, rhs.ss0_, rhs.k0_, rhs.ll0_, rhs.jj0_, rhs.type_);
}
bool operator == (const adXta& lhs, const adXta& rhs)
{
	return std::tie(lhs.nd_, lhs.nt_, lhs.lm0_, lhs.mu0_, lhs.ss0_, lhs.k0_, lhs.ll0_, lhs.jj0_, lhs.type_) == std::tie(rhs.nd_, rhs.nt_, rhs.lm0_, rhs.mu0_, rhs.ss0_, rhs.k0_, rhs.ll0_, rhs.jj0_, rhs.type_);
}

bool operator != (const adXta& lhs, const adXta& rhs)
{
	return std::tie(lhs.nd_, lhs.nt_, lhs.lm0_, lhs.mu0_, lhs.ss0_, lhs.k0_, lhs.ll0_, lhs.jj0_, lhs.type_) != std::tie(rhs.nd_, rhs.nt_, rhs.lm0_, rhs.mu0_, rhs.ss0_, rhs.k0_, rhs.ll0_, rhs.jj0_, rhs.type_);
}

//	Helper function to inspect the content of the data structure which holds
//	one-body density matrices
void ShowContent(const std::map<adXta, double>& obdmes)
{
	for (auto obdme_iter = obdmes.begin(); obdme_iter != obdmes.end(); ++obdme_iter)
	{
		adXta labels(obdme_iter->first);
		double obdme_value = obdme_iter->second;

		std::cout << labels.nd_ << " " << labels.nt_ << " ";
		std::cout << labels.lm0_ << " " << labels.mu0_ << " " << labels.ss0_ << " ";
		std::cout << labels.k0_ << " " << labels.ll0_ << " " << labels.jj0_ << " " << ((labels.type_ == kProtons) ? 'p' : 'n') << " " << obdme_value << std::endl;
	}
}

//	Read an input file and store its content in the memory using an STL map structure.
bool ReadOBDMEs(const char* obdmes_filename, std::map<adXta, double>& obdmes, int& JJf, int& JJi)
{
    std::ifstream obdmes_file(obdmes_filename);
	if (!obdmes_file)
	{
		std::cout << "Could not open '" << obdmes_filename << "' file with one-body densities." << std::endl;
		return EXIT_FAILURE;
	}

	int number_obdmes;
	int nd, nt, lm0, mu0, SS0, k0, LL0, JJ0, type;
	char type_char;
	double obd_value;
	
//	Read from file with a list of obdmes and construct a map
	obdmes_file >> number_obdmes;
	for (int i = 0; i < number_obdmes; ++i)
	{
		obdmes_file >> nd >> nt >> lm0 >> mu0 >> SS0 >> k0 >> LL0 >> JJ0 >> type_char;
		if (type_char != 'p' && type_char != 'n')
		{
			std::cout << "Wrong type entered: '" << type_char << "'" << std::endl;
			return EXIT_FAILURE;
		}
		obdmes_file >> obd_value;

		switch (type_char)
		{
			case 'p': type = kProtons; break;
			case 'n': type = kNeutrons; break;
			default: std::cout << "Type of [a+ x ta] term was not specified!\n";abort();
		}
		obdmes[adXta(nd, nt, lm0, mu0, SS0, k0, LL0, JJ0, type)] = obd_value;
	}
	obdmes_file >> JJf >> JJi;
}


//////////////////////////////////////////////////////////////
// <J || [ a+^(n1 0) x ta^(0 n2) ]^+ || J > = (-)^phase * < J || [a+^(n2 0) x ta^(0 n1)] || J>
int ComputeConjugatePhase(const adXta& labels)
// This relation was not derived analytically. It was derived
// from the resulting one-body density matrices computed
// for various nuclear eigenstates with nmax=7
//////////////////////////////////////////////////////////////
{
	int SS0 = labels.ss0_;
	int JJ0 = labels.jj0_;

	return MINUSto(SS0/2+JJ0/2);
}
//////////////////////////////////////////////////////////

//	iteratore over OBDMEs stored in an STL map and for each element try to find
//	its conjugate counterpart.  If not found it is inserted into the data
//	structure.
void GenerateConjugateOBDMEs(std::map<adXta, double>& obdmes)
{
	for (auto obdme_iter = obdmes.begin(); obdme_iter != obdmes.end(); ++obdme_iter)
	{
		adXta obdme_labels(obdme_iter->first);
		double obdme_value = obdme_iter->second;
		adXta conjugate_obdme_labels(obdme_labels.conjugate());

		if (obdme_labels != conjugate_obdme_labels && obdmes.find(conjugate_obdme_labels) == obdmes.end())
		{
			int phase = ComputeConjugatePhase(obdme_labels);
			obdmes[conjugate_obdme_labels] = phase*obdme_value;
		}
	}
}

//	Function reads one body operator and computes its expectation
//	value using OBDMEs stored on STL map obdmes. If particle type
//
//	particle_type == kProtons  ==> only proton OBDMEs are considered
//	particle_type == kNeutrons ==> only neutron OBDMEs are considered
//	particle_type == kNucleons ==> both proton & neutron OBDMEs are considered
template <int particle_type = kNucleons>
double ComputeExpectationValue(const std::map<adXta, double>& obdmes, const char* operator_filename, const int JJf, const int JJi)
{ 
	double result(0), coeffP(0), coeffN(0);
	std::ifstream one_body_operator_file(operator_filename);
	if (!one_body_operator_file)
	{
		std::cout << "Could not open '" << operator_filename << "' file with one-body densities." << std::endl;
		abort();
	}


	int total_MM0, total_JJ0, k0max;
	one_body_operator_file >> total_JJ0 >> total_MM0;
	if (!SU2::mult(JJi, total_JJ0, JJf))
	{
		std::cout << "Tensor operator with 2J0:" << total_JJ0 << " can not couple the ket state 2Ji:" << JJi << " with the final bra state 2Jf:" << JJf << std::endl;
		abort();
	}

	std::cout << "{coeff} x {< JJf:" << JJf << "|| {a+ x ta} || JJi:" << JJi << ">}" << std::endl;

	while (true)
	{
		int n1, n2, nd, nt;
		one_body_operator_file >> n1 >> n2;

		if (n1 > 0) 
		{
			nd = n1 - 1;
			nt = abs(n2)-1;
		}
		else // n1 < 0 => [ta^(0 n1) x a+^(n2 0)] term => need to transform into [a+(n2 0) x ta^(0 n1)]
		{
			nd = n2 - 1;
			nt = abs(n1) - 1;
		}

		if (one_body_operator_file.eof()) {
			break;
		}

		int nTensors;
		one_body_operator_file >> nTensors;

		int lm0, mu0, SS0, LL0, JJ0;
//		iterate over tensors in the CTensorGroup in the input file 
		for (int i = 0; i < nTensors; ++i)
		{
			one_body_operator_file >> lm0 >> mu0 >> SS0 >> LL0 >> JJ0;
			if (JJ0 != total_JJ0)
			{
				std::cout << "Error one-body operator must have a constant J0 quantum number" << std::endl;
				abort();
			}

			int phase = 1;
			if (n1 < 0) // we need to multiple a+a strength by the phase due to the recoupling [ta x a+] ---> [a+ x ta]
			{
				phase = -MINUSto(nt + nd - lm0 - mu0 + 1 - SS0/2);
			}

			k0max = SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2);
			for (int k0 = 0; k0 < k0max; ++k0)
			{
				one_body_operator_file >> coeffP;
				one_body_operator_file >> coeffN;

				coeffP *= phase;
				coeffN *= phase;

				if (!Negligible(coeffP) && (particle_type == kProtons || particle_type == kNucleons))
				{
					auto iter = obdmes.find(adXta(nd, nt, lm0, mu0, SS0, k0, LL0, JJ0, kProtons));
					if (iter != obdmes.end())
					{
						double obd_value = iter->second;
						result += coeffP*obd_value;
						std::cout << "{" << coeffP << "} x {" << obd_value << "}" << "\t\t" << coeffP*obd_value << "\t\t\t";
						std::cout << "{a+(" << nd << " 0)1/2 x ta(0 " << nt << ")1/2}(" << lm0 << " " << mu0 << ")SS0:" << SS0 << " k0:" << k0 << " LL0:" << LL0 << " JJ0:" << JJ0 << std::endl;
					}
				}
				if (!Negligible(coeffN) && (particle_type == kNeutrons || particle_type == kNucleons))
				{
					auto iter = obdmes.find(adXta(nd, nt, lm0, mu0, SS0, k0, LL0, JJ0, kNeutrons));
					if (iter != obdmes.end())
					{
						double obd_value = iter->second;
						result += coeffN*obd_value;
						std::cout << "{" << coeffN << "} x {" << obd_value << "}" << "\t\t" << coeffN*obd_value << "\t\t\t";
						std::cout << "{a+(" << nd << " 0)1/2 x ta(0 " << nt << ")1/2}(" << lm0 << " " << mu0 << ")SS0:" << SS0 << " k0:" << k0 << " LL0:" << LL0 << " JJ0:" << JJ0 << std::endl;
					}
				}
			}
		}
	}
	return result;
}

void PrintJ(int JJ)
{
	std::cout << "J = "; 
	switch (JJ%2) 
	{
      case 0: std::cout << JJ/2; break; 
      case 1: std::cout << JJ << "/2 "; 
	} 
	std::cout << std::endl << std::endl;
}

void E2(double total, int JJ)
{
	double hw, b0;

	std::cout << "Enter harmonic oscillator strength in MeV:"; 
	std::cin >> hw; 
	b0 = HoLength_fm(hw);
	std::cout << "hw = " << hw << "\tb0=" << b0 << " [fm]." << std::endl << std::endl;
	PrintJ(JJ);
	std::cout << "Q(JJ) [e fm^2]" << std::endl; 
	std::cout << CalculateE2(total, JJ, b0) << std::endl;
}

void RMS(double r2, int JJ)
{
	double hw, b0, A;

	std::cout << "Enter the total number of nucleons:";
	std::cin >> A;

	std::cout << "Enter harmonic oscillator strength in MeV:"; 
	std::cin >> hw; 
	b0 = HoLength_fm(hw);
	std::cout << "hw = " << hw << "\tb0=" << b0 << " [fm]." << std::endl << std::endl;
	PrintJ(JJ);
	double rms = sqrt((1.0/A)*(r2 - 1.5))*b0;
   std::cout << "Point-nucleon matter rms radius: " << rms << " [fm]." << std::endl;
}

void RMSproton(double r2protons, int JJ)
{
	double hw, b0, A, Z;

	std::cout << "Enter the total number of nucleons:";
	std::cin >> A;

	std::cout << "Enter the total number of protons:";
	std::cin >> Z;

	std::cout << "Enter harmonic oscillator strength in MeV:"; 
	std::cin >> hw; 
	b0 = HoLength_fm(hw);
	std::cout << "hw = " << hw << "\tb0=" << b0 << " [fm]." << std::endl << std::endl;
	PrintJ(JJ);
	double rms = sqrt((1.0/Z)*r2protons - 1.5/A)*b0;
   std::cout << "Point-proton rms radius: " << rms << " [fm]." << std::endl;
}

void RMSneutron(double r2neutrons, int JJ)
{
	double hw, b0, A, N;

	std::cout << "Enter the total number of nucleons:";
	std::cin >> A;

	std::cout << "Enter the total number of neutrons:";
	std::cin >> N;

	std::cout << "Enter harmonic oscillator strength in MeV:"; 
	std::cin >> hw; 
	b0 = HoLength_fm(hw);
	std::cout << "hw = " << hw << "\tb0=" << b0 << " [fm]." << std::endl << std::endl;
	PrintJ(JJ);
	double rms = sqrt((1.0/N)*r2neutrons - 1.5/A)*b0;
   std::cout << "Point-neutron rms radius: " << rms << " [fm]." << std::endl;
}

void M1(double total, int JJ)
{ 
	PrintJ(JJ);
   std::cout << "M1(JJ) [mu_N]" << std::endl; 
   std::cout << CalculateM1(total, JJ) << std::endl;
}


int main(int argc, char** argv)
{
   su3::init();
	if (argc != 3)
	{
		std::cout << "Usage: "<< argv[0] <<"<filename: obds>  <filename: one-body operator>" << std::endl;
		return EXIT_FAILURE;
	}

	std::map<adXta, double> obdmes;
	int JJf, JJi;
	ReadOBDMEs(argv[1], obdmes, JJf, JJi);
	GenerateConjugateOBDMEs(obdmes);

	enum OperatorType {kE2 = 0, kM1 = 1, kRMS = 2, kRMSproton = 3, kRMSneutron = 4};
	std::cout << "Which observable do you want to compute?" << std::endl;
	std::cout << kE2 << " ... Q2 moment" << std::endl;
	std::cout << kM1 << " ... M1 moment" << std::endl;
	std::cout << kRMS << " ... matter rms radius" << std::endl;
	std::cout << kRMSproton << " ... proton rms radius" << std::endl;
	std::cout << kRMSneutron << " ... neutron rms radius" << std::endl;

	int observable_type;
	std::cin >> observable_type;

	if (JJf != JJi)
	{
		std::cout << "Bra and ket total angular momentum must be equal, but we have JJf:" << JJf << " and JJi:" << JJi << "!" << std::endl;
		return EXIT_FAILURE;
	}

	double expectation_value;
	if (observable_type >= kE2 && observable_type <= kRMS)
	{
		expectation_value = ComputeExpectationValue<kNucleons>(obdmes, argv[2], JJf, JJi);
	}
	else if (observable_type == kRMSproton)
	{
		expectation_value = ComputeExpectationValue<kProtons>(obdmes, argv[2], JJf, JJi);
	}
	else if (observable_type == kRMSneutron)
	{
		expectation_value = ComputeExpectationValue<kNeutrons>(obdmes, argv[2], JJf, JJi);
	}

	std::cout << "<" << JJf << "|| O ||" << JJi << ">: " << expectation_value << std::endl;

	switch (observable_type)
	{
		case kE2: E2(expectation_value, JJf); break;
		case kM1: M1(expectation_value, JJf); break;
		case kRMS: RMS(expectation_value, JJf); break;
		case kRMSproton: RMSproton(expectation_value, JJf); break;
		case kRMSneutron: RMSneutron(expectation_value, JJf); break;
		default: std::cout << "Operator type " << observable_type << " is not implemented yet." << std::endl;
	}
   su3::finalize();
	return EXIT_SUCCESS;
}
