#include <mpi.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/BroadcastDataContainer.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <su3.h>

#include <chrono>
#include <stdexcept>
#include <sstream>
#include <cmath>
#include <vector>
#include <stack>

//#define SAVE_MATRIX_AS_TEXT

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;


/* 
	\f$b_{0}=\sqrt{\frac{197.327}{938.92\hbar\Omega}}\f$
*/	
inline double HoLength_fm(double hw)
{
	return sqrt((197.327*197.327)/(938.92*hw)); // hw must be given in MeV; result units: fm
}
/**
    This function calculates
	\f$  C_{JJ\,20}^{JJ}b_{0}^{2}\left[\frac{1}{b_{0}^{2}}\frac{\lange J||Q_{2}||J\rangle}{\sqrt{2J + 1}}\right]\f$
*/
double CalculateE2(double total, SU2::LABEL JJ, double b0)
{
	return clebschGordan(JJ, JJ, 4, 0, JJ, JJ)*b0*b0*total;
}
/**
    This function calculates
	\f$ \sqrt{\frac{4\pi}{3}}C_{JJ\,10}^{JJ}\left[\frac{\langle J||M_{1}||J\rangle}{\sqrt{2J + 1}}\right]\f$
	in units of \f$\left[\mu_{N}\right]\f$.
*/
double CalculateM1(double total, SU2::LABEL JJ)
{
	return sqrt((4*M_PI)/3.0)*clebschGordan(JJ, JJ, 2, 0, JJ, JJ)*total;
}

//#define SAVE_MATRIX_AS_TEXT

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col)
{
/*
	if (ndiag % 2 == 0)
	{
		if (my_rank == 0)
		{
			cout << "number of diagonal processes = " << ndiag <<
" must be an odd number." << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}
*/
	int executing_process_id(0);
	for (size_t i = 0; i < ndiag; ++i)
	{
		row = 0;
		for (col = i; col < ndiag; ++col, ++row, ++executing_process_id)
		{
			if (my_rank == executing_process_id)
			{
				return;
			}
		}
	}
}

bool ReadWfnSection(const string& wfn_file_name, lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, const int ndiag, vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

	wfn.reserve(nelems); 
	float* wfn_full;
	wfn_full = new float[nelems];
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a file that contains order
//	of basis states for ndiag=1	
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		
//		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn_full[ipos], number_of_states_in_block*sizeof(float));
		}
	}

//	This loop iterates over blocks that belong to isection of basis split into ndiag segments
//	I am using the fact that for isection of the basis split into ndiag segment has the
//	following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
	for (uint32_t isection = 0; isection < ndiag; ++isection)
	{
		for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag)
		{
//	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
		}
	}
//	trim excess memory
	vector<float>(wfn).swap(wfn);
	delete []wfn_full;
	return true;
}

class CObservablesRunParameters
{
	public:
	enum OperatorType {kE2, kM1, kRMS};

	CObservablesRunParameters(): ncsmModelSpace_(), wfns_() {}

	void Load(const std::string& file_name);
	
	inline OperatorType GetOperatorType() const {return operator_type_;}
	inline int GetNdiagWfn() const {return ndiag_input_wfn_;}
	inline int GetNdiagBasis() const {return ndiag_basis_;}
	inline float hw() const {return hw_;} 
	inline const proton_neutron::ModelSpace& GetModelSpace() {return ncsmModelSpace_;}
	inline std::string GetOperatorTypeName() const 
	{
		switch (operator_type_)
		{
			case kE2: return std::string("E2");
			case kM1: return std::string("M1");
			case kRMS: return std::string("RMS");
		}
	}
	inline const std::vector<std::string>& GetWnfs() const {return wfns_;}
	private:
	OperatorType operator_type_;
	int ndiag_input_wfn_;
	int ndiag_basis_;
	proton_neutron::ModelSpace ncsmModelSpace_;
	float hw_;
	std::vector<string> wfns_;

	friend class boost::serialization::access;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
   	{
		ar & operator_type_;
		ar & ndiag_input_wfn_;
		ar & ndiag_basis_;
		ar & ncsmModelSpace_;
		ar & hw_;
		ar & wfns_;
   	}
};

void CObservablesRunParameters::Load(const std::string& file_name)
{
	ifstream input_file(file_name.c_str());
	if (!input_file)
	{
		std::stringstream error_message;
		error_message << "Could not open '" << file_name << "' input file! File:" << __FILE__ << "\tLine:" << __LINE__ << ".\n";
		throw std::logic_error(error_message.str());
	}
//	Read an input file:
	std::string operator_type_name;
	input_file >> operator_type_name;

	if (operator_type_name == "E2")
	{
		operator_type_ = kE2;
	}
	else if (operator_type_name == "M1")
	{
		operator_type_ = kM1;
	}
	else if (operator_type_name == "RMS")
	{
		operator_type_ = kRMS;
	}

	input_file >> ndiag_input_wfn_ >> ndiag_basis_;

	int nprocs;

	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	if (nprocs != ndiag_basis_*(ndiag_basis_ + 1)/2)
	{
		std::stringstream error_message;
		error_message << "Total number of processes must be equal to (#sections)*(#sections + 1)/2 !" << endl;
		error_message << "#processes required = " << ndiag_basis_*(ndiag_basis_ + 1)/2 << endl;
		error_message << "#processes = " << nprocs << endl;
		throw std::logic_error(error_message.str());
	}

	std::string model_space_filename;
	input_file >> model_space_filename;
	ncsmModelSpace_.Load(model_space_filename);

	int num_wfns;
	input_file >> num_wfns;

	wfns_.resize(num_wfns);
	for (int i = 0; i < num_wfns; ++i)
	{
		input_file >> wfns_[i];
	}

	if (operator_type_ == kE2 || operator_type_ == kRMS)
	{ 
		input_file >> hw_;	// calculating E2 & RMS require the value of HO strength
	}
}

float bra_x_Observable_x_ket_symmetric(	const vector<float>& bra_wfn, 
										const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs, 
										const vector<float>& ket_wfn, 
										int rowOffset, int columnOffset)
{
	vector<float> observable_x_ket(bra_wfn.size(), 0); // size equal to bra vector 

	assert(vals.size() == row_ptrs.back());

	for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow)
	{
		size_t iglobal_row = irow + rowOffset;
		for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival)
		{
            size_t iglobal_col = column_indices[ival] + columnOffset;
            float  val  = vals[ival];

        //  observable_x_ket[irow] += vals[ival]*ket_wfn[column_indices[ival]];
            observable_x_ket[iglobal_row] += val * ket_wfn[iglobal_col];
            if (iglobal_row != iglobal_col)
                observable_x_ket[iglobal_col] += val * ket_wfn[iglobal_row];
		}
	}

	float dresult = std::inner_product(bra_wfn.begin(), bra_wfn.end(), observable_x_ket.begin(), 0.0);
	return dresult;
}

int main(int argc,char **argv)
{
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

   	MPI_Init(&argc, &argv);

	int my_rank, my_row, my_col;
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

	cout.precision(10);

	if (argc != 2)
	{
		if (my_rank == 0)
		{
			cout << "Usage: "<< argv[0] <<" <input file name>" << endl;
			cout << "Structure of input file:" << endl;
			cout << "<operator type> one of the following E2 M1 RMS" << endl;
			cout << "<number of sections utilized to produce input wfns>" << endl;
			cout << "<number of diagonal processes [i.e. #section for basis spliting]>" << endl;
			cout << "<model space file name>" << endl;
			cout << "<number of input wave functions>" << endl;
			cout << "<file name 0>" << endl;
			cout << "<file name 1>" << endl;
			cout << " 	. " << endl;
			cout << " 	. " << endl;
			cout << " 	. " << endl;
			cout << "<hw value> in the case of E2 and RMS" << endl;
         cout << endl;
         cout << "Note:";
         cout << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can set the "
                 "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
              << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}
	
   su3::init();
	CObservablesRunParameters runParams;
//	root process reads the input file	
	if (my_rank == 0)
	{
		try
		{
			runParams.Load(argv[1]);
		}
		catch (const std::logic_error& e) 
		{ 
		   std::cerr << e.what() << std::endl;
    	   MPI_Abort(MPI_COMM_WORLD, -1);
    	}
	}
//	broadcast data from input file among collaborating processes
	BroadcastDataContainer(runParams, "CObservablesRunParameters");

//	assign a row and a column of a submatrix that the process with my_rank will be evaluating
	MapRankToColRow_MFDn_Compatible(runParams.GetNdiagBasis(), my_rank, my_row, my_col);

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

	lsu3::CncsmSU3xSU2Basis bra_basis(runParams.GetModelSpace(), my_row, runParams.GetNdiagBasis());	
	lsu3::CncsmSU3xSU2Basis ket_basis(runParams.GetModelSpace(), bra_basis, my_col, runParams.GetNdiagBasis());	

	int Z = runParams.GetModelSpace().number_of_protons();
	int N = runParams.GetModelSpace().number_of_neutrons();
	int Nmax = runParams.GetModelSpace().back().N();
		
	std::ofstream output_file("/dev/null");
	CBaseSU3Irreps baseSU3Irreps(Z, N, Nmax); 
	CInteractionPPNN interactionPPNN(baseSU3Irreps, true, output_file);
	CInteractionPN interactionPN(baseSU3Irreps, true, false, output_file);

	SU2::LABEL JJ = runParams.GetModelSpace().JJ();
	int A = Z + N;
	try
	{
		string sOperatorFile(su3shell_data_directory);
		COperatorLoader operatorLoader;

		if (runParams.GetOperatorType() == CObservablesRunParameters::kE2)
		{ 
			sOperatorFile += "/SU3_Interactions_Operators/Q2/Q2_1b_nmax14";
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
		else if (runParams.GetOperatorType() == CObservablesRunParameters::kM1)
		{
			sOperatorFile += "/SU3_Interactions_Operators/M1/M1_1b_nmax14";
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
		else if (runParams.GetOperatorType() == CObservablesRunParameters::kRMS) 
		{
			operatorLoader.Add_r2_mass_intrinsic(A);
		}
		else // ==> add an arbitrary operator such as e.g. Hamiltonian ...
		{
			std::stringstream error_message;
			error_message << "Unknown type of operator!" << endl;
			error_message << "Allowed values: any of the following E2 M1 RMS" << endl;
			error_message << "File:" << __FILE__ << "\tLine:" << __LINE__ << ".\n";
			throw std::logic_error(error_message.str());

//            sOperatorFile += "/SU3_Interactions_Operators/LS/LS_1b_nmax8";
//            operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
/** J0 and M0 labels of the operator are stored as: MECalculatorData::JJ0_ and  MECalculatorData::MM0_ 
 	and are being read from one-body interaction files ...
 */
		operatorLoader.Load(my_rank, interactionPPNN, interactionPN); // interaction PN is empty ...
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
       MPI_Abort(MPI_COMM_WORLD, -1);
    }

	if (!SU2::mult(JJ, MECalculatorData::JJ0(), JJ))
	{
		if (my_rank == 0)
		{
			cout << "Operator " << runParams.GetOperatorTypeName() << " is J0=" << (int)MECalculatorData::JJ0() << " and hence could not couple wave functions with JJ=" << (int)JJ << endl << endl;
		}
        MPI_Abort(MPI_COMM_WORLD, -1);
	}

//	The order of coefficients is given as follows:
// 	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//	TransformTensorStrengthsIntoPP_NN_structure turns that into:
// 	index = type*k0max*rho0max + k0*rho0max + rho0
	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

	vector<float> vals;	
	vector<size_t> column_indices;
	vector<size_t> row_ptrs;
	row_ptrs.push_back(0);

/**	
	Construct data structures allowing me to transform a wave function with the
	basis states order given by the runParams.GetNdiagWfn()-cyclic distribution into ndiag'-cyclic 
	distribution.
*/	
	unsigned long firstStateId_bra = bra_basis.getFirstStateId();
	unsigned long firstStateId_ket = ket_basis.getFirstStateId();

/*
    Matrix matrix(false);
    Props& p = matrix.getProps();// global and local properties of submatrix with my_row row and my_col col coordinates

    p.globalNRows.setSize(bra_basis.getModelSpaceDim()); //	total number of rows
    p.localNRows.setSize(bra_basis.dim());	// number of local rows

    p.globalNColumns.setSize(ket_basis.getModelSpaceDim()); //  total number of columns
    p.localNColumns.setSize(ket_basis.dim());	// number of local columns

    p.rowOffset    = bra_basis.getFirstStateId();
    p.columnOffset = ket_basis.getFirstStateId();
*/
    cout << "Process " << my_rank << " starts calculation of me" << endl;
#ifdef SU3_9LM_HASHINDEXEDARRAY
        CWig9lmLookUpTable<float>::initialize();
#endif
	CalculateME_Scalar_UpperTriang(interactionPPNN, interactionPN, bra_basis, ket_basis, 0, my_row, 0, my_col, vals, column_indices, row_ptrs);
#ifdef SU3_9LM_HASHINDEXEDARRAY
        CWig9lmLookUpTable<float>::finalize();
#endif

//	if there are no matrix elements ==> mascot throw expection "Local number of
//	nonzeros value must be set."
    if (vals.size() == 0)
    {
        cout << "FYI: my_row: " << my_row << " my_col:" << my_col << "\t... all m.e. are vanishing." << endl;
//	store a single zero in matrix ==> this will make mascot library happy, i.e.
//	it won't throw exception
        vals.push_back(0.0);
        column_indices.push_back(0);
    }

    if (my_rank == 0)
    {
        bra_basis.Reshuffle(runParams.GetModelSpace(), 0, 1);
    }

    double b0;
    unsigned long dim_total = bra_basis.getModelSpaceDim();
    const std::vector<std::string>& wfns = runParams.GetWnfs();

    for (size_t i = 0; i < wfns.size(); ++i)
    {
        vector<float> bra_wfn;
/** root reads the entire wave function and orders basis states properly */			
        if (my_rank == 0)
        {
            ReadWfnSection(wfns[i], bra_basis, runParams.GetNdiagWfn(), runParams.GetNdiagBasis(), bra_wfn);
        }
        else
        {
            bra_wfn.resize(dim_total);
        }
        MPI_Bcast((void*)&bra_wfn[0], bra_wfn.size(), MPI_FLOAT, 0, MPI_COMM_WORLD);
        
        vector<float>  ket_wfn(bra_wfn);

		float result_local = bra_x_Observable_x_ket_symmetric(bra_wfn, vals, column_indices, row_ptrs, ket_wfn, bra_basis.getFirstStateId(), ket_basis.getFirstStateId());
		float total(0);
        MPI_Reduce(&result_local, &total, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

/*
        Vector rightVector(false);
        rightVector.getProps().dataTypeId = DataType::FLOAT;
        rightVector.getProps().globalNRows.setSize(dim_total);
        rightVector.getProps().localNRows .setSize(dim_total);
        rightVector.getProps().rowOffset = 0;
        rightVector.setValues(&ket_wfn[0]);
*/
//      vector<float> Ay_vector(dim_total, 0);
/*
        Vector Ay(false);
        Ay.getProps().dataTypeId = DataType::FLOAT;
        Ay.getProps().globalNRows.setSize(dim_total);
        Ay.getProps().localNRows .setSize(dim_total);
        Ay.getProps().rowOffset = 0;
        Ay.setValues(&Ay_vector[0]);
*/
//		matrix.multiply(rightVector, Ay, MPI_COMM_WORLD);
/*
        Vector leftVector(false);
        leftVector.getProps().dataTypeId = DataType::FLOAT;
        leftVector.getProps().globalNRows.setSize(dim_total);
        leftVector.getProps().localNRows .setSize(dim_total);
        leftVector.getProps().rowOffset = 0;
        leftVector.setValues(&bra_wfn[0]);
*/
//      float total(0);
//      leftVector.dotProduct(Ay, &total, DataType::FLOAT);

        if (my_rank == 0)
        {
            if (runParams.GetOperatorType() == CObservablesRunParameters::kE2)
            { 
                if (i == 0) // first expectation value
                {
                    b0 = HoLength_fm(runParams.hw());
                    cout << "hw = " << runParams.hw() << "\tb0=" << b0 << " [fm]." << endl << endl;

                    cout << "J = ";
                    switch (JJ%2)
                    {
                        case 0: cout << (int)JJ/2; break;
                        case 1: cout << (int)JJ << "/2 ";
                    }
                    cout << endl << endl;

                    cout << "Q(JJ) [e fm^2]" << endl;
                }
                cout << CalculateE2(total, JJ, b0) << endl;
            }
            else if (runParams.GetOperatorType() == CObservablesRunParameters::kM1)
            {
                if (i == 0) // first expectation value
                {
                    cout << "J = ";
                    switch (JJ%2)
                    {
                        case 0: cout << (int)JJ/2; break;
                        case 1: cout << (int)JJ << "/2 ";
                    }
                    cout << endl << endl;

                    cout << "M1(JJ) [mu_N]" << endl;
                }
                cout << CalculateM1(total, JJ) << endl;
            }
            else if (runParams.GetOperatorType() == CObservablesRunParameters::kRMS)
            {
                if (i == 0)
                {
                    b0 = HoLength_fm(runParams.hw());
                    cout << "hw=" << runParams.hw() << "MeV\t\t b0=" << b0 << " [fm]." << endl << endl;

                    cout << "J = ";
                    switch (JJ%2)
                    {
                        case 0: cout << (int)JJ/2; break;
                        case 1: cout << (int)JJ << "/2 ";
                    }
                    cout << endl << endl;

                    cout << "mass rms [fm]" << endl;
                }
                double r2_intr_over_b0sq = total;
                cout << b0*sqrt(r2_intr_over_b0sq/A) << endl;
            }
            else // ==> other operator ==> print result and end
            {
                cout << "Result: " << total << endl;
            }
        }
    }

	MPI_Barrier(MPI_COMM_WORLD);

	if (my_rank == 0)
	{
		cout << "Time: " << std::chrono::duration<double>(std::chrono::system_clock::now() - start).count() << endl;
	}

	MPI_Finalize();
   su3::finalize();
}
