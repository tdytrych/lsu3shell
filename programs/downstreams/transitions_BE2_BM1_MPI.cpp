#include <mpi.h>

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <su3.h>

#include <boost/mpi.hpp>

#include <stdexcept>
#include <sstream>
#include <cmath>
#include <vector>
#include <stack>
#include <ctime>
#include <algorithm> // std::plus

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;


enum OperatorType {kBE2, kBM1};

/* 
	\f$b_{0}=\sqrt{\frac{197.327}{938.92\hbar\Omega}}\f$
*/	
inline double HoLength_fm(double hw)
{
	return sqrt((197.327*197.327)/(938.92*hw)); // hw must be given in MeV; result units: fm
}
/**
    This function calculates
	\f$  \frac{1}{J_{i} + 1}(2J_{f}+1)b_{0}^{4}\left[\frac{1}{b_{0}^{2}}\frac{\lange J||Q_{2}||J\rangle}{\sqrt{2J + 1}}\right]^{2}\f$
*/
double CalculateBE2(double total, SU2::LABEL JJi, SU2::LABEL JJf, double b0)
{
	double b04 = b0*b0*b0*b0;
//  In order to be compatible with MFDn results, I have to omit 1/(2Ji + 1) factor
//	return (5.0/(16.0*M_PI))*b04*(JJf + 1.0)*(total*total);

//	This result may differ from MFDn by factor of 1/(2Ji+1)
	return ((JJf + 1.0)/(JJi+1.0))*(5.0/(16.0*M_PI))*b04*(total*total);
}

/**
    This function calculates
	\f$ \frac{1}{2J_{i} + 1}(2J_{f}+1)[\frac{\lange J||M_{1}||J\rangle}{\sqrt{2J + 1}}\right]^{2} \f$
	in units of \f$\left[\mu^{2}_{N}\right]\f$.
*/
double CalculateBM1(double total, SU2::LABEL JJi, SU2::LABEL JJf)
{
//  In order to be compatible with MFDn results, I have to omit 1/(2Ji + 1) factor
//	return (JJf + 1.0)*(total*total);

//	This result may differ from MFDn by factor of 1/(2Ji+1)
	return ((JJf + 1.0)/(JJi + 1.0))*(total*total);
}



float bra_x_Observable_x_ket(const vector<float>& bra_wfn, const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs, const vector<float>& ket_wfn)
{
	vector<float> observable_x_ket(bra_wfn.size(), 0); // size equal to bra vector 

	assert(vals.size() == row_ptrs.back());
	assert(observable_x_ket.size() == row_ptrs.size() - 1);

	for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow)
	{
		for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival)
		{
			observable_x_ket[irow] += vals[ival]*ket_wfn[column_indices[ival]];
		}
	}

	float dresult = std::inner_product(bra_wfn.begin(), bra_wfn.end(), observable_x_ket.begin(), 0.0);
	return dresult;
}


bool ReadWfnSection(const string& wfn_file_name, lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, const int ndiag, const int isection, vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

// 	this is actually more then we really need ...
	wfn.reserve(nelems); 
	float* wfn_full;
	wfn_full = new float[nelems];
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a file that contains order
//	of basis states for ndiag=1	
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		
//		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn_full[ipos], number_of_states_in_block*sizeof(float));
		}
	}

//	This loop iterates over blocks that belong to isection of basis split into ndiag segments
//	I am using the fact that for isection of the basis split into ndiag segment has the
//	following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
	for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag)
	{
//	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
		ipos = basis.BlockPositionInSegment(ipin_block);
		number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
		wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
	}

//	trim excess memory
	vector<float>(wfn).swap(wfn);

	delete []wfn_full;

	return true;
}

bool LoadInputFile(	const string& input_file_name,
					int& type,
					int& ndiag_bra_wfn,
					int& ndiag_ket_wfn,
					int& ndiag_bra,
					int& ndiag_ket,
					string& bra_model_space_filename,
					string& ket_model_space_filename,
					vector<pair<string, string> >& bra_ket_wfns, float& hw)
{
	boost::mpi::communicator comm_world;

	ifstream input_file(input_file_name.c_str());
	if (!input_file)
	{
		cerr << "Could not open '" << input_file_name << "' input file!" << endl;
		return false;
	}

	std::string operator_type;
	input_file >> operator_type;
	if (operator_type == "BM1")
	{
		type = kBM1; 
	}
	else if (operator_type == "BE2")
	{
		type = kBE2; 
	}
	else
	{	
		type = -1;
	}

	input_file >> ndiag_bra_wfn >> ndiag_ket_wfn >> ndiag_bra >> ndiag_ket;
	int nprocs = comm_world.size();

	if (nprocs != ndiag_bra*ndiag_ket)
	{
		cerr << "Total number of processes must be equal to the product of (#sections in bra space) and (#sections in ket space)!" << endl;
		cerr << "(#sections in bra space) = " << ndiag_bra << endl;
		cerr << "(#sections in ket space) = " << ndiag_ket << endl;
		cerr << "#processes = " << nprocs << endl;
		return false;
	}


	input_file >> bra_model_space_filename >> ket_model_space_filename;
	int num_bra_ket_wfns;
	input_file >> num_bra_ket_wfns;

	bra_ket_wfns.reserve(num_bra_ket_wfns);

	for (int i = 0; i < num_bra_ket_wfns; ++i)
	{
		string sbra_wfn_name;
		string sket_wfn_name;

		input_file >> sbra_wfn_name >> sket_wfn_name;
		bra_ket_wfns.push_back(make_pair(sbra_wfn_name, sket_wfn_name));
	}

	if (type == kBE2)
	{
		if (!input_file)
		{
			std::cerr << "Warning: harmonic oscillator strength is required for BE2 transition, but it is missing. Using hw=1 MeV." << std::endl;
			hw = 1.0;
		}
		else
		{
			input_file >> hw;
		}
	}
	return true;
}

unsigned long CalculateME(	
					const CInteractionPPNN& interactionPPNN,
					const CInteractionPN& interactionPN, // REDUNDANT!!!! TODO: Extract PN part of algorithm into a separate subroutine
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					std::vector<float>& vals, std::vector<size_t>& column_indices, std::vector<size_t>& row_ptr)
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//	These variables are empty. They need to be declared since helper functions calculating matrix elements
//	requires various PN interaction data as an input. TODO: implement PP/NN version of those procedures.
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;
	vector<int> phase_p_pn, phase_n_pn;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;
//////////////////////////////////////////////////////////////////////////////////////////////////////////

	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;

	vector<int> phasePP, phaseNN;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;

	std::vector<MECalculatorData> rmeCoeffsPNPN;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

   uint16_t max_mult_p = bra.getMaximalMultiplicity_p();
   uint16_t max_mult_n = bra.getMaximalMultiplicity_n();

	InitializeIdenticalOperatorRME(identityOperatorRMEPP, max_mult_p*max_mult_p);
	InitializeIdenticalOperatorRME(identityOperatorRMENN, max_mult_n*max_mult_n);

	SingleDistributionSmallVector distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_SMALL_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;

	unsigned long number_nonzero_me(0);
	
	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	uint32_t blockFirstRow(0);
	
	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n;

	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		if (bra.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);

		unsigned long blockFirstColumn(0);

		vector<vector<float> > vals_local(bra.NumberOfStatesInBlock(ipin_block));
		vector<vector<size_t> > col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());
//	loop over (jp, jn) pairs
		for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}

			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			uint32_t currentRow = blockFirstRow;
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				uint32_t currentColumn = blockFirstColumn;
				uint32_t jbegin = ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						Reset_rmeCoeffs(rmeCoeffsPNPN);
						if (in == jn && !rmeCoeffsPP.empty())
						{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
							CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
						}

						if (ip == jp  && !rmeCoeffsNN.empty())
						{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
							CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
						}

						if (!rmeCoeffsPNPN.empty())
						{
							CalculateME_nonDiagonal_nonScalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
						}
					}
					currentColumn += aimax*ket.omega_pn_dim(jwpn);
				}
				currentRow += afmax*bra.omega_pn_dim(iwpn);
			}
			blockFirstColumn += ket.NumberOfStatesInBlock(jpjn_block);
			last_jp = jp;
			last_jn = jn;
		}
		for (size_t irow = 0; irow < vals_local.size(); ++irow)
		{
			vals.insert(vals.end(), vals_local[irow].begin(), vals_local[irow].end());
			column_indices.insert(column_indices.end(), col_ind_local[irow].begin(), col_ind_local[irow].end()); 
			row_ptr.push_back(row_ptr.back() + vals_local[irow].size());
		}
		blockFirstRow += bra.NumberOfStatesInBlock(ipin_block);
	}
	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;

	return number_nonzero_me;
}

int main(int argc,char **argv)
{
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator comm_world;

	int my_rank = comm_world.rank();

	if (argc != 2)
	{
		if (my_rank == 0)
		{
			cout << "Usage: "<< argv[0] <<" <input file name>" << endl;
			cout << "<transition type> - BE2 or BM1" << endl;
			cout << "<filename> with a list of {na nb lm0 mu0 SS0 k0 ll0 jj0}" << endl;
			cout << "<ndiag_bra_wfn> number of sections utilized to produce bra wfns			<ndiag_ket_wfn> number of sections utilized to produce ket wfns" << endl;
			cout << "<ndiag_bra> number of sections in bra space               			<ndiag_ket> number of sections in ket space" << endl;
			cout << "<filename> bra model space               					<filename> ket model space" << endl;
			cout << "<number> of bra-ket pairs" << endl;
			cout << "<filename> bra wfn 											<filename> ket wfn" << endl;
			cout << ". 															." << endl;
			cout << ". 															." << endl;
			cout << ". 															." << endl;
			cout << "<hw value> in the case of E2 transitions" << endl;
         cout << endl;
         cout << "Note:";
         cout << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can set the "
                 "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
              << endl;
		}
		env.abort(-1);
		return EXIT_FAILURE;
	}

	int type;
	float hw;
	proton_neutron::ModelSpace bra_ncsmModelSpace;
	proton_neutron::ModelSpace ket_ncsmModelSpace;
	int ndiag_bra_wfn, ndiag_ket_wfn, ndiag_bra, ndiag_ket;
	vector<pair<string, string> > bra_ket_wfns;
	string bra_model_space_filename; 
	string ket_model_space_filename; 

	if (my_rank == 0)
	{
		bool success = LoadInputFile(argv[1], type, ndiag_bra_wfn, ndiag_ket_wfn, ndiag_bra, ndiag_ket, bra_model_space_filename, ket_model_space_filename, bra_ket_wfns, hw);
		if (!success)
		{
			env.abort(-1);
			return EXIT_FAILURE;
		}
		bra_ncsmModelSpace.Load(bra_model_space_filename);
		ket_ncsmModelSpace.Load(ket_model_space_filename);
	}

	boost::mpi::broadcast(comm_world, bra_ncsmModelSpace, 0);
	boost::mpi::broadcast(comm_world, ket_ncsmModelSpace, 0);

	SU2::LABEL JJf = bra_ncsmModelSpace.JJ();
	SU2::LABEL JJi = ket_ncsmModelSpace.JJ();

	boost::mpi::broadcast(comm_world, ndiag_bra_wfn, 0);
	boost::mpi::broadcast(comm_world, ndiag_ket_wfn, 0);
	boost::mpi::broadcast(comm_world, ndiag_bra, 0);
	boost::mpi::broadcast(comm_world, ndiag_ket, 0);
	boost::mpi::broadcast(comm_world, bra_ket_wfns, 0);
	boost::mpi::broadcast(comm_world, type, 0);
	boost::mpi::broadcast(comm_world, hw, 0);

	vector<vector<float> > wfns_bra(bra_ket_wfns.size());
	vector<vector<float> > wfns_ket(bra_ket_wfns.size());

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

	std::ofstream output_file("/dev/null");
	bool allow_generate_missing_rme_files = (comm_world.size() == 1);

	int JJbra = bra_ncsmModelSpace.JJ();
	int JJket = ket_ncsmModelSpace.JJ();


	lsu3::CncsmSU3xSU2Basis bra_basis(bra_ncsmModelSpace, 0, 1);
	lsu3::CncsmSU3xSU2Basis ket_basis(ket_ncsmModelSpace, 0, 1);


//	assert(bra_basis.Nmax() == ket_basis.Nmax());

	CBaseSU3Irreps baseSU3Irreps(bra_ncsmModelSpace.Z(), bra_ncsmModelSpace.N(), std::max(bra_basis.Nmax(), ket_basis.Nmax())); 

	uint16_t my_row, my_col;
	my_row = my_rank / ndiag_ket;
	my_col = my_rank % ndiag_ket;

	MPI_Comm ROW_COMM, COL_COMM;
	MPI_Comm_split(comm_world, my_row, my_col, &ROW_COMM);	// processes in row_comm share my_row value
	MPI_Comm_split(comm_world, my_col, my_row, &COL_COMM);	// processes in col_comm share my_col value

	boost::mpi::communicator row_comm(ROW_COMM, boost::mpi::comm_take_ownership); 
	boost::mpi::communicator col_comm(COL_COMM, boost::mpi::comm_take_ownership);
	int my_row_rank, my_col_rank;

	my_row_rank = row_comm.rank();
	my_col_rank = col_comm.rank();

	if (my_row_rank == 0)	// I am the root in a set of processes that have the same value of my_row
	{
		bool ok;
		for (int i = 0; i < bra_ket_wfns.size(); ++i)
		{
			ok = ReadWfnSection(bra_ket_wfns[i].first, bra_basis, ndiag_bra_wfn, ndiag_bra, my_row, wfns_bra[i]);
			if (!ok)
			{
				env.abort(-1);
				return EXIT_FAILURE;
			}
		}
	}
	if (my_col_rank == 0)
	{
		bool ok;
		for (int i = 0; i < bra_ket_wfns.size(); ++i)
		{
			ok = ReadWfnSection(bra_ket_wfns[i].second, ket_basis, ndiag_ket_wfn, ndiag_ket, my_col, wfns_ket[i]);
			if (!ok)
			{
				env.abort(-1);
				return EXIT_FAILURE;
			}
		}
	}

	boost::mpi::broadcast(row_comm, wfns_bra, 0);
	boost::mpi::broadcast(col_comm, wfns_ket, 0);

	bra_basis.Reshuffle(bra_ncsmModelSpace, my_row, ndiag_bra);
	ket_basis.Reshuffle(ket_ncsmModelSpace, my_col, ndiag_ket);

#ifndef NDEBUG
//TODO: rewrite using for_each(....)
	for (int i = 0; i < wfns_bra.size(); ++i)
	{
		if (bra_basis.dim() != wfns_bra[i].size())
		{
			cerr << "Size of wfns_bra[" << i << "] != dim of bra basis" << endl;
			env.abort(-1);
			return EXIT_FAILURE;
		}
		if (ket_basis.dim() != wfns_ket[i].size())
		{
			cerr << "Size of wfns_ket[" << i << "] != dim of bra basis" << endl;
			env.abort(-1);
			return EXIT_FAILURE;
		}
	}
#endif

   su3::init();
#ifdef SU3_9LM_HASHINDEXEDARRAY
   CWig9lmLookUpTable<float>::initialize();
#endif


	CInteractionPPNN interactionPPNN(baseSU3Irreps, true, output_file);
// 	TODO: PN should be removed. It is redundant but for the sake of fast implementation I am keeping it here. 
	CInteractionPN interactionPN(baseSU3Irreps, allow_generate_missing_rme_files, true, output_file);
	try
	{
		string sOperatorFile(su3shell_data_directory);
		COperatorLoader operatorLoader;
		if (type == kBE2)
		{ 
			sOperatorFile += "/SU3_Interactions_Operators/Q2/Q2_1b_nmax12";
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
		else if (type == kBM1)
		{
			sOperatorFile += "/SU3_Interactions_Operators/M1/M1_1b_nmax12"; 
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
		else
		{
			sOperatorFile += "/SU3_Interactions_Operators/LS/LS_1b_nmax8"; 
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
/** J0 and M0 labels of the operator are stored as: MECalculatorData::JJ0_ and  MECalculatorData::MM0_ 
 	and are being read from one-body interaction files ...
 */
		operatorLoader.Load(my_rank, interactionPPNN, interactionPN, false);
	}
	catch (const std::logic_error& e) 
	{	 
   		std::cerr << e.what() << std::endl;
		env.abort(-1);
		return EXIT_FAILURE;
   	}

	if (!SU2::mult(JJi, MECalculatorData::JJ0(), JJf))
	{
		if (my_rank == 0)
		{
			cout << "Operator JJ0:" << (int)MECalculatorData::JJ0() << " ==> could not couple ket with JJi:" << (int)JJi << " with bra carrying JJf:" << (int)JJf << endl << endl;
		}
        MPI_Abort(MPI_COMM_WORLD, -1);
	}

//		The order of coefficients is given as follows:
// 		index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//		TransformTensorStrengthsIntoPP_NN_structure turns that into:
// 		index = type*k0max*rho0max + k0*rho0max + rho0
	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

	vector<float> vals;	
	vector<size_t> column_indices;
	vector<size_t> row_ptrs;
	row_ptrs.push_back(0);

	CalculateME(interactionPPNN, interactionPN, bra_basis, ket_basis, vals, column_indices, row_ptrs);

	for (size_t i = 0; i < bra_ket_wfns.size(); ++i)
	{
		float result_local = bra_x_Observable_x_ket(wfns_bra[i], vals, column_indices, row_ptrs, wfns_ket[i]);
		float total(0);

		boost::mpi::reduce(comm_world, result_local, total, std::plus<float>(), 0);

		if (my_rank == 0)
		{
			if (type == kBE2)
			{ 
				float b0 = HoLength_fm(hw);
				if (i == 0) // first expectation value
				{
					cout << "hw = " << hw << "\tb0=" << b0 << " [fm]." << endl << endl;
					cout << "Jf = ";
					switch (JJf%2)
					{
						case 0: cout << (int)JJf/2; break;
						case 1: cout << (int)JJf << "/2 ";
					}
					cout << "\t" << "Ji = ";
					switch (JJi%2)
					{
						case 0: cout << (int)JJi/2; break;
						case 1: cout << (int)JJi << "/2 ";
					}
					cout << endl << endl;
					cout << "Jf file \t\t Ji file \t B(E2; Ji --> Jf) [e^2 fm^4]" << endl;
				}
				cout << bra_ket_wfns[i].first << "\t" << bra_ket_wfns[i].second << "\t" << CalculateBE2(total, JJi, JJf, b0) << endl;
			}
			else if (type == kBM1)
			{
				if (i == 0) // first expectation value
				{
					cout << "Jf = ";
					switch (JJf%2)
					{
						case 0: cout << (int)JJf/2; break;
						case 1: cout << (int)JJf << "/2 ";
					}
					cout << "\t" << "Ji = ";
					switch (JJi%2)
					{
						case 0: cout << (int)JJi/2; break;
						case 1: cout << (int)JJi << "/2 ";
					}
					cout << endl << endl;

					cout << "Jf file \t Ji file \t B(M1; Ji --> Jf) [mu_{N}^2]" << endl;
				}
				cout << bra_ket_wfns[i].first << "\t" << bra_ket_wfns[i].second << "\t" << CalculateBM1(total, JJi, JJf) << endl;
			}
			else
			{
				cout << bra_ket_wfns[i].first << "\t" << bra_ket_wfns[i].second << "\t" << total << endl;
			}
		}
	}
   su3::finalize();
#ifdef SU3_9LM_HASHINDEXEDARRAY
        CWig9lmLookUpTable<float>::finalize();
#endif
}
