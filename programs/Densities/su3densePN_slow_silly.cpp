#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>
#include <su3dense/tensor_read.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

float bra_x_Observable_x_ket_per_thread(
    const lsu3::CncsmSU3xSU2Basis& bra, const std::vector<float>& bra_wfn,
    const std::vector<float>& ket_wfn,
    // data structures describing sparse submatrix on CSR format
    const std::vector<float>& vals, const std::vector<size_t>& column_indices,
    const std::vector<size_t>& row_ptrs,
    // rows are organized into groups of (ip in) states. For each block we store its size and
    // starting position in basis.
    // ipin_block_ids holds indices of non vanishing row blocks stored in CSR structure
    const std::vector<uint32_t> ipin_block_ids) {
   // If matrix is empty ==> return 0.0
   if (vals.empty()) {
      assert(row_ptrs.empty());
      assert(column_indices.empty());
      return 0.0;
   }
   std::vector<float> observable_x_ket(row_ptrs.size() - 1, 0);

   assert(vals.size() == row_ptrs.back());
   assert(observable_x_ket.size() == row_ptrs.size() - 1);

   for (int irow = 0; irow < row_ptrs.size() - 1; ++irow) {
      for (int ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival) {
         observable_x_ket[irow] += vals[ival] * ket_wfn[column_indices[ival]];
      }
   }

   float dresult = 0.0;
   int row0;
   int nrows;
   int i = 0;
   for (int iblock = 0; iblock < ipin_block_ids.size(); ++iblock) {
      row0 = bra.BlockPositionInSegment(ipin_block_ids[iblock]);
      nrows = bra.NumberOfStatesInBlock(ipin_block_ids[iblock]);
      for (int irow = 0; irow < nrows; ++irow, ++i) {
         dresult += bra_wfn[row0 + irow] * observable_x_ket[i];
      }
   }
   return dresult;
}

void ReadWfn(lsu3::CncsmSU3xSU2Basis& basis, const std::string& wfn_filename,
             std::vector<float>& wfn) {
   std::fstream wfn_file(wfn_filename.c_str(),
                         std::ios::in | std::ios::binary |
                             std::ios::ate);  // open at the end of file so we can get file size
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_filename << "' wfn file" << endl;
      exit(EXIT_FAILURE);
   }

   size_t size = wfn_file.tellg();
   size_t nelems = size / sizeof(float);

   if (size % sizeof(float) || (nelems != basis.getModelSpaceDim())) {
      cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x "
           << sizeof(float) << " = " << basis.getModelSpaceDim() * sizeof(float) << " bytes."
           << endl;
      cout << "The actual size of the file: " << size << " bytes!";
      exit(EXIT_FAILURE);
   }

   wfn.resize(nelems);
   wfn_file.seekg(0, std::ios::beg);
   wfn_file.read((char*)wfn.data(), wfn.size() * sizeof(float));
}

unsigned long ComputeMatrixElements(lsu3::CncsmSU3xSU2Basis& bra, lsu3::CncsmSU3xSU2Basis& ket,
                                    const IJ_RMES& prot_rmes_table, const IJ_RMES& neut_rmes_table,
                                    const SU3xSU2::LABELS& protTensorLabels,
                                    const SU3xSU2::LABELS& neutTensorLabels,
                                    const SU3xSU2::LABELS& tensorLabels, int a0, int k0, int L0,
                                    std::vector<float>& vals, std::vector<size_t>& column_indices,
                                    std::vector<size_t>& row_ptrs,
                                    std::vector<uint32_t>& ipin_block_ids) {
   uint32_t LL0 = 2 * L0;
   // definition: libraries/LookUpContainers/WigEckSU3SO3CGTable.h line 134
   WigEckSU3SO3CGTable su3so3cgTable(tensorLabels, LL0);
   unsigned long number_nonzero_me(0);
   int k0max = SU3::kmax(tensorLabels, L0);
   int a0max = tensorLabels.rho;
   // vector of tensor coefficients for all k0 < k0max and a0 < a0max,
   // ordered as [k0][a0] (see libraries/SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJ.fix line)
   std::vector<TENSOR_STRENGTH> coeffs(k0max * a0max, 0.0);
   // IMPORTANT TRICK:
   // We will supply this vector of coefficients into
   // CalculateME_nonDiagonal_nonScalar to get non-zero matrix only for a given
   // a0 and k0 component of a tensor. This allows me to use existing method
   // which is meant to be applied for computations of matrix elements of a realistic
   // Hamiltonian.
   // NOTE: This is silly as one should carry out computation for all allowed a0 and k0 components
   // of tensor
   coeffs[k0 * a0max + a0] = 1.0;

   const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
   const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

   for (uint32_t ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      if (bra.NumberOfStatesInBlock(ipin_block) == 0) {
         continue;
      }
      bool vanishingBlock = true;
      uint32_t blockFirstRow = bra.BlockPositionInSegment(ipin_block);

      uint32_t ip = bra.getProtonIrrepId(ipin_block);
      uint32_t in = bra.getNeutronIrrepId(ipin_block);

      SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

      uint16_t aip_max = bra.getMult_p(ip);
      uint16_t ain_max = bra.getMult_n(in);

      unsigned long blockFirstColumn(0);

      std::vector<std::vector<float>> vals_local(bra.NumberOfStatesInBlock(ipin_block));
      std::vector<std::vector<size_t>> col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         if (ket.NumberOfStatesInBlock(jpjn_block) == 0) {
            continue;
         }
         uint32_t jp = ket.getProtonIrrepId(jpjn_block);
         uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

         auto ipjp_rmes = prot_rmes_table.find(std::make_pair(ip, jp));
         if (ipjp_rmes == prot_rmes_table.end()) {
            continue;
         }
         auto injn_rmes = neut_rmes_table.find(std::make_pair(in, jn));
         if (injn_rmes == neut_rmes_table.end()) {
            continue;
         }

         uint32_t blockFirstColumn = ket.BlockPositionInSegment(jpjn_block);

         uint16_t ajp_max = ket.getMult_p(jp);
         uint16_t ajn_max = ket.getMult_n(jn);

         SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
         SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

         // note that protTensorLabels.rho contains maximal multiplicity a0p
         SU3xSU2::RME prot_rme(aip_max, w_ip, protTensorLabels.rho, protTensorLabels, ajp_max, w_jp,
                               ipjp_rmes->second);
         // note that neutTensorLabels.rho contains maximal multiplicity a0n
         SU3xSU2::RME neut_rme(ain_max, w_in, neutTensorLabels.rho, neutTensorLabels, ajn_max, w_jn,
                               injn_rmes->second);

         //	loop over wpn that result from coupling ip x in
         uint32_t ibegin = bra.blockBegin(ipin_block);
         uint32_t iend = bra.blockEnd(ipin_block);
         uint32_t currentRow = blockFirstRow;
         for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn) {
            SU3xSU2::LABELS wpn_i(bra.getOmega_pn(ip, in, iwpn));
            size_t afmax = aip_max * ain_max * wpn_i.rho;
            SU3xSU2::BasisJfixed braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));

            uint32_t currentColumn = blockFirstColumn;
            uint32_t jbegin = ket.blockBegin(jpjn_block);
            uint32_t jend = ket.blockEnd(jpjn_block);
            for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn) {
               SU3xSU2::LABELS wpn_j(ket.getOmega_pn(jp, jn, jwpn));
               size_t aimax = ajp_max * ajn_max * wpn_j.rho;
               SU3xSU2::BasisJfixed ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));

               if (SU2::mult(wpn_j.S2, tensorLabels.S2, wpn_i.S2) &&
                   SU3::mult(wpn_j, tensorLabels, wpn_i)) {
                  vanishingBlock = false;
                  ///////////////////////////////////////////////////
                  // ERROR!!! ==> I forgot to compute the phase !!!!
                  ///////////////////////////////////////////////////
                  // int phase = ... ???
                  ///////////////////////////////////////////////////
                  SU3xSU2::RME total_rme(wpn_i, tensorLabels, wpn_j, &prot_rme, &neut_rme, NULL);
                  // Obtain <(lmi mui) *; (lm0 mu0) * L0 || (lmf muf) *>_{*}
                  WigEckSU3SO3CG* pWigEckSU3SO3CG = su3so3cgTable.GetWigEckSU3SO3CG(wpn_i, wpn_j);
                  // MECalculatorData represent components of a tensor
                  std::vector<MECalculatorData> rmeCoeffsPNPN(
                      1, MECalculatorData(&total_rme, pWigEckSU3SO3CG, coeffs.data(), LL0));
                  CalculateME_nonDiagonal_nonScalar(
                      (currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax,
                      ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
                  delete[] total_rme.m_rme;
               }
               currentColumn += aimax * ket.omega_pn_dim(jwpn);
            }  // wpn_j
            currentRow += afmax * bra.omega_pn_dim(iwpn);
         }  // wpn_i
         delete[] prot_rme.m_rme;
         delete[] neut_rme.m_rme;
      }
      if (!vanishingBlock) {
         // Store index of ipin block of bra basis states whose matrix elements we are going to
         // store into CSR structure
         ipin_block_ids.push_back(ipin_block);
         for (size_t irow = 0; irow < vals_local.size(); ++irow) {
            vals.insert(vals.end(), vals_local[irow].begin(), vals_local[irow].end());
            row_ptrs.push_back(vals.size());
            column_indices.insert(column_indices.end(), col_ind_local[irow].begin(),
                                  col_ind_local[irow].end());
            number_nonzero_me += vals_local[irow].size();
         }
      }
      else
      {
         ipin_block_ids.push_back(ipin_block);
         for (size_t irow = 0; irow < vals_local.size(); ++irow) {
            row_ptrs.push_back(vals.size());
         }
      }
   }
   return number_nonzero_me;
}

int main(int argc, char** argv) {
   if (argc != 14) {
      std::cerr << "Usage: " << argv[0]
                << " <bra model space> <bra wfn> <ket model space> <ket wfn> <proton rmes> "
                   "<neutron rmes> <a0> <lm0> <mu0> <2S0> <k0> <L0> <2J0>"
                << std::endl;
      std::cerr << "to compute <bra wfn || [P x N]^a0 (lm0 mu0) k0 (S0 L0) J0 || ket wfn>, "
                   "where bra/ket wfn spans model space given by file \"<bra/ket model space>\""
                << std::endl;
      std::cerr << "NOTE: wave functions must be given in ndiag:1 order!\n" << std::endl;
      std::cerr << "NOTE: Generally, a0 = a0p*a0n_max*rho0max + a0n*rho0max + rho0, where a0pmax "
                   "and a0nmax are maximal multiplicities of proton and neutron tensors."
                << std::endl;
      std::cerr << "      In most applications, a0pmax=1 and a0nmax=1 and one has a0=rho0."
                << std::endl;
      std::cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one "
                   "can set the "
                   "size of look-up tables for 9lm, u6lm, z6lm, \"frequent\" 9lm symbols."
                << std::endl;
      return EXIT_FAILURE;
   }
   std::string bra_model_space_filename = argv[1];
   std::string bra_wfn_filename = argv[2];
   std::string ket_model_space_filename = argv[3];
   std::string ket_wfn_filename = argv[4];
   std::string prot_rmes_filename = argv[5];
   std::string neut_rmes_filename = argv[6];
   int a0 = atoi(argv[7]);
   int lm0 = atoi(argv[8]);
   int mu0 = atoi(argv[9]);
   int SS0 = atoi(argv[10]);
   int k0 = atoi(argv[11]);
   int L0 = atoi(argv[12]);
   int JJ0 = atoi(argv[13]);

   if (!SU2::mult(SS0, 2 * L0, JJ0)) {
      std::cerr << "SS0: " << SS0 << " LL0:" << 2 * L0 << " can not couple to JJ0:" << JJ0
                << std::endl;
      return EXIT_FAILURE;
   }

   // We need to change it according to the input parameter JJ0
   // MECalculatorData::JJ0_ ... needed for Wig-Eck theorem
   MECalculatorData::JJ0_ = JJ0;

   proton_neutron::ModelSpace bra_model_space, ket_model_space;
   bra_model_space.Load(bra_model_space_filename);
   ket_model_space.Load(ket_model_space_filename);

   lsu3::CncsmSU3xSU2Basis ket, bra;
   // NOTE: basis sets are ordered according to ndiag:1. Therefore, input wave functions
   // must be given in this order as well.
   bra.ConstructBasis(bra_model_space, 0, 1);
   ket.ConstructBasis(ket_model_space, 0, 1);

   // Jket x J0 --> ? Jbra
   if (!SU2::mult(ket.JJ(), JJ0, bra.JJ())) {
      std::cerr << "ket 2J:" << (int)ket.JJ() << " coupled with tensor 2J0:" << JJ0
                << " does not yeld bra 2J:" << bra.JJ() << std::endl;
      return EXIT_FAILURE;
   }

   SU3xSU2::LABELS protTensorLabels, neutTensorLabels;
   // rmes are stored in hash tables that holds:
   // <ip,jp> ---> std::vector{< aip ip ||| a+/ta ||| ajp jp>_rho}
   IJ_RMES prot_rmes_table, neut_rmes_table;

   uint32_t Zf, Zi, NmaxP;
   // a tensor file contains Af, Ai, Nmax as a header --> Zf Zi NmaxP
   LoadRMEsIJ_RMES(prot_rmes_filename, Zf, Zi, NmaxP, protTensorLabels, prot_rmes_table);
   std::cout << "Proton tensor:" << (int)protTensorLabels.rho << "(" << (int)protTensorLabels.lm
             << " " << (int)protTensorLabels.mu << ") 2S0:" << (int)protTensorLabels.S2
             << std::endl;
   std::cout << "Proton tensor file '" << prot_rmes_filename << "' contains rmes for Zf:" << Zf
             << " Zi:" << Zi << " Nmax:" << NmaxP << std::endl;
   // Make sure the input rme file is appropriate for given bra and ket model spaces
   if (Zf != bra.NProtons() || Zi != ket.NProtons() || NmaxP != bra.Nmax()) {
      std::cerr << "Error: given model spaces need proton tensor for Zf:" << bra.NProtons()
                << " Zi:" << ket.NProtons() << " Nmax:" << bra.Nmax() << std::endl;
      return EXIT_FAILURE;
   }
   //   ShowRMEsTableIJ_RMES(prot_rmes_table);

   uint32_t Nf, Ni, NmaxN;
   // a tensor file contains Af, Ai, Nmax as a header --> Nf Ni NmaxN
   LoadRMEsIJ_RMES(neut_rmes_filename, Nf, Ni, NmaxN, neutTensorLabels, neut_rmes_table);
   std::cout << "Neutron tensor:" << (int)neutTensorLabels.rho << "(" << (int)neutTensorLabels.lm
             << " " << (int)neutTensorLabels.mu << ") 2S0:" << (int)neutTensorLabels.S2
             << std::endl;
   std::cout << "Neutron tensor file '" << neut_rmes_filename << "' contains rmes for Nf:" << Nf
             << " Ni:" << Ni << " Nmax:" << NmaxN << std::endl;
   // Make sure the input rme file matches our need given the input pair of bra and ket model spaces
   if (Nf != bra.NNeutrons() || Ni != ket.NNeutrons() || NmaxN != bra.Nmax()) {
      std::cerr << "ERROR: given model spaces need neutron tensor for Nf:" << bra.NNeutrons()
                << " Zi:" << ket.NNeutrons() << " Nmax:" << bra.Nmax() << "!" << std::endl;
      return EXIT_FAILURE;
   }
   //  ShowRMEsTableIJ_RMES(neut_rmes_table);

   int rho0max = SU3::mult(protTensorLabels, neutTensorLabels, SU3::LABELS(lm0, mu0));
   if (!rho0max) {
      std::cerr << "Proton tensor SU(3) coupled with neutron SU(3) does not couple to (" << lm0
                << " " << mu0 << ")" << std::endl;
      return EXIT_FAILURE;
   }

   // Let's suppose proton/neutron tensors may have multiplitities > 1
   // In such case one computes a0max = ap_max * an_max * rho0max tensor SU(3) multiplicities
   int a0max = protTensorLabels.rho * neutTensorLabels.rho * rho0max;
   if (a0 >= a0max) {
      std::cerr << "Error a0:" << a0 << " >= a0max:" << a0max << std::endl;
      return EXIT_FAILURE;
   }
   SU3xSU2::LABELS tensorLabels(a0max, lm0, mu0, SS0);

   // S0p x S0n --> ? S0
   if (!SU2::mult(protTensorLabels.S2, neutTensorLabels.S2, SS0)) {
      std::cerr << "Error 2Sp:" << (int)protTensorLabels.S2 << " x 2Sn:" << neutTensorLabels.S2
                << " does not couple to 2S0:" << (int)SS0 << std::endl;
      return EXIT_FAILURE;
   }

   int k0max = SU3::kmax(tensorLabels, L0);

   if (!k0max) {
      std::cerr << "A given tensor does not contain given L0:" << L0 << std::endl;
      return EXIT_FAILURE;
   }
   if (k0 >= k0max) {
      std::cerr << "Error k0:" << k0 << " >= k0max:" << k0max << "!" << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Computing obds for PN tensor with "
             << "(" << lm0 << " " << mu0 << ") 2S0:" << SS0
             << " SU(3)xSU(2) character and a0:" << a0 << " k0:" << k0 << " L0:" << L0
             << " JJ0:" << JJ0 << " tensorial component" << std::endl;

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   std::vector<float> bra_wfn;
//   bra_wfn.resize(bra.getModelSpaceDim(), 1.0);
   ReadWfn(bra, bra_wfn_filename, bra_wfn);

   std::vector<float> ket_wfn;
//   ket_wfn.resize(ket.getModelSpaceDim(), 1.0);
   ReadWfn(ket, ket_wfn_filename, ket_wfn);

   std::vector<float> vals;
   std::vector<size_t> column_indices;
   std::vector<size_t> row_ptrs;
   row_ptrs.push_back(0);
   // CSR data structure stored in vals, column_indices, row_ptrs describes a submatrix defined by
   // set of (wp x wn) bra basis configurations
   // identification number of wp x wn block of states will be stored in ipin_block_ids
   std::vector<uint32_t> ipin_block_ids;

   su3::init();
#ifdef SU3_9LM_HASHINDEXEDARRAY
   CWig9lmLookUpTable<float>::initialize();
#endif
   ComputeMatrixElements(bra, ket, prot_rmes_table, neut_rmes_table, protTensorLabels,
                         neutTensorLabels, tensorLabels, a0, k0, L0, vals, column_indices, row_ptrs,
                         ipin_block_ids);
   su3::finalize();
#ifdef SU3_9LM_HASHINDEXEDARRAY
   CWig9lmLookUpTable<float>::finalize();
#endif

/*   
   std::cout << "nme: " << vals.size() << std::endl;
   std::cout << "Matrix elements:" << std::endl;
   assert((bra.getModelSpaceDim() + 1) == row_ptrs.size());
   size_t index = 0;
   for (int irow = 0; irow < bra.getModelSpaceDim(); ++irow)
   {
      for (int i = row_ptrs[irow]; i < row_ptrs[irow + 1]; ++i, ++index)
      {
         std::cout << irow << " " << column_indices[index] << " " << vals[index] << std::endl;
      }
   }
*/   
   float result_thread = bra_x_Observable_x_ket_per_thread(
       bra, bra_wfn, ket_wfn, vals, column_indices, row_ptrs, ipin_block_ids);

   // Compute phase arising due to the SU(3)-rme reduction formula for proton and neutron system.
   boost::container::small_vector<unsigned char, 2> ZNbra = {(unsigned char)bra.NProtons(),
                                                             (unsigned char)bra.NNeutrons()};
   boost::container::small_vector<unsigned char, 2> ZNket = {(unsigned char)ket.NProtons(),
                                                             (unsigned char)ket.NNeutrons()};
   int G1, G2;
   std::cout << "Enter the total number of proton creational and annihilational operators in the "
                "input proton tensor '"
             << prot_rmes_filename << "'." << std::endl;
   // Example: a+p --> 1;  {a+_p x ta_p} --> 2;  {a+_p x a+_p} --> 2 etc ...";
   std::cin >> G1;
   std::cout << "Enter the total number of neutron creational and annihilational operators in the "
                "input neutron tensor '"
             << neut_rmes_filename << "'." << std::endl;
   // Example: tar_p --> 1;  {a+_n x ta_n} --> 2;  {a+_n x a+_n} --> 2 etc ...";
   std::cin >> G2;
   std::vector<unsigned char> tensor = {(unsigned char)G1, (unsigned char)G2};
   int phasePN = rme_uncoupling_phase(ZNbra, tensor, ZNket);

   std::cout << "Result: " << result_thread * phasePN << std::endl;

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();  // clears memory allocated for U9, U6, and Z6
                                                      // coefficients
   //   CSSTensorRMELookUpTablesContainer::ReleaseMemory();  // clear memory allocated for
   //   single-shell
   // SU(3) rmes
   //   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory allocated for
   //   SU(3)>SO(3)

   return EXIT_SUCCESS;
}
