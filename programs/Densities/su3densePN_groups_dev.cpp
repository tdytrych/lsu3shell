#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <LookUpContainers/WigEckSU3SO3CGTable.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>
#include <su3dense/tensor_read.h>
#include <su3dense/CWig9JLookUpTables.h>
#include <SU3NCSMUtils/clebschGordan.h> // CWig9jLookUpTable<double>

#include <su3.h>

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <unordered_map>

#include <boost/functional/hash.hpp>
#include <boost/multi_array.hpp>

#include <sys/resource.h>

#include "su3densePN_helpers.h"

// bash color prefixes:

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[1;36m");
const std::string magenta("\033[1;35m");
const std::string reset("\033[0m");

void reportMaxRSS(const std::string& phase)
{
   struct rusage rus;
   getrusage(RUSAGE_SELF, &rus);
   double max_rss_gb = (double)rus.ru_maxrss / 1024.0 / 1024.0;

   std::cout << yellow
      << "MaxRSS [" << phase << "] : " << max_rss_gb << " [GiB]" << reset << std::endl;
}

using clock_type = std::chrono::high_resolution_clock;
using duration_type = std::chrono::duration<double>;

// wig9j_cached[iL0][iSf][iSi] unique_ptr to --> LookUpTable(Lf, Li) --> {\Pi_{JiJ0fSf} * 9j{Li Si Ji}{L0 S0 J0}{Lf Sf Jf}}[iJJ0]
// where, generally, the value associated to (Lf, Li) is array of coefficients for all J0, such that Ji x J0 -> Jf.
// in our case we have only a single J0 value and hence 
// wig0j_cached[iL0][iSf][iSi] --> LookUpTable(Lf, Li) --> {Pi*Wig9 for J0}
Wigner9JCache wig9j_cached;
// Implicit sizes for maximal number of U6-lm and Z6-lm coefficients
int max_num_u6lm = 2500000;
int max_num_z6lm = 2500000;
// This class computes SU(3) 9-lm coefficients using hashed Z6-lm and U6-lm coefficients.
// SU(3) 9-lm are not hashed. This is not reuired as for each {wp wp'}{wn wn'} block
// we generate unique set of 9-lm symbols.
CSU39lm<double> su39lmComputer;

// vector with SU(3) equivalent rmes 
// in case of protons: {(ip, jp, {<ip|||P|||jp>}), (ip',jp', {<ip'|||P|||jp'>}), ... } 
// in case of neutrons: {(in, jn, {<in|||N|||jn>}), (in',jn', {<in'|||N|||jn'>}), ... } 
using EQV_RMES = std::vector<std::tuple<uint32_t, uint32_t, std::vector<float>>>;

// RME_BLOCKS: block of SU(3) equivalent rmes
// <(lm mu), (lm' mu)'> --> {(i, j, {<i||| O ||| j>), (i',j', {<i' ||| O ||| j'>}), ... }
// where i/j denotes index associated with a set of quantum numbers that characterize SU(3)xSU(2)
// irreps that span Hilber space of A identical nucleons. All i and j' irreps are SU(3) equivalent,
// that is, SU(3)[i]=const=(lm mu) and SU(3)[j]=const.=(lm' mu')
using RME_BLOCKS = std::map<std::pair<SU3::LABELS, SU3::LABELS>, EQV_RMES>;

std::vector<SU3::LABELS> CreateListofSU3Labels(const lsu3::CncsmSU3xSU2Basis basis) {
   std::set<SU3::LABELS> unique_su3;
   for (int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++) {
      if (!basis.NumberOfStatesInBlock(ipin_block)) {
         continue;
      }
      uint32_t ip = basis.getProtonIrrepId(ipin_block);
      uint32_t in = basis.getNeutronIrrepId(ipin_block);
      for (int iwpn = basis.blockBegin(ipin_block); iwpn < basis.blockEnd(ipin_block); ++iwpn) {
         SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
         unique_su3.insert(omega_pn);
      }
   }
   return std::vector<SU3::LABELS>(unique_su3.begin(), unique_su3.end());
}

using IPIN = std::pair<uint32_t, uint32_t>;
using JPJN = std::pair<uint32_t, uint32_t>;

// structure that holds quantum numbers of a general proton-neutron operator tensors:
// [P^{aGp(lmGp muGp)SGp x N^{aGn(lmGn muGn)SGn}]^{rho0(lm0 mu0)S0}_L0J0
struct OperatorTensorLabels
{
   int aGpmax, lmGp, muGp;
   int aGnmax, lmGn, muGn;
   int rho0max, lm0, mu0;
   int SSGp, SSGn, SS0;
   int L0, JJ0;
};

// basis(ip in) --> ipin_blockid ... this in turn can be later used to
// obtain all quantum numbers of generating proton and neutron irreps
void CreateHashIJ_blockID(const lsu3::CncsmSU3xSU2Basis& basis, std::unordered_map<IPIN, uint32_t, boost::hash<IPIN>>& hash_ipin_blockID)
{
   const uint32_t number_ipin_blocks = basis.NumberOfBlocks();
   // iterate over (ip in) blocks of states
   for (uint32_t ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      // if a given block of states has an empty basis => skip block
      if (basis.NumberOfStatesInBlock(ipin_block) == 0) {
         continue;
      }
      uint32_t ip = basis.getProtonIrrepId(ipin_block);
      uint32_t in = basis.getNeutronIrrepId(ipin_block);
      hash_ipin_blockID[std::make_pair(ip, in)] = ipin_block;
   }
}

// This struct computes and stores SU(3) 9-lm 
// {wp'  wGp  wp}rhop
// {wn'  wGn  wn}rhon
// {wi   w0   wf}whot
// rhoi rho0 rhof 
// 
// It can also use this set of SU(3) 9-lm to compute and store
// proton-neutron SU(3) rmes and SU(3)SO3 matrix elements
//
// Initialization: Compute 9-lm coefficients that will be needed 
//
// ComputePNRMEs: compute proton-neutron SU(3) reduced matrix elements
// <ai (gip gin) wf ||| P x N ||| ai (gjp gjn) wi>rhot
//
// ComputeSU3SO3ME: compute SU3SO3 matrix elements
// <af (gip gin) wf kf Lf || P x N^{a0w0S0}_{k0L0} || ai (gjp gin) wi ki Li>
struct SU39LM_PNMeComputer
{
   // su39lm[rhot][rho0][rhoi][rhof][rhon][rhop]:
   std::vector<double> su39lm_;
   // {wp_ket  wGp   wp_bra} rhop
   // {wn_ket  wGn   wn_bra} rhon
   // {wi      w0    wf    } rhot
   // rhoi    rho0  rhof
   int rhotmax_, rhoimax_, rhofmax_, rho0max_, rhopmax_, rhonmax_;

   // pnrmes_: current proton-neutron rmes
   // <aip ain (gip gin) rhof wf ||| P x N^{a0w0S0} ||| ajp ajn (gjp gjn) rhoi wi>rhot[ai][af][a0][rhot]
   // af: [aip][ain][rhof]
   // ai: [ajp][ajn][rhoi]
   std::vector<double> pnrmes_;
   // lfli_me_: {Lf, Li} --> {<af (gip gin) wf kf Lf || P x N^{a0w0S0}_{k0L0} || ai (gjp gjn) wi ki Li>[af][kf][ai][ki][k0][a0]}
   std::map<std::pair<int, int>, std::vector<double>> lfli_me_;
   bool not_valid_LfLi_;

   // Contructor computes 9-lm coefficients that will be required for computation of proton-neutron 
   // reduced matrix elements
   SU39LM_PNMeComputer(const SU3::LABELS& wp_ket, const SU3::LABELS& wGp, const SU3::LABELS& wp_bra,
              const SU3::LABELS& wn_ket, const SU3::LABELS& wGn, const SU3::LABELS& wn_bra,
              const SU3::LABELS& wi, const SU3::LABELS& w0, const SU3::LABELS& wf) {
      rhotmax_ = SU3::mult(wi, w0, wf);
      rhoimax_ = SU3::mult(wp_ket, wn_ket, wi);
      rhofmax_ = SU3::mult(wp_bra, wn_bra, wf);
      rho0max_ = SU3::mult(wGp, wGn, w0);
      rhopmax_ = SU3::mult(wp_ket, wGp, wp_bra);
      rhonmax_ = SU3::mult(wn_ket, wGn, wn_bra);
      size_t nsu39lm = rhotmax_ * rhoimax_ * rhofmax_ * rho0max_ * rhopmax_ * rhonmax_;
      su39lm_.resize(nsu39lm, 0.0);
      su39lmComputer.Get9lm(wp_ket, wGp, wp_bra, wn_ket, wGn, wn_bra, wi, w0, wf, su39lm_.data());
//      assert(su39lm.size() == nsu39lm);
      // lfli_me structure is not valid
      not_valid_LfLi_ = true;

      // Alternatively, SU(3) 9-lm could be also computed directly without using U6-lm and Z6-lm
      // hash structures
      /*
            su3::wu39lm(wp_ket.lm, wp_ket.mu, wGp.lm, wGp.mu, wp_bra.lm, wp_bra.mu, wn_ket.lm,
         wn_ket.mu, wGn.lm, wGn.mu, wn_bra.lm, wn_bra.mu, wi.lm, wi.mu, w0.lm, w0.mu, wf.lm, wf.mu,
                        su39lm);
      */
   }
   SU39LM_PNMeComputer(SU39LM_PNMeComputer&& copy)
       : su39lm_(std::move(copy.su39lm_)),
         pnrmes_(std::move(copy.pnrmes_)),
         lfli_me_(std::move(copy.lfli_me_)),
         rhotmax_(copy.rhotmax_),
         rhoimax_(copy.rhoimax_),
         rhofmax_(copy.rhofmax_),
         rho0max_(copy.rho0max_),
         rhopmax_(copy.rhopmax_),
         rhonmax_(copy.rhonmax_),
         not_valid_LfLi_(copy.not_valid_LfLi_) {}

   // Apply formula (9) to SU(3) 9-lm coefficients to compute pnrmes: vector of proton-neutron rmes
   // <aip ain (gip gin) rhof wf ||| [P^(aGp) x N^{aGn}]^{a0w0S0} ||| ajp ajn (gjp gjn) rhoi wi>rhot[ai][af][a0][rhot]
   // Input:
   // a*max: ranges of indices aip, ain, ajp, ajn, aGp, aGn 
   // prmes: {<aip ip ||| P^aGp ||| ajp jp>rhop[aip][ajp][aGp][rhop]}
   // nrmes: {<ain in ||| N^aGn ||| ajn jn>rhop[ain][ajn][aGn][rhon]}
   void ComputePNRMEs(int aipmax, int aGpmax, int ajpmax, int ainmax, int aGnmax, int ajnmax,
                  const std::vector<float>& prmes, const std::vector<float>& nrmes) {
      const int aimax = ajpmax * ajnmax * rhoimax_;
      const int a0max = aGpmax * aGnmax * rho0max_;
      const int a0_rhot_max = a0max * rhotmax_;
      const int ai_a0_rhot_max = aimax * a0_rhot_max;

      assert(prmes.size() == aipmax * ajpmax * aGpmax * rhopmax_);
      assert(nrmes.size() == ainmax * ajnmax * aGnmax * rhonmax_);
      const size_t nPNrmes = aipmax * ainmax * rhofmax_ * aGpmax * aGnmax * rho0max_ * ajpmax * ajnmax * rhoimax_ * rhotmax_;
      pnrmes_.resize(nPNrmes);

      // index_rtr0rirf --> 9lm[rhot][rho0][rhoi][rhof][rhon:0][rhop:0]
      size_t index_rtr0rirf = 0;
      for (int rhot = 0; rhot < rhotmax_; ++rhot)
      {
         for (int rho0 = 0; rho0 < rho0max_; ++rho0)
         {
            for (int rhoi = 0; rhoi < rhoimax_; ++rhoi)
            {
               for (int rhof = 0; rhof < rhofmax_; ++rhof, index_rtr0rirf += rhopmax_ * rhonmax_) {
                  // <aip ||| P^aGp ||| ajp>rhot[aip][ajp][aGp][rhop:0]
                  // index_aipajpaGp = aip * ajpmax * aGpmax * rhopmax_ + ajp * aGpmax * rhopmax_ +
                  // aGp * rhopmax_
                  size_t index_aipajpaGp = 0;
                  for (int aip = 0; aip < aipmax; ++aip)
                  {
                     for (int ajp = 0; ajp < ajpmax; ++ajp)
                     {
                        for (int aGp = 0; aGp < aGpmax; ++aGp, index_aipajpaGp += rhopmax_)
                        {
                           // <ain ||| N^aGn ||| ajn>rhot[ain][ajn][aGn][rhon:0]
                           //  size_t index_ainajnaGn = ain * ajnmax * aGnmax * rhonmax_ + ajn *
                           //  aGnmax * rhonmax_ + aGn * rhonmax_; // + rhon=0
                           size_t index_ainajnaGn = 0;
                           for (int ain = 0; ain < ainmax; ++ain)
                           {
                              int af = aip * ainmax * rhofmax_ + ain * rhofmax_ + rhof;
                              for (int ajn = 0; ajn < ajnmax; ++ajn)
                              {
                                 int ai = ajp * ajnmax * rhoimax_ + ajn * rhoimax_ + rhoi;
                                 for (int aGn = 0; aGn < aGnmax; ++aGn, index_ainajnaGn += rhonmax_)
                                 {
                                    int a0 = aGp * aGnmax * rho0max_ + aGn * rho0max_ + rho0;
                                    size_t index_9lm = index_rtr0rirf;
                                    double rme = 0.0;
                                    for (int rhon = 0; rhon < rhonmax_; ++rhon)
                                    {
                                       for (int rhop = 0; rhop < rhopmax_; ++rhop, ++index_9lm) {
                                          //     prmes[aip][ajp][aGp][rhop] * 9lm[rhot][rho0][rhoi][rhof][rhon][rhop] * nrmes[ain][ajn][aGn][rhon]
                                          rme += prmes[index_aipajpaGp + rhop] * su39lm_[index_9lm];
                                       }
                                       rme *= nrmes[index_ainajnaGn + rhon];
                                    }
                                    // <af||| a0 ||| ai>rhot[af][ai][a0][rhot]
                                    size_t index_rme = af * ai_a0_rhot_max + ai * a0_rhot_max +
                                                       a0 * rhotmax_ + rhot;
                                    pnrmes_[index_rme] = rme;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      // SU3 proton-neutron rmes were recomputed for a new (ip in) (jp jn) submatrix
      not_valid_LfLi_ = true;
   }

   // Compute complete set of SU(3)SO(3) matrix elements associated with (Lf, L0, Li) for all af, kf, ai, ki, a0 and all k0 values
   // Input:
   // a*max: ranges of multiplicities afmax, aimax, a0max.
   // lmf muf: wf
   // lm0 mu0: w0
   // lmi mui: wi
   // su3cg: LookUpTable(Lf, Li) --> {<wi * Li; w0 * L0|| wf * Li>_{*}[kf][k0][ki][rhot]}
   //
   // Output:
   // lfli_me structure is recomputed
   void ComputeSU3SO3ME(int afmax, int lmf, int muf, int a0max, int lm0, int mu0, int L0, int aimax,
                        int lmi, int mui, WigEckSU3SO3CG* su3cgs) {
      int k0max = su3::kmax(lm0, mu0, L0);
      for (int Lf = 0; Lf <= (lmf + muf); ++Lf) {
         int kfmax = su3::kmax(lmf, muf, Lf);
         if (!kfmax) {
            continue;
         }
         for (int Li = 0; Li <= (lmi + mui); ++Li) {
            int kimax = su3::kmax(lmi, mui, Li);
            if (!kimax) {
               continue;
            }
            if (!SO3::mult(2 * Li, 2 * L0, 2 * Lf)) {
               continue;
            }

            int nrows = kfmax * afmax;
            int ncols = kimax * aimax;
            size_t nmes = nrows * ncols * k0max * a0max;
            std::vector<double> meLfLi(nmes, 0.0);

            double* su3so3cgs = su3cgs->SU3SO3CGs(2*Lf, 2*Li);

            size_t index_kfk0ki = 0;
            for (int kf = 0; kf < kfmax; ++kf) {
               for (int k0 = 0; k0 < k0max; ++k0) {
                  for (int ki = 0; ki < kimax; ++ki, index_kfk0ki += rhotmax_) {
                     size_t irow = kf * afmax;
                     for (int af = 0; af < afmax; ++af, ++irow) {
                        size_t icol = ki * aimax;
                        // index_afaia0 --> [af][ai:0][a0:0][rhot:0]
                        size_t index_afaia0 = af * aimax * a0max * rhotmax_;
                        for (int ai = 0; ai < aimax; ++ai, ++icol) {
                           // index_ijk0a0 --> me[i][j][k0][a0] and here it is set to a0:0
                           size_t index_ijk0a0 = irow * ncols * k0max * a0max + icol * k0max * a0max + k0 * a0max;
                           for (int a0 = 0; a0 < a0max; ++a0, ++index_ijk0a0, index_afaia0 += rhotmax_) {
                              for (int rhot = 0; rhot < rhotmax_; ++rhot) {
                                 meLfLi[index_ijk0a0] += su3so3cgs[index_kfk0ki + rhot] * pnrmes_[index_afaia0 + rhot];
                              }
                           }
                        }
                     }
                  }
               }
            }
            lfli_me_[std::make_pair(Lf, Li)] = std::move(meLfLi);
         }
      }
      not_valid_LfLi_ = false;
   }
}; // struct SU39LM_PNMeComputer

// Input:
// {wp_ket  wGp   wp_bra} 
// {wn_ket  wGn   wn_bra} 
// {wi      w0    wf    } 
//
// Output:
// wfwi_SU39lm_PNMEComp: LookUpStructure(wf, wi) --> struct SU39LM_PNMEComputer
void Compute9lmSymbols_for_WFWI_MeComp(
    const SU3::LABELS& wp_ket, const SU3::LABELS& wGp, const SU3::LABELS& wp_bra,
    const SU3::LABELS& wn_ket, const SU3::LABELS& wGn, const SU3::LABELS& wn_bra,
    const SU3::LABELS& w0, std::map<std::pair<SU3::LABELS, SU3::LABELS>, SU39LM_PNMeComputer>& wfwi_SU39lm_PNMEComp) {
   std::vector<std::tuple<int, int, int>> wf_vec, wi_vec;
   // wp x wn --> {wf}
   su3::couple(wp_bra.lm, wp_bra.mu, wn_bra.lm, wn_bra.mu, wf_vec);
   // wp' x wn' --> {wi}
   su3::couple(wp_ket.lm, wp_ket.mu, wn_ket.lm, wn_ket.mu, wi_vec);

   // For all possible {wf, wi} compute SU(3) 9-lm coefficients
   // Notice that sometime the actual set will be smaller due to
   // selection rules.
   for (auto& wf : wf_vec) {
      int rhofmax, lmf, muf;
      std::tie(rhofmax, lmf, muf) = wf;
      // iterate over wi in {wi}
      for (auto& wi : wi_vec) {
         int rhoimax, lmi, mui;
         std::tie(rhoimax, lmi, mui) = wi;
         // wi x w0 -> wf ?
         int rhotmax = su3::mult(lmi, mui, w0.lm, w0.mu, lmf, muf);
         if (!rhotmax) {
            continue;
         }
         // Create structure responsible for computing matrix elements
         wfwi_SU39lm_PNMEComp.insert(
             std::make_pair(std::make_pair(SU3::LABELS(lmf, muf), SU3::LABELS(lmi, mui)),
                            SU39LM_PNMeComputer(wp_ket, wGp, wp_bra, wn_ket, wGn, wn_bra,
                                       SU3::LABELS(lmi, mui), w0, SU3::LABELS(lmf, muf))));
      }
   }
}

//////////////////////////////////////////////////////
void ReadWfn(lsu3::CncsmSU3xSU2Basis& basis, const std::string& wfn_filename,
             std::vector<float>& wfn) {
   std::fstream wfn_file(wfn_filename.c_str(),
                         std::ios::in | std::ios::binary |
                             std::ios::ate);  // open at the end of file so we can get file size
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_filename << "' wfn file" << endl;
      exit(EXIT_FAILURE);
   }

   size_t size = wfn_file.tellg();
   size_t nelems = size / sizeof(float);

   if (size % sizeof(float) || (nelems != basis.getModelSpaceDim())) {
      cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x "
           << sizeof(float) << " = " << basis.getModelSpaceDim() * sizeof(float) << " bytes."
           << endl;
      cout << "The actual size of the file: " << size << " bytes!";
      exit(EXIT_FAILURE);
   }

   wfn.resize(nelems);
   wfn_file.seekg(0, std::ios::beg);
   wfn_file.read((char*)wfn.data(), wfn.size() * sizeof(float));
}

// Definition of submatrix:
// ip in ipin_block
// jp jn jpjn_block
// Submatrix is defined by basis of proton-neutron product (ip in) bra space and (jp jn) product in ket space.
// Information about blocks in terms of their quantum numbers can be used obtained from
// class lsu3::CncsmSU3xSU2Basis using ipin_block and jpjn_block indices.
//
// prmes, nrmes:
// Seeding reduced matrix elements for the computation of tensor matrix elements of this submatrix.
struct SUBMATRIX_DEFINITION {
   // (ip in) ipin_block uniquely identify basis of SU(3)xSU(2) coupled proton-neutron bra space.
   // ipin_block ... block position in bra basis
   uint32_t ip, in, ipin_block;
   // (jp jn) jpjn_block uniquely identify basis of SU(3)xSU(2) coupled proton-neutron ket space.
   // jpjn_block ... block position in ket basis
   uint32_t jp, jn, jpjn_block;
   // {<in ||| N ||| jn>}
   double dSU2;
   SUBMATRIX_DEFINITION& operator=(const SUBMATRIX_DEFINITION& other) {
      ip = other.ip;
      in = other.in;
      ipin_block = other.ipin_block;
      jp = other.jp;
      jn = other.jn;
      jpjn_block = other.jpjn_block;
      dSU2 = other.dSU2;
      return *this;
   }
};


// Takes as input proton rme block of equivalent SU(3) irreps ... proton_rmes
// Takes as input neutron rme block of equivalent SU(3) irreps ... neutron_rmes
// and generate matrixBlocks ... list of product space submatrices (ip in) (jp jn) and references to
// connecting rmes <ip|||P|||jp> <in|||N|||jn> 
// bra_basis: hash[ip,in] --> blockID(ip,in) 
// ket_basis: hash[jp,jn] --> blockID(jp,jn)
// returns vector of proton-neutron submatrices generated by coupled product of proton and neutron irreps.
// SUBMATRIX_DEFINITION: (ip in) ipin_block, (jp jn) jpjn_block <ip|||P|||jp> <in|||N|||jn>
void CreateTensorProductMatrixBlocks(
    const std::unordered_map<IPIN, uint32_t, boost::hash<IPIN>>& bra_basis,
    const std::unordered_map<JPJN, uint32_t, boost::hash<JPJN>>& ket_basis, const GROUP_RMES& gipgjp_rmes,
    const GROUP_RMES& gingjn_rmes, std::vector<SUBMATRIX_DEFINITION>& matrixBlocks) {

   matrixBlocks.reserve(gipgjp_rmes.ijC.size()*gingjn_rmes.ijC.size());
   for (const auto& ipjpdSU2 : gipgjp_rmes.ijC)
   {
      uint32_t ip, jp;
      double dSU2p;
      std::tie(ip, jp, dSU2p) = ipjpdSU2;
      for (const auto& injndSU2 : gingjn_rmes.ijC) {
         uint32_t in, jn;
         double dSU2n;
         std::tie(in, jn, dSU2n) = injndSU2;
         // Does (ip in) block belong to bra basis?
         auto ipin_blockID = bra_basis.find(std::make_pair(ip, in));
         if (ipin_blockID == bra_basis.end()) {
            continue;
         }
         // Does (jp jn) block belong to ket basis?
         auto jpjn_blockID = ket_basis.find(JPJN(jp, jn));
         if (jpjn_blockID == ket_basis.end()) {
            continue;
         }
         matrixBlocks.push_back({ip, in, ipin_blockID->second, jp, jn, jpjn_blockID->second, dSU2p*dSU2n});

      }
   }
}

// dme[k0][a0] = sum_{i}^{nrows} \sum_{j}^{ncols} vbra[i] x me[i][j][k0][a0] x vket[j]
// Basically vector x matrix x vector multiplication
// Each element of matrix me[i][j] has k0max*a0max elements in [k0][a0] order
void ComputeObd(int nrows, int ncols, const float* v_bra, const std::vector<double>& me, const float* v_ket,
                std::vector<double>& dme_LfLi) {
   size_t index = 0;
   for (int irow = 0; irow < nrows; ++irow) {
      for (int icol = 0; icol < ncols; ++icol) {
         for (auto& dme : dme_LfLi)
         {
            dme += v_bra[irow] * me[index++] * v_ket[icol];
         } 
      }
   }    
}



/////////////////////////////////////////////////////////////////////////
//////////////////////////// G R O U P S ////////////////////////////////
/////////////////////////////////////////////////////////////////////////

// iterate over basis and create mapping between proton irreps and groups: ip --> gip
// Output:
// gp[ip] = gip
void GenerateProtonIrrep2GroupMapping(const lsu3::CncsmSU3xSU2Basis& basis, std::vector<uint32_t>& gp)
{
   std::vector<uint32_t> gp_local(basis.pconf_size(), 0);

   SingleDistributionSmallVector distr_p;
   UN::SU3xSU2_VEC gamma_p;
   SU3xSU2_SMALL_VEC vW_p;

   CGroup current_Group;
   int igp = -1;
   // iterate over proton irreps ip
   for (uint32_t ip = 0; ip < basis.pconf_size(); ip++) 
   {
      distr_p.resize(0);
      gamma_p.resize(0);
      vW_p.resize(0);

      basis.getDistr_p(ip, distr_p);
      basis.getGamma_p(ip, gamma_p);
      basis.getOmega_p(ip, vW_p);

      SU3_VEC omega_p(vW_p.size());
      for (size_t i = 0; i < vW_p.size(); ++i)
      {
         omega_p[i] = SU3::LABELS(vW_p[i].rho, vW_p[i].lm, vW_p[i].mu);
      }

      if (current_Group.distr == distr_p && gamma_p == current_Group.gamma && omega_p == current_Group.omega)
      {
         gp_local[ip] = igp;
      }
      else // new group started ...
      {
         // store distr_ip, gamma_p, omega_p
         current_Group.distr = distr_p;
         current_Group.gamma = gamma_p;
         current_Group.omega = omega_p;
         ++igp;
         gp_local[ip] = igp;
      }
   }
   gp = std::move(gp_local);
}

// iterate over basis and create mapping between neutron irreps and groups: ip --> gip
// Output:
// gp[ip] = gip
void GenerateNeutronIrrep2GroupMapping(const lsu3::CncsmSU3xSU2Basis& basis,  std::vector<uint32_t>& gn)
{
   std::vector<uint32_t> gn_local(basis.nconf_size(), 0);

   SingleDistributionSmallVector distr_n;
   UN::SU3xSU2_VEC gamma_n;
   SU3xSU2_SMALL_VEC vW_n;

   CGroup current_Group;
   int ign = -1;
   // iterate over neutron irreps in
   for (uint32_t in = 0; in < basis.nconf_size(); in++) 
   {
      distr_n.resize(0);
      gamma_n.resize(0);
      vW_n.resize(0);

      basis.getDistr_n(in, distr_n);
      basis.getGamma_n(in, gamma_n);
      basis.getOmega_n(in, vW_n);

      SU3_VEC omega_n(vW_n.size());
      for (size_t i = 0; i < vW_n.size(); ++i)
      {
         omega_n[i] = SU3::LABELS(vW_n[i].rho, vW_n[i].lm, vW_n[i].mu);
      }

      if (current_Group.distr == distr_n && gamma_n == current_Group.gamma && omega_n == current_Group.omega)
      {
         gn_local[in] = ign;
      }
      else // new group started ...
      {
         // store distr_n, gamma_n, omega_n
         current_Group.distr = distr_n;
         current_Group.gamma = gamma_n;
         current_Group.omega = omega_n;
         ++ign;
         gn_local[in] = ign;
      }
   }
   gn = std::move(gn_local);
}

// Generate set of unique {gp, gn} combinations that occur in basis
void GenerateAllowedGiGj(const lsu3::CncsmSU3xSU2Basis& basis, std::set<std::pair<uint32_t, uint32_t>>& gpgn_set) {
   const uint32_t number_ipin_blocks = basis.NumberOfBlocks();
   std::vector<uint32_t> gp, gn;
   gp.reserve(number_ipin_blocks);
   gn.reserve(number_ipin_blocks);

   GenerateProtonIrrep2GroupMapping(basis, gp);
   GenerateNeutronIrrep2GroupMapping(basis, gn);

   for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      uint32_t blockFirstRow = basis.BlockPositionInSegment(ipin_block);
      if (basis.NumberOfStatesInBlock(ipin_block) == 0) {
         continue;
      }
      uint32_t ip = basis.getProtonIrrepId(ipin_block);
      uint32_t in = basis.getNeutronIrrepId(ipin_block);
      // insert {gp, gn} where gp = gp[ip]  gn = gn[in]
      gpgn_set.insert(std::make_pair(gp[ip], gn[in]));
   }
}

// Combine proton and neutron rmes tables to generate
// information about non-vanishing matrix elements
//
//  {{wp, wp', wn, wn'}, .... }
//  wpwn[iw] --> {wp, wp', wn, wn'}
//
// ipinjpjn_groups_wpwn[iw][ig] --> {(ip in)(jp jn) dSU2(ip, jp)*dSU2(in jn)...
// ig --> (gp gp' gn gn')
// each element represent list of non-vanishing submatrices (ip in)(jp jn) dSU2p*dSU2n ...
// that belong to a given combination of bra and ket proton and neutron SU(3) equivalent
// groups (gp gp' gn gn')
//
// rme_groups_wpwn[iw][ig] --> {{<gip ||| P ||| gjp>}, {<gin ||| N ||| gjn>}}
// These group proton and group neutron rmes together with associated dSU2p*dSUn generate all matrix
// elements for (ip in)(jp jn) blocks of submtrices.
void ResolveNonVanishingSubmatrices(
    const lsu3::CncsmSU3xSU2Basis& bra, const lsu3::CncsmSU3xSU2Basis& ket,
    const RMES_TABLE& rme_blocks_p, const RMES_TABLE& rme_blocks_n,
    std::vector<std::tuple<SU3::LABELS, SU3::LABELS, SU3::LABELS, SU3::LABELS>>& wpwn,
    std::vector<std::vector<std::vector<SUBMATRIX_DEFINITION>>>& ipinjpjn_groups_wpwn,
    std::vector<std::vector<std::tuple<std::vector<float>, std::vector<float>>>>&
        rmes_groups_wpwn) {
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   std::cout << "\tCreating [IP, IN] & [JP JN] -> blockId hashes ... "; std::cout.flush();
   start = std::chrono::system_clock::now();
   std::unordered_map<IPIN, uint32_t, boost::hash<IPIN>> bra_basis;
   CreateHashIJ_blockID(bra, bra_basis);

   std::unordered_map<JPJN, uint32_t, boost::hash<JPJN>> ket_basis;
   CreateHashIJ_blockID(ket, ket_basis);
   duration = std::chrono::system_clock::now() - start;
   std::cout << "Done in " << duration.count() << "s." << endl;


   std::set<std::pair<uint32_t, uint32_t>> gipgin_set;
   std::set<std::pair<uint32_t, uint32_t>> gjpgjn_set;

   std::cout << "\tGenerating set of {gip, gin} and {gjp, gjn} ... "; std::cout.flush();
   start = std::chrono::system_clock::now();
   // generate set<{gip, gin}> that occur in bra basis
   GenerateAllowedGiGj(bra, gipgin_set);
   // generate set<{gjp, gjn}> that occur in ket basis
   GenerateAllowedGiGj(ket, gjpgjn_set);
   duration = std::chrono::system_clock::now() - start;
   std::cout << "Done in " << duration.count() << "s." << endl;

   std::cout << "\tGenerating non-vanishing submatrices ... "; std::cout.flush();
   start = std::chrono::system_clock::now();
   // iterate over {wp, wp'} SU(3) equivalent proton blocks
   for (const SU3_EQV_RMES& wipwjp_rmes : rme_blocks_p)
   {
      SU3::LABELS wp_ket = wipwjp_rmes.wgj;
      SU3::LABELS wp_bra = wipwjp_rmes.wgi;
      // iterate over {wn, wn'} SU(3) equivalent neutron blocks
      for (const SU3_EQV_RMES& winwjn_rmes : rme_blocks_n) {
         SU3::LABELS wn_ket = winwjn_rmes.wgj;
         SU3::LABELS wn_bra = winwjn_rmes.wgi;
         // Compute 9-(lm mu) symbols
         std::vector<std::tuple<std::vector<float>, std::vector<float>>> rmes_groups;
         std::vector<std::vector<SUBMATRIX_DEFINITION>> ipinjpjn_groups;
         // {gip, gjp, {<gip||| N ||| gjp>}, {{ip, jp, dSU2}, {ip', jp', dSU2'} ...}
         for (const GROUP_RMES& gipgjp_rmes : wipwjp_rmes.gigj_rmes)
         {
            // {gin, gjn, {<gin||| N ||| gjn>}, {{in, jn, dSU2}, {in', jn', dSU2'} ...}
            for (const GROUP_RMES& gingjn_rmes : winwjn_rmes.gigj_rmes) {
               if (gipgin_set.find(std::make_pair(gipgjp_rmes.gi, gingjn_rmes.gi)) == gipgin_set.end())
               {
                  continue;
               }
               if (gjpgjn_set.find(std::make_pair(gipgjp_rmes.gj, gingjn_rmes.gj)) == gjpgjn_set.end())
               {
                  continue;
               }
               std::vector<SUBMATRIX_DEFINITION> matrixBlocks;
               CreateTensorProductMatrixBlocks(bra_basis, ket_basis, gipgjp_rmes, gingjn_rmes, matrixBlocks);
               if (matrixBlocks.empty())
               {
                  continue;
               }
               rmes_groups.push_back(std::make_tuple(gipgjp_rmes.su3_rmes, gingjn_rmes.su3_rmes));
               ipinjpjn_groups.push_back(matrixBlocks);
            }
         }
         if (!rmes_groups.empty())
         {
            wpwn.push_back(std::make_tuple(wp_bra, wp_ket, wn_bra, wn_ket));
            rmes_groups_wpwn.push_back(rmes_groups);
            ipinjpjn_groups_wpwn.push_back(ipinjpjn_groups);
         }
      }
   }
   duration = std::chrono::system_clock::now() - start;
   std::cout << "Done in " << duration.count() << "s." << endl;
}

// Main function ... computes dmes
void ComputeDmes(const lsu3::CncsmSU3xSU2Basis& bra, const std::vector<float>& bra_wfn,
                 const RMES_TABLE& rme_blocks_p, const RMES_TABLE& rme_blocks_n,
                 const OperatorTensorLabels operatorLabels, const std::vector<float>& ket_wfn,
                 const lsu3::CncsmSU3xSU2Basis& ket, std::vector<double>& dmes) {
   clock_type::time_point start;
   duration_type duration;

   static CWig9jLookUpTable<double> wig9jTable;

   const int Af = bra.NProtons() + bra.NNeutrons();
   const int Ai = ket.NProtons() + ket.NNeutrons();
   const int JJf = bra.JJ();
   const int JJi = ket.JJ();

   const int aGpmax = operatorLabels.aGpmax;
   const int lmGp(operatorLabels.lmGp);
   const int muGp(operatorLabels.muGp);
   const int aGnmax = operatorLabels.aGnmax;
   const int lmGn(operatorLabels.lmGn);
   const int muGn(operatorLabels.muGn);
   const int rho0max(operatorLabels.rho0max);
   const int lm0(operatorLabels.lm0);
   const int mu0(operatorLabels.mu0);
   const int SSGp = operatorLabels.SSGp;
   const int SSGn = operatorLabels.SSGn;
   const int SS0 = operatorLabels.SS0;
   const int L0 = operatorLabels.L0;
   const int JJ0 = operatorLabels.JJ0;

   int k0max = su3::kmax(lm0, mu0, L0);
   int a0max = aGpmax * aGnmax * rho0max;

   std::cout << "Generating 9j look up tables ... "; std::cout.flush();
   Generate9JLookUpTables(Af, Ai, SS0, JJf, JJi, {{2 * L0, {JJ0}}}, wig9j_cached);
   std::cout << "Done" << std::endl;
   
   std::cout << "Generating sets of unique SU3 labels for bra and ket spaces ... "; std::cout.flush();
   start = std::chrono::system_clock::now();
   std::vector<SU3::LABELS> wf_vec = CreateListofSU3Labels(bra);
   std::vector<SU3::LABELS> wi_vec = CreateListofSU3Labels(ket);
   duration = std::chrono::system_clock::now() - start;
   std::cout << "Done in " << duration.count() << "s." << std::endl;

   std::cout << "Computing SU3SO3 CGs ... "; std::cout.flush();
   start = std::chrono::system_clock::now();
   WigEckSU3SO3CGTable su3cgs_w0L0(SU3::LABELS(lm0, mu0), 2*L0);
   // Hash structure for storing <wi ki:* Li:*; w0 k0 L0 || wf kf:* Lf:*>rhot:*
   for (auto wf : wf_vec) {
      for (auto wi : wi_vec) {
         if (SU3::mult(wi, SU3::LABELS(lm0, mu0), wf)) {
            su3cgs_w0L0.GetWigEckSU3SO3CG(wf, wi);
         }
      }
   }
   duration = std::chrono::system_clock::now() - start;
   cout << "Time to compute SU3SO3 CGs ... " << duration.count() << "s." << endl;

   std::vector<std::vector<std::unique_ptr<CWig9JLookUpTable>>>& wig9j_L0 = wig9j_cached[0];

   std::cout << "Resolving non-vanishing submatrices ... " << std::endl;
   start = std::chrono::system_clock::now();
   // {{wp, wp', wn, wn'}, .... }
   std::vector<std::tuple<SU3::LABELS, SU3::LABELS, SU3::LABELS, SU3::LABELS>> wpwn;
   // ipinjpjn_groups_wpwn[iwpwn][i(gip gjp gin gjn)] --> {(ip in)(jp jn) dSU2(ip, jp)*dSU2(in jn)..... }
   std::vector<std::vector<std::vector<SUBMATRIX_DEFINITION>>> ipinjpjn_groups_wpwn;
   // rme_groups[iwpwn][i(gip gjp gin gjn)] --> {{<gip ||| P ||| gjp>}, {<gin ||| N ||| gjn>}}
   std::vector<std::vector<std::tuple<std::vector<float>, std::vector<float>>>> rmes_groups_wpwn;
   ResolveNonVanishingSubmatrices(bra, ket, rme_blocks_p, rme_blocks_n, wpwn, ipinjpjn_groups_wpwn,
                                  rmes_groups_wpwn);
   duration = std::chrono::system_clock::now() - start;
   std::cout << "Total time for resolving non-vanishing submatrices done in " << duration.count() << "s." << std::endl;

   // report basis statistics:

   std::cout << std::endl;

   std::cout << "bra dimension: " << bra.getModelSpaceDim() << std::endl;
   std::cout << "ket dimension: " << ket.getModelSpaceDim() << std::endl;

   std::cout << "bra pconfs size: " << bra.pconf_size() << std::endl;
   std::cout << "bra nconfs size: " << bra.nconf_size() << std::endl;
   std::cout << "ket pconfs size: " << ket.pconf_size() << std::endl;
   std::cout << "ket nconfs size: " << ket.nconf_size() << std::endl;

   std::cout << "bra wpn_ size: " << bra.wpn_.size() << std::endl;
   std::cout << "ket wpn_ size: " << ket.wpn_.size() << std::endl;

   std::cout << "bra ipin blocks: " << bra.NumberOfBlocks() << std::endl;
   std::cout << "ket jpjn blocks: " << ket.NumberOfBlocks() << std::endl;

   uint32_t bra_irreps = bra.wpn_irreps_container_.end() - bra.wpn_irreps_container_.begin();
   uint32_t ket_irreps = ket.wpn_irreps_container_.end() - ket.wpn_irreps_container_.begin();

   std::cout << "bra irreps: " << bra_irreps << std::endl;
   std::cout << "ket irreps: " << ket_irreps << std::endl;

   // resolve bra useq databases:

   std::cout << std::endl << cyan
      << "Resolving bra useq databases... " << reset; std::cout.flush();

   start = clock_type::now();

   basis_useq_dbs<> bra_useq_dbs(bra);

   duration = clock_type::now() - start;

   std::cout << cyan << " [DONE]" << reset << std::endl;
   std::cout << "Time = " << duration.count() << " [s]" << std::endl;
   std::cout << "Dataset size = " << bra_useq_dbs.memory_gib() << " [GiB]" << std::endl;

   // resolve ket useq databases:

   std::cout << std::endl << cyan
      << "Resolving ket useq databases... " << reset; std::cout.flush();

   start = clock_type::now();

   basis_useq_dbs<> ket_useq_dbs(ket);

   duration = clock_type::now() - start;

   std::cout << cyan << " [DONE]" << reset << std::endl;
   std::cout << "Time = " << duration.count() << " [s]" << std::endl;
   std::cout << "Dataset size = " << ket_useq_dbs.memory_gib() << " [GiB]" << std::endl;

   // resolve non-vanishing bra-ket PN irreps: 

   std::cout << std::endl << cyan
      << "Evaluating non-vanishing bra-ket PN irrep pairs... "; std::cout.flush();
   start = clock_type::now();

   boost::multi_array<bool, 2> bra_ket_irrep_pairs(boost::extents[bra_irreps][ket_irreps]);

   for (uint32_t I = 0; I < bra_irreps; I++)
   {
      const auto& bra_irrep = bra.wpn_irreps_container_[I];

      int lmf = bra_irrep.lm;
      int muf = bra_irrep.mu;
      int SSf = bra_irrep.S2;

      for (uint32_t J = 0; J < ket_irreps; J++)
      {
         const auto& ket_irrep = ket.wpn_irreps_container_[J];

         int lmi = ket_irrep.lm;
         int mui = ket_irrep.mu;
         int SSi = ket_irrep.S2;

         bool non_vanishing = SU2::mult(SSi, SS0, SSf) && su3::mult(lmi, mui, lm0, mu0, lmf, muf);

         bra_ket_irrep_pairs[I][J] = non_vanishing;
      }
   }

   duration = clock_type::now() - start;
   std::cout << cyan << " [DONE]" << reset << std::endl;
   std::cout << "Time = " << duration.count() << " [s]" << std::endl;

   // evaluate processed bra-ket UIIS pairs:

   std::cout << std::endl << cyan
      << "Evaluating processed bra-ket UIIS pairs... "; std::cout.flush();
   start = clock_type::now();

   uint32_t bra_UIIS_count = bra_useq_dbs.UIIS_db.useqs();
   uint32_t ket_UIIS_count = ket_useq_dbs.UIIS_db.useqs();

   boost::multi_array<bool, 2> bra_ket_processed_UIIS_pairs(boost::extents[bra_UIIS_count][ket_UIIS_count]);

   assert(wpwn.size() == ipinjpjn_groups_wpwn.size());
   for (const auto& ipinjpjn_groups : ipinjpjn_groups_wpwn)
   {
      for (const auto& matrixBlocks : ipinjpjn_groups)
      {
         for (const auto& ipin_jpjn_block : matrixBlocks)
         {
            int ipin_block = ipin_jpjn_block.ipin_block;
            int jpjn_block = ipin_jpjn_block.jpjn_block;

            uint32_t ipin_UIIS_index = bra_useq_dbs.UIIS_db.index(ipin_block);
            uint32_t jpjn_UIIS_index = ket_useq_dbs.UIIS_db.index(jpjn_block);

            bra_ket_processed_UIIS_pairs[ipin_UIIS_index][jpjn_UIIS_index] = true;
         }
      }
   }

   duration = clock_type::now() - start;
   std::cout << cyan << " [DONE]" << reset << std::endl;
   std::cout << "Time = " << duration.count() << " [s]" << std::endl;

   // resolve non-vanishing inner iterations:

   std::cout << std::endl << cyan
      << "Constructing non-vanishing (iwi, iwf) offset pairs... "; std::cout.flush();
   start = clock_type::now();

   std::vector<std::pair<uint32_t, uint32_t>> kwf_kwi;

   boost::multi_array<std::pair<uint32_t, uint32_t>, 2>
      UIIS_kwf_kwi(boost::extents[bra_UIIS_count][ket_UIIS_count]);

   for (uint32_t bra_UIIS_index = 0; bra_UIIS_index < bra_UIIS_count; bra_UIIS_index++)
   {
      for (uint32_t ket_UIIS_index = 0; ket_UIIS_index < ket_UIIS_count; ket_UIIS_index++)
      {
         if (!bra_ket_processed_UIIS_pairs[bra_UIIS_index][ket_UIIS_index])
            continue;

         UIIS_kwf_kwi[bra_UIIS_index][ket_UIIS_index].first = kwf_kwi.size();
         uint32_t counter = 0;

         const uint32_t* bra_UIIS_wpn_ptr = bra_useq_dbs.UIIS_db.begin(bra_UIIS_index);
         uint32_t bra_UIIS_wpn_count = bra_useq_dbs.UIIS_db.count(bra_UIIS_index);

         const uint32_t* ket_UIIS_wpn_ptr = ket_useq_dbs.UIIS_db.begin(ket_UIIS_index);
         uint32_t ket_UIIS_wpn_count = ket_useq_dbs.UIIS_db.count(ket_UIIS_index);

         for (uint32_t kwf = 0; kwf < bra_UIIS_wpn_count; kwf++)
         {
            uint32_t wf_index = *(bra_UIIS_wpn_ptr + kwf);

            for (uint32_t kwi = 0; kwi < ket_UIIS_wpn_count; kwi++)
            {
               uint32_t wi_index = *(ket_UIIS_wpn_ptr + kwi);

               if (!bra_ket_irrep_pairs[wf_index][wi_index])
                  continue;

               kwf_kwi.emplace_back(kwf, kwi);
               counter++;
            }
         }

         UIIS_kwf_kwi[bra_UIIS_index][ket_UIIS_index].second = counter;
      }
   }

   duration = clock_type::now() - start;
   std::cout << cyan << " [DONE]" << reset << std::endl;
   std::cout << "Time = " << duration.count() << " [s]" << std::endl;

   std::cout << std::endl;

   std::cout << "Computing density matrices ... ";std::cout.flush();
   start = std::chrono::system_clock::now();

#define PROGRESS
#ifdef PROGRESS
   size_t processed_iters = 0;
   size_t progress = 0;
   std::cerr << std::endl;
#endif

   // Take structure describing non-vanishing matrix elements
   // wpwn[iw]: {wp wp' wn wn'}
   // ipinjpjn_groups_wpwn[iw][ig]: {(ip in)(jp jn) dSU2p*dSU2n, ... }
   // rmes_groups_wpwn[iw][ig]: {<gip|||P|||gjp>, <gin|||N|||gjn>}
   // 
   // and compute their contributions to density matrix elements
#pragma omp parallel 
   {
      std::vector<double> dmes_ipinjpjn(k0max * a0max, 0.0);
      std::vector<double> dmes_wfSfwiSi(k0max * a0max, 0.0);
      std::vector<double> dmes_LfLi(k0max * a0max, 0.0);
      std::vector<double> dmes_thread(k0max * a0max, 0.0);
#pragma omp for schedule(dynamic, 1)     
   // iterate over SU(3) equivalent block quantum numbers {wp, wp', wn, wn'}
      for (size_t iw = 0; iw < wpwn.size(); ++iw) {

#ifdef PROGRESS

#pragma omp atomic update 
         processed_iters++;

         if (omp_get_thread_num() == 0) 
         {
            size_t temp;
#pragma omp atomic read 
            temp = processed_iters;

            size_t actual_progress = 100UL * temp / wpwn.size();
            if (actual_progress != progress)
            {
               progress = actual_progress;
               std::cerr << progress << " ";
            }
         }

#endif

         SU3::LABELS wp_bra, wp_ket, wn_bra, wn_ket;
         std::tie(wp_bra, wp_ket, wn_bra, wn_ket) = wpwn[iw];

         // Compute 9-(lm mu) symbols
         std::map<std::pair<SU3::LABELS, SU3::LABELS>, SU39LM_PNMeComputer> wfwi_SU39lm_PNMEComp;
         // Initialize wfwi_SU39lm_PNMEComp structure with SU(3) 9-(lm mu) symbols:
         //                { wp_ket  wGp wp_bra }
         // {wf wi} ---->  { wn_ket  wGn wn_bra }
         //                { wi*     w0   wf*   }
         Compute9lmSymbols_for_WFWI_MeComp(wp_ket, SU3::LABELS(lmGp, muGp), wp_bra, wn_ket,
                                           SU3::LABELS(lmGn, muGn), wn_bra, SU3::LABELS(lm0, mu0),
                                           wfwi_SU39lm_PNMEComp);
         // For a given w0, {wi} x w0 --> {wi} is an empty set.
         // i.e. forbidden by SU(3) selection rules
         if (wfwi_SU39lm_PNMEComp.empty()) {
            std::cerr << "Something wrong with input?" << std::endl;
            exit(EXIT_FAILURE);
         }

         // ipinjpjn_groups[ig=i(gip gjp gin gjn)] --> {(ip in)(jp jn) dSU2(ip, jp)*dSU2(in jn).....
         // }
         const std::vector<std::vector<SUBMATRIX_DEFINITION>>& ipinjpjn_groups =
             ipinjpjn_groups_wpwn[iw];
         // iterate over {{<gip||| P ||| gjp>}, {<gin||| N ||| gjn>} that share {wp, wp', wn, wn'}
         // iterate over all ig ---> {gip, gjp, gin, gjn}
         // that have {wp wp' wn wn'} character
         for (size_t ig = 0; ig < rmes_groups_wpwn[iw].size(); ++ig) {
            // {<gip||| P |||gjp>}
            std::vector<float>& prmes = std::get<0>((rmes_groups_wpwn[iw])[ig]);
            // {<gin||| N |||gjn>}
            std::vector<float>& nrmes = std::get<1>((rmes_groups_wpwn[iw])[ig]);

            // list of submatrices (ip in)(jp jn) with dSU2p*dSU2n
            // that share the same seeding group SU(3) rmes
            // {<gip||| P |||gjp>} and {<gin||| N |||gjn>}
            const std::vector<SUBMATRIX_DEFINITION>& matrixBlocks = ipinjpjn_groups[ig];
            int aipmax = bra.getMult_p(matrixBlocks.front().ip);
            int ajpmax = ket.getMult_p(matrixBlocks.front().jp);
            int ainmax = bra.getMult_n(matrixBlocks.front().in);
            int ajnmax = ket.getMult_n(matrixBlocks.front().jn);

//#define CHECK
#ifdef CHECK
            for (const auto& mb : matrixBlocks)
            {
               if (aipmax != bra.getMult_p(mb.ip)) { std::cerr << "aipmax mismatch" << std::endl; exit(1); }
               if (ainmax != bra.getMult_n(mb.in)) { std::cerr << "ainmax mismatch" << std::endl; exit(1); } 
               if (ajpmax != ket.getMult_p(mb.jp)) { std::cerr << "ajpmax mismatch" << std::endl; exit(1); }
               if (ajnmax != ket.getMult_n(mb.jn)) { std::cerr << "ajnmax mismatch" << std::endl; exit(1); }
            }
#endif

            // we need to recompute SU3 proton-neutron rmes as we have
            // a new set of {<gip|||P||| gjp>} {<gin|||N|||gjn>}
            // iterate over {wf wi} elements
            for (auto& mecomp : wfwi_SU39lm_PNMEComp) {
               // recompute <(gip gin)wf ||| [P x N] ||| (gjp gjn)wi> for all {wf, wi}
               // using previously computed set of SU(3) 9-lm coefficients.
               mecomp.second.ComputePNRMEs(aipmax, aGpmax, ajpmax, ainmax, aGnmax, ajnmax, prmes,
                                           nrmes);
            }
            // iterate over {(ip in)(jp jn) dSU2p*dSU2n} submatrices
            for (const auto& ipin_jpjn_block : matrixBlocks) {
               std::fill(dmes_ipinjpjn.begin(), dmes_ipinjpjn.end(), 0.0);

               int ip = ipin_jpjn_block.ip;
               int in = ipin_jpjn_block.in;
               int ipin_block = ipin_jpjn_block.ipin_block;
               int jp = ipin_jpjn_block.jp;
               int jn = ipin_jpjn_block.jn;
               int jpjn_block = ipin_jpjn_block.jpjn_block;
               double dSU2 = ipin_jpjn_block.dSU2;

               int SSip = (bra.getProtonSU3xSU2(ip)).S2;
               int SSin = (bra.getNeutronSU3xSU2(in)).S2;
               int SSjp = (ket.getProtonSU3xSU2(jp)).S2;
               int SSjn = (ket.getNeutronSU3xSU2(jn)).S2;

               uint32_t blockFirstRow = bra.BlockPositionInSegment(ipin_block);
               uint32_t blockFirstColumn = ket.BlockPositionInSegment(jpjn_block);

               //	loop over wf Sf that result from coupling ip x in
               uint32_t ibegin = bra.blockBegin(ipin_block);
               uint32_t iend = bra.blockEnd(ipin_block);

// NEW VERSION:

               uint32_t row_first = blockFirstRow;
               uint32_t column_first = blockFirstColumn;

               uint32_t bra_UAS_index = bra_useq_dbs.UAS_db.index(ipin_block);
               const uint32_t* afmax_ptr = bra_useq_dbs.UAS_db.begin(bra_UAS_index);

               uint32_t bra_URCOS_index = bra_useq_dbs.URCOS_db.index(ipin_block);
               const uint32_t* row_offset_ptr = bra_useq_dbs.URCOS_db.begin(bra_URCOS_index);

               uint32_t bra_UIIS_index = bra_useq_dbs.UIIS_db.index(ipin_block);
               const uint32_t* iwf_ptr = bra_useq_dbs.UIIS_db.begin(bra_UIIS_index);
               
               uint32_t ket_UAS_index = ket_useq_dbs.UAS_db.index(jpjn_block);
               const uint32_t* aimax_ptr = ket_useq_dbs.UAS_db.begin(ket_UAS_index);

               uint32_t ket_URCOS_index = ket_useq_dbs.URCOS_db.index(jpjn_block);
               const uint32_t* column_offset_ptr = ket_useq_dbs.URCOS_db.begin(ket_URCOS_index);

               uint32_t ket_UIIS_index = ket_useq_dbs.UIIS_db.index(jpjn_block);
               const uint32_t* iwi_ptr = ket_useq_dbs.UIIS_db.begin(ket_UIIS_index);

               uint32_t kwf_kwi_first = UIIS_kwf_kwi[bra_UIIS_index][ket_UIIS_index].first;
               uint32_t kwf_kwi_count = UIIS_kwf_kwi[bra_UIIS_index][ket_UIIS_index].second;

               for (uint32_t k = 0; k < kwf_kwi_count; k++)
               {
                  uint32_t kwf = kwf_kwi[kwf_kwi_first + k].first;
                  uint32_t kwi = kwf_kwi[kwf_kwi_first + k].second;

                  uint32_t afmax = *(afmax_ptr + kwf);
                  uint32_t aimax = *(aimax_ptr + kwi);

                  uint32_t row_offset = *(row_offset_ptr + kwf);
                  uint32_t column_offset = *(column_offset_ptr + kwi);

                  uint32_t currentRow = row_first + row_offset;
                  uint32_t currentColumn = column_first + column_offset;

                  uint32_t wf_index = *(iwf_ptr + kwf);
                  uint32_t wi_index = *(iwi_ptr + kwi);

                  const SU3xSU2::BasisJfixed& braSU3xSU2basis = bra.wpn_irreps_container_[wf_index];
                  const SU3xSU2::BasisJfixed& ketSU3xSU2basis = ket.wpn_irreps_container_[wi_index];

                  SU3xSU2::LABELS wf = braSU3xSU2basis;
                  SU3xSU2::LABELS wi = ketSU3xSU2basis;

                  int SSf = braSU3xSU2basis.S2;
                  int SSi = ketSU3xSU2basis.S2;

// ORIGINAL VERSION:

/*
               // set current row position of the first matrix element in (ip in)(jp jn)
               uint32_t currentRow = blockFirstRow;
               // iterate over {wf Sf}
               for (uint32_t iwf = ibegin; iwf < iend; ++iwf) {
                  SU3xSU2::LABELS wf(bra.Get_Omega_pn_Basis(iwf));
                  int SSf = wf.S2;
                  // multiplicity of irrep wf: af (ip in) wf
                  size_t afmax = aipmax * ainmax * SU3::mult(wp_bra, wn_bra, wf);
                  const SU3xSU2::BasisJfixed& braSU3xSU2basis = bra.Get_Omega_pn_Basis(iwf);

                  // set column position to the first matrix element in (ip in)(jp jn)
                  uint32_t currentColumn = blockFirstColumn;
                  uint32_t jbegin = ket.blockBegin(jpjn_block);
                  uint32_t jend = ket.blockEnd(jpjn_block);
                  // iterate over {wi Si}
                  for (uint32_t iwi = jbegin; iwi < jend; ++iwi) {
                     SU3xSU2::LABELS wi(ket.Get_Omega_pn_Basis(iwi));
                     int SSi = wi.S2;
                     size_t aimax = ajpmax * ajnmax * SU3::mult(wp_ket, wn_ket, wi);
                     // test spin coupling: Si x S0 --> Sf ?
                     if (!SU2::mult(SSi, SS0, SSf)) {
                        currentColumn += aimax * ket.omega_pn_dim(iwi);
                        continue;
                     }
                     // not all wi and wf satisfy wi x wo -> wf
                     if (!su3::mult(wi.lm, wi.mu, lm0, mu0, wf.lm, wf.mu)) {
                        currentColumn += aimax * ket.omega_pn_dim(iwi);
                        continue;
                     }
                     const SU3xSU2::BasisJfixed& ketSU3xSU2basis = ket.Get_Omega_pn_Basis(iwi);
*/

                     // wig9jcoeffs: LookUpTable(Lf, Li) --> {Pi*9j}
                     CWig9JLookUpTable& wig9jcoeffs = *wig9j_L0[SSf][SSi];
                     // {9-lm, < (ipin)wf||| ||| (jpjn)wi>, < wf kf Lf|| || wi ki Li>}
                     auto mecomp = wfwi_SU39lm_PNMEComp.find(
                         std::make_pair(SU3::LABELS(wf), SU3::LABELS(wi)));
                     assert(mecomp != wfwi_SU39lm_PNMEComp.end());
                     SU39LM_PNMeComputer& computer = mecomp->second;
                     // check if the current set of LfLi was generated with current (gp gn)(gp'
                     // gn') block
                     if (computer.not_valid_LfLi_) {
                        // su3cgs --> { <wi * ;w0 L0 || wf *>* }
                        WigEckSU3SO3CG* su3cgs = su3cgs_w0L0.GetWigEckSU3SO3CG(wf, wi);
                        // for all {Lf, Li} pairs compute and create [Lf, Li] --> {<af wf kf Lf
                        // || O^{a0 w0 k0 L0} || ai wi ki Li>}
                        computer.ComputeSU3SO3ME(afmax, wf.lm, wf.mu, a0max, lm0, mu0, L0, aimax,
                                                 wi.lm, wi.mu, su3cgs);
                        //               std::cout << "lfli_me.size(): " <<
                        //               computer.lfli_me.size() << std::endl;
                     }
                     // at this moment we have all SU3SO3 matrix elements
                     // and hence we can compute dmes...

                     std::fill(dmes_wfSfwiSi.begin(), dmes_wfSfwiSi.end(), 0.0);
                     // row position of irst state with Lf in braSU3xSU2basis
                     uint32_t irowLf = currentRow;
                     //	iterate over kf Lf
                     for (SU3xSU2::BasisJfixed::iterator bra_iter = braSU3xSU2basis.rewind(); !braSU3xSU2basis.IsDone(bra_iter);
                          braSU3xSU2basis.nextL(bra_iter)) {
                        int LLf = braSU3xSU2basis.L(bra_iter);
                        // column position of first state with Li in ketSU3xSU2basis
                        uint32_t icolLi = currentColumn;
                        // iterate over ki Li
                        for (SU3xSU2::BasisJfixed::iterator ket_iter = ketSU3xSU2basis.rewind(); !ketSU3xSU2basis.IsDone(ket_iter);
                             icolLi += ketSU3xSU2basis.kmax(ket_iter) * aimax, ketSU3xSU2basis.nextL(ket_iter)) {
                           int LLi = ketSU3xSU2basis.L(ket_iter);
                           if (!SO3::mult(LLi, 2 * L0, LLf)) {
                              continue;
                           }
                           // Since we have single J0 value ==> find returns vector with one
                           // element.
                           double su2factor = *wig9jcoeffs.find(LLf, LLi);
                           // find [Lf, Li] --> {<af (ip in)wf kf Lf || O || ai (jp jn)wi ki Li>}
                           // TODO: it would be nice to avoid searches
                           // this operation is very frequent as it is nested deep in loops.
                           auto me_iter =
                               computer.lfli_me_.find(std::make_pair((int)LLf / 2, (int)LLi / 2));
                           assert(me_iter != computer.lfli_me_.end());
                           // <af (ip in) wf kf Lf || O^a0w0_k0L0 || ai (jp jn) wi ki Li> =
                           // me[kf][af][ki][ai][k0][a0]=me[i][j][k0][a0] i --> kf*afmax + af j
                           // --> ki*aimax + ai
                           std::vector<double>& me = me_iter->second;
                           assert(!me.empty());
                           std::fill(dmes_LfLi.begin(), dmes_LfLi.end(), 0.0);
                           ComputeObd(braSU3xSU2basis.kmax(bra_iter) * afmax,
                                      ketSU3xSU2basis.kmax(ket_iter) * aimax, &bra_wfn[irowLf], me,
                                      &ket_wfn[icolLi], dmes_LfLi);
                           for (size_t ik0a0 = 0; ik0a0 < (k0max * a0max); ++ik0a0) {
                              //                                 std::cout << su2factor *
                              //                                 dme_LfLi[ik0a0] << " ";
                              dmes_wfSfwiSi[ik0a0] += su2factor * dmes_LfLi[ik0a0];
                           }
                        }  // {wi Si} Li
                        irowLf += braSU3xSU2basis.kmax(bra_iter) * afmax;
                     }
                     double Cspin = std::sqrt((SSi + 1) * (SS0 + 1) * (SSip + 1) * (SSin + 1));
                     Cspin *=
                         wig9jTable.GetWigner9j(SSjp, SSjn, SSi, SSGp, SSGn, SS0, SSip, SSin, SSf);
                     for (size_t ik0a0 = 0; ik0a0 < (k0max * a0max); ++ik0a0) {
                        dmes_ipinjpjn[ik0a0] += Cspin * dmes_wfSfwiSi[ik0a0];
                     }

//                   currentColumn += aimax * ket.omega_pn_dim(iwi);
//                }  // {wi Si}
//                currentRow += afmax * bra.omega_pn_dim(iwf);
//             }  // {wf Sf}

               }  // k

               for (size_t ik0a0 = 0; ik0a0 < (k0max * a0max); ++ik0a0) {
                  dmes_thread[ik0a0] += dSU2 * dmes_ipinjpjn[ik0a0];
               }
            }  // (ip in)(jp jn)
         }     // {gip, gjp, gin, gjn}
      }        // {wp, wp', wn, wn'}
#pragma omp critical
      {
         for (int i = 0; i < dmes.size(); ++i) {
            dmes[i] += dmes_thread[i];
         }
      }
   }// end of omp parallel
   duration = std::chrono::system_clock::now() - start;

#ifdef PROGRESS
   cout << endl;
#endif

   cout << "Time to compute dmes ... " << duration.count() << "s." << endl;
}


int main(int argc, char** argv) {
   if (argc != 12) {
      std::cerr << "Usage: \n"
                << argv[0]
                << " <bra model space> <bra wfn> <ket model space> <ket wfn> <proton rmes> "
                   "<neutron rmes> <lm0> <mu0> <2S0> <L0> <2J0>\n"
                << std::endl;
      std::cerr << "to compute <bra wfn || [P x N]^a0 (lm0 mu0) k0 (S0 L0) J0 || ket wfn>, "
                   "where bra/ket wfn spans model space given by file \"<bra/ket model space>\" "
                   "for all possible multiplicities a0 < a0max = aGpmax * aGnmax * rho0max and k0 "
                   "< k0max."
                << std::endl;
      std::cerr << "NOTE: wave functions must be given in ndiag:1 order!\n" << std::endl;
      std::cerr << "NOTE: Generally, a0 = a0p*a0n_max*rho0max + a0n*rho0max + rho0, where a0pmax "
                   "and a0nmax are maximal multiplicities of proton and neutron tensors."
                << std::endl;
      std::cerr << "      In most applications, a0pmax=1 and a0nmax=1 and one has a0=rho0."
                << std::endl;
      std::cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one "
                   "can set the "
                   "size of look-up tables for 9lm, u6lm, z6lm, \"frequent\" 9lm symbols."
                << std::endl;
      return EXIT_FAILURE;
   }
   if (getenv("U6SIZE")) {
      max_num_u6lm = atoi(getenv("U6SIZE"));
   }
   if (getenv("Z6SIZE")) {
      max_num_z6lm = atoi(getenv("U6SIZE"));
   }

   // We compute SU(3) 9-lm on-the-fly as each step of the algorithm
   // uses a unique set of SU(3) 9-lm.
   su39lmComputer.AllocateMemory(max_num_u6lm, max_num_z6lm);

   std::string bra_model_space_filename = argv[1];
   std::string bra_wfn_filename = argv[2];
   std::string ket_model_space_filename = argv[3];
   std::string ket_wfn_filename = argv[4];
   std::string prot_rmes_filename = argv[5];
   std::string neut_rmes_filename = argv[6];
   // a0 is obsolete ... we compute for all possible a0 values!
   int lm0 = atoi(argv[7]);
   int mu0 = atoi(argv[8]);
   int SS0 = atoi(argv[9]);
   int L0 = atoi(argv[10]);
   int JJ0 = atoi(argv[11]);

   if (!SU2::mult(SS0, 2 * L0, JJ0)) {
      std::cerr << "SS0: " << SS0 << " LL0:" << 2 * L0 << " can not couple to JJ0:" << JJ0
                << std::endl;
      return EXIT_FAILURE;
   }

   // We need to change it according to the input parameter JJ0
   // MECalculatorData::JJ0_ ... needed for Wig-Eck theorem
   MECalculatorData::JJ0_ = JJ0;

   proton_neutron::ModelSpace bra_model_space, ket_model_space;
   bra_model_space.Load(bra_model_space_filename);
   ket_model_space.Load(ket_model_space_filename);

   lsu3::CncsmSU3xSU2Basis ket, bra;
   // NOTE: basis sets are ordered according to ndiag:1. Therefore, input wave functions
   // must be given in this order as well.
   bra.ConstructBasis(bra_model_space, 0, 1);
   ket.ConstructBasis(ket_model_space, 0, 1);

   reportMaxRSS("after bases construction");

   const int32_t Af = bra.NProtons() + bra.NNeutrons();
   const int32_t Ai = ket.NProtons() + ket.NNeutrons();
   const int32_t JJf = bra.JJ();
   const int32_t JJi = ket.JJ();

   // Jket x J0 --> ? Jbra
   if (!SU2::mult(ket.JJ(), JJ0, bra.JJ())) {
      std::cerr << "ket 2J:" << (int)ket.JJ() << " coupled with tensor 2J0:" << JJ0
                << " does not yeld bra 2J:" << bra.JJ() << std::endl;
      return EXIT_FAILURE;
   }

   SU3xSU2::LABELS protTensorLabels, neutTensorLabels;
   uint32_t Zf, Zi, NmaxP;
   RMES_TABLE rme_blocks_p;
   LoadRMES_TABLE(prot_rmes_filename, Zf, Zi, NmaxP, protTensorLabels, rme_blocks_p);
//   ShowRMES_TABLE(rme_blocks_p);
   // a tensor file contains Af, Ai, Nmax as a header --> Zf Zi NmaxP
   std::cout << "Proton tensor:" << (int)protTensorLabels.rho << "(" << (int)protTensorLabels.lm
             << " " << (int)protTensorLabels.mu << ") 2S0:" << (int)protTensorLabels.S2
             << std::endl;
   std::cout << "Proton tensor file '" << prot_rmes_filename << "' contains rmes for Zf:" << Zf
             << " Zi:" << Zi << " Nmax:" << NmaxP << std::endl;
   // Make sure the input rme file is appropriate for given bra and ket model spaces
   if (Zf != bra.NProtons() || Zi != ket.NProtons() || NmaxP != bra.Nmax()) {
      std::cerr << "Error: given model spaces need proton tensor for Zf:" << bra.NProtons()
                << " Zi:" << ket.NProtons() << " Nmax:" << bra.Nmax() << std::endl;
      return EXIT_FAILURE;
   }

   uint32_t Nf, Ni, NmaxN;
   // a tensor file contains Af, Ai, Nmax as a header --> Nf Ni NmaxN
   RMES_TABLE rme_blocks_n;
   LoadRMES_TABLE(neut_rmes_filename, Nf, Ni, NmaxN, neutTensorLabels, rme_blocks_n);
//   ShowRMES_TABLE(rme_blocks_n);
   std::cout << "Neutron tensor:" << (int)neutTensorLabels.rho << "(" << (int)neutTensorLabels.lm
             << " " << (int)neutTensorLabels.mu << ") 2S0:" << (int)neutTensorLabels.S2
             << std::endl;
   std::cout << "Neutron tensor file '" << neut_rmes_filename << "' contains rmes for Nf:" << Nf
             << " Ni:" << Ni << " Nmax:" << NmaxN << std::endl;
   // Make sure the input rme file matches our need given the input pair of bra and ket model spaces
   if (Nf != bra.NNeutrons() || Ni != ket.NNeutrons() || NmaxN != bra.Nmax()) {
      std::cerr << "ERROR: given model spaces need neutron tensor for Nf:" << bra.NNeutrons()
                << " Zi:" << ket.NNeutrons() << " Nmax:" << bra.Nmax() << "!" << std::endl;
      return EXIT_FAILURE;
   }

   int rho0max = SU3::mult(protTensorLabels, neutTensorLabels, SU3::LABELS(lm0, mu0));
   if (!rho0max) {
      std::cerr << "Proton tensor SU(3) coupled with neutron SU(3) does not couple to (" << lm0
                << " " << mu0 << ")" << std::endl;
      return EXIT_FAILURE;
   }

   // Let's suppose proton/neutron tensors may have multiplitities > 1
   // In such case one computes a0max = ap_max * an_max * rho0max tensor SU(3) multiplicities
   int a0max = protTensorLabels.rho * neutTensorLabels.rho * rho0max;
   SU3xSU2::LABELS tensorLabels(a0max, lm0, mu0, SS0);
   // S0p x S0n --> ? S0
   if (!SU2::mult(protTensorLabels.S2, neutTensorLabels.S2, SS0)) {
      std::cerr << "Error 2Sp:" << (int)protTensorLabels.S2 << " x 2Sn:" << neutTensorLabels.S2
                << " does not couple to 2S0:" << (int)SS0 << std::endl;
      return EXIT_FAILURE;
   }

   int k0max = SU3::kmax(tensorLabels, L0);
   if (!k0max) {
      std::cerr << "A given tensor does not contain given L0:" << L0 << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Computing dmes for PN tensor with "
             << "(" << lm0 << " " << mu0 << ") 2S0:" << SS0 << " SU(3)xSU(2) character"
             << " L0:" << L0 << " JJ0:" << JJ0 << std::endl;

   // wGp x wGn --> rhom0max w0
   //   int rho0max = su3::mult(protTensorLabels.lm, protTensorLabels.mu, neutTensorLabels.lm,
   //   neutTensorLabels.mu, lm0, mu0);
   OperatorTensorLabels operatorLabels({protTensorLabels.rho, protTensorLabels.lm,
                                        protTensorLabels.mu, neutTensorLabels.rho,
                                        neutTensorLabels.lm, neutTensorLabels.mu, rho0max, lm0, mu0,
                                        protTensorLabels.S2, neutTensorLabels.S2, SS0, L0, JJ0});

   std::vector<float> bra_wfn;
   bra_wfn.resize(bra.getModelSpaceDim(), 1.0);
// ReadWfn(bra, bra_wfn_filename, bra_wfn);

   std::vector<float> ket_wfn;
   ket_wfn.resize(ket.getModelSpaceDim(), 1.0);
// ReadWfn(ket, ket_wfn_filename, ket_wfn);

   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;
   start = std::chrono::system_clock::now();

   std::vector<double> dmes(k0max * a0max, 0.0);
   ///////////////////////////////////////////////////////////////////
#pragma omp parallel
   { su3::init_thread(); }
   ComputeDmes(bra, bra_wfn, rme_blocks_p, rme_blocks_n, operatorLabels, ket_wfn, ket, dmes);
#pragma omp parallel
   { su3::finalize_thread(); }
   ///////////////////////////////////////////////////////////////////
   // Compute phase arising due to the SU(3)-rme reduction formula for proton and neutron system.
   boost::container::small_vector<unsigned char, 2> ZNbra = {(unsigned char)bra.NProtons(),
                                                             (unsigned char)bra.NNeutrons()};
   boost::container::small_vector<unsigned char, 2> ZNket = {(unsigned char)ket.NProtons(),
                                                             (unsigned char)ket.NNeutrons()};
   int G1, G2;
   std::cout << "Enter the total number of proton creational and annihilational operators in the "
                "input proton tensor '"
             << prot_rmes_filename << "'." << std::endl;
   // Example: a+p --> 1;  {a+_p x ta_p} --> 2;  {a+_p x a+_p} --> 2 etc ...";
   std::cin >> G1;
   std::cout << "Enter the total number of neutron creational and annihilational operators in the "
                "input neutron tensor '"
             << neut_rmes_filename << "'." << std::endl;
   // Example: tar_p --> 1;  {a+_n x ta_n} --> 2;  {a+_n x a+_n} --> 2 etc ...";
   std::cin >> G2;
   std::vector<unsigned char> tensor = {(unsigned char)G1, (unsigned char)G2};
   int phasePN = rme_uncoupling_phase(ZNbra, tensor, ZNket);

   std::cout << "Results:" << std::endl;
   for (auto dme : dmes) {
      std::cout << phasePN * dme << std::endl;
   }

   reportMaxRSS("final");

   return EXIT_SUCCESS;
}
