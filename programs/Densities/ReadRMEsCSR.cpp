#include <LSU3/ncsmSU3xSU2Basis.h>
#include <su3dense/tensor_read.h>

#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

// the following must correspond with CSRMEatrix<> template arguments in
// ComputeMultiShellRMEs.cpp:
using value_t = float;
using index_t = lsu3::CncsmSU3xSU2Basis::IRREP_INDEX;

// Print information stored in the header of a tensor file
void ShowHeader(const std::vector<std::vector<char>> &structures,
                const std::vector<SU3xSU2_VEC> &tensorLabels,
                const SU3xSU2_VEC &omegas) {
  int nshells = structures.size();

  std::cout << "nshells: " << nshells << "\t";

  std::cout << "structure=[";
  for (int i = 0; i < nshells; ++i) {
    std::vector<char> structure(structures[i]);
    std::cout << "{";
    for (size_t j = 0; j < structure.size() - 1; ++j) {
      std::cout << (int)structure[j] << ",";
    }
    std::cout << (int)structure.back() << "}";
    if (i == (nshells - 1)) {
      std::cout << "]";
    } else {
      std::cout << ",";
    }
  }
  std::cout << "\t";

  std::cout << "SingleShellTensors=[";
  for (int i = 0; i < nshells; ++i) {
    SU3xSU2_VEC labels(tensorLabels[i]);
    std::cout << "{";
    for (size_t j = 0; j < labels.size() - 1; ++j) {
      std::cout << (int)labels[j].rho << "(" << (int)labels[j].lm << " "
                << (int)labels[j].mu << ")" << (int)labels[j].S2 << ",";
    }
    SU3xSU2::LABELS last(labels.back());
    std::cout << (int)last.rho << "(" << (int)last.lm << " " << (int)last.mu
              << ")" << (int)last.S2 << "}";
    if (i == (nshells - 1)) {
      std::cout << "]";
    } else {
      std::cout << ",";
    }
  }
  std::cout << "\t";

  if (nshells > 1) {
    std::cout << "Omega=[";
    for (int j = 0; j < nshells - 2; ++j) {
      std::cout << (int)omegas[j].rho << "(" << (int)omegas[j].lm << " "
                << (int)omegas[j].mu << ")" << (int)omegas[j].S2 << " ";
    }
    std::cout << (int)omegas.back().rho << "(" << (int)omegas.back().lm << " "
              << (int)omegas.back().mu << ")" << (int)omegas.back().S2 << "]";
  } else {
    std::cout << "Omega={empty}" << std::endl;
  }
  std::cout << std::endl;
}

void ReadShowContent(std::ifstream &file, lsu3::CncsmSU3xSU2Basis &bra,
                     lsu3::CncsmSU3xSU2Basis &ket, uint32_t a0max,
                     SU3xSU2::LABELS &ir0) {
  uint64_t n_rows, n_cols;
  uint64_t global_values, global_colinds;
  // size of data type for matrix elements values	and size of indices
  uint64_t real_bytesize, index_bytesize;

  file.read((char *)&n_rows, sizeof(uint64_t));
  file.read((char *)&n_cols, sizeof(uint64_t));
  file.read((char *)&global_values, sizeof(uint64_t));
  file.read((char *)&global_colinds, sizeof(uint64_t));
  file.read((char *)&real_bytesize, sizeof(uint64_t));
  file.read((char *)&index_bytesize, sizeof(uint64_t));

  if (index_bytesize != sizeof(index_t)) {
    std::cerr << "Trying to store indices from the tensor file into "
                 "non-compatible data type!";
    std::cerr << "Size of indices stored in tensor file: " << index_bytesize;
    std::cerr << ". Size of index_t:" << sizeof(index_t) << std::endl;
    return;
  }

  if (real_bytesize != sizeof(value_t)) {
    std::cerr << "Trying to store rme values from the tensor file into "
                 "non-compatible data type!";
    std::cerr << "Size of rme values stored in the tensor file: "
              << real_bytesize;
    std::cerr << ". Size of rme values we would like to use:" << sizeof(value_t)
              << std::endl;
    return;
  }

  std::vector<value_t> values(global_values);
  std::vector<index_t> colinds(global_colinds);
  std::vector<index_t> rowptrs_values(n_rows + 1);
  std::vector<index_t> rowptrs_colinds(n_rows + 1);

  file.read((char *)values.data(), global_values * real_bytesize);
  file.read((char *)colinds.data(), global_colinds * index_bytesize);
  file.read((char *)rowptrs_values.data(), (n_rows + 1) * index_bytesize);
  file.read((char *)rowptrs_colinds.data(), (n_rows + 1) * index_bytesize);

  for (uint32_t row = 0; row < n_rows; row++) {
    lsu3::CncsmSU3xSU2Basis::IRREP_INDEX ip = row;
    SU3::LABELS wip(bra.getProtonSU3xSU2(ip));
    uint32_t bra_mult = bra.getMult_p(ip);
    auto values_index = rowptrs_values[row];
    for (uint32_t column_index = rowptrs_colinds[row];
         column_index < rowptrs_colinds[row + 1]; column_index++) {
      lsu3::CncsmSU3xSU2Basis::IRREP_INDEX jp = colinds[column_index];
      SU3::LABELS wjp(ket.getProtonSU3xSU2(jp));
      uint32_t ket_mult = ket.getMult_p(jp);
      uint32_t rhot_max = SU3::mult(wjp, ir0, wip);
      uint32_t nrme = bra_mult * ket_mult * a0max * rhot_max;

      assert(nrme);

      std::cout << "ip:" << ip << " jp:" << jp << " data : ";
      for (uint32_t i = 0; i < nrme; ++i) {
        cout << values[values_index + i] << " ";
      }
      std::cout << endl;

      values_index += nrme;
    }
  }
}

int main(int argc, char *argv[]) {

  if (argc != 2) {
    std::cerr << "Usage: " << argv[0]
              << " <tensor file>"
              << std::endl;
    std::cerr << "Show content of file with rmes of a tensor operator generated for A identical nucleons and "
                 "complete Nmax model space. This file is created by ComputeMultiShellRMEs."
              << std::endl;
    return EXIT_FAILURE;
  }
  // The smallest proton-neutron basis that generates complete Nmax A-proton space of U(3) irreps.
  std::string filename(argv[1]);
  std::ifstream file(filename.c_str(), std::ifstream::in | std::ifstream::binary);
  std::vector<std::vector<char>> structures;
  std::vector<SU3xSU2_VEC> tensorLabels;
  SU3xSU2_VEC omegas;

  SU3xSU2::LABELS ir0;
  int A;
  int Nmax;
  ReadHeaderCSR(file, A, Nmax, structures, tensorLabels, omegas, ir0);
  std::cout << "Reduced matrix elements for a system of " << A << " identical nucleons spanning Nmax:" << Nmax << " model space." << std::endl;

  lsu3::CncsmSU3xSU2Basis bra(proton_neutron::ModelSpace(A, 1, Nmax), 0, 1);
  lsu3::CncsmSU3xSU2Basis ket(proton_neutron::ModelSpace(A, 1, Nmax), bra, 0, 1);

  ShowHeader(structures, tensorLabels, omegas);

  uint32_t a0max = GetTensorMaxMult(tensorLabels, omegas);
  ReadShowContent(file, bra, ket, a0max, ir0);

  return EXIT_SUCCESS;
}
