#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

using IJ_RMES = std::unordered_map<std::pair<uint32_t, uint32_t>, std::vector<float>,
                                   boost::hash<std::pair<uint32_t, uint32_t>>>;

float bra_x_Observable_x_ket_per_thread(
    const lsu3::CncsmSU3xSU2Basis& bra, const std::vector<float>& bra_wfn,
    const std::vector<float>& ket_wfn,
    // data structures describing sparse submatrix on CSR format
    const std::vector<float>& vals, const std::vector<size_t>& column_indices,
    const std::vector<size_t>& row_ptrs,
    // rows are organized into groups of (ip in) states. For each block we store its size and
    // starting position in basis.
    // ipin_block_ids holds indices of non vanishing row blocks stored in CSR structure
    const std::vector<uint32_t> ipin_block_ids) {
   // If matrix is empty ==> return 0.0
   if (vals.empty()) {
      assert(row_ptrs.size() == 1);
      assert(column_indices.empty());
      return 0.0;
   }
   std::vector<float> observable_x_ket(row_ptrs.size() - 1, 0);

   assert(vals.size() == row_ptrs.back());
   assert(observable_x_ket.size() == row_ptrs.size() - 1);

   for (int irow = 0; irow < row_ptrs.size() - 1; ++irow) {
      for (int ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival) {
         observable_x_ket[irow] += vals[ival] * ket_wfn[column_indices[ival]];
      }
   }

   float dresult = 0.0;
   int row0;
   int nrows;
   int i = 0;
   for (int iblock = 0; iblock < ipin_block_ids.size(); ++iblock) {
      row0 = bra.BlockPositionInSegment(ipin_block_ids[iblock]);
      nrows = bra.NumberOfStatesInBlock(ipin_block_ids[iblock]);
      for (int irow = 0; irow < nrows; ++irow, ++i) {
         dresult += bra_wfn[row0 + irow] * observable_x_ket[i];
      }
   }
   return dresult;
}

// Content of rmes file:
// Af: number of fermions in bra space
// Ai: number of fermions in ket space
// Nmax: model space used for calculation of given multi-shell rmes
// SU3xSU2::LABELS: a0_max (lm mu) 2S of tensor operator
// NOTE: in this case a0_max is the total multiplicity
// nij_pairs: total number of {(i j) --> <ai i ||| Trho0 |||aj j>rho}
// std::pair(i j) nrmes {<i ||| T |||j>[af][ai][rho0][a0]}
// std::pair(i j) nrmes {<i ||| T |||j>[af][ai][rho0][a0]}
// .
// .
// .
void ReadRMEsFile(const std::string& filename, uint32_t& Af, uint32_t& Ai, uint32_t& Nmax,
                  SU3xSU2::LABELS& tensorLabels, IJ_RMES& rmes_table) {
   std::ifstream file(filename, std::ios::binary);
   if (!file) {
      std::cerr << "Could not open '" << filename << "'!" << std::endl;
      exit(EXIT_FAILURE);
   }
   file.read((char*)&Af, sizeof(Af));
   file.read((char*)&Ai, sizeof(Ai));
   file.read((char*)&Nmax, sizeof(Nmax));
   file.read((char*)&tensorLabels, sizeof(SU3xSU2::LABELS));
   size_t nij_pairs;
   file.read((char*)&nij_pairs, sizeof(nij_pairs));

   for (size_t i = 0; i < nij_pairs; ++i) {
      std::pair<uint32_t, uint32_t> ij;
      file.read((char*)&ij, sizeof(ij));
      size_t nrmes;
      file.read((char*)&nrmes, sizeof(nrmes));
      std::vector<float> rmes(nrmes, 0.0);
      file.read((char*)rmes.data(), nrmes * sizeof(float));

      rmes_table[ij] = std::move(rmes);
   }
}

void ShowRMEsTable(const IJ_RMES& rmes_table) {
   for (const auto& ij_rme : rmes_table) {
      std::pair<uint32_t, uint32_t> ij = ij_rme.first;
      std::cout << "i:" << ij.first << " j:" << ij.second << " ";

      std::vector<float> rmes(ij_rme.second);
      for (const auto& rme : rmes) {
         std::cout << rme << " ";
      }
      std::cout << std::endl;
   }
}

void ReadWfn(lsu3::CncsmSU3xSU2Basis& basis, const std::string& wfn_filename,
             std::vector<float>& wfn) {
   std::fstream wfn_file(wfn_filename.c_str(),
                         std::ios::in | std::ios::binary |
                             std::ios::ate);  // open at the end of file so we can get file size
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_filename << "' wfn file" << endl;
      exit(EXIT_FAILURE);
   }

   size_t size = wfn_file.tellg();
   size_t nelems = size / sizeof(float);

   if (size % sizeof(float) || (nelems != basis.getModelSpaceDim())) {
      cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x "
           << sizeof(float) << " = " << basis.getModelSpaceDim() * sizeof(float) << " bytes."
           << endl;
      cout << "The actual size of the file: " << size << " bytes!";
      exit(EXIT_FAILURE);
   }

   wfn.resize(nelems);
   wfn_file.seekg(0, std::ios::beg);
   wfn_file.read((char*)wfn.data(), wfn.size() * sizeof(float));
}

unsigned long ComputeMatrixElements(lsu3::CncsmSU3xSU2Basis& bra, lsu3::CncsmSU3xSU2Basis& ket,
                                    const IJ_RMES& neut_rmes_table,
                                    const SU3xSU2::LABELS& neutTensorLabels,
                                    const SU3xSU2::LABELS& tensorLabels, int a0, int k0, int L0,
                                    std::vector<float>& vals, std::vector<size_t>& column_indices,
                                    std::vector<size_t>& row_ptrs,
                                    std::vector<uint32_t>& ipin_block_ids) {
   uint32_t LL0 = 2 * L0;
   // definition: libraries/LookUpContainers/WigEckSU3SO3CGTable.h line 134
   WigEckSU3SO3CGTable su3so3cgTable(tensorLabels, LL0);
   unsigned long number_nonzero_me(0);
   int k0max = SU3::kmax(tensorLabels, L0);
   int a0max = tensorLabels.rho;
   // vector of tensor coefficients for all k0 < k0max and a0 < a0max,
   // ordered as [k0][a0] (see libraries/SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJ.fix line)
   std::vector<TENSOR_STRENGTH> coeffs(k0max * a0max, 0.0);
   // IMPORTANT TRICK:
   // We will supply this vector of coefficients into
   // CalculateME_nonDiagonal_nonScalar to get non-zero matrix only for a given
   // a0 and k0 component of a tensor. This allows me to use existing method
   // which is meant to be applied for computations of matrix elements of a realistic
   // Hamiltonian.
   // NOTE: This is silly as one should carry out computation for all allowed a0 and k0 components
   // of tensor
   coeffs[k0 * a0max + a0] = 1.0;

   SU3xSU2::RME identityOperatorRMEPP;
  
   uint16_t max_mult_p = bra.getMaximalMultiplicity_p();
   InitializeIdenticalOperatorRME(identityOperatorRMEPP, max_mult_p*max_mult_p);

   const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
   const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

   for (uint32_t ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      if (bra.NumberOfStatesInBlock(ipin_block) == 0) {
         continue;
      }
      bool vanishingBlock = true;
      uint32_t blockFirstRow = bra.BlockPositionInSegment(ipin_block);

      uint32_t ip = bra.getProtonIrrepId(ipin_block);
      uint32_t in = bra.getNeutronIrrepId(ipin_block);

      SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

      uint16_t aip_max = bra.getMult_p(ip);
      uint16_t ain_max = bra.getMult_n(in);

      unsigned long blockFirstColumn(0);

      std::vector<std::vector<float>> vals_local(bra.NumberOfStatesInBlock(ipin_block));
      std::vector<std::vector<size_t>> col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         if (ket.NumberOfStatesInBlock(jpjn_block) == 0) {
            continue;
         }
         uint32_t jp = ket.getProtonIrrepId(jpjn_block);
         uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

         if (jp != ip)
         {
            continue;
         }

         auto injn_rmes = neut_rmes_table.find(std::make_pair(in, jn));
         if (injn_rmes == neut_rmes_table.end()) {
            continue;
         }

         uint32_t blockFirstColumn = ket.BlockPositionInSegment(jpjn_block);

         uint16_t ajp_max = ket.getMult_p(jp);
         uint16_t ajn_max = ket.getMult_n(jn);

         SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
         SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);

         // note that neutTensorLabels.rho contains maximal multiplicity a0n
         SU3xSU2::RME neut_rme(ain_max, w_in, neutTensorLabels.rho, neutTensorLabels, ajn_max, w_jn,
                               injn_rmes->second);

         //	loop over wpn that result from coupling ip x in
         uint32_t ibegin = bra.blockBegin(ipin_block);
         uint32_t iend = bra.blockEnd(ipin_block);
         uint32_t currentRow = blockFirstRow;
         for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn) {
            SU3xSU2::LABELS wpn_i(bra.getOmega_pn(ip, in, iwpn));
            size_t afmax = aip_max * ain_max * wpn_i.rho;
            SU3xSU2::BasisJfixed braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));

            uint32_t currentColumn = blockFirstColumn;
            uint32_t jbegin = ket.blockBegin(jpjn_block);
            uint32_t jend = ket.blockEnd(jpjn_block);
            for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn) {
               SU3xSU2::LABELS wpn_j(ket.getOmega_pn(jp, jn, jwpn));
               size_t aimax = ajp_max * ajn_max * wpn_j.rho;
               SU3xSU2::BasisJfixed ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));

               if (SU2::mult(wpn_j.S2, tensorLabels.S2, wpn_i.S2) &&
                   SU3::mult(wpn_j, tensorLabels, wpn_i)) {
                  vanishingBlock = false;
                  SU3xSU2::RME total_rme(wpn_i, tensorLabels, wpn_j, &identityOperatorRMEPP, &neut_rme, NULL);
                  // Obtain <(lmi mui) *; (lm0 mu0) * L0 || (lmf muf) *>_{*}
                  WigEckSU3SO3CG* pWigEckSU3SO3CG = su3so3cgTable.GetWigEckSU3SO3CG(wpn_i, wpn_j);
                  // MECalculatorData represent components of a tensor
                  std::vector<MECalculatorData> rmeCoeffsPNPN(
                      1, MECalculatorData(&total_rme, pWigEckSU3SO3CG, coeffs.data(), LL0));
                  CalculateME_nonDiagonal_nonScalar(
                      (currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax,
                      ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
                  delete[] total_rme.m_rme;
               }
               currentColumn += aimax * ket.omega_pn_dim(jwpn);
            }  // wpn_j
            currentRow += afmax * bra.omega_pn_dim(iwpn);
         }  // wpn_i
         delete[] neut_rme.m_rme;
      }
      if (!vanishingBlock) {
         // Store index of ipin block of bra basis states whose matrix elements we are going to
         // store into CSR structure
         ipin_block_ids.push_back(ipin_block);
         for (size_t irow = 0; irow < vals_local.size(); ++irow) {
            vals.insert(vals.end(), vals_local[irow].begin(), vals_local[irow].end());
            row_ptrs.push_back(vals.size());
            column_indices.insert(column_indices.end(), col_ind_local[irow].begin(),
                                  col_ind_local[irow].end());
            number_nonzero_me += vals_local[irow].size();
         }
      }
   }
   return number_nonzero_me;
}

int main(int argc, char** argv) {
   if (argc != 10) {
      std::cerr << "Usage: " << argv[0]
                << " <bra model space> <bra wfn> <ket model space> <ket wfn> "
                   "<neutron rmes> <a0> <k0> <L0> <2J0>"
                << std::endl;
      std::cerr << "to compute <bra wfn || [N]^a0 (lm0 mu0) k0 (S0 L0) J0 || ket wfn>, "
                   "where bra/ket wfn spans model space given by file \"<bra/ket model space>\""
                << std::endl;
      std::cerr << "NOTE: wave functions must be given in ndiag:1 order!\n" << std::endl;
      std::cerr << "NOTE: Generally, a0 <= a0nmax, where a0nmax is the maximal multiplicity of "
                   "neutron tensor."
                << std::endl;
      std::cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one "
                   "can set the "
                   "size of look-up tables for 9lm, u6lm, z6lm, \"frequent\" 9lm symbols."
                << std::endl;
      return EXIT_FAILURE;
   }
   std::string bra_model_space_filename = argv[1];
   std::string bra_wfn_filename = argv[2];
   std::string ket_model_space_filename = argv[3];
   std::string ket_wfn_filename = argv[4];
   std::string neut_rmes_filename = argv[5];
   int a0 = atoi(argv[6]);
   int k0 = atoi(argv[7]);
   int L0 = atoi(argv[8]);
   int JJ0 = atoi(argv[9]);

   // We need to change it according to the input parameter JJ0
   // MECalculatorData::JJ0_ ... needed for Wig-Eck theorem
   MECalculatorData::JJ0_ = JJ0;

   proton_neutron::ModelSpace bra_model_space, ket_model_space;
   bra_model_space.Load(bra_model_space_filename);
   ket_model_space.Load(ket_model_space_filename);

   lsu3::CncsmSU3xSU2Basis ket, bra;
   // NOTE: basis sets are ordered according to ndiag:1. Therefore, input wave functions
   // must be given in this order as well.
   bra.ConstructBasis(bra_model_space, 0, 1);
   ket.ConstructBasis(ket_model_space, 0, 1);

   // Jket x J0 --> ? Jbra
   if (!SU2::mult(ket.JJ(), JJ0, bra.JJ())) {
      std::cerr << "ket 2J:" << (int)ket.JJ() << " coupled with tensor 2J0:" << JJ0
                << " does not yeld bra 2J:" << bra.JJ() << std::endl;
      return EXIT_FAILURE;
   }

   SU3xSU2::LABELS neutTensorLabels;
   // rmes are stored in hash tables that holds:
   // <ip,jp> ---> std::vector{< aip ip ||| a+/ta ||| ajp jp>_rho}
   IJ_RMES neut_rmes_table;

   uint32_t Nf, Ni, NmaxN;
   // a tensor file contains Af, Ai, Nmax as a header --> Nf Ni NmaxN
   ReadRMEsFile(neut_rmes_filename, Nf, Ni, NmaxN, neutTensorLabels, neut_rmes_table);
   int lm0 = neutTensorLabels.lm;
   int mu0 = neutTensorLabels.mu;
   int SS0 = neutTensorLabels.S2;
   int k0max = SU3::kmax(neutTensorLabels, L0);

   if (!k0max) {
      std::cerr << "A given tensor does not contain given L0:" << L0 << std::endl;
      return EXIT_FAILURE;
   }
   if (k0 >= k0max) {
      std::cerr << "Error k0:" << k0 << " >= k0max:" << k0max << "!" << std::endl;
      return EXIT_FAILURE;
   }

   if (!SU2::mult(SS0, 2 * L0, JJ0)) {
      std::cerr << "SS0: " << SS0 << " LL0:" << 2 * L0 << " can not couple to JJ0:" << JJ0
                << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Neutron tensor:" << (int)neutTensorLabels.rho << "(" << (int)neutTensorLabels.lm
             << " " << (int)neutTensorLabels.mu << ") 2S0:" << (int)neutTensorLabels.S2
             << std::endl;
   std::cout << "Neutron tensor file '" << neut_rmes_filename << "' contains rmes for Nf:" << Nf
             << " Ni:" << Ni << " Nmax:" << NmaxN << std::endl;
   // Make sure the input rme file matches our need given the input pair of bra and ket model spaces
   if (Nf != bra.NNeutrons() || Ni != ket.NNeutrons() || NmaxN != bra.Nmax()) {
      std::cerr << "ERROR: given model spaces need neutron tensor for Nf:" << bra.NNeutrons()
                << " Zi:" << ket.NNeutrons() << " Nmax:" << bra.Nmax() << "!" << std::endl;
      return EXIT_FAILURE;
   }

   //   ShowRMEsTable(neut_rmes_table);

   // Let's suppose neutron tensor has multiplicity > 1
   // In such case one computes a0max = an_max tensor SU(3) multiplicities
   int a0max = neutTensorLabels.rho;
   if (a0 >= a0max) {
      std::cerr << "Error a0:" << a0 << " >= a0max:" << a0max << std::endl;
      return EXIT_FAILURE;
   }
   SU3xSU2::LABELS tensorLabels(a0max, lm0, mu0, SS0);

   std::cout << "Computing obds for neutron tensor with "
             << "(" << lm0 << " " << mu0 << ") 2S0:" << SS0
             << " SU(3)xSU(2) character and a0:" << a0 << " k0:" << k0 << " L0:" << L0
             << " JJ0:" << JJ0 << " tensorial component" << std::endl;

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   std::vector<float> bra_wfn;
   ReadWfn(bra, bra_wfn_filename, bra_wfn);

   std::vector<float> ket_wfn;
   ReadWfn(ket, ket_wfn_filename, ket_wfn);

   std::vector<float> vals;
   std::vector<size_t> column_indices;
   std::vector<size_t> row_ptrs;
   row_ptrs.push_back(0);
   // CSR data structure stored in vals, column_indices, row_ptrs describes a submatrix defined by
   // set of (wp x wn) bra basis configurations
   // identification number of wp x wn block of states will be stored in ipin_block_ids
   std::vector<uint32_t> ipin_block_ids;

   su3::init();
#ifdef SU3_9LM_HASHINDEXEDARRAY
    CWig9lmLookUpTable<float>::initialize();
#endif
   ComputeMatrixElements(bra, ket, neut_rmes_table, neutTensorLabels, tensorLabels, a0, k0, L0, vals, column_indices, row_ptrs,
                         ipin_block_ids);
   su3::finalize();
#ifdef SU3_9LM_HASHINDEXEDARRAY
   CWig9lmLookUpTable<float>::finalize();
#endif

   float result_thread = bra_x_Observable_x_ket_per_thread(
       bra, bra_wfn, ket_wfn, vals, column_indices, row_ptrs, ipin_block_ids);

   // Compute phase arising due to the SU(3)-rme reduction formula for proton and neutron system.
   boost::container::small_vector<unsigned char, 2> ZNbra = {(unsigned char)bra.NProtons(),
                                                             (unsigned char)bra.NNeutrons()};
   boost::container::small_vector<unsigned char, 2> ZNket = {(unsigned char)ket.NProtons(),
                                                             (unsigned char)ket.NNeutrons()};
   int G1, G2;
// number of proton creational/annihilation operators for the identity operator is equal to zero.   
   G1 = 0;
   std::cout << "Enter the total number of neutron creational and annihilational operators in the "
                "input neutron tensor '"
             << neut_rmes_filename << "'." << std::endl;
   // Example: tar_p --> 1;  {a+_n x ta_n} --> 2;  {a+_n x a+_n} --> 2 etc ...";
   std::cin >> G2;
   std::vector<unsigned char> tensor = {(unsigned char)G1, (unsigned char)G2};
   int phasePN = rme_uncoupling_phase(ZNbra, tensor, ZNket);

   std::cout << "Result: " << result_thread * phasePN << std::endl;

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();  // clears memory allocated for U9, U6, and Z6
                                                      // coefficients
   //   CSSTensorRMELookUpTablesContainer::ReleaseMemory();  // clear memory allocated for
   //   single-shell
   // SU(3) rmes
   //   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory allocated for
   //   SU(3)>SO(3)

   return EXIT_SUCCESS;
}
