/** \file
 * Computation of density matrices for proton tensors.
 */

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CSU39lm.h>
#include <LookUpContainers/WigEckSU3SO3CGTable.h>
#include <su3dense/CWig9JLookUpTables.h>
#include <su3dense/CommonInGenerator.h>
#include <su3dense/SU3Kernels.h>
#include <su3dense/colors.h>
#include <su3dense/helpers.h>
#include <su3dense/tensor_read.h>
#include <su3dense/timer.h>

#include <su3.h>

#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <utility>
#include <vector>

template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
   return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

using timer_type = chrono_timer<>;

// Input:
//
// me ... matrix elements of 'ntensor' submatrices stored as a one-dimensional vector
//        T[i][j][itensor] --> me[i*ncols*ntensors + j*ntensors + itensor]
//
// positions ... positions of copies of 'me' submatrices in the main matrix.
//               positions = <row1, col1, row2, col2, .... , rowN, colN>
//
void VectorMatrixVector(const std::vector<double>& me, const std::vector<uint32_t>& positions,
                        const std::vector<float>& vleft, const std::vector<float>& vright,
                        uint32_t nrows, uint32_t ncols, std::vector<double>& densities) {
   // number of tensors we need to handle
   const int32_t ntensors = densities.size();

   assert(me.size() == nrows * ncols * ntensors);

   for (int32_t posIdx = 0; posIdx < positions.size(); posIdx += 2) {
      const uint32_t firstRow = positions[posIdx];
      const uint32_t firstColumn = positions[posIdx + 1];

      for (int32_t row = 0; row < nrows; row++) {
         for (int32_t column = 0; column < ncols; column++)
            for (int32_t i = 0; i < ntensors; i++) {
               densities[i] += double(vleft[row + firstRow]) *
                               me[(row * ncols + column) * ntensors + i] *
                               double(vright[column + firstColumn]);
            }
      }
   }
}

void ComputeDensities(lsu3::CncsmSU3xSU2Basis& row_basis, lsu3::CncsmSU3xSU2Basis& col_basis,
                      const CommonInGenerator& common_in_generator,
                      const std::vector<std::pair<int32_t, std::vector<int32_t>>>& ll0_jj0vec,
                      const std::vector<int32_t>& k0max_LL0, std::vector<int32_t>& ntensors_LL0,
                      uint32_t a0max, const SU3xSU2::LABELS& ir0, const IJ_Indices& ipjpindices,
                      const std::vector<tensorfile::value_t>& tensor_rmes,
                      const std::vector<float>& vleft, const std::vector<float>& vright,
                      const std::vector<std::unique_ptr<WigEckSU3SO3CGTable>>& su3so3cgs,
                      const Wigner9JCache& wig9j_cached, const Wigner9SCached& wig9s_cached,
                      std::vector<std::vector<double>>& global_densities) {
   std::vector<std::vector<double>> densities = AllocateDensitiesVectors(ntensors_LL0);

   // matrix elements of a submatrix
   std::vector<double> me;
   // vector with row and column positions of copies of a submatrix in the matrix
   std::vector<uint32_t> positions;
   // a cache (i.e. it is reused) that stores a block in a submatrix
   std::vector<double> me_wpn_row_col;
   std::vector<double> su39lmPtoPN;

   SU3_VEC wpn_col_vec;
   SU3_VEC wpn_row_vec;

   SpinlessRMEsCache l2m2_RMEs_cache;

   size_t n = ipjpindices.size();
#pragma omp for schedule(dynamic)
   for (size_t i = 0; i < n; i++) {
      // #pragma omp critical
      //      std::cout << std::setw( 10 ) << 100.0 * (double)i / (double)n << " % done ... \r " <<
      //      std::flush;
      uint32_t ip = ipjpindices[i][0];
      uint32_t jp = ipjpindices[i][1];
      uint32_t rme_position = ipjpindices[i][2];

      CommonInGenerator::intersection_t intersection;
      // nonvanishing_submatrices: vector of {(lm2 mu2), {{S2 Nnhw a2}, <in ..... >}}
      NonVanishingSubmatrices nonvanishing_submatrices;
      common_in_generator.nonvanishing_equivalent_submatrices(ip, jp, intersection,
                                                              nonvanishing_submatrices);
      if (intersection.empty()) {
         continue;
      }

      //      common_in_generator.show_nonvanishing_submatrices(intersection,
      //      nonvanishing_submatrices);

      SU3xSU2::LABELS wp_row(row_basis.getProtonSU3xSU2(ip));
      int32_t SS1 = wp_row.S2;
      uint32_t a1_max = row_basis.getMult_p(ip);
      SU3xSU2::LABELS wp_col(col_basis.getProtonSU3xSU2(jp));
      int32_t SS1p = wp_col.S2;
      uint32_t a1p_max = col_basis.getMult_p(jp);
      // iterate over all unique wn = (lm2 mu2) in a set of intersecting {in}
      for (auto& wn_submatrices : nonvanishing_submatrices) {
         SU3::LABELS wn(wn_submatrices.first);
         GenerateSpinlessRMEs(a1_max, wp_row, a1p_max, wp_col, wn, a0max, ir0,
                              &(tensor_rmes[rme_position]), wpn_col_vec, wpn_row_vec, su39lmPtoPN,
                              l2m2_RMEs_cache);

         // iterate over (S2, N2hw, a2max) --> N2hw a2max (lm2 mu2)S2
         // block.second ---> {i0, i1, ... } where
         // each index points to intersection_t[i] element
         // which is {ROW_BLOCK_ID, COL_BLOCK_ID}
         for (auto& ss_N_amax_equiv_submatrices : wn_submatrices.second) {
            uint8_t Nn, S2;
            uint16_t a2max;
            std::tie(S2, Nn, a2max) = ss_N_amax_equiv_submatrices.first;

            uint32_t index = ss_N_amax_equiv_submatrices.second.front();

            uint32_t ipin_block = std::get<0>(intersection[index]);
            uint32_t jpin_block = std::get<1>(intersection[index]);
            uint32_t nrows = row_basis.NumberOfStatesInBlock(ipin_block);
            uint32_t ncols = col_basis.NumberOfStatesInBlock(jpin_block);

            size_t ill0 = 0;
            // for each ll0 jj0 pair that is allowed for <Jf || a0=* k0=* l0 J0 || Ji>
            // we compute the matrix elements for the entire block
            for (auto& ll0jj0vec : ll0_jj0vec) {
               me.clear();
               me.resize(nrows * ncols * ntensors_LL0[ill0], 0.0);
               if (a2max == 1) {
                  ComputeMatrixElements(row_basis, col_basis, a1_max, wp_row, a1p_max, wp_col,
                                        ipin_block, jpin_block, a0max, k0max_LL0[ill0], ir0,
                                        ll0jj0vec.first, ll0jj0vec.second, su3so3cgs[ill0].get(),
                                        wig9j_cached[ill0], *wig9s_cached[SS1][SS1p][S2],
                                        l2m2_RMEs_cache, me_wpn_row_col, me);
               } else {
                  assert(a2max == row_basis.getMult_n(row_basis.getNeutronIrrepId(ipin_block)));
                  assert(a2max == col_basis.getMult_n(col_basis.getNeutronIrrepId(jpin_block)));
                  /*
                                    std::cout << "ip:" << ip << " jp:" << jp << std::endl;
                                    std::cout << "in: " << row_basis.getNeutronIrrepId(ipin_block)
                     << std::endl; std::cout << "ipin_block:" << ipin_block << std::endl; std::cout
                     << "jpin_block:" << jpin_block << std::endl; std::cout << "Nhw:" << (int)Nn <<
                     " a2max:" << (int)a2max << " S2:" << (int)S2
                                              << std::endl;
                  */
                  ComputeMatrixElements(row_basis, col_basis, a1_max, wp_row, a1p_max, wp_col,
                                        ipin_block, jpin_block, a2max, a0max, k0max_LL0[ill0], ir0,
                                        ll0jj0vec.first, ll0jj0vec.second, su3so3cgs[ill0].get(),
                                        wig9j_cached[ill0], *wig9s_cached[SS1][SS1p][S2],
                                        l2m2_RMEs_cache, me_wpn_row_col, me);
                  /*
                                    std::cout << "me={";
                                    for (size_t i = 0; i < me_wpn_row_col.size(); ++i)
                                    {
                                       std::cout << me_wpn_row_col[i] << ", ";
                                    }
                                    std::cout << std::endl;
                  */
               }
               positions.clear();
               positions.reserve(2 * ss_N_amax_equiv_submatrices.second.size());
               for (auto& index_submatrix : ss_N_amax_equiv_submatrices.second) {
                  uint32_t ipin_block = std::get<0>(intersection[index_submatrix]);
                  uint32_t jpin_block = std::get<1>(intersection[index_submatrix]);
                  positions.push_back(row_basis.BlockPositionInSegment(ipin_block));
                  positions.push_back(col_basis.BlockPositionInSegment(jpin_block));
               }
               // ShowMatrixElements(me, positions, nrows, ncols, densities[ill0].size());
               VectorMatrixVector(me, positions, vleft, vright, nrows, ncols, densities[ill0]);
               ++ill0;
            }
         }
      }
   }
#pragma omp critical
   {
      for (size_t i = 0, m = densities.size(); i < m; i++)
         for (size_t j = 0, n = densities[i].size(); j < n; j++)
            global_densities[i][j] += densities[i][j];
   }
   //   std::cout << std::endl;
}

void ReadLeftRightVectors(std::vector<float>& vleft, uint32_t row_basis_dim,
                          std::vector<float>& vright, uint32_t col_basis_dim) {
   vleft.resize(row_basis_dim, 1.0);
   vright.resize(col_basis_dim, 1.0);
}

bool ReadWfn(const std::string& wfn_file_name, std::vector<float>& wfn, uint32_t basis_dim) {
   // open at the end of file so we can get file size
   std::fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
      return false;
   }

   size_t filesize = wfn_file.tellg();
   size_t nelems = filesize / sizeof(float);

   if (filesize % sizeof(float) || (nelems != basis_dim)) {
      std::cout << "Error: file size == " << filesize
                << "; dim x sizeof(float) == " << basis_dim * sizeof(float) << "!" << std::endl;
      return false;
   }

   wfn.resize(basis_dim);
   wfn_file.seekg(0, std::ios::beg);
   wfn_file.read((char*)wfn.data(), filesize);

   return true;
}

int main(int argc, char* argv[]) {
   enum { BRA_SPACE = 1, BRA_WFN = 2, KET_SPACE = 3, KET_WFN = 4, OP_TYPE = 5, TENSOR_LIST = 6, JJ0SEL = 7};
   if (argc != 7 && argc != 8) {
      std::cerr << "Computation of proton two-body or one-body densities of a+a+aa or a+a "
                   "operators coupled to good [SU(3)>SO(3)]xSU(2)>SU(2) tensors for a pair of "
                   "LSU3shell bra and ket wavefunctions |Jf> and |Ji>. Densities are computed for "
                   "all allowed k0 L0 J0 tensor quantum numbers that can couple a "
                   "given pair of wave functions."
                << std::endl
                << std::endl;
      std::cerr << "Usage: \n\t" << argv[0]
                << " <bra model space> <bra wfn> <ket model space> <ket wfn> <particle rank> "
                   "<tensor list file> {<2J0 selection>}"
                << std::endl
                << std::endl;
      std::cerr << "Arguments:" << std::endl;
      std::cerr << "\t<bra model space> <bra wfn>: specify bra wave function <Jf|" << std::endl;
      std::cerr << "\t<ket model space> <ket wfn>: specify ket wave function |Ji>" << std::endl;
      std::cerr << "\t<particle rank>: \n\t\t 1 ... one-body tensors \n\t\t 2 ... two-body tensors" << std::endl;
      std::cerr << "\t<tensor list file>: \n\t\t one-body tensors -- each line contains n1 n2 lm0 mu0 2S0" << std::endl;
      std::cerr << "\t\t two-body tensors -- each line contains n1 n2 n3 n4 lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0" << std::endl;
      std::cerr << "\t optional argument {<J0 selection>}: compute OBDMEs only for tensors with selected J0 value." << std::endl;
      return EXIT_FAILURE;
   }

   int JJ0sel = -1;
   if (argc == 8)
   {
      JJ0sel = 2*atoi(argv[JJ0SEL]);
   }

   ///////////////////////////////////////////////////////////////////
   // Construct basis of rows and columns
   ///////////////////////////////////////////////////////////////////
   // TODO: Implement MPI sharing of workload using NDIAG x NDIAG processes
   lsu3::CncsmSU3xSU2Basis row_basis(proton_neutron::ModelSpace(argv[BRA_SPACE]), 0, 1);
   lsu3::CncsmSU3xSU2Basis col_basis(proton_neutron::ModelSpace(argv[KET_SPACE]), 0, 1);
   std::cout << "====================================================================="
             << std::endl;
   // Since we currently implement only a+a+tata and a+ta operators, we need to check
   // if Zf=Zi Nf=Ni Nmaxf = Nmaxi
   if (row_basis.NProtons() != col_basis.NProtons() ||
       row_basis.NNeutrons() != col_basis.NNeutrons() || row_basis.Nmax() != col_basis.Nmax()) {
      std::cerr << "Bra and ket model spaces does not satisfy Zf=Zi Nf=Ni Nmaxf=Nmaxi" << std::endl;
      return EXIT_FAILURE;
   }

   const int32_t JJf = row_basis.JJ();
   const int32_t JJi = col_basis.JJ();

   // Obtain filenames of tensors we are going to compute
   std::ifstream tensor_list_file(argv[TENSOR_LIST]);
   if (!tensor_list_file) {
      std::cerr << "Cannot open file '" << argv[TENSOR_LIST] << "' with list of tensors."
                << std::endl;
      exit(EXIT_FAILURE);
   }

   enum { ONE_BODY, TWO_BODY };
   int operator_type;

   // TODO: implement as a structure to improve code readability
   // n1 n2 lm0 mu0 2S0
   std::vector<std::array<int, 5>> tensor_labels_1B;
   // TODO: implement as a structure to improve code readability
   // n1 n2 n3 n4 lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0
   std::vector<std::array<int, 13>> tensor_labels_2B;

   // Each element holds rme input file associated with a given set of 1B or 2B quantum numbers of a
   // tensor.
   std::vector<std::string> tensor_rmes_filenames;
   if (std::string(argv[OP_TYPE]) == "1") {
      operator_type = ONE_BODY;
      while (true) {
         int n1, n2, lm0, mu0, ss0;
         tensor_list_file >> n1;
         tensor_list_file >> n2;

         tensor_list_file >> lm0;
         tensor_list_file >> mu0;
         tensor_list_file >> ss0;

         // check if 1/2 x 1/2 --> ss0
         if (ss0 != 0 && ss0 != 2) {
            std::cerr << "ss0:" << ss0 << " cannot be produced by coupling two spins: 1/2 x 1/2"
                      << std::endl;
            return EXIT_FAILURE;
         }
         // check if (n1 0) x (0 n2) --> (lm0 mu0)
         if (!SU3::mult(SU3::LABELS(n1, 0), SU3::LABELS(0, n2), SU3::LABELS(lm0, mu0))) {
            std::cerr << "n1:" << n1 << " n2:" << n2 << " does not couple to (" << lm0 << " " << mu0
                      << ")!" << std::endl;
            return EXIT_FAILURE;
         }

         std::array<int, 5> tensor = {n1, n2, lm0, mu0, ss0};
         if (tensor_list_file) {
            tensor_labels_1B.push_back(tensor);
         } else {
            break;
         }
      }
      // iterate over tensors (and their labels)
      for (auto tensor : tensor_labels_1B) {
         // Obtain filename associated with n1 n2 lm0 mu0 2S0 quantum numbers
         // If n1 > n2 => filename will include proper swap of tensors
         // Note that in such case we have to multiply rmes from input file by a phase
         tensor_rmes_filenames.emplace_back(GetFilename_SU3xSU2(tensor));
      }
   } else if (std::string(argv[OP_TYPE]) == "2") {
      operator_type = TWO_BODY;
      while (true) {
         int n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0;
         tensor_list_file >> n1;
         tensor_list_file >> n2;
         tensor_list_file >> n3;
         tensor_list_file >> n4;

         tensor_list_file >> lmf;
         tensor_list_file >> muf;
         tensor_list_file >> ssf;

         tensor_list_file >> lmi;
         tensor_list_file >> mui;
         tensor_list_file >> ssi;

         tensor_list_file >> lm0;
         tensor_list_file >> mu0;
         tensor_list_file >> ss0;

         std::array<int, 13> tensor = {n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0};
         if (tensor_list_file) {
            tensor_labels_2B.push_back(tensor);
         } else {
            break;
         }
      }
      for (auto tensor : tensor_labels_2B) {
         // Obtain filename associated with given tensor quantum numbers, number of protons, and
         // Nmax parameter.
         tensor_rmes_filenames.emplace_back(
             GetFilename_SU3xSU2(tensor, row_basis.NProtons(), row_basis.Nmax()));
      }
   } else {
      // Error: neither 1B or 2B !!!
      std::cerr << "Unknown type of operator: '" << argv[OP_TYPE] << "'!" << std::endl;
      return EXIT_FAILURE;
   }

   // create output file with resulting densities
   std::stringstream output_filename;
   if (operator_type == ONE_BODY)
   {
      output_filename << argv[TENSOR_LIST] << ".su3_pp_obdmes";
   }
   else // if TWO_BODY;
   {
      assert(operator_type == TWO_BODY);
      output_filename << argv[TENSOR_LIST] << ".su3_pp_tbdmes";
   }
   // Here is a trick: we will store resulting resulting densities in a temporary file named
   // <output_filename>_ and at the end will create a new file with name <output_filename> that will
   // have the number of densities as a first line.
   std::ofstream file_resulting_densities(output_filename.str() + "_");
   file_resulting_densities.precision(10);

   // total number of seconds we spent computing densities
   double densities_sec = 0;
   // total time of calculations
   timer_type total_time(timer_type::start_now);

   // Initialize SU(3) and SU(2) libraries
#pragma omp parallel
   su3::init_thread();
   ///////////////////////////////////////////////////////////////////
   // Data structure for identifying equivalent matrices
   ///////////////////////////////////////////////////////////////////
   CommonInGenerator common_in_generator(row_basis, col_basis);
   common_in_generator.init();

   ///////////////////////////////////////////////////////////////////
   // Construct input vectors
   ///////////////////////////////////////////////////////////////////
   std::vector<float> vleft, vright;
   bool ok;
   std::cout << "bra:" << argv[BRA_WFN] << std::endl;
   ok = ReadWfn(argv[BRA_WFN], vleft, row_basis.dim());
   if (!ok) {
      return EXIT_FAILURE;
   }
   std::cout << "ket:" << argv[KET_WFN] << std::endl;
   ok = ReadWfn(argv[KET_WFN], vright, col_basis.dim());
   if (!ok) {
      return EXIT_FAILURE;
   }
   std::cout << "====================================================================="
             << std::endl;
   int ndensities = 0;
   // Iterate over tensors and compute all their possible densities for a given pair of wave
   // functions.
   for (size_t i = 0; i < tensor_rmes_filenames.size(); ++i) {
      auto filename = tensor_rmes_filenames[i];
      //      std::cout << filename << std::endl;
      ///////////////////////////////////////////////////////////////////
      // Read reduced matrix elements for an input tensor
      ///////////////////////////////////////////////////////////////////
      SU3xSU2::LABELS ir0;
      IJ_Indices ipjpindices;
      std::vector<tensorfile::value_t> tensor_rmes;
      int a0max;
      // 2B operator case
      if (operator_type == TWO_BODY) {
         auto two_body = tensor_labels_2B[i];
         int n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0;
         n1 = two_body[N1];
         n2 = two_body[N2];
         n3 = two_body[N3];
         n4 = two_body[N4];

         lmf = two_body[LMF];
         muf = two_body[MUF];
         ssf = two_body[SSF];

         lmi = two_body[LMI];
         mui = two_body[MUI];
         ssi = two_body[SSI];

         lm0 = two_body[LM0];
         mu0 = two_body[MU0];
         ss0 = two_body[SS0];

         std::array<int, 13> quantum_numbers_from_file;
         uint32_t A, Nmax;
         try {
            std::cout << "n1:" << n1 << " n2:" << n2 << " n3:" << n3 << " n4:" << n4 << " (" << lmf
                      << " " << muf << ")" << ssf << " (" << lmi << " " << mui << ")" << ssi << " ("
                      << lm0 << " " << mu0 << ")" << ss0 << std::endl;
            // load rme tables from from given in "IJ_Indices" format
            LoadRMEsIJ_Indices(filename, quantum_numbers_from_file, A, Nmax, ipjpindices,
                               tensor_rmes);
         } catch (const std::exception& e) {
            file_resulting_densities << two_body[N1] << " ";
            file_resulting_densities << two_body[N2] << " ";
            file_resulting_densities << two_body[N3] << " ";
            file_resulting_densities << two_body[N4] << "   ";
            file_resulting_densities << two_body[LMF] << " ";
            file_resulting_densities << two_body[MUF] << " ";
            file_resulting_densities << two_body[SSF] << "   ";
            file_resulting_densities << two_body[LMI] << " ";
            file_resulting_densities << two_body[MUI] << " ";
            file_resulting_densities << two_body[SSI] << "   ";
            file_resulting_densities << two_body[LM0] << " ";
            file_resulting_densities << two_body[MU0] << " ";
            file_resulting_densities << two_body[SS0] << "   ";
            file_resulting_densities << e.what() << std::endl;
            std::cout << e.what() << std::endl << std::endl;
            continue;
         }

         if (quantum_numbers_from_file != two_body) {
            std::cerr << "Input tensor quantum numbers are different from those saved in '"
                      << filename << "' input file." << std::endl
                      << std::endl;
            return EXIT_FAILURE;
         }
         a0max = SU3::mult(SU3::LABELS(two_body[LMF], two_body[MUF]),
                           SU3::LABELS(two_body[LMI], two_body[MUI]),
                           SU3::LABELS(two_body[LM0], two_body[MU0]));
         ir0 = SU3xSU2::LABELS(a0max, two_body[LM0], two_body[MU0], two_body[SS0]);
         if (A != row_basis.NProtons() || Nmax != row_basis.Nmax()) {
            std::cerr << "Given rme file was created for Z:" << A
                      << ". However given model spaces have Z:" << row_basis.NProtons() << "!"
                      << std::endl
                      << std::endl;
            return EXIT_FAILURE;
         }
      } else {  // 1B
         auto one_body = tensor_labels_1B[i];
         int n1 = one_body[0];
         int n2 = one_body[1];
         int lm0 = one_body[2];
         int mu0 = one_body[3];
         int ss0 = one_body[4];
         ir0 = SU3xSU2::LABELS(lm0, mu0, ss0);
         try {
            std::cout << "n1:" << n1 << " n2:" << n2 << " (" << lm0 << " " << mu0 << ")" << ss0
                      << std::endl;
            a0max = ReadProtonTensorRMEsCSRtoIJ_Indices(filename, row_basis, col_basis, ir0,
                                                        ipjpindices, tensor_rmes);

         } catch (const std::exception& e) {
            file_resulting_densities << n1 << " ";
            file_resulting_densities << n2 << "   ";
            file_resulting_densities << lm0 << " ";
            file_resulting_densities << mu0 << " ";
            file_resulting_densities << ss0 << "   ";
            file_resulting_densities << e.what() << std::endl;
            std::cout << e.what() << std::endl << std::endl;
            continue;
         }
         // ==> tensor_rmes contains [tan2 x adn1] rmes
         // To get [adn1 x tan2] rmes, we need to multiply by the phase
         if (n1 > n2) {
            int phase = n1 + n2 - lm0 - mu0 - (ss0 / 2);
            if (phase % 2)  // phase is odd ==> (-1)^phase = -1;
            {
               // multiply rmes from file by -1 to get rmes for [adn1 x tan2]^(lm0 mu0)S0
               std::transform(tensor_rmes.begin(), tensor_rmes.end(), tensor_rmes.begin(),
                              std::bind2nd(std::multiplies<double>(), -1));
            }
         }
         assert(a0max == 1);
      }
      // TODO: input tensors should be sorted according to their (lm0 mu0) S0
      // character. All of them share SU(3) and SU(2) coupling coefficients

      ///////////////////////////////////////////////////////////////////
      // Generate L0 anb {J0} values that satisfy <Jf || T^{(lm0 mu0) S0}_{k0:* L0 J0} || Ji> != 0
      // obtained from S0 x L0 ---> {J0} coupling that satisfy JJi x JJ0 --> JJf
      ///////////////////////////////////////////////////////////////////
      // ll0_jj0vec[i].first  ---> LL0 (generated from (lm0 mu0) physical basis k0 L0
      // ll0_jj0vec[i].second ---> vector of all JJ0 values
      std::vector<std::pair<int32_t, std::vector<int32_t>>> ll0_jj0vec;
      // precompute k0max and ntensors for each L0 considered
      std::vector<int32_t> k0max_LL0;
      std::vector<int32_t> ntensors_LL0;
      GenerateAllowedll0jj0(JJf, a0max, ir0, JJ0sel, JJi, ll0_jj0vec, k0max_LL0, ntensors_LL0);

      if (ll0_jj0vec.empty()) {  // ==> all components are vanishing
         std::cout << "Error: redundant input! Tensor (" << (int)ir0.lm << " " << (int)ir0.mu
                   << ") SS0:" << (int)ir0.S2;
         std::cout << " can not couple JJi:" << JJi << " to JJf:" << JJf << std::endl << std::endl;
         continue;
      }

      ///////////////////////////////////////////////////////////////////
      // Initialize caches for Wigner 9J symbols
      ///////////////////////////////////////////////////////////////////
      Wigner9JCache wig9j_cached;
      Wigner9SCached wig9s_cached;
      // NOTE: We compute SU(2) coeffs for each input tensor. This is suboptimal!
      // TODO: Improve storage and computation of wig9j_cached coefficients
      InitializeWignerSU2Coeffs(row_basis, col_basis, ir0, ll0_jj0vec, wig9j_cached, wig9s_cached);

      ///////////////////////////////////////////////////////////////////
      // Prepare tables with SU(3) Clebsch-Gordan coefficients < * ir0 L0 || *>_*
      ///////////////////////////////////////////////////////////////////
      // TODO: generate just once per all input tensors with constant (lm0 mu0)
      std::vector<std::unique_ptr<WigEckSU3SO3CGTable>> su3so3cgs;
      su3so3cgs.reserve(ll0_jj0vec.size());
      for (auto& ll0_jj0s : ll0_jj0vec) {
         su3so3cgs.push_back(make_unique<WigEckSU3SO3CGTable>(ir0, ll0_jj0s.first));
      }
      su3so3cgs.shrink_to_fit();

      ///////////////////////////////////////////////////////////////////
      // Prepare vectors for resulting densities
      ///////////////////////////////////////////////////////////////////
      std::vector<std::vector<double>> global_densities = AllocateDensitiesVectors(ntensors_LL0);

      timer_type densities_timer(timer_type::start_now);
#pragma omp parallel
      {
         ComputeDensities(row_basis, col_basis, common_in_generator, ll0_jj0vec, k0max_LL0,
                          ntensors_LL0, a0max, ir0, ipjpindices, tensor_rmes, vleft, vright,
                          su3so3cgs, wig9j_cached, wig9s_cached, global_densities);
      }
      densities_timer.stop();
      densities_sec += densities_timer.seconds();
      if (operator_type == TWO_BODY) {
         ShowResults(ir0, a0max, ll0_jj0vec, global_densities);
         // each line of output file contains tensor quantum numbers in the following order
         // n1 n2 n3 n4 wf wi (lm0 mu0) 2S0 2L0 k0 rho0 2J0  <f||T^rho0||i>
//         StoreSU3tbdmes_LL0_K0_RHO0_JJ0(file_resulting_densities, tensor_labels_2B[i], ir0, a0max,
//                                        ll0_jj0vec, global_densities);

         // each line of output file contains tensor quantum numbers in the following order
         // n1 n2 n3 n4 wf wi (lm0 mu0) 2S0 2L0 k0 2J0  {<f||T^rho0:0||i> ... <f||T^rho0max-1||i>}
         ndensities += StoreSU3tbdmes_K0_LL0_JJ0(file_resulting_densities, tensor_labels_2B[i], ir0, a0max,
                                   ll0_jj0vec, global_densities);
      } else {
         ShowResults(ir0, a0max, ll0_jj0vec, global_densities);
         // each line of output file contains tensor quantum numbers in the following order
         // n1 n2 lm0 mu0 2S0 2L0 k0 JJ0
   //      StoreSU3obdmes_LL0_K0_JJ0(file_resulting_densities, tensor_labels_1B[i], ir0, ll0_jj0vec,
   //                                global_densities);
         // each line of output file contains tensor quantum numbers in the following order
         // n1 n2 lm0 mu0 2S0 k0 k0 JJ0
         ndensities += StoreSU3obdmes_K0_LL0_JJ0(file_resulting_densities, tensor_labels_1B[i], ir0, ll0_jj0vec,
                                   global_densities, nucleon::PROTON);
      }
      std::cout << "densities_times:" << densities_timer.seconds() << std::endl << std::endl;
   }

   // 2Jf 2Ji
   file_resulting_densities << JJf << " " << JJi << std::endl;
   // <bra model space> <ket model space>
   file_resulting_densities << argv[BRA_SPACE] << " " << argv[KET_SPACE] << std::endl;
   // <bra wfn>  <ket wfn>
   file_resulting_densities << argv[BRA_WFN] << " " << argv[KET_WFN] << std::endl;
   file_resulting_densities.close();

   // create file with resulting densities
   std::ofstream final_resulting_file(output_filename.str());
   // store number of densities as a first line
   final_resulting_file << ndensities << std::endl;

   // open temporary file with densities
   std::ifstream temp_file(output_filename.str() + "_");
   // copy content of temporary file to resulting file
   final_resulting_file << temp_file.rdbuf();
   temp_file.close();
   // erase temporary file from disk
   std::string temp_filename = output_filename.str() + "_";
   std::remove(temp_filename.c_str());

   total_time.stop();
   std::cout << "Total time in ComputeDensities: " << yellow << densities_sec << reset << " [s]"
             << std::endl;
   std::cout << "Total time: " << yellow << total_time.seconds() << reset << " [s]" << std::endl;

#pragma omp barrier
#pragma omp parallel
   { 
      su3::finalize_thread(); 
   }
   return EXIT_SUCCESS;
}
