#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/CTensorStructure.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/SU3InteractionRecoupler.h>
#include <su3dense/tensor_read.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ctime>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

// Read {n1 n2 n3 n4 (lmf muf)2Sf (lmi mui)2Si (lm0 mu0) 2S0} quantum numbers of tensors from a text
// file
std::vector<std::array<int, 13>> ReadListOfTensors(const std::string& filename) {
   std::vector<std::array<int, 13>> tensor_list;
   std::ifstream file(filename);
   if (!file) {
      std::cerr << "Could not open '" << filename << "' input file!" << std::endl;
      exit(EXIT_FAILURE);
   }

   while (true) {
      int n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0;
      file >> n1 >> n2 >> n3 >> n4 >> lmf >> muf >> ssf >> lmi >> mui >> ssi >> lm0 >> mu0 >> ss0;
      if (file) {
         tensor_list.push_back(
             std::array<int, 13>({n1, n2, n3, n4, lmf, muf, ssf, lmi, mui, ssi, lm0, mu0, ss0}));
      } else {
         break;
      }
   }
   return tensor_list;
}

std::string GetTensorFileName(
    int nshells, const std::vector<std::vector<char>>& vSingleShell_structures,
    const std::vector<std::vector<SU3xSU2::LABELS*>>& vSingleShellTensorLabels,
    const std::vector<SU3xSU2::LABELS*>& vOmega) {
   std::stringstream ss;
   ss << nshells << ".structure.";
   //  Information about shell tensor structure
   for (int i = 0; i < nshells; ++i) {
      const std::vector<char>& structure = vSingleShell_structures[i];
      assert(!structure.empty());
      for (size_t j = 0; j < structure.size() - 1; ++j) {
         ss << (int)structure[j] << "_";
      }
      ss << (int)structure.back() << ".";
   }

   ss << "SingleShellTensorLabels.";
   //  SU3xSU2 labels
   for (int i = 0; i < nshells; ++i) {
      const std::vector<SU3xSU2::LABELS*>& tensorLabels = vSingleShellTensorLabels[i];
      if (tensorLabels.empty()) {
         assert(vSingleShell_structures[i].size() == 1);
         int rho = 1; 
         int lm = 0; 
         int mu = 0; 
         int S2 = 1;
         if (vSingleShell_structures[i][0] < 0)
         {
            mu = std::abs(vSingleShell_structures[i][0]) - 1;
         }
         else
         {
            lm = vSingleShell_structures[i][0] - 1;
         }
         ss << rho << "_";
         ss << lm << "_";
         ss << mu << "_";
         ss << S2 << ".";
      } else {
         for (size_t j = 0; j < tensorLabels.size() - 1; ++j) {
            ss << (int)tensorLabels[j]->rho << "_";
            ss << (int)tensorLabels[j]->lm << "_";
            ss << (int)tensorLabels[j]->mu << "_";
            ss << (int)tensorLabels[j]->S2 << "_";
         }
         ss << (int)tensorLabels.back()->rho << "_";
         ss << (int)tensorLabels.back()->lm << "_";
         ss << (int)tensorLabels.back()->mu << "_";
         ss << (int)tensorLabels.back()->S2 << ".";
      }
   }
   //  SU3xSU2 labels describing coupling of single shell tensors
   ss << "Omega.";
   if (nshells > 1) {
      for (int j = 0; j < nshells - 2; ++j) {
         ss << (int)vOmega[j]->rho << "_";
         ss << (int)vOmega[j]->lm << "_";
         ss << (int)vOmega[j]->mu << "_";
         ss << (int)vOmega[j]->S2 << "_";
      }
      ss << (int)vOmega[nshells - 2]->rho << "_";
      ss << (int)vOmega[nshells - 2]->lm << "_";
      ss << (int)vOmega[nshells - 2]->mu << "_";
      ss << (int)vOmega[nshells - 2]->S2 << ".";
   }
   ss << "tensor";

   return ss.str();
}

// NOTE: For large sets of rmes one may run out of memory. In such a case
// one has to rewrite
void ReadTensorRMEsIntoMemory(
    lsu3::CncsmSU3xSU2Basis& basis,
    const std::map<TENSOR_LABELS, std::pair<std::string, std::vector<double>>>& tensor_expansion,
    std::vector<IJ_Indices>& tensor_ipjpindices, std::vector<std::vector<float>>& tensor_rmes) {
   tensor_ipjpindices.reserve(tensor_expansion.size());
   tensor_rmes.reserve(tensor_expansion.size());
   SU3xSU2::LABELS w0;
   for (auto tensor : tensor_expansion) {
      IJ_Indices ipjpindices;
      std::vector<float> rmes;
      std::string filename = tensor.second.first;
      int a0pmax =
          ReadProtonTensorRMEsCSRtoIJ_Indices(filename, basis, basis, w0, ipjpindices, rmes);
      assert(tensor.first.IR0 == w0);
      assert(tensor.first.IR0.rho == a0pmax);

      tensor_ipjpindices.emplace_back(ipjpindices);
      tensor_rmes.emplace_back(rmes);
   }
}

void ShowTensorExpansion(
    int rho0max,
    const std::map<TENSOR_LABELS, std::pair<std::string, std::vector<double>>>& tensor_expansion) {
   for (auto tensor_coeffs : tensor_expansion) {
      std::string filename = tensor_coeffs.second.first;

      std::cout << tensor_coeffs.first.IR1 << "\t";
      std::cout << tensor_coeffs.first.IR2 << "\t";
      std::cout << tensor_coeffs.first.IR0;
      std::cout << "\t" << filename << std::endl;

      int rho0pmax = tensor_coeffs.first.IR0.rho;
      const std::vector<double>& coeffs = tensor_coeffs.second.second;
      int i = 0;
      for (int rho0 = 0; rho0 < rho0max; ++rho0) {
         for (int rho0p = 0; rho0p < rho0pmax; ++rho0p) {
            std::cout << coeffs[i++] << " ";
         }
         std::cout << std::endl;
      }
   }
}

// For all rho0max component of given a+a+tata vector obtain its expansion in terms of LSU3shell
// compatible tensors.
void RecoupleTensor(
    const std::array<int, 13>& tensor,
    std::map<TENSOR_LABELS, std::pair<std::string, std::vector<double>>>& tensor_expansion) {
   int n1(tensor[adadtataLABELS::N1]);
   int n2(tensor[adadtataLABELS::N2]);
   int n3(tensor[adadtataLABELS::N3]);
   int n4(tensor[adadtataLABELS::N4]);
   int lmf(tensor[adadtataLABELS::LMF]);
   int muf(tensor[adadtataLABELS::MUF]);
   int ssf(tensor[adadtataLABELS::SSF]);
   int lmi(tensor[adadtataLABELS::LMI]);
   int mui(tensor[adadtataLABELS::MUI]);
   int ssi(tensor[adadtataLABELS::SSI]);
   int lm0(tensor[adadtataLABELS::LM0]);
   int mu0(tensor[adadtataLABELS::MU0]);
   int ss0(tensor[adadtataLABELS::SS0]);

   size_t nActiveShells = std::set<int>{n1, n2, n3, n4}.size();
   char n1n2n3n4[4];
   n1n2n3n4[0] = n1;
   n1n2n3n4[1] = n2;
   n1n2n3n4[2] = n3;
   n1n2n3n4[3] = n4;

   SU3xSU2::LABELS wf(1, lmf, muf, ssf);
   SU3xSU2::LABELS wi(1, lmi, mui, ssi);

   int rho0max = SU3::mult(wf, wi, SU3::LABELS(1, lm0, mu0));
   SU3xSU2::LABELS w0(rho0max, lm0, mu0, ss0);

   // all recoupled tensors share the same order of HO shells of a+/ta operators
   std::vector<char> recoupled_structure;

   int nCoeffs = 3 * rho0max;

   // Recoupling is performed for each value of rho0
   for (int rho0 = 0; rho0 < rho0max; ++rho0) {
      std::vector<double> CoeffsPPNNPN(nCoeffs, 0.0);
      int index = rho0 * 3;
      // We want to find recoupling of the rho0^th component of the input tensor
      // Set the approriate coefficients to 1.0 and the rest is set to zero
      CoeffsPPNNPN[index] = 1.0;

      SU3InteractionRecoupler Recoupler;
      int bSuccess = Recoupler.Insert_adad_aa_Tensor(n1n2n3n4, wf, wi, w0, CoeffsPPNNPN);
      if (!bSuccess) {
         // Very unlikely to happen as recoupler has complete set of rules for two-body tensors
         std::cout << "IMPLEMENT: ";

         std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
         std::cout << "\t" << wf << " x " << wi << " -> " << w0;
         std::cout << "\t"
                   << "Sf = " << ssf << "/2  Si = " << ssi << "/2 S0 = " << ss0 << "/2";
         std::cout << std::endl;
         exit(EXIT_FAILURE);
      }
      // Recover tensors and expansion coefficients from SU3Recoupler
      // first and only element of map
      auto n1n2n3n4_tensors = (Recoupler.m_PPNN[nActiveShells - 1]).begin();
      assert((Recoupler.m_PPNN[nActiveShells - 1]).size() == 1);

      if (rho0 == 0) {
         recoupled_structure.resize(7, 0);
         for (int i = 0; i < 6; ++i) {
            recoupled_structure[i] = n1n2n3n4_tensors->first[i];
         }
      }

      // tensor == map< TENSOR_LABELS, {d_rho0p_type} >
      // d_rho0_type={d_{0 p}, d_{0 n}, d_{1 p}, d_{1 n}, ... d_{rho0max-1 p}, d_{rho0max-1 n}}
      // iterate over recoupled tensors
      for (auto tensor : n1n2n3n4_tensors->second) {
         // TENSOR_LABELS = struct {IR1 IR2 IR0}
         // Recoupled tensor labels
         TENSOR_LABELS labels = tensor.first;
         int rho0pmax = labels.IR0.rho;

         // find if a given tensor is already in map of
         // tensor expansion
         auto found = tensor_expansion.find(labels);
         if (found == tensor_expansion.end()) {
            // not found ==> insert TENSOR_LABELS and drho0rhop "matrix" which is initially set to
            // contain only zeros
            SU3xSU2_VEC TensorLabels = {labels.IR1, labels.IR2, labels.IR0};
            std::vector<unsigned char> Shells;
            std::vector<std::vector<char>> vSingleShell_structures;
            std::vector<std::vector<SU3xSU2::LABELS*>> vSingleShellTensorLabels;
            std::vector<SU3xSU2::LABELS*> vOmega;

            StructureToSingleShellTensors(recoupled_structure, Shells, TensorLabels,
                                          vSingleShell_structures, vSingleShellTensorLabels,
                                          vOmega);

            std::string filename = GetTensorFileName(nActiveShells, vSingleShell_structures,
                                                     vSingleShellTensorLabels, vOmega);

            found = (tensor_expansion.insert(std::make_pair(
                         labels,
                         std::make_pair(filename, std::vector<double>(rho0max * rho0pmax, 0.0)))))
                        .first;
         }
         // Store d_{rho0p} coefficients --> d_{rho0}{...}
         std::vector<double>& expansion_coeffs = found->second.second;
         // start_index --> d[rho0][rho0p:0]
         size_t start_index = rho0 * rho0pmax;
         for (size_t irho0p = 0; irho0p < rho0pmax; ++irho0p) {
            expansion_coeffs[start_index + irho0p] = tensor.second[2 * irho0p];
         }
      }  // tensors
   }     // rho0
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// T = L + R  ==> <ip||| T |||jp> = <ip||| L |||jp>  +  <ip||| R |||jp>
// Tensors L and R have SU(3) characker w0 and maximal multiplicity a0max.
//
// Tensor rmes are represented as a vector of (ip jp index) arrays and vector of rmes.
// ipjpindices: {(ip jp index)}, where index points to the first element in vector of rmes,
// which corresponds to {< ip||| L^a0 ||| jp>_rhot} with aip=ajp=a0=rhot=0.
// Size of the array of rmes is equal to the product of multiplicities aip ajp a0 and rhot.
// The order of rmes is irrelevant for the given algorithm, which merely adds two sets of rmes.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
void AddTensorRMEs(const lsu3::CncsmSU3xSU2Basis& basis, int a0max, const SU3::LABELS w0,
                   const IJ_Indices& ipjpindicesL, const std::vector<float>& rmesL,
                   const IJ_Indices& ipjpindicesR, const std::vector<float>& rmesR,
                   IJ_Indices& ipjpindices, std::vector<float>& rmes) {
   ipjpindices.reserve(ipjpindicesL.size() + ipjpindicesR.size());
   rmes.reserve(rmesL.size() + rmesR.size());

   uint32_t rmesL_index = 0;
   uint32_t rmesR_index = 0;
   size_t iL, iR;

   // iterate over <ip||| L ||| jp> and <ip||| R ||| jp> rmes
   // TODO: rmesL_index and rmesR_index can be easily computed.
   // IJ_Indices do not need to contain an index into rmes as it
   // can be computed.
   for (iL = 0, iR = 0; iL < ipjpindicesL.size() && iR < ipjpindicesR.size();) {
      std::pair<uint32_t, uint32_t> L(ipjpindicesL[iL][0], ipjpindicesL[iL][1]);
      std::pair<uint32_t, uint32_t> R(ipjpindicesR[iR][0], ipjpindicesR[iR][1]);

      // compare (ip jp)L and (ip jp)R
      if (L > R) {
         // tensor L does not contain  <ip||| L |||jp>  => <ip||| L ||| jp> = 0
         // ==> <ip||| T ||| jp> = <ip||| R ||| jp>
         uint32_t ip = R.first;
         uint32_t jp = R.second;

         ipjpindices.push_back(std::array<uint32_t, 3>{ip, jp, (uint32_t)rmes.size()});
         SU3::LABELS wip(basis.getProtonSU3xSU2(ip));
         SU3::LABELS wjp(basis.getProtonSU3xSU2(jp));
         uint32_t nrmes =
             basis.getMult_p(ip) * basis.getMult_p(jp) * a0max * SU3::mult(wjp, w0, wip);
         for (size_t i = 0; i < nrmes; ++i) {
            rmes.push_back(rmesR[rmesR_index]);
            rmesR_index++;
         }
         iR++;
      } else if (L == R)  // ==> both <ip||| L ||| jp> and <ip||| R ||| jp> exist
      {
         uint32_t ip = L.first;
         uint32_t jp = L.second;

         ipjpindices.push_back(std::array<uint32_t, 3>{ip, jp, (uint32_t)rmes.size()});
         SU3::LABELS wip(basis.getProtonSU3xSU2(ip));
         SU3::LABELS wjp(basis.getProtonSU3xSU2(jp));
         uint32_t nrmes =
             basis.getMult_p(ip) * basis.getMult_p(jp) * a0max * SU3::mult(wjp, w0, wip);
         for (size_t i = 0; i < nrmes; ++i) {
            double d = (double)rmesL[rmesL_index] + (double)rmesR[rmesR_index];
            rmes.push_back(d);
            rmesL_index++;
            rmesR_index++;
         }
         iL++;
         iR++;
      } else  // if (L < R)
      {
         uint32_t ip = L.first;
         uint32_t jp = L.second;

         ipjpindices.push_back(std::array<uint32_t, 3>{ip, jp, (uint32_t)rmes.size()});
         SU3::LABELS wip(basis.getProtonSU3xSU2(ip));
         SU3::LABELS wjp(basis.getProtonSU3xSU2(jp));
         uint32_t nrmes =
             basis.getMult_p(ip) * basis.getMult_p(jp) * a0max * SU3::mult(wjp, w0, wip);
         for (size_t i = 0; i < nrmes; ++i) {
            rmes.push_back(rmesL[rmesL_index]);
            rmesL_index++;
         }
         iL++;
      }
   }

   if (iL < ipjpindicesL.size()) {
      for (; iL < ipjpindicesL.size(); ++iL) {
         uint32_t ip = ipjpindicesL[iL][0];
         uint32_t jp = ipjpindicesL[iL][1];

         ipjpindices.push_back(std::array<uint32_t, 3>{ip, jp, (uint32_t)rmes.size()});
         SU3::LABELS wip(basis.getProtonSU3xSU2(ip));
         SU3::LABELS wjp(basis.getProtonSU3xSU2(jp));
         uint32_t nrmes =
             basis.getMult_p(ip) * basis.getMult_p(jp) * a0max * SU3::mult(wjp, w0, wip);
         for (size_t i = 0; i < nrmes; ++i) {
            rmes.push_back(rmesL[rmesL_index]);
            rmesL_index++;
         }
      }
   } else if (iR < ipjpindicesR.size()) {
      for (; iR < ipjpindicesR.size(); ++iR) {
         uint32_t ip = ipjpindicesR[iR][0];
         uint32_t jp = ipjpindicesR[iR][1];

         ipjpindices.push_back(std::array<uint32_t, 3>{ip, jp, (uint32_t)rmes.size()});
         SU3::LABELS wip(basis.getProtonSU3xSU2(ip));
         SU3::LABELS wjp(basis.getProtonSU3xSU2(jp));
         uint32_t nrmes =
             basis.getMult_p(ip) * basis.getMult_p(jp) * a0max * SU3::mult(wjp, w0, wip);
         for (size_t i = 0; i < nrmes; ++i) {
            rmes.push_back(rmesR[rmesR_index]);
            rmesR_index++;
         }
      }
   }
}

/*
rmes
====
{<ip||| R^a0p ||| jp>_rhot}[ai][aj][a0p][rhot]
index: ai*ajmax*a0pmax*rhotmax + aj*a0pmax*rhotmax + a0p*rhotmax + rhot

da0a0p
======
[a0][a0p]
index: a0*a0pmax + a0p
*/
void ContractTensor(int a0max, int a0pmax, const SU3::LABELS& w0,
                    const lsu3::CncsmSU3xSU2Basis& basis, const std::vector<double>& da0a0p,
                    IJ_Indices& ipjpindices, std::vector<float>& rmes) {
   /*
   rmesResult
   ==========
   {<ip||| T^a0 ||| jp>_rhot}[ai][aj][a0][rhot]
   index: ai*ajmax*a0max*rhotmax + aj*a0max*rhotmax + a0p*rhotmax + rhot
   The size of "contracted" rme values will be a0max*(rmes.size()/a0pmax)
   */
   std::vector<float> rmesResult(a0max * (rmes.size() / a0pmax), 0);
   // -->resulting rmes[ai ip][aj jp][a0][rhot]
   size_t index_ai_aj_a0_rhot = 0;
   // iterate over <ip||| R || jp> rmes
   for (auto& ipjpindex : ipjpindices) {
      SU3::LABELS wip(basis.getProtonSU3xSU2(ipjpindex[0]));
      uint32_t aimax = basis.getMult_p(ipjpindex[0]);
      SU3::LABELS wjp(basis.getProtonSU3xSU2(ipjpindex[1]));
      uint32_t ajmax = basis.getMult_p(ipjpindex[1]);

      // index_rmes --> <jp||| R |||jp>[ai:0][aj:0][a0':0][rhot:0]
      size_t index_rmes = ipjpindex[2];
      // change index of rme accordingly
      ipjpindex[2] = index_ai_aj_a0_rhot;

      int rhotmax = SU3::mult(wjp, w0, wip);

      for (int ai = 0; ai < aimax; ++ai) {
         for (int aj = 0; aj < ajmax; ++aj) {
            // ai_aj_a0p0 -> [ai][aj][a0p:0][rhot:0] --> ai*ajmax*a0pmax*rhotmax +
            // aj*a0pmax*rhotmax + 0*rhotmax + 0
            //          size_t ai_aj_a0p0 = index_rmes + ai * ajmax * a0pmax * rhotmax + aj * a0pmax
            //          * rhotmax;
            size_t ai_aj_a0p0 = index_rmes + (ai * ajmax + aj) * a0pmax * rhotmax;
            // index_a0 --> d[a0][a0p:0], initialize with [a0:0][a0p:0]
            size_t index_a0 = 0;
            for (int a0 = 0; a0 < a0max; ++a0) {
               for (int rhot = 0; rhot < rhotmax; ++rhot) {
                  //   ai_aj_a0p_rhot --> [ai][aj][a0p:0][rhot]: ai*ajmax*a0pmax*rhotmax +
                  //   aj*a0pmax*rhotmax + 0 * rhotmax + rhot
                  size_t ai_aj_a0p_rhot = ai_aj_a0p0 + rhot;

                  double rme = 0.0;
                  for (int a0p = 0; a0p < a0pmax; ++a0p) {
                     rme += da0a0p[index_a0 + a0p] * (double)rmes[ai_aj_a0p_rhot];
                     // index_ai_aj_a0p_rhot --> [ai][aj][a0p+1][rhot]
                     ai_aj_a0p_rhot += rhotmax;
                  }  // a0p

                  rmesResult[index_ai_aj_a0_rhot] = rme;
                  index_ai_aj_a0_rhot++;
               }                    // rhot
               index_a0 += a0pmax;  // index_a0 --> d[a0+1][a0p:0]
            }                       // a0
         }                          // aj
      }                             // ai
   }

   rmes.swap(rmesResult);

   /*
      // for each ip jp a new index pointing to rmeResults vector
      // can be computed as a0max*(rmes.size()/a0pmax)
      for (auto ipjpindex : ipjpindicesResult) {
         assert(ipjpindex[2] % a0pmax == 0);
         ipjpindex[2] /= a0pmax;
         ipjpindex[2] *= a0max;
      }
   */
}

void ShowRMEs(const lsu3::CncsmSU3xSU2Basis& basis, int a0max, const SU3::LABELS& w0,
              const IJ_Indices& ipjpindices, const std::vector<float>& rmes) {
   // Show resulting rmes ...
   for (auto ipjp_index : ipjpindices) {
      uint32_t ip = ipjp_index[0];
      uint32_t jp = ipjp_index[1];
      uint32_t index = ipjp_index[2];
      SU3::LABELS wip(basis.getProtonSU3xSU2(ip));
      SU3::LABELS wjp(basis.getProtonSU3xSU2(jp));
      uint32_t rhotmax = SU3::mult(wjp, w0, wip);
      uint32_t nrmes = basis.getMult_p(ip) * basis.getMult_p(jp) * a0max * rhotmax;
      std::cout << ip << " " << jp << "\t";
      for (size_t i = 0; i < nrmes; ++i) {
         std::cout << rmes[index + i] << " ";
      }
      std::cout << std::endl;
   }
}

// Core algorighm
void ComputeRMEs(
    int a0max, const SU3::LABELS& w0, const lsu3::CncsmSU3xSU2Basis& basis,
    const std::map<TENSOR_LABELS, std::pair<std::string, std::vector<double>>>& tensor_expansion,
    std::vector<IJ_Indices>& tensor_ipjpindices, std::vector<std::vector<float>>& tensor_rmes,
    IJ_Indices& ipjpindices, std::vector<float>& rmes) {
   // take the first recoupled tensor from expansion
   auto tensor = tensor_expansion.begin();
   // get its drho0rhop coeffs
   auto& drho0rho0p = tensor->second.second;
   int a0pmax = tensor->first.IR0.rho;
   // Sum_{rho0p} d_{rho0 rhop} <ip ||| R^rho0p ||| jp>_rhot
   // NOTE: resulting ipjp_indices and rmes are returned in the initial input vectors
   ContractTensor(a0max, a0pmax, w0, basis, drho0rho0p, tensor_ipjpindices[0], tensor_rmes[0]);

   ipjpindices.swap(tensor_ipjpindices[0]);
   rmes.swap(tensor_rmes[0]);

   if (tensor_expansion.size() == 1) {
      return;
   }

   for (size_t i = 1; i < tensor_expansion.size(); ++i) {
      tensor++;
      int a0pmax = tensor->first.IR0.rho;
      auto& drho0rho0p = tensor->second.second;
      ContractTensor(a0max, a0pmax, w0, basis, drho0rho0p, tensor_ipjpindices[i], tensor_rmes[i]);

      IJ_Indices ipjpindices_tmp;
      std::vector<float> rmes_tmp;
      AddTensorRMEs(basis, a0max, w0, ipjpindices, rmes, tensor_ipjpindices[i], tensor_rmes[i],
                    ipjpindices_tmp, rmes_tmp);

      ipjpindices.swap(ipjpindices_tmp);
      rmes.swap(rmes_tmp);
   }
}

// TODO: Instead of working with proton interface to proton-neutron basis, we should work
// in a basis of A-fermion irreps spanning up to some Nmax
int main(int argc, char** argv) {
   if (argc != 4) {
      std::cout << "Usage: " << argv[0] << " <A> <Nmax> <tensor list file>" << endl;
      std::cout << endl;
      std::cout
          << "Compute reduced matrix elements (rmes) <j ||| T^rho0 (lm0 mu0)S0 ||| j>_rhot "
             "for two-body tensors for A identical nucleons and the complete Nmax model space."
          << std::endl;
      std::cout << "A set of input tensors is provided in <tensor list file> with the following "
                   "structure:"
                << std::endl;
      std::cout << "n1 n2 n3 n4 lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0" << std::endl;
      return EXIT_FAILURE;
   }
   int A = atoi(argv[1]);
   int Nmax = atoi(argv[2]);
   // NOTE: only proton irreps are needed. Create a smallest proton-neutron basis
   // that will construct complete Nmax set of proton  U(3) irreps for Z=A protons.
   lsu3::CncsmSU3xSU2Basis basis(proton_neutron::ModelSpace(A, 1, Nmax), 0, 1);

   k_dependent_tensor_strenghts = false;
   su3::init();
   // Needed to carry out recoupling transformation
   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   // n1 n2 n3 n4 lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0
   std::vector<std::array<int, 13>> tensors = ReadListOfTensors(argv[3]);

   // iterate over {a+n1 x a+n2}^(lmf muf)S2 x {tan3 x tan4}^(lmi mui)Si]^{*(lm0 mu0)S0} tensors
   for (const auto& tensor : tensors) {
      // spin degree of freedom are not involved in the given transformation.
      SU3::LABELS w0(1, tensor[adadtataLABELS::LM0], tensor[adadtataLABELS::MU0]);
      int rho0max =
          SU3::mult(SU3::LABELS(1, tensor[adadtataLABELS::LMF], tensor[adadtataLABELS::MUF]),
                    SU3::LABELS(1, tensor[adadtataLABELS::LMI], tensor[adadtataLABELS::MUI]), w0);
      w0.rho = rho0max;

      std::cout << "Tensor: [{a+" << tensor[adadtataLABELS::N1] << "a+"
                << tensor[adadtataLABELS::N2] << "}^(" << tensor[adadtataLABELS::LMF] << " "
                << tensor[adadtataLABELS::MUF] << ")" << tensor[adadtataLABELS::SSF];
      std::cout << " x {ta" << tensor[adadtataLABELS::N3] << "ta" << tensor[adadtataLABELS::N4]
                << "}^(" << tensor[adadtataLABELS::LMI] << " " << tensor[adadtataLABELS::MUI] << ")"
                << tensor[adadtataLABELS::SSI];
      std::cout << "]^rho0max:" << rho0max << "(" << tensor[adadtataLABELS::LM0] << " "
                << tensor[adadtataLABELS::MU0] << ")" << tensor[adadtataLABELS::SS0] << std::endl;

      // Obtain expansion of a given [a+a+tata] tensor in terms of LSU3shell compatible tensors
      std::map<TENSOR_LABELS, std::pair<std::string, std::vector<double>>> tensor_expansion;

      std::cout << "Recoupling [a+a+tata] tensor ... ";
      std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
      RecoupleTensor(tensor, tensor_expansion);
      std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
      std::cout << "Done in " << duration.count() << "." << std::endl;

      // ShowTensorExpansion(rho0max, tensor_expansion);

      // For all tensors in expansion, read rmes from external files into memory
      // TODO: for large rmes files we may run out of memory ...
      // NOTE: For different tensors that have the same n1 n2 n3 n4 HO shell numbers, the same set
      // of recoupled tenors are needed. Therefore we will read the same rmes multiple times.
      std::vector<IJ_Indices> tensor_ipjpindices;
      std::vector<std::vector<float>> tensor_rmes;
      try
      {
         ReadTensorRMEsIntoMemory(basis, tensor_expansion, tensor_ipjpindices, tensor_rmes);
      } catch (const std::exception& e) 
      {
         std::cout
             << "A given tensor was not generated by ComputeMultiShellRecoupledRMEs! It needs to "
                "be created or this tensor must be removed from the list of target su3 tensors."
             << std::endl
             << std::endl;
         return EXIT_FAILURE;
      }

      // rmes of the resulting tensor
      IJ_Indices ipjpindices;
      std::vector<float> rmes;
      std::cout << "Transforming rmes to [a+a+tata] form ... ";
      std::cout.flush();

      start = std::chrono::system_clock::now();
      ComputeRMEs(rho0max, w0, basis, tensor_expansion, tensor_ipjpindices, tensor_rmes,
                  ipjpindices, rmes);
      duration = std::chrono::system_clock::now() - start;

      std::cout << "Done in " << duration.count() << "." << std::endl;

      //      ShowRMEs(basis, rho0max, w0, ipjpindices, rmes);
      SaveRMEs_IJIndices(tensor, basis.NProtons(), basis.Nmax(), ipjpindices, rmes);
   }
   su3::finalize();
   return EXIT_SUCCESS;
}
