#include <SU3ME/SU3xSU2Basis.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <array>

//	This struct is used for remove_if algorithm ==> it has to return true if a given tensor is
// not included
struct lm0_plus_mu0_is_Odd {
   inline bool operator()(const SU3::LABELS& su3ir) const {
      // returns true if lm + mu is odd.
      return ((su3ir.lm + su3ir.mu) % 2);
   }
};

//	This struct is used for remove_if algorithm ==> it has to return true if a given tensor is
// not included
struct lm0_plus_mu0_is_Even {
   inline bool operator()(const SU3::LABELS& su3ir) const {
      // returns true if lm + mu is even
      return !((su3ir.lm + su3ir.mu) % 2);
   }
};

// This structs are used in GenerateAllCombinationsHOShells
// Returns true if selected
struct PositiveParity {
   // true if n1 + n2 + n3 + n4 is even
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((n1 + n2 + n3 + n4) % 2 == 0);
   }
};

// This structs are used in GenerateAllCombinationsHOShells
// Returns true if selected
struct NegativeParity {
   // true if n1 + n2 + n3 + n4 is odd
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((n1 + n2 + n3 + n4) % 2);
   }
};

enum { kNA = 0, kNB = 1, kNC = 2, kND = 3 };
typedef std::array<int, 4> NANBNCND;

const int SfSiS0[6][3] = {{0, 0, 0}, {2, 2, 0}, {0, 2, 2}, {2, 0, 2}, {2, 2, 2}, {2, 2, 4}};

template <typename SelectionRules>
void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell,
                                     std::vector<NANBNCND>& ho_shells_combinations) {
   SelectionRules IsIncluded;
   ho_shells_combinations.resize(0);

   NANBNCND na_nb_nc_nd;
   for (int na = 0; na <= maxShell; ++na) {
      for (int nb = 0; nb <= maxShell; ++nb) {
         int adxad_Nhw = 0;
         if (na > valence_shell) {
            adxad_Nhw += (na - valence_shell);
         }
         if (nb > valence_shell) {
            adxad_Nhw += (nb - valence_shell);
         }
         if (adxad_Nhw > Nmax) {
            continue;
         }
         for (int nc = 0; nc <= maxShell; ++nc) {
            for (int nd = 0; nd <= maxShell; ++nd) {
               int taxta_Nhw = 0;
               if (nc > valence_shell) {
                  taxta_Nhw += (nc - valence_shell);
               }
               if (nd > valence_shell) {
                  taxta_Nhw += (nd - valence_shell);
               }
               if (taxta_Nhw > Nmax) {
                  continue;
               }
               na_nb_nc_nd = {na, nb, nc, nd};

               if (IsIncluded(na, nb, nc, nd))  // operator has this combination of HO shells
               {
                  ho_shells_combinations.push_back(na_nb_nc_nd);  //	in such case we do not need
               }
            }
         }
      }
   }
}

void ShowTensor(int n1, int n2, int n3, int n4, const SU3::LABELS& wf, int ssf,
                const SU3::LABELS& wi, int ssi, const SU3::LABELS& w0, int ss0, int k0, int ll0,
                int jj0) {
   std::cout << n1 << " " << n2 << " " << n3 << " " << n4 << "   ";
   std::cout << (int)wf.lm << " " << (int)wf.mu << " " << ssf << "   ";
   std::cout << (int)wi.lm << " " << (int)wi.mu << " " << ssi << "   ";
   std::cout << (int)w0.lm << " " << (int)w0.mu << " " << ss0 << "   ";
   std::cout << k0 << " " << ll0 << " " << jj0 << std::endl;
}

int main(int argc, char** argv) {
   if (argc < 6) {
      std::cerr
          << "Print out quantum numbers \n n1 n2 n3 n4 wf Sf wi Si w0 S0 k0 2L0 2J0 \nassociated "
             "with selected set of proton-neutron tensors.\n\n"
          << "Usage: " << argv[0]
          << " <valence shell> <Nmax> <parity> <SU3 selection> <JJ0 selection>\n\n"
          << "\nArguments:\n\t<parity>: \n\t\t+1 for parity preserving tensors (n1+n2+n3+n4 is "
             "even)\n\t\t-1 for "
             "parity switching tensors (n1+n2+n3+n4 is odd).\n"
          << "\t<SU3 selection>: \n\t\t 0 ... select tensors with lm0 + mu0 even \n\t\t 1 ... select "
             "tensors with lm0 + mu0 odd \n\t\t-1 select all physically allowed tensors \n"
          << "\t<JJ0 selection>: list of selected 2J0 values; -1 if all possible J0 tensors "
             "will be generated."
          << std::endl;
      return EXIT_FAILURE;
   }
   int valence_shell = atoi(argv[1]);
   int Nmax = atoi(argv[2]);
   int nmax = Nmax + valence_shell;
   int parity = atoi(argv[3]);
   if (parity != 1 && parity != -1) {
      std::cerr << "Error: parity=" << parity
                << " is not valid value! Permitted values are either 1 for positive parity or -1 "
                   "for negative parity."
                << std::endl;
      return EXIT_FAILURE;
   }

   int selection = atoi(argv[4]);
   bool selectEven = false;
   bool selectOdd = false;
   if (selection == 0) {
      selectEven = true;
   } else if (selection == 1) {
      selectOdd = true;
   }
   else if (selection != -1)
   {
      std::cerr << "Wrong specification of SU(3) tensor selection: " << selection
                << "! Only 1, 0, or -1 are allowed values " << std::endl;
      return EXIT_FAILURE;
   }

   std::set<int> selectedJJ0;
   int JJ0 = atoi(argv[5]);
   if (JJ0 != -1) {
      selectedJJ0.insert(JJ0);
      for (int i = 6; i < argc; ++i) {
         JJ0 = atoi(argv[i]);
         selectedJJ0.insert(JJ0);
      }
   }
/*
   for (auto JJ0 : selectedJJ0) {
      std::cout << "JJ0:" << JJ0 << std::endl;
   }
*/

   std::vector<NANBNCND> ho_shells_combinations;
   if (parity == 1) {
      GenerateAllCombinationsHOShells<PositiveParity>(Nmax, valence_shell, nmax,
                                                      ho_shells_combinations);
   } else {
      GenerateAllCombinationsHOShells<NegativeParity>(Nmax, valence_shell, nmax,
                                                      ho_shells_combinations);
   }

   size_t ntensors = 0;
   SU3_VEC FinalIrreps;  //	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
   SU3_VEC InitIrreps;   //	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)
   for (auto nanbncnd : ho_shells_combinations) {
      int n1 = nanbncnd[0];
      int n2 = nanbncnd[1];
      int n3 = nanbncnd[2];
      int n4 = nanbncnd[3];

      FinalIrreps.clear();
      InitIrreps.clear();
      SU3::Couple(SU3::LABELS(n1, 0), SU3::LABELS(n2, 0), FinalIrreps);
      SU3::Couple(SU3::LABELS(0, n3), SU3::LABELS(0, n4), InitIrreps);

      //	iterate over (lmf muf) irreps
      for (auto wf : FinalIrreps) {
         //	iterate over (lmi mui) irreps
         for (auto wi : InitIrreps) {
            SU3_VEC Irreps0;
            SU3::Couple(wf, wi, Irreps0);  // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)

            if (selectEven) // ==> remove if (lm0 + mu0) == odd
            {
               Irreps0.erase(
                   std::remove_if(Irreps0.begin(), Irreps0.end(), lm0_plus_mu0_is_Odd()),
                   end(Irreps0));
            }
            else if (selectOdd) // ==> remove if (lm0 + mu0) == even
            {
               Irreps0.erase(
                   std::remove_if(Irreps0.begin(), Irreps0.end(), lm0_plus_mu0_is_Even()),
                   end(Irreps0));
            }

            for (auto w0 : Irreps0) {
               // iterate over 2Sf 2Si 2S0 combinations of spins for two-body operators
               for (int iS = 0; iS < 6; iS++) {
                  int ssf = SfSiS0[iS][0];
                  int ssi = SfSiS0[iS][1];
                  int ss0 = SfSiS0[iS][2];
                  if (JJ0 == -1) {
                     // maximal 2Jmax value
                     int JJmax = 2 * (w0.lm + w0.mu) + ss0;
                     // basis states of (lm mu)S irrep with J <= Jmax ==> we generate basis for all
                     // J values.
                     SU3xSU2::BasisJcut basis(SU3xSU2::LABELS(w0.lm, w0.mu, ss0), -1);
                     // iterate over L
                     for (basis.rewind(); !basis.IsDone(); basis.nextL()) {
                        for (int jj = basis.Jmin(); jj <= basis.Jmax(); jj += 2) {
                           for (int k0 = 0; k0 < basis.kmax(); ++k0) {
                              ShowTensor(n1, n2, n3, n4, wf, ssf, wi, ssi, w0, ss0, k0, basis.L(),
                                         jj);
                              ntensors += w0.rho;
                           }
                        }
                     }
                  } else {
                     // iterate over selected jj0 tensor values
                     for (auto jj0 : selectedJJ0) {
                        SU3xSU2::BasisJfixed basis(SU3xSU2::LABELS(w0.lm, w0.mu, ss0), jj0);
                        for (basis.rewind(); !basis.IsDone(); basis.nextL()) {
                           for (int k0 = 0; k0 < basis.kmax(); ++k0) {
                              ShowTensor(n1, n2, n3, n4, wf, ssf, wi, ssi, w0, ss0, k0, basis.L(),
                                         jj0);
                              ntensors += w0.rho;
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }  
//   std::cout << "number of tensors:" << ntensors << std::endl;
}
