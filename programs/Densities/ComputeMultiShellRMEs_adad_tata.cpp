#include "CSRMEMatrix.h"

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>
#include <su3dense/tensor_read.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

std::string GetTensorFileName(const std::vector<std::vector<char>> &single_shell_structures,
                              const std::vector<SU3xSU2_VEC> &single_shell_TensorLabels,
                              const SU3xSU2_VEC &omega) {
   int nshells = single_shell_structures.size();
   std::stringstream ss;
   ss << nshells << ".structure.";
   //  Information about shell tensor structure
   for (int i = 0; i < nshells; ++i) {
      const std::vector<char> &structure = single_shell_structures[i];
      assert(!structure.empty());
      for (size_t j = 0; j < structure.size() - 1; ++j) {
         ss << (int)structure[j] << "_";
      }
      ss << (int)structure.back() << ".";
   }

   ss << "SingleShellTensorLabels.";
   //  SU3xSU2 labels
   for (int i = 0; i < nshells; ++i) {
      const SU3xSU2_VEC &tensorLabels = single_shell_TensorLabels[i];
      for (size_t j = 0; j < tensorLabels.size() - 1; ++j) {
         ss << (int)tensorLabels[j].rho << "_";
         ss << (int)tensorLabels[j].lm << "_";
         ss << (int)tensorLabels[j].mu << "_";
         ss << (int)tensorLabels[j].S2 << "_";
      }
      ss << (int)tensorLabels.back().rho << "_";
      ss << (int)tensorLabels.back().lm << "_";
      ss << (int)tensorLabels.back().mu << "_";
      ss << (int)tensorLabels.back().S2 << ".";
   }
   //  SU3xSU2 labels describing coupling of single shell tensors
   ss << "Omega.";
   if (nshells > 1) {
      for (int j = 0; j < nshells - 2; ++j) {
         ss << (int)omega[j].rho << "_";
         ss << (int)omega[j].lm << "_";
         ss << (int)omega[j].mu << "_";
         ss << (int)omega[j].S2 << "_";
      }
      ss << (int)omega[nshells - 2].rho << "_";
      ss << (int)omega[nshells - 2].lm << "_";
      ss << (int)omega[nshells - 2].mu << "_";
      ss << (int)omega[nshells - 2].S2 << ".";
   }
   ss << "tensor";

   return ss.str();
}

void ShowOperator(char type, int eta1, int eta2, bool ad_type) {
   if (ad_type) {
      std::cout << "[a" << type << "+" << eta1 << " x a" << type << "+" << eta2 << "]";
   } else {
      std::cout << "[ta" << type << "+" << eta1 << " x ta" << type << "+" << eta2 << "]";
   }
}


// Can a given a+a+/tata perator couple proton (neutron) bra basis with proton (neutron) ket basis?
bool CanCouple(char type, int eta1, int eta2, bool ad_type,
               proton_neutron::ModelSpace &bra_ncsmModelSpace,
               proton_neutron::ModelSpace &ket_ncsmModelSpace) {
   int Af, Ai;

   if (type == 'p') {
      Af = bra_ncsmModelSpace.Z();
      Ai = ket_ncsmModelSpace.Z();
   } else {
      Af = bra_ncsmModelSpace.N();
      Ai = ket_ncsmModelSpace.N();
   }

   // [a+a+] => Af = Ai + 2 OR [tata] => Af = Ai - 2
   return (ad_type == true && Af == (Ai + 2)) || (ad_type == false && Af == (Ai - 2));
}

int getPhase(const SingleDistributionSmallVector &distr_i,
             const SingleDistributionSmallVector &distr_j, int eta1, int eta2,
             const std::vector<unsigned char> &hoShells) {
   std::vector<unsigned char> tensor(distr_i.size(), 0);
   // find an index in hoShells that contains HO shell number eta
   int index1 = std::distance(hoShells.begin(), std::find(hoShells.begin(), hoShells.end(), eta1));
   int index2 = std::distance(hoShells.begin(), std::find(hoShells.begin(), hoShells.end(), eta2));
   // index == hoShells.size() ==> a given shell eta is not an active shell ... should never happen
   assert(index1 < hoShells.size());
   assert(index2 < hoShells.size());

   // we have a single a+ or ta operator acting on eta1 shell
   tensor[index1] += 1;
   // we have a single a+ or ta operator acting on eta2 shell
   tensor[index2] += 1;
   return rme_uncoupling_phase(distr_i, tensor, distr_j);
}

// Can operator [a+_{\eta1} a+_{eta2}] couple given bra and ket distributions of nucleons over HO
// shell?
bool adad_canCoupleDistributions(int eta1, int eta2,
                                 const SingleDistributionSmallVector &distribution_bra,
                                 SingleDistributionSmallVector distribution_ket,
                                 const std::vector<unsigned char> &hoShells) {
   assert(eta1 <= eta2);
   for (size_t i = 0; i < hoShells.size(); i++) {
      if (hoShells[i] == eta1) {
         distribution_ket[i] += 1;
      }
      if (hoShells[i] == eta2) {
         distribution_ket[i] += 1;
         // since eta1 <= eta2 we can end iteration
         break;
      }
   }
   return (distribution_bra == distribution_ket);
}

// Can operator [ta_{\eta1} ta_{\eta2}] couple given bra and ket distributions of nucleons over HO
// shells?
bool tata_canCoupleDistributions(int eta1, int eta2,
                                 const SingleDistributionSmallVector &distribution_bra,
                                 SingleDistributionSmallVector distribution_ket,
                                 const std::vector<unsigned char> &hoShells) {
   assert(eta1 <= eta2);
   for (size_t i = 0; i < hoShells.size(); i++) {
      if (hoShells[i] == eta1) {
         distribution_ket[i] -= 1;
      }
      if (hoShells[i] == eta2) {
         distribution_ket[i] -= 1;
         // since eta1 <= eta2 we can end iteration
         break;
      }
   }
   return (distribution_bra == distribution_ket);
}

/////////////////////////////////////////////////////////////////////////
//////////////////////////// G R O U P S ////////////////////////////////
/////////////////////////////////////////////////////////////////////////
void GenerateProtonGroups(const lsu3::CncsmSU3xSU2Basis& basis, std::vector<CGroup>& g)
{
   SingleDistributionSmallVector distr_p;
   UN::SU3xSU2_VEC gamma_p;
   SU3xSU2_SMALL_VEC vW_p;

   CGroup current_Group;
   // iterate over proton irreps ip
   for (uint32_t ip = 0; ip < basis.pconf_size(); ip++) 
   {
      distr_p.resize(0);
      gamma_p.resize(0);
      vW_p.resize(0);

      basis.getDistr_p(ip, distr_p);
      basis.getGamma_p(ip, gamma_p);
      basis.getOmega_p(ip, vW_p);

      SU3_VEC omega_p(vW_p.size());
      std::vector<int> spins(vW_p.size());
      for (size_t i = 0; i < vW_p.size(); ++i)
      {
         omega_p[i] = SU3::LABELS(vW_p[i].rho, vW_p[i].lm, vW_p[i].mu);
         spins[i] = vW_p[i].S2;
      }

      if (current_Group.distr == distr_p && gamma_p == current_Group.gamma && omega_p == current_Group.omega)
      {
         g.back().irrep_ids.push_back(ip);
         g.back().spins.push_back(spins);
      }
      else // new group started ...
      {
         // store distr_ip, gamma_p, omega_p
         current_Group.distr = distr_p;
         current_Group.gamma = gamma_p;
         current_Group.omega = omega_p;
         // with current ip and spins as its first irrep
         current_Group.irrep_ids = {ip};
         current_Group.spins = {spins};
         g.push_back(current_Group);
      }
   }
}

void GenerateNeutronGroups(const lsu3::CncsmSU3xSU2Basis& basis, std::vector<CGroup>& g)
{
   SingleDistributionSmallVector distr_n;
   UN::SU3xSU2_VEC gamma_n;
   SU3xSU2_SMALL_VEC vW_n;

   CGroup current_Group;
   // iterate over neutron irreps in
   for (uint32_t in = 0; in < basis.nconf_size(); in++) 
   {
      distr_n.resize(0);
      gamma_n.resize(0);
      vW_n.resize(0);

      basis.getDistr_n(in, distr_n);
      basis.getGamma_n(in, gamma_n);
      basis.getOmega_n(in, vW_n);

      SU3_VEC omega_n(vW_n.size());
      std::vector<int> spins(vW_n.size());
      for (size_t i = 0; i < vW_n.size(); ++i)
      {
         omega_n[i] = SU3::LABELS(vW_n[i].rho, vW_n[i].lm, vW_n[i].mu);
         spins[i] = vW_n[i].S2;
      }

      if (current_Group.distr == distr_n && gamma_n == current_Group.gamma && omega_n == current_Group.omega)
      {
         g.back().irrep_ids.push_back(in);
         g.back().spins.push_back(spins);
      }
      else // new group started ...
      {
         // store distr_ip, gamma_p, omega_p
         current_Group.distr = distr_n;
         current_Group.gamma = gamma_n;
         current_Group.omega = omega_n;
         // with current ip and spins as its first irrep
         current_Group.irrep_ids = {in};
         current_Group.spins = {spins};
         g.push_back(current_Group);
      }
   }
}



// Compute < aip gip ||| a+/ta ||| ajp gjp>_{\rho=0}, {ip, jp, dSU2}, {ip', jp', dSU2'}, ... }
// for all possible values aip = 0, 1, ..., gip.mult = bra.getMult_p(ip)
// for all possible values ajp = 0, 1, ..., gjp.mult = ket.getMult_p(jp)
// Note that coupling jp  x Tensor --> ip is always multiplicity free
// if tensor character is either (lm 0) or (0 mu) as in our case
// jp x (lm 0) --> rhomax=1 & jp x (0 mu) --> rhomax=1
//
// Output: group_rmes is a hash table with
// [gi, gj] --> {std::vector{< aip gip ||| a+/ta ||| ajp gjp>_rho}, {{ip, jp, dSU2}, {ip', jp', dSU2'}, ... }
void CalculateMultiShellRME_adad_tata_ProtonsGroups(
    const lsu3::CncsmSU3xSU2Basis &bra, int eta1, int eta2, bool ad_type, CTensorStructure &tensor,
    const lsu3::CncsmSU3xSU2Basis &ket, std::vector<CGroup> &bra_groups,
    std::vector<CGroup> &ket_groups, GIGJ_RMES &group_rmes) {
   if (eta1 > eta2) {
      std::cerr << "Error! eta1:" << eta1 << " > eta2:" << eta2 << std::endl;
      exit(EXIT_FAILURE);
   }

   // set HO shells of a+/ta operators 
   std::vector<unsigned char> tensorShells;
   if (eta1 == eta2) { // single shell tensor
      tensorShells = {(unsigned char)eta1};
   } else {
      tensorShells = {(unsigned char)eta1, (unsigned char)eta2};
   }

   // Qantum labels of proton irreps
   SingleDistributionSmallVector distr_ip, distr_jp;
   UN::SU3xSU2_VEC gamma_ip, gamma_jp;
   SU3xSU2_SMALL_VEC vW_ip, vW_jp;

   std::cout << "Organizing bra and ket proton irreps into SU(3) equivalent groups ... "; std::cout.flush();
   auto computation_start = std::chrono::system_clock::now();

   std::vector<CGroup> gip, gjp;
   GenerateProtonGroups(bra, gip);
   GenerateProtonGroups(ket, gjp);

   std::chrono::duration<double> duration = std::chrono::system_clock::now() - computation_start;
   std::cout << duration.count() << " [s]" << std::endl;

   std::cout << "Computing rmes ... "; std::cout.flush();
   computation_start = std::chrono::system_clock::now();
/*
   std::cout << "Proton bra groups" << std::endl;
   for (size_t i = 0; i < gip.size(); ++i) {
      std::cout << "Group " << i << ":" << std::endl;
      CGroup group = gip[i];
      group.ShowContent();
   }
   std::cout << "number of bra proton irreps:" << bra.pconf_size() << std::endl;
   std::cout << "number of bra proton groups:" << gip.size() << std::endl;
   
   std::cout << "Proton ket groups" << std::endl;
   
   for (size_t i = 0; i < gjp.size(); ++i) {
      std::cout << "Group " << i << ":" << std::endl;
      CGroup group = gjp[i];
      group.ShowContent();
   }
   std::cout << "number of ket proton irreps:" << ket.pconf_size() << std::endl;
   std::cout << "number of ket proton groups:" << gip.size() << std::endl;
*/
 
   for (size_t igip = 0; igip < gip.size(); ++igip) {
      for (size_t igjp = 0; igjp < gjp.size(); ++igjp) {
         distr_ip = gip[igip].distr;
         gamma_ip = gip[igip].gamma;
         // we are interested in SU(3) intershell quantum numbers
         // hence we can take first irrep and disregard its intershell spins
         vW_ip.resize(0);
         bra.getOmega_p(gip[igip].irrep_ids.front(), vW_ip);

         distr_jp = gjp[igjp].distr;
         gamma_jp = gjp[igjp].gamma;
         // we are interested in SU(3) intershell quantum numbers
         // hence we can take first irrep and disregard its intershell spins
         vW_jp.resize(0);
         ket.getOmega_p(gjp[igjp].irrep_ids.front(), vW_jp);

         std::vector<unsigned char> hoShells;
         SingleDistributionSmallVector transformed_distribution_bra, transformed_distribution_ket;
         // Input:
         // distr_ip, distr_jp ... distribution of protons over HO shells
         //
         // Output:
         // hoShells ... HO shells obtained as union of shells occuring in distr_ip/jp
         // transformed_distribution_bra[i] == #number of protons at HO shell eta=hoShells[i]
         // transformed_distribution_ket[i] == #number of protons at HO shell eta=hoShells[i]
         //
         // For details see libraries/SU3ME/distr2gamma2omega.cpp
         Transform(distr_ip, distr_jp, hoShells, transformed_distribution_bra,
                   transformed_distribution_ket);
         distr_ip.swap(transformed_distribution_bra);
         distr_jp.swap(transformed_distribution_ket);

         // Check if a given tensor can possibly couple bra & ket proton distributions
         bool canCoupleDistr;
         if (ad_type) {
            canCoupleDistr = adad_canCoupleDistributions(eta1, eta2, distr_ip, distr_jp, hoShells);
         } else {
            canCoupleDistr = tata_canCoupleDistributions(eta1, eta2, distr_ip, distr_jp, hoShells);
         }

         if (!canCoupleDistr) {
            continue;
         }

         // number of empty shells in transformed bra distribution ip
         size_t num_vacuums_bra_distr = std::count(distr_ip.begin(), distr_ip.end(), 0);
         // number of empty shells in transformed ket distribution jp
         size_t num_vacuums_ket_distr = std::count(distr_jp.begin(), distr_jp.end(), 0);

         if (num_vacuums_bra_distr > 0) {
            //	augment gamma_bra with vacuum U(N)>SU(3) irreps and
            //	change omega_bra array of inter-shell coupling accordingly
            UN::SU3xSU2_VEC gamma_bra_vacuum_augmented;
            AugmentGammaByVacuumShells(distr_ip, gamma_ip, gamma_bra_vacuum_augmented);
            AugmentOmegaByVacuumShells(distr_ip, gamma_bra_vacuum_augmented,
                                       vW_ip);  // input: omega_bra output: omega_bra !!!
            gamma_ip.swap(gamma_bra_vacuum_augmented);
         }

         if (num_vacuums_ket_distr > 0)  // ==>	we must to add a vacuum shells into bra
         {
            //	augment gamma_ket with vacuum U(N)>SU(3) irreps and
            //	change omega_ket array of inter-shell coupling accordingly
            UN::SU3xSU2_VEC gamma_ket_vacuum_augmented;
            AugmentGammaByVacuumShells(distr_jp, gamma_jp, gamma_ket_vacuum_augmented);
            AugmentOmegaByVacuumShells(distr_jp, gamma_ket_vacuum_augmented,
                                       vW_jp);  // input: omega_bra output: omega_bra !!!
            gamma_jp.swap(gamma_ket_vacuum_augmented);
         }

         // Check whether single-shell U(N)>SU(3) in bra and ket irreps can couple
         // with a given tensor
         if (!tensor.Couple(gamma_ip, gamma_jp, hoShells, tensorShells)) {
            continue;
         }

         // Obtain phase due to antisymmetrization of multi-shell irreps
         int phase = getPhase(distr_ip, distr_jp, eta1, eta2, hoShells);
         //	Construct struct CRMECalculator which performs calculation of rme for
         //	multi-shell configurations using SU(3)xSU(2) RME reduction rule.
         CRMECalculator *rmeCalculator =
             tensor.GetRMECalculator(gamma_ip, gamma_jp, distr_jp, hoShells, tensorShells, phase);
         if (rmeCalculator == NULL) {
            continue;
         }
         bool spinless = true;
         SU3xSU2::RME *ptrRME = rmeCalculator->CalculateRME(gamma_ip, vW_ip, gamma_jp, vW_jp, spinless);
         if (!ptrRME)
         {
            continue;
         }

         // copy results from internal array of ptrRME into std::vector
         std::vector<float> resulting_rmes(ptrRME->m_rme, ptrRME->m_rme + ptrRME->m_ntotal);
/*         
         std::cout << "gip:" << igip << " gjp:" << igjp << " data : ";
         for (auto rme : resulting_rmes) {
            std::cout << rme << " ";
         }
         std::cout << std::endl;
*/         

         // finally, store a given <ip, jp> ---> std::vector{<aip ip ||| a+/ta ||| ajp jp>
         // in hash table
//         rmes[std::make_pair(igip, igjp)] = std::move(resulting_rmes);
         delete[] ptrRME->m_rme;
         delete ptrRME;

         std::vector<std::tuple<uint32_t, uint32_t, double>> ipjp_spin_vector;
         for (const auto ip : gip[igip].irrep_ids) {
            vW_ip.resize(0);
            bra.getOmega_p(ip, vW_ip);
            if (num_vacuums_bra_distr > 0) {
               AugmentOmegaByVacuumShells(distr_ip, gamma_ip,
                                          vW_ip);  // input: omega_bra output: omega_bra !!!
            }
            for (const auto jp : gjp[igjp].irrep_ids) {
               vW_jp.resize(0);
               ket.getOmega_p(jp, vW_jp);
               if (num_vacuums_ket_distr > 0)  // ==>	we must to add a vacuum shells into bra
               {
                  AugmentOmegaByVacuumShells(distr_jp, gamma_jp,
                                             vW_jp);  // input: omega_bra output: omega_bra !!!
               }

               double dSU2 = rmeCalculator->CalculateSpinCoeffRME(gamma_ip, vW_ip, gamma_jp, vW_jp);
               if (std::fabs(dSU2) < 1.0e-10)
               {
                  continue;
               }
               // store given {ip, jp, dSU2}
               ipjp_spin_vector.push_back(std::make_tuple(ip, jp, dSU2));
/*
               std::cout << "ip:" << gip[igip].irrep_ids[ip] << " jp:" <<  gjp[igjp].irrep_ids[jp] << " data : ";
               for (auto rme : resulting_rmes) {
                  std::cout << dSU2*rme << " ";
               }
               std::cout << std::endl;
*/

            } // jp
         } // ip

         // store current data: {gi, gj, {<gi|||T|||gj>}, { {ip, jp, dSU2}, {ip', jp', dSU2'}, ... }
         GROUP_RMES gigj_rmes;
         gigj_rmes.gi = igip;
         gigj_rmes.gj = igjp;
         gigj_rmes.su3_rmes = resulting_rmes;
         gigj_rmes.ijC = ipjp_spin_vector;
         group_rmes.push_back(gigj_rmes);

         // to properly delete all elements of CRMECalculator::vector<RME*> m_Gamma_t
         // that are stored in memory_pool_rme
         rmeCalculator->~CRMECalculator();
         // delete a given CRMECalculator* from memory_pool::crmecalculator
         memory_pool::crmecalculator.free(rmeCalculator);
      }
   }
   bra_groups = std::move(gip);
   ket_groups = std::move(gjp);

   duration = std::chrono::system_clock::now() - computation_start;
   std::cout << duration.count() << " [s]" << std::endl;
}

void CalculateMultiShellRME_adad_tata_NeutronsGroups(
    const lsu3::CncsmSU3xSU2Basis &bra, int eta1, int eta2, bool ad_type, CTensorStructure &tensor,
    const lsu3::CncsmSU3xSU2Basis &ket, std::vector<CGroup> &bra_groups,
    std::vector<CGroup> &ket_groups, GIGJ_RMES &group_rmes) {
   if (eta1 > eta2) {
      std::cerr << "Error! eta1:" << eta1 << " > eta2:" << eta2 << std::endl;
      exit(EXIT_FAILURE);
   }

   std::vector<unsigned char> tensorShells;
   if (eta1 == eta2) {
      tensorShells = {(unsigned char)eta1};
   } else {
      tensorShells = {(unsigned char)eta1, (unsigned char)eta2};
   }

   // a+/ta ==> only a single HO shell involved
   // Qantum labels of proton irreps
   SingleDistributionSmallVector distr_in, distr_jn;
   UN::SU3xSU2_VEC gamma_in, gamma_jn;
   SU3xSU2_SMALL_VEC vW_in, vW_jn;

   std::cout << "Organizing bra and ket neutron irreps into SU(3) equivalent groups ... "; std::cout.flush();
   auto computation_start = std::chrono::system_clock::now();

   std::vector<CGroup> gin, gjn;
   GenerateNeutronGroups(bra, gin);
   GenerateNeutronGroups(ket, gjn);

   std::chrono::duration<double> duration = std::chrono::system_clock::now() - computation_start;
   std::cout << duration.count() << " [s]" << std::endl;

   std::cout << "Computing rmes ... "; std::cout.flush();
   computation_start = std::chrono::system_clock::now();
/*
   std::cout << "Neutron bra groups" << std::endl;
   for (size_t i = 0; i < gip.size(); ++i) {
      std::cout << "Group " << i << ":" << std::endl;
      CGroup group = gip[i];
      group.ShowContent();
   }
   std::cout << "number of bra proton irreps:" << bra.pconf_size() << std::endl;
   std::cout << "number of bra proton groups:" << gip.size() << std::endl;
   
   std::cout << "Neutron ket groups" << std::endl;
   
   for (size_t i = 0; i < gjp.size(); ++i) {
      std::cout << "Group " << i << ":" << std::endl;
      CGroup group = gjp[i];
      group.ShowContent();
   }
   std::cout << "number of ket proton irreps:" << ket.pconf_size() << std::endl;
   std::cout << "number of ket proton groups:" << gip.size() << std::endl;
*/
   for (size_t igin = 0; igin < gin.size(); ++igin) {
      for (size_t igjn = 0; igjn < gjn.size(); ++igjn) {
         distr_in = gin[igin].distr;
         gamma_in = gin[igin].gamma;
         vW_in.resize(0);
         bra.getOmega_n(gin[igin].irrep_ids.front(), vW_in);

         distr_jn = gjn[igjn].distr;
         gamma_jn = gjn[igjn].gamma;
         vW_jn.resize(0);
         ket.getOmega_n(gjn[igjn].irrep_ids.front(), vW_jn);

         std::vector<unsigned char> hoShells;
         SingleDistributionSmallVector transformed_distribution_bra, transformed_distribution_ket;

         Transform(distr_in, distr_jn, hoShells, transformed_distribution_bra,
                   transformed_distribution_ket);
         distr_in.swap(transformed_distribution_bra);
         distr_jn.swap(transformed_distribution_ket);

         // Check if a given tensor can possibly couple bra & ket proton distributions
         bool canCoupleDistr;
         if (ad_type) {
            canCoupleDistr = adad_canCoupleDistributions(eta1, eta2, distr_in, distr_jn, hoShells);
         } else {
            canCoupleDistr = tata_canCoupleDistributions(eta1, eta2, distr_in, distr_jn, hoShells);
         }

         if (!canCoupleDistr) {
            continue;
         }

         // number of empty shells in transformed bra distribution ip
         size_t num_vacuums_bra_distr = std::count(distr_in.begin(), distr_in.end(), 0);
         // number of empty shells in transformed ket distribution jp
         size_t num_vacuums_ket_distr = std::count(distr_jn.begin(), distr_jn.end(), 0);

         if (num_vacuums_bra_distr > 0) {
            //	augment gamma_bra with vacuum U(N)>SU(3) irreps and
            //	change omega_bra array of inter-shell coupling accordingly
            UN::SU3xSU2_VEC gamma_bra_vacuum_augmented;
            AugmentGammaByVacuumShells(distr_in, gamma_in, gamma_bra_vacuum_augmented);
            AugmentOmegaByVacuumShells(distr_in, gamma_bra_vacuum_augmented,
                                       vW_in);  // input: omega_bra output: omega_bra !!!
            gamma_in.swap(gamma_bra_vacuum_augmented);
         }

         if (num_vacuums_ket_distr > 0)  // ==>	we must to add a vacuum shells into bra
         {
            //	augment gamma_ket with vacuum U(N)>SU(3) irreps and
            //	change omega_ket array of inter-shell coupling accordingly
            UN::SU3xSU2_VEC gamma_ket_vacuum_augmented;
            AugmentGammaByVacuumShells(distr_jn, gamma_jn, gamma_ket_vacuum_augmented);
            AugmentOmegaByVacuumShells(distr_jn, gamma_ket_vacuum_augmented,
                                       vW_jn);  // input: omega_bra output: omega_bra !!!
            gamma_jn.swap(gamma_ket_vacuum_augmented);
         }

         // Check whether single-shell U(N)>SU(3) in bra and ket irreps can couple
         // with a given tensor
         if (!tensor.Couple(gamma_in, gamma_jn, hoShells, tensorShells)) {
            continue;
         }

         // Obtain phase due to antisymmetrization of multi-shell irreps
         int phase = getPhase(distr_in, distr_jn, eta1, eta2, hoShells);
         //	Construct struct CRMECalculator which performs calculation of rme for
         //	multi-shell configurations using SU(3)xSU(2) RME reduction rule.
         CRMECalculator *rmeCalculator =
             tensor.GetRMECalculator(gamma_in, gamma_jn, distr_jn, hoShells, tensorShells, phase);
         if (rmeCalculator == NULL) {
            continue;
         }
         bool spinless = true;
         SU3xSU2::RME *ptrRME = rmeCalculator->CalculateRME(gamma_in, vW_in, gamma_jn, vW_jn, spinless);
         if (!ptrRME)
         {
            continue;
         }

         // copy results from internal array of ptrRME into std::vector
         std::vector<float> resulting_rmes(ptrRME->m_rme, ptrRME->m_rme + ptrRME->m_ntotal);
/*         
         std::cout << "gip:" << igip << " gjp:" << igjp << " data : ";
         for (auto rme : resulting_rmes) {
            std::cout << rme << " ";
         }
         std::cout << std::endl;
*/         

         // finally, store a given <ip, jp> ---> std::vector{<aip ip ||| a+/ta ||| ajp jp>
         // in hash table
//         rmes[std::make_pair(igip, igjp)] = std::move(resulting_rmes);
         delete[] ptrRME->m_rme;
         delete ptrRME;

         std::vector<std::tuple<uint32_t, uint32_t, double>> injn_spin_vector;
         for (const auto in : gin[igin].irrep_ids) {
            vW_in.resize(0);
            bra.getOmega_n(in, vW_in);
            if (num_vacuums_bra_distr > 0) {
               AugmentOmegaByVacuumShells(distr_in, gamma_in,
                                          vW_in);  // input: omega_bra output: omega_bra !!!
            }
            for (const auto jn : gjn[igjn].irrep_ids) {
               vW_jn.resize(0);
               ket.getOmega_n(jn, vW_jn);
               if (num_vacuums_ket_distr > 0)  // ==>	we must to add a vacuum shells into bra
               {
                  AugmentOmegaByVacuumShells(distr_jn, gamma_jn,
                                             vW_jn);  // input: omega_bra output: omega_bra !!!
               }

               double dSU2 = rmeCalculator->CalculateSpinCoeffRME(gamma_in, vW_in, gamma_jn, vW_jn);
               if (std::fabs(dSU2) < 1.0e-10)
               {
                  continue;
               }
               // store given {ip, jp, dSU2}
               injn_spin_vector.push_back(std::make_tuple(in, jn, dSU2));
/*
               std::cout << "ip:" << gip[igip].irrep_ids[ip] << " jp:" <<  gjp[igjp].irrep_ids[jp] << " data : ";
               for (auto rme : resulting_rmes) {
                  std::cout << dSU2*rme << " ";
               }
               std::cout << std::endl;
*/

            } // jp
         } // ip

         GROUP_RMES gigj_rmes;
         gigj_rmes.gi = igin;
         gigj_rmes.gj = igjn;
         gigj_rmes.su3_rmes = resulting_rmes;
         gigj_rmes.ijC = injn_spin_vector;
         group_rmes.push_back(gigj_rmes);

         // to properly delete all elements of CRMECalculator::vector<RME*> m_Gamma_t
         // that are stored in memory_pool_rme
         rmeCalculator->~CRMECalculator();
         // delete a given CRMECalculator* from memory_pool::crmecalculator
         memory_pool::crmecalculator.free(rmeCalculator);
      }
   }
   bra_groups = std::move(gin);
   ket_groups = std::move(gjn);

   duration = std::chrono::system_clock::now() - computation_start;
   std::cout << duration.count() << " [s]" << std::endl;
}

bool CalculateRMEs(const std::string &bra_space_definition_file_name,
                   const std::string &ket_space_definition_file_name, char type, int eta1, int eta2,
                   bool ad_type, int lm0, int mu0, int SS0) {
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   // time how long it will take to create bra and ket basis
   start = std::chrono::system_clock::now();
   proton_neutron::ModelSpace bra_ncsmModelSpace;
   proton_neutron::ModelSpace ket_ncsmModelSpace;

   bra_ncsmModelSpace.Load(bra_space_definition_file_name);
   ket_ncsmModelSpace.Load(ket_space_definition_file_name);

   // Check if operator characterized by type, eta1, eta2, da_type
   if (!CanCouple(type, eta1, eta2, ad_type, bra_ncsmModelSpace, ket_ncsmModelSpace)) {
      if (type == 'p') {
         std::cout << "<Zf:" << (int)bra_ncsmModelSpace.Z() << "|";
      } else {
         std::cout << "<Nf:" << (int)bra_ncsmModelSpace.N() << "|";
      }
      ShowOperator(type, eta1, eta2, ad_type);
      if (type == 'p') {
         std::cout << "|Zi:" << (int)ket_ncsmModelSpace.Z();
      } else {
         std::cout << "|Ni:" << (int)ket_ncsmModelSpace.N();
      }
      std::cout << "> is trivially vanishing due to the incompatible number of particles in bra space and ket space."
                << std::endl;
      return false;
   }

   lsu3::CncsmSU3xSU2Basis ket, bra;
   std::cout << "Starting constructing bra and ket basis for ndiag:1 ..." << std::endl;
   ket.ConstructBasis(ket_ncsmModelSpace, 0, 1);
   bra.ConstructBasis(bra_ncsmModelSpace, 0, 1);

   if (bra.Nmax() != ket.Nmax())
   {
      std::cerr << "Bra and ket model space must have the same Nmax parameter" << std::endl;
      exit(EXIT_FAILURE);
   }

   CBaseSU3Irreps baseSU3Irreps(std::max(bra.NProtons(), ket.NProtons()),
                                std::max(bra.NNeutrons(), ket.NNeutrons()),
                                std::max(bra.Nmax(), ket.Nmax()));

   duration = std::chrono::system_clock::now() - start;
   std::cout << "Time to construct basis: ... " << duration.count() << " [s]" << std::endl;

   // if eta1 > eta2 => we switch order of a+/ta operators
   // and compute RMEs that must be multiplied by phaseHOswitch
   int phaseHOswitch = 1;

   bool generate_rme = true;
   bool log_is_on = true;
   bool binaryIO = true;

   std::vector<SU3xSU2::LABELS> tensorLabels;
   std::vector<char> structure;
   // prepare data structure CTensorStructure needed for computation
   // of multi-shell rmes
   std::vector<CrmeTable *> singleShellTensorRmeTables;
   std::vector<SU3xSU2::LABELS *> vOmega;

   // Needed to generate output filename for RMEs with given input order of HO shells
   // if eta1 > eta2 => they differ from CTensorStructure
   std::vector<std::vector<char>> single_shell_structures;
   std::vector<SU3xSU2_VEC> single_shell_TensorLabels;
   SU3xSU2_VEC omega;

   if (eta1 == eta2)  // => [a+a+]/[tata] is a single shell operator
   {
      if (ad_type) {  // ==> [a+a+] case
         structure.push_back(eta1 + 1);
         structure.push_back(eta1 + 1);
         structure.push_back(0);
      } else {  // ==> [tata] case
         structure.push_back(-(eta1 + 1));
         structure.push_back(-(eta1 + 1));
         structure.push_back(0);
      }
      tensorLabels.push_back(SU3xSU2::LABELS(1, lm0, mu0, SS0));
      // Do not forget to delete (or use unique_ptr)
      singleShellTensorRmeTables.push_back(new CrmeTable(
          structure, tensorLabels, baseSU3Irreps, generate_rme, log_is_on, std::cout, binaryIO));
      //      singleShellTensorRmeTables[0]->Show(baseSU3Irreps);

      // vectors that are used to construct correct filename for output file with rmes
      single_shell_TensorLabels.push_back(tensorLabels);
      single_shell_structures.push_back(structure);
   } else  // [a+^{n1} x a+^{n2}]/ case [ta^{n1} x ta^{n2}]
   {
      bool swapped_ho_shells = false;
      // if (n1 > n2) => we will compute < ||| [n2 n1]^(lm0 mu0)S0||| > and use < ||| [n1 n2]^(lm0
      // mu0)S0||| > = (-)^(n1+n1-lm0-mu0-S0) x < ||| [n2 n1]^(lm0 mu0)S0||| >
      if (eta1 > eta2) {
         swapped_ho_shells = true;  // we need to change name accordingly
         // phase due to interchange of HO shells [n1 n2] --> [n2 n1]
         phaseHOswitch = std::pow(-1, eta1 + eta2 - lm0 - mu0 - SS0 / 2);
         std::cout << "phase:" << phaseHOswitch << std::endl;
         std::swap(eta1, eta2);
      }

      if (ad_type) {
         structure.push_back(eta1 + 1);
         tensorLabels.push_back(SU3xSU2::LABELS(1, eta1, 0, 1));
      } else {
         structure.push_back(-(eta1 + 1));
         tensorLabels.push_back(SU3xSU2::LABELS(1, 0, eta1, 1));
      }
      singleShellTensorRmeTables.push_back(new CrmeTable(
          structure, tensorLabels, baseSU3Irreps, generate_rme, log_is_on, std::cout, binaryIO));
      //      singleShellTensorRmeTables[0]->Show(baseSU3Irreps);

      // vectors that are used for output filename construction
      single_shell_structures.push_back(structure);
      single_shell_TensorLabels.push_back(tensorLabels);

      if (ad_type) {
         structure[0] = eta2 + 1;
         tensorLabels[0] = SU3xSU2::LABELS(1, eta2, 0, 1);
      } else {
         structure[0] = -(eta2 + 1);
         tensorLabels[0] = SU3xSU2::LABELS(1, 0, eta2, 1);
      }
      singleShellTensorRmeTables.push_back(new CrmeTable(
          structure, tensorLabels, baseSU3Irreps, generate_rme, log_is_on, std::cout, binaryIO));
      //      singleShellTensorRmeTables[1]->Show(baseSU3Irreps);

      // vectors that are used for output filename construction
      single_shell_structures.push_back(structure);
      single_shell_TensorLabels.push_back(tensorLabels);

      if (swapped_ho_shells) {
         // we swapped ho shells and hence file will contain rmes for original order of n1 and n2
         // given by input
         std::swap(single_shell_structures[0], single_shell_structures[1]);
         std::swap(single_shell_TensorLabels[0], single_shell_TensorLabels[1]);
      }

      vOmega.push_back(new SU3xSU2::LABELS(1, lm0, mu0, SS0));
      // vector is used for output filename construction
      omega.push_back(SU3xSU2::LABELS(1, lm0, mu0, SS0));
   }
   // the last argument of the constructor is a pointer to tables of SU(3)>SO(3) CG coefficients.
   // We do not need this in our calculation => set to nullptr
   CTensorStructure tensorStructure(singleShellTensorRmeTables, vOmega, nullptr);

   if (type == 'p') {
      std::cout << "Starting calculation of multi-shell proton rmes ...";
   } else {
      std::cout << "Starting calculation of multi-shell neutron rmes ...";
   }
   std::cout.flush();
   start = std::chrono::system_clock::now();

   uint32_t Ai, Af, Nmax;
   Nmax = bra.Nmax();

   std::vector<CGroup> bra_groups, ket_groups;
   IJ_RMES rmes;
   GIGJ_RMES group_rmes;
   if (type == 'p') {
      // resulting rmes depend only on nunber if fermions (Af, Ai) and maximal Nmax considered
      Af = bra.NProtons();
      Ai = ket.NProtons();
      CalculateMultiShellRME_adad_tata_ProtonsGroups(bra, eta1, eta2, ad_type, tensorStructure, ket,
                                                     bra_groups, ket_groups, group_rmes);
   } else {
      // resulting rmes depend only on nunber if fermions (Af, Ai) and maximal Nmax considered
      Af = bra.NNeutrons();
      Ai = ket.NNeutrons();
      CalculateMultiShellRME_adad_tata_NeutronsGroups(bra, eta1, eta2, ad_type, tensorStructure,
                                                      ket, bra_groups, ket_groups, group_rmes);
   }

   // phase due to switching HO shells 
   // resulting rmes must be multiplied by this phase
   if (phaseHOswitch == -1) {
      for (auto &ij_rme : rmes) {
         for (auto &rme : ij_rme.second) {
            rme *= -1;
         }
      }
   }

   std::cout << " Done" << std::endl;

   duration = std::chrono::system_clock::now() - start;
   std::cout << "Calculation multi-shell rmes for <Af:" << Af << "|"; 
   ShowOperator(type, eta1, eta2, ad_type);
   std::cout << "|Ai:" << Ai << "> and Nmax:" << Nmax << " took " << duration.count() << " [s]"
             << std::endl;

   if (!group_rmes.empty()) {
//      ShowRMEsTableIJ_RMES(type, rmes);
      std::string filename =
          GetTensorFileName(single_shell_structures, single_shell_TensorLabels, omega);
      filename += ".Af";
      filename += std::to_string(Af);
      filename += "_Ai";
      filename += std::to_string(Ai);
      filename += "_Nmax";
      filename += std::to_string(Nmax);

      GIGJRMES2IJRMES(group_rmes, rmes);
      if (eta1 == eta2) // ==> single-shell operator
      {
         // store results with an "old school" rme structure
         SaveRMEsIJ_RMES(filename, Af, Ai, Nmax, tensorLabels[0], rmes);

         // store to be compatible with RMES_TABLE
         filename += "_groups";
         SaveAsRMES_TABLE(filename, Af, Ai, Nmax, tensorLabels[0], bra_groups, ket_groups,
                           group_rmes);
         //         ShowRMEsTableIJ_RMES(rmes);
      }
      else
      {
         // store results with an "old school" rme structure
         SaveRMEsIJ_RMES(filename, Af, Ai, Nmax, omega[0], rmes);  // added G.S.

         // store to be compatible with RMES_TABLE
         filename += "_groups";
         SaveAsRMES_TABLE(filename, Af, Ai, Nmax, omega[0], bra_groups, ket_groups, group_rmes);
      //         ShowRMEsTableIJ_RMES(rmes);
      }
   } else {
      std::cout << "******************************************************" << std::endl;
      std::cout << "Resulting tensor is EMPTY ==> no output file produced!" << std::endl;
      std::cout << "******************************************************" << std::endl;
      exit(EXIT_SUCCESS);
   }
   return true;
}

int main(int argc, char **argv) {
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   if (su3shell_data_directory == NULL) {
      std::cerr << "System variable 'SU3SHELL_DATA' was not defined!" << std::endl;
      return EXIT_FAILURE;
   }

   if (argc != 9) {
      std::cerr
          << "Usage: " << argv[0]
          << " <bra model space> <ket model space> <+/-(n1+1)> <+/-(n2+1)> <p/n> <lm0> <mu0> <2S0>"
          << std::endl;
      std::cerr << "Compute multi-shell SU(3) reduced matrix elements." << std::endl;
      std::cerr << "+(n1+1) +(n2+1) p: compute <ip ||| [a+n1 x a+n2]^(lm0 mu0)S0 ||| jp>_{rhot} "
                   "for protons."
                << std::endl;
      std::cerr << "+(n1+1) +(n2+1) n: compute <in ||| [a+n1 x a+n2]^(lm0 mu0)S0 ||| jn>_{rhot} "
                   "for neutrons."
                << std::endl;
      std::cerr << "-(n1+1) -(n2+1) p: compute <ip ||| [tan1 x tan2]^(lm0 mu0)S0 ||| jp>_{rhot} "
                   "for protons."
                << std::endl;
      std::cerr << "-(n1+1) -(n2+1) n: compute <in ||| [tan1 x tan2]^(lm0 mu0)S0 ||| jn>_{rhot} "
                   "for neutrons."
                << std::endl;
      std::cerr << std::endl;
      std::cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one "
                   "can set the "
                   "size of look-up tables for 9lm, u6lm, z6lm, \"frequent\" 9lm symbols."
                << std::endl;
      return EXIT_FAILURE;
   }

   su3::init();

   std::string bra_space_definition_file_name(argv[1]);
   std::string ket_space_definition_file_name(argv[2]);
   bool ad_type = (atoi(argv[3]) > 0);  // true if a+; false if ta
   if (ad_type != (atoi(argv[4]) > 0)) {
      std::cerr << "Operator must be made up of either two creation or two annihilation operators!"
                << std::endl;
      return EXIT_FAILURE;
   }
   int eta1 = abs(atoi(argv[3])) - 1;  // HO shell number
   int eta2 = abs(atoi(argv[4])) - 1;  // HO shell number

   char nucleon_type = argv[5][0];
   if (nucleon_type != 'p' && nucleon_type != 'n') {
      std::cerr << "Wrong type of operator. Type '" << nucleon_type << "' is not defined."
                << std::endl;
      return EXIT_FAILURE;
   }
   int lm0 = atoi(argv[6]);
   int mu0 = atoi(argv[7]);
   int SS0 = atoi(argv[8]);

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   /////////////   main function //////////////////////
   bool success = CalculateRMEs(bra_space_definition_file_name, ket_space_definition_file_name,
                                nucleon_type, eta1, eta2, ad_type, lm0, mu0, SS0);
   ///////////////////////////////////////////////////
   if (!success) {
      return EXIT_FAILURE;
   }

   size_t num_frequent_u9, num_u9, num_u6, num_z6;
   CWig9lmLookUpTable<RME::DOUBLE>::coeffs_info(num_frequent_u9, num_u9, num_u6, num_z6);

//   std::cout << "number of u9: " << num_u9 << std::endl;
//   std::cout << "number of u6: " << num_u6 << std::endl;
//   std::cout << "number of z6: " << num_z6 << std::endl;
//   std::cout << "number of frequent u9: " << num_frequent_u9 << std::endl;

   size_t frequent_u9_size, u9_size, u6_size, z6_size;
   CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);

//   std::cout << "Size of frequent U9: " << frequent_u9_size / (1024.0 * 1024.0) << " [MB]"
//             << std::endl;
//   std::cout << "Size of U9: " << u9_size / (1024.0 * 1024.0) << " [MB]" << std::endl;
//   std::cout << "Size of U6: " << u6_size / (1024.0 * 1024.0) << " [MB]" << std::endl;
//   std::cout << "Size of Z6: " << z6_size / (1024.0 * 1024.0) << " [MB]" << std::endl;
//   size_t total = frequent_u9_size + u9_size + u6_size + z6_size;
//   std::cout << "Total size of memory: " << total / (1024.0 * 1024.0) << " [MB]" << std::endl;

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();       // clears memory allocated
                                                           // for U9, U6, and Z6
                                                           // coefficients
   CSSTensorRMELookUpTablesContainer::ReleaseMemory();     // clear memory allocated
                                                           // for single-shell SU(3)
                                                           // rmes
   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory
                                                           // allocated for
                                                           // SU(3)>SO(3)
                                                           // coefficients

   su3::finalize();
   return EXIT_SUCCESS;
}
