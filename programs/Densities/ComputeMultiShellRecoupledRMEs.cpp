#include <mpi.h>
#include <omp.h>

#include "CSRMEMatrix.h"

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/mpi.hpp>

// the following must correspond with CSRMEatrix<> template arguments in
// ComputeMultiShellRMEs.cpp:
using value_t = float;
using index_type = lsu3::CncsmSU3xSU2Basis::IRREP_INDEX;
// Columns and rows in the matrix have the same type as the proton/neutron irrep
// indices in the SU(3)-coupled ncsm basis
using CSRMEMatrixType = CSRMEMatrix<value_t, index_type>;

std::vector<CTensorStructure *> tensors;

void ShowTensors(const CInteractionPPNN &interactionPPNN, const CInteractionPN &interactionPN) {
   interactionPPNN.ShowRMETables();
   std::cout << "Size of memory: "
             << interactionPPNN.GetRmeLookUpTableMemoryRequirements() / (1024.0 * 1024.0) << " MB."
             << std::endl;
   std::cout << "********************************" << std::endl;
   std::cout << "Tensors from PPNN interaction: " << std::endl;
   std::cout << "********************************" << std::endl;
   interactionPPNN.ShowTensors();
   std::cout << "********************************" << std::endl;
   std::cout << "PN tensors from PN interaction: " << std::endl;
   std::cout << "********************************" << std::endl;
   interactionPN.ShowTensorGroupsAndTheirTensorStructures();
}

// Structure of header:
// A ... number of identical nucleons 
// Nmax ... many-body cutoff
uint64_t SaveHeader(int A, int Nmax, CTensorStructure *tensor_ptr,
                    MPI_File &fh)  // must be called only by rank 0
{
   using boost::mpi::get_mpi_datatype;

   uint64_t offset = 0;

   MPI_File_write_at(fh, offset, &A, 1, get_mpi_datatype(A), MPI_STATUS_IGNORE);
   offset += sizeof(A);

   MPI_File_write_at(fh, offset, &Nmax, 1, get_mpi_datatype(Nmax), MPI_STATUS_IGNORE);
   offset += sizeof(Nmax);

   uint64_t nshells = tensor_ptr->m_nShells;
   MPI_File_write_at(fh, offset, &nshells, 1, get_mpi_datatype(nshells), MPI_STATUS_IGNORE);
   offset += sizeof(nshells);

   //  Information about shell tensor structure
   for (int i = 0; i < nshells; ++i) {
      const auto &structure = tensor_ptr->m_SingleShellTensors[i]->GetStructure();
      assert(!structure.empty());

      uint64_t structure_size = structure.size();
      MPI_File_write_at(fh, offset, &structure_size, 1, get_mpi_datatype(structure_size),
                        MPI_STATUS_IGNORE);
      offset += sizeof(structure_size);
#if OMPI_MINOR_VERSION == 6
      MPI_File_write_at(fh, offset, (void *)structure.data(), structure_size,
                        get_mpi_datatype(structure[0]), MPI_STATUS_IGNORE);
#else
      MPI_File_write_at(fh, offset, structure.data(), structure_size,
                        get_mpi_datatype(structure[0]), MPI_STATUS_IGNORE);
#endif
      offset += structure_size * sizeof(structure[0]);
   }

   //  SU3xSU2 labels
   for (int i = 0; i < nshells; ++i) {
      const SU3xSU2_VEC &tensorLabels = tensor_ptr->m_SingleShellTensors[i]->GetTensorLabels();
      uint64_t tensorLabels_size = tensorLabels.size();
      MPI_File_write_at(fh, offset, &tensorLabels_size, 1, get_mpi_datatype(tensorLabels_size),
                        MPI_STATUS_IGNORE);
      offset += sizeof(tensorLabels_size);

      for (size_t j = 0; j < tensorLabels.size(); ++j) {
#if OMPI_MINOR_VERSION == 6
         MPI_File_write_at(fh, offset, (void *)&(tensorLabels[j].rho), 1,
                           get_mpi_datatype(tensorLabels[0].rho), MPI_STATUS_IGNORE);
#else
         MPI_File_write_at(fh, offset, &(tensorLabels[j].rho), 1,
                           get_mpi_datatype(tensorLabels[0].rho), MPI_STATUS_IGNORE);
#endif
         offset += sizeof(tensorLabels[0].rho);

#if OMPI_MINOR_VERSION == 6
         MPI_File_write_at(fh, offset, (void *)&(tensorLabels[j].lm), 1,
                           get_mpi_datatype(tensorLabels[0].lm), MPI_STATUS_IGNORE);
#else
         MPI_File_write_at(fh, offset, &(tensorLabels[j].lm), 1,
                           get_mpi_datatype(tensorLabels[0].lm), MPI_STATUS_IGNORE);
#endif
         offset += sizeof(tensorLabels[0].lm);

#if OMPI_MINOR_VERSION == 6
         MPI_File_write_at(fh, offset, (void *)&(tensorLabels[j].mu), 1,
                           get_mpi_datatype(tensorLabels[0].mu), MPI_STATUS_IGNORE);
#else
         MPI_File_write_at(fh, offset, &(tensorLabels[j].mu), 1,
                           get_mpi_datatype(tensorLabels[0].mu), MPI_STATUS_IGNORE);
#endif
         offset += sizeof(tensorLabels[0].mu);

#if OMPI_MINOR_VERSION == 6
         MPI_File_write_at(fh, offset, (void *)&(tensorLabels[j].S2), 1,
                           get_mpi_datatype(tensorLabels[0].S2), MPI_STATUS_IGNORE);
#else
         MPI_File_write_at(fh, offset, &(tensorLabels[j].S2), 1,
                           get_mpi_datatype(tensorLabels[0].S2), MPI_STATUS_IGNORE);
#endif
         offset += sizeof(tensorLabels[0].S2);
      }
   }
   //  SU3xSU2 labels describing coupling of single shell tensors
   if (nshells > 1) {
      for (int j = 0; j < nshells - 1; ++j) {
         MPI_File_write_at(fh, offset, &(tensor_ptr->m_Omega[j].rho), 1,
                           get_mpi_datatype(tensor_ptr->m_Omega[0].rho), MPI_STATUS_IGNORE);
         offset += sizeof(tensor_ptr->m_Omega[0].rho);

         MPI_File_write_at(fh, offset, &(tensor_ptr->m_Omega[j].lm), 1,
                           get_mpi_datatype(tensor_ptr->m_Omega[0].lm), MPI_STATUS_IGNORE);
         offset += sizeof(tensor_ptr->m_Omega[0].lm);

         MPI_File_write_at(fh, offset, &(tensor_ptr->m_Omega[j].mu), 1,
                           get_mpi_datatype(tensor_ptr->m_Omega[0].mu), MPI_STATUS_IGNORE);
         offset += sizeof(tensor_ptr->m_Omega[0].mu);

         MPI_File_write_at(fh, offset, &(tensor_ptr->m_Omega[j].S2), 1,
                           get_mpi_datatype(tensor_ptr->m_Omega[0].S2), MPI_STATUS_IGNORE);
         offset += sizeof(tensor_ptr->m_Omega[0].S2);
      }
   }

   return offset;
}

std::string GetTensorFileName(CTensorStructure *tensor_ptr) {
   int nshells = tensor_ptr->m_nShells;
   std::stringstream ss;
   ss << nshells << ".structure.";
   //  Information about shell tensor structure
   for (int i = 0; i < nshells; ++i) {
      const std::vector<char> &structure = tensor_ptr->m_SingleShellTensors[i]->GetStructure();
      assert(!structure.empty());
      for (size_t j = 0; j < structure.size() - 1; ++j) {
         ss << (int)structure[j] << "_";
      }
      ss << (int)structure.back() << ".";
   }

   ss << "SingleShellTensorLabels.";
   //  SU3xSU2 labels
   for (int i = 0; i < nshells; ++i) {
      const SU3xSU2_VEC &tensorLabels = tensor_ptr->m_SingleShellTensors[i]->GetTensorLabels();
      for (size_t j = 0; j < tensorLabels.size() - 1; ++j) {
         ss << (int)tensorLabels[j].rho << "_";
         ss << (int)tensorLabels[j].lm << "_";
         ss << (int)tensorLabels[j].mu << "_";
         ss << (int)tensorLabels[j].S2 << "_";
      }
      ss << (int)tensorLabels.back().rho << "_";
      ss << (int)tensorLabels.back().lm << "_";
      ss << (int)tensorLabels.back().mu << "_";
      ss << (int)tensorLabels.back().S2 << ".";
   }
   //  SU3xSU2 labels describing coupling of single shell tensors
   ss << "Omega.";
   if (nshells > 1) {
      for (int j = 0; j < nshells - 2; ++j) {
         ss << (int)tensor_ptr->m_Omega[j].rho << "_";
         ss << (int)tensor_ptr->m_Omega[j].lm << "_";
         ss << (int)tensor_ptr->m_Omega[j].mu << "_";
         ss << (int)tensor_ptr->m_Omega[j].S2 << "_";
      }
      ss << (int)tensor_ptr->m_Omega[nshells - 2].rho << "_";
      ss << (int)tensor_ptr->m_Omega[nshells - 2].lm << "_";
      ss << (int)tensor_ptr->m_Omega[nshells - 2].mu << "_";
      ss << (int)tensor_ptr->m_Omega[nshells - 2].S2 << ".";
   }
   ss << "tensor";

   return ss.str();
}

void TransformGammaKet_SelectByGammas(
    //	input
    const std::vector<unsigned char> &hoShells, const SingleDistributionSmallVectorBase &distr_ket,
    const unsigned char num_vacuums_ket_distr, const std::vector<int> &phase,
    const std::vector<CTensorGroup *> &tensorGroups, const std::vector<int> &phase_pn,
    const std::vector<CTensorGroup_ada *> &tensorGroups_pn,
    const UN::SU3xSU2_VEC &gamma_bra_vacuum_augmented,
    // output:
    UN::SU3xSU2_VEC &gamma_ket,
    std::vector<std::pair<CRMECalculator *, const CTensorStructure *>> &selected_tensors) {
   assert(selected_tensors.empty());

   UN::SU3xSU2_VEC gamma_ket_vacuum_augmented;
   if (num_vacuums_ket_distr > 0) {
      AugmentGammaByVacuumShells(distr_ket, gamma_ket, gamma_ket_vacuum_augmented);
      gamma_ket.swap(gamma_ket_vacuum_augmented);
   }
   // else if (num_vacuums_ket_distr == 0) ==> no shell with vacuum has to be
   // included ==> gamma_ket does not need to be augmented

   for (size_t i = 0, e = tensorGroups.size(); i < e; ++i)
      tensorGroups[i]->SelectTensorsByGamma(
          gamma_bra_vacuum_augmented, gamma_ket, distr_ket, hoShells, phase[i],
          selected_tensors);  // each RMECalculator::phase_ == vphase[i]

   for (size_t i = 0, e = tensorGroups_pn.size(); i < e; ++i)
      tensorGroups_pn[i]->SelectTensorsByGamma(gamma_bra_vacuum_augmented, gamma_ket, distr_ket,
                                               hoShells, phase_pn[i], selected_tensors);
}

void CalculateMultiShellRME_PP(const CInteractionPPNN &interactionPPNN,
                               const CInteractionPN &interactionPN,
                               const lsu3::CncsmSU3xSU2Basis &bra,
                               const lsu3::CncsmSU3xSU2Basis &ket) {
   // Structure facilitating dynamically allocated arrays release
   struct DeleteCRMECalculatorPtrs
       : public std::unary_function<const std::pair<CRMECalculator *, const CTensorStructure *> &,
                                    void> {
      void operator()(const std::pair<CRMECalculator *, const CTensorStructure *> &Item) const {
         Item.first->~CRMECalculator();
         memory_pool::crmecalculator.free(Item.first);
      }
   };

   // MPI initiation
   int my_rank, nprocs;
   boost::mpi::communicator mpi_comm_world;
   my_rank = mpi_comm_world.rank();
   nprocs = mpi_comm_world.size();

   if (my_rank == 0) {
      std::cout << "nprocs = " << nprocs << std::endl;
   }

   auto computation_start = std::chrono::system_clock::now();
   // tensor is represented by a pointer to CTensorStructure
   // resulting multi-shell reduced matrix elements are represented
   // by CSRMEMatrixType
   using tensors_matrices_table_t =
       std::map<const CTensorStructure *, std::unique_ptr<CSRMEMatrixType>>;
   // i-th element (i.e. thread_tensors_matrices_table[i]) stores results from i-th
   // thread
   std::vector<tensors_matrices_table_t> thread_tensors_matrices_table;

#pragma omp parallel
   {
      su3::init_thread();
      int threadsNum = omp_get_num_threads();
      int threadIdx = omp_get_thread_num();
#pragma omp master
      if (my_rank == 0) {
         std::cout << "threadsNum = " << threadsNum << std::endl;
      }

      // mapping of rows to MPI processes and threads:
      uint32_t n_workers = nprocs * threadsNum;
      uint32_t i_worker = my_rank * threadsNum + threadIdx;
      uint32_t div = bra.pconf_size() / n_workers;
      uint32_t mod = bra.pconf_size() % n_workers;
      uint32_t my_first_row, my_last_row;
      // i_workers: [0 ... mod-1] ==> compute (div+1) rows.
      // i_workers: [mod ... nworkers-1] ==> compute div rows.
      if (i_worker < mod) {
         my_first_row = (div + 1) * i_worker;
         my_last_row = my_first_row + div;
      } else {
         my_first_row = div * i_worker + mod;
         my_last_row = my_first_row + div - 1;
      }

#pragma omp single
      thread_tensors_matrices_table.resize(threadsNum);

      // for all tensors create a data structure representing RMEs
      for (auto tensor : tensors) {
#pragma omp critical
         thread_tensors_matrices_table[threadIdx][tensor] = std::make_unique<CSRMEMatrixType>(
             bra.pconf_size(), ket.pconf_size(), my_first_row, my_last_row);
      }
#pragma omp barrier
      for (uint32_t ip = my_first_row; ip <= my_last_row; ip++) {
         uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max());
         uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max());

         std::vector<unsigned char> hoShells_p;
         unsigned char num_vacuums_J_distr_p;

         // Groups of proton two body [a+a+aa] SU(3) tensors
         std::vector<CTensorGroup *> tensorGroupsPP;
         // Groups of proton one body [a+a] SU(3) tensors
         std::vector<CTensorGroup_ada *> tensorGroups_p_pn;

         // proton two body [a+a+aa] SU(3) tensors and proton one body [a+a] SU(3)
         // tensors
         std::vector<std::pair<CRMECalculator *, const CTensorStructure *>> selected_tensors;

         // Phase change due to exchange of a+ and a operators when computing
         // two-system rmes
         std::vector<int> phasePP;
         std::vector<int> phase_p_pn;

         // Qantum labels of proton irreps
         SingleDistributionSmallVector distr_ip, distr_jp;
         UN::SU3xSU2_VEC gamma_ip, gamma_jp;
         SU3xSU2_SMALL_VEC vW_ip, vW_jp;

         uint16_t icurrentDistr_p;
         uint32_t icurrentGamma_p;

         // loop over block columns
         for (uint32_t jp = 0, e = ket.pconf_size(); jp < e; jp++) {
            icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
            icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

            if (ilastDistr_p != icurrentDistr_p) {
               // If false -> current ket proton irrep has the same distribution as
               // the previous one [jp - 1]
               // This means we can skip selecting coupling tensors based on the
               // changes in the shell distribution of protons/neutrons.
               distr_ip.resize(0);
               bra.getDistr_p(ip, distr_ip);
               gamma_ip.resize(0);
               bra.getGamma_p(ip, gamma_ip);
               vW_ip.resize(0);
               bra.getOmega_p(ip, vW_ip);

               distr_jp.resize(0);
               ket.getDistr_p(jp, distr_jp);
               hoShells_p.resize(0);
               TransformDistributions_SelectByDistribution(
                   interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p,
                   num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
            }

            if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p) {
               // execute if either gammas or distributions have changed
               // it is not executed if jp and jp - 1 irreps have the same
               // distribution and gamma  <ip ||| ||| jp> and <ip ||| ||| jp-1> share
               if (!selected_tensors.empty()) {
                  std::for_each(selected_tensors.begin(), selected_tensors.end(),
                                DeleteCRMECalculatorPtrs());
               }

               selected_tensors.resize(0);

               if (tensorGroupsPP.size() || tensorGroups_p_pn.size()) {
                  gamma_jp.resize(0);
                  ket.getGamma_p(jp, gamma_jp);
                  TransformGammaKet_SelectByGammas(
                      hoShells_p, distr_jp, num_vacuums_J_distr_p, phasePP, tensorGroupsPP,
                      phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensors);
               }
            }

            if (selected_tensors.size()) {
               vW_jp.resize(0);
               ket.getOmega_p(jp, vW_jp);

               if (num_vacuums_J_distr_p > 0) AugmentOmegaByVacuumShells(distr_jp, gamma_jp, vW_jp);

               for (size_t i = 0, e = selected_tensors.size(); i < e; ++i) {
                  const CTensorStructure *selected_tensor = selected_tensors[i].second;
                  // return pointer to dynamicallty allocated array of rmes	==> it
                  // has to be deleted after it has been used
                  // rme is deleted in
                  // Reset_rmeCoeffs(vector<RmeCoeffsSU3SO3CGTablePointers> ) function
                  SU3xSU2::RME *pRME =
                      selected_tensors[i].first->CalculateRME(gamma_ip, vW_ip, gamma_jp, vW_jp);
                  if (pRME) {
                     /*
                                   std::cout << "ip:" << ip << " jp:" << jp << " data : ";
                                   for (size_t i = 0; i < pRME->m_ntotal; ++i) {
                                     std::cout << pRME->m_rme[i] << " ";
                                   }
                                   std::cout << std::endl;
                     */
                     // Move data stored in pRME into some CSR data structure
                     // tensors_data_table[selected_tensor] += pRME->m_ntotal;
                     auto &tensors_map = thread_tensors_matrices_table[threadIdx];
                     auto matrix_iter = tensors_map.find(selected_tensor);
                     assert(matrix_iter != tensors_map.end());
                     matrix_iter->second->addRMEs(ip, jp, pRME->m_rme, pRME->m_ntotal);
                     // allocated in CRMECalculator::CalculateRME
                     delete[] pRME->m_rme;
                     delete pRME;
                  }  // if( pRME))
               }     // for (size_t i = 0; i < selected_tensors.size(); ++i)
            }        // if (selected_tensors.size())
            ilastDistr_p = icurrentDistr_p;
            ilastGamma_p = icurrentGamma_p;
         }
         // we need to make sure to deallocate memory
         if (!selected_tensors.empty()) {
            std::for_each(selected_tensors.begin(), selected_tensors.end(),
                          DeleteCRMECalculatorPtrs());
         }
      }
// before calling finalize_thread we have to make sure all
// threads are done using su3lib and wigxjpf libraries
#pragma omp barrier
      su3::finalize_thread();
      for (auto &it : thread_tensors_matrices_table[threadIdx]) it.second->finalize();
   }  // #pragma omp parallel

   MPI_Barrier(MPI_COMM_WORLD);

   if (my_rank == 0)
      std::cout << "Computation finished. Time = "
                << std::chrono::duration<double>(std::chrono::system_clock::now() -
                                                 computation_start)
                           .count() /
                       1.0e9
                << " [s]" << std::endl;

   // MPI I/O errors are fatal
   MPI_File_set_errhandler(MPI_FILE_NULL, MPI_ERRORS_ARE_FATAL);

   // create output files for all tensors
   for (auto i_tensor_ptr : tensors) {
      std::string filename = GetTensorFileName(i_tensor_ptr);

      // delete file if it exists; MPI_File_open cannot do this :(
      std::remove(filename.c_str());

      MPI_File fh;
      MPI_File_open(MPI_COMM_WORLD, (char *)filename.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY,
                    MPI_INFO_NULL, &fh);

      MPI_Datatype mdt_uint64 = boost::mpi::get_mpi_datatype<uint64_t>();
      MPI_Datatype mdt_real = boost::mpi::get_mpi_datatype<typename CSRMEMatrixType::real_type>();
      MPI_Datatype mdt_index = boost::mpi::get_mpi_datatype<typename CSRMEMatrixType::index_type>();

      uint64_t real_bytesize = sizeof(typename CSRMEMatrixType::real_type);
      uint64_t index_bytesize = sizeof(typename CSRMEMatrixType::index_type);

      // calculate local array counts
      uint64_t local_values = 0;
      uint64_t local_colinds = 0;
      uint64_t local_values_rowptrs = 0;
      uint64_t local_colinds_rowptrs = 0;

      for (auto &i_t : thread_tensors_matrices_table) {
         const auto &matrix = (i_t.find(i_tensor_ptr))->second;
         local_values += matrix->get_values().size();
         local_colinds += matrix->get_colinds().size();
         local_values_rowptrs += matrix->get_values_rowptrs().size() - 1;
         local_colinds_rowptrs += matrix->get_colinds_rowptrs().size() - 1;
      }

      if (my_rank == nprocs - 1) {
         local_values_rowptrs++;
         local_colinds_rowptrs++;
      }

      // values local and global counts, and offsets
      uint64_t global_values;
      MPI_Allreduce(&local_values, &global_values, 1, mdt_uint64, MPI_SUM, MPI_COMM_WORLD);
      uint64_t local_values_offset;
      MPI_Exscan(&local_values, &local_values_offset, 1, mdt_uint64, MPI_SUM, MPI_COMM_WORLD);
      // undefined value after exscan :(
      if (my_rank == 0) {
         local_values_offset = 0;
      }
      // saved for row pointers shifting;
      uint64_t local_values_disp = local_values_offset;
      local_values_offset *= real_bytesize;

      // column indexes local and global counts, and offsets
      uint64_t global_colinds;
      MPI_Allreduce(&local_colinds, &global_colinds, 1, mdt_uint64, MPI_SUM, MPI_COMM_WORLD);
      uint64_t local_colinds_offset;
      MPI_Exscan(&local_colinds, &local_colinds_offset, 1, mdt_uint64, MPI_SUM, MPI_COMM_WORLD);
      if (my_rank == 0) local_colinds_offset = 0;
      uint64_t local_colinds_disp = local_colinds_offset;
      local_colinds_offset *= index_bytesize;

      if (i_tensor_ptr == tensors[0])
         std::cout << "Process " << my_rank << " global colinds = " << global_colinds
                   << ", local_colinds_offset = " << local_colinds_offset << std::endl;

      // values row pointers local and global counts, and offsets
      uint64_t global_values_rowptrs;
      MPI_Allreduce(&local_values_rowptrs, &global_values_rowptrs, 1, mdt_uint64, MPI_SUM,
                    MPI_COMM_WORLD);
      uint64_t local_values_rowptrs_offset;
      MPI_Exscan(&local_values_rowptrs, &local_values_rowptrs_offset, 1, mdt_uint64, MPI_SUM,
                 MPI_COMM_WORLD);
      if (my_rank == 0) local_values_rowptrs_offset = 0;
      local_values_rowptrs_offset *= index_bytesize;

      // colinds row pointers local and global counts, and offsets
      uint64_t global_colinds_rowptrs;
      MPI_Allreduce(&local_colinds_rowptrs, &global_colinds_rowptrs, 1, mdt_uint64, MPI_SUM,
                    MPI_COMM_WORLD);
      uint64_t local_colinds_rowptrs_offset;
      MPI_Exscan(&local_colinds_rowptrs, &local_colinds_rowptrs_offset, 1, mdt_uint64, MPI_SUM,
                 MPI_COMM_WORLD);
      if (my_rank == 0) local_colinds_rowptrs_offset = 0;
      local_colinds_rowptrs_offset *= index_bytesize;

      // write header
      uint64_t current_offset;
      if (my_rank == 0) {
         current_offset = SaveHeader(bra.NProtons(), bra.Nmax(), i_tensor_ptr, fh);

         // tensor matrix dimensions
         uint64_t n_rows = bra.pconf_size();
         uint64_t n_cols = ket.pconf_size();
         MPI_File_write_at(fh, current_offset + 0, &n_rows, 1, mdt_uint64,
                           MPI_STATUS_IGNORE);  // offset 0: number of rows
         MPI_File_write_at(fh, current_offset + 8, &n_cols, 1, mdt_uint64,
                           MPI_STATUS_IGNORE);  // 8: number of columns
         MPI_File_write_at(fh, current_offset + 16, &global_values, 1, mdt_uint64,
                           MPI_STATUS_IGNORE);  // 16: number of values
         MPI_File_write_at(fh, current_offset + 24, &global_colinds, 1, mdt_uint64,
                           MPI_STATUS_IGNORE);  // 24: number of column idxs
         MPI_File_write_at(fh, current_offset + 32, &real_bytesize, 1, mdt_uint64,
                           MPI_STATUS_IGNORE);  // 32: bytesize of real type
         MPI_File_write_at(fh, current_offset + 40, &index_bytesize, 1, mdt_uint64,
                           MPI_STATUS_IGNORE);  // 40: bytesize of idx type
         current_offset += 48;
      }
      MPI_Bcast(&current_offset, 1, mdt_uint64, 0, MPI_COMM_WORLD);

      // write matrix
      local_values_offset += current_offset;
      current_offset += real_bytesize * global_values;
      local_colinds_offset += current_offset;
      current_offset += index_bytesize * global_colinds;
      local_values_rowptrs_offset += current_offset;
      current_offset += index_bytesize * global_values_rowptrs;
      local_colinds_rowptrs_offset += current_offset;

      // iterate over tensors and associated CSR matrices
      // computed by each thread
      for (auto thread = thread_tensors_matrices_table.cbegin(),
                e_t = thread_tensors_matrices_table.cend();
           thread != e_t; ++thread) {
         const auto &matrix = (thread->find(i_tensor_ptr))->second;
         bool very_last_matrix = (my_rank == nprocs - 1) && (std::distance(thread, e_t) == 1);

         // write values
         const auto &values = matrix->get_values();
         MPI_File_write_at(fh, local_values_offset, (void *)values.data(), values.size(), mdt_real,
                           MPI_STATUS_IGNORE);
         local_values_offset += real_bytesize * values.size();

         // write colinds
         const auto &colinds = matrix->get_colinds();
         MPI_File_write_at(fh, local_colinds_offset, (void *)colinds.data(), colinds.size(),
                           mdt_index, MPI_STATUS_IGNORE);
         local_colinds_offset += index_bytesize * colinds.size();

         // shift values row pointers
         matrix->shift_values_rowptrs(local_values_disp);
         local_values_disp += values.size();
         // write values_rowptrs
         const auto &values_rowptrs = matrix->get_values_rowptrs();
         uint64_t count = very_last_matrix ? values_rowptrs.size() : values_rowptrs.size() - 1;
         MPI_File_write_at(fh, local_values_rowptrs_offset, (void *)values_rowptrs.data(), count,
                           mdt_index, MPI_STATUS_IGNORE);
         local_values_rowptrs_offset += index_bytesize * count;

         // shift colinds row pointers
         matrix->shift_colinds_rowptrs(local_colinds_disp);
         local_colinds_disp += colinds.size();
         // write colinds_rowptrs
         const auto &colinds_rowptrs = matrix->get_colinds_rowptrs();
         count = very_last_matrix ? colinds_rowptrs.size() : colinds_rowptrs.size() - 1;
         MPI_File_write_at(fh, local_colinds_rowptrs_offset, (void *)colinds_rowptrs.data(), count,
                           mdt_index, MPI_STATUS_IGNORE);
         local_colinds_rowptrs_offset += index_bytesize * count;
      }

      MPI_File_close(&fh);
      MPI_Barrier(MPI_COMM_WORLD);
   }
}

// operator_type == 1 ==> 1B operators
// operator_type == 2 ==> 2B operators
void CalculateRMEs(int Z, int Nmax, int  operator_type,
                   const std::string &su3_tensors_filename) {
   boost::mpi::communicator mpi_comm;
   int my_rank = mpi_comm.rank();

   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   // time how long it will take to create bra and ket basis
   start = std::chrono::system_clock::now();
   proton_neutron::ModelSpace ncsmModelSpace(Z, 1, Nmax);
   lsu3::CncsmSU3xSU2Basis ket, bra;
   if (my_rank == 0) {
      std::cout << "Starting constructing bra and ket basis..." << std::endl;
   }
   ket.ConstructBasis(ncsmModelSpace, 0, 1);
   bra.ConstructBasis(ncsmModelSpace, ket, 0, 1);

   duration = std::chrono::system_clock::now() - start;
   if (my_rank == 0) {
      std::cout << "Time to construct basis: ... " << duration.count() << " [s]" << std::endl;
   }

   // stringstream interaction_log_file_name;
   // interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
   // ofstream interaction_log_file(interaction_log_file_name.str().c_str());
   std::ofstream interaction_log_file("/dev/null");

   CBaseSU3Irreps baseSU3Irreps(bra.NProtons(), bra.NNeutrons(), bra.Nmax());

   // since root process will read rme files, it is save to let this
   // process to create missing rme files (for PPNN interaction) if they do
   // not exist
   bool log_is_on = false;
   bool generate_missing_rme = true;
   CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);
   // create an empty one as it is not needed
   CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on,
                                interaction_log_file);

   std::vector<std::pair<std::string, TENSOR_STRENGTH>> fileNameStrength(
       1, std::make_pair(su3_tensors_filename, 1.0));

   // we may need to construct single-shell rmes and hence we need 
   // SU(3)>SU(2)xU(1) CGs, which internally needs 3j and 6j
   su3::init();
   if (operator_type == 2)
   {
      interactionPPNN.LoadTwoBodyOperators(my_rank, fileNameStrength);
   }
   else
   {
      SU2::LABEL jj0(255), mm0(255);
      interactionPPNN.AddOneBodyOperators(my_rank, fileNameStrength, jj0, mm0);
   }
   su3::finalize();
// ShowTensors(interactionPPNN, interactionPN);

   interactionPPNN.GetTensors(tensors);

   if (my_rank == 0) {
      duration = std::chrono::system_clock::now() - start;
      std::cout << "LoadInteractionTerms took " << duration.count() << " [s]" << std::endl;

      std::cout << "Starting calculation of multi-shell proton rmes..." << std::endl;
      start = std::chrono::system_clock::now();
   }

   CalculateMultiShellRME_PP(interactionPPNN, interactionPN, bra, ket);

   if (my_rank == 0) {
      duration = std::chrono::system_clock::now() - start;
      std::cout << "Resulting time: " << duration.count() << " [s]" << std::endl;
   }

   // ShowTensorData();
}

int main(int argc, char **argv) {
   // MPI should be initialized with MPI_THREAD_SINGLE
   // i.e. by calling MPI_Init(...)
   boost::mpi::environment env(argc, argv);

   MECalculatorData dummy;
   int jj0 = dummy.JJ0();
   int mm0 = dummy.MM0();

   if ((jj0 != 0) || (mm0 != 0)) return EXIT_FAILURE;

   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;
   int my_rank, nprocs;

   boost::mpi::communicator mpi_comm_world;
   my_rank = mpi_comm_world.rank();
   nprocs = mpi_comm_world.size();
   // Important: files with tensors have dummy coefficients given without k0 dependence
   k_dependent_tensor_strenghts = false;
   if (su3shell_data_directory == NULL) {
      if (my_rank == 0)
         std::cerr << "System variable 'SU3SHELL_DATA' was not defined!" << std::endl;
      mpi_comm_world.abort(EXIT_FAILURE);
   }
   ///////////////////////////////////////////////////////////////////////////////
   // TODO: This code is limited to a+a and a+a+aa tensors only. Their list is
   // obtained from the Hamiltonian definition file name compatible with
   // LSU3shell. Also we would like to be able to compute RMEs of general tensors
   // such as, e.g., a+a+a, a+aa, etc. As these tensors may not preserve the
   // number of fermions, one should also enter the number of fermions in bra and
   // ket spaces. Our current goal is to use existing infrastructure, but clearly
   // we will need to implement new functionality sometimes later.
   ///////////////////////////////////////////////////////////////////////////////
   if (argc != 5) {
      if (my_rank == 0) {
         std::cerr << "Compute rmes of HO recoupled (a+ta) or (a+a+tata) tensors for a system of A "
                      "identical nucleons in Nmax model space."
                   << std::endl;

         std::cerr << "Usage: " << argv[0] << " <A> <Nmax> <rank> <1B/2B tensors file>"
                   << std::endl;
         std::cerr << "<A>: number of identical nucleons.\n";
         std::cerr <<"<Nmax>: include U(3) irreps with Nex <= Nmax\n";
         std::cerr << "<rank>: 1 ... one-body tensors, 2 ... two-body tensors" << std::endl;
         std::cerr << "<1B/2B tensors file> ... name of file with one-body or two-body tensors. ";
         std::cerr
             << "Use Generate_adta_SU3list or Generate_adadtata_SU3list to create such "
                "files.\n\n"
             << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can "
                "set the size of look-up tables for 9lm, u6lm, z6lm, \"frequent\" 9lm symbols."
             << std::endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

   int Z = atoi(argv[1]);
   int Nmax = atoi(argv[2]);
   int rank = atoi(argv[3]);
   if (rank != 1 && rank != 2)
   {
      std::cerr << "Uknown particle rank:" << rank << "! Allowed value is 1 or 2." << std::endl;
      return EXIT_FAILURE;
   }
   std::string su3_tensors_filename(argv[4]);

   CalculateRMEs(Z, Nmax, rank, su3_tensors_filename);

   // TODO: obtain and display maximal number of 9lm, 6lm and z6lm symbols across
   // all collaborating processes
   if (my_rank == 0) {
      size_t num_frequent_u9, num_u9, num_u6, num_z6;
      CWig9lmLookUpTable<RME::DOUBLE>::coeffs_info(num_frequent_u9, num_u9, num_u6, num_z6);

      std::cout << "number of u9: " << num_u9 << std::endl;
      std::cout << "number of u6: " << num_u6 << std::endl;
      std::cout << "number of z6: " << num_z6 << std::endl;
      std::cout << "number of frequent u9: " << num_frequent_u9 << std::endl;

      size_t frequent_u9_size, u9_size, u6_size, z6_size;
      CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);

      std::cout << "Size of frequent U9: " << frequent_u9_size / (1024.0 * 1024.0) << " [MB]"
                << std::endl;
      std::cout << "Size of U9: " << u9_size / (1024.0 * 1024.0) << " [MB]" << std::endl;
      std::cout << "Size of U6: " << u6_size / (1024.0 * 1024.0) << " [MB]" << std::endl;
      std::cout << "Size of Z6: " << z6_size / (1024.0 * 1024.0) << " [MB]" << std::endl;
      size_t total = frequent_u9_size + u9_size + u6_size + z6_size;
      std::cout << "Total size of memory: " << total / (1024.0 * 1024.0) << " [MB]" << std::endl;
   }

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();       // clears memory allocated
                                                           // for U9, U6, and Z6
                                                           // coefficients
   CSSTensorRMELookUpTablesContainer::ReleaseMemory();     // clear memory allocated
                                                           // for single-shell SU(3)
                                                           // rmes
   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory
                                                           // allocated for
                                                           // SU(3)>SO(3)
                                                           // coefficients

   return EXIT_SUCCESS;
}
