#ifndef SU3DENSEPN_HELPERS_H
#define SU3DENSEPN_HELPERS_H

#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <unordered_map>
#include <vector>

//#include <boost/container/small_vector.hpp>
#include <boost/functional/hash.hpp>

#include <LSU3/ncsmSU3xSU2Basis.h>

// unique sequence database for ncsmSU3xSU2 basis:

template <typename T = uint32_t, typename U = uint32_t>
class basis_useq_db
{
   public:
      using value_type = T;
      using index_type = U;

      using key_type = std::vector<T>;
//    using key_type = boost::container::small_vector<T, 8>;

      basis_useq_db(const lsu3::CncsmSU3xSU2Basis& basis)
         : basis_(basis)
      {
         auto ipin_block_count = basis_.NumberOfBlocks();
         useq_ipin_.resize(ipin_block_count);
      }

      void insert(U ipin_block, const key_type& seq)
      {
         assert(seq.size() == basis_.blockEnd(ipin_block) - basis_.blockBegin(ipin_block));

         const auto res = map_.emplace(seq, 0);

         auto iter = res.first;
         bool inserted = res.second;

         if (inserted)
         {
            U useq_wpn_index = useq_wpn_.size();
            useq_db_.push_back(useq_wpn_index);

            for (auto value : seq)
               useq_wpn_.push_back(value);

            iter->second = useq_db_.size() - 1;
         }

         useq_ipin_[ipin_block] = iter->second;
      }

      void finish_insertion()
      {
         useq_db_.push_back(useq_wpn_.size());

         map_type_{}.swap(map_);  // release memory
      }

      // number of useqs:
      size_t useqs() const { return useq_db_.size() - 1; }

      // number of values:
      size_t values() const { return useq_wpn_.size(); }

      // index of useq for ipin block
      U index(U ipin_block) const { return useq_ipin_[ipin_block]; }

      // pointer to first value of ipin block useq
      const T* begin(U useq_index) const
      {
         U value_index = useq_db_[useq_index];
         return useq_wpn_.data() + value_index;
      }

      // nubmer of values of ipin block useq
      const size_t count(U useq_index) const
      {
         U begin_value_index = useq_db_[useq_index];
         U end_value_index = useq_db_[useq_index + 1];
         return end_value_index - begin_value_index;
      }

      double memory_gib() const
      {
         size_t byte_size = useq_wpn_.size() * sizeof(T)
            + useq_db_.size() * sizeof(U)
            + useq_ipin_.size() * sizeof(U);

         return (double)byte_size / 1024.0 / 1024.0 / 1024.0;
      }

   private:
      const lsu3::CncsmSU3xSU2Basis& basis_;

      std::vector<T> useq_wpn_;   // sequences array
      std::vector<U> useq_db_;    // first sequence index in useq_wpn_
      std::vector<U> useq_ipin_;  // map: ipin_block -> useq_db_ index

      using map_type_ = std::unordered_map<key_type, U, boost::hash<key_type>>;
      map_type_ map_;
};

// useq (amax, cr_offset, and irrep indexes) databases for basis

template <typename T = uint32_t, typename U = uint32_t>
class basis_useq_dbs
{
   public:
      basis_useq_db<T, U> UAS_db;
      basis_useq_db<T, U> URCOS_db;
      basis_useq_db<T, U> UIIS_db;

      basis_useq_dbs(const lsu3::CncsmSU3xSU2Basis& basis)
         : UAS_db(basis), URCOS_db(basis), UIIS_db(basis)
      {
         typename basis_useq_db<T, U>::key_type UAS_seq;
         typename basis_useq_db<T, U>::key_type URCOS_seq;
         typename basis_useq_db<T, U>::key_type UIIS_seq;

         for (uint32_t ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++)
         {
            uint32_t ip = basis.getProtonIrrepId(ipin_block);
            uint32_t in = basis.getNeutronIrrepId(ipin_block);

            uint32_t row_first = basis.BlockPositionInSegment(ipin_block);
            uint32_t row = row_first;

            int aipmax = basis.getMult_p(ip);
            int ainmax = basis.getMult_n(in);

            const SU3xSU2::LABELS& proton_SU3xSU2 = basis.getProtonSU3xSU2(ip);
            const SU3xSU2::LABELS& neutron_SU3xSU2 = basis.getNeutronSU3xSU2(in);

            UAS_seq.clear();
            URCOS_seq.clear();
            UIIS_seq.clear();

            for (uint32_t iw = basis.blockBegin(ipin_block); iw < basis.blockEnd(ipin_block); iw++)
            {
               const auto& wf = basis.Get_Omega_pn_Basis(iw); 
               uint32_t rhomax = SU3::mult(proton_SU3xSU2, neutron_SU3xSU2, wf);
               uint32_t afmax = aipmax * ainmax * rhomax;

               UAS_seq.push_back(afmax);

               uint32_t row_offset = row - row_first;

               URCOS_seq.push_back(row_offset);

               row += afmax * basis.omega_pn_dim(iw);

               UIIS_seq.push_back(basis.wpn_[iw]);
            }

            UAS_db.insert(ipin_block, UAS_seq);
            URCOS_db.insert(ipin_block, URCOS_seq);
            UIIS_db.insert(ipin_block, UIIS_seq);
         }

         UAS_db.finish_insertion();
         URCOS_db.finish_insertion();
         UIIS_db.finish_insertion();
      }

      double memory_gib() const 
      {
         return UAS_db.memory_gib() + URCOS_db.memory_gib() + UIIS_db.memory_gib();
      }
};

#endif
