#include <LookUpContainers/CSU39lm.h>
#include <SU3ME/SU3xSU2Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <su3.h>

#include <iomanip>
#include <iostream>
#include <algorithm>
#include <iomanip>

//#define DEBUG_OUTPUT

struct SU2TENSOR
{
   int n1, l1, jj1, n2, l2, jj2, JJ1, n3, l3, jj3, n4, l4, jj4, JJ2, JJ0;
   void Show() const
   {
      std::cout << n1 << " " << l1 << " " << jj1 << "  ";
      std::cout << n2 << " " << l2 << " " << jj2 << "  ";
      std::cout << JJ1 << "   ";
      std::cout << n3 << " " << l3 << " " << jj3 << "  ";
      std::cout << n4 << " " << l4 << " " << jj4 << "  ";
      std::cout << JJ2 << "   ";
      std::cout << JJ0;
   }
};

// taken from C++ su3lib ... to be removed when merging with this library
int su3mult(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3) {
   int result = 0;
   int phase = lm1 + lm2 - lm3 - mu1 - mu2 + mu3;

   if (phase % 3 == 0) {
      int redphase;
      int l1, l2, l3, m1, m2, m3;

      redphase = phase / 3;
      if (phase >= 0) {
         l1 = lm1;
         l2 = lm2;
         l3 = lm3;
         m1 = mu1;
         m2 = mu2;
         m3 = mu3;
      } else {
         l1 = mu1;
         l2 = mu2;
         l3 = mu3;
         m1 = lm1;
         m2 = lm2;
         m3 = lm3;
         redphase = -redphase;
      }
      phase = redphase + m1 + m2 - m3;

      int invl1 = std::min(l1 - redphase, m2);
      if (invl1 < 0) {
         return result;
      }

      int invl2 = std::min(l2 - redphase, m1);
      if (invl2 < 0) {
         return result;
      }
      result = std::max(std::min(phase, invl2) - std::max(phase - invl1, 0) + 1, 0);
   }
   return result;
}

// taken from C++ su3lib ... to be removed when merging with this library
inline int KMAX(int lm, int mu, int L) {
   return (std::max(0, (lm + mu + 2 - L) / 2) - std::max(0, (lm + 1 - L) / 2) -
           std::max(0, (mu + 1 - L) / 2));
}

const int SSfSSiSS0[6][3] = {{0, 0, 0}, {2, 2, 0}, {0, 2, 2}, {2, 0, 2}, {2, 2, 2}, {2, 2, 4}};
const double PiSfSiS0[6] = {1, 3, 3, 3, sqrt(27), sqrt(45)};

enum { kSSf, kSSi, kSS0 };

enum { kN1, kN2, kN3, kN4, kJJ0 };
using N1N2N3N4JJ0 = std::array<int, 5>;
enum { WF, WI, W0 };
using WFWIW0 = std::array<SU3::LABELS, 3>;

struct TBDMES {
   std::vector<double> tbdmes;
   std::vector<int> k0l0;  /// k0 L0 k0' L0' ...
};

using WFWIW0_6SPINS_TBDMES = std::map<WFWIW0, std::array<TBDMES, 6>>;
using N1N2N3N4JJ0_TBDMES = std::map<N1N2N3N4JJ0, WFWIW0_6SPINS_TBDMES>;

//////////////////////////////////////////////////////////////////////////////////////
// Holds <(lm1 mu1) L1; (lm2 mu2) L2||(lm3 mu3) k3 L3>[L3][k3]
// for all values of L3 and k3.
// It is assumed that
//    kmax(L1) == kmax(L2) == 1
//    rho0max == 1
// and hence this array depends only on L3 and k3 values
//////////////////////////////////////////////////////////////////////////////////////
struct SU3CGs_Simple {
   std::vector<double> su3cgs_L3K3;
   // su3cgs_L3[Lf] --> <(lm1 mu1) L1; (lm2 mu2) L2||(lm3 mu3) k3:0 L3:Lf>[L3:Lf][0]
   std::vector<size_t> index;

   // Compute < (lm1 mu1) L1 (lm2 mu2) L2 || (lm3 mu3) k3 L3>[L3][k3]
   void ComputeSU3CGs(int lm1, int mu1, int L1, int lm2, int mu2, int L2, int lm3, int mu3);

   // return pointer to <w1 L1 w2 L2 || w3 k3:0 L3>
   inline double* get_first_coeff(int L3) {
      assert(index[L3] != -1);
      return &su3cgs_L3K3[index[L3]];
   }

   void ShowF(int lm1, int mu1, int L1, int lm2, int mu2, int L2, int lm3, int mu3) {
      for (int L3 = 0; L3 < index.size(); ++L3) {
         if (index[L3] != -1) {
            int k3max = KMAX(lm3, mu3, L3);
            for (int k3 = 0; k3 < k3max; ++k3) {
               std::cout << "<(" << lm1 << " " << mu1 << ")" << L1 << " (" << lm2 << " " << mu2
                         << ")" << L2 << "||(" << lm3 << " " << mu3 << ")kf" << k3 << " Lf:" << L3
                         << ">: " << su3cgs_L3K3[index[L3] + k3] << std::endl;
            }
         }
      }
   }
   void ShowI(int lm1, int mu1, int L1, int lm2, int mu2, int L2, int lm3, int mu3) {
      for (int L3 = 0; L3 < index.size(); ++L3) {
         if (index[L3] != -1) {
            int k3max = KMAX(lm3, mu3, L3);
            for (int k3 = 0; k3 < k3max; ++k3) {
               std::cout << "<(" << lm1 << " " << mu1 << ")" << L1 << " (" << lm2 << " " << mu2
                         << ")" << L2 << "||(" << lm3 << " " << mu3 << ")ki" << k3 << " Li:" << L3
                         << ">: " << su3cgs_L3K3[index[L3] + k3] << std::endl;
            }
         }
      }
   }
};

struct LFLI {
   int lf, li;
};

// Computes and stores SU(3) CGs of type:
// su3cgs: {<wf * *; wi * * || w0 * L0>*}
// where {Lf, Li} values are stored in lfli_vec
// The order of SU(3) CGs for a given Lf, Li is
// <wf kf Lf; wi ki Li || w0 k0 L0>rho0 [k0][ki][kf][rho0]
struct SU3CGS {
   // <wf * *; wi * * || w0 * L0>*
   std::vector<double> su3cgs;
   std::vector<LFLI> lfli_vec;
   // compute coefficients only for Lf Li such that
   // |l1-l2|<=Lf<=l1+l2
   // |l3-l4|<=Li<=l3+l4
   // and Lf x Li --> L0
   void ComputeSU3CGsLimited(const WFWIW0& wfwiw0, int l1, int l2, int l3, int l4, int L0);

   // compute coefficients for Lf Li such that
   // and Lf x Li --> L0
   void ComputeSU3CGs(const WFWIW0& wfwiw0, int L0);
   // show all coefficients 
   void Show(const WFWIW0& wfwiw0, int L0);

   // show coefficients only for Lf Li that:
   // |l1-l2|<=Lf<=l1+l2
   // |l3-l4|<=Li<=l3+l4
   void ShowRelevant(const WFWIW0& wfwiw0, int l1, int l2, int l3, int l4, int L0);
};

// Compute < (lm1 mu1) L1 (lm2 mu2) L2 || (lm3 mu3) k3 L3>[L3][k3]
void SU3CGs_Simple::ComputeSU3CGs(int lm1, int mu1, int L1, int lm2, int mu2, int L2, int lm3,
                                  int mu3) {
   index.clear();
   su3cgs_L3K3.clear();

   const int k1max = 1;
   const int k2max = 1;
   const int rhomax = 1;

   assert(su3::mult(lm1, mu1, lm2, mu2, lm3, mu3) == 1);
   assert(su3::kmax(lm1, mu1, L1) == 1);
   assert(su3::kmax(lm2, mu2, L2) == 1);

   // su3cgs_L3 is indexed by L3 values
   index.assign(lm3 + mu3 + 1, -1);
   // su3cgs[L3] contains index of appropriate element in su3cgs_L3k3
   size_t nsu3cgs = 0;
   for (int L3 = std::abs(L1 - L2); L3 <= (L1 + L2); ++L3) {
      int k3max = su3::kmax(lm3, mu3, L3);
      if (k3max) {
         index[L3] = nsu3cgs;
         nsu3cgs += k3max;
      }
   }

   std::vector<double> dCG;
   dCG.reserve(32);

   su3cgs_L3K3.resize(nsu3cgs);
   size_t index = 0;
   for (int L3 = std::abs(L1 - L2); L3 <= L1 + L2; ++L3) {
      int k3max = su3::kmax(lm3, mu3, L3);
      if (k3max == 0) {
         continue;
      }
      su3::wu3r3w(lm1, mu1, lm2, mu2, lm3, mu3, L1, L2, L3, k3max, k2max, k1max, rhomax, dCG);
      for (int k3 = 0; k3 < k3max; ++k3) {
         su3cgs_L3K3[index++] = dCG[k3];
      }
   }
}

void SU3CGS::Show(const WFWIW0& wfwiw0, int L0) {
   int k0max = SU3::kmax(wfwiw0[W0], L0);
   assert(k0max);
   int lmf = wfwiw0[WF].lm;
   int muf = wfwiw0[WF].mu;
   int lmi = wfwiw0[WI].lm;
   int mui = wfwiw0[WI].mu;
   int rho0max = wfwiw0[W0].rho;
   int lm0 = wfwiw0[W0].lm;
   int mu0 = wfwiw0[W0].mu;

   size_t index_su3cg = 0;
   for (const LFLI& lfli : lfli_vec) {
      int Lf = lfli.lf;
      int Li = lfli.li;
      int kfmax = SU3::kmax(wfwiw0[WF], Lf);
      int kimax = SU3::kmax(wfwiw0[WI], Li);
      for (int k0 = 0; k0 < k0max; ++k0) {
         for (int ki = 0; ki < kimax; ++ki) {
            for (int kf = 0; kf < kfmax; ++kf) {
               std::cout << "<(" << lmf << " " << lmi << ")ki:" << ki << " Li:" << Li << " (" << lmi
                         << " " << mui << ")ki:" << ki << " Li:" << Li << "||(" << lm0 << " " << mu0
                         << ")k0:" << k0 << " L0:" << L0 << ">[rho0]: ";
               for (int rho0 = 0; rho0 < rho0max; ++rho0) {
                  std::cout << su3cgs[index_su3cg] << "\t";
                  index_su3cg++;
               }
               std::cout << std::endl;
            }
         }
      }
   }
}

void SU3CGS::ShowRelevant(const WFWIW0& wfwiw0, int l1, int l2, int l3, int l4, int L0)
{
   int k0max = SU3::kmax(wfwiw0[W0], L0);
   assert(k0max);
   int lmf = wfwiw0[WF].lm;
   int muf = wfwiw0[WF].mu;
   int lmi = wfwiw0[WI].lm;
   int mui = wfwiw0[WI].mu;
   int rho0max = wfwiw0[W0].rho;
   int lm0 = wfwiw0[W0].lm;
   int mu0 = wfwiw0[W0].mu;

   size_t index_su3cg = 0;
   for (const LFLI& lfli : lfli_vec) {
      int Lf = lfli.lf;
      int Li = lfli.li;
      int kfmax = SU3::kmax(wfwiw0[WF], Lf);
      int kimax = SU3::kmax(wfwiw0[WI], Li);

      if (std::abs(l1 - l2) > Lf || (Lf > (l1 + l2))) {
         index_su3cg += k0max*kimax*kfmax*rho0max;
         continue;
      }
      if (std::abs(l3 - l3) > Li || (Li > (l3 + l4))) {
         index_su3cg += k0max*kimax*kfmax*rho0max;
         continue;
      }

      for (int k0 = 0; k0 < k0max; ++k0) {
         for (int ki = 0; ki < kimax; ++ki) {
            for (int kf = 0; kf < kfmax; ++kf) {
               std::cout << "<(" << lmf << " " << lmi << ")ki:" << ki << " Li:" << Li << " (" << lmi
                         << " " << mui << ")ki:" << ki << " Li:" << Li << "||(" << lm0 << " " << mu0
                         << ")k0:" << k0 << " L0:" << L0 << ">[rho0]: ";
               for (int rho0 = 0; rho0 < rho0max; ++rho0) {
                  std::cout << su3cgs[index_su3cg] << "\t";
                  index_su3cg++;
               }
               std::cout << std::endl;
            }
         }
      }
   }
}

// Compute <wf kf Lf; wi ki Li || w0 k0 L0> for all allowed Lf x Li --> L0 pairs
void SU3CGS::ComputeSU3CGs(const WFWIW0& wfwiw0, int L0) {
   // This subroutine must be called just once!
   assert(su3cgs.empty() && lfli_vec.empty());

   int lmf = wfwiw0[WF].lm;
   int muf = wfwiw0[WF].mu;
   int lmi = wfwiw0[WI].lm;
   int mui = wfwiw0[WI].mu;
   int rho0max = wfwiw0[W0].rho;
   int lm0 = wfwiw0[W0].lm;
   int mu0 = wfwiw0[W0].mu;

   int k0max = su3::kmax(lm0, mu0, L0);
   assert(k0max);
   assert(rho0max == su3::mult(lmf, muf, lmi, mui, lm0, mu0));

   std::vector<double> dCG;
   dCG.reserve(128);

   // how to estimate the number of Lf Li pairs?
   // maximal size: (lmf + muf)*(lmi + mui)
   lfli_vec.reserve(256);
   // create list of {Lf, Li} pairs and compute number
   // of SU(3) Wigner coefficients
   size_t nsu3wigcoeffs = 0;
   for (int Lf = 0; Lf <= (lmf + muf); ++Lf) {
      int kfmax = su3::kmax(lmf, muf, Lf);
      if (!kfmax) {
         continue;
      }
      for (int Li = 0; Li <= (lmi + mui); ++Li) {
         int kimax = su3::kmax(lmi, mui, Li);
         if (!kimax) {
            continue;
         }
         if ((L0 < std::abs(Lf - Li)) || L0 > ((Lf + Li))) {
            continue;
         }
         lfli_vec.push_back({Lf, Li});
         nsu3wigcoeffs += k0max * kimax * kfmax * rho0max;
      }
   }
   // clear excess memory
   lfli_vec.shrink_to_fit();

   // now we can reserve exact size of su3cgs
   su3cgs.reserve(nsu3wigcoeffs);
   for (const LFLI& lfli : lfli_vec) {
      int Lf = lfli.lf;
      int Li = lfli.li;
      int kfmax = su3::kmax(lmf, muf, Lf);
      int kimax = su3::kmax(lmi, mui, Li);
      su3::wu3r3w(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, k0max, kimax, kfmax, rho0max, dCG);
      su3cgs.insert(su3cgs.end(), dCG.begin(), dCG.end());
   }
   assert(su3cgs.size() == nsu3wigcoeffs);
}

// Compute <wf kf Lf; wi ki Li || w0 k0 L0> for |l1 - l2| <= Lf <= l1 + l2 && |l3 - l4| <= Li <= l3
// + l4
void SU3CGS::ComputeSU3CGsLimited(const WFWIW0& wfwiw0, int l1, int l2, int l3, int l4, int L0) {
   // This subroutine must be called just once!
   assert(su3cgs.empty() && lfli_vec.empty());

   int lmf = wfwiw0[WF].lm;
   int muf = wfwiw0[WF].mu;
   int lmi = wfwiw0[WI].lm;
   int mui = wfwiw0[WI].mu;
   int rho0max = wfwiw0[W0].rho;
   int lm0 = wfwiw0[W0].lm;
   int mu0 = wfwiw0[W0].mu;

   int k0max = su3::kmax(lm0, mu0, L0);
   assert(k0max);
   assert(rho0max == su3::mult(lmf, muf, lmi, mui, lm0, mu0));

   std::vector<double> dCG;
   dCG.reserve(128);

   // how to estimate the number of Lf Li pairs?
   // maximal size: (lmf + muf)*(lmi + mui)
   lfli_vec.reserve(256);
   // create list of {Lf, Li} pairs and compute number
   // of SU(3) Wigner coefficients
   size_t nsu3wigcoeffs = 0;
   for (int Lf = std::abs(l1 - l2); Lf <= (l1 + l2); ++Lf) {
      int kfmax = su3::kmax(lmf, muf, Lf);
      if (!kfmax) {
         continue;
      }
      for (int Li = std::abs(l3 - l4); Li <= (l3 + l4); ++Li) {
         int kimax = su3::kmax(lmi, mui, Li);
         if (!kimax) {
            continue;
         }
         if ((L0 < std::abs(Lf - Li)) || L0 > ((Lf + Li))) {
            continue;
         }
         lfli_vec.push_back({Lf, Li});
         nsu3wigcoeffs += k0max * kimax * kfmax * rho0max;
      }
   }
   // clear excess memory
   lfli_vec.shrink_to_fit();

   // now we set size of su3cgs to proper value
   su3cgs.reserve(nsu3wigcoeffs);
   for (const LFLI& lfli : lfli_vec) {
      int Lf = lfli.lf;
      int Li = lfli.li;
      int kfmax = su3::kmax(lmf, muf, Lf);
      int kimax = su3::kmax(lmi, mui, Li);
      su3::wu3r3w(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, k0max, kimax, kfmax, rho0max, dCG);
      su3cgs.insert(su3cgs.end(), dCG.begin(), dCG.end());
   }
   assert(su3cgs.size() == nsu3wigcoeffs);
}

void ReadSU3tbdmesFromInputFile(const std::string& filename, N1N2N3N4JJ0_TBDMES& tbdmes) {
   // ssfssiss0_index[(Sf, Si, S0)] ---> index in SSfSSiSS0 array
   // It has to correspond to the order of Sf Si S0 given by SSfSSiSS0[6][3].
   std::map<std::array<int, 3>, size_t> ssfssiss0_index = {{{0, 0, 0}, 0}, {{2, 2, 0}, 1},
                                                           {{0, 2, 2}, 2}, {{2, 0, 2}, 3},
                                                           {{2, 2, 2}, 4}, {{2, 2, 4}, 5}};
   std::ifstream su3_tbdmes_file(filename);
   if (!su3_tbdmes_file) {
      std::cerr << "Cannot open file '" << filename << "' with list of tensors." << std::endl;
      exit(EXIT_FAILURE);
   }
   int num_tbdmes;
   su3_tbdmes_file >> num_tbdmes;
   std::cout << "Reading " << num_tbdmes << " lines with tbdmes from file '" << filename << "'." << std::endl;
   for (int i = 0; i < num_tbdmes; ++i)
   {
      int n1, n2, n3, n4;
      int lmf, muf, ssf;
      int lmi, mui, ssi;
      int lm0, mu0, ss0;
      // TODO: In case of SU(3) labels that are guaranteed to be integers, I should work with l0/L0
      // rather then with ll0 = 2*l0/LL0=2*L0 values.
      int k0, ll0, jj0;

      su3_tbdmes_file >> n1;
      su3_tbdmes_file >> n2;
      su3_tbdmes_file >> n3;
      su3_tbdmes_file >> n4;

      su3_tbdmes_file >> lmf;
      su3_tbdmes_file >> muf;
      su3_tbdmes_file >> ssf;

      su3_tbdmes_file >> lmi;
      su3_tbdmes_file >> mui;
      su3_tbdmes_file >> ssi;

      su3_tbdmes_file >> lm0;
      su3_tbdmes_file >> mu0;
      su3_tbdmes_file >> ss0;

      su3_tbdmes_file >> k0;
      // NOTE that from input file we get 2*L0 but in memory we have L0
      su3_tbdmes_file >> ll0;
      su3_tbdmes_file >> jj0;

      int l0 = ll0 / 2;

      std::array<int, 3> ssfssiss0 = {ssf, ssi, ss0};
      size_t index_ssfssiss0 = ssfssiss0_index[ssfssiss0];
      assert(index_ssfssiss0 < 6);

      SU3xSU2::LABELS wf(lmf, muf, ssf);
      SU3xSU2::LABELS wi(lmi, mui, ssi);
      SU3xSU2::LABELS w0(lm0, mu0, ss0);
      w0.rho = SU3::mult(wf, wi, w0);

      WFWIW0 wfwiw0 = {wf, wi, w0};

      std::vector<double> tbdmes_rho(w0.rho, 0.0);
      for (size_t i = 0; i < w0.rho; ++i) {
         su3_tbdmes_file >> tbdmes_rho[i];
      }

      N1N2N3N4JJ0 n1n2n3n4JJ0 = {n1, n2, n3, n4, jj0};

      auto WFWIW0_6SPINS_TBDMES_iter = tbdmes.find(n1n2n3n4JJ0);
      if (WFWIW0_6SPINS_TBDMES_iter == tbdmes.end()) {
         TBDMES tbdmes_tmp;
         tbdmes_tmp.tbdmes = tbdmes_rho;
         tbdmes_tmp.k0l0 = {k0, l0};

         std::array<TBDMES, 6> ssfssiss0_tbdmes;
         ssfssiss0_tbdmes[index_ssfssiss0] = tbdmes_tmp;

         WFWIW0_6SPINS_TBDMES wfwiw0_6spins_tbdmes = {{wfwiw0, ssfssiss0_tbdmes}};

         // insert current n1n2n3n4JJ0 and initilize it
         auto current_wfwiw0 = tbdmes.insert(std::make_pair(n1n2n3n4JJ0, wfwiw0_6spins_tbdmes));
      } else {
         WFWIW0_6SPINS_TBDMES& wfwiw0_6spins_tbdmes = WFWIW0_6SPINS_TBDMES_iter->second;
         auto TBDMES_iter = wfwiw0_6spins_tbdmes.find(wfwiw0);
         if (TBDMES_iter == wfwiw0_6spins_tbdmes.end())  // wf wi w0 does not exist
         {
            TBDMES tbdmes_tmp;
            tbdmes_tmp.tbdmes = tbdmes_rho;
            tbdmes_tmp.k0l0 = {k0, l0};

            std::array<TBDMES, 6> ssfssiss0_tbdmes;
            ssfssiss0_tbdmes[index_ssfssiss0] = tbdmes_tmp;
            wfwiw0_6spins_tbdmes.insert(std::make_pair(wfwiw0, ssfssiss0_tbdmes));
         } else  // insert new data into existing arrays
         {
            TBDMES& tbdmes_mem = TBDMES_iter->second[index_ssfssiss0];
            tbdmes_mem.tbdmes.insert(tbdmes_mem.tbdmes.end(), tbdmes_rho.begin(), tbdmes_rho.end());
            tbdmes_mem.k0l0.push_back(k0);
            // TODO: change LL0 --> L0 in su3denseP/N family of codes
            tbdmes_mem.k0l0.push_back(l0);
         }
      }
   }
}

double ComputeSU2LfLi(int Lf, int Li, int L0, int ssf, int ssi, int ss0, int l1, int l2, int l3,
                      int l4, int jj1, int jj2, int jj3, int jj4, int JJ1, int JJ2, int JJ0) {
   double a = wigner9j(2 * Lf, 2 * Li, 2 * L0, ssf, ssi, ss0, JJ1, JJ2, JJ0);
   double b = wigner9j(2 * l1, 1, jj1, 2 * l2, 1, jj2, 2 * Lf, ssf, JJ1);
   double c = wigner9j(2 * l3, 1, jj3, 2 * l4, 1, jj4, 2 * Li, ssi, JJ2);
#ifdef DEBUG_OUTPUT
   std::cout << "a:" << a << std::endl;
   std::cout << "b:" << b << std::endl;
   std::cout << "c:" << c << std::endl;
   std::cout << "PiLfLi:" << std::sqrt((2 * Lf + 1) * (2 * Li + 1)) << std::endl;
#endif
   double result = std::sqrt((2 * Lf + 1) * (2 * Li + 1)) * a * b * c;
   return result;
}


void StoreSU2TBDME(std::ofstream& file, const SU2TENSOR& t, double tbdme) 
{
   file << t.n1 << " " << t.l1 << " " << t.jj1 << "  ";
   file << t.n2 << " " << t.l2 << " " << t.jj2 << "  ";
   file << t.JJ1 << "   ";
   file << t.n3 << " " << t.l3 << " " << t.jj3 << "  ";
   file << t.n4 << " " << t.l4 << " " << t.jj4 << "  ";
   file << t.JJ2 << "   ";
   file << t.JJ0;
   file << std::fixed << std::setw(19) << tbdme << std::endl;
}

double SU3toSU2(const SU2TENSOR& t, const N1N2N3N4JJ0_TBDMES& su3_tbdmes,
                std::map<WFWIW0, std::vector<SU3CGS>>& wfwiw0_su3cgs) {
   int n1 = t.n1, l1 = t.l1, jj1 = t.jj1;
   int n2 = t.n2, l2 = t.l2, jj2 = t.jj2;
   int JJ1 = t.JJ1;
   int n3 = t.n3, l3 = t.l3, jj3 = t.jj3;
   int n4 = t.n4, l4 = t.l4, jj4 = t.jj4;
   int JJ2 = t.JJ2;
   int JJ0 = t.JJ0;

   assert(KMAX(n1, 0, l1));
   assert(KMAX(n2, 0, l2));
   assert(KMAX(0, n3, l3));
   assert(KMAX(0, n4, l4));

   // result of this function ...
   double su2_tbdme = 0.0;

   auto wfwiw0_6spins_tbdmes_iter = su3_tbdmes.find({n1, n2, n3, n4, JJ0});
   if (wfwiw0_6spins_tbdmes_iter == su3_tbdmes.end()) {
      std::cerr << "n1:" << n1 << " n2:" << n2 << " n3:" << n3 << " n4:" << n4 << " JJ0:" << JJ0
                << " not found in su3 tbdmes tables! SU(2) tbdmes for this set of input parameters "
                   "will be vanishing."
                << std::endl;
      return 0.0;
//    exit(EXIT_FAILURE);
   }

   SU3CGs_Simple n1n2wfLf;
   SU3CGs_Simple n3n4wiLi;
   SU3::LABELS wf_last(255, 255, 255), wi_last(255, 255, 255);
   const WFWIW0_6SPINS_TBDMES& wfwiw0_6spins_tbdmes = wfwiw0_6spins_tbdmes_iter->second;
   // iterate over {wf, wi, w0} blocks of SU(3) tbdmes
   for (const auto& _6spins_tbdmes : wfwiw0_6spins_tbdmes) {
      const WFWIW0& wfwiw0 = _6spins_tbdmes.first;
      int lmf = wfwiw0[WF].lm;
      int muf = wfwiw0[WF].mu;
      int lmi = wfwiw0[WI].lm;
      int mui = wfwiw0[WI].mu;
      int rho0max = wfwiw0[W0].rho;
      int lm0 = wfwiw0[W0].lm;
      int mu0 = wfwiw0[W0].mu;

      std::vector<double> sum_kikf(rho0max, 0.0);
      std::vector<double> sum_kf(rho0max, 0.0);
#ifdef DEBUG_OUTPUT
      std::cout << "wf:(" << lmf << " " << muf << ") x wi:(" << lmi << " " << mui << ") --> w0:("
                << lm0 << " " << mu0 << ")" << std::endl;
#endif      
      const std::array<TBDMES, 6>& ssfssiss0_tbdmes = _6spins_tbdmes.second;

      if (wf_last != wfwiw0[WF]) {
         wf_last = wfwiw0[WF];
         // compute <(n1 0) l1; (n2 0) l2 || (lmf muf) kf Lf>[Lf][kf]
         n1n2wfLf.ComputeSU3CGs(n1, 0, l1, n2, 0, l2, lmf, muf);
      }
      // true if new wi ==> compute SU(3) cgs
      if (wi_last != wfwiw0[WI]) {
         wi_last = wfwiw0[WI];
         // compute <(0 n3) l3; (0 n4) l4 || (lmi mui) ki Li>[Li][ki]
         n3n4wiLi.ComputeSU3CGs(0, n3, l3, 0, n4, l4, lmi, mui);
      }

#ifdef DEBUG_OUTPUT
      n1n2wfLf.ShowF(n1, 0, l1, n2, 0, l2, lmf, muf);
      std::cout << std::endl;
      n3n4wiLi.ShowI(0, n3, l3, 0, n4, l4, lmi, mui);
#endif      

      // try to find <wf wi || w0 >
      auto wfwiw0_su3cgs_iter = wfwiw0_su3cgs.find(wfwiw0);
      // if it does not exist yet
      if (wfwiw0_su3cgs_iter == wfwiw0_su3cgs.end()) {
         // create an empty data structure
         wfwiw0_su3cgs_iter =
             (wfwiw0_su3cgs.insert(std::make_pair(wfwiw0, std::vector<SU3CGS>(lm0 + mu0 + 1))))
                 .first;
      }

      // L0_su3cgs[L0] --> <wf * wi * || w0 * L0>*
      std::vector<SU3CGS>& L0_su3cgs = wfwiw0_su3cgs_iter->second;
      assert(L0_su3cgs.size() == (lm0 + mu0 + 1));

      // iterate over (Sf, Si, S0) components of (wf, wi, w0) SU(3) tbdmes
      for (int ispin = 0; ispin < 6; ++ispin) {
         int ssf = SSfSSiSS0[ispin][kSSf];
         int ssi = SSfSSiSS0[ispin][kSSi];
         int ss0 = SSfSSiSS0[ispin][kSS0];

         // < Jf || [wfSf wiSi]^w0S0k0L0J0 || Ji>[k0l0][rho0]
         const std::vector<double>& su3_tbdmes = ssfssiss0_tbdmes[ispin].tbdmes;
         const std::vector<int>& k0l0 = ssfssiss0_tbdmes[ispin].k0l0;

#ifdef DEBUG_OUTPUT
         if (!k0l0.empty()) {
            std::cout << "Sf" << ssf / 2 << " Si:" << ssi / 2 << " S0:" << ss0 / 2 << std::endl;
         } else {
            std::cout << "Sf" << ssf / 2 << " Si:" << ssi / 2 << " S0:" << ss0 / 2 << "\t empty"
                      << std::endl;
         }
#endif      

         // index_tbdme_k0L0rho0;
         size_t index_tbdme_k0L0rho0 = 0;
         // iterate over {k0, L0}
         for (size_t ik0l0 = 0; ik0l0 < k0l0.size(); ik0l0 += 2) {
            int k0 = k0l0[ik0l0];
            int L0 = k0l0[ik0l0 + 1];
            double dPiL0 = std::sqrt(2 * L0 + 1);
#ifdef DEBUG_OUTPUT
            std::cout << "\tk0:" << k0 << " L0:" << L0 << std::endl;
#endif      
            int k0max = KMAX(lm0, mu0, L0);
            assert(k0max);

            // if <wf wi || w0 L0> do not exist in memory ==> compute them
            if (L0_su3cgs[L0].lfli_vec.empty()) {
               //             L0_su3cgs[L0].ComputeSU3CGsLimited(wfwiw0, l1, l2, l3, l4, L0);
               L0_su3cgs[L0].ComputeSU3CGs(wfwiw0, L0);
            }
#ifdef DEBUG_OUTPUT
            // show coefficients with |l1-l2|<=Lf<=(l1+l2) && |l3-l4|<=Li<=(l3+l4)
            L0_su3cgs[L0].ShowRelevant(wfwiw0, l1, l2, l3, l4, L0);
#endif      
            // sum_LfLi[rho] = sum_LfLi dSU2LfLi * (sum_ki < ... || > sum_kf < ... || > < ... ||
            // >)[rho0]
            std::vector<double> sum_LfLi(rho0max, 0.0);
            const std::vector<double>& wfwiw0_L0_su3cgs = L0_su3cgs[L0].su3cgs;
            const std::vector<LFLI>& lfli_vec = L0_su3cgs[L0].lfli_vec;

            int kimax, kfmax;
            // index_LfLi:0 --> <wf  minLf, wi 0 minLi || w0 0 L0>0
            // iterate over relevant {Lf, Li} pairs
            for (size_t ilfli = 0, index_LfLi = 0; ilfli < lfli_vec.size();
                 ++ilfli, index_LfLi += k0max * kimax * kfmax * rho0max) {
               int Lf = lfli_vec[ilfli].lf;
               int Li = lfli_vec[ilfli].li;
               kimax = KMAX(lmi, mui, Li);
               kfmax = KMAX(lmf, muf, Lf);
               // skip {LfLi} if not |l1-l2/|<=Lf<=l1+l2
               if ((std::abs(l1 - l2) > Lf) || (Lf > l1 + l2)) {
                  continue;
               }
               // skip {LfLi} if not |l3-l4/|<=Li<=l3+l4
               if ((std::abs(l3 - l4) > Li) || (Li > l3 + l4)) {
                  continue;
               }
               // skip if not  Lf x Sf -> JJ1 or not Li x Si -> JJ2
               if (!SU2::mult(2 * Lf, ssf, JJ1) || !SU2::mult(2 * Li, ssi, JJ2)) {
                  continue;
               }
#ifdef DEBUG_OUTPUT
               std::cout << "\t\tLf:" << Lf << " Li:" << Li << std::endl;
#endif      
               double* n1l1n2l2_wfLf = n1n2wfLf.get_first_coeff(Lf);
               double* n3l3n4l4_wiLi = n3n4wiLi.get_first_coeff(Li);
               // index_LfLik0 --> [LfLi][k0][ki:0][kf:0][rho0:0]
               size_t index_LfLik0 = index_LfLi + k0 * kimax * kfmax * rho0max;
               // wfwiw0_su3cgs[0] == <wf kf:0 Lf wi ki:0 Li || w0 k0 L0> rho0:0
               double* wfwiw0_su3cgs = &L0_su3cgs[L0].su3cgs[index_LfLik0];
               // wfwiw0_su3cgs[index_kikfrho0] == <wf kf Lf wi ki Li || w0 k0 L0> rho0
               size_t index_kikfrho0 = 0;
               // set sum_kikf[rho0] = \sum_ki \sum_kf to zero
               std::fill(sum_kikf.begin(), sum_kikf.end(), 0);
               for (int ki = 0; ki < kimax; ++ki) {
#ifdef DEBUG_OUTPUT
                  std::cout << "\t\t\tki:" << ki << std::endl;
#endif      
                  // set sum_kf[rho0] = \sum_kf to zero
                  std::fill(sum_kf.begin(), sum_kf.end(), 0);
                  for (int kf = 0; kf < kfmax; ++kf) {
#ifdef DEBUG_OUTPUT
                     std::cout << "\t\t\t\tkf:" << kf << std::endl;
                     std::cout << "\t\t\t\t\t<(" << n1 << " 0)" << l1 << " (" << n2 << " 0)" << l2
                               << "||(" << lmf << " " << muf << ")kf:" << kf << " Lf:" << Lf
                               << ">:" << n1l1n2l2_wfLf[kf] << std::endl;
                     std::cout << "\t\t\t\t\trho0:"
                               << "0 ... " << rho0max - 1 << std::endl;
#endif      
                     for (int rho0 = 0; rho0 < rho0max; ++rho0, ++index_kikfrho0) {
#ifdef DEBUG_OUTPUT
                        std::cout << "\t\t\t\t\t     sum_kf[" << rho0 << "] +=" << n1l1n2l2_wfLf[kf]
                                  << " * " << wfwiw0_su3cgs[index_kikfrho0] << ":<(" << lmf << " "
                                  << muf << ")kf:" << kf << " Lf:" << Lf << ";(" << lmi << " "
                                  << mui << ")ki:" << ki << " Li:" << Li << "||(" << lm0 << " "
                                  << mu0 << ")k0:" << k0 << " L0:" << L0 << ">rho0:" << rho0
                                  << "--> += " << n1l1n2l2_wfLf[kf] * wfwiw0_su3cgs[index_kikfrho0]
                                  << std::endl;
#endif      
                        // sum_kf[rho0] +=
                        // <(n1 0)l1;(n2 0)l2||wf kf Lf>*<wf kf Lf; wi ki Li ||w0 k0 L0>[rho0]
                        sum_kf[rho0] += n1l1n2l2_wfLf[kf] * wfwiw0_su3cgs[index_kikfrho0];
                     }
#ifdef DEBUG_OUTPUT
                     std::cout << "\t\t\t\tkf:" << kf << " done" << std::endl;
#endif      
                  }
#ifdef DEBUG_OUTPUT
                  std::cout << "\t\t\t\t<(0 " << n3 << ")" << l3 << " (0 " << n4 << ")" << l4
                            << "||(" << lmi << " " << mui << ")ki:" << ki << " Li:" << Li
                            << ">: " << n3l3n4l4_wiLi[ki] << std::endl;
                  std::cout << "\t\t\t\trho0:"
                            << "0 ... " << rho0max - 1 << std::endl;
#endif      
                  for (int rho0 = 0; rho0 < rho0max; ++rho0) {
                     //////////////////////////////////////////////////
                     sum_kikf[rho0] += n3l3n4l4_wiLi[ki] * sum_kf[rho0];
                     //////////////////////////////////////////////////
#ifdef DEBUG_OUTPUT
                     std::cout << "\t\t\t\t   sum_kikf[" << rho0 << "] += " << n3l3n4l4_wiLi[ki]
                               << " * " << sum_kf[rho0] << ":sum_kf[" << rho0
                               << "] --> += " << n3l3n4l4_wiLi[ki] * sum_kf[rho0] << std::endl;
#endif      
                  }
#ifdef DEBUG_OUTPUT
                  std::cout << "\t\t\tki:" << ki << " done" << std::endl;
#endif      
               }

               /////////////////////////////////////////////////////////////////////////////////////
               double dsu2LfLi = ComputeSU2LfLi(Lf, Li, L0, ssf, ssi, ss0, l1, l2, l3, l4, jj1, jj2,
                                                jj3, jj4, JJ1, JJ2, JJ0);
               /////////////////////////////////////////////////////////////////////////////////////
#ifdef DEBUG_OUTPUT
               std::cout << "\t\t\tdsu2LfLi:" << dsu2LfLi << std::endl;
#endif      
               for (int rho0 = 0; rho0 < rho0max; ++rho0) {
#ifdef DEBUG_OUTPUT
                  std::cout << "\t\t\t   sum_LfLi[" << rho0 << "] += " << dsu2LfLi << " * "
                            << sum_kikf[rho0] << ":sum_kikf[" << rho0
                            << "] --> += " << dsu2LfLi * sum_kikf[rho0] << std::endl;
#endif      
                  /////////////////////////////////////////
                  sum_LfLi[rho0] += dsu2LfLi * sum_kikf[rho0];
                  /////////////////////////////////////////
               }
            }  // Lf Li
#ifdef DEBUG_OUTPUT
            std::cout << "\t\tDone with sum_LfLi" << std::endl;
#endif      
            // carry out summation over rho
            // sum_rho0 = sum_rho0 < || rho0 || > * sum_LfLi[rho0]
#ifdef DEBUG_OUTPUT
            std::cout << "\t\trho0:0 ... " << rho0max - 1 << std::endl;
#endif      
            double sum_rho0 = 0.0;
            for (int rho0 = 0; rho0 < rho0max; ++rho0) {
#ifdef DEBUG_OUTPUT
               std::cout << "\t\t   sum_rho0 += tbdmes[" << index_tbdme_k0L0rho0
                         << "]:" << su3_tbdmes[index_tbdme_k0L0rho0] << " * " << sum_LfLi[rho0]
                         << ":sum_LfLi[" << rho0 << "]" << std::endl;
#endif      
               //////////////////////////////////////////////////
               sum_rho0 += su3_tbdmes[index_tbdme_k0L0rho0] * sum_LfLi[rho0];
               index_tbdme_k0L0rho0++;
               //////////////////////////////////////////////////
            }  // rho0
#ifdef DEBUG_OUTPUT
            std::cout << "\t\tsu2_tbdme += PiSfSiS0L0:" << PiSfSiS0[ispin] * dPiL0 << " * "
                      << sum_rho0 << ":sum_rho0" << std::endl;
#endif      
            //////////////////////////////////////////////////
            su2_tbdme += PiSfSiS0[ispin] * dPiL0 * sum_rho0;
            //////////////////////////////////////////////////
#ifdef DEBUG_OUTPUT
            std::cout << "\t\tsu2_tbdme = " << su2_tbdme << std::endl;
            std::cout << "\tDone with k0:" << k0 << " L0:" << L0 << std::endl;
#endif      
         }  // k0l0
#ifdef DEBUG_OUTPUT
         std::cout << "Done with Sf:" << ssf / 2 << " Si:" << ssi / 2 << " S0:" << ss0 / 2
                   << std::endl
                   << std::endl;
#endif      
      }
   }

   // (-)^(n3 + n4) Pi_{J1 J2 j1 j2 j3 j4}
   double phase = std::sqrt((JJ1 + 1) * (JJ2 + 1) * (jj1 + 1) * (jj2 + 1) * (jj3 + 1) * (jj4 + 1));
   if ((n3 + n4) % 2) {
      phase = -phase;
   }
#ifdef DEBUG_OUTPUT
   std::cout << "resulting tbdme: " << phase << " * " << su2_tbdme << ":su3tbme -->"
             << phase * su2_tbdme << std::endl;
#endif      
   return phase * su2_tbdme;
}

void ShowSU3TBDMEs(const N1N2N3N4JJ0_TBDMES& tbdmes) {
   std::cout.precision(10);
   for (const auto& n1n2n3n4JJ0_wfwiw0_6spins_tbdmes : tbdmes) {
      N1N2N3N4JJ0 n1n2n3n4JJ0 = n1n2n3n4JJ0_wfwiw0_6spins_tbdmes.first;
      int n1 = n1n2n3n4JJ0[kN1];
      int n2 = n1n2n3n4JJ0[kN2];
      int n3 = n1n2n3n4JJ0[kN3];
      int n4 = n1n2n3n4JJ0[kN4];
      int jj0 = n1n2n3n4JJ0[kJJ0];
      WFWIW0_6SPINS_TBDMES wfwiw0_6spins_tbdmes = n1n2n3n4JJ0_wfwiw0_6spins_tbdmes.second;
      for (const auto& _6spins_tbdmes : wfwiw0_6spins_tbdmes) {
         const WFWIW0& wfwiw0 = _6spins_tbdmes.first;
         int lmf = wfwiw0[WF].lm;
         int muf = wfwiw0[WF].mu;
         int lmi = wfwiw0[WI].lm;
         int mui = wfwiw0[WI].mu;
         int rho0max = wfwiw0[W0].rho;
         int lm0 = wfwiw0[W0].lm;
         int mu0 = wfwiw0[W0].mu;
         const std::array<TBDMES, 6>& ssfssiss0_tbdmes = _6spins_tbdmes.second;

         for (int i = 0; i < 6; ++i) {
            int ssf = SSfSSiSS0[i][kSSf];
            int ssi = SSfSSiSS0[i][kSSi];
            int ss0 = SSfSSiSS0[i][kSS0];

            const std::vector<double>& tbdmes = ssfssiss0_tbdmes[i].tbdmes;
            const std::vector<int>& k0l0 = ssfssiss0_tbdmes[i].k0l0;

            size_t index_tbdme = 0;
            for (size_t ik0l0 = 0; ik0l0 < k0l0.size(); ik0l0 += 2) {
               int k0 = k0l0[ik0l0];
               int l0 = k0l0[ik0l0 + 1];

               std::cout << n1 << " " << n2 << " " << n3 << " " << n4 << "   ";
               std::cout << lmf << " " << muf << " " << ssf << "   ";
               std::cout << lmi << " " << mui << " " << ssi << "   ";
               std::cout << lm0 << " " << mu0 << " " << ss0 << "   ";
               std::cout << k0 << " " << l0 << " " << jj0 << "   ";
               for (int rho0 = 0; rho0 < rho0max; ++rho0) {
                  double tbdme = tbdmes[index_tbdme];
                  index_tbdme++;
                  std::cout << std::fixed << std::setw(19) << tbdme << " ";
               }
               std::cout << std::endl;
            }
         }
      }
   }
}

void ReadSU2tensors(const std::string& filename, std::vector<SU2TENSOR>& su2_tensors) 
{
   std::ifstream tensor_list_file(filename);
   if (!tensor_list_file) {
      std::cerr << "Cannot open file '" << filename << "' with list of SU(2) tensors." << std::endl;
      exit(EXIT_FAILURE);
   }
   while (true) {
      SU2TENSOR t;
      tensor_list_file >> t.n1 >> t.l1 >> t.jj1 >> t.n2 >> t.l2 >> t.jj2 >> t.JJ1 >> t.n3 >> t.l3 >> t.jj3 >> t.n4 >> t.l4 >> t.jj4 >> t.JJ2 >> t.JJ0;
      if (!tensor_list_file)
      {
         break;
      }
      su2_tensors.push_back(t);
   }
}

int main(int argc, char** argv) {
   if (argc != 3) {
      std::cout << "Usage: " << argv[0] << " <SU3 TBDMEs file>  <file with SU2 tensors>" << std::endl;
      return EXIT_FAILURE;
   }

   N1N2N3N4JJ0_TBDMES su3_tbdmes;
   ReadSU3tbdmesFromInputFile(argv[1], su3_tbdmes);
//   ShowSU3TBDMEs(su3_tbdmes);

   std::vector<SU2TENSOR> su2_tensors;
   ReadSU2tensors(argv[2], su2_tensors);
//   ShowSU2Tensors(su2_tensors);

   std::string outfilename = argv[2];
   outfilename += ".tbdmes";
   std::ofstream outfile(outfilename);
   outfile.precision(10);
   std::cout.precision(10);

   su3::init();
   std::map<WFWIW0, std::vector<SU3CGS>> wfwiw0_su3cgs;
   for (const auto& t : su2_tensors)
   {
      t.Show();
      double su2_tbdme = SU3toSU2(t, su3_tbdmes, wfwiw0_su3cgs);
      std::cout << std::fixed << std::setw(19) << su2_tbdme << std::endl;
      StoreSU2TBDME(outfile, t, su2_tbdme);
   }
   su3::finalize();
}
