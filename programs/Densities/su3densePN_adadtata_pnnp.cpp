#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/SU3InteractionRecoupler.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>
#include <su3dense/tensor_read.h>

#include <su3.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

// {n1_p n1_n n2_n n2_p}
typedef std::array<int, 4> SHELLS;
// {k0, 2*L0, 2*J0}
typedef std::array<int, 3> K0LL0JJ0;

struct WFWIW0 {
   WFWIW0(const SU3xSU2::LABELS& wf_, const SU3xSU2::LABELS& wi_, const SU3xSU2::LABELS& w0_)
       : wf(wf_), wi(wi_), w0(w0_) {}
   inline bool operator<(const WFWIW0& r) const {
      return std::tie(wf, wi, w0) < std::tie(r.wf, r.wi, r.w0);
   }
   SU3xSU2::LABELS wf, wi, w0;
};

// INPUT_TENSOR == map {n1_p n1_n n2_n n2_p} --> map {wf, wi, w0} --> {k0 l0 j0, k0'l0'j0', ...}
typedef std::map<SHELLS, std::map<WFWIW0, std::vector<K0LL0JJ0>>> INPUT_TENSORS;
// {wp wn w0} --> drho0rho0'
typedef std::map<TENSOR_LABELS, std::vector<double>> TENSOR_EXPANSION;

struct WPWN {
   WPWN(const SU3xSU2::LABELS& wp_, const SU3xSU2::LABELS& wn_) : wp(wp_), wn(wn_) {}
   inline bool operator<(const WPWN& r) const { return std::tie(wp, wn) < std::tie(r.wp, r.wn); }
   SU3xSU2::LABELS wp, wn;
};

void ShowInputTensor(const INPUT_TENSORS& tensors) {
   for (auto N_map_wfwiw0 : tensors) {
      SHELLS hoshells = N_map_wfwiw0.first;
      std::cout << "n1_p:" << hoshells[0] << " n1_n:" << hoshells[1] << " n2_n:" << hoshells[2]
                << " n2_p:" << hoshells[3] << std::endl;
      for (auto wfwiw0_k0l0j0 : N_map_wfwiw0.second) {
         WFWIW0 wfwiw0 = wfwiw0_k0l0j0.first;
         std::vector<K0LL0JJ0> k0ll0jj0_vec = wfwiw0_k0l0j0.second;

         std::cout << "\twf:" << wfwiw0.wf << " wi:" << wfwiw0.wi << " w0:" << wfwiw0.w0 << "\t";

         for (auto k0ll0jj0 : k0ll0jj0_vec) {
            std::cout << "k0:" << k0ll0jj0[0] << " ll0:" << k0ll0jj0[1] << " jj0:" << k0ll0jj0[2]
                      << ", ";
         }
         std::cout << std::endl;
      }
   }
}

void RecoupleTensor(int n1, int n2, int n3, int n4, const SU3xSU2::LABELS& wf,
                    const SU3xSU2::LABELS& wi, const SU3xSU2::LABELS& w0,
                    TENSOR_EXPANSION& tensor_expansion) {
   size_t nActiveShells = std::set<int>{n1, n2, n3, n4}.size();
   char n1n2n3n4[4] = {(char)n1, (char)n2, (char)n3, (char)n4};

   assert(SU3::mult(wf, wi, w0) == w0.rho);
   int rho0max = w0.rho;

   int nCoeffs = 3 * rho0max;

   // Recoupling is performed for each value of rho0
   for (int rho0 = 0; rho0 < rho0max; ++rho0) {
      std::vector<double> CoeffsPPNNPN(nCoeffs, 0.0);

      // rho0:  0  0  0  1  1  1 ...
      // type: PP NN PN PP NN PN
      // index: 0  1  2  3  4  5
      int index = rho0 * 3 + 2;
      // We want to find recoupling of the rho0^th component of the proton-neutron input tensor
      // Set the approriate coefficients to 1.0 and the rest is set to zero
      CoeffsPPNNPN[index] = 1.0;

      SU3InteractionRecoupler Recoupler;
      int bSuccess = Recoupler.Insert_adad_aa_Tensor(n1n2n3n4, wf, wi, w0, CoeffsPPNNPN);
      if (!bSuccess) {
         // Very unlikely to happen as recoupler has complete set of rules for two-body tensors
         std::cout << "IMPLEMENT: ";

         std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
         std::cout << "\t" << wf << " x " << wi << " -> " << w0;
         std::cout << std::endl;
         exit(EXIT_FAILURE);
      }
      // Recover tensors and expansion coefficients from SU3Recoupler
      // first and only element of map
      // We need proton-neutron expansion ==> m_PN
      auto n1n2n3n4_tensors = (Recoupler.m_PN[nActiveShells - 1]).begin();
      assert((Recoupler.m_PN[nActiveShells - 1]).size() == 1);

      // tensor == map< TENSOR_LABELS, {d_rho0p} >
      // d_rho0p={d_{0}, d_{1}, ... d_{rho0pmax-1}}
      // iterate over recoupled tensors
      for (auto tensor : n1n2n3n4_tensors->second) {
         // TENSOR_LABELS = struct {IR1 IR2 IR0}
         // Recoupled tensor labels
         TENSOR_LABELS labels = tensor.first;
         int rho0pmax = labels.IR0.rho;
         assert(rho0pmax == SU3::mult(labels.IR1, labels.IR2, labels.IR0));

         // find if a given tensor is already in map of
         // tensor expansion
         auto found = tensor_expansion.find(labels);
         if (found == tensor_expansion.end()) {
            found = (tensor_expansion.insert(
                         std::make_pair(labels, std::vector<double>(rho0max * rho0pmax, 0.0))))
                        .first;
         }
         // Store d_{rho0p} coefficients --> d_{rho0}{...}
         std::vector<double>& expansion_coeffs = found->second;
         // start_index --> d[rho0][rho0p:0]
         size_t start_index = rho0 * rho0pmax;
         for (size_t irho0p = 0; irho0p < rho0pmax; ++irho0p) {
            expansion_coeffs[start_index + irho0p] = tensor.second[irho0p];
         }
      }  // tensors
   }     // rho0
}

void ReadListOfTensors(const std::string& filename, INPUT_TENSORS& tensors) {
   std::ifstream tensor_list_file(filename);
   if (!tensor_list_file) {
      std::cerr << "Cannot open file '" << filename << "' with list of tensors." << std::endl;
      exit(EXIT_FAILURE);
   }
   while (true) {
      int n1_p, n1_n, n2_n, n2_p;
      int lmf, muf, ssf;
      int lmi, mui, ssi;
      int lm0, mu0, ss0;
      int k0, ll0, jj0;

      tensor_list_file >> n1_p;
      tensor_list_file >> n1_n;
      tensor_list_file >> n2_n;
      tensor_list_file >> n2_p;
      SHELLS hoshells = {n1_p, n1_n, n2_n, n2_p};

      auto map_N_map_wfwiw0_iter = tensors.find(hoshells);
      if (tensor_list_file) {
         if (map_N_map_wfwiw0_iter == tensors.end()) {
            map_N_map_wfwiw0_iter = (tensors.insert(std::make_pair(
                                         hoshells, std::map<WFWIW0, std::vector<K0LL0JJ0>>())))
                                        .first;
         }
      } else {
         break;
      }
      std::map<WFWIW0, std::vector<K0LL0JJ0>>& wfwiw0_map = map_N_map_wfwiw0_iter->second;

      tensor_list_file >> lmf;
      tensor_list_file >> muf;
      tensor_list_file >> ssf;

      tensor_list_file >> lmi;
      tensor_list_file >> mui;
      tensor_list_file >> ssi;

      // note that value of rho0max for w0 must be computed
      // for each wf wi combination. It can differ and thus I
      // do not set it for NW0 key.
      tensor_list_file >> lm0;
      tensor_list_file >> mu0;
      tensor_list_file >> ss0;

      tensor_list_file >> k0;
      tensor_list_file >> ll0;
      tensor_list_file >> jj0;

      if (tensor_list_file) {
         SU3xSU2::LABELS wf(lmf, muf, ssf);
         SU3xSU2::LABELS wi(lmi, mui, ssi);
         SU3xSU2::LABELS w0(lm0, mu0, ss0);
         w0.rho = SU3::mult(wf, wi, w0);

         WFWIW0 wfwiw0(wf, wi, w0);
         K0LL0JJ0 k0ll0jj0 = {k0, ll0, jj0};

         auto wfwiw0_map_iter = wfwiw0_map.find(wfwiw0);

         if (wfwiw0_map_iter == wfwiw0_map.end()) {
            wfwiw0_map.insert(std::make_pair(wfwiw0, std::vector<K0LL0JJ0>(1, {k0ll0jj0})));
         } else {
            wfwiw0_map_iter->second.push_back(k0ll0jj0);
         }
      } else {
         break;
      }
   }
}

void ShowContributingTensors(std::map<WPWN, std::vector<std::pair<WFWIW0, std::vector<double>>>>&
                                 wpwn_contributing_tensors) {
   for (auto wpwn_wfwiw0_d_vec : wpwn_contributing_tensors) {
      WPWN wpwn = wpwn_wfwiw0_d_vec.first;
      std::cout << "wp:" << wpwn.wp << " wn:" << wpwn.wn << std::endl;
      std::vector<std::pair<WFWIW0, std::vector<double>>> wfwiw0_d_vec = wpwn_wfwiw0_d_vec.second;
      for (auto wfwiw0_d : wfwiw0_d_vec) {
         WFWIW0 wfwiw0 = wfwiw0_d.first;
         std::cout << "\twf:" << wfwiw0.wf << " wf:" << wfwiw0.wi << " w0:" << wfwiw0.w0 << "\td:";
         std::vector<double>& d_vec = wfwiw0_d.second;
         for (auto value : d_vec) {
            std::cout << value << ", ";
         }
         std::cout << endl;
      }
   }
}

struct Comp {
   bool operator()(const std::pair<WFWIW0, std::vector<double>>& l,
                   const SU3xSU2::LABELS& w0) const {
      return l.first.w0 < w0;
   }
   bool operator()(const SU3xSU2::LABELS& w0,
                   const std::pair<WFWIW0, std::vector<double>>& r) const {
      return w0 < r.first.w0;
   }
};

void IJ_Indices2IJ_RMES(const IJ_Indices& ij_indices, const std::vector<float>& rmes,
                        IJ_RMES& ij_rmes) {
   uint32_t i,j;
   size_t istart, inext_start;
   for (int q = 0; q < ij_indices.size() - 1; ++q)
   {
      // {i, j, indexrme}
      i = ij_indices[q][0];
      j = ij_indices[q][1];
      istart = ij_indices[q][2];
      inext_start = ij_indices[q + 1][2];
      ij_rmes[std::make_pair(i, j)] =
          std::move(std::vector<float>(rmes.begin() + istart, rmes.begin() + inext_start));
   }
   auto ijindex = ij_indices.back();
   i = ijindex[0];
   j = ijindex[1];
   istart = ijindex[2]; // == inext_start;
   ij_rmes[std::make_pair(i, j)] = std::move(std::vector<float>(rmes.begin() + istart, rmes.end()));
}


void IJ_RMES2IJ_Indices(const IJ_RMES& rmes_table, IJ_Indices& ij_indices, std::vector<float>& rmes)
{
   size_t nrmes = 0;
   // rmes_table is not sorted according to {i,j} indices
   // ==> store them in map to carry out sorting first
   std::map<std::pair<uint32_t, uint32_t>, std::vector<float>> rmes_sorted;
   for (auto ij_rmes : rmes_table)
   {
      rmes_sorted.insert(ij_rmes);
      nrmes += ij_rmes.second.size();
   }

   rmes.reserve(nrmes);
   size_t istart = 0;
   for (auto ij_rmes : rmes_sorted)
   {
      uint32_t i = ij_rmes.first.first;
      uint32_t j = ij_rmes.first.second;
      std::array<uint32_t, 3> ijindex = {i, j, (uint32_t)istart};
      ij_indices.push_back(ijindex);
      istart += ij_rmes.second.size();
      rmes.insert(rmes.end(), ij_rmes.second.begin(), ij_rmes.second.end());
   }
}

// Returns a0pmax ... maximal tensor multiplicity
uint32_t ReadProtonTensorRMEsCSRtoIJ_RMES(const std::string& rmes_filename_p,
                                 lsu3::CncsmSU3xSU2Basis& bra,
                                 lsu3::CncsmSU3xSU2Basis& ket, SU3xSU2::LABELS& wp,
                                 IJ_RMES& rmes_table_p)
{
   IJ_Indices ipjp_indices;
   std::vector<tensorfile::value_t> tensor_rmes_p;
   int a0pmax = ReadProtonTensorRMEsCSRtoIJ_Indices(rmes_filename_p, bra, ket, wp, ipjp_indices, tensor_rmes_p);
   if (tensor_rmes_p.empty())
   {
      std::cout << "This rme table is empty." << std::endl;
      return a0pmax;
   }
   IJ_Indices2IJ_RMES(ipjp_indices, tensor_rmes_p, rmes_table_p);
//////////////////////////////////////////////////////////////
/// test if IJ_Indices -> IJ_RMES -> IJ_Indices is "unitary"
   IJ_Indices ipjp_indices_inverse;
   std::vector<tensorfile::value_t> tensor_rmes_inverse;
   IJ_RMES2IJ_Indices(rmes_table_p, ipjp_indices_inverse, tensor_rmes_inverse);
   if (ipjp_indices != ipjp_indices_inverse)
   {
      std::cout << "Indices are not equal!" << std::endl;
      exit(EXIT_FAILURE);
   }
   if (tensor_rmes_p != tensor_rmes_inverse)
   {
      std::cout << "rmes are not equal!" << std::endl;
      exit(EXIT_FAILURE);
   }
//////////////////////////////////////////////////////////////
   return a0pmax;
}

// Returns a0nmax ... maximal tensor multiplicity
uint32_t ReadNeutronTensorRMEsCSRtoIJ_RMES(const std::string& rmes_filename_n,
                                  lsu3::CncsmSU3xSU2Basis& bra,
                                  lsu3::CncsmSU3xSU2Basis& ket, SU3xSU2::LABELS& wn,
                                  IJ_RMES& rmes_table_n)
{
   IJ_Indices injn_indices;
   std::vector<tensorfile::value_t> tensor_rmes_n;
   int a0nmax = ReadNeutronTensorRMEsCSRtoIJ_Indices(rmes_filename_n, bra, ket, wn, injn_indices, tensor_rmes_n);
   if (tensor_rmes_n.empty())
   {
      std::cout << "This rme table is empty." << std::endl;
      return a0nmax;
   }
   IJ_Indices2IJ_RMES(injn_indices, tensor_rmes_n, rmes_table_n);
//////////////////////////////////////////////////////////////
/// test if IJ_Indices -> IJ_RMES -> IJ_Indices is "unitary"
   IJ_Indices injn_indices_inverse;
   std::vector<tensorfile::value_t> tensor_rmes_inverse;
   IJ_RMES2IJ_Indices(rmes_table_n, injn_indices_inverse, tensor_rmes_inverse);
   if (injn_indices != injn_indices_inverse)
   {
      std::cout << "Indices are not equal!" << std::endl;
      exit(EXIT_FAILURE);
   }
   if (tensor_rmes_n != tensor_rmes_inverse)
   {
      std::cout << "rmes are not equal!" << std::endl;
      exit(EXIT_FAILURE);
   }
//////////////////////////////////////////////////////////////
   return a0nmax;
}

unsigned long ComputeMatrixElements(lsu3::CncsmSU3xSU2Basis& bra, lsu3::CncsmSU3xSU2Basis& ket,
                                    const IJ_RMES& prot_rmes_table, const IJ_RMES& neut_rmes_table,
                                    const SU3xSU2::LABELS& protTensorLabels,
                                    const SU3xSU2::LABELS& neutTensorLabels,
                                    const SU3xSU2::LABELS& tensorLabels, int a0, int k0, int L0,
                                    std::vector<float>& vals, std::vector<size_t>& column_indices,
                                    std::vector<size_t>& row_ptrs,
                                    std::vector<uint32_t>& ipin_block_ids) {
   uint32_t LL0 = 2 * L0;
   // definition: libraries/LookUpContainers/WigEckSU3SO3CGTable.h line 134
   WigEckSU3SO3CGTable su3so3cgTable(tensorLabels, LL0);
   unsigned long number_nonzero_me(0);
   int k0max = SU3::kmax(tensorLabels, L0);
   int a0max = tensorLabels.rho;
   // vector of tensor coefficients for all k0 < k0max and a0 < a0max,
   // ordered as [k0][a0] (see libraries/SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJ.fix line)
   std::vector<TENSOR_STRENGTH> coeffs(k0max * a0max, 0.0);
   // IMPORTANT TRICK:
   // We will supply this vector of coefficients into
   // CalculateME_nonDiagonal_nonScalar to get non-zero matrix only for a given
   // a0 and k0 component of a tensor. This allows me to use existing method
   // which is meant to be applied for computations of matrix elements of a realistic
   // Hamiltonian.
   // NOTE: This is silly as one should carry out computation for all allowed a0 and k0 components
   // of tensor
   coeffs[k0 * a0max + a0] = 1.0;

   const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
   const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

   for (uint32_t ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      if (bra.NumberOfStatesInBlock(ipin_block) == 0) {
         continue;
      }
      bool vanishingBlock = true;
      uint32_t blockFirstRow = bra.BlockPositionInSegment(ipin_block);

      uint32_t ip = bra.getProtonIrrepId(ipin_block);
      uint32_t in = bra.getNeutronIrrepId(ipin_block);

      SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
      SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

      uint16_t aip_max = bra.getMult_p(ip);
      uint16_t ain_max = bra.getMult_n(in);

      unsigned long blockFirstColumn(0);

      std::vector<std::vector<float>> vals_local(bra.NumberOfStatesInBlock(ipin_block));
      std::vector<std::vector<size_t>> col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

      for (unsigned int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++) {
         if (ket.NumberOfStatesInBlock(jpjn_block) == 0) {
            continue;
         }
         uint32_t jp = ket.getProtonIrrepId(jpjn_block);
         uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

         auto ipjp_rmes = prot_rmes_table.find(std::make_pair(ip, jp));
         if (ipjp_rmes == prot_rmes_table.end()) {
            continue;
         }
         auto injn_rmes = neut_rmes_table.find(std::make_pair(in, jn));
         if (injn_rmes == neut_rmes_table.end()) {
            continue;
         }

         uint32_t blockFirstColumn = ket.BlockPositionInSegment(jpjn_block);

         uint16_t ajp_max = ket.getMult_p(jp);
         uint16_t ajn_max = ket.getMult_n(jn);

         SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
         SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

         // note that protTensorLabels.rho contains maximal multiplicity a0p
         SU3xSU2::RME prot_rme(aip_max, w_ip, protTensorLabels.rho, protTensorLabels, ajp_max, w_jp,
                               ipjp_rmes->second);
         // note that neutTensorLabels.rho contains maximal multiplicity a0n
         SU3xSU2::RME neut_rme(ain_max, w_in, neutTensorLabels.rho, neutTensorLabels, ajn_max, w_jn,
                               injn_rmes->second);

         //	loop over wpn that result from coupling ip x in
         uint32_t ibegin = bra.blockBegin(ipin_block);
         uint32_t iend = bra.blockEnd(ipin_block);
         uint32_t currentRow = blockFirstRow;
         for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn) {
            SU3xSU2::LABELS wpn_i(bra.getOmega_pn(ip, in, iwpn));
            size_t afmax = aip_max * ain_max * wpn_i.rho;
            SU3xSU2::BasisJfixed braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));

            uint32_t currentColumn = blockFirstColumn;
            uint32_t jbegin = ket.blockBegin(jpjn_block);
            uint32_t jend = ket.blockEnd(jpjn_block);
            for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn) {
               SU3xSU2::LABELS wpn_j(ket.getOmega_pn(jp, jn, jwpn));
               size_t aimax = ajp_max * ajn_max * wpn_j.rho;
               SU3xSU2::BasisJfixed ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));

               if (SU2::mult(wpn_j.S2, tensorLabels.S2, wpn_i.S2) &&
                   SU3::mult(wpn_j, tensorLabels, wpn_i)) {
                  vanishingBlock = false;
                  ///////////////////////////////////////////////////
                  // ERROR!!! ==> I forgot to compute the phase !!!!
                  ///////////////////////////////////////////////////
                  // int phase = ... ???
                  ///////////////////////////////////////////////////
                  SU3xSU2::RME total_rme(wpn_i, tensorLabels, wpn_j, &prot_rme, &neut_rme, NULL);
                  // Obtain <(lmi mui) *; (lm0 mu0) * L0 || (lmf muf) *>_{*}
                  WigEckSU3SO3CG* pWigEckSU3SO3CG = su3so3cgTable.GetWigEckSU3SO3CG(wpn_i, wpn_j);
                  // MECalculatorData represent components of a tensor
                  std::vector<MECalculatorData> rmeCoeffsPNPN(
                      1, MECalculatorData(&total_rme, pWigEckSU3SO3CG, coeffs.data(), LL0));
                  CalculateME_nonDiagonal_nonScalar(
                      (currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax,
                      ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
                  delete[] total_rme.m_rme;
               }
               currentColumn += aimax * ket.omega_pn_dim(jwpn);
            }  // wpn_j
            currentRow += afmax * bra.omega_pn_dim(iwpn);
         }  // wpn_i
         delete[] prot_rme.m_rme;
         delete[] neut_rme.m_rme;
      }
      if (!vanishingBlock && !vals_local.empty()) {
         // Store index of ipin block of bra basis states whose matrix elements we are going to
         // store into CSR structure
         ipin_block_ids.push_back(ipin_block);
         for (size_t irow = 0; irow < vals_local.size(); ++irow) {
            vals.insert(vals.end(), vals_local[irow].begin(), vals_local[irow].end());
            row_ptrs.push_back(vals.size());
            column_indices.insert(column_indices.end(), col_ind_local[irow].begin(),
                                  col_ind_local[irow].end());
            number_nonzero_me += vals_local[irow].size();
         }
      }
   }
   return number_nonzero_me;
}

double bra_x_Observable_x_ket_per_thread(
    const lsu3::CncsmSU3xSU2Basis& bra, const std::vector<float>& bra_wfn,
    const std::vector<float>& ket_wfn,
    // data structures describing sparse submatrix on CSR format
    const std::vector<float>& vals, const std::vector<size_t>& column_indices,
    const std::vector<size_t>& row_ptrs,
    // rows are organized into groups of (ip in) states. For each block we store its size and
    // starting position in basis.
    // ipin_block_ids holds indices of non vanishing row blocks stored in CSR structure
    const std::vector<uint32_t> ipin_block_ids) {
   // If matrix is empty ==> return 0.0
   if (vals.empty()) {
      std::cout << "vals is empty!" << std::endl;
      std::cout << "row_ptrs.size():" << row_ptrs.size() << std::endl; 
//      assert(row_ptrs.empty());
      assert(column_indices.empty());
      return 0.0;
   }
   std::vector<double> observable_x_ket(row_ptrs.size() - 1, 0);

   assert(vals.size() == row_ptrs.back());
   assert(observable_x_ket.size() == row_ptrs.size() - 1);

   for (int irow = 0; irow < row_ptrs.size() - 1; ++irow) {
      for (int ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival) {
         observable_x_ket[irow] += (double)vals[ival] * (double)ket_wfn[column_indices[ival]];
      }
   }

   double dresult = 0.0;
   int row0;
   int nrows;
   int i = 0;
   for (int iblock = 0; iblock < ipin_block_ids.size(); ++iblock) {
      row0 = bra.BlockPositionInSegment(ipin_block_ids[iblock]);
      nrows = bra.NumberOfStatesInBlock(ipin_block_ids[iblock]);
      for (int irow = 0; irow < nrows; ++irow, ++i) {
         dresult += (double)bra_wfn[row0 + irow] * observable_x_ket[i];
      }
   }
   return dresult;
}

void ReadWfn(lsu3::CncsmSU3xSU2Basis& basis, const std::string& wfn_filename,
             std::vector<float>& wfn) {
   std::fstream wfn_file(wfn_filename.c_str(),
                         std::ios::in | std::ios::binary |
                             std::ios::ate);  // open at the end of file so we can get file size
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_filename << "' wfn file" << endl;
      exit(EXIT_FAILURE);
   }

   size_t size = wfn_file.tellg();
   size_t nelems = size / sizeof(float);

   if (size % sizeof(float) || (nelems != basis.getModelSpaceDim())) {
      cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x "
           << sizeof(float) << " = " << basis.getModelSpaceDim() * sizeof(float) << " bytes."
           << endl;
      cout << "The actual size of the file: " << size << " bytes!";
      exit(EXIT_FAILURE);
   }

   wfn.resize(nelems);
   wfn_file.seekg(0, std::ios::beg);
   wfn_file.read((char*)wfn.data(), wfn.size() * sizeof(float));
}

using RESULTING_TBDMES =
    std::map<WFWIW0, std::pair<std::vector<K0LL0JJ0>, std::vector<std::vector<double>>>>;

void  InitializeResultingTBDMEs(const WFWIW0& wfwiw0, const std::vector<K0LL0JJ0>& k0ll0jj0_vec, RESULTING_TBDMES& resulting_tbdmes)
{
   int rho0max = wfwiw0.w0.rho;
   assert(rho0max == SU3::mult(wfwiw0.wf, wfwiw0.wi, wfwiw0.w0));
   std::vector<std::vector<double>> tbdmes(k0ll0jj0_vec.size(), std::vector<double>(rho0max, 0.0));
   resulting_tbdmes.insert(std::make_pair(wfwiw0, std::make_pair(k0ll0jj0_vec, tbdmes)));
}

void Copy(const std::map<TENSOR_LABELS, std::vector<double>>& tensor_expansion, const WFWIW0& wfwiw0, std::map<WPWN, std::vector<std::pair<WFWIW0, std::vector<double>>>>& wpwn_contributing_tensors)
{
   // Copy tensor_expansion into table of "contributing" {wp, wn} tensors
   for (auto wpwnw0_drho0rho0p : tensor_expansion) {
      WPWN wpwn(wpwnw0_drho0rho0p.first.IR1, wpwnw0_drho0rho0p.first.IR2);
      auto wpwn_iter = wpwn_contributing_tensors.find(wpwn);
      if (wpwn_iter == wpwn_contributing_tensors.end()) {
         std::vector<std::pair<WFWIW0, std::vector<double>>> contributing_wfwiw0_d(
             1, std::make_pair(wfwiw0, wpwnw0_drho0rho0p.second));
         // insert [{wp, wn}] = {{wfwiw0, d}}
         wpwn_contributing_tensors.insert(std::make_pair(wpwn, contributing_wfwiw0_d));
      } else {  // {wp, wn} already exist
         //  add {wfwiw0, d} element into vector associated with [{wp,wn}] key
         wpwn_iter->second.push_back(std::make_pair(wfwiw0, wpwnw0_drho0rho0p.second));
      }
   }
}

template <class RANGE>
void CreateSetOfUniqueK0LL0JJ0(RANGE& block_with_constant_w0,
                               const std::map<WFWIW0, std::vector<K0LL0JJ0>>& map_wfwiw0_k0ll0jj0,
                               std::set<K0LL0JJ0>& k0ll0jj0_set) {
   // iterate over elements of vector<{wf,wi,w0=const}, {{k0,l0j0},...}>
   for (auto it = block_with_constant_w0.first; it != block_with_constant_w0.second; ++it) {
      const WFWIW0& wfwiw0 = it->first;
      // find in map [{wf, wi, w0}] ---> {{k0 LL0 JJ0}, ... }
      auto wfwiw0_k0l0j0 = map_wfwiw0_k0ll0jj0.find(wfwiw0);
      if (wfwiw0_k0l0j0 == map_wfwiw0_k0ll0jj0.end()) {
         std::cerr << "a given wf wi w0 is missing in input tensors!" << std::endl;
         exit(EXIT_FAILURE);
      }
      const std::vector<K0LL0JJ0>& k0l0j0_vec = wfwiw0_k0l0j0->second;
      std::copy(k0l0j0_vec.begin(), k0l0j0_vec.end(),
                std::inserter(k0ll0jj0_set, k0ll0jj0_set.end()));
   }
}

using RECOUPLED_TBDMES = std::map<K0LL0JJ0, std::vector<double>>;

void ComputeRecoupledDensities(const std::set<K0LL0JJ0>& k0ll0jj0_set,
                               lsu3::CncsmSU3xSU2Basis& bra, const std::vector<float>& bra_wfn,
                               lsu3::CncsmSU3xSU2Basis& ket, const std::vector<float>& ket_wfn,
                               SU3xSU2::LABELS& wp, const SU3xSU2::LABELS& wn, SU3xSU2::LABELS w0,
                               const IJ_RMES& prot_rmes_table, const IJ_RMES neut_rmes_table,
                               RECOUPLED_TBDMES& recoupled_tbdmes) {
   int rho0pmax = w0.rho;
   // iterate over set of unique {k0 2l0 2j0} \in w0 compute [wp wn]w0 k0l0j0 density
   // for all multiplicities rho0p
   for (auto k0ll0jj0 : k0ll0jj0_set) {
      int k0 = k0ll0jj0[0];
      int ll0 = k0ll0jj0[1];
      int jj0 = k0ll0jj0[2];
      std::cout << "k0:" << k0 << " ll0:" << ll0 << " jj0:" << jj0 << std::endl;
      MECalculatorData::JJ0_ = jj0;
      // tbdmes associated with {wp x wn}^rho0p w0_{k0 l0 j0} tensor for each value of rho0p
      std::vector<double> rho0p_recoupled_tbdmes(rho0pmax, 0.0);
      // for earch rho0p compute tbdme for tensor {wp x wn}^rho0p w0_{k0 l0 j0}
      for (int rho0p = 0; rho0p < rho0pmax; ++rho0p) {
         std::vector<float> vals;
         std::vector<size_t> column_indices;
         std::vector<size_t> row_ptrs;
         row_ptrs.push_back(0);
         std::vector<uint32_t> ipin_block_ids;

         unsigned long nmes = ComputeMatrixElements(
             bra, ket, prot_rmes_table, neut_rmes_table, wp, wn, w0, rho0p, k0,
             ll0 / 2, vals, column_indices, row_ptrs, ipin_block_ids);
         if (nmes != 0) {
            rho0p_recoupled_tbdmes[rho0p] = bra_x_Observable_x_ket_per_thread(
                bra, bra_wfn, ket_wfn, vals, column_indices, row_ptrs, ipin_block_ids);
         }
         std::cout << "rho0p:" << rho0p << " result:" << rho0p_recoupled_tbdmes[rho0p]
                   << std::endl;
      }
      recoupled_tbdmes.insert(std::make_pair(k0ll0jj0, rho0p_recoupled_tbdmes));
   }
}


void  PrintResultingTBDMEs(int n1_p, int n1_n, int n2_n, int n2_p, const RESULTING_TBDMES& resulting_tbdmes, std::fstream& file)
{
   for (auto wfwiw0_k0ll0jj0_tbdmes : resulting_tbdmes)
   {
      const WFWIW0& wfwiw0 = wfwiw0_k0ll0jj0_tbdmes.first;
      int lmf = wfwiw0.wf.lm;
      int muf = wfwiw0.wf.mu;
      int ssf = wfwiw0.wf.S2;

      int lmi = wfwiw0.wi.lm;
      int mui = wfwiw0.wi.mu;
      int ssi = wfwiw0.wi.S2;

      int rho0max = wfwiw0.w0.rho;
      int lm0 = wfwiw0.w0.lm;
      int mu0 = wfwiw0.w0.mu;
      int ss0 = wfwiw0.w0.S2;

      const std::vector<K0LL0JJ0>& k0ll0jj0_vec = wfwiw0_k0ll0jj0_tbdmes.second.first;
      const std::vector<std::vector<double>>& tbdmes = wfwiw0_k0ll0jj0_tbdmes.second.second;
      assert(k0ll0jj0_vec.size() == tbdmes.size());
      for (size_t ik0ll0jj0 = 0; ik0ll0jj0 < k0ll0jj0_vec.size(); ++ik0ll0jj0)
      {
         int k0 = k0ll0jj0_vec[ik0ll0jj0][0];
         int ll0 = k0ll0jj0_vec[ik0ll0jj0][1];
         int jj0 = k0ll0jj0_vec[ik0ll0jj0][2];

         std::cout << n1_p << " " << n1_n << " " << n2_n << " " << n2_p << " " << lmf << " " << muf
                   << " " << ssf << " " << lmi << " " << mui << " " << ssi << " " << lm0 << " "
                   << mu0 << " " << k0 << " " << ll0 << " " << jj0 << "\t";
         file << n1_p << " " << n1_n << " " << n2_n << " " << n2_p << " " << lmf << " " << muf
              << " " << ssf << " " << lmi << " " << mui << " " << ssi << " " << lm0 << " " << mu0
              << " " << ss0 << " " << k0 << " " << ll0 << " " << jj0 << "\t";

         for (int rho0 = 0; rho0 < rho0max; ++rho0)
         {
            std::cout << tbdmes[ik0ll0jj0][rho0] << " ";
            file << tbdmes[ik0ll0jj0][rho0] << " ";
         }
         std::cout << std::endl;
         file << std::endl;
      }
   }
}

int main(int argc, char** argv) {
   if (argc != 6) {
      std::cerr << "Usage: " << argv[0]
                << " <bra model space> <bra wfn> <ket model space> <ket wfn> <tensor list file>\n"
                << " <tensor list file>: contains a list of two-body PN tensor quantum labels:\n "
                   "n1_p n1_n n2_n n2_p lmf muf 2Sf lmi mui 2Si lm0 mu0 2S0"
                << std::endl;
      return EXIT_FAILURE;
   }

   std::string bra_model_space_filename = argv[1];
   std::string bra_wfn_filename = argv[2];
   std::string ket_model_space_filename = argv[3];
   std::string ket_wfn_filename = argv[4];

   proton_neutron::ModelSpace bra_model_space, ket_model_space;
   bra_model_space.Load(bra_model_space_filename);
   ket_model_space.Load(ket_model_space_filename);

   lsu3::CncsmSU3xSU2Basis bra, ket;
   bra.ConstructBasis(bra_model_space, 0, 1);
   ket.ConstructBasis(ket_model_space, 0, 1);

   std::vector<float> bra_wfn;
   ReadWfn(bra, bra_wfn_filename, bra_wfn);

   std::vector<float> ket_wfn;
   ReadWfn(ket, ket_wfn_filename, ket_wfn);

   INPUT_TENSORS tensors;
   std::string tensor_filename(argv[5]);
   std::string output_filename(tensor_filename);
   output_filename += ".su3_pn_tbdmes";
   std::cout << output_filename << std::endl;
   std::fstream output_file(output_filename, std::ios::out);
   if (!output_file)
   {
      std::cerr << "Unable to open output file '" << output_filename << "'!" << std::endl;
      return EXIT_FAILURE;
   }

   ReadListOfTensors(tensor_filename, tensors);

   ShowInputTensor(tensors);
   k_dependent_tensor_strenghts = false;

   su3::init();
   // Needed to carry out recoupling transformation
   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(true);

   // iterate over {n1_n, n1_p, n2_n, n2_p}TT
   for (auto N_map_wfwiw0 : tensors) {
      SHELLS hoshells = N_map_wfwiw0.first;
      int n1_p = hoshells[0];
      int n1_n = hoshells[1];
      int n2_n = hoshells[2];
      int n2_p = hoshells[3];

      std::cout << "n1_p:" << n1_p << " n1_n:" << n1_n << " n2_n:" << n2_n << " n2_p:" << n2_p
                << std::endl;

      std::map<WPWN, std::vector<std::pair<WFWIW0, std::vector<double>>>> wpwn_contributing_tensors;
      RESULTING_TBDMES resulting_tbdmes;
      // iterate over {wf wi w0}
      for (auto wfwiw0_k0l0j0 : N_map_wfwiw0.second) {
         WFWIW0 wfwiw0 = wfwiw0_k0l0j0.first;
         std::cout << "Recoupling " << wfwiw0.wf << "   " << wfwiw0.wi << "   " << wfwiw0.w0 << std::endl;
         // prepare map[wf, wi, w0] --> resulting tbdmes
         InitializeResultingTBDMEs(wfwiw0, wfwiw0_k0l0j0.second, resulting_tbdmes);

         std::map<TENSOR_LABELS, std::vector<double>> tensor_expansion;
         RecoupleTensor(n1_p, n1_n, n2_n, n2_p, wfwiw0.wf, wfwiw0.wi, wfwiw0.w0, tensor_expansion);

         Copy(tensor_expansion, wfwiw0, wpwn_contributing_tensors);
      }

      // iterate over {wp, wn}
      for (auto& wpwn_wfwiw0 : wpwn_contributing_tensors) {
         // sort vector of ({wf,wi,w0}, d) pairs according to value of w0
         std::sort(wpwn_wfwiw0.second.begin(), wpwn_wfwiw0.second.end(),
                   [](const std::pair<WFWIW0, std::vector<double>>& l,
                      const std::pair<WFWIW0, std::vector<double>>& r) {
                      return l.first.w0 < r.first.w0;
                   });
      }

      ShowContributingTensors(wpwn_contributing_tensors);

      // iterate over {wp, wn}
      for (auto wpwn_wfwiw0 : wpwn_contributing_tensors) {
         // read proton rmes and neutron rmes
         SU3xSU2::LABELS wp = wpwn_wfwiw0.first.wp;
         std::string proton_rmes_filename = GetFilename_SU3xSU2({n1_p, n2_p, wp.lm, wp.mu, wp.S2});

         SU3xSU2::LABELS wn = wpwn_wfwiw0.first.wn;
         std::string neutron_rmes_filename = GetFilename_SU3xSU2({n1_n, n2_n, wn.lm, wn.mu, wn.S2});

         std::cout << "proton table:" << proton_rmes_filename << std::endl;
         IJ_RMES prot_rmes_table;
         int  a0pmax = ReadProtonTensorRMEsCSRtoIJ_RMES(proton_rmes_filename, bra, ket, wp, prot_rmes_table);
//         ShowRMEsTableIJ_RMES(prot_rmes_table);

         std::cout << "neutron table:" << neutron_rmes_filename << std::endl;
         IJ_RMES neut_rmes_table;
         int  a0nmax = ReadNeutronTensorRMEsCSRtoIJ_RMES(neutron_rmes_filename, bra, ket, wn, neut_rmes_table);
//         ShowRMEsTableIJ_RMES(neut_rmes_table);

         // w0 = rho0 (lm0 mu0), result from wf x wi coupling
         SU3xSU2::LABELS w0 = wpwn_wfwiw0.second.front().first.w0;
         // iterate over {wf, wi, w0=const} blocks
         do {
            SU3xSU2::LABELS w0pn = w0;  // rho0pmax w0
            int rho0pmax = SU3::mult(wp, wn, w0);
            w0pn.rho = rho0pmax;
            std::cout << "wp:" << wp << " wn:" << wn << " w0:" << w0pn << std::endl;
            // find block of {wf, wi, w0} with w0=const
            auto block_with_constant_w0 =
                std::equal_range(wpwn_wfwiw0.second.begin(), wpwn_wfwiw0.second.end(), w0, Comp{});

            // iterate over all possible {wf, wi, w0=const} tensors
            // and built a set of {k0, ll0, jj0} quantum numbers that will be needed
            // for LSU3shell compatible < || R_{k0l0j0} || > TBDMEs
            std::set<K0LL0JJ0> k0ll0jj0_set;
            CreateSetOfUniqueK0LL0JJ0(block_with_constant_w0, N_map_wfwiw0.second, k0ll0jj0_set);

            RECOUPLED_TBDMES recoupled_tbdmes;
            ComputeRecoupledDensities(k0ll0jj0_set, bra, bra_wfn, ket, ket_wfn, wp, wn, w0pn,
                                      prot_rmes_table, neut_rmes_table, recoupled_tbdmes);

           // iterate over blocks of {{wf, wi, w0}, d^{wp wn}} with cont w0
            for (auto it = block_with_constant_w0.first; it != block_with_constant_w0.second; ++it) 
            {
               // {wf, wi, w0}
               const WFWIW0& wfwiw0 = it->first;
               int rho0max = wfwiw0.w0.rho;
               assert(rho0max = SU3::mult(wfwiw0.wf, wfwiw0.wi, wfwiw0.w0));
               // d_{rho0' rho0}^{wp wn}
               // it has rho0max*rho0pmax elements
               const std::vector<double>& d = it->second;
               assert(d.size() == rho0max*rho0pmax);
               // find vector<{k0,l0,j0}> and resulting TBDMEs values associated with [{wf, wi, w0}] tensor
               auto k0ll0jj0_tbdmes = resulting_tbdmes.find(wfwiw0);
               assert(k0ll0jj0_tbdmes != resulting_tbdmes.end());
               // vector of {k0 l0 j0} components of {wf, wi, w0} tensor that we want to compute
               const std::vector<K0LL0JJ0>& k0ll0jj0_vec = k0ll0jj0_tbdmes->second.first;
               // tbdmes[{k0, l0, j0}][rho0]
               std::vector<std::vector<double>>& tbdmes = k0ll0jj0_tbdmes->second.second;
               assert(tbdmes.size() == k0ll0jj0_vec.size());
               size_t ik0ll0jj0 = 0;
               for (const auto& k0ll0jj0 : k0ll0jj0_vec)
               {
                  assert(tbdmes[ik0ll0jj0].size() == rho0max);
                  // note that set of {k0,l0,j0} stored in recoupled_tbdmes is not always identical
                  // to {k0,l0,j0} for a given wf wi w0.
                  auto recoupled_tbdmes_iter = recoupled_tbdmes.find(k0ll0jj0);
                  assert(recoupled_tbdmes_iter != recoupled_tbdmes.end());
                  // this vector has rho0pmax elements and
                  // it represents <Jf || [wp wn]^rho0'w0_k0l0j0 || Ji>
                  const std::vector<double>& recoupled_tbdmes = recoupled_tbdmes_iter->second;
                  assert(recoupled_tbdmes.size() == rho0pmax);
                  size_t index = 0;
                  for (int rho0 = 0; rho0 < rho0max; ++rho0)
                  {
                     double contribution = 0;
                     for (int rho0p = 0; rho0p < rho0pmax; ++rho0p)
                     {
                        contribution += d[index++]*recoupled_tbdmes[rho0p]; 
                     }
                     tbdmes[ik0ll0jj0][rho0] += contribution;
                  }
                  ik0ll0jj0 += 1;
               }
            }
            if (block_with_constant_w0.second < wpwn_wfwiw0.second.end())
            {
               // set the value w0 for the next block
               w0 = block_with_constant_w0.second->first.w0;
            }
            else
            {
               break;
            }
         } while (true);

      }
      PrintResultingTBDMEs(n1_p, n1_n, n2_n, n2_p, resulting_tbdmes, output_file);
   }
   su3::finalize();
   return 1;
}
