#include <SU3ME/SU3xSU2Basis.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <array>
#include <fstream>
#include <iostream>
#include <set>
#include <vector>

// This structs are used in GenerateAllCombinationsHOShells
// Returns true if selected
struct PositiveParity {
   // true if n1 + n2 + n3 + n4 is even
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((n1 + n2 + n3 + n4) % 2 == 0);
   }
};

// This structs are used in GenerateAllCombinationsHOShells
// Returns true if selected
struct NegativeParity {
   // true if n1 + n2 + n3 + n4 is odd
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((n1 + n2 + n3 + n4) % 2);
   }
};

enum { kNA = 0, kNB = 1, kNC = 2, kND = 3 };
typedef std::array<int, 4> NANBNCND;

template <typename SelectionRules>
void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell,
                                     std::vector<NANBNCND>& ho_shells_combinations) {
   SelectionRules IsIncluded;
   ho_shells_combinations.resize(0);

   NANBNCND na_nb_nc_nd;
   for (int na = 0; na <= maxShell; ++na) {
      for (int nb = 0; nb <= maxShell; ++nb) {
         int adxad_Nhw = 0;
         if (na > valence_shell) {
            adxad_Nhw += (na - valence_shell);
         }
         if (nb > valence_shell) {
            adxad_Nhw += (nb - valence_shell);
         }
         if (adxad_Nhw > Nmax) {
            continue;
         }
         for (int nc = 0; nc <= maxShell; ++nc) {
            for (int nd = 0; nd <= maxShell; ++nd) {
               int taxta_Nhw = 0;
               if (nc > valence_shell) {
                  taxta_Nhw += (nc - valence_shell);
               }
               if (nd > valence_shell) {
                  taxta_Nhw += (nd - valence_shell);
               }
               if (taxta_Nhw > Nmax) {
                  continue;
               }
               na_nb_nc_nd = {na, nb, nc, nd};

               if (IsIncluded(na, nb, nc, nd))  // operator has this combination of HO shells
               {
                  ho_shells_combinations.push_back(na_nb_nc_nd);  //	in such case we do not need
               }
            }
         }
      }
   }
}

void Generate_ljj(int n, std::vector<int>& ljj) {
   for (int l = n; l >= 0; l -= 2) {
      for (int jj = std::abs(2 * l - 1); jj <= (2 * l + 1); jj += 2) {
         ljj.push_back(l);
         ljj.push_back(jj);
      }
   }
}

void GeneratePPNNSU2Basis(int n1, const std::vector<int>& l1jj1_vec, int n2,
                          const std::vector<int>& l2jj2_vec,
                          std::vector<std::array<int, 7>>& basis) {
   // ==> in the same shell generate |(I1 <= I2) J> states
   if (n1 == n2) {
      for (int i = 0; i < l1jj1_vec.size(); i += 2) {
         int l1 = l1jj1_vec[i];
         int jj1 = l1jj1_vec[i + 1];
         for (int j = i; j < l2jj2_vec.size(); j += 2) {
            int l2 = l2jj2_vec[j];
            int jj2 = l2jj2_vec[j + 1];
            for (int JJ = std::abs(jj1 - jj2); JJ <= (jj1 + jj2); JJ += 2) {
               // skip symmetric two-nucleon wave functions (a == b) and J odd
               if (i == j && ((JJ / 2) % 2)) {
                  continue;
               }
               basis.push_back({n1, l1, jj1, n2, l2, jj2, JJ});
            }
         }
      }
   } else {  // different shells ==> just a coupled product of two wfns
      for (int i = 0; i < l1jj1_vec.size(); i += 2) {
         int l1 = l1jj1_vec[i];
         int jj1 = l1jj1_vec[i + 1];
         for (int j = 0; j < l2jj2_vec.size(); j += 2) {
            int l2 = l2jj2_vec[j];
            int jj2 = l2jj2_vec[j + 1];
            for (int JJ = std::abs(jj1 - jj2); JJ <= (jj1 + jj2); JJ += 2) {
               basis.push_back({n1, l1, jj1, n2, l2, jj2, JJ});
            }
         }
      }
   }
}

// Generate TBDMEs labels such that
// n1 <= n2 && n3 <= n4
// and include only antisymmetric tensors
void GenerateUniqueTBDMES_PPNN(int n1, int n2, int n3, int n4, const std::set<int>& allowed_JJ0) {
   // tensors swithed order creational and/or annihilation operators are
   // up to a phase equal, e.g.,
   // {a+n1 a+n2}^J = (-)(-)j1+j2-J {a+n2 a+n1}^J
   // Thus we generate only
   if (n1 > n2 || n3 > n4) {
      return;
   }
   std::vector<int> l1jj1_vec;
   std::vector<int> l2jj2_vec;
   std::vector<int> l3jj3_vec;
   std::vector<int> l4jj4_vec;

   Generate_ljj(n1, l1jj1_vec);
   Generate_ljj(n2, l2jj2_vec);
   Generate_ljj(n3, l3jj3_vec);
   Generate_ljj(n4, l4jj4_vec);

   std::vector<std::array<int, 7>> n1l1jj1_n2l2jj2_JJ1_basis;
   std::vector<std::array<int, 7>> n3l3jj3_n4l4jj4_JJ2_basis;
   // generate J-coupled basis for two identical nucleon system
   GeneratePPNNSU2Basis(n1, l1jj1_vec, n2, l2jj2_vec, n1l1jj1_n2l2jj2_JJ1_basis);
   // generate J-coupled basis for two identical nucleon system
   GeneratePPNNSU2Basis(n3, l3jj3_vec, n4, l4jj4_vec, n3l3jj3_n4l4jj4_JJ2_basis);

   for (const auto& n1l1jj1_n2l2jj2_JJ1 : n1l1jj1_n2l2jj2_JJ1_basis) {
      int n1 = n1l1jj1_n2l2jj2_JJ1[0];
      int l1 = n1l1jj1_n2l2jj2_JJ1[1];
      int jj1 = n1l1jj1_n2l2jj2_JJ1[2];
      int n2 = n1l1jj1_n2l2jj2_JJ1[3];
      int l2 = n1l1jj1_n2l2jj2_JJ1[4];
      int jj2 = n1l1jj1_n2l2jj2_JJ1[5];
      int JJ1 = n1l1jj1_n2l2jj2_JJ1[6];
      for (const auto& n3l3jj3_n4l4jj4_JJ2 : n3l3jj3_n4l4jj4_JJ2_basis) {
         int n3 = n3l3jj3_n4l4jj4_JJ2[0];
         int l3 = n3l3jj3_n4l4jj4_JJ2[1];
         int jj3 = n3l3jj3_n4l4jj4_JJ2[2];
         int n4 = n3l3jj3_n4l4jj4_JJ2[3];
         int l4 = n3l3jj3_n4l4jj4_JJ2[4];
         int jj4 = n3l3jj3_n4l4jj4_JJ2[5];
         int JJ2 = n3l3jj3_n4l4jj4_JJ2[6];
         for (int JJ0 = std::abs(JJ1 - JJ2); JJ0 <= (JJ1 + JJ2); JJ0 += 2) {
            if (!allowed_JJ0.empty()) {
               // if a given J0 does not belong among allowed J0 values
               if (allowed_JJ0.find(JJ0) == allowed_JJ0.end()) {
                  continue;
               }
            }
            std::cout << n1 << " " << l1 << " " << jj1 << "  ";
            std::cout << n2 << " " << l2 << " " << jj2 << "   " << JJ1 << "   ";
            std::cout << n3 << " " << l3 << " " << jj3 << "  ";
            std::cout << n4 << " " << l4 << " " << jj4 << "   " << JJ2 << "   ";
            std::cout << JJ0 << std::endl;
         }
      }
   }
}
// Generate all physically allowed combinations of 2B tensors.
// This list will be redundant as many 2B tensors are equal (up to a phase)
void GenerateAllTBDMES_PPNN(int n1, int n2, int n3, int n4, const std::set<int>& allowed_JJ0) {
   std::vector<int> l1jj1_vec;
   std::vector<int> l2jj2_vec;
   std::vector<int> l3jj3_vec;
   std::vector<int> l4jj4_vec;

   Generate_ljj(n1, l1jj1_vec);
   Generate_ljj(n2, l2jj2_vec);
   Generate_ljj(n3, l3jj3_vec);
   Generate_ljj(n4, l4jj4_vec);

   for (size_t i1 = 0; i1 < l1jj1_vec.size(); i1 += 2) {
      int l1 = l1jj1_vec[i1];
      int jj1 = l1jj1_vec[i1 + 1];
      for (size_t i2 = 0; i2 < l2jj2_vec.size(); i2 += 2) {
         int l2 = l2jj2_vec[i2];
         int jj2 = l2jj2_vec[i2 + 1];
         for (int JJ1 = std::abs(jj1 - jj2); JJ1 <= (jj1 + jj2); JJ1 += 2) {
            // if 1 = 2 && J odd ==> not antisymmetric
            if ((n1 == n2 && l1 == l2 && jj1 == jj2) && ((JJ1 / 2) % 2)) {
               continue;
            }
            for (size_t i3 = 0; i3 < l3jj3_vec.size(); i3 += 2) {
               int l3 = l3jj3_vec[i3];
               int jj3 = l3jj3_vec[i3 + 1];
               for (size_t i4 = 0; i4 < l4jj4_vec.size(); i4 += 2) {
                  int l4 = l4jj4_vec[i4];
                  int jj4 = l4jj4_vec[i4 + 1];
                  for (int JJ2 = std::abs(jj3 - jj4); JJ2 <= (jj3 + jj4); JJ2 += 2) {
                     // if 3 = 4 && J odd ==> not antisymmetric
                     if ((n3 == n4 && l3 == l4 && jj3 == jj4) && ((JJ2 / 2) % 2)) {
                        continue;
                     }
                     for (int JJ0 = std::abs(JJ1 - JJ2); JJ0 <= (JJ1 + JJ2); JJ0 += 2) {
                        if (!allowed_JJ0.empty()) {
                           // if a given J0 does not belong among allowed J0 values
                           if (allowed_JJ0.find(JJ0) == allowed_JJ0.end()) {
                              continue;
                           }
                        }
                        std::cout << n1 << " " << l1 << " " << jj1 << "  ";
                        std::cout << n2 << " " << l2 << " " << jj2 << "   " << JJ1 << "   ";
                        std::cout << n3 << " " << l3 << " " << jj3 << "  ";
                        std::cout << n4 << " " << l4 << " " << jj4 << "   " << JJ2 << "   ";
                        std::cout << JJ0 << std::endl;
                     }
                  }
               }
            }
         }
      }
   }
}

void GenerateTBDMES_PN(int n1, int n2, int n3, int n4, const std::set<int>& allowed_JJ0) {
   std::vector<int> l1jj1_vec;
   std::vector<int> l2jj2_vec;
   std::vector<int> l3jj3_vec;
   std::vector<int> l4jj4_vec;

   Generate_ljj(n1, l1jj1_vec);
   Generate_ljj(n2, l2jj2_vec);
   Generate_ljj(n3, l3jj3_vec);
   Generate_ljj(n4, l4jj4_vec);

   for (size_t i1 = 0; i1 < l1jj1_vec.size(); i1 += 2) {
      int l1 = l1jj1_vec[i1];
      int jj1 = l1jj1_vec[i1 + 1];
      for (size_t i2 = 0; i2 < l2jj2_vec.size(); i2 += 2) {
         int l2 = l2jj2_vec[i2];
         int jj2 = l2jj2_vec[i2 + 1];
         for (int JJ1 = std::abs(jj1 - jj2); JJ1 <= (jj1 + jj2); JJ1 += 2) {
            for (size_t i3 = 0; i3 < l3jj3_vec.size(); i3 += 2) {
               int l3 = l3jj3_vec[i3];
               int jj3 = l3jj3_vec[i3 + 1];
               for (size_t i4 = 0; i4 < l4jj4_vec.size(); i4 += 2) {
                  int l4 = l4jj4_vec[i4];
                  int jj4 = l4jj4_vec[i4 + 1];
                  for (int JJ2 = std::abs(jj3 - jj4); JJ2 <= (jj3 + jj4); JJ2 += 2) {
                     for (int JJ0 = std::abs(JJ1 - JJ2); JJ0 <= (JJ1 + JJ2); JJ0 += 2) {
                        if (!allowed_JJ0.empty()) {
                           // if a given J0 does not belong among allowed J0 values
                           if (allowed_JJ0.find(JJ0) == allowed_JJ0.end()) {
                              continue;
                           }
                        }
                        std::cout << n1 << " " << l1 << " " << jj1 << "  ";
                        std::cout << n2 << " " << l2 << " " << jj2 << "   " << JJ1 << "   ";
                        std::cout << n3 << " " << l3 << " " << jj3 << "  ";
                        std::cout << n4 << " " << l4 << " " << jj4 << "   " << JJ2 << "   ";
                        std::cout << JJ0 << std::endl;
                     }
                  }
               }
            }
         }
      }
   }
}

int main(int argc, char** argv) {
   if (argc < 6) {
      std::cerr
          << "Print out quantum numbers \n"
          << "n1 l1 2j1  n2 l2 2j2   2J1   n3 l3 2j3  n4 l4 2j4   2J2    2J0 \n"
             "associated with selected set of two-body (two identical nucleons or proton-neutron) "
             "tensors\n"
          << "Usage: " << argv[0] << " <type> <valence shell> <Nmax> <parity> <JJ0 selection>\n\n"
          << "\nArguments:\n\t<type>: \n\t\tPN for proton-neutron tensors \n\t\tPPNN for two "
             "identical nucleon tensors \n\t<parity>: \n\t\t+1 for parity preserving tensors "
             "(n1+n2+n3+n4 is "
             "even)\n\t\t-1 for "
             "parity switching tensors (n1+n2+n3+n4 is odd).\n"
          << "\t<JJ0 selection>: list of selected 2J0 values; -1 if all possible J0 tensors "
             "will be generated."
          << std::endl;
      return EXIT_FAILURE;
   }

   std::string type = argv[1];
   if (type != "PN" && type != "PPNN") {
      std::cerr
          << "Unknown tensor <type>:'" << type
          << "'. Allowed values are 'PN' for proton-neutron or 'PPNN' for two-nucleon tensors."
          << std::endl;
      return EXIT_FAILURE;
   }
   bool isProtonNeutron = (type == "PN");
   int valence_shell = atoi(argv[2]);
   int Nmax = atoi(argv[3]);
   int nmax = Nmax + valence_shell;
   int parity = atoi(argv[4]);
   if (parity != 1 && parity != -1) {
      std::cerr << "Error: parity=" << parity
                << " is not valid value! Permitted values are either 1 for positive parity or -1 "
                   "for negative parity."
                << std::endl;
      return EXIT_FAILURE;
   }

   std::set<int> selectedJJ0;
   int JJ0 = atoi(argv[5]);
   if (JJ0 != -1) {
      selectedJJ0.insert(JJ0);
      for (int i = 6; i < argc; ++i) {
         JJ0 = atoi(argv[i]);
         selectedJJ0.insert(JJ0);
      }
   }

   /*
      for (auto JJ0 : selectedJJ0) {
         std::cout << "JJ0:" << JJ0 << std::endl;
      }
   */

   std::vector<NANBNCND> ho_shells_combinations;
   if (parity == 1) {
      GenerateAllCombinationsHOShells<PositiveParity>(Nmax, valence_shell, nmax,
                                                      ho_shells_combinations);
   } else {
      GenerateAllCombinationsHOShells<NegativeParity>(Nmax, valence_shell, nmax,
                                                      ho_shells_combinations);
   }

   for (const auto& nanbncnd : ho_shells_combinations) {
      int n1 = nanbncnd[0];
      int n2 = nanbncnd[1];
      int n3 = nanbncnd[2];
      int n4 = nanbncnd[3];
      if (isProtonNeutron) {
         GenerateTBDMES_PN(n1, n2, n3, n4, selectedJJ0);
      } else {
         GenerateUniqueTBDMES_PPNN(n1, n2, n3, n4, selectedJJ0);
         // generate overcomplete list of two identical nucleon SU(2) tensors
         // GenerateAllTBDMES_PPNN(n1, n2, n3, n4, selectedJJ0);
      }
   }
   return EXIT_SUCCESS;
}
