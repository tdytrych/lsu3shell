#include <SU3ME/SU3xSU2Basis.h>
#include <SU3ME/global_definitions.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <array>

using std::cin;
using std::cout;
using std::endl;
using std::ofstream;
using std::string;

int main(int argc, char **argv) {
   int valence_shell;
   int Nmax;
   int nmax;
   int parity;
   int JJ0;
   int generateRecoupledFile;

   std::cout << argv[0]
             << ": generate labels of one-body tensors n1 n2 lm0 mu0 2S0 allowed for a given model "
                "space and store them in an output file."
             << std::endl;
   std::cout << "enter valence shell HO number:";
   std::cin >> valence_shell;

   std::cout << "enter Nmax:";
   std::cin >> Nmax;
   nmax = Nmax + valence_shell;

   std::cout << "Parity (1 ... positive, -1 ... negative, 0 ... both parities):";
   std::cin >> parity;

   std::cout << "Select tensors that have specific J0 value? Enter 2J0 or -1 to include all tensors:";
   std::cin >> JJ0;
   if (JJ0 >= 0 && (JJ0%2))
   {
      std::cerr << "2J0 value cannot be an odd number!" << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Generate file with recoupled one-body tensors for ComputeMultiShellRecoupledRME (1 ... yes):";
   std::cin >> generateRecoupledFile;

// positive == even; negative == odd
   enum {EVEN = 1, ODD = -1};

   std::vector<std::array<int, 5>> su3_tensors;
   for (int na = 0; na <= nmax; ++na) {
      for (int nb = 0; nb <= nmax; ++nb) {
         SU3xSU2_VEC lm0mu0_irreps;
         SU3xSU2::Couple(SU3xSU2::LABELS(1, na, 0, 1), SU3xSU2::LABELS(1, 0, nb, 1), lm0mu0_irreps);
         for (int i = 0; i < lm0mu0_irreps.size(); ++i) {
            // even == parity conserving
            if (parity == EVEN && (na + nb) % 2)  // na + nb odd not allowed even parity
            {
               continue;
            }
            // odd == not conserving parity
            if (parity == ODD && ((na + nb) % 2 == 0))  // na + nb even no allowed for odd parity
            {
               continue;
            }
            // select SU(3)xSU(2) tensors that have JJ0
            if (JJ0 >= 0)
            {
               SU3xSU2::BasisJfixed lm0mu0(lm0mu0_irreps[i], JJ0);
               // true only if JJ0 does not exist in (lm0 mu0) S0
               if (lm0mu0.dim() == 0) {
                  continue;
               }
            }
            su3_tensors.push_back(
                {na, nb, lm0mu0_irreps[i].lm, lm0mu0_irreps[i].mu, lm0mu0_irreps[i].S2});
         }
      }
   }

   std::stringstream ss;
   ss << valence_shell << "vshell_" << Nmax << "Nmax_"; 
   if (parity == 1)
   {
      ss << "positive_parity";
   }
   else if (parity == -1)
   {

      ss << "negative_parity";
   }
   else if (parity == 0)
   {
      ss << "both_parities";
   }
   
   if (JJ0 >= 0) {
      ss << "_with_J0_" << JJ0 / 2;
      }
   ss << ".su3_a+ta_tensors";

   std::cout << "na nb lm0 mu0 2S0 --> " << ss.str() << " file." << std::endl;
   std::ofstream tensor_file(ss.str());
   if (!tensor_file)
   {
      std::cerr << "Unable to open '" << ss.str() << "' as an output file for tensor labels."
                << std::endl;
      return EXIT_FAILURE;
   }
   for (const auto& t : su3_tensors)
   {
      int na, nb, lm0, mu0, SS0;
      na = t[0]; nb = t[1]; lm0 = t[2]; mu0 = t[3]; SS0 = t[4];
      tensor_file << na << " " << nb << " ";
      tensor_file << lm0 << " " << mu0 << " " << SS0;
      tensor_file << std::endl;
   }

   if (generateRecoupledFile) {
      ss << ".Recoupled";

      std::cout << "HO recoupled tensors --> " << ss.str() << " file." << std::endl;
      std::ofstream tensor_file(ss.str());
      if (!tensor_file) {
         std::cerr << "Unable to open '" << ss.str() << "' as an output file for tensor labels."
                   << std::endl;
         return EXIT_FAILURE;
      }

      std::map<std::pair<int,int>, std::vector<std::array<int, 3>>> nanb_w0S0vec;
      for (const auto& t : su3_tensors) {
         int na, nb, lm0, mu0, SS0;
         na = t[0];
         nb = t[1];
         lm0 = t[2];
         mu0 = t[3];
         SS0 = t[4];
         auto iter = nanb_w0S0vec.find(std::make_pair(na, nb));
         if (iter == nanb_w0S0vec.end())
         {
            nanb_w0S0vec.insert(std::make_pair(std::make_pair(na, nb), std::vector<std::array<int, 3>>(1, {lm0, mu0, SS0})));
         }
         else
         {
            iter->second.push_back({lm0, mu0, SS0});
         }
      }

      for (const auto& nanb_tensors : nanb_w0S0vec)
      {
         int na, nb;
         na = nanb_tensors.first.first;
         nb = nanb_tensors.first.second;
         if (na > nb)
         {
            tensor_file << -(nb + 1) << " " << (na + 1) << " ";
         }
         else
         {
            tensor_file << (na + 1) << " " << -(nb + 1) << " ";
         }
         const auto& w0SS0vec = nanb_tensors.second;
         tensor_file << "\n" << w0SS0vec.size() << std::endl;
         for (const auto w0SS0 : w0SS0vec) {
            int lm0 = w0SS0[0];
            int mu0 = w0SS0[1];
            int SS0 = w0SS0[2];
            tensor_file << lm0 << " " << mu0 << " " << SS0;
            tensor_file << std::endl;
            tensor_file << "1.0 1.0" << std::endl;
         }
      }
   }
   return EXIT_SUCCESS;
}
