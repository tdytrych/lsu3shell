#include <SU3ME/SU3InteractionRecoupler.h>
#include <SU3ME/SU3xSU2Basis.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <su3.h>

#include <array>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>

//	This struct is used for remove_if algorithm ==> it has to return true if a given tensor is
// not included
struct lm0_plus_mu0_is_Odd {
   inline bool operator()(const SU3::LABELS& su3ir) const {
      // returns true if lm + mu is odd.
      return ((su3ir.lm + su3ir.mu) % 2);
   }
};

//	This struct is used for remove_if algorithm ==> it has to return true if a given tensor is
// not included
struct lm0_plus_mu0_is_Even {
   inline bool operator()(const SU3::LABELS& su3ir) const {
      // returns true if lm + mu is even
      return !((su3ir.lm + su3ir.mu) % 2);
   }
};

//	This struct is used for remove_if algorithm ==> it has to return true if a given tensor is
// not included
struct Antisymmetry_sel_rules {
   inline bool operator()(const SU3xSU2::LABELS& ir) const {
      // returns true if lm + mu + S is odd, which is unphysical for two-fermion system
      return ((ir.lm + ir.mu + ir.S2 / 2) % 2);
   }
};

// This structs are used in GenerateAllCombinationsHOShells
// Returns true if selected
struct PositiveParity {
   // true if n1 + n2 + n3 + n4 is even
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((n1 + n2 + n3 + n4) % 2 == 0);
   }
};

// This structs are used in GenerateAllCombinationsHOShells
// Returns true if selected
struct NegativeParity {
   // true if n1 + n2 + n3 + n4 is odd
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((n1 + n2 + n3 + n4) % 2);
   }
};

enum { kNA = 0, kNB = 1, kNC = 2, kND = 3 };
typedef std::array<int, 4> NANBNCND;

const int SfSiS0[6][3] = {{0, 0, 0}, {2, 2, 0}, {0, 2, 2}, {2, 0, 2}, {2, 2, 2}, {2, 2, 4}};

template <typename SelectionRules>
void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell,
                                     std::vector<NANBNCND>& ho_shells_combinations) {
   SelectionRules IsIncluded;
   ho_shells_combinations.resize(0);

   NANBNCND na_nb_nc_nd;
   for (int na = 0; na <= maxShell; ++na) {
      for (int nb = 0; nb <= maxShell; ++nb) {
         int adxad_Nhw = 0;
         if (na > valence_shell) {
            adxad_Nhw += (na - valence_shell);
         }
         if (nb > valence_shell) {
            adxad_Nhw += (nb - valence_shell);
         }
         if (adxad_Nhw > Nmax) {
            continue;
         }
         for (int nc = 0; nc <= maxShell; ++nc) {
            for (int nd = 0; nd <= maxShell; ++nd) {
               int taxta_Nhw = 0;
               if (nc > valence_shell) {
                  taxta_Nhw += (nc - valence_shell);
               }
               if (nd > valence_shell) {
                  taxta_Nhw += (nd - valence_shell);
               }
               if (taxta_Nhw > Nmax) {
                  continue;
               }
               na_nb_nc_nd = {na, nb, nc, nd};

               if (IsIncluded(na, nb, nc, nd))  // operator has this combination of HO shells
               {
                  ho_shells_combinations.push_back(na_nb_nc_nd);  //	in such case we do not need
               }
            }
         }
      }
   }
}

void ShowTensor(int n1, int n2, int n3, int n4, const SU3xSU2::LABELS& wf,
                const SU3xSU2::LABELS& wi, const SU3xSU2::LABELS& w0) {
   int ssf = wf.S2;
   int ssi = wi.S2;
   int ss0 = w0.S2;
   std::cout << n1 << " " << n2 << " " << n3 << " " << n4 << "   ";
   std::cout << (int)wf.lm << " " << (int)wf.mu << " " << ssf << "   ";
   std::cout << (int)wi.lm << " " << (int)wi.mu << " " << ssi << "   ";
   std::cout << (int)w0.lm << " " << (int)w0.mu << " " << ss0 << std::endl;
}

enum { N1, N2, N3, N4, LMF, MUF, SSF, LMI, MUI, SSI, RHO0MAX, LM0, MU0, SS0 };

void RecoupleAndSaveTensors(const std::vector<std::array<int, 14>>& su3_tensors,
                            const std::string& sOutputFileNamePPNN) {
   k_dependent_tensor_strenghts = false;
   SU3InteractionRecoupler Recoupler;

   for (const auto labels : su3_tensors) {
      SU3::LABELS IRf, IRi, Irrep0;
      int Sf, Si, S0;
      int n1, n2, n3, n4;
      int rho0max, rho0;

      n1 = labels[N1];
      n2 = labels[N2];
      n3 = labels[N3];
      n4 = labels[N4];
      IRf.lm = labels[LMF];
      IRf.mu = labels[MUF];
      Sf = labels[SSF];
      IRi.lm = labels[LMI];
      IRi.mu = labels[MUI];
      Si = labels[SSI];
      rho0max = labels[RHO0MAX];
      Irrep0.rho = rho0max;
      Irrep0.lm = labels[LM0];
      Irrep0.mu = labels[MU0];
      S0 = labels[SS0];

      size_t nCoeffs = 3 * rho0max;
      std::vector<double> CoeffsPPNNPN(nCoeffs, 1.0);

      std::cout << "Recoupling: " << std::endl;
      std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
      std::cout << "\t" << IRf << " x " << IRi << " -> " << Irrep0;
      std::cout << "\t"
                << "Sf = " << Sf << "/2  Si = " << Si << "/2 S0 = " << S0 << "/2" << std::endl;

      char n1n2n3n4[4];
      n1n2n3n4[0] = n1;
      n1n2n3n4[1] = n2;
      n1n2n3n4[2] = n3;
      n1n2n3n4[3] = n4;

      //	Note that InsertTTY1 uses formulas for recoupling which do not depend on J0
      //	and M0 (see A.35-A.43) and hence these quantum labeks do not need to be specified.
      int bSuccess = Recoupler.Insert_adad_aa_Tensor(n1n2n3n4, SU3xSU2::LABELS(IRf, Sf),
                                                     SU3xSU2::LABELS(IRi, Si),
                                                     SU3xSU2::LABELS(Irrep0, S0), CoeffsPPNNPN);
      if (!bSuccess) {
         std::cout << "IMPLEMENT: ";

         std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
         std::cout << "\t" << IRf << " x " << IRi << " -> " << Irrep0;
         std::cout << "\t"
                   << "Sf = " << Sf << "/2  Si = " << Si << "/2 S0 = " << S0 << "/2";
         std::cout << std::endl;
         return;
      }
   }
   // We are interested in recoupled tensors and their labels only. Some coefficients from Recoupler
   // are vanishing and then they are discarded by ComputeMultiShellRecoupledRMEs. So I set all of
   // them to 1.0 to make sure this does not happen.
   for (size_t n = 0; n <= 3; ++n) {
      auto& map_structure_2_tensors = Recoupler.m_PPNN[n];
      for (auto& structure_tensors : map_structure_2_tensors) {
         for (auto& tensors : structure_tensors.second) {
            auto& coeffs = tensors.second;
            for (auto& coeff : coeffs) {
               coeff = 1;
            }
         }
      }
   }
   // Ok, now we can save it into file compatible with ComputeMultiShellRecoupledRMEs
   Recoupler.Save(sOutputFileNamePPNN, TENSOR::PPNN);
}

void SaveSU3Tensors(std::vector<std::array<int, 14>>& su3_tensors, const std::string& filename) {
   std::ofstream file(filename);
   if (!file) {
      std::cerr << "Could not open output file '" << filename << "'!" << std::endl;
      return;
   }

   for (const auto labels : su3_tensors) {
      SU3::LABELS wf, wi, w0;
      int ssf, ssi, ss0;
      int n1, n2, n3, n4;

      n1 = labels[N1];
      n2 = labels[N2];
      n3 = labels[N3];
      n4 = labels[N4];
      wf.lm = labels[LMF];
      wf.mu = labels[MUF];
      ssf = labels[SSF];
      wi.lm = labels[LMI];
      wi.mu = labels[MUI];
      ssi = labels[SSI];
      w0.lm = labels[LM0];
      w0.mu = labels[MU0];
      ss0 = labels[SS0];

      file << n1 << " " << n2 << " " << n3 << " " << n4 << "   ";
      file << (int)wf.lm << " " << (int)wf.mu << " " << ssf << "   ";
      file << (int)wi.lm << " " << (int)wi.mu << " " << ssi << "   ";
      file << (int)w0.lm << " " << (int)w0.mu << " " << ss0 << std::endl;
   }
}

int main(int argc, char** argv) {
   int valence_shell;
   int Nmax;
   int nmax;
   int parity;
   int su3_selection, su2_selection;
   int generateRecoupledFile;
   int JJ0;

   std::cout << "enter valence shell HO number:";
   std::cin >> valence_shell;

   std::cout << "enter Nmax:";
   std::cin >> Nmax;
   nmax = Nmax + valence_shell;

   std::cout << "Parity (1 ... positive, -1 ... negative):";
   std::cin >> parity;
   if (parity != 1 && parity != -1) {
      std::cerr << "Error: parity=" << parity
                << " is not valid value! Permitted values are either 1 for positive parity or -1 "
                   "for negative parity."
                << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "SU(3) selection (0 ... (lm0 + mu0) even, 1 ... (lm0 + mu0) odd, -1 ... all):";
   std::cin >> su3_selection;

   std::cout
       << "Select tensors that have specific J0 value? Enter 2J0 or -1 to include all tensors:";
   std::cin >> JJ0;
   if (JJ0 >= 0 && (JJ0 % 2)) {
      std::cerr << "2J0 value cannot be an odd number!" << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Generate recoupled tensors for ComputeMultiShellRecoupledRME (1 ... yes):";
   std::cin >> generateRecoupledFile;

   bool selectEven = false;
   bool selectOdd = false;
   if (su3_selection == 0) {
      selectEven = true;
   } else if (su3_selection == 1) {
      selectOdd = true;
   } else if (su3_selection != -1) {
      std::cerr << "Wrong specification of SU(3) tensor selection: " << su3_selection
                << "! Only 1, 0, or -1 are allowed values " << std::endl;
      return EXIT_FAILURE;
   }

   std::stringstream ss;
   ss << valence_shell << "vshell_" << Nmax << "Nmax_" << parity << "parity_";
   switch (su3_selection) {
      case 0:
         ss << "evenSU3";
         break;
      case 1:
         ss << "oddSU3";
         break;
      case -1:
         ss << "allSU3";
         break;
   };
   if (JJ0 >= 0) {
      ss << "_with_J0_" << JJ0 / 2;
   }
   ss << ".su3_tensors";

   std::string filename(ss.str());

   std::vector<NANBNCND> ho_shells_combinations;
   if (parity == 1) {
      GenerateAllCombinationsHOShells<PositiveParity>(Nmax, valence_shell, nmax,
                                                      ho_shells_combinations);
   } else {
      GenerateAllCombinationsHOShells<NegativeParity>(Nmax, valence_shell, nmax,
                                                      ho_shells_combinations);
   }

   std::vector<std::array<int, 14>> su3_tensors;

   size_t ntensors_JJ0 = 0;
   SU3xSU2_VEC FinalIrreps;  //	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
   SU3xSU2_VEC InitIrreps;   //	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)
   for (auto nanbncnd : ho_shells_combinations) {
      int n1 = nanbncnd[0];
      int n2 = nanbncnd[1];
      int n3 = nanbncnd[2];
      int n4 = nanbncnd[3];

      if (n1 > n2 || n3 > n4) {
         continue;
      }

      FinalIrreps.clear();
      InitIrreps.clear();

      SU3xSU2::Couple(SU3xSU2::LABELS(n1, 0, 1), SU3xSU2::LABELS(n2, 0, 1), FinalIrreps);
      SU3xSU2::Couple(SU3xSU2::LABELS(0, n3, 1), SU3xSU2::LABELS(0, n4, 1), InitIrreps);

      if (n1 == n2)  // we need to select antisymmetric SU(3)xSU(2) irreps
      {
         // remove if (lm + mu + 2) is odd
         FinalIrreps.erase(
             std::remove_if(FinalIrreps.begin(), FinalIrreps.end(), Antisymmetry_sel_rules()),
             end(FinalIrreps));
      }

      if (n3 == n4)  // we need to select antisymmetric SU(3)xSU(2) irreps
      {
         // remove if (lm + mu + 2) is odd
         InitIrreps.erase(
             std::remove_if(InitIrreps.begin(), InitIrreps.end(), Antisymmetry_sel_rules()),
             end(InitIrreps));
      }

      //	iterate over (lmf muf) irreps
      for (auto wf : FinalIrreps) {
         //	iterate over (lmi mui) irreps
         for (auto wi : InitIrreps) {
            SU3xSU2_VEC Irreps0;
            SU3xSU2::Couple(wf, wi, Irreps0);  // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
            if (selectEven) {
               //	exclude tensors with (lm0 + mu0 = odd) as they do not occur realistic
               // Hamiltonian
               Irreps0.erase(std::remove_if(Irreps0.begin(), Irreps0.end(), lm0_plus_mu0_is_Odd()),
                             end(Irreps0));
            } else if (selectOdd) {
               Irreps0.erase(std::remove_if(Irreps0.begin(), Irreps0.end(), lm0_plus_mu0_is_Even()),
                             end(Irreps0));
            }

            for (auto w0 : Irreps0) {
               if (JJ0 >= 0)  // => generate (lm0 mu0)S0 that have J0
               {
                  SU3xSU2::BasisJcut basis(w0, JJ0);
                  // select (lm0 mu)S0 with 2J0 value
                  if (basis.dim()) {
                     ntensors_JJ0 += w0.rho * basis.dim();
                     // ShowTensor(n1, n2, n3, n4, wf, wi, w0);
                     su3_tensors.push_back({n1, n2, n3, n4, wf.lm, wf.mu, wf.S2, wi.lm, wi.mu,
                                            wi.S2, w0.rho, w0.lm, w0.mu, w0.S2});
                  }
               } else  // ==> no SU(2) selection => include
               {
                  // ShowTensor(n1, n2, n3, n4, wf, wi, w0);
                  su3_tensors.push_back({n1, n2, n3, n4, wf.lm, wf.mu, wf.S2, wi.lm, wi.mu, wi.S2,
                                         w0.rho, w0.lm, w0.mu, w0.S2});
               }
            }
         }
      }
   }
   SaveSU3Tensors(su3_tensors, filename);
   if (generateRecoupledFile) {
      std::string sOutputFileNamePPNN(ss.str());
      sOutputFileNamePPNN += ".Recoupled";
      su3::init();
      RecoupleAndSaveTensors(su3_tensors, sOutputFileNamePPNN);
      su3::finalize();
   }
   // to be compared with Generate_adadtata_SU3list PPNN <valence> <Nmax> <parity> <0>
   //   std::cout << ntensors_JJ0 << std::endl;
}
