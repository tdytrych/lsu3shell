$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h :=  CSRMEMatrix su3densePN_helpers.h
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := ComputeMultiShellRecoupledRMEs \
	ReadRMEsCSR ComputeMultiShellRMEs_ad_ta \
	su3densePN_slow_silly su3denseN_slow_silly \
	ComputeMultiShellRMEs_adad_tata su3denseP \
	GenerateMultiShellRMEs_adadtata\
	su3densePN_adadtata_pnnp \
	Generate_adadtata_pnnp_SU3list_k0LL0JJ0 \
	Generate_adadtata_SU3list Generate_adta_SU3list \
	TBDMEsSU3toSU2 \
   Generate_adadtata_SU2list \
	su3densePN \
	su3densePN_groups \
	su3densePN_groups_dev \


# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))
