#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/LRUCache.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <stdexcept>
#include <cmath>
#include <vector>
#include <stack>
#include <ctime>

using namespace std;

unsigned long CalculateME(	const CInteractionPPNN& interactionPPNN,
					const CInteractionPN& interactionPN,
					const CncsmSU3BasisFastIteration& bra, 
					const CncsmSU3BasisFastIteration& ket, 
					const unsigned int ndiag,
					const unsigned long firstStateId_bra, const unsigned int iblock, 
					const unsigned long firstStateId_ket, const unsigned int jblock,
							const string& outputFileName)
{
	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_ket_distr_p;
	unsigned char num_vacuums_ket_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;
	unsigned int ap_bra, an_bra;
	unsigned int ap_ket, an_ket;
	SU3xSU2::LABELS omega_pn_bra, omega_pn_ket;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);
	
	unsigned long stateId_bra(firstStateId_bra);
	unsigned int dim_bra_irrep = 0;

	SingleDistribution distr_p_bra, distr_n_bra, distr_p_ket, distr_n_ket;
	UN::SU3xSU2_VEC gamma_p_bra, gamma_n_bra, gamma_p_ket, gamma_n_ket;
	SU3xSU2_VEC omega_p_bra, omega_n_bra, omega_p_ket, omega_n_ket;

	unsigned int dim_ket_irrep = 0;

	unsigned char deltaP, deltaN;

	unsigned long number_nonzero_me(0);
	
	unsigned long dim_bra = bra.dim();
	unsigned long dim_ket = ket.dim();
	
	ofstream fresults(outputFileName.c_str());
	fresults.precision(10);
	fresults << "%%MatrixMarket matrix coordinate real symmetric" << endl;
	fresults << dim_bra << " " << dim_ket << " here you must enter number of nonzero matrix elements" << endl;

	unsigned int previous_bra_index = 0xFFFF;
	unsigned char status_bra = CncsmSU3BasisFastIteration::kNewDistr_p;
	for (unsigned int bra_index = 0; bra_index < bra.size(); ++bra_index, status_bra = bra.status(bra_index, previous_bra_index))
	{
		omega_pn_bra  = bra.getOmega_pn(bra_index);
		dim_bra_irrep = bra.rhomax_x_dim(bra_index);	// rhomax * dim[(lm mu)S]
		switch (status_bra)
		{
			case CncsmSU3BasisFastIteration::kNewDistr_p: 
			case CncsmSU3BasisFastIteration::kNewDistr_n: 
			case CncsmSU3BasisFastIteration::kNewGamma_p: 
			case CncsmSU3BasisFastIteration::kNewGamma_n: 
			case CncsmSU3BasisFastIteration::kNewOmega_p: 
				ap_bra = bra.getMult_p(bra_index);
			case CncsmSU3BasisFastIteration::kNewOmega_n: 
				an_bra = bra.getMult_n(bra_index);
		}

		unsigned long stateId_ket((iblock == jblock) ? stateId_bra : firstStateId_ket);
		size_t afmax = ap_bra*an_bra*omega_pn_bra.rho;
		IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(bra_index));
		size_t num_rows_in_block = afmax*braSU3xSU2basis.dim();
		vector<vector<float> > vals_local(num_rows_in_block);	// how to resize elements ?
		vector<vector<size_t> > col_ind_local(num_rows_in_block);

		unsigned int previous_ket_index = 0xFFFF;
		unsigned char status_ket = CncsmSU3BasisFastIteration::kNewDistr_p;
		for (unsigned int ket_index = (iblock == jblock) ? bra_index : 0; ket_index < ket.size(); ++ket_index, status_ket = ket.status(ket_index, previous_ket_index))
		{
			dim_ket_irrep = ket.rhomax_x_dim(ket_index);
			if (status_ket == CncsmSU3BasisFastIteration::kNewDistr_p)
			{
				distr_p_bra.resize(0); bra.getDistr_p(bra_index, distr_p_bra);
				gamma_p_bra.resize(0); bra.getGamma_p(bra_index, gamma_p_bra);
				omega_p_bra.resize(0); bra.getOmega_p(bra_index, omega_p_bra);
				
				distr_p_ket.resize(0); ket.getDistr_p(ket_index, distr_p_ket);
				hoShells_p.resize(0);
				deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_p_bra, gamma_p_bra, omega_p_bra, distr_p_ket, hoShells_p, num_vacuums_ket_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
			}

			if (status_ket >= CncsmSU3BasisFastIteration::kNewDistr_n)
			{
				distr_n_bra.resize(0); bra.getDistr_n(bra_index, distr_n_bra);
				gamma_n_bra.resize(0); bra.getGamma_n(bra_index, gamma_n_bra);
				omega_n_bra.resize(0); bra.getOmega_n(bra_index, omega_n_bra); 

				distr_n_ket.resize(0); ket.getDistr_n(ket_index, distr_n_ket);
				hoShells_n.resize(0);
				deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_n_bra, gamma_n_bra, omega_n_bra, distr_n_ket, hoShells_n, num_vacuums_ket_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
			}

			if (status_ket >= CncsmSU3BasisFastIteration::kNewGamma_p)
			{
				if (deltaP <= 4)
				{
					if (!selected_tensorsPP.empty())
					{
						std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
					}
					selected_tensorsPP.resize(0);

					if (!selected_tensors_p_pn.empty())
					{
						std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
					}
					selected_tensors_p_pn.resize(0);

					gamma_p_ket.resize(0); ket.getGamma_p(ket_index, gamma_p_ket);
					TransformGammaKet_SelectByGammas(hoShells_p, distr_p_ket, num_vacuums_ket_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_p_bra, gamma_p_ket, selected_tensorsPP, selected_tensors_p_pn);

				}
			}

			if (status_ket >= CncsmSU3BasisFastIteration::kNewGamma_n)
			{
				if (deltaP + deltaN <= 4)
				{
					if (!selected_tensorsNN.empty())
					{
						std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
					}
					selected_tensorsNN.resize(0);

					if (!selected_tensors_n_pn.empty())
					{
						std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
					}
					selected_tensors_n_pn.resize(0);

					gamma_n_ket.resize(0); ket.getGamma_n(ket_index, gamma_n_ket);
					TransformGammaKet_SelectByGammas(hoShells_n, distr_n_ket, num_vacuums_ket_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_n_bra, gamma_n_ket, selected_tensorsNN, selected_tensors_n_pn);
				}
			}

			if (status_ket >= CncsmSU3BasisFastIteration::kNewOmega_p)
			{
				ap_ket = ket.getMult_p(ket_index);
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					omega_p_ket.resize(0); ket.getOmega_p(ket_index, omega_p_ket);
					TransformOmegaKet_CalculateRME(distr_p_ket, gamma_p_bra, omega_p_bra, gamma_p_ket, num_vacuums_ket_distr_p, selected_tensorsPP, selected_tensors_p_pn, omega_p_ket, rmeCoeffsPP, rme_index_p_pn);
				}
			}

			if (status_ket >= CncsmSU3BasisFastIteration::kNewOmega_n)
			{
				an_ket = ket.getMult_n(ket_index);
				if (deltaP + deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					omega_n_ket.resize(0); ket.getOmega_n(ket_index, omega_n_ket);
					TransformOmegaKet_CalculateRME(distr_n_ket, gamma_n_bra, omega_n_bra, gamma_n_ket, num_vacuums_ket_distr_n, selected_tensorsNN, selected_tensors_n_pn, omega_n_ket, rmeCoeffsNN, rme_index_n_pn);
				}	
			}

			if (deltaP + deltaN <= 4)
			{
				omega_pn_ket  = ket.getOmega_pn(ket_index);
				Reset_rmeCoeffs(rmeCoeffsPNPN);

				if (bra.neutronConf(bra_index) == ket.neutronConf(ket_index) && !rmeCoeffsPP.empty())
				{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
					CreateIdentityOperatorRME(bra.getNeutronSU3xSU2(bra_index), ket.getNeutronSU3xSU2(ket_index), an_ket, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
					Calculate_Proton_x_Identity_MeData(omega_pn_bra, omega_pn_ket, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
				}

				if (bra.protonConf(bra_index) == ket.protonConf(ket_index) && !rmeCoeffsNN.empty())
				{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
					CreateIdentityOperatorRME(bra.getProtonSU3xSU2(bra_index), ket.getProtonSU3xSU2(ket_index), ap_ket, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
					Calculate_Identity_x_Neutron_MeData(omega_pn_bra, omega_pn_ket, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
				}

				if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
				{
					CalculatePNInteractionMeData(interactionPN, omega_pn_bra, omega_pn_ket, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
				}
				size_t aimax = ap_ket*an_ket*omega_pn_ket.rho;
				IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(ket_index));
				if (!rmeCoeffsPNPN.empty())
				{
					if (stateId_bra == stateId_ket)
					{
						assert(iblock == jblock );
						CalculateME_Diagonal_UpperTriang_Scalar(afmax, braSU3xSU2basis, stateId_bra, aimax, ketSU3xSU2basis, stateId_ket, rmeCoeffsPNPN, vals_local, col_ind_local);
					}
					else
					{
						CalculateME_nonDiagonal_Scalar(afmax, braSU3xSU2basis, stateId_bra, aimax, ketSU3xSU2basis, stateId_ket, rmeCoeffsPNPN, vals_local, col_ind_local);
					}
				}
			}
			stateId_ket += ap_ket*an_ket*dim_ket_irrep;
			previous_ket_index = ket_index;
		}

		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			size_t irow = stateId_bra + i;
			for (size_t j = 0; j < vals_local[i].size(); ++j, ++number_nonzero_me)
			{
				fresults << (irow + 1) << " " << (col_ind_local[i][j] + 1) << " " << vals_local[i][j] << "\n";
			}
		}
		cout << stateId_bra << endl;
		stateId_bra += ap_bra*an_bra*dim_bra_irrep; 
		previous_bra_index = bra_index;
	}
	fresults << number_nonzero_me;

	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;
	return number_nonzero_me;
}

int main(int argc,char **argv)
{
	if (argc != 3 && argc != 6)
	{
	  cout << "Usage: "<<argv[0]<<" <model space definition> <outputfile> [<ndiag> <iblock> <jblock>]" << endl;
		cout << "Name of the file with the model space definition and the name of the output file are the required input parameters." << endl;
		return EXIT_FAILURE;
	}

	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);

	int Z    = ncsmModelSpace.number_of_protons();
	int N    = ncsmModelSpace.number_of_neutrons();
	int Nmax = ncsmModelSpace.back().N();

	CBaseSU3Irreps baseSU3Irreps(Z, N, Nmax); 

	unsigned int ndiag = 1;
	unsigned int idiag = 0;
	unsigned int jdiag = 0;

	if (argc == 6) 
	{
		 ndiag = atoi(argv[3]);
		 idiag = atoi(argv[4]);
		 jdiag = atoi(argv[5]); 
 	}

	CncsmSU3BasisFastIteration ket(ncsmModelSpace, jdiag, ndiag);	
	CncsmSU3BasisFastIteration bra(ncsmModelSpace, idiag, ndiag);	

	std::ofstream log_file("/dev/null");

	CInteractionPPNN interactionPPNN(baseSU3Irreps, true, log_file);
	CInteractionPN interactionPN(baseSU3Irreps, true, true, log_file);
	int A = Z + N;
	const float hw = 15;
	const float lambda = 50;

	try
	{
		COperatorLoader operatorLoader;

		operatorLoader.AddTrel(A, hw);
		operatorLoader.AddVcoul(hw);
		operatorLoader.AddVnn();
		operatorLoader.AddBdB(A, lambda);
/** J0 and M0 labels of the operator are stored as: MECalculatorData::JJ0_ and  MECalculatorData::MM0_ 
 	and are being read from one-body interaction files ...
 */
		operatorLoader.Load(0, interactionPPNN, interactionPN);
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
	   return EXIT_FAILURE;
    }

//	interactionPN.ShowTensorGroupsAndTheirTensorStructures();
//	interactionPN.ShowInteractionStructures();
//	return 1;

//	The order of coefficients is given as follows:
//  index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//	TransformTensorStrengthsIntoPP_NN_structure turns that into:
//  index = type*k0max*rho0max + k0*rho0max + rho0
	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

	time_t start,end;
	time(&start);
	unsigned long firstStateId_bra = bra.getFirstStateId();
	unsigned long firstStateId_ket = ket.getFirstStateId();


	unsigned long nonvanishing_me = CalculateME(interactionPPNN, interactionPN, bra, ket, ndiag, firstStateId_bra, idiag, firstStateId_ket, jdiag, argv[2]);
	time(&end);
	double dif = difftime (end,start);
	cout << "Resulting time: " << dif << " seconds." << endl;
	cout << "Number of non-vanishing matrix elements: " << nonvanishing_me << endl;
}
