#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <LookUpContainers/lock.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <stack>
#include <stdexcept>
#include <vector>

//#define LSU3SHELL_REPORT_MAXRSS
#ifdef LSU3SHELL_REPORT_MAXRSS
   #include <sys/resource.h>
#endif

#include <boost/mpi.hpp>
//	To be able to load and distribute basis from a binary archive file
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <su3.h>

#include <ompilancz/span.h>
#include <ompilancz/VMV.h>

using namespace std;

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col) {
   assert(ndiag * (ndiag + 1) / 2 >= my_rank);
   /*
           if (ndiag % 2 == 0)
           {
                   if (my_rank == 0)
                   {
                           cout << "number of diagonal processes = " << ndiag << " must be an odd
      number." << endl;
                   }
                   MPI_Finalize();
                   return EXIT_FAILURE;
           }
   */
   int executing_process_id(0);
   for (size_t i = 0; i < ndiag; ++i) {
      row = 0;
      for (col = i; col < ndiag; ++col, ++row, ++executing_process_id) {
         if (my_rank == executing_process_id) {
            return;
         }
      }
   }
}

uintmax_t CalculateME(const std::string& hilbert_space_definition_file_name,
                      const std::string& hamiltonian_file_name, int ndiag, int idiag, int jdiag,
                      lsu3::VBC_Matrix& vbc, long int& nstates_sofar, uint64_t& dim,
                      bool simulate,
                      proton_neutron::ModelSpace& ncsmModelSpace
                      ) {
   boost::mpi::communicator mpi_comm;
   int my_rank = mpi_comm.rank();
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   //  test if the first parameter is model space file name
   bool modelSpaceProvided =
       hilbert_space_definition_file_name.find(".model_space") !=
       std::string::npos;  // true if the first argument contains ".model_space"
                           //	time how long it will take to create bra and ket basis
   start = std::chrono::system_clock::now();

   lsu3::CncsmSU3xSU2Basis ket, bra;

   if (simulate) {
      if (!modelSpaceProvided)  // ==> we need to load basis states from files
      {
         cout << "Reading basis from '"
              << MakeBasisName(hilbert_space_definition_file_name, idiag, ndiag) << "'."
              << std::endl;
         bra.LoadBasis(hilbert_space_definition_file_name, idiag, ndiag);

         cout << "Reading basis from '"
              << MakeBasisName(hilbert_space_definition_file_name, jdiag, ndiag) << "'."
              << std::endl;
         ket.LoadBasis(hilbert_space_definition_file_name, jdiag, ndiag);
      } else {
         proton_neutron::ModelSpace ncsmModelSpace;
         ncsmModelSpace.Load(hilbert_space_definition_file_name);

         ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
         bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);
      }
   } else {
      if (!modelSpaceProvided)  // ==> we need to load basis states from files
      {
         if (my_rank == 0) {
            cout << "Reading basis from " << ndiag << " input files, starting with '"
                 << MakeBasisName(hilbert_space_definition_file_name, 0, ndiag) << "'."
                 << std::endl;
         }

         MPI_Comm ROW_COMM, COL_COMM;
         //	All processes with the same value of idiag are grouped and within
         //	each group ordered based on jdiag value
         MPI_Comm_split(mpi_comm, idiag, my_rank, &ROW_COMM);
         //	All processes with the same value of jdiag are grouped and within
         //	each group ordered based on idiag value
         MPI_Comm_split(mpi_comm, jdiag, my_rank, &COL_COMM);

         std::string bra_buffer, ket_buffer;
         int bra_buffer_length, ket_buffer_length;

         if (idiag == jdiag)  // I am diagonal process
         {
            assert(idiag == my_rank && my_rank < mpi_comm.size());

            //	load basis states from file that contains an idiag segment of a
            //	basis split into ndiag segments
            ket.LoadBasis(hilbert_space_definition_file_name, jdiag, ndiag);
            //	create a binary archive from basis
            std::ostringstream ss;
            boost::archive::binary_oarchive oa(ss);
            oa << ket;
            //	copy memory content of a binary archive into a buffer
            ket_buffer = ss.str();
            ket_buffer_length = ket_buffer.size();
            //	diagonal processes have bra and ket basis equal to each other
            bra = ket;
            bra_buffer = ket_buffer;
            bra_buffer_length = ket_buffer_length;

            //				cout << "Size of serialized class: " <<
            // sizeof(char)*bra_buffer_length/(1024.0*1024.0) << " MB." << endl;
         }
         // broadcast length of buffers ...
         // process with my_rank = idiag is a root for each group of row processes
         // process with my_rank = jdiag is a root for each group of column processes
         MPI_Bcast((void*)&bra_buffer_length, 1, MPI_INT, 0, ROW_COMM);
         MPI_Bcast((void*)&ket_buffer_length, 1, MPI_INT, 0, COL_COMM);

         if (idiag != jdiag)  // I am not a diagonal process ==> need to allocate buffer for bra and
                              // ket basis
         {
            bra_buffer.resize(bra_buffer_length);
            ket_buffer.resize(ket_buffer_length);
         }

         MPI_Bcast((void*)bra_buffer.data(), bra_buffer_length, MPI_CHAR, 0, ROW_COMM);
         MPI_Bcast((void*)ket_buffer.data(), ket_buffer_length, MPI_CHAR, 0, COL_COMM);

         if (idiag !=
             jdiag)  // If not diagonal process => create bra and ket basis from buffers received
         {
            std::istringstream bra_ss(bra_buffer);
            boost::archive::binary_iarchive bra_oa(bra_ss);
            bra_oa >> bra;

            std::istringstream ket_ss(ket_buffer);
            boost::archive::binary_iarchive ket_oa(ket_ss);
            ket_oa >> ket;
         }
      } else {
//       proton_neutron::ModelSpace ncsmModelSpace;
         std::string model_space_buffer;
         int model_space_buffer_length;
         if (my_rank == 0) {
            ncsmModelSpace.Load(hilbert_space_definition_file_name);

            std::ostringstream ss;
            boost::archive::binary_oarchive oa(ss);
            oa << ncsmModelSpace;
            //	copy memory content of a binary archive into a buffer
            model_space_buffer = ss.str();
            model_space_buffer_length = model_space_buffer.size();
         }

         MPI_Bcast((void*)&model_space_buffer_length, 1, MPI_INT, 0, mpi_comm);
         if (my_rank != 0) {
            model_space_buffer.resize(model_space_buffer_length);
         }
         MPI_Bcast((void*)model_space_buffer.data(), model_space_buffer_length, MPI_CHAR, 0,
                   mpi_comm);
         if (my_rank != 0) {
            std::istringstream model_space_ss(model_space_buffer);
            boost::archive::binary_iarchive model_space_oa(model_space_ss);
            model_space_oa >> ncsmModelSpace;
         }

         ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
         bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);
      }
   }

   dim = ket.getModelSpaceDim();

   if (my_rank == 0) {
      duration = std::chrono::system_clock::now() - start;
      cout << "Time to construct basis: ... " << duration.count() << endl;
   }

   unsigned long firstStateId_bra = bra.getFirstStateId();
   unsigned long firstStateId_ket = ket.getFirstStateId();

   //	stringstream interaction_log_file_name;
   //	interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
   //	ofstream interaction_log_file(interaction_log_file_name.str().c_str());
   ofstream interaction_log_file("/dev/null");

   CBaseSU3Irreps baseSU3Irreps(bra.NProtons(), bra.NNeutrons(), bra.Nmax());

   //	since root process will read rme files, it is save to let this
   //	process to create missing rme files (for PPNN interaction) if they do not exist
   //	=> true
   bool log_is_on = false;
   CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);

   //	PN interaction is read after PPNN, and hence all rmes should be already in memory.
   //	if rme file does not exist, then false
   bool generate_missing_rme = false;
   CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on,
                                interaction_log_file);

   // if rmes do not exist then they will be computed and we will 
   // need SU(3)>SU(2)xU(1) coefficients
   su3::init();
   try {
      start = std::chrono::system_clock::now();
      CRunParameters run_params;
      run_params.LoadHamiltonian(my_rank, hamiltonian_file_name, interactionPPNN, interactionPN,
                                 bra.NProtons() + bra.NNeutrons());
   } catch (const std::logic_error& e) {
      std::cerr << e.what() << std::endl;
      mpi_comm.abort(-1);
   }
   su3::finalize();

   if (my_rank == 0) {
      duration = std::chrono::system_clock::now() - start;
      cout << "Process 0: LoadInteractionTerms took " << duration.count() << endl;
      if (simulate) {
         //       interactionPPNN.ShowRMETables();
         cout << "Size of memory: "
              << interactionPPNN.GetRmeLookUpTableMemoryRequirements() / (1024.0 * 1024.0) << " MB."
              << endl;
      }

      // Check minimal and maximal rme values. If they are too large then
      // do not proceed.
      float min, max;
      interactionPPNN.min_max_values(min, max);
      std::cout << "minimal rme value read from tables: " << min << std::endl;
      std::cout << "maximal rme value read from tables: " << max << std::endl;
      if (fabs(min) > 100 || fabs(max) > 100) {
         std::cerr << "Error: magnitude of minimal/maximal rme values are suspitious!" << std::endl;
         std::cerr << "Data stored in rme tables may be corrupted." << std::endl;
         mpi_comm.abort(-1);
      }
   }

   // matrix data
   uintmax_t local_nnz(0);

   try {
      vbc.irow = firstStateId_bra;
      vbc.icol = firstStateId_ket;
      vbc.nrows = bra.dim();
      vbc.ncols = ket.dim();

      //		The order of coefficients is given as follows:
      //  	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
      //		TransformTensorStrengthsIntoPP_NN_structure turns that into:
      //  	index = type*k0max*rho0max + k0*rho0max + rho0
      interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

      if (my_rank == 0)
         cout << "Process " << my_rank << " starts calculation of me..." << endl;

      std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

#ifdef TIME_LOCK
      for (int i = 0; i < 1024; ++i)
         openmp_locking::locktime[i] = std::chrono::duration<double>::zero();
#endif
#pragma omp parallel reduction(+ : local_nnz)
      {
         su3::init_thread();

#ifdef SU3_9LM_HASHINDEXEDARRAY
         CWig9lmLookUpTable<float>::initialize();
#endif

         local_nnz = CalculateME_Scalar_UpperTriang_OpenMP(interactionPPNN, interactionPN, bra, ket,
                                                           idiag, jdiag, vbc);

#ifdef SU3_9LM_HASHINDEXEDARRAY
         CWig9lmLookUpTable<float>::finalize();
#endif
// Threads synchronize here to make sure no one needs su3 and su2 libraries anymore
#pragma omp barrier
         su3::finalize_thread();
      }
#ifdef TIME_LOCK
      std::cout << "reporting locking time" << std::endl;
      for (int i = 0; i < 1024; ++i)
         std::cout << "Thread " << i << " : " << openmp_locking::locktime[i] << std::endl;
#endif

      std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
      cout << "Process " << my_rank << " (idiag=" << idiag << ", jdiag=" << jdiag << ") resulting time: " << duration.count() << endl;
   } catch (const std::exception& e) {
      cerr << e.what() << std::endl;
      mpi_comm.abort(-1);
   }
   mpi_comm.barrier();

   if (my_rank == 0)
      std::cout << std::endl;

   // nstates_sofar is needed by MFDn eigensolver
   if (my_rank < ndiag)  // ==> diagonal process ... i.e. bra = ket
   {
      nstates_sofar = bra.getFirstStateId(my_rank);
   }

   /*
   // TODO - REMOVE THE FOLLOWING PRINT OUTS :)
       cout << "Process " << my_rank << ": "
            << "nblocks: " << vbc.rowind.size()
            << ", nrows: " << vbc.nrows
            << ", ncols: " << vbc.ncols
            << ", row: " << vbc.irow
            << ", col: " << vbc.icol
            << ", nnz: " << vbc.nnz
            << ", rowind: " << vbc.rowind.size()
            << ", colind: " << vbc.colind.size()
            << ", rownnz: " << vbc.rownnz.size()
            << ", colnnz: " << vbc.colnnz.size()
            << ", vals: " << vbc.vals.size();
       if (vbc.rowind.size() > 0)
           cout << ", rowind[0]: " << vbc.rowind[0];
       if (vbc.colind.size() > 0)
           cout << ", colind[0]: " << vbc.colind[0];
       if (vbc.rownnz.size() > 0)
           cout << ", rownnz[0]: " << vbc.rownnz[0];
       if (vbc.colnnz.size() > 0)
           cout << ", colnnz[0]: " << vbc.colnnz[0];
       if (vbc.vals.size() > 0)
           cout << ", vals[0]: " << vbc.vals[0];
       cout << endl;
   */
   return local_nnz;
}

// BEGIN: COPIED FROM programs/downstreas/ObdSU3Calculator_MPI_OpenMP.cpp

bool ReadWfnSection(const string& wfn_file_name, const lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, const int ndiag, const int isection, vector<float>& wfn)
{
   //	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
   assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

   fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
   if (!wfn_file)
   {
      cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
      return false;
   }

   size_t size = wfn_file.tellg();
   size_t nelems = size/sizeof(float);

   if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
   {
      cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
      cout << "The actual size of the file: " << size << " bytes!";
      return false;
   }

   // 	this is actually more then we really need ...
   wfn.reserve(nelems); 
   float* wfn_full;
   wfn_full = new float[nelems];
   wfn_file.seekg (0, std::ios::beg);

   //	Load wave function generated for ndiag_wfn into a file that contains order
   //	of basis states for ndiag=1	
   uint32_t ipos;
   uint16_t number_of_states_in_block;
   uint32_t number_ipin_blocks = basis.NumberOfBlocks();
   for (uint16_t i = 0; i < ndiag_wfn; ++i)
   {

      //		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
      //		
      //	section --> {iblock0, iblock1, ...... }
      //		----------------------------------
      //		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
      //		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
      //		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
      //		.
      //		.
      //		.
      for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
      {
         //	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
         ipos = basis.BlockPositionInSegment(ipin_block);
         number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);

         wfn_file.read((char*)&wfn_full[ipos], number_of_states_in_block*sizeof(float));
      }
   }

   //	This loop iterates over blocks that belong to isection of basis split into ndiag segments
   //	I am using the fact that for isection of the basis split into ndiag segment has the
   //	following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
   for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag)
   {
      //	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
      ipos = basis.BlockPositionInSegment(ipin_block);
      number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
      wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
   }

   //	trim excess memory
   vector<float>(wfn).swap(wfn);

   delete []wfn_full;

   return true;
}

int ReadWfns(
      const vector<string> wfns,
      const lsu3::CncsmSU3xSU2Basis& full_basis,
      int ndiag_wfn,
      int ndiag,
      int isegment,
      vector<vector<float> >& wfns_bra)
{
	bool ok;
	for (int i = 0; i < wfns.size(); ++i)
	{
		// Note that each wave function in wfns_bra contains only (my_row)th section of bra model space that is made out of ndiag_bra sections 
		ok = ReadWfnSection(wfns[i], full_basis, ndiag_wfn, ndiag, isegment, wfns_bra[i]);
		if (!ok)
		{
			return false;
		}
	}
	return ok;
}


// END: COPIED FROM programs/downstreas/ObdSU3Calculator_MPI_OpenMP.cpp

int main(int argc, char** argv) {
   boost::mpi::environment env(argc, argv);

   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> duration;

   int my_rank, nprocs, idiag, jdiag;
   unsigned int ndiag;
   bool simul =
       false;  // if simul then a the process simulate what happens on a particuliar mpi_rank

   boost::mpi::communicator mpi_comm_world;
   my_rank = mpi_comm_world.rank();
   nprocs = mpi_comm_world.size();

   if (su3shell_data_directory == NULL) {
      if (my_rank == 0) {
         cerr << "System variable 'SU3SHELL_DATA' was not defined!" << endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   if (argc != 5) {
      if (my_rank == 0) {
         cerr << "Usage: " << argv[0]
              << " <[model space]/[precalculated basis] file name> <Hamiltonian file name> <#ndiag "
                 "wfns>  <#wfns>"
              << endl;
         cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can "
                 "set the "
                 "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
              << endl;
         cerr << "It is highly advisable to set environment variable NNZ to preallocates storage "
                 "for non vanishing matrix elements."
              << endl;
         cerr << "By setting environment variables NDIAG, IDIAG and JDIAG, one can execute only "
                 "one block of an hypothetical parallel execution."
              << endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   int ndiag_wfns = atoi(argv[3]);
   if (my_rank == 0)
      std::cout << "NDIAG used for wfns calculation: " << ndiag_wfns << std::endl;

   std::string wfns_file(argv[4]);
   if (my_rank == 0)
      std::cout << "File with wfns list: " << wfns_file << std::endl;

   long nnz_reserve = 1L << 20;  // default reserved space for 1M elements
   if (getenv("NNZ"))
      nnz_reserve = atol(getenv("NNZ"));
   else {
      if (my_rank == 0) {
         std::cout << "By setting environment variable NNZ one can preallocate array for "
                      "non-vanishing matrix elements. HIGHLY RECOMMENDED!"
                   << std::endl;
      }
   }
   if (my_rank == 0) {
      std::cout << "Reserved number of non-vanishing matrix elements: " << nnz_reserve << std::endl;
   }

   if (getenv("NDIAG") || getenv("IDIAG") || getenv("JDIAG")) {
      simul = true;
      if (nprocs != 1) {
         if (my_rank == 0) {
            cerr << "When simulating, use a single MPI rank." << endl;
         }
         mpi_comm_world.abort(EXIT_FAILURE);
      }
      if (getenv("NDIAG") == NULL || getenv("IDIAG") == NULL || getenv("JDIAG") == NULL) {
         cerr << "When simulating, all the following environment variables must be defined: NDIAG, "
                 "IDIAG, JDIAG."
              << endl;
         return EXIT_FAILURE;
      }
      ndiag = atoi(getenv("NDIAG"));
      if (ndiag % 2 == 0) {
         cerr << "ndiag:" << ndiag << " ... parameter ndiag must be an odd number!" << endl;
         return EXIT_FAILURE;
      }
      idiag = atoi(getenv("IDIAG"));
      jdiag = atoi(getenv("JDIAG"));
      if ((idiag >= ndiag) || (jdiag >= ndiag))
      {
         cerr << "ERROR: the value of IDIAG:" << idiag << " or JDIAG:" << jdiag
              << " is larger then or equal to NDIAG:" << ndiag << "!" << endl;
         return EXIT_FAILURE;
      }
   } else {
      ndiag = (-1 + sqrt(1 + 8 * nprocs)) / 2;
      if (fabs(ndiag - (double)(-1.0 + sqrt(1 + 8 * nprocs)) / 2.0) > 1.0e-7) {
         if (my_rank == 0) {
            cerr << "number of processes = " << nprocs
                 << " is wrong: must be qual to ndiag*(ndiga+1)/2, where ndiag is number of "
                    "diagonal processes."
                 << endl;
         }
         mpi_comm_world.abort(EXIT_FAILURE);
      }
      MapRankToColRow_MFDn_Compatible(ndiag, my_rank, idiag, jdiag);
   }

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

   std::string hilbert_space_definition_file_name(argv[1]);
   std::string hamiltonian_file_name(argv[2]);

   lsu3::VBC_Matrix vbc;
   vbc.vals.reserve(nnz_reserve);
   //	these two variable are needed for MFDn
   int vectors = true;
   long int nstates_sofar(0);
   uintmax_t local_nnz;
   uint64_t dim;

   proton_neutron::ModelSpace ncsmModelSpace;

   local_nnz = CalculateME(hilbert_space_definition_file_name, hamiltonian_file_name, ndiag, idiag,
                           jdiag, vbc, nstates_sofar, dim, simul,
                           ncsmModelSpace);

   //	Sometimes it may be useful to store have matrix market compatible matrix on disk ...
   //	vbc.save_matrix_market("vbc_dummy.mtx");

   if (simul) {
      cout << "#nnz:" << local_nnz << endl;
      cout << "required NNZ:" << vbc.vals.size() << endl;
      cout << "Size of matrix in memory:" << vbc.memory_usage() / (1024.0 * 1024.0) << " MB."
           << endl;

      size_t frequent_u9_size;
      size_t u9_size;
      size_t u6_size;
      size_t z6_size;
      CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);
      cout << "Size of frequent U9: " << frequent_u9_size / (1024.0 * 1024.0) << " MB." << endl;
      cout << "Size of U9: " << u9_size / (1024.0 * 1024.0) << " MB." << endl;
      cout << "Size of U6: " << u6_size / (1024.0 * 1024.0) << " MB." << endl;
      cout << "Size of Z6: " << z6_size / (1024.0 * 1024.0) << " MB." << endl;

      size_t total = vbc.memory_usage() + frequent_u9_size + u9_size + u6_size + z6_size;
      cout << "Total size of memory:" << total / (1024.0 * 1024.0) << " MB." << endl;

      CWigEckSU3SO3CGTablesLookUpContainer::ShowInfo();  // clear memory allocated for SU(3)>SO(3)
                                                         // coefficients
      CWig9lmLookUpTable<RME::DOUBLE>::ShowInfo();
      CWig9lmLookUpTable<RME::DOUBLE>::ShowMemoryUsage();
   } else {
      uintmax_t local_vbc_size = vbc.memory_usage();
      uintmax_t total_vbc_size, maxlocmatsize;
      uintmax_t local_vbc_nnz = vbc.vals.size();
      uintmax_t total_vbc_nnz, total_nnz;
      uintmax_t max_vbc_nnz = 0;
      boost::mpi::reduce(mpi_comm_world, local_vbc_size, total_vbc_size, std::plus<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_vbc_size, maxlocmatsize,
                         boost::mpi::maximum<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_nnz, total_nnz, std::plus<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_vbc_nnz, total_vbc_nnz, std::plus<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_vbc_nnz, max_vbc_nnz,
                         boost::mpi::maximum<uintmax_t>(), 0);

      size_t num_frequent_u9, frequent_u9_size, max_num_frequent_u9;
      size_t num_u9, u9_size, max_num_u9;
      size_t num_u6, u6_size, max_num_u6;
      size_t num_z6, z6_size, max_num_z6;
      CWig9lmLookUpTable<RME::DOUBLE>::coeffs_info(num_frequent_u9, num_u9, num_u6, num_z6);
      boost::mpi::reduce(mpi_comm_world, num_frequent_u9, max_num_frequent_u9,
                         boost::mpi::maximum<size_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, num_u9, max_num_u9, boost::mpi::maximum<size_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, num_u6, max_num_u6, boost::mpi::maximum<size_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, num_z6, max_num_z6, boost::mpi::maximum<size_t>(), 0);

      CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);

      size_t memory_usage_coeffs = frequent_u9_size + u9_size + u6_size + z6_size;
      size_t max_memory_usage_coeffs;
      boost::mpi::reduce(mpi_comm_world, memory_usage_coeffs, max_memory_usage_coeffs,
                         boost::mpi::maximum<size_t>(), 0);

      if (my_rank == 0) {
         // model space dimension: getModelSpaceDim()
         size_t total_csr_size =
             total_nnz * sizeof(float) + total_nnz * sizeof(int) + dim * sizeof(int);

         std::cout << "Total size of Hamiltonian matrix in VBC format: "
                   << total_vbc_size / (1024.0 * 1024.0) << " MB." << std::endl;
         std::cout << "Total size of Hamiltonian matrix in CRS format: "
                   << total_csr_size / (1024.0 * 1024.0) << " MB." << std::endl;
         std::cout << "Maximum local matrix block size: " << maxlocmatsize / (1024.0 * 1024.0)
                   << " MB." << std::endl;
         std::cout << "Number of matrix elements stored in vbc:" << total_vbc_nnz << std::endl;
         std::cout << "Required value of NNZ: " << max_vbc_nnz << std::endl;
         std::cout << "Number of non vanishing matrix elements:" << total_nnz << std::endl;
         std::cout << "\n\nmax number of u9: " << max_num_u9 << std::endl;
         std::cout << "max number of u6: " << max_num_u6 << std::endl;
         std::cout << "max number of z6: " << max_num_z6 << std::endl;
         std::cout << "max number of frequent u9: " << max_num_frequent_u9 << std::endl;
         std::cout << "max amount of memory for SU(3) coeffs: "
                   << max_memory_usage_coeffs / (1024.0 * 1024.0) << " MB.\n\n";
      }
   }

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();  // clears memory allocated for U9, U6, and Z6
                                                      // coefficients
   CSSTensorRMELookUpTablesContainer::ReleaseMemory();  // clear memory allocated for single-shell
                                                        // SU(3) rmes
   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory allocated for SU(3)>SO(3)
                                                           // coefficients

   if (simul) {
      cout << "Simulation is done" << endl;
      return 0;
   }

   mpi_comm_world.barrier();

   // load wfns:

   std::vector<std::vector<float>> wfns;

   if (my_rank < ndiag)
   {
      std::ifstream wfns_stream(wfns_file);

      std::string wfn_file;
      std::vector<std::string> wfns_string;

      while (wfns_stream >> wfn_file)
      {
         if (my_rank == 0)
            std::cerr << "Loading wfn from file: " << wfn_file << std::endl;

         wfns_string.push_back(std::move(wfn_file));
      }

      wfns.resize(wfns_string.size());

      // full basis required by ReadWfns():
      lsu3::CncsmSU3xSU2Basis full_basis;
      full_basis.ConstructBasis(ncsmModelSpace, 0, 1);

      auto ok = ReadWfns(wfns_string, full_basis, ndiag_wfns, ndiag, idiag, wfns);
      if (!ok)
      {
         std::cerr << "Error returned by ReadWfns() function" << std::endl;
         mpi_comm_world.abort(EXIT_FAILURE);
      }
   }

   // OMPILancz vector-matrix-vector multiplication:

   using F = double; // VMV precision

   ompilancz::vmv<F> vmv(idiag, jdiag, ndiag, vbc.nrows, vbc.ncols);

   if (my_rank < ndiag)
   {
      for (int i = 0; i < wfns.size(); i++)
      {
         ompilancz::span<float> span(wfns[i].data(), wfns[i].size());
         vmv.add_vector(span);
      }
   }

   lsu3::VBC_single_vector_matvec<F> single_vector_matvec(vbc, idiag, jdiag, vbc.nrows, vbc.ncols);
   vmv.multiply(single_vector_matvec);

   if (my_rank < ndiag)
   {
      for (int i = 0; i < wfns.size(); i++)
      {
         for (int j = 0; j < wfns.size(); j++)
         {
            auto res = vmv.result(i, j);
            if (my_rank == 0)
               std::cout
                  << std::right << std::setw(8) << std::setprecision(4) << std::fixed
                  << res << " ";
         }
         if (my_rank == 0)
            std::cout << std::endl;
      }
   }

   return EXIT_SUCCESS;
}
