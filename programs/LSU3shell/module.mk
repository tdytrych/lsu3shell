$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
# module_units_f := 
#module_programs_cpp := LSU3shell_MPI LSU3shell_MPI_OpenMP LSU3shell_analytics LSU3shell_MPI_OpenMP_OMPILancz LSU3shell_MPI_OpenMP_IT 
module_programs_cpp := LSU3shell_MPI_OpenMP LSU3shell_analytics LSU3shell_MPI_OpenMP_VMV LSU3shell_MPI_OpenMP_LO

# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))
