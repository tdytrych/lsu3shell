#include <boost/mpi.hpp>

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <mascot/Props.h>
#include <mascot/MatrixWriter.h>

#include <chrono>
#include <cmath>
#include <stack>
#include <stdexcept>
#include <vector>

using namespace std;

using mascot::caughtAt;
using mascot::DataType;
using mascot::EntryType;
using mascot::MatrixWriter;
using mascot::Part;
using mascot::Props;
using mascot::Symmetry;

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col)
{
	assert(ndiag * (ndiag + 1) / 2 >= my_rank); 
/*
	if (ndiag % 2 == 0)
	{
		if (my_rank == 0)
		{
			cout << "number of diagonal processes = " << ndiag << " must be an odd number." << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}
*/
	int executing_process_id(0);
	for (size_t i = 0; i < ndiag; ++i)
	{
		row = 0;
		for (col = i; col < ndiag; ++col, ++row, ++executing_process_id)
		{
			if (my_rank == executing_process_id)
			{
				return;
			}
		}
	}
}

unsigned long CalculateME(	
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					const unsigned long firstStateId_I, const unsigned int idiag, 
					const unsigned long firstStateId_J, const unsigned int jdiag,
					MatrixWriter& fresults)
{
	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);

    SingleDistributionSmallVector distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_SMALL_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;

	unsigned long number_nonzero_me(0);
	
	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	uint32_t blockFirstRow(firstStateId_I);
	
	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n;

	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		if (bra.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);

		unsigned long blockFirstColumn((idiag == jdiag) ? blockFirstRow : firstStateId_J);

		vector<vector<float> > vals_local(bra.NumberOfStatesInBlock(ipin_block));
		vector<vector<size_t> > col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());
//	loop over (jp, jn) pairs
		for (unsigned int jpjn_block = (idiag == jdiag) ? ipin_block : 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						if (!selected_tensors_p_pn.empty())
						{
							std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_p_pn.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						if (!selected_tensors_n_pn.empty())
						{
							std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_n_pn.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}

			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			uint32_t currentRow = blockFirstRow;
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				bool isDiagonalBlock = (idiag == jdiag && ipin_block == jpjn_block); 
				uint32_t currentColumn = (isDiagonalBlock) ? currentRow : blockFirstColumn;
				uint32_t jbegin = (isDiagonalBlock) ? iwpn : ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						Reset_rmeCoeffs(rmeCoeffsPNPN);
						if (in == jn && !rmeCoeffsPP.empty())
						{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
							CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
						}

						if (ip == jp  && !rmeCoeffsNN.empty())
						{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
							CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
						}

						if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
						{
							CalculatePNInteractionMeData(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
						}

						if (!rmeCoeffsPNPN.empty())
						{
							if (isDiagonalBlock && iwpn == jwpn)
							{
								assert(idiag == jdiag);
								CalculateME_Diagonal_UpperTriang_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
							else
							{
								CalculateME_nonDiagonal_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
						}
					}
					currentColumn += aimax*ket.omega_pn_dim(jwpn);
				}
				currentRow += afmax*bra.omega_pn_dim(iwpn);
			}
			blockFirstColumn += ket.NumberOfStatesInBlock(jpjn_block);
			last_jp = jp;
			last_jn = jn;
		}
		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			size_t irow = blockFirstRow + i;
			for (size_t j = 0; j < vals_local[i].size(); ++j, ++number_nonzero_me)
			{
				fresults.write(irow, col_ind_local[i][j], vals_local[i][j]);
// The following can be used to generate matrix in matrix market format.				
//				cout << (irow+1) << " " << (col_ind_local[i][j]+1) << " " << vals_local[i][j] << endl;
			}
		}
		blockFirstRow += bra.NumberOfStatesInBlock(ipin_block);
	}
	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;

	return number_nonzero_me;
}

int main(int argc,char **argv)
{
 	boost::mpi::environment env(argc, argv);

	std::chrono::system_clock::time_point start;
	std::chrono::duration<double> duration;

	int my_rank, nprocs, idiag, jdiag;
	unsigned int ndiag;

 	boost::mpi::communicator mpi_comm_world;
	my_rank = mpi_comm_world.rank();
	nprocs  = mpi_comm_world.size(); 

	if (su3shell_data_directory == NULL)
	{
		if (my_rank == 0)
		{
			cout << "System variable 'SU3SHELL_DATA' was not defined!" << endl;
		}
		mpi_comm_world.abort(EXIT_FAILURE);
	}

	if (nprocs > 1 && argc != 3)
	{
		if (my_rank == 0)
		{
	  		cout << "Usage: "<< argv[0] <<" <file name with run parameters> <output directory>" << endl;
		}
		mpi_comm_world.abort(EXIT_FAILURE);
	}
	else if (nprocs == 1 && (argc != 6 && argc != 3))
	{
		if (my_rank == 0)
		{
			cout << "nprocs = " << nprocs << " argc = " << argc << endl;
			cout << "Usage: "<< argv[0] <<" <file name with run parameters> <output directory> [<ndiag> <iblock> <jblock>]" << endl;
		}
		mpi_comm_world.abort(EXIT_FAILURE);
	}

	if (argc == 6)
	{
		assert(nprocs == 1);
		ndiag = atoi(argv[3]);
		idiag = atoi(argv[4]);
		jdiag = atoi(argv[5]); 
	}
	else
	{
		assert(argc == 3);
		ndiag = (-1 + sqrt(1 + 8*nprocs))/2; 
		if (fabs(ndiag - (double)(-1.0 + sqrt(1 + 8*nprocs))/2.0) > 1.0e-7)
		{
			if (my_rank == 0)
			{
				cout << "number of processes = " << nprocs << " is wrong: must be qual to ndiag*(ndiga+1)/2, where ndiag is number of diagonal processes." << endl;
			}
			mpi_comm_world.abort(EXIT_FAILURE);
		}
	//	We must map processes according to MFDn scheme
		MapRankToColRow_MFDn_Compatible(ndiag, my_rank, idiag, jdiag);
	}

	CRunParameters run_params;

	if (my_rank == 0)
	{
		try
		{
			run_params.LoadRunParameters(argv[1]);
		}
		catch (const std::logic_error& e) 
		{ 
		   std::cerr << e.what() << std::endl;
		   mpi_comm_world.abort(-1);
	    }
	}

	boost::mpi::broadcast(mpi_comm_world, run_params, 0);

	start = std::chrono::system_clock::now();
	lsu3::CncsmSU3xSU2Basis ket(run_params.GetModelSpace(), jdiag, ndiag);	
	lsu3::CncsmSU3xSU2Basis bra(run_params.GetModelSpace(), ket, idiag, ndiag);	

	mpi_comm_world.barrier();

	if (my_rank == 0)
	{
		duration = std::chrono::system_clock::now() - start;
		cout << "Time to construct basis: ... " << duration.count() << endl;
	}

	unsigned long firstStateId_bra = bra.getFirstStateId(); 
	unsigned long firstStateId_ket = ket.getFirstStateId();

//	stringstream interaction_log_file_name;
//	interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
//	ofstream interaction_log_file(interaction_log_file_name.str().c_str());
	ofstream interaction_log_file("/dev/null");

	CBaseSU3Irreps baseSU3Irreps(run_params.Z(), run_params.N(), run_params.Nmax()); 

//	since root process will read rme files, it is save to let this
//	process to create missing rme files (for PPNN interaction) if they do not exist 
//	=> true
	bool log_is_on = false;
	CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);
//	PN interaction is read after PPNN, and hence all rmes should be already in memory.
//	if rme file does not exist, then false
	bool generate_missing_rme = false;	
	CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on, interaction_log_file);

	try 
	{

		start = std::chrono::system_clock::now();
		run_params.LoadInteractionTerms(my_rank, interactionPPNN, interactionPN);

	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
	   mpi_comm_world.abort(-1);
    } 
	
	if (my_rank == 0) 
	{ 
		duration = std::chrono::system_clock::now() - start; 
		cout << "Process 0: LoadInteractionTerms took " << duration.count() << endl;
	}

    try
	{
		Props p; // global and local properties of submatrix with idiag row and jdiag col coordinates
        p.globalNRows.setSize(bra.getModelSpaceDim()); //	total number of rows 
        p.localNRows.setSize(bra.dim());	// number of local rows

		p.globalNColumns.setSize(ket.getModelSpaceDim()); //  total number of columns
 		p.localNColumns.setSize(ket.dim());	// number of local columns

///* May be needed for SLEPc
        p.rowOffset    = firstStateId_bra;
        p.columnOffset = firstStateId_ket;
//*/

// MFDn takes relative indices of rows and columns
//	    p.rowOffset    = 0;
//   	p.columnOffset = 0;

       	p.localNNonzeros.setDataType(DataType::UINT32); // type for indexing non zero matrix elements
        p.entryType = EntryType::REAL;
//		p.dataType  = DataType::FLOAT;
		p.dataTypeId  = DataType::FLOAT;

      	p.symmetry = Symmetry::SYMMETRIC;
     	p.part = Part::UPPER_TRIANGULAR;

       	MatrixWriter hdf5_writer;
//		hdf5_writer.create(argv[2], p, MPI_COMM_WORLD);
		hdf5_writer.create(argv[2], p, mpi_comm_world, 4096, 4096, 1024);

//		The order of coefficients is given as follows:
//  	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//		TransformTensorStrengthsIntoPP_NN_structure turns that into:
//  	index = type*k0max*rho0max + k0*rho0max + rho0
		interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

		
		cout << "Process " << my_rank << " starts calculation of me" << endl;
		std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

		unsigned long nme = CalculateME(interactionPPNN, interactionPN, bra, ket, firstStateId_bra, idiag, firstStateId_ket, jdiag, hdf5_writer);

		std::chrono::duration<double> duration = std::chrono::system_clock::now() - start;
		cout << "Resulting time: " << duration.count() << endl;
		if (nprocs == 1)
		{
			cout << "#nme = " << nme << endl;
		}
	}
   	catch (const std::exception& e)
	{
       		cerr << e.what() << caughtAt(__FILE__, __LINE__);
       		mpi_comm_world.abort(-1);
    }
}
