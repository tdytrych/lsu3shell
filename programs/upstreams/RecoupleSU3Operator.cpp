#include <iostream>
#include <string>

#include <SU3ME/SU3InteractionRecoupler.h>

#include <su3.h>

int main(int argc, char* argv[]) {
   // tensor strenghts do not depend on multiplicity k
   // as a result SU3InteractionRecoupler will work always with kmax = 1
   // which effectively means that strenghts will depend only on rho0
   k_dependent_tensor_strenghts = false;

   if (argc != 3) {
      std::cout << "Usage: " << argv[0] << " <input filename> <output filename prefix>" << std::endl;

      std::cout << "This program takes as an input list of SU(3) tensors operators in form" << std::endl;
      std::cout << "n1 n2 n3 n4 IRf Sf IRi Si IR0 S0" << std::endl;
      std::cout << "{a_{pp} 	a_{nn} 		a_{pn}}_{rho0=0}" << std::endl;
      std::cout << "{a_{pp}	a_{nn}		a_{pn}}_{rho0=1}" << std::endl;
      std::cout << "				. " << std::endl;
      std::cout << " 				. " << std::endl;
      std::cout << "				. " << std::endl;
      std::cout << "{a_{pp}	a_{nn}		a_{pn}}_{rho0=rho0max}" << std::endl;
      std::cout << "and represents the following tensor:" << std::endl;
      std::cout << "[ {a+_n1 x a+_n2}^{IRf}Sf x {a_n3 x a_n4}^{IRi}Si ]^{IR0} S0" << std::endl << std::endl;

      std::cout << "Name of the file with SU3 tensors is required as an input argument." << std::endl;
      return 0;
   }

   SU3InteractionRecoupler Recoupler;
   std::string sInputFileName(argv[1]);
   std::string sOutputFileName(argv[2]);
   std::ifstream inter_file(sInputFileName.c_str());

   if (!inter_file) {
      std::cerr << "Could not open the file '" << sInputFileName << "'" << std::endl;
      return 0;
   }
   std::cout << "reading tensors from the input file '" << sInputFileName << "'" << std::endl;

   su3::init();
   while (true) {
      if (inter_file.eof()) {
         break;
      }

      SU3::LABELS IRf, IRi, Irrep0;
      int nCoeffs;
      int Sf, Si, S0;
      int n1, n2, n3, n4;
      int rho0max, rho0;

      inter_file >> n1;
      inter_file >> n2;
      inter_file >> n3;
      inter_file >> n4;

      inter_file >> IRf;
      inter_file >> Sf;
      inter_file >> IRi;
      inter_file >> Si;
      inter_file >> Irrep0;
      inter_file >> S0;

      if (inter_file.eof()) {
         break;
      }

      rho0max = Irrep0.rho;

      if (rho0max != SU3::mult(IRf, IRi, Irrep0))
      {
         std::cerr << "Error: coupling (" << (int)IRf.lm  << " " << (int)IRf.mu << ") x (" << (int)IRi.lm << " " << (int)IRi.mu << ") ---> (";
         std::cerr << (int)Irrep0.lm << " " << (int)Irrep0.mu << ") has rho0max:" << SU3::mult(IRf, IRi, Irrep0) << ", while tensor given in the input file ";
         std::cerr << "'" << sInputFileName << "' has rho0max:" << (int)Irrep0.rho << "! Such tensor is unphysical." << std::endl;

         return EXIT_FAILURE;
      }

      nCoeffs = 3 * rho0max;
      std::vector<double> CoeffsPPNNPN(nCoeffs, 0.0);
      for (int i = 0; i < nCoeffs; ++i) {
         inter_file >> CoeffsPPNNPN[i];
      }

      if (std::count_if(CoeffsPPNNPN.begin(), CoeffsPPNNPN.end(), Negligible) == nCoeffs) {
         std::cerr << "All coefficients of the tensor [" << n1 << " " << n2 << " " << n3 << " " << n4
              << " " << IRf << " Sf=" << Sf << "/2 " << IRi << " Si=" << Si << "/2 " << Irrep0;
         std::cerr << " S0=" << S0 << "/2 ] are equal to zero!" << std::endl;
         exit(EXIT_FAILURE);
      }
      ///////////////////////////////////////////////////////////////////////////////////////////
      std::cout << "Recoupling: " << std::endl;
      std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
      std::cout << "\t" << IRf << " x " << IRi << " -> " << Irrep0;
      std::cout << "\t"
           << "Sf = " << Sf << "/2  Si = " << Si << "/2 S0 = " << S0 << "/2" << std::endl;
      size_t index = 0;
      for (rho0 = 0; rho0 < rho0max; ++rho0, index += 3) {
         std::cout << CoeffsPPNNPN[index + PP] << " " << CoeffsPPNNPN[index + NN] << " "
              << CoeffsPPNNPN[index + PN] << std::endl;
      }
      ///////////////////////////////////////////////////////////////////////////////////////////

      char n1n2n3n4[4];
      n1n2n3n4[0] = n1;
      n1n2n3n4[1] = n2;
      n1n2n3n4[2] = n3;
      n1n2n3n4[3] = n4;

      //	Note that InsertTTY1 uses formulas for recoupling which do not depend on J0
      //	and M0 (see A.35-A.43) and hence these quantum labeks do not need to be specified.
      int bSuccess = Recoupler.Insert_adad_aa_Tensor(n1n2n3n4, SU3xSU2::LABELS(IRf, Sf),
                                                     SU3xSU2::LABELS(IRi, Si),
                                                     SU3xSU2::LABELS(Irrep0, S0), CoeffsPPNNPN);
      if (!bSuccess) {
         std::cout << "IMPLEMENT: ";

         std::cout << "n1 = " << n1 << " n2 = " << n2 << " n3 = " << n3 << " n4 = " << n4;
         std::cout << "\t" << IRf << " x " << IRi << " -> " << Irrep0;
         std::cout << "\t"
              << "Sf = " << Sf << "/2  Si = " << Si << "/2 S0 = " << S0 << "/2";
         size_t index = 0;
         for (rho0 = 0; rho0 < rho0max; ++rho0, index += 3) {
            std::cout << CoeffsPPNNPN[index + PP] << " " << CoeffsPPNNPN[index + NN] << " "
                 << CoeffsPPNNPN[index + PN] << std::endl;
         }
         std::cout << std::endl;
         return 0;
      }
   }
   su3::finalize();

   Recoupler.RemoveTensorsWithAmplitudesLess(
       9.0e-6);  // remove tensors with amplitude lees than 1.0e-5
   Recoupler.PrintInfo();

   std::string sOutputFileNamePPNN(sOutputFileName);
   std::string sOutputFileNamePN(sOutputFileName);
   sOutputFileNamePN += ".PN";
   sOutputFileNamePPNN += ".PPNN";
   Recoupler.Save(sOutputFileNamePPNN, TENSOR::PPNN);
   Recoupler.Save(sOutputFileNamePN, TENSOR::PN);

   Recoupler.Show2();
}
