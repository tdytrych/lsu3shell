#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/OperatorLoader.h>

#include <su3.h>

#include <sys/stat.h>
#include <cstdio>
#include <cstdlib>
#include <stdexcept>

using namespace std;

int main(int argc,char **argv)
{
	if (argc != 2)
	{
		cout << "Name of the file with the model space definition is the required input parameter." << endl;
		return EXIT_FAILURE;
	}

   su3::init();
	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);	

	struct stat results;  
  	if (stat("rme", &results) != 0)
	{
		cout << "directory rme does not exist ==> creating rme .... " << endl;
		system("mkdir rme");
	}
  	if (stat("hws", &results) != 0)
	{
		cout << "directory hws does not exist ==> creating hws .... " << endl;
		system("mkdir hws");
	}

	CncsmSU3Basis  Basis(ncsmModelSpace);
	std::ofstream log_file("/dev/null");
	CInteractionPPNN interactionPPNN(Basis, true, log_file);
	CInteractionPN interactionPN(Basis, true, true, log_file);
	int A = ncsmModelSpace.number_of_protons() + ncsmModelSpace.number_of_neutrons();
	const float hw = 15;
	const float lambda = 50;
	try
	{
		COperatorLoader operatorLoader;

		operatorLoader.AddTrel(A, hw);
		operatorLoader.AddVcoul(hw);
		operatorLoader.AddVnn();
		operatorLoader.AddBdB(A, lambda);
/** J0 and M0 labels of the operator are stored as: MECalculatorData::JJ0_ and  MECalculatorData::MM0_ 
 	and are being read from one-body interaction files ...
 */
		operatorLoader.Load(0, interactionPPNN, interactionPN);
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
	   return EXIT_FAILURE;
    }
	cout << "memory size of all seed rmes stored: " << (double)interactionPPNN.GetRmeLookUpTableMemoryRequirements()/(1024.0*1024.0) << " mb." << endl;
   su3::finalize();
	return 0;
}
