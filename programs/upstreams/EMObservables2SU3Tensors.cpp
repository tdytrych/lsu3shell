#include <SU3ME/global_definitions.h>
#include <SU3ME/bdbme.h>
#include <SU3ME/OneBodyOperator2SU3Tensors.h>	// contains the template class COneBodyOperators2SU3Tensors<COneBodyMatrixElements> that performs decomposition

#include <sstream>
#include <cmath>

namespace electric_moments
{
	template<typename OneBodyMscheme>
	class MatrixElementOneBody
	{
		public:
		typedef typename OneBodyMscheme::MatrixElementValue MatrixElementValue;
		public:
		inline size_t GetNumberOfOperators() const {return 1;}

		inline bool Me(const int na, const int la, const int ja, const int ma, const int nb, const int lb, const int jb, const int mb, const int type, MatrixElementValue& matrix_element) const
		{
			assert(type == PP || type == NN);
			return CalculateOneBodyMschemeMe(type, na, la, ja, ma, nb, lb, jb, mb, matrix_element);
		}

		inline int JJ0() const {return OneBodyMscheme::kJJ0;}
		inline int MM0() const {return OneBodyMscheme::kMM0;}
		private:
		OneBodyMscheme CalculateOneBodyMschemeMe;
	};
}

class E2
{
	public:
	typedef double MatrixElementValue;
	public:
	enum {kJJ0 = 4, kMM0 = 0}; // my code calculates \sum_{...} c_{...} * < Jf || [a+_{a} x ta_{b}]^(l0 m0)S0_{k0 L0 J0} || Ji> which is equal to < Jf ||O^J0|| Ji>

	bool operator()(const int type, const int nf, const int llf, const int jjf, const int mmf, 
					const int ni, const int lli, const int jji, const int mmi, 
					MatrixElementValue& matrix_element) const
	{
		if (abs(nf - ni) > 2 || type == NN)
		{
			matrix_element = 0;
			return false;
		}
		matrix_element = std::sqrt(7.5)*MINUSto((1 + jji + lli)/2)*std::sqrt(jji + 1)*clebschGordan(jji, mmi, kJJ0, kMM0, jjf, mmf)*wigner6j(lli, 1, jji, jjf, kJJ0, llf);

		int nt;
		double dsum = 0.0;
		nt  = nf - 1;	// ==> <nf lf || b+ || nt lt> is non vanishing
		if (nt == (ni + 1) || nt == (ni - 1))
		{
			double dA, dB, dC, dD, dWig6jT;
			for (int llt = 2*nt; llt >=0; llt -= 4)
			{
				dWig6jT = wigner6j(2, 2, kJJ0, lli, llf, llt);

				dA = bd(nf, llf, nt, llt);
				dB = b(nf, llf, nt, llt);

				dC = bd(nt, llt, ni, lli);
				dD = b(nt, llt, ni, lli);
					
				dsum += dWig6jT*(dA+dB)*(dC+dD);
			}
		}

		nt = nf + 1;	// ==> <nf lf || b || nt lt> is non vanishing
		if (nt == (ni + 1) || nt == (ni - 1))
		{
			double dA, dB, dC, dD, dWig6jT;
			for (int llt = 2*nt; llt >= 0; llt -= 4)
			{
				dWig6jT = wigner6j(2, 2, kJJ0, lli, llf, llt);

				dA = bd(nf, llf, nt, llt);
				dB = b (nf, llf, nt, llt);

				dC = bd(nt, llt, ni, lli);
				dD = b (nt, llt, ni, lli);
					
				dsum += dWig6jT*(dA+dB)*(dC+dD);
			}
		}

		matrix_element *= dsum;

		if (Negligible(matrix_element)) 
		{
			return false;
		}
		else
		{	
			return true;
		}
	}
};

class M1
{
	private:
	double CalculateLrme(const int np, const int llp, const int jjp, const int n, const int ll, const int jj) const
	{
		return ((np == n) && (llp == ll)) ? MINUSto((ll + jj + 3)/2)*std::sqrt((ll >> 1)*((ll >> 1) + 1)*(ll + 1))*std::sqrt((jj + 1)*(jjp + 1))*wigner6j(ll, 1, jj, jjp, 2, ll) : 0.0;
	}

	double CalculateSrme(const int np, const int llp, const int jjp, const int n, const int ll, const int jj) const
	{
		static double sqrt3o2 = 1.224744871391589;	// sqrt(1/2*(1/2 + 1)(2*1/2 + 1))
		return ((np == n) && (llp == ll)) ? MINUSto((ll + jjp + 3)/2)*sqrt3o2*std::sqrt((jj + 1)*(jjp + 1))*wigner6j(1, ll, jj, jjp, 2, 1) : 0.0;
	}

	public:
	typedef double MatrixElementValue;
	public:
	enum {kJJ0 = 2, kMM0 = 0}; // my code calculates \sum_{...} c_{...} * < Jf || [a+_{a} x ta_{b}]^(l0 m0)S0_{k0 L0 J0} || Ji> which is equal to < Jf ||O^J0|| Ji>

	bool operator()(const int type, const int nf, const int llf, const int jjf, const int mmf, 
					const int ni, const int lli, const int jji, const int mmi, 
					MatrixElementValue& matrix_element) const
	{
		static const double gp = 5.5857;
		static const double gn = -3.8263;
		static const double sqrt3o4pi = sqrt(3.0/(4.0*M_PI));
		
		if ((nf != ni) || (mmi + kMM0 != mmf) || (jjf < abs(jji - kJJ0) || jjf > jji + kJJ0))
		{
			matrix_element = 0;
			return false;
		}

		double dcoeff = sqrt3o4pi*clebschGordan(jji, mmi, kJJ0, kMM0, jjf, mmf)/std::sqrt(jjf + 1);

		double l1rme = (type == PP) ? CalculateLrme(nf, llf, jjf, ni, lli, jji) : 0.0;
		double s1rme = CalculateSrme(nf, llf, jjf, ni, lli, jji);

		if (type == PP)
		{
			matrix_element = dcoeff*(l1rme + gp*s1rme);
		}
		else
		{
			matrix_element = dcoeff*gn*s1rme;
		}	
		return !Negligible(matrix_element);
	}
};

int main(int argc, char* argv[])
{
   // We do not need to call su3::init & su3::finalize
   // as it is done in PerformSU3Decomposition_TensorWithSingleJ0M0
   if (argc != 3) {
      std::cout << "Usage: " << argv[0] << " [<1> or <2>] <nmax> " << std::endl;
      std::cout << "option <1>: generate SU(3) decomposition of Q2 operator" << std::endl;
      std::cout << "option <2>: generate SU(3) decomposition of M1 operator" << std::endl;
      return 1;
	}

	int ioperator = atoi(argv[1]);
	if (ioperator != 1 && ioperator != 2)
	{
		std::cout << "Error: undefined type of operator" << std::endl;
		std::cout << "only 1 [Q2] or 2 [M1] are defined" << std::endl;
	  	std::cout << "Usage: "<< argv[0] << " [<1> or <2>] <nmax>" << std::endl; 
		std::cout << "option <1>: generate SU(3) decomposition of Q2 operator" << std::endl;
		std::cout << "option <2>: generate SU(3) decomposition of M1 operator" << std::endl;
		return 1;
	}

	int nmax = atoi(argv[2]);

   std::stringstream ss_filename;
	std::cout << "SU(3) decomposition of";
	if (ioperator == 1)
	{
		electric_moments::MatrixElementOneBody<E2>  Q;
		COneBodyOperators2SU3Tensors<electric_moments::MatrixElementOneBody<E2> > QDecomposer(Q, nmax);
		std::cout << " quadrupole mass operator Q_{JJ0=" << E2::kJJ0 << " " << E2::kMM0 << "} ... ";
      ss_filename << "Q2_1b_nmax" << nmax;
		QDecomposer.PerformSU3Decomposition_TensorWithSingleJ0M0(ss_filename.str());
	}
	else if (ioperator == 2)
	{
		electric_moments::MatrixElementOneBody<M1>  M;
		COneBodyOperators2SU3Tensors<electric_moments::MatrixElementOneBody<M1> > M1Decomposer(M, nmax);
		std::cout << " magnetic dipole moment operator M_{JJ0=" << M1::kJJ0 << " " << M1::kMM0 << "} ... ";
      ss_filename << "M1_1b_nmax" << nmax;
		M1Decomposer.PerformSU3Decomposition_TensorWithSingleJ0M0(ss_filename.str());
	}
	std::cout << "Done" << std::endl;
   std::cout << "Resulting SU(3) tensorial decomposition stored in '" << ss_filename.str() << "' file." << std::endl;
   return 0;
}
