#include <SU3ME/ScalarOperators2SU3Tensors.h>
#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/global_definitions.h>
#include <iostream>
#include <fstream>
#include <unordered_map>

#include <su3.h>

#include <LSU3/std.h>

using std::cout;
using std::endl;
using std::string;
using std::tuple;

struct CNoSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return true;
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return true;
	}
} noSelectionRules;


struct Cinteraction_sel_rules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm + su3ir.mu) % 2) == 0;
	}
	private:
} CInteractionSelectionRules;


struct JCoupledProtonNeutron2BMe_Hermitian_SingleOperator
{
	typedef double DOUBLE;
	typedef std::unordered_map<uint64_t, DOUBLE> HASH_TABLE;

	enum InteractionType {kPP = 0, kNN = 1, kPN = 2};

	std::vector<HASH_TABLE> mePP_, meNN_, mePN_;

	JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName);

	void SwitchHOConvention();

	inline size_t GetNumberOfOperators() const {return 1;}
	void Get_nlj(const int index, int& n, int& l, int& j) const // Warning: procedure returns angular momentum l, and not 2*l, while for total momentum j = 2*j
	{
		SPS2Index::Get_nlj(index, n, l, j);
	}
	inline int Get_Index(const int n, const int l, const int j) const	// ASSUMPTION: l == l ... usually l == 2*l, but this method expects l
	{
		return SPS2Index::Get_Index(n, l, j);
	}

	inline uint64_t MakeKey(uint16_t i1, uint16_t i2, uint16_t i3, uint16_t i4) const
	{
		return ((uint64_t)i1 << 48) | ((uint64_t)i2 << 32) | ((uint64_t)i3 << 16) | (uint64_t)i4;
	}

	inline void Key2Indices(const uint64_t key, uint16_t& i1, uint16_t& i2, uint16_t& i3, uint16_t& i4) const
	{ 
		i4 = (key & 0x000000000000FFFF);
      	i3 = (key & 0x00000000FFFF0000) >> 16;
    	i2 = (key & 0x0000FFFF00000000) >> 32;
	    i1 = (key & 0xFFFF000000000000) >> 48;
	}


	bool MeJ(const j_coupled_bra_ket::TwoBodyLabels& I1I2I3I4J, DOUBLE& mePP, DOUBLE& meNN, DOUBLE& mePN) const;

//	Iterate over "type" part of interaction and calculate Nmax,
//	which is defined with respect to valence_shell number
	int32_t Nmax(InteractionType type, int32_t valence_shell = 1);

//	For each element <I1 I2 JJ | V_{type} | I3 I4 JJ> look if its conjugate
//	counterpart <I3 I4 JJ | V | I1 I2 JJ > is also present in the file - which
//	would be strange - and if so then print its value.
	void TestHermicity(InteractionType type);
	
	void TestHermicityPP() {TestHermicity(kPP);}
	void TestHermicityNN() {TestHermicity(kNN);}
	void TestHermicityPN() {TestHermicity(kPN);}

	static void CheckI1I2I3I4(const std::string& sInteractionFileName);
};


void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::SwitchHOConvention()
{

	std::vector<HASH_TABLE>* meTablePtr;

	for (int type = kPP; type <= kPN; type++)
	{
		switch (type)
		{
			case kPP: meTablePtr = &mePP_; break;
			case kNN: meTablePtr = &meNN_; break;
			case kPN: meTablePtr = &mePN_; break;
			default : continue;
		}

		for (int i = 0; i < 255; ++i)
		{
			if (!(*meTablePtr)[i].empty())
			{
				for (HASH_TABLE::iterator it = (*meTablePtr)[i].begin(); it != (*meTablePtr)[i].end(); ++it)
				{
					uint32_t phase(0);
					uint16_t i1, i2, i3, i4;
					int32_t n, l, j;

					Key2Indices(it->first, i1, i2, i3, i4);

					Get_nlj(i1, n, l, j);
					phase += (n - l);
					
					Get_nlj(i2, n, l, j);
					phase += (n - l);
					
					Get_nlj(i3, n, l, j);
					phase += (n - l);
					
					Get_nlj(i4, n, l, j);
					phase += (n - l);

					it->second *= MINUSto(phase/2);
				}
			}
		}
	}
}

bool JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MeJ(const j_coupled_bra_ket::TwoBodyLabels& I1I2I3I4J, DOUBLE& mePP, DOUBLE& meNN, DOUBLE& mePN) const
{
	bool exist = false;
	int n, l, jj1, jj2, jj3, jj4;
	int phase(0);

	int i1(I1I2I3I4J[jt_coupled_bra_ket::I1]);
	int i2(I1I2I3I4J[jt_coupled_bra_ket::I2]);
	int i3(I1I2I3I4J[jt_coupled_bra_ket::I3]);
	int i4(I1I2I3I4J[jt_coupled_bra_ket::I4]);
	int JJ(2*I1I2I3I4J[jt_coupled_bra_ket::J]);

//	NOTE: We can not restrict proton-neutron matrix elements we store down to those
//	with bra/ket states with i1 <= i2 increasing indices. 
//	since | 1_{p} 2_{n} J> != (-)^phase |2_{p} 1_{n} J > !!!!
//
//	For proton-neutron interaction one needs to store only the upper diagonal
//	matrix. Unfortunately, it is not obvious from Nikola interaction files what
//	matrix elements belong to upper/lower triagonal matrix. Therefore, if
//	matrix element <i1 i2 J | Vpn | i3 i4 J>, is not in memory, we attempt to
//	find its conjugate <i3 i4 J| Vpn | i1 i2 J>. Only after this also fails, we
//	consider a given element to be zero.
//
//         1 1    1 2     1 3     1 4     2 1     2 2    2 3     2 4     3 1     3 2
//	1 1    x      x       x       x       x       x      x       x       x       x
//	1 2           x       x       x       x       x      x       x       x       x
//	1 3                   x       x       x       x      x       x       x       x
//	1 4                           x       x       x      x       x       x       x
//	2 1                                   x       x      x       x       x       x
//	2 2                                           x      x       x       x       x
//	2 3                                                  x       x       x       x
//	2 4                                                          x       x       x
//	3 1                                                                  x       x
//	3 2                                                                          x  ...
//	3 3                                                                             ...
//	3 4      
//	4 1      
//	4 2      
//	4 3      
//	4 4      
//
	uint64_t keyPN = MakeKey(i1, i2, i3, i4);
	HASH_TABLE::const_iterator itPN = mePN_[JJ].find(keyPN);

	if (itPN == mePN_[JJ].end())	// ==> <a b J || Vpn || c d J> does not exist
	{
		itPN = mePN_[JJ].find(MakeKey(i3, i4, i1, i2));	// try to find <c d J || Vpn || a b J>
	}

	if (itPN != mePN_[JJ].end())
	{
		mePN = itPN->second;
		exist =true;
	}

//	Obtain PP and NN matrix element
//	For proton-proton and neutron-neutron interaction, we store only the upper diagonal matrix:
//         1 1    1 2     1 3     1 4     2 2     2 3    2 4     3 3     3 4     4 4
//	1 1    x      x       x       x       x       x      x       x       x       x
//	1 2           x       x       x       x       x      x       x       x       x
//	1 3                   x       x       x       x      x       x       x       x
//	1 4                           x       x       x      x       x       x       x
//	2 2                                   x       x      x       x       x       x
//	2 3                                           x      x       x       x       x
//	2 4                                                  x       x       x       x
//	3 3                                                          x       x       x
//	3 4                                                                  x       x
//	4 4                                                                          x
//	
//	and hence besides I1 <= I2 and I3 <= I4, we store only those matrix elements that satisfy:
//
//	((I1 < I3) || ((I1 == I3) && I2 <= I4)))
//	
//	This can be seen, e.g., from the structure of the third row which is being stored:
//	<1 3 || || 1 3>, <1 3|| ||1 4>  ... I1 == I3 && I2 <= I4
//	<1 3 || || 2 2>, <1 3|| ||2 4>, <1 3|| ||3 3>, <1 3|| ||3 4>, <1 3|| ||4 4> .... I1 < I3

	if (i1 > i2) //	Use symmetry (A.5)		
	{
		Get_nlj(i1, n, l, jj1);
		Get_nlj(i2, n, l, jj2);
		std::swap(i1, i2);
		phase += (jj1 + jj2 - JJ)/2 + 1;
	}
	if (i3 > i4) //	Use symmetry (A.6)		
	{
		Get_nlj(i3, n, l, jj3);
		Get_nlj(i4, n, l, jj4);
		std::swap(i3, i4);
		phase += (jj3 + jj4 - JJ)/2 + 1;
	}
	if	(!((i1 < i3) || ((i1 == i3)  && (i2 <= i4))))	// since the operator is Hermition => use relation (A.9)
	{
		std::swap(i1, i3);
		std::swap(i2, i4);
	}

	uint64_t keyPPNN = MakeKey(i1, i2, i3, i4);

	HASH_TABLE::const_iterator itPP = mePP_[JJ].find(keyPPNN);
	HASH_TABLE::const_iterator itNN = meNN_[JJ].find(keyPPNN);

	if (itPP != mePP_[JJ].end())
	{
		mePP = 0.25*itPP->second*MINUSto(phase); // divided by 4.0
		exist =true;
	}

	if (itNN != meNN_[JJ].end())
	{
		meNN = 0.25*itNN->second*MINUSto(phase); // divide by 4.0
		exist =true;
	}

	return exist;
}

int32_t JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::Nmax(InteractionType type, int32_t valence_shell)
{
	assert(valence_shell > 0);

	HASH_TABLE::iterator it, it_transposed;
	int32_t n1, n2, l, j;
	uint16_t i1, i2, i3, i4;
	uint64_t key_transposed;
	int32_t N, Nmax(0);

	std::vector<HASH_TABLE>* meTablePtr;

	switch (type)
	{
		case kPP: meTablePtr = &mePP_; break;
		case kNN: meTablePtr = &meNN_; break;
		default: meTablePtr = &mePN_;
	}

	for (int i = 0; i < 255; ++i)
	{
		if (!(*meTablePtr)[i].empty())
		{
			for (it = (*meTablePtr)[i].begin(); it != (*meTablePtr)[i].end(); ++it)
			{
				Key2Indices(it->first, i1, i2, i3, i4);

				Get_nlj(i1, n1, l, j);
				Get_nlj(i2, n2, l, j);

				N = (n1 - valence_shell) + (n2 - valence_shell);
				if (N > Nmax)
				{
					std::swap(N, Nmax);
				}
			}
		}
	}
	return Nmax;
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::TestHermicity(InteractionType type)
{
	HASH_TABLE::iterator it, it_transposed;
	uint16_t i1, i2, i3, i4;
	uint64_t key_transposed;

	std::vector<HASH_TABLE>* meTablePtr;

	switch (type)
	{
		case kPP: meTablePtr = &mePP_; break;
		case kNN: meTablePtr = &meNN_; break;
		default: meTablePtr = &mePN_;
	}

	for (int i = 0; i < 255; ++i)
	{
		if (!(*meTablePtr)[i].empty())
		{
			for (it = (*meTablePtr)[i].begin(); it != (*meTablePtr)[i].end(); ++it)
			{
				Key2Indices(it->first, i1, i2, i3, i4);

				if (i1 == i3 && i2 == i4) //  <a b || V || a b> ... is certainly Hermitian
				{
					continue;
				}

				key_transposed = MakeKey(i3, i4, i1, i2);
				it_transposed = (*meTablePtr)[i].find(key_transposed);

				if (it_transposed != (*meTablePtr)[i].end())
				{
					cout << i1 << " " << i2 << " " << i3 << " " << i4 << " JJ:" << i << "\t" << it->second << "\t\t Has its hermitian conjugate with value:" << it_transposed->second << endl;
					int n, l, j;
					Get_nlj(i1, n, l, j);
					cout << "n1:" << n << " l1:" << l << " j1:" << j << "\t";

					Get_nlj(i2, n, l, j);
					cout << "n2:" << n << " l2:" << l << " j2:" << j << "\t";

					Get_nlj(i3, n, l, j);
					cout << "n3:" << n << " l3:" << l << " j3:" << j << "\t";

					Get_nlj(i4, n, l, j);
					cout << "n4:" << n << " l4:" << l << " j4:" << j << endl << endl;
				}
			}
		}
	}
}

//	Check whether for proton-proton and neutron-neutron interaction in the file
//	"sInteractionFileName" all matrix elements do not occur multiple times with
//	a different order of indices, i.e.
//
//	<I1 I2 JJ || V || I3 I4 JJ > .... <I2 I1 JJ || V || J3 I4 JJ> etc ...
//	
//	Reason to check:
//
//	This would be a wate of disk space since those matrix element are related by a trivial phase.
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::CheckI1I2I3I4(const std::string& sInteractionFileName)
{
	int i1, i2, i3, i4, jj, tz;
	DOUBLE value;

	std::fstream file(sInteractionFileName.c_str());
	if (!file)
	{
		std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
		return;
	}

	std::map<std::tuple<int, int, int, int, int>, int > mepp_cntr, menn_cntr, mepn_cntr;

	cout << "Start reading interaction file ... "; cout.flush();
	uint32_t line_number = 0;
	
	while (true)
	{
		file >> i1 >> i2 >> i3 >> i4 >> tz >> jj >> value;

		if (!file) {
			break;
		}

//	for proton-proton and neutron-neutron interaction, we store only the upper diagonal matrix:
//         1 1    1 2     1 3     1 4     2 2     2 3    2 4     3 3     3 4     4 4
//	1 1    x      x       x       x       x       x      x       x       x       x
//	1 2           x       x       x       x       x      x       x       x       x
//	1 3                   x       x       x       x      x       x       x       x
//	1 4                           x       x       x      x       x       x       x
//	2 2                                   x       x      x       x       x       x
//	2 3                                           x      x       x       x       x
//	2 4                                                  x       x       x       x
//	3 3                                                          x       x       x
//	3 4                                                                  x       x
//	4 4                                                                          x
//	
//	and hence besides I1 <= I2 and I3 <= I4, we store only those matrix elements that satisfy: 
//
//	((I1 < I3) || ((I1 == I3) && I2 <= I4)))
//	
//	This can be seen, e.g., from the structure of the third row which is being stored:
//	<1 3 || || 1 3>, <1 3|| ||1 4>  ... I1 == I3 && I2 <= I4
//	<1 3 || || 2 2>, <1 3|| ||2 4>, <1 3|| ||3 3>, <1 3|| ||3 4>, <1 3|| ||4 4> .... I1 < I3
		if (tz != 0)
		{
//	Instead of |I1 > I2 JJ> store |J2 < I1 JJ>
			if (i1 > i2)
			{
				std::swap(i1, i2);
			}
//	Instead of |I3 > I4 JJ> store |J3 < I4 JJ>
			if (i3 > i4)
			{
				std::swap(i3, i4);
			}
//	make sure we store only the upper triangular part of an interaction matrix
			if	(!((i1 < i3) || ((i1 == i3)  && (i2 <= i4))))	// since the operator is Hermition 
			{
				std::swap(i1, i3);
				std::swap(i2, i4);
			}
		}

		switch (tz)
		{
			case +1: mepp_cntr[std::make_tuple(i1, i2, i3, i4, jj)] += 1; break;
			case  0: mepn_cntr[std::make_tuple(i1, i2, i3, i4, jj)] += 1; break;
			case -1: menn_cntr[std::make_tuple(i1, i2, i3, i4, jj)] += 1; break;
			default:
			cout << "line#:" << line_number << " contains a strange value of Tz!" << endl;
			cout << i1 << " " << i2 << " " << i3 << " " << i4 << " " << " Tz:" << tz << " " << jj << " " << value << endl;
			cout << "Error ... tz=" << tz << "?!" << endl;
			exit(EXIT_FAILURE);
		}
	}
	cout << "\t Done" << endl;

	std::map<tuple<int, int, int, int, int>, int >::const_iterator itpp =  mepp_cntr.begin();
	for (; itpp != mepp_cntr.end(); ++itpp)
	{
		if (itpp->second > 1)
		{
			cout << std::get<0>(itpp->first) << " " << std::get<1>(itpp->first) << " " << std::get<2>(itpp->first) << " " << std::get<3>(itpp->first) << " ";
			cout << std::get<4>(itpp->first)  << "\t" <<  itpp->second << endl;
			cout << "Error: this terms occurs multiple times!" << endl;
		}
	}

	std::map<tuple<int, int, int, int, int>, int >::const_iterator itnn =  menn_cntr.begin();
	for (; itnn != menn_cntr.end(); ++itnn)
	{
		if (itnn->second > 1)
		{
			cout << std::get<0>(itnn->first) << " " << std::get<1>(itnn->first) << " " << std::get<2>(itnn->first) << " " << std::get<3>(itnn->first) << " ";
			cout << std::get<4>(itnn->first)  << "\t" <<  itnn->second << endl;
			cout << "Error: this term occurs multiple times!" << endl;
		}
	}
/*
	std::map<tuple<int, int, int, int, int>, int >::const_iterator itpn =  mepn_cntr.begin();
	for (; itpn != mepn_cntr.end(); ++itpn)
	{
		cout << std::tr1::get<0>(itpn->first) << " " << std::tr1::get<1>(itpn->first) << " " << std::tr1::get<2>(itpn->first) << " " << std::tr1::get<3>(itpn->first) << " ";
		cout << std::tr1::get<4>(itpn->first)  << "\t" <<  itpn->second << endl;
	}
*/	
}

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName):	mePP_(255, HASH_TABLE()), 
																																					meNN_(255, HASH_TABLE()), 
																																					mePN_(255, HASH_TABLE())
{
	int n, l, jja, jjb, jjc, jjd;
	int phase(0);

	int i1, i2, i3, i4, jj, tz;
	DOUBLE value;

	std::fstream file(sInteractionFileName.c_str());
	if (!file)
	{
		std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
		exit (EXIT_FAILURE);
	}

	cout << "Start reading interaction file ... "; cout.flush();
	uint32_t line_number = 0;
	while (true)
	{
		file >> i1 >> i2 >> i3 >> i4 >> tz >> jj >> value;

		if (!file) {
			break;
		}

		line_number++;

		if (tz != 0)
		{
			if (i1 > i2) //	Use symmetry (A.5)		
			{
				Get_nlj(i1, n, l, jja);
				Get_nlj(i2, n, l, jjb);
				std::swap(i1, i2);
				value *= MINUSto((jja + jjb - jj)/2 + 1);
			}
			if (i3 > i4) //	Use symmetry (A.6)		
			{
				Get_nlj(i3, n, l, jjc);
				Get_nlj(i4, n, l, jjd);
				std::swap(i3, i4);
				value *= MINUSto((jjc + jjd - jj)/2 + 1);
			}
			if	(!((i1 < i3) || ((i1 == i3)  && (i2 <= i4))))	// since the operator is Hermition => use relation (A.9)
			{
				std::swap(i1, i3);
				std::swap(i2, i4);
			}
		}

		if (tz == -1)	// NN
		{
			meNN_[jj][MakeKey(i1, i2, i3, i4)] = value;
		}
		else if (tz == +1)	// PP
		{
			mePP_[jj][MakeKey(i1, i2, i3, i4)] = value;
		}
		else if (tz == 0)	// PN
		{
#ifdef NPNP_ORDER 
//	SU3decomposition code assumes that the order of proton-neutron indices is following: <p n|| V ||p n>.
//	If this is not the case, then we first employ the following relation: <p n || V || p n> = (-)^(j1+j2+j3+j3)*<n p || V || n p> 

			Get_nlj(i1, n, l, jja);
			Get_nlj(i2, n, l, jjb);
			Get_nlj(i3, n, l, jjc);
			Get_nlj(i4, n, l, jjd);

			std::swap(i1, i2);
			std::swap(i3, i4);

			value *= MINUSto((jja + jjb + jjc + jjd)/2);
#endif		
			mePN_[jj][MakeKey(i1, i2, i3, i4)] = value;
		}
		else
		{
			cout << "line#:" << line_number << " contains a strange value of Tz!" << endl;
			cout << i1 << " " << i2 << " " << i3 << " " << i4 << " " << " Tz:" << tz << " " << jj << " " << value << endl;
			cout << "Error ... tz=" << tz << "?!" << endl;
			exit(EXIT_FAILURE);
		}
	}
	cout << "\t Done" << endl;

	cout << "Nmax_pp:" << Nmax(kPP, 1) << endl;
	cout << "Nmax_nn:" << Nmax(kNN, 1) << endl;
	cout << "Nmax_pn:" << Nmax(kPN, 1) << endl;
}


int main(int argc, char* argv[])
{
#ifdef USER_FRIENDLY_OUTPUT
	cerr << "You have to comment line with '#define USER_FRIENDLY_OUTPUT' in UNU3SU3basics.h !" << endl;
	return 1;
#endif	
/** \todo nmax could be obtained from input_file */	
	if (argc != 5)
	{
		cout << endl;
	  	cout << "Usage: " << argv[0] << " <Nmax> <valence shell> <file in proton-neutron J-coupled format> <output file with SU3 tensors>" << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << "Structure of the outputfile:" << endl;
		cout << "n1 n2 n3 n4 IRf IRi IR0 iS" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 0" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 0" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 0" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 1" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 1" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
		return 0;
	}

	int Nmax = atoi(argv[1]);
	int valence_shell = atoi(argv[2]);
	int nmax = Nmax + valence_shell;

	string sFileName(argv[3]);
	string sFileSU3Decomposition(argv[4]);

	JCoupledProtonNeutron2BMe_Hermitian_SingleOperator JCoupledPNME(sFileName);

//	SwitchHOConvention: 
//	multiplies matrix elements by phase = (-)^{1/2 sum (ni - li)} transforming
//	matrix elements given in HO basis positive at origin into positive at
//	infinity and the other way around.
	JCoupledPNME.SwitchHOConvention();

/*
	JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::CheckI1I2I3I4(sFileName);

	cout << "Testing Hermicity of Vpp" << endl;
	JCoupledPNME.TestHermicityPP();

	cout << "Testing Hermicity of Vnn" << endl;
	JCoupledPNME.TestHermicityNN();

	cout << "Testing Hermicity of Vpn" << endl;
	JCoupledPNME.TestHermicityPN();
*/

	CScalarTwoBodyOperators2SU3Tensors<JCoupledProtonNeutron2BMe_Hermitian_SingleOperator> Decomposer(JCoupledPNME, Nmax, valence_shell, nmax);
	
   su3::init();
	size_t nTensorComponents = Decomposer.PerformSU3Decomposition(CInteractionSelectionRules, sFileSU3Decomposition);
   su3::finalize();
}
