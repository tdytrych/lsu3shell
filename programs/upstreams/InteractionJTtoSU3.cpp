#include <boost/mpi.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif

#include <LookUpContainers/HashFixed.h>
#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/ScalarOperators2SU3Tensors.h>
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <LSU3/std.h>

#include <su3.h>

#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <tuple>

#define TIMING
#define USE_GSL_WIG

#ifdef USE_GSL_WIG
#include <gsl/gsl_sf.h>
#endif

using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::string;
using std::vector;

//	The simplest hash function possible is used for CWigner9j and CWigner6j
//	structure
struct dontdoanything {
  public:
   inline std::size_t operator()(int const& i) const { return i; }
};

struct ParityPreserving {
   inline bool operator()(int n1, int n2, int n3, int n4) const {
      return ((MINUSto(n1) * MINUSto(n2)) ==
              (MINUSto(n3) * MINUSto(n4)));  // return true if parity is conserved
   }
};

// TODO: Note that the spin selection rules do not work as they are never applied

//	This struct is used for remove_if algorithm ==> it has to return true if a given tensor is
//not included
struct Cinteraction_sel_rules {
   inline bool operator()(const SU2::LABEL& Spin) const { return false; }

   inline bool operator()(const SU3::LABELS& su3ir) const { return ((su3ir.lm + su3ir.mu) % 2); }

  private:
} CInteractionSelectionRules;

struct CVcoul {
   inline bool operator()(const SU2::LABEL& Spin) const { return Spin != 0; }

   inline bool operator()(const SU3::LABELS& su3ir) const { return ((su3ir.lm + su3ir.mu) % 2); }

  private:
} VcoulSelectionRules;

struct CTrelSelectionRules {
   inline bool operator()(const SU2::LABEL& Spin) const { return Spin != 0; }
   inline bool operator()(const SU3::LABELS& su3ir) const {
      if ((su3ir.lm == 2) && (su3ir.mu == 0)) {
         return false;
      }
      if ((su3ir.lm == 0) && (su3ir.mu == 2)) {
         return false;
      }
      if ((su3ir.lm == 0) && (su3ir.mu == 0)) {
         return false;
      }
      //		return ((su3ir.lm == 2) && (su3ir.mu == 0)) || ((su3ir.lm == 0) && (su3ir.mu
      //== 2)) || ((su3ir.lm == 0) && (su3ir.mu == 0));
      return true;
   }

  private:
} TrelSelectionRules;

typedef CTuple<uint8_t, 4> NANBNCND;

enum { kNA = 0, kNB = 1, kNC = 2, kND = 3 };
enum { kDoesNotExist = 0xFFFF };

const int SfSiS0[6][3] = {{0, 0, 0}, {1, 1, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 1}, {1, 1, 2}};
const double PiSfSiS0[6] = {1, 3, 3, 3, sqrt(27), sqrt(45)};

////////////////////////////////////////////////////////////////////////////////////
//  		Hash structures for storing Wigner 9j and 6 j coefficients
////////////////////////////////////////////////////////////////////////////////////
//	Store Wigner 9j coefficients:
//    la    lb  L
//    1/2  1/2  S
//    ja    jb  J
////////////////////////////////////////////////////////////////////////////////////
class CWigner9j {
  public:
   typedef HashFixed<int64_t, double, dontdoanything> CACHE;
   typedef CACHE* WIGNER_9J_TABLE;

  public:
   CWigner9j()
       : wig9jTable_((WIGNER_9J_TABLE) new char[sizeof(CACHE) * 256])  // L <= 2*nmax
   {
      for (int i = 0; i <= 255; ++i) {
         new (wig9jTable_ + i) CACHE(4096);
      }
   }

   double GetWigner9j(int lla, int llb, int ll, int ss, int jja, int jjb, int jj) {
      CACHE& wig9jS0 = wig9jTable_[ll / 2];
      CACHE::iterator wig9j_iter = wig9jS0.find(INTEGER64_KEY(lla, llb, ss, jja, jjb, jj));

      if (wig9j_iter == wig9jS0.end()) {
         double wig9j;
#ifdef USE_GSL_WIG
         wig9j = gsl_sf_coupling_9j(lla, llb, ll, 1, 1, ss, jja, jjb, jj);
#else
         wig9j = wigner9j(lla, llb, ll, 1, 1, ss, jja, jjb, jj);
#endif
         wig9jS0.insert(INTEGER64_KEY(lla, llb, ss, jja, jjb, jj), wig9j);
         return wig9j;
      } else {
         return *wig9j_iter;
      }
   }

   ~CWigner9j() {
      for (int i = 0; i <= 255; ++i) {
         (wig9jTable_ + i)->~CACHE();
      }
      delete[](char*)(wig9jTable_);
   }

  private:
   /** Construct integer key from the values of \f$L_{i}, S_{i}, L_{f}, S_{f}\f$. */
   inline int64_t INTEGER64_KEY(int64_t a, int64_t b, int c, int d, int e, int f) const {
      return (a << 40) | (b << 32) | (c << 24) | (d << 16) | (e << 8) | f;
   }
   WIGNER_9J_TABLE wig9jTable_;
};

class CWigner6j {
  public:
   typedef HashFixed<int64_t, double, dontdoanything> WIGNER_6J_TABLE;

  public:
   CWigner6j() : wig6jTable_(4096){};

   double GetWigner6j(int a, int b, int c, int d, int e, int f) {
      WIGNER_6J_TABLE::iterator wig6j_iter = wig6jTable_.find(INTEGER64_KEY(a, b, c, d, e, f));

      if (wig6j_iter == wig6jTable_.end()) {
         double wig6j;
#ifdef USE_GSL_WIG
         wig6j = gsl_sf_coupling_6j(a, b, c, d, e, f);
#else
         wig6j = wigner6j(a, b, c, d, e, f);
#endif
         wig6jTable_.insert(INTEGER64_KEY(a, b, c, d, e, f), wig6j);
         return wig6j;
      } else {
         return *wig6j_iter;
      }
   }

  private:
   /** Construct integer key from the values of \f$L_{i}, S_{i}, L_{f}, S_{f}\f$. */
   inline int64_t INTEGER64_KEY(int64_t a, int64_t b, int c, int d, int e, int f) const {
      return (a << 40) | (b << 32) | (c << 24) | (d << 16) | (e << 8) | f;
   }
   WIGNER_6J_TABLE wig6jTable_;
};
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
// 			Functions computing SU(3) Wigner coefficients
////////////////////////////////////////////////////////////////////////////////////
//	This function calculates and returns SU(3) CGs of the following type:
//	<(na 0) 0 *; (nb 0) 0 * || wf * *>.
//
//	For each allowed value la and lb, elements la x lb_max + la contains index of su3cgs vector
//	whose element contains <(na 0) 0 la, (nb 0) 0 lb || wf 0 min(L0)>_{0}, where
//	min(L0) is the lowest allowed value of L0 for a given irrep wf.
//
//	Order: index = L0 x k0_max + L0 -------> su3cgs[k0, rho0, kf, ki]
void ComputeSU3CGs_Simple(const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0,
                          vector<uint16_t>& indices, vector<double>& su3cgs) {
   const int max_ki = 1;
   const int max_kf = 1;
   const int max_rho = 1;

   assert((wf.lm == 0 && wi.lm == 0) || (wf.mu == 0 && wi.mu == 0));

   su3cgs.resize(0);
   int max_Lf = wf.lm + wf.mu + 1;
   int max_Li = wi.lm + wi.mu + 1;
   int max_L0 = w0.lm + w0.mu + 1;

   std::vector<double> dCG;
   dCG.reserve(64);  // allocate some memory

   vector<uint16_t> temp_indices(max_Lf * max_Li, kDoesNotExist);
   std::vector<double> temp_su3cgs;
   // preallocate enough of memory
   temp_su3cgs.reserve(max_Lf * max_Li * max_L0);

   int lmf(wf.lm), muf(wf.mu), lmi(wi.lm), mui(wi.mu), lm0(w0.lm), mu0(w0.mu);

   for (int Lf = 0; Lf < max_Lf; Lf++)  // iterate over all Lf in (lmf muf)
   {
      if (!su3::kmax(lmf, muf, Lf)) {
         continue;
      }
      for (int Li = 0; Li < max_Li; Li++)  // iterate over all Li in (lmi mui)
      {
         if (!su3::kmax(lmi, mui, Li)) {
            continue;
         }
         temp_su3cgs.resize(0);
         for (int L0 = abs(Lf - Li); L0 <= (Lf + Li) && L0 < max_L0; L0++) {
            int max_k0 = su3::kmax(lm0, mu0, L0);
            if (!max_k0) {
               continue;
            }
            su3::wu3r3w(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_k0, max_ki, max_kf, max_rho,
                        dCG);
            temp_su3cgs.insert(temp_su3cgs.end(), dCG.begin(), dCG.end());
         }
         if (!temp_su3cgs.empty()) {
            temp_indices[Lf * max_Li + Li] = su3cgs.size();
            su3cgs.insert(su3cgs.end(), temp_su3cgs.begin(), temp_su3cgs.end());
         }
      }
   }
   temp_indices.swap(indices);
}

//	This function calculates and returns SU(3) CGs of the following type:
//	<wf * *; wi * * || w0 * L0>_{*}, where L0 = 0, 1, 2 ... i.e. I assume
//	it is needed for a two-body scalar (L0=S0) interaction. It could be
//	easily generalized for a list of L0 values if needed [i.e. for 3-b interactions]
//
//	For each allowed value Lf and Li, elements Lf x Li_max + Li contains index of su3cgs vector
//	whose element contains <wf 0 Lf, wi 0 Li || w0 0 min(L0)>_{0}, where
//	min(L0) is the lowest allowed value of L0 for a given irrep w0.
//
//	Order: index = k0 x rho0_max x kf_max x ki_max + rho0 x kf_max x ki_max + kf x ki_max + ki
//-------> su3cgs[k0, rho0, kf, ki]
void ComputeSU3CGs(const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0,
                   vector<uint16_t>& indices, vector<double>& su3cgs) {
   su3cgs.clear();
   //	NOTE: Lf ranges from 0 up to (lm + mu) ---> number of Lf == max_Lf
   int max_Lf = wf.lm + wf.mu + 1;
   int max_Li = wi.lm + wi.mu + 1;

   std::vector<double> dCG;
   dCG.reserve(32);

   vector<uint16_t> temp_indices(max_Lf * max_Li, kDoesNotExist);
   //	Estimation of maximal size of temp_su3cgs:
   std::vector<double> temp_su3cgs;
   temp_su3cgs.reserve(128);

   int lmf(wf.lm), muf(wf.mu), lmi(wi.lm), mui(wi.mu), lm0(w0.lm), mu0(w0.mu);
   int max_rho = su3::mult(lmf, muf, lmi, mui, lm0, mu0);

   for (int Lf = 0; Lf < max_Lf; Lf++)  // iterate over all Lf in (lmf muf)
   {
      int max_kf = su3::kmax(lmf, muf, Lf);
      if (!max_kf) {
         continue;
      }

      for (int Li = 0; Li < max_Li; Li++)  // iterate over all Li in (lmi mui)
      {
         int max_ki = su3::kmax(lmi, mui, Li);
         if (!max_ki) {
            continue;
         }

         temp_su3cgs.resize(0);
         for (int L0 = abs(Lf - Li); L0 <= std::min(2, Lf + Li); L0++) {
            int max_k0 = su3::kmax(lm0, mu0, L0);
            if (!max_k0) {
               continue;
            }

            // dCG[k0][ki][kf][rho]
            su3::wu3r3w(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_k0, max_ki, max_kf, max_rho, dCG);

            for (int k0 = 0; k0 < max_k0; k0++)  // for all values of k0
            {
               for (int irho = 0; irho < max_rho; irho++)  // for all possible multiplicities rho0
               {
                  for (int kf = 0; kf < max_kf; kf++)  // for all values of kf
                  {
                     for (int ki = 0; ki < max_ki; ki++)  // for all values of ki
                     {
                        size_t index_k0kikfrho =
                            ((k0 * max_ki + ki) * max_kf + kf) * max_rho + irho;
                        temp_su3cgs.push_back(dCG[index_k0kikfrho]);
                     }
                  }
               }
            }
         }
         if (!temp_su3cgs.empty()) {
            temp_indices[Lf * max_Li + Li] = su3cgs.size();
            su3cgs.insert(su3cgs.end(), temp_su3cgs.begin(), temp_su3cgs.end());
         }
      }
   }
   temp_indices.swap(indices);
}
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

struct JCoupledProtonNeutron2BMe_Hermitian_SingleOperator {
   typedef double DOUBLE;

   enum ME_DATA_STRUCTURE { kSize, kI1, kI2, kI3, kI4, kJ, kVpp, kVnn, kVpn };
   typedef std::tuple<uint32_t, uint16_t*, uint16_t*, uint16_t*, uint16_t*, uint8_t*, DOUBLE*,
                        DOUBLE*, DOUBLE*>
       ME_DATA;
   typedef std::tuple_element<kSize, ME_DATA>::type SIZE_t;
   typedef std::tuple_element<kI1, ME_DATA>::type I1_t;
   typedef std::tuple_element<kI2, ME_DATA>::type I2_t;
   typedef std::tuple_element<kI3, ME_DATA>::type I3_t;
   typedef std::tuple_element<kI4, ME_DATA>::type I4_t;
   typedef std::tuple_element<kJ, ME_DATA>::type J_t;
   typedef std::tuple_element<kVpp, ME_DATA>::type VPP_t;
   typedef std::tuple_element<kVnn, ME_DATA>::type VNN_t;
   typedef std::tuple_element<kVpn, ME_DATA>::type VPN_t;

   typedef map<NANBNCND, ME_DATA> INTERACTION_DATA;
   typedef INTERACTION_DATA::iterator ME_ITER;

   INTERACTION_DATA interaction_data_;

   JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName,
                                                      vector<NANBNCND>& ho_shells_combinations);
   ~JCoupledProtonNeutron2BMe_Hermitian_SingleOperator() {
      for (ME_ITER it = interaction_data_.begin(); it != interaction_data_.end(); ++it) {
         delete[] std::get<kI1>(it->second);
         delete[] std::get<kI2>(it->second);
         delete[] std::get<kI3>(it->second);
         delete[] std::get<kI4>(it->second);
         delete[] std::get<kJ>(it->second);
         delete[] std::get<kVpp>(it->second);
         delete[] std::get<kVnn>(it->second);
         delete[] std::get<kVpn>(it->second);
      }
   }
   //	Switching between the two conventions for the single-particle radial HO wave functions
   void SwitchHOConvention();

   //	Multiply each matrix elements by
   void MultiplyByCommonFactor();
   ME_DATA GetMe(NANBNCND nanbncnd);

   void Show();

   template <typename SelectionRules>
   void PerformSU3Decomposition(const string& sOutputFileName);
};

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
    JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName,
                                                       vector<NANBNCND>& ho_shells_combinations) {
   boost::mpi::communicator mpi_comm_world;
   int my_rank = mpi_comm_world.rank();
   int nprocs = mpi_comm_world.size();

   int rank_destination;
   uint32_t n_elems;
   NANBNCND nanbncnd;

   //	Root process reads file and distributes it to other processes
   if (my_rank == 0) {
      std::ifstream file(sInteractionFileName.c_str(), std::ifstream::binary);
      if (!file) {
         std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
         exit(EXIT_FAILURE);
      }
      cout << "Start reading interaction file ... \n";
      cout.flush();

      std::vector<int> number_combinations(nprocs, 0);
      std::vector<int> number_tbme(nprocs, 0);

      int na, nb, nc, nd;
      while (true) {
         file.read((char*)&na, sizeof(int));
         file.read((char*)&nb, sizeof(int));
         file.read((char*)&nc, sizeof(int));
         file.read((char*)&nd, sizeof(int));
         file.read((char*)&n_elems, sizeof(uint32_t));
         //	We are at the end of file
         if (!file) {
            //	Brocadcast -1 ===> Let other processes know that end of file has been achieved
            n_elems = -1;
            MPI_Bcast((void*)&n_elems, 1, MPI_INT, 0, mpi_comm_world);
            break;
         }

         I1_t vec_i1 = new uint16_t[n_elems];
         I2_t vec_i2 = new uint16_t[n_elems];
         I3_t vec_i3 = new uint16_t[n_elems];
         I4_t vec_i4 = new uint16_t[n_elems];
         J_t vec_J = new uint8_t[n_elems];
         VPP_t vec_Vpp = new double[n_elems];
         VNN_t vec_Vnn = new double[n_elems];
         VPN_t vec_Vpn = new double[n_elems];

         file.read((char*)vec_i1, n_elems * sizeof(uint16_t));
         file.read((char*)vec_i2, n_elems * sizeof(uint16_t));
         file.read((char*)vec_i3, n_elems * sizeof(uint16_t));
         file.read((char*)vec_i4, n_elems * sizeof(uint16_t));
         file.read((char*)vec_J, n_elems * sizeof(uint8_t));
         file.read((char*)vec_Vpp, n_elems * sizeof(double));
         file.read((char*)vec_Vnn, n_elems * sizeof(double));
         file.read((char*)vec_Vpn, n_elems * sizeof(double));

         nanbncnd[kNA] = na;
         nanbncnd[kNB] = nb;
         nanbncnd[kNC] = nc;
         nanbncnd[kND] = nd;

         //	find a position of {na, nb, nc, nd} in a list of HO shell combination
         int position = std::distance(
             ho_shells_combinations.begin(),
             std::find(ho_shells_combinations.begin(), ho_shells_combinations.end(), nanbncnd));
         if (position ==
             ho_shells_combinations
                 .size())  // ==> {na, nb, nc, nd} is not between HO combinations to be processes
         {
            delete[] vec_i1;
            delete[] vec_i2;
            delete[] vec_i3;
            delete[] vec_i4;
            delete[] vec_J;
            delete[] vec_Vpp;
            delete[] vec_Vnn;
            delete[] vec_Vpn;
            continue;
         }
         int rank_destination = position % nprocs;
         number_tbme[rank_destination] += n_elems;
         number_combinations[rank_destination] += 1;
         MPI_Bcast((void*)&n_elems, 1, MPI_INT, 0, mpi_comm_world);
         MPI_Bcast((void*)&rank_destination, 1, MPI_INT, 0, mpi_comm_world);

         //			cout << na << " " << nb << " " << nc << " " << nd << "\tNumber of matrix
         //elements:" << n_elems << "\t Assigned to process:" << rank_destination << endl;

         if (rank_destination !=
             my_rank)  // data obtained from file will be sent to rank = rank_destination
         {
            mpi_comm_world.send(rank_destination, 0, nanbncnd);
            mpi_comm_world.send(rank_destination, 0, vec_i1, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_i2, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_i3, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_i4, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_J, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_Vpp, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_Vnn, n_elems);
            mpi_comm_world.send(rank_destination, 0, vec_Vpn, n_elems);

            delete[] vec_i1;
            delete[] vec_i2;
            delete[] vec_i3;
            delete[] vec_i4;
            delete[] vec_J;
            delete[] vec_Vpp;
            delete[] vec_Vnn;
            delete[] vec_Vpn;
         } else  // data that root process obtained from input file are assigned to root process
         {
            INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
            if (it == interaction_data_.end()) {
               interaction_data_.insert(std::make_pair(
                   nanbncnd, std::make_tuple(n_elems, vec_i1, vec_i2, vec_i3, vec_i4, vec_J,
                                               vec_Vpp, vec_Vnn, vec_Vpn)));
            } else {
               // This should never happen
               std::cerr << "The same set of {na, nb, nc, nd}={" << na << ", " << nb << ", " << nc
                         << ", " << nc << "} occurs multiple times in the input file '"
                         << sInteractionFileName << "' !!!" << std::endl;
               exit(EXIT_FAILURE);
            }
         }
      }
      cout << "Done" << endl;
      for (int irank = 0; irank < nprocs; ++irank) {
         cout << "rank:" << irank << "\t#HO combinations:" << number_combinations[irank]
              << "\t #tbme:" << number_tbme[irank] << endl;
      }
   } else {
      do {
         MPI_Bcast((void*)&n_elems, 1, MPI_INT, 0, mpi_comm_world);
         if (n_elems == -1) {
            break;
         }
         MPI_Bcast((void*)&rank_destination, 1, MPI_INT, 0, mpi_comm_world);

         if (my_rank == rank_destination) {
            I1_t vec_i1 = new uint16_t[n_elems];
            I2_t vec_i2 = new uint16_t[n_elems];
            I3_t vec_i3 = new uint16_t[n_elems];
            I4_t vec_i4 = new uint16_t[n_elems];
            J_t vec_J = new uint8_t[n_elems];
            VPP_t vec_Vpp = new double[n_elems];
            VNN_t vec_Vnn = new double[n_elems];
            VPN_t vec_Vpn = new double[n_elems];

            mpi_comm_world.recv(0, 0, nanbncnd);
            mpi_comm_world.recv(0, 0, vec_i1, n_elems);
            mpi_comm_world.recv(0, 0, vec_i2, n_elems);
            mpi_comm_world.recv(0, 0, vec_i3, n_elems);
            mpi_comm_world.recv(0, 0, vec_i4, n_elems);
            mpi_comm_world.recv(0, 0, vec_J, n_elems);
            mpi_comm_world.recv(0, 0, vec_Vpp, n_elems);
            mpi_comm_world.recv(0, 0, vec_Vnn, n_elems);
            mpi_comm_world.recv(0, 0, vec_Vpn, n_elems);

            INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
            if (it == interaction_data_.end()) {
               interaction_data_.insert(std::make_pair(
                   nanbncnd, std::make_tuple(n_elems, vec_i1, vec_i2, vec_i3, vec_i4, vec_J,
                                               vec_Vpp, vec_Vnn, vec_Vpn)));
            } else {
               // This should never happen
               std::cerr
                   << "The same set of {na, nb, nc, nd} occurs multiple times in the input file '"
                   << sInteractionFileName << "' !!!" << std::endl;
               exit(EXIT_FAILURE);
            }
         }
      } while (true);
   }
}

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::ME_DATA
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::GetMe(NANBNCND nanbncnd) {
   INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
   if (it != interaction_data_.end()) {
      return it->second;
   } else {
#if (__GNUC__ == 4 && __GNUC_MINOR__ == 4)
      uint16_t* p1(0);
      uint8_t* p2(0);
      DOUBLE* p3(0);
      return std::make_tuple(0, p1, p1, p1, p1, p2, p3, p3, p3);
#else
      return std::make_tuple(0, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
                               nullptr);
#endif
   }
}

// This function multiplies all matrix elements by a factor
// described in devel/docs/Recoupling/Recoupling.pdf
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MultiplyByCommonFactor() {
   INTERACTION_DATA::iterator it = interaction_data_.begin();
   for (; it != interaction_data_.end(); ++it) {
      ME_DATA me_data = it->second;
      SIZE_t size = std::get<kSize>(me_data);
      I1_t i1 = std::get<kI1>(me_data);
      I2_t i2 = std::get<kI2>(me_data);
      I3_t i3 = std::get<kI3>(me_data);
      I4_t i4 = std::get<kI4>(me_data);
      J_t J = std::get<kJ>(me_data);
      VPP_t Vpp = std::get<kVpp>(me_data);
      VNN_t Vnn = std::get<kVnn>(me_data);
      VPN_t Vpn = std::get<kVpn>(me_data);

      for (int i = 0; i < size; ++i) {
         int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

         SPS2Index::Get_nlj(i1[i], na, la, jja);
         SPS2Index::Get_nlj(i2[i], nb, lb, jjb);
         SPS2Index::Get_nlj(i3[i], nc, lc, jjc);
         SPS2Index::Get_nlj(i4[i], nd, ld, jjd);
         JJ = 2 * J[i];

         double common_phase = MINUSto(nd + nc - (jjd + jjc) / 2) *
                               std::sqrt((jja + 1) * (jjb + 1) * (jjc + 1) * (jjd + 1)) * (JJ + 1);
         //	delta_{na nb}{la lb}{ja jb}{ta tb}
         int iab = (na == nb) && (la == lb) && (jja == jjb);
         //	delta_{nc nd}{lc ld}{jc jd}{tc td}
         int idc = (nd == nc) && (ld == lc) && (jjd == jjc);

         Vpp[i] *= common_phase * std::sqrt((iab + 1) * (idc + 1));
         Vnn[i] *= common_phase * std::sqrt((iab + 1) * (idc + 1));
         Vpn[i] *= common_phase;
      }
   }
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::SwitchHOConvention() {
   INTERACTION_DATA::iterator it = interaction_data_.begin();
   for (; it != interaction_data_.end(); ++it) {
      ME_DATA me_data = it->second;

      SIZE_t size = std::get<kSize>(me_data);
      I1_t i1 = std::get<kI1>(me_data);
      I2_t i2 = std::get<kI2>(me_data);
      I3_t i3 = std::get<kI3>(me_data);
      I4_t i4 = std::get<kI4>(me_data);
      VPP_t Vpp = std::get<kVpp>(me_data);
      VNN_t Vnn = std::get<kVnn>(me_data);
      VPN_t Vpn = std::get<kVpn>(me_data);

      for (int i = 0; i < size; ++i) {
         int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

         SPS2Index::Get_nlj(i1[i], na, la, jja);
         SPS2Index::Get_nlj(i2[i], nb, lb, jjb);
         SPS2Index::Get_nlj(i3[i], nc, lc, jjc);
         SPS2Index::Get_nlj(i4[i], nd, ld, jjd);

         int phase = MINUSto((na - la + nb - lb + nc - lc + nd - ld) / 2);

         Vpp[i] *= phase;
         Vnn[i] *= phase;
         Vpn[i] *= phase;
      }
   }
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::Show() {
   cout << "na   nb   nc   nd" << endl;
   INTERACTION_DATA::iterator it = interaction_data_.begin();
   for (; it != interaction_data_.end(); ++it) {
      NANBNCND nanbncnd = it->first;
      ME_DATA me_data = it->second;

      cout << (int)nanbncnd[kNA] << "    " << (int)nanbncnd[kNB] << "    " << (int)nanbncnd[kNC]
           << "    " << (int)nanbncnd[kND] << endl;

      SIZE_t size = std::get<kSize>(me_data);
      I1_t i1 = std::get<kI1>(me_data);
      I2_t i2 = std::get<kI2>(me_data);
      I3_t i3 = std::get<kI3>(me_data);
      I4_t i4 = std::get<kI4>(me_data);
      J_t J = std::get<kJ>(me_data);
      VPP_t Vpp = std::get<kVpp>(me_data);
      VNN_t Vnn = std::get<kVnn>(me_data);
      VPN_t Vpn = std::get<kVpn>(me_data);

      for (int i = 0; i < size; ++i) {
         int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

         SPS2Index::Get_nlj(i1[i], na, la, jja);
         SPS2Index::Get_nlj(i2[i], nb, lb, jjb);
         SPS2Index::Get_nlj(i3[i], nc, lc, jjc);
         SPS2Index::Get_nlj(i4[i], nd, ld, jjd);
         cout << "\t\t";
         cout << "<I1:" << i1[i] << " I2:" << i2[i] << "|| V || I3:" << i3[i] << " I4:" << i4[i]
              << "> --> ";
         cout << "<(" << la << " " << jja << "/2 ";
         cout << lb << " " << jjb << "/2) J:" << (int)J[i];
         cout << "|| V ||(" << lc << " " << jjc << "/2  ";
         cout << ld << " " << jjd << "/2) J:" << (int)J[i] << "> = " << Vpp[i] << " " << Vnn[i]
              << " " << Vpn[i] << endl;
      }
   }
}

template <typename SelectionRules>
void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell,
                                     vector<NANBNCND>& ho_shells_combinations) {
   SelectionRules IsIncluded;
   ho_shells_combinations.resize(0);

   NANBNCND na_nb_nc_nd;
   for (int na = 0; na <= maxShell; ++na) {
      for (int nb = 0; nb <= maxShell; ++nb) {
         int adxad_Nhw = 0;
         if (na > valence_shell) {
            adxad_Nhw += (na - valence_shell);
         }
         if (nb > valence_shell) {
            adxad_Nhw += (nb - valence_shell);
         }
         if (adxad_Nhw > Nmax) {
            continue;
         }
         na_nb_nc_nd[kNA] = na;
         na_nb_nc_nd[kNB] = nb;

         for (int nc = 0; nc <= maxShell; ++nc) {
            for (int nd = 0; nd <= maxShell; ++nd) {
               int taxta_Nhw = 0;
               if (nc > valence_shell) {
                  taxta_Nhw += (nc - valence_shell);
               }
               if (nd > valence_shell) {
                  taxta_Nhw += (nd - valence_shell);
               }
               if (taxta_Nhw > Nmax) {
                  continue;
               }
               na_nb_nc_nd[kNC] = nc;
               na_nb_nc_nd[kND] = nd;

               if (IsIncluded(na, nb, nd, nc))  // operator has this combination of HO shells
               {
                  ho_shells_combinations.push_back(na_nb_nc_nd);  //	in such case we do not need
               }
            }
         }
      }
   }
   /*
                na_nb_nc_nd[kNA] = 2;
            na_nb_nc_nd[kNB] = 2;
            na_nb_nc_nd[kNC] = 2;
            na_nb_nc_nd[kND] = 2;
            ho_shells_combinations.push_back(na_nb_nc_nd);	//	in such case we do not need

                na_nb_nc_nd[kNA] = 3;
            na_nb_nc_nd[kNB] = 3;
            na_nb_nc_nd[kNC] = 3;
            na_nb_nc_nd[kND] = 3;
            ho_shells_combinations.push_back(na_nb_nc_nd);	//	in such case we do not need

                na_nb_nc_nd[kNA] = 5;
            na_nb_nc_nd[kNB] = 5;
            na_nb_nc_nd[kNC] = 7;
            na_nb_nc_nd[kND] = 7;
            ho_shells_combinations.push_back(na_nb_nc_nd);	//	in such case we do not need
   */
   // shrink vector to its proper size to save some memory
   vector<NANBNCND>(ho_shells_combinations.begin(), ho_shells_combinations.end())
       .swap(ho_shells_combinations);
}

void ComputeTensorStrenghts(const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0,
                            const uint32_t number_twobody_me, const uint16_t* i1,
                            const uint16_t* i2, const uint16_t* i3, const uint16_t* i4,
                            const uint8_t* J, const double* Vpp, const double* Vnn,
                            const double* Vpn, const uint16_t* lalb_indices,
                            const uint16_t* ldlc_indices, const uint16_t* lfli_indices,
                            const double* abf_cgSU3, const double* dci_cgSU3,
                            const double* fi0_cgSU3, double* results) {
   static CWigner9j wigner9j_calculator;
   static CWigner6j wigner6j_calculator;
   int index_LfLi;
   int na, nb, nc, nd, la, lb, lc, ld, jja, jjb, jjc, jjd, JJ;
   //	Lf_max: number of possible values Lf can have in wf irrep.
   int Lf_max(wf.lm + wf.mu + 1);
   //	Li_max: number of possible values Li can have in wi irrep.
   int Li_max(wi.lm + wi.mu + 1);
   int ki_max, kf_max, k0_max;
   int rho0_max(w0.rho);
   int phase;
   double phaseSU2;

//   double local_results[18] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// vector local_results contains (k0max*rho0max) elements.
// In case of 2B interaction, L0 = S0 = 0, 1, 2 ==> we have k0max <= 2 
   std::vector<double> local_results;
// reserving 64 elements means we can handle even k0max=2 & rho0max=32 case
   local_results.reserve(64);

   //	loop over {na la ja nb lb jb nc lc jc nd ld jd J} ---> {Vpp, Vnn, Vpn}
   for (int ime = 0; ime < number_twobody_me; ime++) {
      /*
                      cout << "i1:" << (int) i1i2i3i4J[ilabel] << " ";
                      cout << "i2:" << (int) i1i2i3i4J[ilabel+1] << " ";
                      cout << "i3:" << (int) i1i2i3i4J[ilabel+2] << " ";
                      cout << "i4:" << (int) i1i2i3i4J[ilabel+3] << " ";
                      cout << "J:" << (int) i1i2i3i4J[ilabel+4] << endl;
      */
      SPS2Index::Get_nlj(i1[ime], na, la, jja);
      SPS2Index::Get_nlj(i2[ime], nb, lb, jjb);
      SPS2Index::Get_nlj(i3[ime], nc, lc, jjc);
      SPS2Index::Get_nlj(i4[ime], nd, ld, jjd);
      JJ = 2 * J[ime];

      int iab_cgs0 =
          lalb_indices[la * (nb + 1) +
                       lb];  // abf_cgSU3[iab_cgs] ---> <(na 0) 0 la; (nb 0) 0 lb || wf * *>_{0}
      int idc_cgs0 =
          ldlc_indices[ld * (nc + 1) +
                       lc];  // dci_cgSU3[idc_cgs] ---> <(0 nd) 0 ld; (0 nc) 0 lc || wi * *>_{0}

      for (int iS = 0; iS < 6; iS++) {
         int S0 = SfSiS0[iS][2];
         k0_max = SU3::kmax(w0, S0);

//         memset(local_results, 0, sizeof(local_results));
         local_results.resize(k0_max*rho0_max);
         std::fill(local_results.begin(), local_results.end(), 0);

         if (idc_cgs0 != kDoesNotExist && iab_cgs0 != kDoesNotExist && k0_max) {
            int Sf = SfSiS0[iS][0];
            int Si = SfSiS0[iS][1];

            for (int Lf = abs(la - lb), iab_cgs = iab_cgs0; (Lf <= (la + lb) && Lf < Lf_max);
                 ++Lf, iab_cgs += kf_max) {
               kf_max = SU3::kmax(wf, Lf);
               if (!kf_max) {
                  continue;
               }

               if (!SU2::mult(2 * Lf, 2 * Sf, JJ))  // wigner9j(la, lb, Lf, 1, 1, Sf, ja, jb, J) = 0
               {
                  continue;
               }
               double wig1 =
                   wigner9j_calculator.GetWigner9j(2 * la, 2 * lb, 2 * Lf, 2 * Sf, jja, jjb, JJ);
               //					double wig1 = GetWigner9j(2*la, 2*lb, 2*Lf,
               //2*Sf, jja, jjb, JJ);

               for (int Li = abs(ld - lc), idc_cgs = idc_cgs0; (Li <= (ld + lc) && Li < Li_max);
                    ++Li, idc_cgs += ki_max) {
                  ki_max = SU3::kmax(wi, Li);
                  if (!ki_max) {
                     continue;
                  }
                  if (S0 < abs(Lf - Li) || S0 > (Lf + Li)) {
                     continue;
                  }
                  if (!SU2::mult(2 * Li, 2 * Si,
                                 JJ))  // wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J) = 0
                  {
                     continue;
                  }

                  index_LfLi = lfli_indices[Lf * Li_max + Li];  // fi0_cgSU3[index_LfLi] ---> <wf *
                                                                // Lf; wi * Li || w0 * S0min>_{*}
                  if (index_LfLi == kDoesNotExist) {
                     continue;
                  }

                  // at this momnet index_LfLi points to first SU(3) CG with S0min, which may be
                  // less then S0
                  // ==> we need to move it so it points to SU(3) CG corresponding to S0
                  for (int s0 = abs(Lf - Li); s0 < S0; s0++) {
                     index_LfLi += SU3::kmax(w0, s0) * rho0_max * kf_max * ki_max;
                  }

                  phaseSU2 =
                      PiSfSiS0[iS] * MINUSto(Sf + S0 + Li) * std::sqrt((2 * Li + 1) * (2 * Lf + 1));
                  phaseSU2 *=
                      wig1 *
                      wigner9j_calculator.GetWigner9j(2 * ld, 2 * lc, 2 * Li, 2 * Si, jjd, jjc,
                                                      JJ) *
                      wigner6j_calculator.GetWigner6j(2 * Lf, 2 * Li, 2 * S0, 2 * Si, 2 * Sf, JJ);
                  //						phaseSU2*= wig1*GetWigner9j(2*ld, 2*lc, 2*Li, 2*Si, jjd,
                  //jjc, JJ)*GetWigner6j(2*Lf, 2*Li, 2*S0, 2*Si, 2*Sf, JJ);

                  for (int i = 0, j = 0; i < rho0_max * k0_max; ++i) {
                     double su3cg_sum = 0;
                     for (int kf = 0; kf < kf_max; kf++) {
                        double sum_ki_result(0);
                        for (int ki = 0; ki < ki_max; ki++, j++) {
                           sum_ki_result += fi0_cgSU3[index_LfLi + j] * dci_cgSU3[idc_cgs + ki];
                        }
                        su3cg_sum += sum_ki_result * abf_cgSU3[iab_cgs + kf];
                     }
                     local_results[i] += phaseSU2 * su3cg_sum;
                  }
               }  // Li
            }     // Lf
         }        // if (idc_cgs0 != kDoesNotExist && iab_cgs0 != kDoesNotExist && k0_max)

         // array with results is indexed in the following way:
         // [iS][type][k0][rho0] --> iS*3*k0_max*rho0_max + type*k0_max*rho0_max + k0*rho0_max +
         // rho0 where I assume that k0_max is always <= 2
         uint32_t starting_index = iS * 6 * rho0_max;  // 6 = 3 * 2 ... #types x (k0_max==2)
         for (int j = 0, i = 0; j < k0_max * rho0_max; j++, i += 3) {
            results[starting_index + i] += local_results[j] * Vpp[ime];
            results[starting_index + i + 1] += local_results[j] * Vnn[ime];
            results[starting_index + i + 2] += local_results[j] * Vpn[ime];
         }
      }  // iS
   }     // ime
}

void SaveOnDisk(std::vector<std::vector<NANBNCND> >& all_nanbncnd,
                std::vector<std::vector<uint32_t> >& all_results_ntensors,
                std::vector<std::vector<uint8_t> >& all_results_iS_wf_wi_w0,
                std::vector<std::vector<double> >& all_results_coeffs, std::ofstream& outfile) {
   assert(all_nanbncnd.size() == all_results_ntensors.size() == all_results_iS_wf_wi_w0.size() ==
          all_results_coeffs.size());

   for (int rank = 0; rank < all_nanbncnd.size(); ++rank) {
      uint32_t fi0_index = 0;
      uint32_t coeff_index = 0;
      for (int k = 0; k < all_nanbncnd[rank].size(); ++k) {
         int na = all_nanbncnd[rank][k][0];
         int nb = all_nanbncnd[rank][k][1];
         int nc = all_nanbncnd[rank][k][2];
         int nd = all_nanbncnd[rank][k][3];

         uint32_t ntensors = all_results_ntensors[rank][k];

         for (int itensor = 0; itensor < ntensors; ++itensor, fi0_index += 7) {
            int iS = all_results_iS_wf_wi_w0[rank][fi0_index];
            int SSf = 2 * SfSiS0[iS][0];
            int SSi = 2 * SfSiS0[iS][1];
            int SS = 2 * SfSiS0[iS][2];
            int lmf = all_results_iS_wf_wi_w0[rank][fi0_index + 1];
            int muf = all_results_iS_wf_wi_w0[rank][fi0_index + 2];
            int lmi = all_results_iS_wf_wi_w0[rank][fi0_index + 3];
            int mui = all_results_iS_wf_wi_w0[rank][fi0_index + 4];
            int lm0 = all_results_iS_wf_wi_w0[rank][fi0_index + 5];
            int mu0 = all_results_iS_wf_wi_w0[rank][fi0_index + 6];

            SU3xSU2::LABELS wf(1, lmf, muf, SSf);
            SU3xSU2::LABELS wi(1, lmi, mui, SSi);
            SU3xSU2::LABELS w0(1, lm0, mu0, SS);
            w0.rho = SU3::mult(wf, wi, w0);

            outfile << na << " " << nb << " " << nd << " " << nc;
            outfile << "\t\t" << wf << "\t" << wi << "\t" << w0 << "\n";

            int k0_max = SU3::kmax(w0, w0.S2 / 2);
            assert(k0_max);

            double* coeffs = &all_results_coeffs[rank][coeff_index];
            for (int k0 = 0, j = 0; k0 < k0_max; k0++) {
               for (int rho0 = 0; rho0 < w0.rho; ++rho0, j += 3) {
                  if (Negligible(coeffs[j])) {
                     outfile << 0 << " ";
                  } else {
                     outfile << coeffs[j] << " ";
                  }

                  if (Negligible(coeffs[j + 1])) {
                     outfile << 0 << " ";
                  } else {
                     outfile << coeffs[j + 1] << " ";
                  }

                  if (Negligible(coeffs[j + 2])) {
                     outfile << 0 << "\n";
                  } else {
                     outfile << coeffs[j + 2] << "\n";
                  }
               }
            }
            coeff_index += 3 * k0_max * w0.rho;
         }
      }
   }
}

template <typename NotIncluded>
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::PerformSU3Decomposition(
    const string& sOutputFileName) {
   boost::mpi::communicator mpi_comm_world;
   int my_rank = mpi_comm_world.rank();
   int nprocs = mpi_comm_world.size();

#ifdef TIMING
   std::chrono::system_clock::time_point start;
   std::chrono::duration<double> total_duration_SU3_1(0), total_duration_SU3_2(0),
       total_duration_SU3_3(0), total_duration_transformation(0), duration;
#endif

   vector<uint16_t> lalb_indices;  // (la, lb): lalb_indices[la*lb_max + lb] ---> <(na 0) la; (nb 0)
                                   // lb || wf * *>
   vector<uint16_t> ldlc_indices;  // (ld, lc): lalb_indices[ld*lc_max + lc] ---> <(0 nd) ld; (0 nc)
                                   // lc || wi * *>

   vector<double> abf_cgSU3;  // <(na 0) * (nb 0) * || (lm mu)f *>
   vector<double> dci_cgSU3;  // <(0 nd) * (0 nc) * || (lm mu)i *>

   //
   abf_cgSU3.reserve(1024);
   dci_cgSU3.reserve(1024);

   SU3::LABELS ir[4] = {SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0)};

   SU3_VEC FinalIrreps;  //	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
   SU3_VEC InitIrreps;   //	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)

   FinalIrreps.reserve(nmax + 1);  // (na 0) x (nb 0) ---> max(na, nb) SU(3) irreps
   InitIrreps.reserve(nmax + 1);

   std::vector<NANBNCND> nanbncnd;
   // each element of results_ntensors array contains number of tensors generated
   // for a given na nb nc nd combination
   std::vector<uint32_t> results_ntensors;
   // array with tensor labels: iS lmf muf lmi mui lm0 mu0
   std::vector<uint8_t> results_iS_wf_wi_w0;
   std::vector<double> results_coeffs;

   //	iterate over {na, nb, nc, nd} HO shell quantum numbers
   for (ME_ITER nanbncnd_tbme = interaction_data_.begin(); nanbncnd_tbme != interaction_data_.end();
        ++nanbncnd_tbme) {
      double* meV;
      uint16_t* labels;
      int me_phase;
      //		Obtain a set of matrix elements {<na* nb*|| {Vpp, Vnn, Vpn} || nc* nd*>}
      ME_DATA me_data = nanbncnd_tbme->second;

      SIZE_t size = std::get<kSize>(me_data);

      assert(size != 0);
      //		cout << "size:" << size << endl;

      I1_t i1 = std::get<kI1>(me_data);
      I2_t i2 = std::get<kI2>(me_data);
      I3_t i3 = std::get<kI3>(me_data);
      I4_t i4 = std::get<kI4>(me_data);
      J_t J = std::get<kJ>(me_data);
      VPP_t Vpp = std::get<kVpp>(me_data);
      VNN_t Vnn = std::get<kVnn>(me_data);
      VPN_t Vpn = std::get<kVpn>(me_data);

      ir[kNA].lm = nanbncnd_tbme->first[kNA];
      ir[kNB].lm = nanbncnd_tbme->first[kNB];
      ir[kNC].mu = nanbncnd_tbme->first[kNC];
      ir[kND].mu = nanbncnd_tbme->first[kND];

      if (my_rank == 0) {
         std::cout << (int)ir[kNA].lm << " " << (int)ir[kNB].lm << " " << (int)ir[kNC].mu << " "
                   << (int)ir[kND].mu << "\n";
      }

      FinalIrreps.clear();
      InitIrreps.clear();
      SU3::Couple(ir[kNA], ir[kNB], FinalIrreps);  //	(na 0) x (nb 0) -> rho=1{(lmf muf)}
      SU3::Couple(ir[kND], ir[kNC], InitIrreps);   //	(0 nd) x (0 nc) -> rho=1{(lmi mui)}

      //		number of tensors generated for a given set of tbme with a given na nb
      //		nc nd combination
      uint32_t ntensors = 0;
      //	iterate over (lmf muf) irreps
      for (int iif = 0; iif < FinalIrreps.size(); ++iif) {
         ComputeSU3CGs_Simple(ir[kNA], ir[kNB], FinalIrreps[iif], lalb_indices, abf_cgSU3);

         //	iterate over (lmi mui) irreps
         for (int ii = 0; ii < InitIrreps.size(); ++ii) {
            ComputeSU3CGs_Simple(ir[kND], ir[kNC], InitIrreps[ii], ldlc_indices, dci_cgSU3);

            SU3_VEC Irreps0;
            SU3::Couple(FinalIrreps[iif], InitIrreps[ii],
                        Irreps0);  // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
            //	exclude [i.e. remove] tensors with unphysical SU(3) quantum numbers and
            //	calculate number of valid tensors
            int number_ir0 = std::distance(
                Irreps0.begin(), std::remove_if(Irreps0.begin(), Irreps0.end(), NotIncluded()));
            if (number_ir0 == 0) {
               continue;
            }
#pragma omp parallel
            {
               // [iS][type][k0][rho0] --> iS*3*k0_max*rho0_max + type*k0_max*rho0_max + k0*rho0_max
               // + rho0 6 ... number of Sf Si S0 combinations 3 ... PP NN PN 2 ... k0_max <= 2 
               vector<uint16_t>
                   lfli_indices;  // (Lf, Li): lflf_indices[Lf*Li_max + Li] ---> {<wf * Lf; wi * Li
                                  // || w0 * L0=0>_{*}, <wf * Lf; wi * Li || w0 * L0=1>_{*}, <wf *
                                  // Lf; wi * Li || w0 * L0=2>_{*} }
               vector<double> fi0_cgSU3;  // <wf * wi * || w0 * L0={0,1,2}>_{*}
               fi0_cgSU3.reserve(1024);
//	iterate over rho(lmu0 mu0)
#pragma omp for schedule(dynamic)
               for (int i0 = 0; i0 < number_ir0; ++i0) {
                  int rho0_max = Irreps0[i0].rho;
                  double wfwiw0_results[6 * 3 * 2 * rho0_max];
                  memset(wfwiw0_results, 0, sizeof(double) * 6 * 3 * 2 * rho0_max);
/*
                                                if (my_rank == 0)
                                                {
                                                        cout << FinalIrreps[iif] << "\t" <<
   InitIrreps[ii] << "\t" << Irreps0[i0]; cout.flush();
                                                }
*/
#ifdef TIMING
                  start = std::chrono::system_clock::now();
#endif
                  ComputeSU3CGs(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], lfli_indices,
                                fi0_cgSU3);
#ifdef TIMING
                  duration = std::chrono::system_clock::now() - start;
                  total_duration_SU3_3 += duration;
/*
                                                if (my_rank == 0)
                                                {
                                                        cout << "\t" << duration.count();
   cout.flush();
                                                }
*/
#endif
                  /*
                   *	Scalar opetator [i.e. J0=0] has L0==S0. Since we are dealing with
                   *	a two-body interaction ==> S0 = 0, 1, or 2.
                   *	S0=0 appears 2 times: Sf=0 x Si=0 --> S0=0 and Sf=1 x Si=1 --> S0=0
                   *	==> 2 x SU3::kmax(w0, L0=0) x rho0_max x 3
                   *
                   *	S0=1 appears 3 times: Sf=1 x Si=0 --> S0=1; Sf=0 x Si=1 --> S0=0; Sf=1 x
                   *Si=1 ---> S0=1
                   *	==> 3 x SU3::kmax(w0, L0=1) x rho0_max x 3
                   *
                   *	S0=2 appears 1 times: Sf=1 x Si=1 --> S0=2
                   *	==> 1 x SU3::kmax(w0, L0=1) x rho0_max x 3
                   *
                   *	Two body interaciton has L0=S0 and  can be either 0, 1, or 2.
                   *
                   */
                  //	int nresults = (2*(SU3::kmax(Irreps0[i0], 0)) + 3*SU3::kmax(Irreps0[i0], 1)
                  //+ SU3::kmax(Irreps0[i0], 2))*Irreps0[i0].rho*3; 	Order of elements in results:
                  //results[iS, k0, rho0, type], where iS = 0 ... 6 [= number of Sf Si S0 quantum
                  //numbers for two-body interaction] 	index = iS x k0_max x rho0_max x 3 + k0 x
                  //rho0_max x 3 + rho0 x 3 + type

#ifdef TIMING
                  start = std::chrono::system_clock::now();
#endif
                  ComputeTensorStrenghts(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], size, i1,
                                         i2, i3, i4, J, Vpp, Vnn, Vpn, &lalb_indices[0],
                                         &ldlc_indices[0], &lfli_indices[0], &abf_cgSU3[0],
                                         &dci_cgSU3[0], &fi0_cgSU3[0], wfwiw0_results);
#ifdef TIMING
                  duration = std::chrono::system_clock::now() - start;
                  total_duration_transformation += duration;
/*
                                                if (my_rank == 0)
                                                {
                                                        cout << "\t" << duration.count();
                                                }
*/
#endif
                  /*
                                                                  if (my_rank == 0)
                                                                  {
                                                                          cout << endl;
                                                                  }
                  */
                  for (int iS = 0, index = 0, k0_max = 0; iS < 6; ++iS, index += 3 * 2 * rho0_max) {
                     k0_max = SU3::kmax(Irreps0[i0], SfSiS0[iS][2]);
                     if (!k0_max) {
                        continue;
                     }

                     if (std::count_if(&wfwiw0_results[index],
                                       &wfwiw0_results[index + 3 * k0_max * rho0_max],
                                       Negligible) != 3 * k0_max * rho0_max) {
#pragma omp critical
                        {
                           results_iS_wf_wi_w0.push_back(iS);
                           results_iS_wf_wi_w0.push_back(FinalIrreps[iif].lm);
                           results_iS_wf_wi_w0.push_back(FinalIrreps[iif].mu);
                           results_iS_wf_wi_w0.push_back(InitIrreps[ii].lm);
                           results_iS_wf_wi_w0.push_back(InitIrreps[ii].mu);
                           results_iS_wf_wi_w0.push_back(Irreps0[i0].lm);
                           results_iS_wf_wi_w0.push_back(Irreps0[i0].mu);
                           results_coeffs.insert(results_coeffs.end(), &wfwiw0_results[index],
                                                 &wfwiw0_results[index + 3 * k0_max * rho0_max]);
                           ntensors++;
                        }
                     }
                  }  // loop over SfSiS triplets
               }     // loop over w0
            }        // #pragma parallel
         }           // loop over wi
      }              // loop over wf
      results_ntensors.push_back(ntensors);
      nanbncnd.push_back(nanbncnd_tbme->first);
   }  // loop over {na, nb, nc, nd} combinations
#ifdef TIMING
   if (my_rank == 0) {
      cout << "Computing <wf wi || w0>:" << total_duration_SU3_3.count() << "\t"
           << "\t Transformation:" << total_duration_transformation.count() << endl;
   }
#endif

   mpi_comm_world.barrier();

   if (my_rank == 0) {
      std::ofstream outfile(sOutputFileName.c_str());
      if (!outfile) {
         std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
         exit(EXIT_FAILURE);
      }
      outfile.precision(10);

      std::vector<std::vector<NANBNCND> > all_nanbncnd;
      std::vector<std::vector<uint32_t> > all_results_ntensors;
      std::vector<std::vector<uint8_t> > all_results_iS_wf_wi_w0;
      std::vector<std::vector<double> > all_results_coeffs;

      all_nanbncnd.reserve(nprocs);
      all_results_ntensors.reserve(nprocs);
      all_results_iS_wf_wi_w0.reserve(nprocs);
      all_results_coeffs.reserve(nprocs);

      gather(mpi_comm_world, nanbncnd, all_nanbncnd, 0);
      gather(mpi_comm_world, results_ntensors, all_results_ntensors, 0);
      gather(mpi_comm_world, results_iS_wf_wi_w0, all_results_iS_wf_wi_w0, 0);
      gather(mpi_comm_world, results_coeffs, all_results_coeffs, 0);

      SaveOnDisk(all_nanbncnd, all_results_ntensors, all_results_iS_wf_wi_w0, all_results_coeffs,
                 outfile);
   } else {
      gather(mpi_comm_world, nanbncnd, 0);
      gather(mpi_comm_world, results_ntensors, 0);
      gather(mpi_comm_world, results_iS_wf_wi_w0, 0);
      gather(mpi_comm_world, results_coeffs, 0);
   }
}

int main(int argc, char* argv[]) {
   boost::mpi::environment env(argc, argv);
   boost::mpi::communicator mpi_comm_world;
   int my_rank = mpi_comm_world.rank();
#ifdef USER_FRIENDLY_OUTPUT
   if (my_rank == 0) {
      cerr << "You have to comment line with '#define USER_FRIENDLY_OUTPUT' in UNU3SU3basics.h !"
           << endl;
   }
   boost::mpi::environment::abort(EXIT_FAILURE);
   return EXIT_FAILURE;
#endif
   /** \todo nmax could be obtained from input_file */
   if (argc != 5) {
      if (my_rank == 0) {
         cout << endl;
         cout << "Usage: " << argv[0]
              << " <Nmax> <valence shell> <file in proton-neutron J-coupled format> <output file "
                 "with SU3 tensors>"
              << endl;
         cout << endl;
         cout << endl;
         cout << endl;
         cout << endl;
         cout << "Structure of the outputfile:" << endl;
         cout << "n1 n2 n3 n4 IRf IRi IR0 iS" << endl;
         cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 0" << endl;
         cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 0" << endl;
         cout << " . " << endl;
         cout << " . " << endl;
         cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 0" << endl;
         cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 1" << endl;
         cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 1" << endl;
         cout << " . " << endl;
         cout << " . " << endl;
         cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
         cout << " . " << endl;
         cout << " . " << endl;
         cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   int Nmax = atoi(argv[1]);
   int valence_shell = atoi(argv[2]);
   int nmax = Nmax + valence_shell;

   //	All ranks create a list of {na nb nc nd} HO shells that are allowed by
   //	CInteractionSelectionRules.
   vector<NANBNCND> ho_shells_combinations;
   GenerateAllCombinationsHOShells<ParityPreserving>(Nmax, valence_shell, nmax,
                                                     ho_shells_combinations);

   if (my_rank == 0) {
      cout << "Number of HO shell combinations:" << ho_shells_combinations.size()
           << " will be split among " << mpi_comm_world.size() << " MPI processes." << endl;
   }

   //	for the latter to work properly
   string sFileName(argv[3]);
   string sFileSU3Decomposition(argv[4]);

   //	Root reads interaction file and distributes two-body matrix elements i-th
   //	combination of {na, nb, nc, nd} will be assigned to process with
   //	rank ==  i % number of processes
   JCoupledProtonNeutron2BMe_Hermitian_SingleOperator JCoupledPNME(sFileName,
                                                                   ho_shells_combinations);

   //	mpi_comm_world.barrier();
   //	return 0;

   //	JCoupledPNME.Show();

   //	Multiply each matrix element by factor that depends on its quantum numbers
   JCoupledPNME.SwitchHOConvention();
   JCoupledPNME.MultiplyByCommonFactor();

#pragma omp parallel
   su3::init_thread();
   //	Perform decomposition and then send results to root rank which stores
   //	them on disk
   JCoupledPNME.PerformSU3Decomposition<Cinteraction_sel_rules>(sFileSU3Decomposition);
#pragma omp barrier
#pragma omp parallel
   su3::finalize_thread();

   //	JCoupledPNME.PerformSU3Decomposition<CVcoul>(sFileSU3Decomposition);
   //	JCoupledPNME.PerformSU3Decomposition<CTrelSelectionRules>(sFileSU3Decomposition);

   return EXIT_SUCCESS;
}
