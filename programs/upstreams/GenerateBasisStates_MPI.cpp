#include <mpi.h>
#include <boost/mpi.hpp>

#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 

#include "SU3ME/ModelSpaceExclusionRules.h"
#include "LSU3/ncsmSU3xSU2Basis.h"
#include "SU3ME/global_definitions.h"

int main(int argc,char **argv)
{
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator comm_world;

	if (argc != 2 && argc != 4)
	{
		cout << "Usage: " << argv[0] << " <model space> [<ndiag> <idiag>]" << endl;
		cout << "Generate many-body basis states for N segments (N is the number of processors) and store them on disk." << endl;
		return EXIT_FAILURE;
	}

	int nprocs;
	int my_rank;
	int ndiag;
	int idiag;

	nprocs = comm_world.size();
	my_rank = comm_world.rank();

	if (argc == 2)
	{
		ndiag = nprocs;
		idiag = my_rank;
	}
	else
	{
		if (nprocs > 1)
		{
			std::cerr << argv[0] << " must be run as a serial program when specifying ndiag and idiag parameters." << endl;
			return EXIT_FAILURE;
		}
		ndiag = atoi(argv[2]);
		idiag = atoi(argv[3]);
	}

	std::string model_space_name(argv[1]);
	proton_neutron::ModelSpace ncsmModelSpace(model_space_name);
//	Generate <idiag> segment of basis defined by <ncsmModelSpace> and made up of <ndiag> segments.	
	lsu3::CncsmSU3xSU2Basis  basis(ncsmModelSpace, idiag, ndiag);

	std::size_t found = model_space_name.find(".model_space");
  	if (found == std::string::npos)
	{
    	std::cout << "Model space file name needs to end with '.model_space'!" << std::endl;
		return EXIT_FAILURE;
	}
//	generate file name for the output binary file with generated <idiag>
//	segment of basis which consists of <ndiag> segments.
	std::string header(model_space_name.substr(0, found));
	std::string basis_file_name = MakeBasisName(header, idiag, ndiag);

	std::ofstream basis_file(basis_file_name, std::ios::binary);
	if (!basis_file)
	{
		std::cerr << "Could not open the output file" << std::endl;
		return EXIT_FAILURE;
	}

	boost::archive::binary_oarchive ia(basis_file);
	ia << basis;
}
